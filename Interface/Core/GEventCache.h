#ifndef GEVENTCACHE_H
#define GEVENTCACHE_H
/*!
	File: GEventCache.h
	Purpose: A Gateware interface that allows recording of GEvents from a GEventGenerator for later processing.
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Lari H. Norri
	Contributors: N/A
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "GThreadShared.h"

namespace GW
{
	namespace I
	{
		class GEventCacheInterface : public virtual GInterfaceInterface
		{
		public:
			virtual GReturn Append(const GEvent& _inEvent) = 0;
			virtual GReturn Waiting(unsigned int& _outCount) const = 0;
			virtual GReturn Pop(GEvent& _outEvent) = 0;
			virtual GReturn Peek(GEvent& _outEvent) const = 0;
			virtual GReturn Peek(unsigned int _eventIndex, GEvent& _outEvent) const = 0;
			virtual GReturn Max(unsigned int& _outSize) const = 0;
			virtual GReturn Missed(unsigned int& _outCount) const = 0;
			virtual GReturn Clear() = 0;
			template<class eventType>
			GReturn Find(eventType _check, bool _remove);
			template<class eventType, typename eventData>
			GReturn Find(eventType _check, bool _remove, eventData& _outData);
		};
	}
};

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Core/GEventCache/GEventCache.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace CORE
	{
		//! A Proxy class that can be used to record and playback GEvents captured from GEventGenerators. 
		/*!
		*	Stores all events produced by an interface derived from GEventGenerator that you are Registered to.
		*   **NOTE: GEventCache is a replacement proxy to GEventQueue which is deprecated.** 
		*	\code{.cpp}
		*		GEventCache myCache;
		*		myCache.Create( 32 ); // can hold up to 32 unique GEvents, try to keep this as small as needed
		*		someEventGenerator.Register(myCache);
		*		// ... some recurring function or thread where you wish to process waiting events ...
		*		GEvent g; 
		*		while (+myCache.Pop(g)) // if you wait too long to process events the oldest ones may be lost.
		*		{
		*			GEventGenerator::Events e; // substitute the generator type you care about
		*			GEventGenerator::EVENT_DATA d; // substitute the event data you care about
		*			// Determine if the "Pop"ed event contains the data you are interested in.
		*			if (+g.Read(e,d)) // if you are interested in multiple events consider using a switch below
		*				if (e == GEventGenerator::Events::NON_EVENT) // substitute actual event
		*					std::cout << d.noData; // substitute actual data
		*		}
		*	\endcode
		*/
		class GEventCache final
			: public I::GProxy<I::GEventCacheInterface, I::GEventCacheImplementation, unsigned int> // cache size
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GEventCache)
			GATEWARE_FUNCTION(Append)
			GATEWARE_CONST_FUNCTION(Waiting)
			GATEWARE_FUNCTION(Pop)
			GATEWARE_CONST_FUNCTION(Peek)
			GATEWARE_CONST_FUNCTION(Max)
			GATEWARE_CONST_FUNCTION(Missed)
			GATEWARE_FUNCTION(Clear)
			GATEWARE_TEMPLATE_FUNCTION(Find)
			//! \endcond
			
			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Allocates & Initializes a GEventCache. 
			/*!
			*	A GEventCache is capable of receiving and storing events created by any GEventGenerator.
			*	You can use this class whenever there is a Gateware class that spawns events you wish record.
			*	Unlike GEventResponder the GEventCache is designed to save multiple events up to a limit defined by you.
			*	If you need to process events on a specific thread, you can use this class to delay processing until
			*	a safe time to do so. Just make sure there is adequate capacity in the Cache, and "Pop" them off when ready.
			*
			*	\param [in] _cacheSize		The maximum amount of GEvents that can be retained by this instance. (must be > 0)
			*								Exceeding this amount without processing a message will cause the oldest to missed.
			*
			*	\retval GReturn::INVALID_ARGUMENT	A valid Cache Capacity > 0 must be provided.
			*	\retval GReturn::FAILURE	Allocation has failed, you don't have enough memory.
			*	\retval GReturn::SUCCESS	The cache was successfully allocated, you can now Register it to GEventGenerators.
			*/
			GReturn Create(unsigned int _cacheSize);

			//! Appends an event to the cache, and increases number of waiting events.   
			/*!
			*	Generally end users can ignore this function as it is invoked by a registered GEventGenerator.
			*
			*	\param [in] _inEvent	The GEvent you wish to store in the Cache. If the Cache is already full of
			*							waiting (unpopped) events then the oldest one falls off the front.
			*
			*	\retval GReturn::SUCCESS	The event was appended to this receiver.
			*/
			virtual GReturn Append(const GEvent& _inEvent) = 0;

			//! Reports how many events have not yet been processed.  
			/*!
			*	Use this to check if any events are currently waiting for you.
			*
			*	\param [out] _outCount The number of waiting events. (use Pop() or Clear() to remove them)
			*
			*	\retval GReturn::SUCCESS	Reports the current amount of waiting events.
			*/
			virtual GReturn Waiting(unsigned int& _outCount) const = 0;

			//! Grabs a waiting event if one is available. Oldest first. 
			/*!
			*	If waiting is non-zero this operation will extract an event and reduce the number of waiting events.
			*	After a successful Pop() the number of waiting event will be reduced by 1.
			*
			*	\param [out] _outEvent The most recent event that needs to be processed.
			*
			*	\retval GReturn::FAILURE	There were no waiting events that needed processing.
			*	\retval GReturn::SUCCESS	Successfully pulled a waiting event from the Queue.
			*/
			virtual GReturn Pop(GEvent& _outEvent) = 0;

			//! Examines but does not remove a waiting event if one is available based on it's historical index. 
			/*!
			*	Used to examine the GEventCache's current backlog.
			*
			*	\param [in] _eventIndex	(OPTIONAL) The event you wish to examine from 0 to "Waiting"-1, with 0 being the oldest item in the Cache.
			*	\param [out] _outEvent A copy of the event at '_eventIndex' waiting it's turn to be processed. (_eventIndex defaults to 0)
			*
			*	\retval GReturn::INVALID_ARGUMENT	The provided _eventIndex was out of bounds.
			*	\retval GReturn::FAILURE	There were no waiting events that could be inspected.
			*	\retval GReturn::SUCCESS	Successfully copied a waiting event from the Queue.
			*/
			virtual GReturn Peek(unsigned int _eventIndex, GEvent& _outEvent) const = 0;

			//! Reports the Maximum amount of Events that this GEventCache instance can record.  
			/*!
			*	This returns the same number that is passed to the GEventCache "Create" function.
			*	Any events recorded while the Queue is at capacity will cause the oldest event to be lost.
			*	Whenever an Event is replaced without processing, it will increase the internal "Missed" count.
			*
			*	\param [out] _outCapacity	The maximum capacity of this GEventCache.
			*
			*	\retval GReturn::SUCCESS	The maximum capacity was successfully reported.
			*/
			virtual GReturn Max(unsigned int& _outCapacity) const = 0;

			//! Reports how many events were not processed before they were replaced.
			/*!
			*	If an event was not processed via Pop(), Clear(), or Find() and ends up being replaced
			*	it will increase the internal event miss count. Use this operation to see how many events have been lost.
			*	**NOTE: Never want to miss an event? considering using GEventResponder or directly registering a callback.**
			*
			*	\param [out] _outCount Number of events that were not processed.
			*
			*	\retval GReturn::SUCCESS	Reports the current amount of missed events.
			*/
			virtual GReturn Missed(unsigned int& _outCount) const = 0;

			//! Removes any & all waiting events without processing them.
			/*!
			*	All pending events are removed. The internal missed event count will not be reset.
			*	NOTE: Though you cannot reset the internal miss count it is possible to track it's change.
			*
			*	\retval GReturn::FAILURE	There were no waiting events, nothing was cleared.
			*	\retval GReturn::SUCCESS	All waiting events were removed successfully.
			*/
			virtual GReturn Clear() = 0;

			//! Finds a specific Event/DataChunk and optionally can remove it and/or return the associated data. 
			/*!
			*	A convenience function that can be used instead of "Pop/Peek" to search waiting events.
			*
			*	\param [in] _check	The specific enum of the event you are looking for, can also be a specific bit of data.
			*	\param [in] _remove	Should the function remove the waiting event if it is found? true == Pop, false == Peek
			*	\param [out] _outData (OPTIONAL) Returns the data block associated with the event if found.
			*
			*	\retval GReturn::FAILURE	No Event or Data was present matching the provided Query.
			*	\retval GReturn::SUCCESS	Operation successfully found and/or removed the event/data in question.
			*/
			template<class eventType, typename eventData>
			GReturn Find(eventType _check, bool _remove, eventData& _outData);
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GEVENTCACHE_H