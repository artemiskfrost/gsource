#ifndef GLOGIC_H
#define GLOGIC_H

/*!
	File: GLogic.h
	Purpose: An interface that can be used to safely store/swap & invoke custom logic.(Functor)  
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Lari H. Norri
	Contributors: N/A
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "GThreadShared.h"
#include <functional>

namespace GW
{
	namespace I
	{
		class GLogicInterface : public virtual GInterfaceInterface
		{
		public:
			virtual GReturn Assign(std::function<void()> _newLogic) = 0;
			virtual GReturn Invoke() const = 0;
		};
	}
};

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Core/GLogic/GLogic.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace CORE
	{
		//! A Proxy/Functor class that can be used to safely store and invoke stored routines/handlers. 
		/*!
		*	The GLogic Interface stores and allows safe swapping of user logic (including GEvent handlers).
		*	It combines the lifetime tracking of a GInterface(via Proxy) with the flexibility of std::function.
		*/
		class GLogic final
			: public I::GProxy<I::GLogicInterface, I::GLogicImplementation, std::function<void()>>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GLogic)
			GATEWARE_FUNCTION(Assign)
			GATEWARE_CONST_FUNCTION(Invoke)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Allocates & Initializes a GLogic. 
			/*!
			*	A GLogic is capable of storing and swapping an std::function which you can safely Invoke.
			*	During the function's invocation you are guaranteed the Proxy will remain valid and the logic
			*	will not be swapped or recursively overwritten.
			*	
			*	\param	[in] _logic		The function or lambda you wish to be invoked when Invoke() is called.
			*							(optional can pass nullptr). May be invoked from a different thread.
			*
			*	\retval GReturn::SUCCESS	The provided logic was stored within this Proxy. 
			*/
			GReturn Create(std::function<void()> _logic);

			//! Overwrites the current logic assigned to this Proxy (if any)
			/*!
			*	Assigns new user provided logic (nullptr[no logic] is a valid argument).
			*	Cannot be done within provided logic as the class guarantees the logic
			*	stack will remain valid for the duration of Invoke().
			*	**Deadlocks will only be detected/prevented in Debug builds, check your logic for errors**
			*
			*	\param	[in] _newLogic		The function or lambda you wish to overwrite the existing logic.
			*
			*	\retval GReturn::DEADLOCK	You have attempted to modify the logic during it's own execution.
			*	\retval GReturn::SUCCESS	Stored logic was successfully overwritten.
			*/
			virtual GReturn Assign(std::function<void()> _newLogic) = 0;

			//! Invokes the stored function/lambda from the current thread.
			/*!
			*	Calls the stored logic on this thread, during the invocation the logic and proxy will remain valid.
			*	During the operation Recursive calls are permitted but swapping the logic of your own proxy is not.
			*
			*	\retval GReturn::FAILURE	No internal logic has been assigned to this GLogic.
			*	\retval GReturn::SUCCESS	Function is valid and was called.
			*/
			virtual GReturn Invoke() const = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GEVENTRECEIVER_H
