#ifndef GTHREADSHARED_H
#define GTHREADSHARED_H

/*!
	File: GThreadShared.h
	Purpose: Identify a Gateware interface which supports(if possible) asynchronous resource access across multiple threads.
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Lari H. Norri
	Contributors: Ryan Powser
	Interface Status: Release
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "GInterface.h"

namespace GW
{
	namespace I
	{
		// An interface which supports safe resource access from multiple threads.
		// Gateware Developers: Use private inheritance(containment) if you do not wish to allow external locks. (recommended)
		class GThreadSharedInterface : virtual public GInterfaceInterface
		{
		public:
			virtual GReturn LockAsyncRead() const = 0;
			virtual GReturn UnlockAsyncRead() const = 0;
			virtual GReturn LockSyncWrite() = 0;
			virtual GReturn UnlockSyncWrite() = 0;
		};
	}
}

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Core/GThreadShared/GThreadShared.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace CORE
	{
		//! A Proxy class that can be used to lock & unlock an asynchronous resource.
		/*!
		*	Proxy to a base class that can efficiently lock and unlock resources for Gateware developers.
		*	Most end users should note that most Gateware thread safe interfaces will privately inherit this interface.
		*	This means that casting these Proxys to this base Proxy will fail. This is intended behavior by the designer.
		*/
		class GThreadShared final
			: public I::GProxy<I::GThreadSharedInterface, I::GThreadSharedImplementation>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GThreadShared)
			GATEWARE_CONST_FUNCTION(LockAsyncRead)
			GATEWARE_CONST_FUNCTION(UnlockAsyncRead)
			GATEWARE_FUNCTION(LockSyncWrite)
			GATEWARE_FUNCTION(UnlockSyncWrite)
			//! \endcond

// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a valid GThreadShared Object. 
			/*!
			*	GThreadShared can be thought of as an std::mutex that is potentially optimized per-platform.
			*	While mainly used internally by Gateware you can also make use of this to efficiently protect resources.
			*	End users should be aware that due to internal performance considerations error checking is only provided
			*	when compiling in DEBUG modes. Non-SUCCESS Results from this interface should be used to temporarily find
			*	logical threading errors and should not be used as final logic conditions in RELEASE code.(ex:DEADLOCK)
			*	We(and suggest you) handle things like deadlock prevention at a higher level to not degrade threading perf.
			*
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory)
			*/
			GReturn Create();

			//! Locks a resource for Reading. (prevents writes from other threads)
			/*!
			*	Prevents other threads from writing data to a resource while you access it. (you should NOT modify it)
			*	Use the UnlockAsyncRead function as soon as you are done to prevent processing delays.
			*	If a resource is currently locked for writing, this operation will block until it is free.
			*	**In Release builds this function is not error checked and will always return SUCCESS**
			*
			* 	\retval GReturn::DEADLOCK  A recursive lock was detected by this thread which already had a read/write lock.
			*	\retval GReturn::SUCCESS   Successfully locked resource for asynchronous reads. (if supported by the platform)
			*/
			virtual GReturn LockAsyncRead() const = 0; // operation will wait for writes but not block other reads 

			//! Unlocks a resource that is currently locked for reading. (potentially allows writes from other threads)
			/*!
			*	Releases a resource once you have finished accessing it. (Again... do NOT modify the resource in question)
			*	Use the LockAsyncRead function before calling this function.
			*	If a resource is currently locked for writing, this operation will block until it is free.
			*   **In Release builds this function is not error checked and will always return SUCCESS**
			*
			*	\retval GReturn::FAILURE  The resource was not locked by this thread for reading.
			*	\retval GReturn::SUCCESS  Successfully unlocked resource.
			*/
			virtual GReturn UnlockAsyncRead() const = 0; // operation will no longer block write access

			//! Locks a resource for writing.
			/*!
			*	Prevents other threads from reading and/or writing data from/to a resource.
			*	Use the UnlockSyncWrite function as soon as you are done to prevent processing delays.
			*	**In Release builds this function is not error checked and will always return SUCCESS**
			*
			*	\retval GReturn::DEADLOCK A recursive lock was detected by a thread which already had a read/write lock.
			*	\retval GReturn::SUCCESS  Successfully locked resource for writing.
			*/
			virtual GReturn LockSyncWrite() = 0; // operation will wait for all reads to complete and then block further reads

			//! Unlocks a resource once you have finished writing to it. (re-enable reads)
			/*!
			*	Allows other threads to read and/or write data from/to a resource after editing.
			*	Use the LockSyncWrite function before calling this function.
			*	**In Release builds this function is not error checked and will always return SUCCESS**
			*
			*	\retval GReturn::FAILURE The resource was not locked by this thread for writing.
			*	\retval GReturn::SUCCESS Successfully unlocked a resource.
			*/
			virtual GReturn UnlockSyncWrite() = 0; // operation will release resource for subsequent writes & async reads 
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GTHREADSHARED_H