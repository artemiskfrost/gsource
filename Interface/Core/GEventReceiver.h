#ifndef GEVENTRECEIVER_H
#define GEVENTRECEIVER_H

/*!
	File: GEventReceiver.h
	Purpose: A Gateware interface that allows anyone to receive events from a GEventGenerator or derived class.
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Lari H. Norri
	Contributors: Ryan Powser
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "GEventGenerator.h"
#include <functional>

namespace GW
{
	namespace I
	{
		class GEventReceiverInterface : public virtual GInterfaceInterface
		{
		public:
			virtual GReturn Append(const GEvent& _inEvent) = 0;
			virtual GReturn Waiting(unsigned int& _outCount) const = 0;
			virtual GReturn Pop(GEvent& _outEvent) = 0;
			virtual GReturn Peek(GEvent& _outEvent) const = 0;
			virtual GReturn Missed(unsigned int& _outCount) const = 0;
			virtual GReturn Clear() = 0;
			virtual GReturn Invoke() const = 0;
			template<class eventType>
			GReturn Find(eventType _check, bool _remove);
			template<class eventType, typename eventData>
			GReturn Find(eventType _check, bool _remove, eventData& _outData);
		};
	}
};

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Core/GEventReceiver/GEventReceiver.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace CORE
	{
		//! A Proxy/Functor class that can be used to listen for Events. 
		/*!
		*	Receives any & all events produced by an interface derived from GEventGenerator.
		*	Accepts an std::function<void()> as the callback function/lambda for flexibility.
		*	Use std::bind to supply your own arguments to your callback, the only restriction is the void return type.
		*	To receive more detailed information, embed this Proxy in your callback and "Pop()" the most recent event.
		*	\code{.cpp}
		*		MyEventReceiver.Create( MyEventGenerator, [](MyEventReceiver&) {
		*		GEvent g; 
		*		GEventGenerator::Events e; // substitute the generator type you care about
		*		GEventGenerator::EVENT_DATA d;
		*		if(+MyEventReceiver.Pop(g))
		*			if(+g.Read(e,d))
		*				if(e == GEventGenerator::Events::NON_EVENT) // substitute actual event
		*					std::cout << d.noData; // substitute actual data
		*		});
		*	\endcode
		*	\deprecated **Replaced by GW::CORE::GEventResponder.**
		*/
		class GEventReceiver final
			: public I::GProxy<I::GEventReceiverInterface, I::GEventReceiverImplementation, GEventGenerator, std::function<void()>>
		{
			// End users please feel free to ignore this struct, it is temporary and only used for internal API wiring.
			struct init_callback { init_callback() {
				internal_gw::event_receiver_callback = internal_gw::event_receiver_logic<GEventReceiver>;
			} }init; // hopefully your compiler will optimize this out
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GEventReceiver)
			GATEWARE_FUNCTION(Append)
			GATEWARE_CONST_FUNCTION(Waiting)
			GATEWARE_FUNCTION(Pop)
			GATEWARE_CONST_FUNCTION(Peek)
			GATEWARE_CONST_FUNCTION(Missed)
			GATEWARE_FUNCTION(Clear)
			GATEWARE_CONST_FUNCTION(Invoke)
			GATEWARE_TEMPLATE_FUNCTION(Find)
			//! \endcond
			
			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Allocates & Initializes a GEventReceiver. 
			/*!
			*	A GEventReceiver is capable of receiving and reacting to events created by a GEventGenerator.
			*	You can use this class whenever there is a Gateware class that spawns events you wish to know about.
			*	
			*	\param	[in] _eventSource	The GEventReceiver you wish to receive messages from.
			*	\param	[in] _callback		The function or lambda you wish to be invoked when an event is received.
			*								(optional can pass nullptr). May be invoked from a different thread.
			*								If you need to receive messages on the current thread consider GEventQueue instead. 	
			*
			*	\retval GReturn::UNEXPECTED_RESULT	Internal API failure, should not happen please report this.
			*	\retval GReturn::INVALID_ARGUMENT	A valid GEventGenerator must be provided.
			*	\retval GReturn::SUCCESS	The receiver was correctly bound and registered to the generator provided.
			*/
			GReturn Create(GEventGenerator& _eventSource, std::function<void()>  _callback);

			//! Appends an event to the receiver, and increases number of waiting events. ( > 1 only in GEventQueue)   
			/*!
			*	Generally end users can ignore this function as it is invoked by the connected GEventGenerator.
			*
			*	\param [in] _inEvent	The GEvent you wish to send to this receiver. (will replace any waiting events)
			*
			*	\retval GReturn::UNEXPECTED_RESULT	Internal API failure, should not happen please report this.
			*	\retval GReturn::SUCCESS	The event was appended to this receiver. (does not cause an Invoke() to occur)
			*/
			virtual GReturn Append(const GEvent& _inEvent) = 0;

			//! Reports how many events have not yet been processed.  
			/*!
			*	Use this to check if any events are waiting for you. (unlikely if you provide a callback)
			*
			*	\param [out] _outCount The number of waiting events. (use Pop() or Clear() to remove them)
			*
			*	\retval GReturn::SUCCESS	Reports the current amount of waiting events.
			*/
			virtual GReturn Waiting(unsigned int& _outCount) const = 0;

			//! Grabs a waiting event if one is available. 
			/*!
			*	If waiting is non-zero this operation will extract an event and reduce the number of waiting events.
			*	After a successful Pop() the number of waiting event will be reduced by 1. (waiting may be > 1 in GEventQueue)
			*
			*	\param [out] _outEvent The most recent event that needs to be processed.
			*
			*	\retval GReturn::FAILURE	There were no waiting events that needed processing.
			*	\retval GReturn::SUCCESS	Successfully pulled a waiting event from the Queue.
			*/
			virtual GReturn Pop(GEvent& _outEvent) = 0;

			//! Examines but does not remove a waiting event if one is available. 
			/*!
			*	Same operation as Pop() but does not remove the event or flag it as processed.
			*
			*	\param [out] _outEvent A copy of the current event waiting to be processed.
			*
			*	\retval GReturn::FAILURE	There were no waiting events that could be inspected.
			*	\retval GReturn::SUCCESS	Successfully copied a waiting event from the Queue.
			*/
			virtual GReturn Peek(GEvent& _outEvent) const = 0;

			//! Reports how many events were not processed before they were replaced.
			/*!
			*	If an event was not processed via Pop(), Clear(), or Find() and ends up being replaced
			*	it will increase the internal event miss count. Use this operation to see how many events have been lost.
			*	NOTE: if you use Pop() or equivalent during a callback missing events will not be an issue.
			*
			*	\param [out] _outCount Number of events that were not processed.
			*
			*	\retval GReturn::SUCCESS	Reports the current amount of missed events.
			*/
			virtual GReturn Missed(unsigned int& _outCount) const = 0;

			//! Removes any & all waiting events without processing them. (Can only be > 1 in GEventQueue)
			/*!
			*	All pending events are removed. The internal missed event count will not be reset.
			*	NOTE: Though you cannot reset the internal miss count it is possible to track it's change.
			*
			*	\retval GReturn::FAILURE	There were no waiting events, nothing was cleared.
			*	\retval GReturn::SUCCESS	All waiting events were removed successfully.
			*/
			virtual GReturn Clear() = 0;

			//! Invokes the associated callback function/lambda from the current thread.
			/*!
			*	Allows you to call the callback function if one is attached.
			*	Generally you wouldn't want to do this as this handled for you by the Event Generator.
			*
			*	\retval GReturn::FAILURE	No callback is associated with this Receiver.
			*	\retval GReturn::SUCCESS	Function is valid and was called.
			*/
			virtual GReturn Invoke() const = 0;

			//! Finds a specific Event/DataChunk and optionally can remove it and/or return the associated data. 
			/*!
			*	A convenience function that can be used instead of "Pop/Peek" to search waiting events.
			*
			*	\param [in] _check	The specific enum of the event you are looking for, can also be a specific bit of data.
			*	\param [in] _remove	Should the function remove the waiting event if it is found? true == Pop, false == Peek
			*	\param [out] _outData (OPTIONAL) Returns the data block associated with the event if found.
			*
			*	\retval GReturn::FAILURE	No Event or Data was present matching the provided Query.
			*	\retval GReturn::SUCCESS	Operation successfully found and/or removed the event/data in question.
			*/
			template<class eventType, typename eventData>
			GReturn Find(eventType _check, bool _remove, eventData& _outData);
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GEVENTRECEIVER_H