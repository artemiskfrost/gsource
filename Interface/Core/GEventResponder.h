#ifndef GEVENTRESPONDER_H
#define GEVENTRESPONDER_H

/*!
	File: GEventResponder.h
	Purpose: An interface that will immediately respond to GEvents posted by a GEventGenerator.  
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Lari H. Norri
	Contributors: N/A
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "GLogic.h"

namespace GW
{
	namespace I
	{
		class GEventResponderInterface : public virtual GLogicInterface
		{
		public:
			// Let the compiler know that we intend to use the base class methods
			// and also override them with an additional parameters.
			using GLogicInterface::Assign; // enables "event-less" response if desired
			using GLogicInterface::Invoke; // A GResponder Invoke with no GEvent will have a dummy event provided.
			// new methods (overrides)
			virtual GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) = 0;
			virtual GReturn Invoke(const GEvent& _incomingEvent) const = 0;
		};
	}
};

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Core/GEventResponder/GEventResponder.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace CORE
	{
		//! A Proxy/Functor class that can be used to immediately respond to a GEventGenerator's events. 
		/*!
		*	The GEventResponder proxy stores and allows safe swapping of GEvent handlers.
		*	It will immediately react to any GEventGenerator it is Registered to that posts a GEvent.
		*	Essentially this is a specialized version of GLogic designed for convenient interception of events.
		*	**NOTE: GEventResponder is a replacement interface to GEventReceiver which is deprecated.**
		*	\code{.cpp}
		*		GEventResponder observer;
		*		// Provide the logic for processing a GEvent you care about.
		*		observer.Create([](const GEvent& g) { 
		*			GEventGenerator::Events e; // substitute the generator type you care about
		*			GEventGenerator::EVENT_DATA d; // substitute the event data you care about
		*			// Determine if the incoming event contains the data you are interested in.
		*			if (+g.Read(e,d)) // if you are interested in multiple events consider using a switch below
		*				if (e == GEventGenerator::Events::NON_EVENT) // substitute actual event
		*					std::cout << d.noData; // substitute actual data
		*		});
		*		someEventGenerator.Register(observer);
		*	\endcode
		*/
		class GEventResponder final
			: public I::GProxy<I::GEventResponderInterface, I::GEventResponderImplementation, std::function<void(const GEvent&)>>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GEventResponder)
			GATEWARE_FUNCTION(Assign)
			GATEWARE_CONST_FUNCTION(Invoke)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Allocates & Initializes a GEventResponder. 
			/*!
			*	A GEventResponder is capable of receiving and reacting to events created by GEventGenerators.
			*	You can use this class whenever there is a Gateware class that spawns events you wish to know about.
			*	A GEventResponder may be Registered to multiple GEventGenerators. (see: GEventGenerator::Register())
			*
			*	\param	[in] _eventHandler	The function or lambda you wish to be invoked when an event is received.
			*								(optional can pass nullptr). May be invoked from different threads.
			*								If you need to receive messages on a specific thread consider GEventCache instead.
			*
			*	\retval GReturn::SUCCESS	The responder was initialized with the handler provided.
			*/
			GReturn Create(std::function<void(const GEvent&)> _eventHandler);

			//! Overwrites the current event handler assigned to this Proxy (if any)
			/*!
			*	Assigns new user provided logic (nullptr[no logic] is a valid argument).
			*	Cannot be done within provided logic as the class guarantees the logic
			*	stack will remain valid for the duration of Invoke().
			*	**Deadlocks will only be detected/prevented in Debug builds, check your logic for errors**
			*
			*	\param	[in] _newEventHandler	The function or lambda you wish to receive all new events with.
			*
			*	\retval GReturn::DEADLOCK	You have attempted to modify the logic during it's own execution.
			*	\retval GReturn::SUCCESS	Stored logic was successfully overwritten.
			*/
			virtual GReturn Assign(std::function<void(const GEvent&)> _newEventHandler) = 0;

			//! Invokes the stored function/lambda event handler from the current thread.
			/*!
			*	Calls the stored handler on this thread, during the invocation the logic and proxy will remain valid.
			*	During the operation Recursive calls are permitted but swapping the handler of your own proxy is not.
			*	In general you will not call this routine directly, rather the provided GEventGenerator will call this.
			*
			*	\param	[in] _incomingEvent		The GEvent you want the GResponder to respond to.
			*
			*	\retval GReturn::FAILURE	No internal logic has been assigned to this GEventResponder.
			*	\retval GReturn::SUCCESS	Handler is valid and was called.
			*/
			virtual GReturn Invoke(const GEvent& _incomingEvent) const = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GEVENTRESPONDER_H