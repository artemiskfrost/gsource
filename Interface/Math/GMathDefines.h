#ifndef GMATHDEFINES_H
#define GMATHDEFINES_H

/*!
	File: GMathDefines.h
	Purpose: A Gateware math define header that handles math structs, such as vector, quaternion and matrix.
	Author: Shuo-Yi Chang
	Contributors: Ryan Bickell, Ryan Powser, Colby Peck, Lari Norri
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#define G_PI					3.14159265358979323846 //G_PI is deprecated! Use G_PI_D or G_PI_F instead. 
#define G_PI_D					3.14159265358979323846
#define G_PI_F					3.141592741f

#define G_PI_R					0.318309886183790671538 //G_PI_R is deprecated! Use G_PI_R_D or G_PI_R_F instead. 
#define G_PI_R_D				0.318309886183790671538
#define G_PI_R_F				0.3183098733f

#define G_TAU					6.283185307179586476925286766559 //G_TAU is deprecated! Use G_TAU_D or G_TAU_F instead. 
#define G_TAU_D					6.283185307179586476925286766559
#define G_TAU_F					6.283185482f

#define G_TAU_R					0.636619772367581343076 //G_TAU_R is deprecated! Use G_TAU_R_D or G_TAU_R_F instead. 
#define G_TAU_R_D				0.636619772367581343076
#define G_TAU_R_F				0.6366197467f 


#define G_ABS(num)				( ( (num) > 0 ) ? (num) : (-(num)) )				//RETURN THE ABSOLUTE VALUE OF THE INPUT NUMBER
#define G_LARGER(A, B)			( ( (A) > (B) ) ? (A) : (B) )						//RETURN THE LARGER INPUT NUMBER
#define G_SMALLER(A, B)			( ( (A) < (B) ) ? (A) : (B) )						//RETURN THE SMALLER INPUT NUMBER
#define G_ABS_LARGER(A, B)		( ( G_ABS(A) > G_ABS(B) ) ?  G_ABS(A) : G_ABS(B) )	//RETURN THE LARGER ABSOLUTE VALUE OF THE TWO INPUT NUMBERS


#define G_EPSILON_F				1.192092896e-07F
#define G_EPSILON_D				2.2204460492503131e-016

#define G_DEVIATION_EXACT		0
#define G_DEVIATION_PRECISE_F	G_EPSILON_F
#define G_DEVIATION_STANDARD_F	(G_EPSILON_F * 10)
#define G_DEVIATION_LOOSE_F		(G_EPSILON_F * 100)

#define G_DEVIATION_PRECISE_D	G_EPSILON_D
#define G_DEVIATION_STANDARD_D	(G_EPSILON_D * 10)
#define G_DEVIATION_LOOSE_D		(G_EPSILON_D * 100)

#define G_ABSOLUTE_COMPARISON(num1, num2, margin) (G_ABS((num1) - (num2)) <= (margin)) //returns true if the absolute difference between the two values is <= the given margin 
#define G_RELATIVE_COMPARISON(num1, num2, margin) (G_ABS((num1) - (num2)) <= margin * (G_ABS_LARGER (num1, num2))) //returns true if the absolute difference between the two given values is <= (the given margin * the larger given value)
//Relative comparison is used to adjust for the fact that floating point imprecison gets bigger as the values get bigger (the further away from 0, the larger the floating-point imprecision). 
//That is: with relative comparison, as the smallest possible error grows the margin for error also grows. 
#define G_HYBRID_COMPARISON(num1, num2, absoluteMargin, relativeMargin) (G_ABSOLUTE_COMPARISON(num1, num2, absoluteMargin) || G_RELATIVE_COMPARISON(num1, num2, relativeMargin))
//This hybrid comparison is used to adjust for the fact that relative comparison breaks down at values very near to zero. First, it does an absolute check. Then, if the absolute check fails, it does a relative check. 

//These macros allow for <= and >= floating point comparison
#define G_HYBRID_COMPARISON_LESSER_OR_EQUAL(num1, num2, absoluteMargin, relativeMargin) (num1 <= num2 || G_ABSOLUTE_COMPARISON(num1, num2, absoluteMargin) || G_RELATIVE_COMPARISON(num1, num2, relativeMargin))
#define G_HYBRID_COMPARISON_GREATER_OR_EQUAL(num1, num2, absoluteMargin, relativeMargin) (num1 >= num2 || G_ABSOLUTE_COMPARISON(num1, num2, absoluteMargin) || G_RELATIVE_COMPARISON(num1, num2, relativeMargin))

//These helper macros are more for readability than for shorthand 
#define G_WITHIN_PRECISE_DEVIATION_F(num1, num2)	(G_HYBRID_COMPARISON(num1, num2, G_DEVIATION_PRECISE_F, G_DEVIATION_PRECISE_F)) //Helper macro for precise float precision checking 
#define G_WITHIN_STANDARD_DEVIATION_F(num1, num2)	(G_HYBRID_COMPARISON(num1, num2, G_DEVIATION_STANDARD_F, G_DEVIATION_STANDARD_F)) //Helper macro for standard float precision checking 
#define G_WITHIN_LOOSE_DEVIATION_F(num1, num2)		(G_HYBRID_COMPARISON(num1, num2, G_DEVIATION_LOOSE_F, G_DEVIATION_LOOSE_F)) //Helper macro for loose float precision checking 

#define G_WITHIN_PRECISE_DEVIATION_D(num1, num2)	(G_HYBRID_COMPARISON(num1, num2, G_DEVIATION_PRECISE_D, G_DEVIATION_PRECISE_D)) //Helper macro for precise double precision checking 
#define G_WITHIN_STANDARD_DEVIATION_D(num1, num2)	(G_HYBRID_COMPARISON(num1, num2, G_DEVIATION_STANDARD_D, G_DEVIATION_STANDARD_D)) //Helper macro for standard double precision checking 
#define G_WITHIN_LOOSE_DEVIATION_D(num1, num2)		(G_HYBRID_COMPARISON(num1, num2, G_DEVIATION_LOOSE_D, G_DEVIATION_LOOSE_D))  //Helper macro for loose double precision checking 

#define G_LESSER_OR_EQUAL_PRECISE_F(num1, num2)		(G_HYBRID_COMPARISON_LESSER_OR_EQUAL(num1, num2, G_DEVIATION_PRECISE_F, G_DEVIATION_PRECISE_F))		//Helper macro for float comparison. Equivalent to '<=' with a precise margin.
#define G_LESSER_OR_EQUAL_STANDARD_F(num1, num2)	(G_HYBRID_COMPARISON_LESSER_OR_EQUAL(num1, num2, G_DEVIATION_STANDARD_F, G_DEVIATION_STANDARD_F))	//Helper macro for float comparison. Equivalent to '<=' with a standard margin.
#define G_LESSER_OR_EQUAL_LOOSE_F(num1, num2)		(G_HYBRID_COMPARISON_LESSER_OR_EQUAL(num1, num2, G_DEVIATION_LOOSE_F, G_DEVIATION_LOOSE_F))			//Helper macro for float comparison. Equivalent to '<=' with a loose margin.

#define G_GREATER_OR_EQUAL_PRECISE_F(num1, num2)	(G_HYBRID_COMPARISON_GREATER_OR_EQUAL(num1, num2, G_DEVIATION_PRECISE_F, G_DEVIATION_PRECISE_F))	//Helper macro for float comparison. Equivalent to '>=' with a precise margin.
#define G_GREATER_OR_EQUAL_STANDARD_F(num1, num2)	(G_HYBRID_COMPARISON_GREATER_OR_EQUAL(num1, num2, G_DEVIATION_STANDARD_F, G_DEVIATION_STANDARD_F))	//Helper macro for float comparison. Equivalent to '>=' with a standard margin.
#define G_GREATER_OR_EQUAL_LOOSE_F(num1, num2)		(G_HYBRID_COMPARISON_GREATER_OR_EQUAL(num1, num2, G_DEVIATION_LOOSE_F, G_DEVIATION_LOOSE_F))		//Helper macro for float comparison. Equivalent to '>=' with a loose margin.

#define G_LESSER_OR_EQUAL_PRECISE_D(num1, num2)		(G_HYBRID_COMPARISON_LESSER_OR_EQUAL(num1, num2, G_DEVIATION_PRECISE_D, G_DEVIATION_PRECISE_D))		//Helper macro for double comparison. Equivalent to '<=' with a precise margin.
#define G_LESSER_OR_EQUAL_STANDARD_D(num1, num2)	(G_HYBRID_COMPARISON_LESSER_OR_EQUAL(num1, num2, G_DEVIATION_STANDARD_D, G_DEVIATION_STANDARD_D))	//Helper macro for double comparison. Equivalent to '<=' with a standard margin.
#define G_LESSER_OR_EQUAL_LOOSE_D(num1, num2)		(G_HYBRID_COMPARISON_LESSER_OR_EQUAL(num1, num2, G_DEVIATION_LOOSE_D, G_DEVIATION_LOOSE_D))			//Helper macro for double comparison. Equivalent to '<=' with a loose margin.

#define G_GREATER_OR_EQUAL_PRECISE_D(num1, num2)	(G_HYBRID_COMPARISON_GREATER_OR_EQUAL(num1, num2, G_DEVIATION_PRECISE_D, G_DEVIATION_PRECISE_D))	//Helper macro for double comparison. Equivalent to '>=' with a precise margin.
#define G_GREATER_OR_EQUAL_STANDARD_D(num1, num2)	(G_HYBRID_COMPARISON_GREATER_OR_EQUAL(num1, num2, G_DEVIATION_STANDARD_D, G_DEVIATION_STANDARD_D))	//Helper macro for double comparison. Equivalent to '>=' with a standard margin.
#define G_GREATER_OR_EQUAL_LOOSE_D(num1, num2)		(G_HYBRID_COMPARISON_GREATER_OR_EQUAL(num1, num2, G_DEVIATION_LOOSE_D, G_DEVIATION_LOOSE_D))		//Helper macro for double comparison. Equivalent to '>=' with a loose margin.


#define G_FIRST_COMPARISON_F(num1 , num2)		( G_ABS( (num1) - (num2) ) <= ( G_EPSILON_F ) ) //G_FIRST_COMPARISON_F is deprecated! Use G_ABSOLUTE_COMPARISON instead. 
#define G_SECOND_COMPARISON_F(num1 , num2)		( G_ABS( (num1) - (num2) ) <= ( G_DEVIATION_STANDARD_F * G_ABS_LARGER(num1, num2)) ) //G_SECOND_COMPARISON_F is deprecated! Use G_RELATIVE_COMPARISON instead.

#define G_COMPARISON_STANDARD_F(num1 , num2)	( G_FIRST_COMPARISON_F( (num1), (num2) ) ? true : G_SECOND_COMPARISON_F( (num1) , (num2) ) ) //G_COMPARISON_STANDARD_F is deprecated! Use G_WITHIN_STANDARD_DEVIATION_F instead.
#define G_COMPARISON_F(num1, num2, deviation)	( G_FIRST_COMPARISON_F( (num1), (num2) ) ? true : ( G_ABS( (num1) - (num2) ) <= ( deviation * G_ABS_LARGER(num1, num2)) ) ) //G_COMPARISON_F is deprecated! Use G_HYBRID_COMPARISON instead. 

#define G_FIRST_COMPARISON_D(num1 , num2)		( G_ABS( (num1) - (num2) ) <= ( G_EPSILON_D ) )	//G_FIRST_COMPARISON_D is deprecated! Use G_ABSOLUTE_COMPARISON instead.
#define G_SECOND_COMPARISON_D(num1 , num2)		( G_ABS( (num1) - (num2) ) <= ( G_DEVIATION_STANDARD_D * G_ABS_LARGER(num1, num2)) ) //G_SECOND_COMPARISON_D is deprecated! Use G_RELATIVE_COMPARISON instead.
#define G_COMPARISON_STANDARD_D(num1 , num2)	( G_FIRST_COMPARISON_D( (num1), (num2) ) ? true : G_SECOND_COMPARISON_D( (num1) , (num2) ) ) //G_COMPARISON_STANDARD_D is deprecated! Use G_WITHIN_STANDARD_DEVIATION_D instead. 
#define G_COMPARISON_D(num1, num2, deviation)	( G_FIRST_COMPARISON_D( (num1), (num2) ) ? true : ( G_ABS( (num1) - (num2) ) <= ( deviation * (G_ABS_LARGER(num1, num2))) ) ) //G_COMPARISON_D is deprecated! Use G_HYBRID_COMPARISON instead.


#define G_LERP(start, end, ratio)			( (start) + (ratio) * ((end) - (start)) )				//LINEARLY INTERPOLATE BETWEEN TWO VALUES BY A RATIO
#define G_LERP_PRECISE(start, end, ratio)	( ((1.0 - (ratio)) * (start)) + ((ratio) * (end)) )		//LINEARLY INTERPOLATE BETWEEN TWO VALUES BY A RATIO, AVOIDING SOME RARE EDGE CASES

#define G_CLAMP(num, min, max)		( ((num) > (max)) ? (max) : ( ((num) < (min)) ? (min) : (num) ) )	//CLAMP THE NUMBER BETWEEN THE TOP NUMBER AND THE BOTTOM NUMBER
#define G_CLAMP_MIN(num, min)		( ((num) < (min)) ? (min) : (num) )									//CLAMP THE NUMBER BETWEEN ITSELF AND THE BOTTOM NUMBER
#define G_CLAMP_MAX(num, max)		( ((num) > (max)) ? (max) : (num) )									//CLAMP THE NUMBER BETWEEN ITSELF AND THE TOP NUMBER

#define G_DEGREE_TO_RADIAN(degree)		( (degree) * 0.01745329251994329576923690768489 ) //G_DEGREE_TO_RADIAN is deprecated! Use G_DEGREE_TO_RADIAN_D or G_DEGREE_TO_RADIAN_F instead. 
#define G_DEGREE_TO_RADIAN_D(degree)	( (degree) * 0.01745329251994329576923690768489 )
#define G_DEGREE_TO_RADIAN_F(degree)	( (degree) * 0.01745329238f )

#define G_RADIAN_TO_DEGREE(radian)	 ((radian) * 57.295779513082320876798154814105 ) //G_RADIAN_TO_DEGREE is deprecated! Use G_RADIAN_TO_DEGREE_D or G_RADIAN_TO_DEGREE_F instead. 
#define G_RADIAN_TO_DEGREE_D(radian) ((radian) * 57.295779513082320876798154814105)
#define G_RADIAN_TO_DEGREE_F(radian) ((radian) * 57.29578018f)


//Component-wise standard floating point comparison
#define COMPARE_VECTORS_STANDARD_F(_a, _b){\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.x, _b.x));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.y, _b.y));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.z, _b.z));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.w, _b.w));\
}

//Component-wise standard floating point comparison
#define COMPARE_VECTORS_STANDARD_D(_a, _b){\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.x, _b.x));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.y, _b.y));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.z, _b.z));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.w, _b.w));\
}

//Component-wise standard floating point comparison
#define COMPARE_MATRICES_STANDARD_F(_a, _b) {\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[0], _b.data[0]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[1], _b.data[1]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[2], _b.data[2]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[3], _b.data[3]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[4], _b.data[4]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[5], _b.data[5]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[6], _b.data[6]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[7], _b.data[7]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[8], _b.data[8]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[9], _b.data[9]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[10], _b.data[10]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[11], _b.data[11]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[12], _b.data[12]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[13], _b.data[13]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[14], _b.data[14]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_F(_a.data[15], _b.data[15]));\
}

//Component-wise standard floating point comparison
#define COMPARE_MATRICES_STANDARD_D(_a, _b) {\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[0], _b.data[0]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[1], _b.data[1]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[2], _b.data[2]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[3], _b.data[3]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[4], _b.data[4]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[5], _b.data[5]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[6], _b.data[6]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[7], _b.data[7]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[8], _b.data[8]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[9], _b.data[9]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[10], _b.data[10]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[11], _b.data[11]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[12], _b.data[12]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[13], _b.data[13]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[14], _b.data[14]));\
CHECK(G_WITHIN_STANDARD_DEVIATION_D(_a.data[15], _b.data[15]));\
}

//Used by collision library to approximate "close enough" detections (may need adjusting)
#define G_COLLISION_THRESHOLD_F 0.000001f
//This seems rather large tbh, but its what the lib was using at the time of ARM port
#define G_COLLISION_THRESHOLD_D 1.192092896e-07

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all math library interface must belong.
	namespace MATH
	{
		/*! \addtogroup MathStructures
		*  @{
		*/

		// Ensure identical binary padding for structures on all platforms.
#pragma pack(push, 1)

		/*! Vector with 4 float elements */
		struct GVECTORF
		{
			union
			{
				struct
				{
					float x;
					float y;
					float z;
					float w;
				};
				float data[4];
			};

			inline const GVECTORF xy() const
			{
				return GVECTORF{{{this->x, this->y, 0.0f, 0.0f}}};
			}

			inline const GVECTORF xyz() const
			{
				return GVECTORF{{{this->x, this->y, this->z, 0.0f}}};
			}

			inline const GVECTORF xyzw() const
			{
				return *this;
			}
		};

		/*! Vector with 4 double elements */
		struct GVECTORD
		{
			union
			{
				struct
				{
					double x;
					double y;
					double z;
					double w;
				};
				double data[4];
			};

			inline const GVECTORD xy() const
			{
				return GVECTORD{{{this->x, this->y, 0.0, 0.0}}};
			}

			inline const GVECTORD xyz() const
			{
				return GVECTORD{{{this->x, this->y, this->z, 0.0}}};
			}

			inline const GVECTORD xyzw() const
			{
				return *this;
			}
		};
		/*! Matrix with 4 float vectors which represent for each row. */
		struct GMATRIXF
		{
			union
			{
				struct
				{
					GVECTORF row1;
					GVECTORF row2;
					GVECTORF row3;
					GVECTORF row4;
				};

				float data[16];
			};
		};

		/*! Matrix with 4 double vectors which represent for each row. */
		struct GMATRIXD
		{
			union
			{
				struct
				{
					GVECTORD row1;
					GVECTORD row2;
					GVECTORD row3;
					GVECTORD row4;
				};
				double data[16];
			};
		};

		/*! Quaternion with 4 float elements */
		struct GQUATERNIONF
		{
			union
			{
				struct
				{
					float x;
					float y;
					float z;
					float w;
				};
				float data[4];
			};
		};

		/*! Quaternion with 4 double elements */
		struct GQUATERNIOND
		{
			union
			{
				struct
				{
					double x;
					double y;
					double z;
					double w;
				};
				double data[4];
			};
		};

		/*! Line with 2 GVECTORFs representing start & end. */
		struct GLINEF
		{
			union
			{
				struct
				{
					GVECTORF start;
					GVECTORF end;
				};
				GVECTORF data[2];
			};

			GVECTORF* operator&()
			{
				return data;
			}

			const GVECTORF* operator&() const
			{
				return data;
			}
		};

		/*! Line with 2 GVECTORDs representing start & end. */
		struct GLINED
		{
			union
			{
				struct
				{
					GVECTORD start;
					GVECTORD end;
				};
				GVECTORD data[2];
			};

			GVECTORD* operator&()
			{
				return data;
			}

			const GVECTORD* operator&() const
			{
				return data;
			}
		};

		/*! Ray with 2 GVECTORFs representing position & direction. */
		struct GRAYF
		{
			union
			{
				struct
				{
					GVECTORF position;
					GVECTORF direction;
				};
				GVECTORF data[2];
			};

			GVECTORF* operator&()
			{
				return data;
			}

			const GVECTORF* operator&() const
			{
				return data;
			}
		};

		/*! Ray with 2 GVECTORDs representing position & direction. */
		struct GRAYD
		{
			union
			{
				struct
				{
					GVECTORD position;
					GVECTORD direction;
				};
				GVECTORD data[2];
			};

			GVECTORD* operator&()
			{
				return data;
			}

			const GVECTORD* operator&() const
			{
				return data;
			}
		};

		/*! Triangle with 3 GVECTORFs representing positions. */
		struct GTRIANGLEF
		{
			union
			{
				struct
				{
					GVECTORF a;
					GVECTORF b;
					GVECTORF c;
				};
				GVECTORF data[3];
			};

			GVECTORF* operator&()
			{
				return data;
			}

			const GVECTORF* operator&() const
			{
				return data;
			}
		};

		/*! Triangle with 3 GVECTORDs representing positions. */
		struct GTRIANGLED
		{
			union
			{
				struct
				{
					GVECTORD a;
					GVECTORD b;
					GVECTORD c;
				};
				GVECTORD data[3];
			};

			GVECTORD* operator&()
			{
				return data;
			}

			const GVECTORD* operator&() const
			{
				return data;
			}
		};

		/*! Plane with 4 floats: 3 representing a normal and 1 a distance along that normal from the origin. */
		struct GPLANEF
		{
			union
			{
				struct
				{
					float x;
					float y;
					float z;
					float distance;
				};
				GVECTORF data;
			};

			GVECTORF* operator&()
			{
				return &data;
			}

			const GVECTORF* operator&() const
			{
				return &data;
			}
		};

		/*! Plane with 4 doubles: 3 representing a normal and 1 a distance along that normal from the origin. */
		struct GPLANED
		{
			union
			{
				struct
				{
					double x;
					double y;
					double z;
					double distance;
				};
				GVECTORD data;
			};

			GVECTORD* operator&()
			{
				return &data;
			}

			const GVECTORD* operator&() const
			{
				return &data;
			}
		};

		/*! Sphere with 4 floats: 3 representing position and 1 radius. */
		struct GSPHEREF
		{
			union
			{
				struct
				{
					float x;
					float y;
					float z;
					float radius;
				};
				GVECTORF data;
			};

			GVECTORF* operator&()
			{
				return &data;
			}

			const GVECTORF* operator&() const
			{
				return &data;
			}
		};

		/*! Sphere with 4 doubles: 3 representing position and 1 radius. */
		struct GSPHERED
		{
			union
			{
				struct
				{
					double x;
					double y;
					double z;
					double radius;
				};
				GVECTORD data;
			};

			GVECTORD* operator&()
			{
				return &data;
			}

			const GVECTORD* operator&() const
			{
				return &data;
			}
		};

		/*! Capsule with 7 floats: 3 representing a start position, 3 representing an end position, 1 representing a radius. */
		struct GCAPSULEF
		{
			union
			{
				struct
				{
					float start_x;
					float start_y;
					float start_z;
					float radius;
					float end_x;
					float end_y;
					float end_z;
					float reserved;
				};
				GVECTORF data[2];
			};

			GVECTORF* operator&()
			{
				return data;
			}

			const GVECTORF* operator&() const
			{
				return data;
			}
		};

		/*! Capsule with 7 double: 3 representing a start position, 3 representing an end position, and 1 representing a radius. */
		struct GCAPSULED
		{
			union
			{
				struct
				{
					double start_x;
					double start_y;
					double start_z;
					double radius;
					double end_x;
					double end_y;
					double end_z;
					double reserved;
				};
				GVECTORD data[2];
			};

			GVECTORD* operator&()
			{
				return data;
			}

			const GVECTORD* operator&() const
			{
				return data;
			}
		};

		/*! AABB with 2 GVECTORFs representing center and extent. */
		struct GAABBCEF
		{
			union
			{
				struct
				{
					GVECTORF center;
					GVECTORF extent;
				};
				GVECTORF data[2];
			};

			GVECTORF* operator&()
			{
				return data;
			}

			const GVECTORF* operator&() const
			{
				return data;
			}
		};

		/*! AABB with 2 GVECTORDs representing center and extent. */
		struct GAABBCED
		{
			union
			{
				struct
				{
					GVECTORD center;
					GVECTORD extent;
				};
				GVECTORD data[2];
			};

			GVECTORD* operator&()
			{
				return data;
			}

			const GVECTORD* operator&() const
			{
				return data;
			}
		};

		/*! AABB with 2 GVECTORFs representing min and max. */
		struct GAABBMMF
		{
			union
			{
				struct
				{
					GVECTORF min;
					GVECTORF max;
				};
				GVECTORF data[2];
			};

			GVECTORF* operator&()
			{
				return data;
			}

			const GVECTORF* operator&() const
			{
				return data;
			}
		};

		/*! AABB with 2 GVECTORDs representing min and max. */
		struct GAABBMMD
		{
			union
			{
				struct
				{
					GVECTORD min;
					GVECTORD max;
				};
				GVECTORD data[2];
			};

			GVECTORD* operator&()
			{
				return data;
			}

			const GVECTORD* operator&() const
			{
				return data;
			}
		};

		/*! OBB with 2 GVECTORFs representing center and extent and 1 quaternion representing rotation. */
		struct GOBBF
		{
			union
			{
				struct
				{
					GVECTORF center;
					GVECTORF extent;
					GQUATERNIONF rotation;
				};
				GVECTORF data[3];
			};

			GVECTORF* operator&()
			{
				return data;
			}

			const GVECTORF* operator&() const
			{
				return data;
			}
		};

		/*! OBB with 2 GVECTORDs representing center and extent and 1 quaternion representing rotation.  */
		struct GOBBD
		{
			union
			{
				struct
				{
					GVECTORD center;
					GVECTORD extent;
					GQUATERNIOND rotation;
				};
				GVECTORD data[3];
			};

			GVECTORD* operator&()
			{
				return data;
			}

			const GVECTORD* operator&() const
			{
				return data;
			}
		};
#pragma pack(pop)

		static const GVECTORF GIdentityVectorF{{{0,0,0,1}}};
		static const GVECTORD GIdentityVectorD{{{0,0,0,1}}};
		static const GVECTORF GZeroVectorF{{{0,0,0,0}}};
		static const GVECTORD GZeroVectorD{{{0,0,0,0}}};

		static const GMATRIXF GIdentityMatrixF{{{{{{1,0,0,0}}},{{{0,1,0,0}}},{{{0,0,1,0}}},{{{0,0,0,1}}}}}};
		static const GMATRIXD GIdentityMatrixD{{{{{{1,0,0,0}}},{{{0,1,0,0}}},{{{0,0,1,0}}},{{{0,0,0,1}}}}}};
		static const GMATRIXF GZeroMatrixF{{{{{{0,0,0,0}}},{{{0,0,0,0}}},{{{0,0,0,0}}},{{{0,0,0,0}}}}}};
		static const GMATRIXD GZeroMatrixD{{{{{{0,0,0,0}}},{{{0,0,0,0}}},{{{0,0,0,0}}},{{{0,0,0,0}}}}}};

		static const GQUATERNIONF GIdentityQuaternionF{{{0,0,0,1}}};
		static const GQUATERNIOND GIdentityQuaternionD{{{0,0,0,1}}};
		static const GQUATERNIONF GZeroQuaternionF{{{0,0,0,0}}};
		static const GQUATERNIOND GZeroQuaternionD{{{0,0,0,0}}};
		/*! @} */
	}
}
#endif // GMATHDEFINES_H
