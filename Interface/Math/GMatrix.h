#ifndef GMATRIX_H
#define GMATRIX_H

/*!
	File: GMatrix.h
	Purpose: A Gateware interface that handles matrix functions.
	Asynchronous: NO
	Author: Shuo-Yi Chang
	Contributors: Caio Tavares, Ryan Powser
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"
#include "GMathDefines.h"

namespace GW
{
	namespace I
	{
		class GMatrixInterface : public virtual GInterfaceInterface
		{
		public:
			// Floats
			static GReturn AddMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SubtractMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn VectorXMatrixF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ConvertQuaternionF(GW::MATH::GQUATERNIONF _quaternion, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyNumF(GW::MATH::GMATRIXF _matrix, float _scalar, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn DeterminantF(GW::MATH::GMATRIXF _matrix, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TransposeF(GW::MATH::GMATRIXF _matrix, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn InverseF(GW::MATH::GMATRIXF _matrix, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IdentityF(GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn GetRotationF(GW::MATH::GMATRIXF _matrix, GW::MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetTranslationF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetScaleF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn RotateXGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateXLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateYGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateYLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateZGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateZLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotationYawPitchRollF(float _yaw, float _pitch, float _roll, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotationByVectorF(GW::MATH::GVECTORF _vector, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn TranslateGlobalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TranslateLocalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn ScaleGlobalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleLocalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn LerpF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, float _ratio, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn ProjectionDirectXLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionDirectXRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionOpenGLLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionOpenGLRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionVulkanLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionVulkanRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn LookAtLHF(GW::MATH::GVECTORF _eye, GW::MATH::GVECTORF _at, GW::MATH::GVECTORF _up, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn LookAtRHF(GW::MATH::GVECTORF _eye, GW::MATH::GVECTORF _at, GW::MATH::GVECTORF _up, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn MakeRelativeF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeSeparateF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn Upgrade(GW::MATH::GMATRIXF _matrixF, GW::MATH::GMATRIXD& _outMatrixD) { return GReturn::NO_IMPLEMENTATION; }


			// Doubles
			static GReturn AddMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SubtractMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn VectorXMatrixD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ConvertQuaternionD(GW::MATH::GQUATERNIOND _quaternion, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyNumD(GW::MATH::GMATRIXD _matrix, double _scalar, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn DeterminantD(GW::MATH::GMATRIXD _matrix, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TransposeD(GW::MATH::GMATRIXD _matrix, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn InverseD(GW::MATH::GMATRIXD _matrix, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IdentityD(GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn GetRotationD(GW::MATH::GMATRIXD _matrix, GW::MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetTranslationD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetScaleD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn RotateXGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateXLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateYGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateYLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateZGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateZLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotationYawPitchRollD(double _yaw, double _pitch, double _roll, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotationByVectorD(GW::MATH::GVECTORD _vector, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn TranslateGlobalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TranslateLocalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn ScaleGlobalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleLocalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn LerpD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, double _ratio, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn ProjectionDirectXLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionDirectXRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionOpenGLLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionOpenGLRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionVulkanLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ProjectionVulkanRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn LookAtLHD(GW::MATH::GVECTORD _eye, GW::MATH::GVECTORD _at, GW::MATH::GVECTORD _up, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn LookAtRHD(GW::MATH::GVECTORD _eye, GW::MATH::GVECTORD _at, GW::MATH::GVECTORD _up, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn MakeRelativeD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeSeparateD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			static GReturn Downgrade(GW::MATH::GMATRIXD _matrixD, GW::MATH::GMATRIXF& _outMatrixF) { return GReturn::NO_IMPLEMENTATION; }
		};
	}// end CORE namespace
}// end GW namespace

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Math/GMatrix/GMatrix.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace MATH
	{
		//! A Gateware interface that handles matrix functions.
		class GMatrix final
			: public I::GProxy<I::GMatrixInterface, I::GMatrixImplementation>
		{
		public:
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GMatrix)

			//Floats
			GATEWARE_STATIC_FUNCTION(AddMatrixF)
			GATEWARE_STATIC_FUNCTION(SubtractMatrixF)
			GATEWARE_STATIC_FUNCTION(MultiplyMatrixF)

			GATEWARE_STATIC_FUNCTION(VectorXMatrixF)
			GATEWARE_STATIC_FUNCTION(ConvertQuaternionF)
			GATEWARE_STATIC_FUNCTION(MultiplyNumF)

			GATEWARE_STATIC_FUNCTION(DeterminantF)
			GATEWARE_STATIC_FUNCTION(TransposeF)
			GATEWARE_STATIC_FUNCTION(InverseF)
			GATEWARE_STATIC_FUNCTION(IdentityF)

			GATEWARE_STATIC_FUNCTION(GetRotationF)
			GATEWARE_STATIC_FUNCTION(GetTranslationF)
			GATEWARE_STATIC_FUNCTION(GetScaleF)

			GATEWARE_STATIC_FUNCTION(RotateXGlobalF)
			GATEWARE_STATIC_FUNCTION(RotateXLocalF)
			GATEWARE_STATIC_FUNCTION(RotateYGlobalF)
			GATEWARE_STATIC_FUNCTION(RotateYLocalF)
			GATEWARE_STATIC_FUNCTION(RotateZGlobalF)
			GATEWARE_STATIC_FUNCTION(RotateZLocalF)
			GATEWARE_STATIC_FUNCTION(RotationYawPitchRollF)
			GATEWARE_STATIC_FUNCTION(RotationByVectorF)

			GATEWARE_STATIC_FUNCTION(TranslateGlobalF)
			GATEWARE_STATIC_FUNCTION(TranslateLocalF)

			GATEWARE_STATIC_FUNCTION(ScaleGlobalF)
			GATEWARE_STATIC_FUNCTION(ScaleLocalF)

			GATEWARE_STATIC_FUNCTION(LerpF)

			GATEWARE_STATIC_FUNCTION(ProjectionDirectXLHF)
			GATEWARE_STATIC_FUNCTION(ProjectionDirectXRHF)
			GATEWARE_STATIC_FUNCTION(ProjectionOpenGLLHF)
			GATEWARE_STATIC_FUNCTION(ProjectionOpenGLRHF)
			GATEWARE_STATIC_FUNCTION(ProjectionVulkanLHF)
			GATEWARE_STATIC_FUNCTION(ProjectionVulkanRHF)

			GATEWARE_STATIC_FUNCTION(LookAtLHF)
			GATEWARE_STATIC_FUNCTION(LookAtRHF)

			GATEWARE_STATIC_FUNCTION(MakeRelativeF)
			GATEWARE_STATIC_FUNCTION(MakeSeparateF)

			GATEWARE_STATIC_FUNCTION(Upgrade)

			//Doubles
			GATEWARE_STATIC_FUNCTION(AddMatrixD)
			GATEWARE_STATIC_FUNCTION(SubtractMatrixD)
			GATEWARE_STATIC_FUNCTION(MultiplyMatrixD)

			GATEWARE_STATIC_FUNCTION(VectorXMatrixD)
			GATEWARE_STATIC_FUNCTION(ConvertQuaternionD)
			GATEWARE_STATIC_FUNCTION(MultiplyNumD)

			GATEWARE_STATIC_FUNCTION(DeterminantD)
			GATEWARE_STATIC_FUNCTION(TransposeD)
			GATEWARE_STATIC_FUNCTION(InverseD)
			GATEWARE_STATIC_FUNCTION(IdentityD)

			GATEWARE_STATIC_FUNCTION(GetRotationD)
			GATEWARE_STATIC_FUNCTION(GetTranslationD)
			GATEWARE_STATIC_FUNCTION(GetScaleD)

			GATEWARE_STATIC_FUNCTION(RotateXGlobalD)
			GATEWARE_STATIC_FUNCTION(RotateXLocalD)
			GATEWARE_STATIC_FUNCTION(RotateYGlobalD)
			GATEWARE_STATIC_FUNCTION(RotateYLocalD)
			GATEWARE_STATIC_FUNCTION(RotateZGlobalD)
			GATEWARE_STATIC_FUNCTION(RotateZLocalD)
			GATEWARE_STATIC_FUNCTION(RotationYawPitchRollD)
			GATEWARE_STATIC_FUNCTION(RotationByVectorD)

			GATEWARE_STATIC_FUNCTION(TranslateGlobalD)
			GATEWARE_STATIC_FUNCTION(TranslateLocalD)

			GATEWARE_STATIC_FUNCTION(ScaleGlobalD)
			GATEWARE_STATIC_FUNCTION(ScaleLocalD)

			GATEWARE_STATIC_FUNCTION(LerpD)

			GATEWARE_STATIC_FUNCTION(ProjectionDirectXLHD)
			GATEWARE_STATIC_FUNCTION(ProjectionDirectXRHD)
			GATEWARE_STATIC_FUNCTION(ProjectionOpenGLLHD)
			GATEWARE_STATIC_FUNCTION(ProjectionOpenGLRHD)
			GATEWARE_STATIC_FUNCTION(ProjectionVulkanLHD)
			GATEWARE_STATIC_FUNCTION(ProjectionVulkanRHD)

			GATEWARE_STATIC_FUNCTION(LookAtLHD)
			GATEWARE_STATIC_FUNCTION(LookAtRHD)

			GATEWARE_STATIC_FUNCTION(MakeRelativeD)
			GATEWARE_STATIC_FUNCTION(MakeSeparateD)

			GATEWARE_STATIC_FUNCTION(Downgrade)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a valid GMatrix Object. 
			/*!
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory)
			*/
			GReturn Create();

			//! Add two specified matrices
			/*!
			*	Adds the two specified matrices and stores the result in the output matrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn AddMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two specified matrices
			/*!
			*	Subtracts the two specified matrices and stores the result in the output matrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of subtraction
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn SubtractMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply two specified matrices
			/*!
			*	Multiplies the two specified matrices and stores the result in the output matrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of Multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MultiplyMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply the specified matrix by the specified vector.
			/*!
			*	Multiplies the specified matrix by the specified vector
			*	and stores the result in the output vector.
			*	The input vector's w value will be returned with 0.
			*
			*	\param [in]  _matrix		The input matrix
			*	\param [in]  _vector		The input vector
			*	\param [out] _outVector		The result of multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn VectorXMatrixF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Convert the specified quaternion to a matrix
			/*!
			*	Converts the specified quaternion to a matrix and stores the result in the output matrix.
			*
			*	\param [in]  _quaternion	The specified quaternion
			*	\param [out] _outMatrix		The result of the convert
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ConvertQuaternionF(GW::MATH::GQUATERNIONF _quaternion, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the matrix
			/*!
			*	Scales the specified matrix with a number and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _scalar		The specified value to scale
			*	\param [out] _outMatrix		The result of the scaling
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MultiplyNumF(GW::MATH::GMATRIXF _matrix, float _scalar, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the determinant of the specified matrix
			/*!
			*	Calculates the determinant of the specified matrix and stores the result in the output value.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outValue		The result of the determinant
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn DeterminantF(GW::MATH::GMATRIXF _matrix, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Transpose the specified matrix
			/*!
			*	Transposes the specified matrix and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outMatrix		The result of the transpose
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TransposeF(GW::MATH::GMATRIXF _matrix, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Inverse the specified matrix
			/*!
			*	Inverses the specified matrix and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outMatrix		The result of the inverse
			*
			*	\retval GReturn::FAILURE				The calculation failed
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn InverseF(GW::MATH::GMATRIXF _matrix, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Identity the specified matrix
			/*!
			*	Set the output matrix as an identity matrix
			*
			*	\param [out] _outMatrix		The result of the identity
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn IdentityF(GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Get the quaternion which represents the rotation of the specified matrix
			/*!
			*	Get the quaternion which represents the rotation of the specified matrix
			*	and stores the result in the output quaternion.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outQuaternion	The quaternion of the specified matrix
			*
			*	\retval GReturn::FAILURE				The calculation failed
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetRotationF(GW::MATH::GMATRIXF _matrix, GW::MATH::GQUATERNIONF& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Gets the translation vector from the specified matrix
			/*!
			*	Gets the translation vector from the specified matrix
			*	and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outVector		The translation vector of the specified matrix
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetTranslationF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Gets the scale vector from the specified matrix
			/*!
			*	Gets the scale vector from the specified matrix
			*	and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outVector		The scale vector of the specified matrix
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetScaleF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the x-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the x-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateXGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the x-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the x-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin. Preserves translation.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateXLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the y-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the y-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateYGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the y-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the y-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin. Preserves translation.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateYLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the z-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the z-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateZGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the z-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the z-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin. Preserves translation.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateZLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a matrix based on specified yaw, pitch, and roll angles in radian. 
			/*!
			*	This is a convenience function to apply a three-axis rotation in one function 
			*	The mathematical formula is: YawPitchRoll_RotationMatrix = ( Mat_Roll * ( Mat_Pitch * Mat_Yaw))
			*
			*	\param [in]  _yaw			Angle of rotation around the y-axis, in radians.
			*	\param [in]  _pitch			Angle of rotation around the x-axis, in radians.
			*	\param [in]  _roll			Angle of rotation around the z-axis, in radians.
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn RotationYawPitchRollF(float _yaw, float _pitch, float _roll, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a rotation matrix based on specified vector and an angle in radian. 
			/*!
			*	Builds a matrix that rotates counter-clockwise around a specified axis.
			*
			*	\param [in]  _vector		Vector describing the axis of rotation.
			*	\param [in]  _radian		Angle of rotation around the vector, in radians.
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn RotationByVectorF(GW::MATH::GVECTORF _vector, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Translate the matrix by the specified vector
			/*!
			*	Translates the matrix by the specified vector
			*	and stores the result in the output matrix.
			*	The translation values along the x, y and z axes.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _vector		The vector to translate
			*	\param [out] _outMatrix		The result of the translation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TranslateGlobalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Translate the matrix by the specified vector
			/*!
			*	Translates the matrix by the specified vector
			*	and stores the result in the output matrix.
			*	The translation values along the x, y and z axes.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _vector		The vector to translate
			*	\param [out] _outMatrix		The result of the translation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TranslateLocalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the matrix by the specified vector
			/*!
			*	Scales the matrix by the specified vector
			*	and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _vector		The vector to scale
			*	\param [out] _outMatrix		The result of the scaling
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleGlobalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the matrix by the specified vector
			/*!
			*	Scales the matrix by the specified vector
			*	and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _vector		The vector to scale
			*	\param [out] _outMatrix		The result of the scaling
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleLocalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Linearly interpolates between two matrices.
			/*!
			*	Linearly interpolates between two matrices
			*	and stores the result in the output matrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [in]  _ratio			The interpolation coefficient
			*	\param [out] _outMatrix		The result of the scaling
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn LerpF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, float _ratio, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a left-handed perspective matrix for DirectX
			/*!
			*	Builds a left-handed perspective matrix for DirectX
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionDirectXLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a right-handed perspective matrix for DirectX
			/*!
			*	Builds a right-handed perspective matrix for DirectX
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionDirectXRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a left-handed perspective matrix for OpenGL
			/*!
			*	Builds a left-handed perspective matrix for OpenGL
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionOpenGLLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a right-handed perspective matrix for OpenGL
			/*!
			*	Builds a right-handed perspective matrix for OpenGL
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionOpenGLRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a left-handed perspective matrix for Vulkan
			/*!
			*	Builds a left-handed perspective matrix for Vulkan
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionVulkanLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a right-handed perspective matrix for Vulkan
			/*!
			*	Builds a right-handed perspective matrix for Vulkan
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionVulkanRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }


			//! Builds a left-handed view matrix
/*!
*	Builds a left-handed view matrix
*
*	\param [in]  _eye			The position of eye
*	\param [in]  _at			The position of the camera look-at target
*	\param [in]  _up			The direction of the world's up
*	\param [out] _outMatrix		The result of the look-at matrix
*
*	\retval GReturn::FAILURE				The building failed
*	\retval GReturn::SUCCESS				The building succeeded
*/
			static GReturn LookAtLHF(GW::MATH::GVECTORF _eye, GW::MATH::GVECTORF _at, GW::MATH::GVECTORF _up, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a right-handed view matrix
/*!
*	Builds a right-handed view matrix
*
*	\param [in]  _eye			The position of eye
*	\param [in]  _at			The position of the camera look-at target
*	\param [in]  _up			The direction of the world's up
*	\param [out] _outMatrix		The result of the look-at matrix
*
*	\retval GReturn::FAILURE				The building failed
*	\retval GReturn::SUCCESS				The building succeeded
*/
			static GReturn LookAtRHF(GW::MATH::GVECTORF _eye, GW::MATH::GVECTORF _at, GW::MATH::GVECTORF _up, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Makes matrix1 relative to matrix2
			/*!
			*	Performs matrix1 * inverse(matrix2)
			*
			*	\param [in]  _matrix1
			*	\param [in]  _matrix2
			*	\param [out] _outMatrix		The result of matrix1 * inverse(matrix2)
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeRelativeF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Makes matrix1 un-relative to matrix2
			/*!
			*	Performs matrix1 * matrix2. This will effectively undo MakeRelative()
			*
			*	\param [in]  _matrix1
			*	\param [in]  _matrix2
			*	\param [out] _outMatrix		The result of matrix1 * matrix2
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeSeparateF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Promotes float matrix to double matrix
			/*!
			*	Performs a static_cast<double>() on every element of the input matrix and assigns them to the output matrix
			*
			*	\param [in]  _matrixF		A float matrix
			*	\param [out] _outMatrixD	The input float matrix static_casted to a double matrix
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Upgrade(GW::MATH::GMATRIXF _matrixF, GW::MATH::GMATRIXD& _outMatrixD) { return GReturn::NO_IMPLEMENTATION; }

			//! Add two specified matrices
			/*!
			*	Adds the two specified matrices and stores the result in the output matrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn AddMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two specified matrices
			/*!
			*	Subtracts the two specified matrices and stores the result in the output matrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of subtraction
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn SubtractMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply two specified matrices
			/*!
			*	Multiplies the two specified matrices and stores the result in the output matrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of Multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MultiplyMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply the specified matrix by the specified vector.
			/*!
			*	Multiplies the specified matrix by the specified vector
			*	and stores the result in the output vector.
			*	The input vectors' w value will be returned with 0.
			*
			*	\param [in]  _matrix		The input matrix
			*	\param [in]  _vector		The input vector
			*	\param [out] _outVector		The result of multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn VectorXMatrixD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Convert the specified quaternion to a matrix
			/*!
			*	Converts the specified quaternion to a matrix and stores the result in the output matrix.
			*
			*	\param [in]  _quaternion	The specified quaternion
			*	\param [out] _outMatrix		The result of the convert
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ConvertQuaternionD(GW::MATH::GQUATERNIOND _quaternion, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the matrix
			/*!
			*	Scales the specified matrix with a number and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _scalar		The specified value to scale
			*	\param [out] _outMatrix		The result of the scaling
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MultiplyNumD(GW::MATH::GMATRIXD _matrix, double _scalar, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the determinant of the specified matrix
			/*!
			*	Calculates the determinant of the specified matrix and stores the result in the output value.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outValue		The result of the determinant
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn DeterminantD(GW::MATH::GMATRIXD _matrix, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Transpose the specified matrix
			/*!
			*	Transposes the specified matrix and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outMatrix		The result of the transpose
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TransposeD(GW::MATH::GMATRIXD _matrix, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Inverse the specified matrix
			/*!
			*	Inverses the specified matrix and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outMatrix		The result of the inverse
			*
			*	\retval GReturn::FAILURE				The calculation failed
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn InverseD(GW::MATH::GMATRIXD _matrix, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Identity the specified matrix
			/*!
			*	Set the output matrix as an identity matrix
			*
			*	\param [out] _outMatrix		The result of the identity
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn IdentityD(GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Get the quaternion which represents the rotation of the specified matrix
			/*!
			*	Get the quaternion which represents the rotation of the specified matrix
			*	and stores the result in the output quaternion.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outQuaternion	The quaternion of the specified matrix
			*
			*	\retval GReturn::FAILURE				The calculation failed
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetRotationD(GW::MATH::GMATRIXD _matrix, GW::MATH::GQUATERNIOND& _outQuaternion) { return GReturn::NO_IMPLEMENTATION; }

			//! Get the translation vector from the specified matrix
			/*!
			*	Gets the translation vector from the specified matrix
			*	and stores the result in the output vector.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outVector		The translation vector of the specified matrix
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetTranslationD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Gets the scale vector from the specified matrix 
			/*!
			*	Gets the scale vector from the specified matrix 
			*	and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [out] _outVector		The scaling vector of the specified matrix
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetScaleD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the x-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the x-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin. Preserves translation.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateXGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the x-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the x-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateXLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the y-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the y-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin. Preserves translation.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateYGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the y-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the y-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateYLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the z-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the z-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin. Preserves translation.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateZGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate the specified matrix around the z-axis by multiplying a left-handed rotation matrix
			/*!
			*	Rotate the specified matrix around the z-axis by multiplying a left-handed rotation matrix
			*	and stores the result in the output matrix. Angles are measured clockwise when
			*	looking along the rotation axis toward the origin.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _radian		The radian to rotate
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateZLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a matrix based on specified yaw, pitch, and roll angles in radian. 
			/*!
			*	This is a convenience function to apply a three-axis rotation in one function
			*	The mathematical formula is: YawPitchRoll_RotationMatrix = ( Mat_Roll * ( Mat_Pitch * Mat_Yaw))
			*
			*	\param [in]  _yaw			Angle of rotation around the y-axis, in radians.
			*	\param [in]  _pitch			Angle of rotation around the x-axis, in radians.
			*	\param [in]  _roll			Angle of rotation around the z-axis, in radians.
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn RotationYawPitchRollD(double _yaw, double _pitch, double _roll, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a rotation matrix based on specified vector and an angle in radian. 
			/*!
			*	Builds a matrix that rotates counter-clockwise around a specified axis. 
			*
			*	\param [in]  _vector		Vector describing the axis of rotation.
			*	\param [in]  _radian		Angle of rotation around the vector, in radians.
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn RotationByVectorD(GW::MATH::GVECTORD _vector, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Translate the matrix by the specified vector
			/*!
			*	Translates the matrix by the specified vector
			*	and stores the result in the output matrix.
			*	The translation values along the x, y and z axes.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _vector		The vector to translate
			*	\param [out] _outMatrix		The result of the translation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TranslateGlobalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Translate the matrix by the specified vector
			/*!
			*	Translates the matrix by the specified vector
			*	and stores the result in the output matrix.
			*	The translation values along the x, y and z axes.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _vector		The vector to translate
			*	\param [out] _outMatrix		The result of the translation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TranslateLocalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the matrix by the specified vector
			/*!
			*	Scales the matrix by the specified vector
			*	and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _vector		The vector to scale
			*	\param [out] _outMatrix		The result of the scaling
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleGlobalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale the matrix by the specified vector
			/*!
			*	Scales the matrix by the specified vector
			*	and stores the result in the output matrix.
			*
			*	\param [in]  _matrix		The specified matrix
			*	\param [in]  _vector		The vector to scale
			*	\param [out] _outMatrix		The result of the scaling
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleLocalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Linearly interpolates between two matrices.
			/*!
			*	Linearly interpolates between two matrices
			*	and stores the result in the output matrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [in]  _ratio			The interpolation coefficient
			*	\param [out] _outMatrix		The result of the scaling
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn LerpD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, double _ratio, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a left-handed perspective matrix for DirectX
			/*!
			*	Builds a left-handed perspective matrix for DirectX
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionDirectXLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a right-handed perspective matrix for DirectX
			/*!
			*	Builds a right-handed perspective matrix for DirectX
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionDirectXRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a left-handed perspective matrix for OpenGL
			/*!
			*	Builds a left-handed perspective matrix for OpenGL
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionOpenGLLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a right-handed perspective matrix for OpenGL
			/*!
			*	Builds a right-handed perspective matrix for OpenGL
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionOpenGLRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a left-handed perspective matrix for Vulkan
			/*!
			*	Builds a left-handed perspective matrix for Vulkan
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionVulkanLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a right-handed perspective matrix for Vulkan
			/*!
			*	Builds a right-handed perspective matrix for Vulkan
			*
			*	\param [in]  _fovY	Field of view in the y direction, in radians
			*	\param [in]  _aspect		Aspect ratio, defined as view space width divided by height
			*	\param [in]  _zn			Z-value of the near view-plane
			*	\param [in]  _zf			Z-value of the far view-plane
			*	\param [out] _outMatrix		The result of the projection matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn ProjectionVulkanRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a left-handed view matrix
			/*!
			*	Builds a left-handed view matrix
			*
			*	\param [in]  _eye			The position of eye
			*	\param [in]  _at			The position of the camera look-at target
			*	\param [in]  _up			The direction of the world's up
			*	\param [out] _outMatrix		The result of the look-at matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn LookAtLHD(GW::MATH::GVECTORD _eye, GW::MATH::GVECTORD _at, GW::MATH::GVECTORD _up, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Builds a right-handed view matrix
			/*!
			*	Builds a right-handed view matrix
			*
			*	\param [in]  _eye			The position of eye
			*	\param [in]  _at			The position of the camera look-at target
			*	\param [in]  _up			The direction of the world's up
			*	\param [out] _outMatrix		The result of the look-at matrix
			*
			*	\retval GReturn::FAILURE				The building failed
			*	\retval GReturn::SUCCESS				The building succeeded
			*/
			static GReturn LookAtRHD(GW::MATH::GVECTORD _eye, GW::MATH::GVECTORD _at, GW::MATH::GVECTORD _up, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Makes matrix1 relative to matrix2
			/*!
			*	Performs matrix1 * inverse(matrix2)
			*
			*	\param [in]  _matrix1
			*	\param [in]  _matrix2
			*	\param [out] _outMatrix		The result of matrix1 * inverse(matrix2)
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeRelativeD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Makes matrix1 un-relative to matrix2
			/*!
			*	Performs matrix1 * matrix2. This will effectively undo MakeRelative()
			*
			*	\param [in]  _matrix1
			*	\param [in]  _matrix2
			*	\param [out] _outMatrix		The result of  matrix1 * matrix2
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeSeparateD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Demotes double matrix to float matrix
			/*!
			*	Performs a static_cast<float>() on every element of the input matrix and assigns them to the output matrix
			*
			*	\param [in]  _matrixD		A double matrix
			*	\param [out] _outMatrixF	The input double matrix static_casted to a float matrix
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Downgrade(GW::MATH::GMATRIXD _matrixD, GW::MATH::GMATRIXF& _outMatrixF) { return GReturn::NO_IMPLEMENTATION; }
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GMATRIX_H