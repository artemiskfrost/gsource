#ifndef GCOLLISION_H
#define GCOLLISION_H

/*!
	File: GCollision.h
	Purpose: A Gateware interface that handles all collision functions.
	Author: Ryan Bickell
	Contributors: Ryan Powser, Lari Norri
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"
#include "GMathDefines.h"
#include "GVector.h"
#include "GMatrix.h"

namespace GW
{
	namespace I
	{
		class GCollisionInterface : public virtual GInterfaceInterface
		{
		public:

			enum class GCollisionCheck
			{
				ERROR_NO_RESULT = -3,
				ABOVE = -2,
				BELOW = -1,
				NO_COLLISION = 0,
				COLLISION = 1
			};

			// float
			static GReturn ConvertAABBCEToAABBMMF(const MATH::GAABBCEF _aabbCE, MATH::GAABBMMF& _outAABBMM) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ConvertAABBMMToAABBCEF(const MATH::GAABBMMF _aabbMM, MATH::GAABBCEF& _outAABCE) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ComputePlaneF(const MATH::GVECTORF _planePositionA, const MATH::GVECTORF _planePositionB, const MATH::GVECTORF _planePositionC, MATH::GPLANEF& _outPlane) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IsTriangleF(const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToConvexPolygonF(const MATH::GVECTORF _queryPoint, const MATH::GVECTORF* _polygonPoints, const unsigned int _pointsCount, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToLineF(const MATH::GLINEF _line, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointsToLineFromLineF(const MATH::GLINEF _line1, const MATH::GLINEF _line2, MATH::GVECTORF& _outPoint1, MATH::GVECTORF& _outPoint2) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToRayF(const MATH::GRAYF _ray, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToTriangleF(const MATH::GTRIANGLEF _triangle, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToPlaneF(const MATH::GPLANEF _plane, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToSphereF(const MATH::GSPHEREF _sphere, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToCapsuleF(const MATH::GCAPSULEF _capsule, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToAABBF(const MATH::GAABBMMF _aabb, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToOBBF(const MATH::GOBBF _obb, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ComputeSphereFromPointsF(const MATH::GVECTORF* _pointCloud, const unsigned int _pointsCount, MATH::GSPHEREF& _outSphere) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ComputeAABBFromPointsF(const MATH::GVECTORF* _pointCloud, const unsigned int _pointsCount, MATH::GAABBMMF& _outAABB) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToLineF(const MATH::GVECTORF _point, const MATH::GLINEF _line, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToRayF(const MATH::GVECTORF _point, const MATH::GRAYF _ray, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToTriangleF(const MATH::GVECTORF _point, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToPlaneF(const MATH::GVECTORF _point, const MATH::GPLANEF _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToSphereF(const MATH::GVECTORF _point, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToCapsuleF(const MATH::GVECTORF _point, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToAABBF(const MATH::GVECTORF _point, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToOBBF(const MATH::GVECTORF _point, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToLineF(const MATH::GLINEF _line1, const MATH::GLINEF _line2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToRayF(const MATH::GLINEF _line, const MATH::GRAYF _ray, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToTriangleF(const MATH::GLINEF _line, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToPlaneF(const MATH::GLINEF _line, const MATH::GPLANEF _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToSphereF(const MATH::GLINEF _line, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToCapsuleF(const MATH::GLINEF _line, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToAABBF(const MATH::GLINEF _line, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToOBBF(const MATH::GLINEF _line, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToTriangleF(const MATH::GRAYF _ray, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToPlaneF(const MATH::GRAYF _ray, const MATH::GPLANEF _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToSphereF(const MATH::GRAYF _ray, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToCapsuleF(const MATH::GRAYF _ray, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToAABBF(const MATH::GRAYF _ray, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToOBBF(const MATH::GRAYF _ray, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToTriangleF(const MATH::GTRIANGLEF _triangle1, const MATH::GTRIANGLEF _triangle2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToPlaneF(const MATH::GTRIANGLEF _triangle, const MATH::GPLANEF _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToSphereF(const MATH::GTRIANGLEF _triangle, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToCapsuleF(const MATH::GTRIANGLEF _triangle, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToAABBF(const MATH::GTRIANGLEF _triangle, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToOBBF(const MATH::GTRIANGLEF _triangle, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToPlaneF(const MATH::GPLANEF _plane1, const MATH::GPLANEF _plane2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToSphereF(const MATH::GPLANEF _plane, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToCapsuleF(const MATH::GPLANEF _plane, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToAABBF(const MATH::GPLANEF _plane, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToOBBF(const MATH::GPLANEF _plane, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestSphereToSphereF(const MATH::GSPHEREF _sphere1, const MATH::GSPHEREF _sphere2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestSphereToCapsuleF(const MATH::GSPHEREF _sphere, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestSphereToAABBF(const MATH::GSPHEREF _sphere, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestSphereToOBBF(const MATH::GSPHEREF _sphere, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestCapsuleToCapsuleF(const MATH::GCAPSULEF _capsule1, const MATH::GCAPSULEF _capsule2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestCapsuleToAABBF(const MATH::GCAPSULEF _capsule, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestCapsuleToOBBF(const MATH::GCAPSULEF _capsule, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestAABBToAABBF(const MATH::GAABBCEF _aabb1, const MATH::GAABBCEF _aabb2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestAABBToOBBF(const MATH::GAABBCEF _aabb, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestOBBToOBBF(const MATH::GOBBF _obb1, const MATH::GOBBF _obb2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToTriangleF(const MATH::GLINEF _line, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF* _outBarycentric, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToPlaneF(const MATH::GLINEF _line, const MATH::GPLANEF _plane, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToSphereF(const MATH::GLINEF _line, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToCapsuleF(const MATH::GLINEF _line, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToAABBF(const MATH::GLINEF _line, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToOBBF(const MATH::GLINEF _line, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToTriangleF(const MATH::GRAYF _ray, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF* _outBarycentric, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToPlaneF(const MATH::GRAYF _ray, const MATH::GPLANEF _plane, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToSphereF(const MATH::GRAYF _ray, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToCapsuleF(const MATH::GRAYF _ray, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToAABBF(const MATH::GRAYF _ray, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToOBBF(const MATH::GRAYF _ray, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectSphereToSphereF(const MATH::GSPHEREF _sphere1, const MATH::GSPHEREF _sphere2, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectSphereToCapsuleF(const MATH::GSPHEREF _sphere, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectSphereToAABBF(const MATH::GSPHEREF _sphere, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectSphereToOBBF(const MATH::GSPHEREF _sphere, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectCapsuleToCapsuleF(const MATH::GCAPSULEF _capsule1, const MATH::GCAPSULEF _capsule2, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectCapsuleToAABBF(const MATH::GCAPSULEF _capsule, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectCapsuleToOBBF(const MATH::GCAPSULEF _capsule, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectAABBToAABBF(const MATH::GAABBCEF _aabb1, const MATH::GAABBCEF _aabb2, GCollisionCheck& _outResult, MATH::GAABBCEF& _outContactAABB, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToLineF(const MATH::GVECTORF _point, const MATH::GLINEF _line, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToRayF(const MATH::GVECTORF _point, const MATH::GRAYF _ray, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToTriangleF(const MATH::GVECTORF _point, const MATH::GTRIANGLEF _triangle, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToPlaneF(const MATH::GVECTORF _point, const MATH::GPLANEF _plane, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToSphereF(const MATH::GVECTORF _point, const MATH::GSPHEREF _sphere, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToCapsuleF(const MATH::GVECTORF _point, const MATH::GCAPSULEF _capsule, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToAABBF(const MATH::GVECTORF _point, const MATH::GAABBMMF _aabb, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToOBBF(const MATH::GVECTORF _point, const MATH::GOBBF _obb, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn BarycentricF(const MATH::GVECTORF _a, const MATH::GVECTORF _b, const MATH::GVECTORF _c, const MATH::GVECTORF _p, MATH::GVECTORF& _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
			// double
			static GReturn ConvertAABBCEToAABBMMD(const MATH::GAABBCED _aabbCE, MATH::GAABBMMD& _outAABBMM) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ConvertAABBMMToAABBCED(const MATH::GAABBMMD _aabbMM, MATH::GAABBCED& _outAABCE) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ComputePlaneD(const MATH::GVECTORD _planePositionA, const MATH::GVECTORD _planePositionB, const MATH::GVECTORD _planePositionC, MATH::GPLANED& _outPlane) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IsTriangleD(const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToConvexPolygonD(const MATH::GVECTORD _queryPoint, const MATH::GVECTORD* _polygonPoints, const unsigned int _pointsCount, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToLineD(const MATH::GLINED _line, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointsToLineFromLineD(const MATH::GLINED _line1, const MATH::GLINED _line2, MATH::GVECTORD& _outPoint1, MATH::GVECTORD& _outPoint2) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToRayD(const MATH::GRAYD _ray, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToTriangleD(const MATH::GTRIANGLED _triangle, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToPlaneD(const MATH::GPLANED _plane, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToSphereD(const MATH::GSPHERED _sphere, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToCapsuleD(const MATH::GCAPSULED _capsule, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToAABBD(const MATH::GAABBMMD _aabb, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ClosestPointToOBBD(const MATH::GOBBD _obb, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ComputeSphereFromPointsD(const MATH::GVECTORD* _pointCloud, const unsigned int _pointsCount, MATH::GSPHERED& _outSphere) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ComputeAABBFromPointsD(const MATH::GVECTORD* _pointCloud, const unsigned int _pointsCount, MATH::GAABBMMD& _outAABB) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToLineD(const MATH::GVECTORD _point, const MATH::GLINED _line, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToRayD(const MATH::GVECTORD _point, const MATH::GRAYD _ray, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToTriangleD(const MATH::GVECTORD _point, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToPlaneD(const MATH::GVECTORD _point, const MATH::GPLANED _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToSphereD(const MATH::GVECTORD _point, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToCapsuleD(const MATH::GVECTORD _point, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToAABBD(const MATH::GVECTORD _point, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToOBBD(const MATH::GVECTORD _point, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToLineD(const MATH::GLINED _line1, const MATH::GLINED _line2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToRayD(const MATH::GLINED _line, const MATH::GRAYD _ray, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToTriangleD(const MATH::GLINED _line, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToPlaneD(const MATH::GLINED _line, const MATH::GPLANED _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToSphereD(const MATH::GLINED _line, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToCapsuleD(const MATH::GLINED _line, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToAABBD(const MATH::GLINED _line, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToOBBD(const MATH::GLINED _line, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToTriangleD(const MATH::GRAYD _ray, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToPlaneD(const MATH::GRAYD _ray, const MATH::GPLANED _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToSphereD(const MATH::GRAYD _ray, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToCapsuleD(const MATH::GRAYD _ray, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToAABBD(const MATH::GRAYD _ray, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRayToOBBD(const MATH::GRAYD _ray, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToTriangleD(const MATH::GTRIANGLED _triangle1, const MATH::GTRIANGLED _triangle2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToPlaneD(const MATH::GTRIANGLED _triangle, const MATH::GPLANED _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToSphereD(const MATH::GTRIANGLED _triangle, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToCapsuleD(const MATH::GTRIANGLED _triangle, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToAABBD(const MATH::GTRIANGLED _triangle, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestTriangleToOBBD(const MATH::GTRIANGLED _triangle, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToPlaneD(const MATH::GPLANED _plane1, const MATH::GPLANED _plane2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToSphereD(const MATH::GPLANED _plane, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToCapsuleD(const MATH::GPLANED _plane, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToAABBD(const MATH::GPLANED _plane, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPlaneToOBBD(const MATH::GPLANED _plane, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestSphereToSphereD(const MATH::GSPHERED _sphere1, const MATH::GSPHERED _sphere2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestSphereToCapsuleD(const MATH::GSPHERED _sphere, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestSphereToAABBD(const MATH::GSPHERED _sphere, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestSphereToOBBD(const MATH::GSPHERED _sphere, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestCapsuleToCapsuleD(const MATH::GCAPSULED _capsule1, const MATH::GCAPSULED _capsule2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestCapsuleToAABBD(const MATH::GCAPSULED _capsule, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestCapsuleToOBBD(const MATH::GCAPSULED _capsule, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestAABBToAABBD(const MATH::GAABBCED _aabb1, const MATH::GAABBCED _aabb2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestAABBToOBBD(const MATH::GAABBCED _aabb, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestOBBToOBBD(const MATH::GOBBD _obb1, const MATH::GOBBD _obb2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToTriangleD(const MATH::GLINED _line, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD* _outBarycentric, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToPlaneD(const MATH::GLINED _line, const MATH::GPLANED _plane, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToSphereD(const MATH::GLINED _line, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToCapsuleD(const MATH::GLINED _line, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToAABBD(const MATH::GLINED _line, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectLineToOBBD(const MATH::GLINED _line, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToTriangleD(const MATH::GRAYD _ray, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD* _outBarycentric, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToPlaneD(const MATH::GRAYD _ray, const MATH::GPLANED _plane, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToSphereD(const MATH::GRAYD _ray, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToCapsuleD(const MATH::GRAYD _ray, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToAABBD(const MATH::GRAYD _ray, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectRayToOBBD(const MATH::GRAYD _ray, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectSphereToSphereD(const MATH::GSPHERED _sphere1, const MATH::GSPHERED _sphere2, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectSphereToCapsuleD(const MATH::GSPHERED _sphere, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectSphereToAABBD(const MATH::GSPHERED _sphere, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectSphereToOBBD(const MATH::GSPHERED _sphere, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectCapsuleToCapsuleD(const MATH::GCAPSULED _capsule1, const MATH::GCAPSULED _capsule2, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectCapsuleToAABBD(const MATH::GCAPSULED _capsule, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectCapsuleToOBBD(const MATH::GCAPSULED _capsule, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn IntersectAABBToAABBD(const MATH::GAABBCED _aabb1, const MATH::GAABBCED _aabb2, GCollisionCheck& _outResult, MATH::GAABBCED& _outContactAABB, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToLineD(const MATH::GVECTORD _point, const MATH::GLINED _line, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToRayD(const MATH::GVECTORD _point, const MATH::GRAYD _ray, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToTriangleD(const MATH::GVECTORD _point, const MATH::GTRIANGLED _triangle, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToPlaneD(const MATH::GVECTORD _point, const MATH::GPLANED _plane, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToSphereD(const MATH::GVECTORD _point, const MATH::GSPHERED _sphere, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToCapsuleD(const MATH::GVECTORD _point, const MATH::GCAPSULED _capsule, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToAABBD(const MATH::GVECTORD _point, const MATH::GAABBMMD _aabb, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToOBBD(const MATH::GVECTORD _point, const MATH::GOBBD _obb, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn BarycentricD(const MATH::GVECTORD _a, const MATH::GVECTORD _b, const MATH::GVECTORD _c, const MATH::GVECTORD _p, MATH::GVECTORD& _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
		};
	}
}

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Math/GCollision/GCollision.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace MATH
	{
		//! A Gateware interface that handles all collision functions.
		class GCollision final
			: public I::GProxy<I::GCollisionInterface, I::GCollisionImplementation>
		{
		public:
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GCollision)
			GATEWARE_TYPEDEF(GCollisionCheck)
			GATEWARE_STATIC_FUNCTION(ConvertAABBCEToAABBMMF)
			GATEWARE_STATIC_FUNCTION(ConvertAABBMMToAABBCEF)
			GATEWARE_STATIC_FUNCTION(ComputePlaneF)
			GATEWARE_STATIC_FUNCTION(IsTriangleF)
			GATEWARE_STATIC_FUNCTION(TestPointToConvexPolygonF)
			GATEWARE_STATIC_FUNCTION(ClosestPointToLineF)
			GATEWARE_STATIC_FUNCTION(ClosestPointsToLineFromLineF)
			GATEWARE_STATIC_FUNCTION(ClosestPointToRayF)
			GATEWARE_STATIC_FUNCTION(ClosestPointToTriangleF)
			GATEWARE_STATIC_FUNCTION(ClosestPointToPlaneF)
			GATEWARE_STATIC_FUNCTION(ClosestPointToSphereF)
			GATEWARE_STATIC_FUNCTION(ClosestPointToCapsuleF)
			GATEWARE_STATIC_FUNCTION(ClosestPointToAABBF)
			GATEWARE_STATIC_FUNCTION(ClosestPointToOBBF)
			GATEWARE_STATIC_FUNCTION(ComputeSphereFromPointsF)
			GATEWARE_STATIC_FUNCTION(ComputeAABBFromPointsF)
			GATEWARE_STATIC_FUNCTION(TestPointToLineF)
			GATEWARE_STATIC_FUNCTION(TestPointToRayF)
			GATEWARE_STATIC_FUNCTION(TestPointToTriangleF)
			GATEWARE_STATIC_FUNCTION(TestPointToPlaneF)
			GATEWARE_STATIC_FUNCTION(TestPointToSphereF)
			GATEWARE_STATIC_FUNCTION(TestPointToCapsuleF)
			GATEWARE_STATIC_FUNCTION(TestPointToAABBF)
			GATEWARE_STATIC_FUNCTION(TestPointToOBBF)
			GATEWARE_STATIC_FUNCTION(TestLineToLineF)
			GATEWARE_STATIC_FUNCTION(TestLineToRayF)
			GATEWARE_STATIC_FUNCTION(TestLineToTriangleF)
			GATEWARE_STATIC_FUNCTION(TestLineToPlaneF)
			GATEWARE_STATIC_FUNCTION(TestLineToSphereF)
			GATEWARE_STATIC_FUNCTION(TestLineToCapsuleF)
			GATEWARE_STATIC_FUNCTION(TestLineToAABBF)
			GATEWARE_STATIC_FUNCTION(TestLineToOBBF)
			GATEWARE_STATIC_FUNCTION(TestRayToTriangleF)
			GATEWARE_STATIC_FUNCTION(TestRayToPlaneF)
			GATEWARE_STATIC_FUNCTION(TestRayToSphereF)
			GATEWARE_STATIC_FUNCTION(TestRayToCapsuleF)
			GATEWARE_STATIC_FUNCTION(TestRayToAABBF)
			GATEWARE_STATIC_FUNCTION(TestRayToOBBF)
			GATEWARE_STATIC_FUNCTION(TestTriangleToTriangleF)
			GATEWARE_STATIC_FUNCTION(TestTriangleToPlaneF)
			GATEWARE_STATIC_FUNCTION(TestTriangleToSphereF)
			GATEWARE_STATIC_FUNCTION(TestTriangleToCapsuleF)
			GATEWARE_STATIC_FUNCTION(TestTriangleToAABBF)
			GATEWARE_STATIC_FUNCTION(TestTriangleToOBBF)
			GATEWARE_STATIC_FUNCTION(TestPlaneToPlaneF)
			GATEWARE_STATIC_FUNCTION(TestPlaneToSphereF)
			GATEWARE_STATIC_FUNCTION(TestPlaneToCapsuleF)
			GATEWARE_STATIC_FUNCTION(TestPlaneToAABBF)
			GATEWARE_STATIC_FUNCTION(TestPlaneToOBBF)
			GATEWARE_STATIC_FUNCTION(TestSphereToSphereF)
			GATEWARE_STATIC_FUNCTION(TestSphereToCapsuleF)
			GATEWARE_STATIC_FUNCTION(TestSphereToAABBF)
			GATEWARE_STATIC_FUNCTION(TestSphereToOBBF)
			GATEWARE_STATIC_FUNCTION(TestCapsuleToCapsuleF)
			GATEWARE_STATIC_FUNCTION(TestCapsuleToAABBF)
			GATEWARE_STATIC_FUNCTION(TestCapsuleToOBBF)
			GATEWARE_STATIC_FUNCTION(TestAABBToAABBF)
			GATEWARE_STATIC_FUNCTION(TestAABBToOBBF)
			GATEWARE_STATIC_FUNCTION(TestOBBToOBBF)
			GATEWARE_STATIC_FUNCTION(IntersectLineToTriangleF)
			GATEWARE_STATIC_FUNCTION(IntersectLineToPlaneF)
			GATEWARE_STATIC_FUNCTION(IntersectLineToSphereF)
			GATEWARE_STATIC_FUNCTION(IntersectLineToCapsuleF)
			GATEWARE_STATIC_FUNCTION(IntersectLineToAABBF)
			GATEWARE_STATIC_FUNCTION(IntersectLineToOBBF)
			GATEWARE_STATIC_FUNCTION(IntersectRayToTriangleF)
			GATEWARE_STATIC_FUNCTION(IntersectRayToPlaneF)
			GATEWARE_STATIC_FUNCTION(IntersectRayToSphereF)
			GATEWARE_STATIC_FUNCTION(IntersectRayToCapsuleF)
			GATEWARE_STATIC_FUNCTION(IntersectRayToAABBF)
			GATEWARE_STATIC_FUNCTION(IntersectRayToOBBF)
			GATEWARE_STATIC_FUNCTION(IntersectSphereToSphereF)
			GATEWARE_STATIC_FUNCTION(IntersectSphereToCapsuleF)
			GATEWARE_STATIC_FUNCTION(IntersectSphereToAABBF)
			GATEWARE_STATIC_FUNCTION(IntersectSphereToOBBF)
			GATEWARE_STATIC_FUNCTION(IntersectCapsuleToCapsuleF)
			GATEWARE_STATIC_FUNCTION(IntersectCapsuleToAABBF)
			GATEWARE_STATIC_FUNCTION(IntersectCapsuleToOBBF)
			GATEWARE_STATIC_FUNCTION(IntersectAABBToAABBF)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToLineF)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToRayF)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToTriangleF)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToPlaneF)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToSphereF)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToCapsuleF)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToAABBF)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToOBBF)
			GATEWARE_STATIC_FUNCTION(BarycentricF)

			// Double
			GATEWARE_STATIC_FUNCTION(ConvertAABBCEToAABBMMD)
			GATEWARE_STATIC_FUNCTION(ConvertAABBMMToAABBCED)
			GATEWARE_STATIC_FUNCTION(ComputePlaneD)
			GATEWARE_STATIC_FUNCTION(IsTriangleD)
			GATEWARE_STATIC_FUNCTION(TestPointToConvexPolygonD)
			GATEWARE_STATIC_FUNCTION(ClosestPointToLineD)
			GATEWARE_STATIC_FUNCTION(ClosestPointsToLineFromLineD)
			GATEWARE_STATIC_FUNCTION(ClosestPointToRayD)
			GATEWARE_STATIC_FUNCTION(ClosestPointToTriangleD)
			GATEWARE_STATIC_FUNCTION(ClosestPointToPlaneD)
			GATEWARE_STATIC_FUNCTION(ClosestPointToSphereD)
			GATEWARE_STATIC_FUNCTION(ClosestPointToCapsuleD)
			GATEWARE_STATIC_FUNCTION(ClosestPointToAABBD)
			GATEWARE_STATIC_FUNCTION(ClosestPointToOBBD)
			GATEWARE_STATIC_FUNCTION(ComputeSphereFromPointsD)
			GATEWARE_STATIC_FUNCTION(ComputeAABBFromPointsD)
			GATEWARE_STATIC_FUNCTION(TestPointToLineD)
			GATEWARE_STATIC_FUNCTION(TestPointToRayD)
			GATEWARE_STATIC_FUNCTION(TestPointToTriangleD)
			GATEWARE_STATIC_FUNCTION(TestPointToPlaneD)
			GATEWARE_STATIC_FUNCTION(TestPointToSphereD)
			GATEWARE_STATIC_FUNCTION(TestPointToCapsuleD)
			GATEWARE_STATIC_FUNCTION(TestPointToAABBD)
			GATEWARE_STATIC_FUNCTION(TestPointToOBBD)
			GATEWARE_STATIC_FUNCTION(TestLineToLineD)
			GATEWARE_STATIC_FUNCTION(TestLineToRayD)
			GATEWARE_STATIC_FUNCTION(TestLineToTriangleD)
			GATEWARE_STATIC_FUNCTION(TestLineToPlaneD)
			GATEWARE_STATIC_FUNCTION(TestLineToSphereD)
			GATEWARE_STATIC_FUNCTION(TestLineToCapsuleD)
			GATEWARE_STATIC_FUNCTION(TestLineToAABBD)
			GATEWARE_STATIC_FUNCTION(TestLineToOBBD)
			GATEWARE_STATIC_FUNCTION(TestRayToTriangleD)
			GATEWARE_STATIC_FUNCTION(TestRayToPlaneD)
			GATEWARE_STATIC_FUNCTION(TestRayToSphereD)
			GATEWARE_STATIC_FUNCTION(TestRayToCapsuleD)
			GATEWARE_STATIC_FUNCTION(TestRayToAABBD)
			GATEWARE_STATIC_FUNCTION(TestRayToOBBD)
			GATEWARE_STATIC_FUNCTION(TestTriangleToTriangleD)
			GATEWARE_STATIC_FUNCTION(TestTriangleToPlaneD)
			GATEWARE_STATIC_FUNCTION(TestTriangleToSphereD)
			GATEWARE_STATIC_FUNCTION(TestTriangleToCapsuleD)
			GATEWARE_STATIC_FUNCTION(TestTriangleToAABBD)
			GATEWARE_STATIC_FUNCTION(TestTriangleToOBBD)
			GATEWARE_STATIC_FUNCTION(TestPlaneToPlaneD)
			GATEWARE_STATIC_FUNCTION(TestPlaneToSphereD)
			GATEWARE_STATIC_FUNCTION(TestPlaneToCapsuleD)
			GATEWARE_STATIC_FUNCTION(TestPlaneToAABBD)
			GATEWARE_STATIC_FUNCTION(TestPlaneToOBBD)
			GATEWARE_STATIC_FUNCTION(TestSphereToSphereD)
			GATEWARE_STATIC_FUNCTION(TestSphereToCapsuleD)
			GATEWARE_STATIC_FUNCTION(TestSphereToAABBD)
			GATEWARE_STATIC_FUNCTION(TestSphereToOBBD)
			GATEWARE_STATIC_FUNCTION(TestCapsuleToCapsuleD)
			GATEWARE_STATIC_FUNCTION(TestCapsuleToAABBD)
			GATEWARE_STATIC_FUNCTION(TestCapsuleToOBBD)
			GATEWARE_STATIC_FUNCTION(TestAABBToAABBD)
			GATEWARE_STATIC_FUNCTION(TestAABBToOBBD)
			GATEWARE_STATIC_FUNCTION(TestOBBToOBBD)
			GATEWARE_STATIC_FUNCTION(IntersectLineToTriangleD)
			GATEWARE_STATIC_FUNCTION(IntersectLineToPlaneD)
			GATEWARE_STATIC_FUNCTION(IntersectLineToSphereD)
			GATEWARE_STATIC_FUNCTION(IntersectLineToCapsuleD)
			GATEWARE_STATIC_FUNCTION(IntersectLineToAABBD)
			GATEWARE_STATIC_FUNCTION(IntersectLineToOBBD)
			GATEWARE_STATIC_FUNCTION(IntersectRayToTriangleD)
			GATEWARE_STATIC_FUNCTION(IntersectRayToPlaneD)
			GATEWARE_STATIC_FUNCTION(IntersectRayToSphereD)
			GATEWARE_STATIC_FUNCTION(IntersectRayToCapsuleD)
			GATEWARE_STATIC_FUNCTION(IntersectRayToAABBD)
			GATEWARE_STATIC_FUNCTION(IntersectRayToOBBD)
			GATEWARE_STATIC_FUNCTION(IntersectSphereToSphereD)
			GATEWARE_STATIC_FUNCTION(IntersectSphereToCapsuleD)
			GATEWARE_STATIC_FUNCTION(IntersectSphereToAABBD)
			GATEWARE_STATIC_FUNCTION(IntersectSphereToOBBD)
			GATEWARE_STATIC_FUNCTION(IntersectCapsuleToCapsuleD)
			GATEWARE_STATIC_FUNCTION(IntersectCapsuleToAABBD)
			GATEWARE_STATIC_FUNCTION(IntersectCapsuleToOBBD)
			GATEWARE_STATIC_FUNCTION(IntersectAABBToAABBD)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToLineD)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToRayD)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToTriangleD)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToPlaneD)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToSphereD)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToCapsuleD)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToAABBD)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToOBBD)
			GATEWARE_STATIC_FUNCTION(BarycentricD)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a valid GCollision Object.
			/*!
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory)
			*/
			GReturn Create();

			//! Listing of common containment or plane intersection codes returned by select GCollision functions.
			/*! \brief	Values that represent collision checks */
			enum class GCollisionCheck {
				ERROR_NO_RESULT = -3, 		//!< Used internally by Gateware for testing purposes
				ABOVE = -2,					//!< An object is above another object
				BELOW = -1,					//!< An object is below another object
				NO_COLLISION = 0,			//!< There is no collision between two objects
				COLLISION = 1				//!< There is a collision between two objects
			};

			//! Convert AABBCE to AABBMM.
			/*!
			* Computes the min-max representation of an AABB from a center-extent AABB representation.
			*
			* \param [in]	_aabbCE		The center-extent AABB.
			* \param [out]	_outAABBMM	The out min-max AABB.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ConvertAABBCEToAABBMMF(const MATH::GAABBCEF _aabbCE, MATH::GAABBMMF& _outAABBMM) { return GReturn::NO_IMPLEMENTATION; }

			//! Convert AABBCE to AABBMM.
			/*!
			* Computes the center-extent representation of an AABB from a min-max AABB representation.
			*
			* \param [in]	_aabbMM		The min-max AABB.
			* \param [out]	_outAABBCE	The out center-extent AABB.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ConvertAABBMMToAABBCEF(const MATH::GAABBMMF _aabbMM, MATH::GAABBCEF& _outAABBCE) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the plane given three points(clockwise).
			/*!
			* Calculates the plane given three points(clockwise) by taking the perpendicular vector that would intersect
			* a triangle that the three points represent. The plane is defined as a normal and distance from the origin.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur when the given plane's position points
			* are collinear.
			*
			* \param [in]	_planePositionA	The plane position a.
			* \param [in]	_planePositionB	The plane position b.
			* \param [in]	_planePositionC	The plane position c.
			* \param [out]	_outPlane	   	The out plane.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ComputePlaneF(const MATH::GVECTORF _planePositionA, const MATH::GVECTORF _planePositionB, const MATH::GVECTORF _planePositionC, MATH::GPLANEF& _outPlane) { return GReturn::NO_IMPLEMENTATION; }

			//! Tests whether a triangle is non-degenerate.
			/*!
			* Tests whether a triangle is non-degenerate. A non-degenerate triangle has an area greater than 0 and is not
			* a line.
			* If the triangle is non-degenerate, the out result is 1.
			* If the triangle is degenerate, the out result is 0.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IsTriangleF(const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Tests whether a point lies within a polygon or not.
			/*!
			* Tests whether a point is contained within a polygon by counting how many times a line that extends to
			* infinity intersects the sides of the polygon.
			* If the point is contained within the polygon, the out result is 1. This does not include the polygon's
			* boundary.
			* If the point is not contained within the polygon, the out result is 0.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the pointer to the polygon's points
			* is null or if the point count is less than 3.
			*
			* \param [in]	_point			The query point.
			* \param [in]	_polygonPoints	If non-null, the set of points representing the polygon vertices.
			* \param [in]	_pointsCount	The number of polygon vertices.
			* \param [out]	_outResult		The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			* \retval GReturn::FAILURE The calculation failed.
			*/
			static GReturn TestPointToConvexPolygonF(const MATH::GVECTORF _point, const MATH::GVECTORF* _polygonPoints, const unsigned int _pointsCount, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a line to a point.
			/*!
			* Computes the closest point on a line to a point by projecting the point onto the line.
			* The out point is the result of this computation.
			*
			* \param [in]	_line	   	The line.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the line.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToLineF(const MATH::GLINEF _line, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest points of two lines.
			/*!
			* Computes the closest points of two lines by determining the minimum distance that could exist between
			* the two line segments which results in the two closest points from one line to the other.
			* The first out point is the point closest to the second line clamped to the first line.
			* The second out point is the point closest to the first line clamped to the second line.
			*
			* \param [in]	_line1	   	The first line.
			* \param [in]	_line2		The second line.
			* \param [out]	_outPoint1  The first out point that lies on the first line.
			* \param [out]	_outPoint2  The second out point that lies on the second line.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointsToLineFromLineF(const MATH::GLINEF _line1, const MATH::GLINEF _line2, MATH::GVECTORF& _outPoint1, MATH::GVECTORF& _outPoint2) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a ray to a point.
			/*!
			* Computes the closest point on a ray by projecting the point onto the ray's path.
			* The out point is the result of this computation.
			*
			* \param [in]	_ray	   	The ray.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the ray's path.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToRayF(const MATH::GRAYF _ray, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a triangle to a point.
			/*!
			* Computes the closest point on a triangle to a point by determining which region the point is closest to (vertex,
			* edge, or face) and projecting onto that region.
			* The out point is the result of this computation.
			*
			* \param [in]	_triangle  	The triangle.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the triangle.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToTriangleF(const MATH::GTRIANGLEF _triangle, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a plane to a point
			/*!
			* Computes the closest point on a plane to a point by projecting that point onto the plane.
			* The out point is the result of this computation.
			*
			* \param [in]	_plane	   	The plane.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the plane.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToPlaneF(const MATH::GPLANEF _plane, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a sphere's surface to a point
			/*!
			* Computes the closest point on a sphere's surface to a point by getting the normalized direction of the
			* sphere's center to the given point and scaling that by the sphere's radius.
			* The out point is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the query point is the sphere's
			* center which results an infinite amount of valid results that cannot be represented in a single out point.
			*
			* \param [in]	_sphere	   	The sphere.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the sphere's surface.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToSphereF(const MATH::GSPHEREF _sphere, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a capsule's surface to a point
			/*!
			* Computes the closest point on a capsule's surface to a point by finding the closest point to the
			* capsule's center line and then computing the closest point to a sphere.
			* The out point is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the query point is on the capsule's
			* center line which results an infinite amount of valid results that cannot be represented in a single out
			* point.
			*
			* \param [in]	_capsule	The capsule.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the capsule's surface.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToCapsuleF(const MATH::GCAPSULEF _capsule, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a min-max AABB to a point.
			/*!
			* Computes the closest point on a min-max AABB to a point by clamping that point to the surface of the
			* AABB unless that point is contained within the AABB.
			* The out point is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the query point is contained within
			* the AABB.
			*
			* \param [in]	_aabb	   	The aabb.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the AABB's surface.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToAABBF(const MATH::GAABBMMF _aabb, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on an OBB to a point.
			/*!
			* Computes the closest point on an OBB to a point by clamping that point to the surface of the OBB.
			* The out point is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the query point is contained within
			* the OBB.
			*
			* \param [in]	_obb	   	The obb.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToOBBF(const MATH::GOBBF _obb, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute sphere from point cloud.
			/*!
			* Computes a sphere from a point cloud by computing a good approximation of the points in the sphere and
			* then iterating through the points to grow the sphere to include all points.
			* The out sphere is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the pointer to the point cloud is
			* NULL or the number of points is less than 2.
			*
			* \param [in]	_pointCloud 	If non-null, the point cloud.
			* \param [in]	_pointsCount	Number of points.
			* \param [out]	_outSphere  	The out sphere.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ComputeSphereFromPointsF(const MATH::GVECTORF* _pointCloud, const unsigned int _pointsCount, MATH::GSPHEREF& _outSphere) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute min-max AABB from point cloud.
			/*!
			* Computes a min-max AABB from point cloud by computing the minimum and maximum values along the x, y, and z
			* from the point cloud.
			* The out AABB is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the pointer to the point cloud is
			* NULL or the number of points is less than 1.
			*
			* \param [in]	_pointCloud 	If non-null, the point cloud.
			* \param [in]	_pointsCount	Number of points.
			* \param [out]	_outAABB		The out min-max AABB.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ComputeAABBFromPointsF(const MATH::GVECTORF* _pointCloud, const unsigned int _pointsCount, MATH::GAABBMMF& _outAABB) { return GReturn::NO_IMPLEMENTATION; }

			//! Test point to line segment.
			/*!
			* Tests a point to a line segment by checking if that point is between the line segment's start and end.
			* If the point lies on the line segment, the out result is GCollisionCheck::COLLISION.
			* If the point does not lie on the line segment, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_line	  	The line.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToLineF(const MATH::GVECTORF _point, const MATH::GLINEF _line, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to ray.
			/*!
			* Tests a point to a ray by checking if that point lies on the ray's path.
			* If the point lies on the ray's path, the out result is GCollisionCheck::COLLISION.
			* If the point does not lie on the ray's path, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_ray	  	The ray.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToRayF(const MATH::GVECTORF _point, const MATH::GRAYF _ray, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to triangle.
			/*!
			* Tests a point to a triangle by checking if the point lies on the surface plane of the triangle's normal and
			* then if the barycentric coordinate's are all positive.
			* If the point lies on the triangle, the out result is GCollisionCheck::COLLISION.
			* If the point does not lie on the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the triangle is degenerate.
			*
			* \param [in]	_point		   	The point.
			* \param [in]	_triangle	   	The triangle.
			* \param [out]	_outResult	   	The out result.
			* \param [out]	_outBarycentric	If non-null, the out barycentric coordinates.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToTriangleF(const MATH::GVECTORF _point, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to a plane.
			/*!
			* Tests a point to a plane by dotting the point and the plane's surface direction and then subtracting the
			* plane's distance from the result of that dot product.
			* If the point lies on the plane, the out result is GCollisionCheck::COLLISION.
			* If the point lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the point lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_plane	  	The plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToPlaneF(const MATH::GVECTORF _point, const MATH::GPLANEF _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to a sphere.
			/*!
			* Test a point to a sphere by checking if the distance between the point and sphere's center is within the
			* sphere's radius.
			* If the point contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the point does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToSphereF(const MATH::GVECTORF _point, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to a capsule.
			/*!
			* Test a point to a capsule by finding the closest point along the capsule's center line segment to the point
			* which reduces the query to a point to sphere test.
			* If the point contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the point does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToCapsuleF(const MATH::GVECTORF _point, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to a center-extent AABB.
			/*!
			* Test a point to a center-extent AABB by checking if it's within the AABB's boundaries along the x, y, and z.
			* If the point contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the point does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToAABBF(const MATH::GVECTORF _point, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to an OBB.
			/*!
			* Test a point to an OBB by checking if that point doesn't need to be clamped to the OBB's boundaries.
			* If the point contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the point does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToOBBF(const MATH::GVECTORF _point, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a line.
			/*!
			* Test a line to a line by computing the shortest distance between the lines.
			* If the first line contacts the second line, the out result is GCollisionCheck::COLLISION.
			* If the first line does not contact the second line, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line1	  	The first line.
			* \param [in]	_line2	  	The second line.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToLineF(const MATH::GLINEF _line1, const MATH::GLINEF _line2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a ray.
			/*!
			* Test a line to a ray by computing the shortest distance between the line and ray's path.
			* If the line contacts the ray's path, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the ray's path, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_ray	  	The ray.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToRayF(const MATH::GLINEF _line, const MATH::GRAYF _ray, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a triangle
			/*!
			* Test a line to a clockwise triangle by computing if the line pierces the triangle. A collision will only be
			* detected if the line is non-coplanar to the triangle and it pierces the from front face (the line's start
			* point lies in front of the triangle and the line's end lies in back of the triangle).
			* If the line contacts the triangle, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			*
			* \param [in]	_line		   	The line.
			* \param [in]	_triangle	   	The triangle.
			* \param [out]	_outResult	   	The out result.
			* \param [out]	_outBarycentric	If non-null, the out barycentric.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToTriangleF(const MATH::GLINEF _line, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a plane.
			/*!
			* Test a line to a plane by computing the interval along the line that intersects the plane.
			* If the line contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the line is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the line lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the line lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_plane	  	The plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToPlaneF(const MATH::GLINEF _line, const MATH::GPLANEF _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a sphere.
			/*!
			* Test a line to a sphere by computing the closest point on that line to the sphere's center and reducing the
			* query to a point to sphere check.
			* If the line contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToSphereF(const MATH::GLINEF _line, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a capsule.
			/*!
			* Test a line to a capsule by finding the closest points of the line to the capsule's center line and reducing
			* the query to a point to sphere check.
			* If the line contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToCapsuleF(const MATH::GLINEF _line, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to an AABB.
			/*!
			* Test a line to an AABB by checking if any section of the line lies within the AABB's bounds.
			* If the line contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToAABBF(const MATH::GLINEF _line, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to an OBB.
			/*!
			* Test a line to an OBB by checking if any section of the line lies within the OBB's bounds.
			* If the line contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToOBBF(const MATH::GLINEF _line, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to a triangle.
			/*!
			* Test a ray to a clockwise triangle by computing if the ray's path intersects the triangle.
			* If the ray's path contacts the triangle, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			*
			* \param [in]	_ray		   	The ray.
			* \param [in]	_triangle	   	The triangle.
			* \param [out]	_outResult	   	The out result.
			* \param [out]	_outBarycentric	If non-null, the out barycentric.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToTriangleF(const MATH::GRAYF _ray, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to a plane.
			/*!
			* Test a ray to a plane by computing if the ray's path pierces the plane.
			* If the ray contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the ray is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the ray lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the ray lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_plane	  	The plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToPlaneF(const MATH::GRAYF _ray, const MATH::GPLANEF _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to a sphere.
			/*!
			* Tests if a collision exists between ray to sphere by using the quadratic formula to determine if there are
			* any points of intersection (roots).
			* If the ray's path contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			* \retval GReturn::FAILURE The calculation failed.
			*/
			static GReturn TestRayToSphereF(const MATH::GRAYF _ray, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to a capsule.
			/*!
			* Test a ray to a capsule by computing the closest points on the ray's path to the capsule's center line and
			* reduces the query to a point to sphere test.
			* If the ray's path contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToCapsuleF(const MATH::GRAYF _ray, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to an AABB.
			/*!
			* Test a ray to an AABB by checking if any section of the ray's path lies within the AABB's bounds.
			* If the ray's path contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToAABBF(const MATH::GRAYF _ray, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to an OBB.
			/*!
			* Test a ray to an OBB by checking if any section of the ray's path lies within the AABB's bounds.
			* If the ray's path contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToOBBF(const MATH::GRAYF _ray, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to a triangle.
			/*!
			* Test a triangle to a triangle by first checking if all either triangle's point lie on one side of the
			* other triangle's face and then brings the triangles into canonical form to check if any of the edges of one
			* triangle intersect the other triangle.
			* If the triangle's contact, the out result is GCollisionCheck::COLLISION.
			* If the triangle's don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle1	The first triangle.
			* \param [in]	_triangle2	The second triangle.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToTriangleF(const MATH::GTRIANGLEF _triangle1, const MATH::GTRIANGLEF _triangle2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to a plane.
			/*!
			* Test a triangle to a plane to compute if all of the triangle's vertices lie completely on one side of the
			* plane.
			* If the triangle contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the triangle is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the triangle lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the triangle lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_plane	  	The plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToPlaneF(const MATH::GTRIANGLEF _triangle, const MATH::GPLANEF _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to a sphere.
			/*!
			* Test a triangle to a sphere by computing the closest point on the triangle to the sphere's center which
			* reduces the query to testing a point to sphere.
			* If the triangle contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the triangle is coplanar to the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToSphereF(const MATH::GTRIANGLEF _triangle, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to a capsule.
			/*!
			* Test a triangle to capsule by computing a line on the triangle closest to the capsule's center line which
			* reduces the query to test a line to capsule.
			* If the triangle contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the triangle does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToCapsuleF(const MATH::GTRIANGLEF _triangle, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to an AABB.
			/*!
			* Test a triangle to AABB by using the separating axis theorem which involves 13 axis: 3 face normals of the
			* AABB, 1 face normal of the triangle, 9 cross products between the their edges.
			* If the triangle contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the triangle does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToAABBF(const MATH::GTRIANGLEF _triangle, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to an OBB.
			/*!
			* Test a triangle to an OBB by bringing the triangle into the OBB's space and performing a triangle to AABB
			* test.
			* If the triangle contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the triangle does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToOBBF(const MATH::GTRIANGLEF _triangle, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to a plane.
			/*!
			* Test a plane to a plane by computing the whether the two planes are not parallel to each other.
			* If the planes contact, the out result is GCollisionCheck::COLLISION.
			* If the planes don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_plane1   	The first plane.
			* \param [in]	_plane2   	The second plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToPlaneF(const MATH::GPLANEF _plane1, const MATH::GPLANEF _plane2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to a sphere.
			/*!
			* Test a plane to a sphere by computing the closest point on the plane to the sphere and perform a point
			* to sphere test.
			* If the plane contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the sphere lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the sphere lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_plane	  	The plane.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToSphereF(const MATH::GPLANEF _plane, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to a capsule.
			/*!
			* Test a plane to a capsule by computing the closest points on the plane to the capsule's start and end points
			* and then perform a line to capsule test.
			* If the plane contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the capsule lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the capsule lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_plane	  	The plane.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToCapsuleF(const MATH::GPLANEF _plane, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to an AABB.
			/*!
			* Test a plane to an AABB by using the separating axis theorem.
			* If the plane contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the AABB lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the AABB lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_plane	  	The plane.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToAABBF(const MATH::GPLANEF _plane, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to an OBB.
			/*!
			* Test a plane to an OBB by using the separating axis theorem.
			* If the plane contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the OBB lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the OBB lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_plane	  	The plane.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToOBBF(const MATH::GPLANEF _plane, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a sphere to sphere.
			/*!
			* Test a sphere to sphere by checking the distance between their centers and the
			* sum of their radii.
			* If the spheres contact, the out result is GCollisionCheck::COLLISION.
			* If the spheres do not contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere1  	The first sphere.
			* \param [in]	_sphere2  	The second sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestSphereToSphereF(const MATH::GSPHEREF _sphere1, const MATH::GSPHEREF _sphere2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a sphere to capsule.
			/*!
			* Test a sphere to capsule by checking the distance between each's center structures
			* and the sum of their radii.
			* If the sphere contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere   	The sphere.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestSphereToCapsuleF(const MATH::GSPHEREF _sphere, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a sphere to an AABB.
			/*!
			* Test a sphere to an AABB by computing the distance from the AABB's boundary to the sphere's center and
			* computing if that distance is within the sphere's radius.
			* If the sphere contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere   	The sphere.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestSphereToAABBF(const MATH::GSPHEREF _sphere, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a sphere to an OBB.
			/*!
			* Test a sphere to an OBB by computing the distance from the OBB's boundary to the sphere's center and
			* computing if that distance is within the sphere's radius.
			* If the sphere contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere   	The sphere.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestSphereToOBBF(const MATH::GSPHEREF _sphere, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a capsule to a capsule.
			/*!
			* Test a capsule to another capsule by computing the closest points on each capsule's center line and
			* performing a sphere to sphere test.
			* If the capsules contact, the out result is GCollisionCheck::COLLISION.
			* If the capsules don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule1 	The first capsule.
			* \param [in]	_capsule2 	The second capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestCapsuleToCapsuleF(const MATH::GCAPSULEF _capsule1, const MATH::GCAPSULEF _capsule2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a capsule to an AABB.
			/*!
			* Test a capsule to an AABB by increasing the AABB's extents by the capsule's radius and then performing a
			* ray to AABB test.
			* If the capsule contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the capsule does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule  	The capsule.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestCapsuleToAABBF(const MATH::GCAPSULEF _capsule, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a capsule to an OBB.
			/*!
			* Test a capsule to an OBB by bringing the capsule into the OBB's space and performing a capsule to AABB test.
			* If the capsule contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the capsule does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule  	The capsule.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestCapsuleToOBBF(const MATH::GCAPSULEF _capsule, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test an AABB to an AABB.
			/*!
			* Test an AABB to an AABB by checking if they overlap on all three axes.
			* If the AABBs contact, the out result is GCollisionCheck::COLLISION.
			* If the AABBs don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_aabb1	  	The first aabb.
			* \param [in]	_aabb2	  	The second aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestAABBToAABBF(const MATH::GAABBCEF _aabb1, const MATH::GAABBCEF _aabb2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test an AABB to an OBB.
			/*!
			* Test an AABB to OBB performing an OBB to OBB test.
			* If the AABB contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the AABB does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_aabb	  	The aabb.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestAABBToOBBF(const MATH::GAABBCEF _aabb, const MATH::GOBBF _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Tests OBB to OBB.
			/*!
			* Tests if a collision exists between OBB to OBB by checking if the sum of their projected radii is less
			* than the distance between their projected centers meaning a separating axis exists. The result will be
			* stored in _outResult as either GCollisionCheck::NO_COLLISION or GCollisionCheck::COLLISION.
			* If the OBBs contact, the out result is GCollisionCheck::COLLISION.
			* If the OBBs don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_obb1	  	The first OBB.
			* \param [in]	_obb2	  	The second OBB.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestOBBToOBBF(const MATH::GOBBF _obb1, const MATH::GOBBF _obb2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to triangle.
			/*!
			* Test a line to a clockwise triangle by computing if the line pierces the triangle. A collision will only be
			* detected if the line is non-coplanar to the triangle and it pierces the from front face (the line's start
			* point lies in front of the triangle and the line's end lies in back of the triangle). Provides the point
			* of contact along with the normalized direction and interval along that direction the contact occurs.
			* If the line contacts the triangle, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			*
			* \param [in]	_line				The line.
			* \param [in]	_triangle			The triangle.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outBarycentric 	If non-null, the out barycentric.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToTriangleF(const MATH::GLINEF _line, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF* _outBarycentric, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to plane.
			/*!
			* Test a line to a plane by computing the interval along the line that intersects the plane. Provides the point
			* of contact along with the normalized direction and interval along that direction the contact occurs.
			* If the line contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the line is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the line lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the line lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_line				The line.
			* \param [in]	_plane				The plane.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToPlaneF(const MATH::GLINEF _line, const MATH::GPLANEF _plane, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to sphere.
			/*!
			* Test a line to a sphere by computing the closest point on that line to the sphere's center and reducing the
			* query to a point to sphere check. Provides the point of contact along with the normalized direction
			* and interval along that direction the contact occurs.
			* If the line contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line				The line.
			* \param [in]	_sphere				The sphere.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToSphereF(const MATH::GLINEF _line, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to capsule.
			/*!
			* Test a line to a capsule by finding the closest points of the line to the capsule's center line and reducing
			* the query to a point to sphere check. Provides the point of contact along with the normalized direction
			* and interval along that direction the contact occurs.
			* If the line contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line				The line.
			* \param [in]	_capsule			The capsule.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToCapsuleF(const MATH::GLINEF _line, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to AABB.
			/*!
			* Test a line to an AABB by checking if any section of the line lies within the AABB's bounds. Provides
			* the point of contact along with the normalized direction and interval along that direction the
			* contact occurs.
			* If the line contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur when the line degenerates to a point.
			*
			* \param [in]	_line				The line.
			* \param [in]	_aabb				The aabb.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToAABBF(const MATH::GLINEF _line, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to OBB.
			/*!
			* Test a line to an OBB by checking if any section of the line lies within the OBB's bounds. Provides
			* the point of contact along with the normalized direction and interval along that direction the
			* contact occurs.
			* If the line contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur when the line degenerates to a point.
			*
			* \param [in]	_line				The line.
			* \param [in]	_obb				The obb.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToOBBF(const MATH::GLINEF _line, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to triangle.
			/*!
			* Test a ray to a clockwise triangle by computing if the ray's path intersects the triangle. Provides the
			* point of contact along with the interval the contact occurs on the ray.
			* If the ray's path contacts the triangle, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_triangle			The triangle.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outBarycentric 	If non-null, the out barycentric.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToTriangleF(const MATH::GRAYF _ray, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF* _outBarycentric, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to plane.
			/*!
			* Test a ray to a plane by computing if the ray's path pierces the plane. Provides the point of contact along
			* with the interval the contact occurs on the ray.
			* If the ray contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the ray is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the ray lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the ray lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_plane				The plane.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToPlaneF(const MATH::GRAYF _ray, const MATH::GPLANEF _plane, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to sphere.
			/*!
			* Tests if a collision exists between ray to sphere by using the quadratic formula to determine if there are
			* any points of intersection (roots). Provides the point of contact along with the interval the contact
			* occurs on the ray.
			* If the ray's path contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_sphere				The sphere.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToSphereF(const MATH::GRAYF _ray, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to capsule.
			/*!
			* Test a ray to a capsule by computing the closest points on the ray's path to the capsule's center line and
			* reduces the query to a point to sphere test. Provides the point of contact along with the interval the
			* contact occurs on the ray.
			* If the ray's path contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_capsule			The capsule.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToCapsuleF(const MATH::GRAYF _ray, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to AABB.
			/*!
			* Test a ray to an AABB by checking if any section of the ray's path lies within the AABB's bounds. Provides
			* the point of contact along with the interval the contact occurs on the ray.
			* If the ray's path contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_aabb				The aabb.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToAABBF(const MATH::GRAYF _ray, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to OBB.
			/*!
			* Test a ray to an OBB by checking if any section of the ray's path lies within the AABB's bounds. Provides
			* the point of contact along with the interval the contact occurs on the ray.
			* If the ray's path contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_obb				The obb.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToOBBF(const MATH::GRAYF _ray, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect sphere to sphere.
			/*!
			* Test a sphere to sphere by checking the distance between their centers and the
			* sum of their radii. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the spheres contact, the out result is GCollisionCheck::COLLISION.
			* If the spheres do not contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere1		   	The first sphere.
			* \param [in]	_sphere2		   	The second sphere.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance		The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectSphereToSphereF(const MATH::GSPHEREF _sphere1, const MATH::GSPHEREF _sphere2, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect sphere to capsule.
			/*!
			* Test a sphere to capsule by checking the distance between each's center structures
			* and the sum of their radii. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the sphere contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere			   	The sphere.
			* \param [in]	_capsule		   	The capsule.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectSphereToCapsuleF(const MATH::GSPHEREF _sphere, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect sphere to AABB.
			/*!
			* Test a sphere to an AABB by computing the distance from the AABB's boundary to the sphere's center and
			* computing if that distance is within the sphere's radius. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the sphere contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere			   	The sphere.
			* \param [in]	_aabb			   	The aabb.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectSphereToAABBF(const MATH::GSPHEREF _sphere, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect sphere to OBB.
			/*!
			* Test a sphere to an OBB by computing the distance from the OBB's boundary to the sphere's center and
			* computing if that distance is within the sphere's radius. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the sphere contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere			   	The sphere.
			* \param [in]	_obb			   	The obb.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectSphereToOBBF(const MATH::GSPHEREF _sphere, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect capsule to capsule.
			/*!
			* Test a capsule to another capsule by computing the closest points on each capsule's center line and
			* performing a sphere to sphere test. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the capsules contact, the out result is GCollisionCheck::COLLISION.
			* If the capsules don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule1		   	The first capsule.
			* \param [in]	_capsule2		   	The second capsule.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectCapsuleToCapsuleF(const MATH::GCAPSULEF _capsule1, const MATH::GCAPSULEF _capsule2, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect capsule to AABB.
			/*!
			* Test a capsule to an AABB by increasing the AABB's extents by the capsule's radius and then performing a
			* ray to AABB test. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the capsule contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the capsule does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule		   	The capsule.
			* \param [in]	_aabb			   	The aabb.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectCapsuleToAABBF(const MATH::GCAPSULEF _capsule, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect capsule to OBB.
			/*!
			* Test a capsule to an OBB by bringing the capsule into the OBB's space and performing a capsule to AABB test.
			* Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the capsule contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the capsule does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule		   	The capsule.
			* \param [in]	_obb			   	The obb.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectCapsuleToOBBF(const MATH::GCAPSULEF _capsule, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect AABB to AABB.
			/*!
			* Test an AABB to an AABB by checking if they overlap on all three axes. Provides the minimal translation
			* information: two points on each shape's surface,the direction the two points form, and distance required to
			* translate the second shape to where the surfaces barely contact.
			* If the AABBs contact, the out result is GCollisionCheck::COLLISION.
			* If the AABBs don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_aabb1		   	The first aabb.
			* \param [in]	_aabb2		   	The second aabb.
			* \param [out]	_outResult	   	The out result.
			* \param [out]	_outContactAABB	The out contact a bb.
			* \param [out]	_outDirection  	The out direction.
			* \param [out]	_outDistance   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectAABBToAABBF(const MATH::GAABBCEF _aabb1, const MATH::GAABBCEF _aabb2, GCollisionCheck& _outResult, MATH::GAABBCEF& _outContactAABB, MATH::GVECTORF& _outDirection, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to line.
			/*!
			* Squared separation distance between point to line.
			*
			* \param [in]	_point			The point.
			* \param [in]	_line			The line.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToLineF(const MATH::GVECTORF _point, const MATH::GLINEF _line, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to ray.
			/*!
			* Squared separation distance between point to ray.
			*
			* \param [in]	_point			The point.
			* \param [in]	_ray			The ray.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToRayF(const MATH::GVECTORF _point, const MATH::GRAYF _ray, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to triangle.
			/*!
			* Squared separation distance between point to triangle.
			*
			* \param [in]	_point			The point.
			* \param [in]	_triangle   	The triangle.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToTriangleF(const MATH::GVECTORF _point, const MATH::GTRIANGLEF _triangle, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to plane.
			/*!
			* Squared separation distance between point to plane.
			*
			* \param [in]	_point			The point.
			* \param [in]	_plane			The plane.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToPlaneF(const MATH::GVECTORF _point, const MATH::GPLANEF _plane, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to sphere.
			/*!
			* Squared separation distance between point to sphere.
			*
			* \param [in]	_point			The point.
			* \param [in]	_sphere			The sphere.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToSphereF(const MATH::GVECTORF _point, const MATH::GSPHEREF _sphere, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to capsule.
			/*!
			* Squared separation distance between point to capsule.
			*
			* \param [in]	_point			The point.
			* \param [in]	_capsule		The capsule.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToCapsuleF(const MATH::GVECTORF _point, const MATH::GCAPSULEF _capsule, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to AABB.
			/*!
			* Squared separation distance between point to a AABB.
			*
			* \param [in]	_point			The point.
			* \param [in]	_aabb			The aabb.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToAABBF(const MATH::GVECTORF _point, const MATH::GAABBMMF _aabb, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to OBB.
			/*!
			* Squared separation distance between point to OBB.
			*
			* \param [in]	_point			The point.
			* \param [in]	_obb			The obb.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToOBBF(const MATH::GVECTORF _point, const MATH::GOBBF _obb, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute the barycentric coordinates of a point to triangle.
			/*!
			* Compute the barycentric coordinates given three points of a triangle with a point in respect to that triangle.
			* Both triangle and point must be coplanar. The result is stored in a vector representing the barycentric coords.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur when the triangle is degenerate.
			*
			* \param [in] 	_a			   	The first point of a triangle.
			* \param [in] 	_b			   	The second point of a triangle.
			* \param [in] 	_c			   	The third point of a triangle.
			* \param [in] 	_p			   	The point coplanar with the triangle points.
			* \param [out]	_outBarycentric	The out barycentric coordinates.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn BarycentricF(const MATH::GVECTORF _a, const MATH::GVECTORF _b, const MATH::GVECTORF _c, const MATH::GVECTORF _p, MATH::GVECTORF& _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }























			//! Convert AABBCE to AABBMM.
			/*!
			* Computes the min-max representation of an AABB from a center-extent AABB representation.
			*
			* \param [in]	_aabbCE		The center-extent AABB.
			* \param [out]	_outAABBMM	The out min-max AABB.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ConvertAABBCEToAABBMMD(const MATH::GAABBCED _aabbCE, MATH::GAABBMMD& _outAABBMM) { return GReturn::NO_IMPLEMENTATION; }

			//! Convert AABBCE to AABBMM.
			/*!
			* Computes the center-extent representation of an AABB from a min-max AABB representation.
			*
			* \param [in]	_aabbMM		The min-max AABB.
			* \param [out]	_outAABBCE	The out center-extent AABB.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ConvertAABBMMToAABBCED(const MATH::GAABBMMD _aabbMM, MATH::GAABBCED& _outAABBCE) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the plane given three points(clockwise).
			/*!
			* Calculates the plane given three points(clockwise) by taking the perpendicular vector that would intersect
			* a triangle that the three points represent. The plane is defined as a normal and distance from the origin.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur when the given plane's position points
			* are collinear.
			*
			* \param [in]	_planePositionA	The plane position a.
			* \param [in]	_planePositionB	The plane position b.
			* \param [in]	_planePositionC	The plane position c.
			* \param [out]	_outPlane	   	The out plane.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ComputePlaneD(const MATH::GVECTORD _planePositionA, const MATH::GVECTORD _planePositionB, const MATH::GVECTORD _planePositionC, MATH::GPLANED& _outPlane) { return GReturn::NO_IMPLEMENTATION; }

			//! Tests whether a triangle is non-degenerate.
			/*!
			* Tests whether a triangle is non-degenerate. A non-degenerate triangle has an area greater than 0 and is not
			* a line.
			* If the triangle is non-degenerate, the out result is 1.
			* If the triangle is degenerate, the out result is 0.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IsTriangleD(const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Tests whether a point lies within a polygon or not.
			/*!
			* Tests whether a point is contained within a polygon by counting how many times a line that extends to
			* infinity intersects the sides of the polygon.
			* If the point is contained within the polygon, the out result is 1. This does not include the polygon's
			* boundary.
			* If the point is not contained within the polygon, the out result is 0.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the pointer to the polygon's points
			* is null or if the point count is less than 3.
			*
			* \param [in]	_point			The query point.
			* \param [in]	_polygonPoints	If non-null, the set of points representing the polygon vertices.
			* \param [in]	_pointsCount	The number of polygon vertices.
			* \param [out]	_outResult		The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToConvexPolygonD(const MATH::GVECTORD _point, const MATH::GVECTORD* _polygonPoints, const unsigned int _pointsCount, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a line to a point.
			/*!
			* Computes the closest point on a line to a point by projecting the point onto the line.
			* The out point is the result of this computation.
			*
			* \param [in]	_line	   	The line.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the line.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToLineD(const MATH::GLINED _line, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest points of two lines.
			/*!
			* Computes the closest points of two lines by determining the minimum distance that could exist between
			* the two line segments which results in the two closest points from one line to the other.
			* The first out point is the point closest to the second line clamped to the first line.
			* The second out point is the point closest to the first line clamped to the second line.
			*
			* \param [in]	_line1	   	The first line.
			* \param [in]	_line2		The second line.
			* \param [out]	_outPoint1  The first out point that lies on the first line.
			* \param [out]	_outPoint2  The second out point that lies on the second line.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointsToLineFromLineD(const MATH::GLINED _line1, const MATH::GLINED _line2, MATH::GVECTORD& _outPoint1, MATH::GVECTORD& _outPoint2) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a ray to a point.
			/*!
			* Computes the closest point on a ray by projecting the point onto the ray's path.
			* The out point is the result of this computation.
			*
			* \param [in]	_ray	   	The ray.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the ray's path.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToRayD(const MATH::GRAYD _ray, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a triangle to a point.
			/*!
			* Computes the closest point on a triangle to a point by determining which region the point is closest to (vertex,
			* edge, or face) and projecting onto that region.
			* The out point is the result of this computation.
			*
			* \param [in]	_triangle  	The triangle.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the triangle.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToTriangleD(const MATH::GTRIANGLED _triangle, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a plane to a point.
			/*!
			* Computes the closest point on a plane to a point by projecting that point onto the plane.
			* The out point is the result of this computation.
			*
			* \param [in]	_plane	   	The plane.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the plane.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToPlaneD(const MATH::GPLANED _plane, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a sphere's surface to a point.
			/*!
			* Computes the closest point on a sphere's surface to a point by getting the normalized direction of the
			* sphere's center to the given point and scaling that by the sphere's radius.
			* The out point is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the query point is the sphere's
			* center which results an infinite amount of valid results that cannot be represented in a single out point.
			*
			* \param [in]	_sphere	   	The sphere.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the sphere's surface.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToSphereD(const MATH::GSPHERED _sphere, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a capsule's surface to a point.
			/*!
			* Computes the closest point on a capsule's surface to a point by finding the closest point to the
			* capsule's center line and then computing the closest point to a sphere.
			* The out point is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the query point is on the capsule's
			* center line which results an infinite amount of valid results that cannot be represented in a single out
			* point.
			*
			* \param [in]	_capsule	The capsule.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the capsule's surface.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToCapsuleD(const MATH::GCAPSULED _capsule, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on a min-max AABB to a point.
			/*!
			* Computes the closest point on a min-max AABB to a point by clamping that point to the surface of the
			* AABB unless that point is contained within the AABB.
			* The out point is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the query point is contained within
			* the AABB.
			*
			* \param [in]	_aabb	   	The aabb.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point that lies on the AABB's surface.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToAABBD(const MATH::GAABBMMD _aabb, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Computes the closest point on an OBB to a point.
			/*!
			* Computes the closest point on an OBB to a point by clamping that point to the surface of the OBB.
			* The out point is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the query point is contained within
			* the OBB.
			*
			* \param [in]	_obb	   	The obb.
			* \param [in]	_queryPoint	The query point.
			* \param [out]	_outPoint  	The out point.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ClosestPointToOBBD(const MATH::GOBBD _obb, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute sphere from point cloud.
			/*!
			* Computes a sphere from a point cloud by computing a good approximation of the points in the sphere and
			* then iterating through the points to grow the sphere to include all points.
			* The out sphere is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the pointer to the point cloud is
			* NULL or the number of points is less than 2.
			*
			* \param [in]	_pointCloud 	If non-null, the point cloud.
			* \param [in]	_pointsCount	Number of points.
			* \param [out]	_outSphere  	The out sphere.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ComputeSphereFromPointsD(const MATH::GVECTORD* _pointCloud, const unsigned int _pointsCount, MATH::GSPHERED& _outSphere) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute min-max AABB from point cloud.
			/*!
			* Computes a min-max AABB from point cloud by computing the minimum and maximum values along the x, y, and z
			* from the point cloud.
			* The out AABB is the result of this computation.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the pointer to the point cloud is
			* NULL or the number of points is less than 1.
			*
			* \param [in]	_pointCloud 	If non-null, the point cloud.
			* \param [in]	_pointsCount	Number of points.
			* \param [out]	_outAABB		The out min-max AABB.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ComputeAABBFromPointsD(const MATH::GVECTORD* _pointCloud, const unsigned int _pointsCount, MATH::GAABBMMD& _outAABB) { return GReturn::NO_IMPLEMENTATION; }

			//! Test point to line segment.
			/*!
			* Tests a point to a line segment by checking if that point is between the line segment's start and end.
			* If the point lies on the line segment, the out result is GCollisionCheck::COLLISION.
			* If the point does not lie on the line segment, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_line	  	The line.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToLineD(const MATH::GVECTORD _point, const MATH::GLINED _line, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to ray.
			/*!
			* Tests a point to a ray by checking if that point lies on the ray's path.
			* If the point lies on the ray's path, the out result is GCollisionCheck::COLLISION.
			* If the point does not lie on the ray's path, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_ray	  	The ray.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToRayD(const MATH::GVECTORD _point, const MATH::GRAYD _ray, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to triangle.
			/*!
			* Tests a point to a triangle by checking if the point lies on the surface plane of the triangle's normal and
			* then if the barycentric coordinate's are all positive.
			* If the point lies on the triangle, the out result is GCollisionCheck::COLLISION.
			* If the point does not lie on the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur if the triangle is degenerate.
			*
			* \param [in]	_point		   	The point.
			* \param [in]	_triangle	   	The triangle.
			* \param [out]	_outResult	   	The out result.
			* \param [out]	_outBarycentric	If non-null, the out barycentric coordinates.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToTriangleD(const MATH::GVECTORD _point, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to a plane.
			/*!
			* Tests a point to a plane by dotting the point and the plane's surface direction and then subtracting the
			* plane's distance from the result of that dot product.
			* If the point lies on the plane, the out result is GCollisionCheck::COLLISION.
			* If the point lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the point lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_plane	  	The plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToPlaneD(const MATH::GVECTORD _point, const MATH::GPLANED _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to a sphere.
			/*!
			* Test a point to a sphere by checking if the distance between the point and sphere's center is within the
			* sphere's radius.
			* If the point contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the point does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToSphereD(const MATH::GVECTORD _point, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to a capsule.
			/*!
			* Test a point to a capsule by finding the closest point along the capsule's center line segment to the point
			* which reduces the query to a point to sphere test.
			* If the point contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the point does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToCapsuleD(const MATH::GVECTORD _point, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to a center-extent AABB.
			/*!
			* Test a point to a center-extent AABB by checking if it's within the AABB's boundaries along the x, y, and z.
			* If the point contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the point does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToAABBD(const MATH::GVECTORD _point, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a point to an OBB.
			/*!
			* Test a point to an OBB by checking if that point doesn't need to be clamped to the OBB's boundaries.
			* If the point contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the point does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_point	  	The point.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToOBBD(const MATH::GVECTORD _point, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a line.
			/*!
			* Test a line to a line by computing the shortest distance between the lines.
			* If the first line contacts the second line, the out result is GCollisionCheck::COLLISION.
			* If the first line does not contact the second line, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line1	  	The first line.
			* \param [in]	_line2	  	The second line.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToLineD(const MATH::GLINED _line1, const MATH::GLINED _line2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a ray.
			/*!
			* Test a line to a ray by computing the shortest distance between the line and ray's path.
			* If the line contacts the ray's path, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the ray's path, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_ray	  	The ray.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToRayD(const MATH::GLINED _line, const MATH::GRAYD _ray, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a triangle
			/*!
			* Test a line to a clockwise triangle by computing if the line pierces the triangle. A collision will only be
			* detected if the line is non-coplanar to the triangle and it pierces the from front face (the line's start
			* point lies in front of the triangle and the line's end lies in back of the triangle).
			* If the line contacts the triangle, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			*
			* \param [in]	_line		   	The line.
			* \param [in]	_triangle	   	The triangle.
			* \param [out]	_outResult	   	The out result.
			* \param [out]	_outBarycentric	If non-null, the out barycentric.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToTriangleD(const MATH::GLINED _line, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a plane.
			/*!
			* Test a line to a plane by computing the interval along the line that intersects the plane.
			* If the line contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the line is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the line lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the line lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_plane	  	The plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToPlaneD(const MATH::GLINED _line, const MATH::GPLANED _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a sphere.
			/*!
			* Test a line to a sphere by computing the closest point on that line to the sphere's center and reducing the
			* query to a point to sphere check.
			* If the line contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToSphereD(const MATH::GLINED _line, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to a capsule.
			/*!
			* Test a line to a capsule by finding the closest points of the line to the capsule's center line and reducing
			* the query to a point to sphere check.
			* If the line contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToCapsuleD(const MATH::GLINED _line, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to an AABB.
			/*!
			* Test a line to an AABB by checking if any section of the line lies within the AABB's bounds.
			* If the line contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToAABBD(const MATH::GLINED _line, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a line to an OBB.
			/*!
			* Test a line to an OBB by checking if any section of the line lies within the OBB's bounds.
			* If the line contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line	  	The line.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToOBBD(const MATH::GLINED _line, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to a triangle.
			/*!
			* Test a ray to a clockwise triangle by computing if the ray's path intersects the triangle.
			* If the ray's path contacts the triangle, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			*
			* \param [in]	_ray		   	The ray.
			* \param [in]	_triangle	   	The triangle.
			* \param [out]	_outResult	   	The out result.
			* \param [out]	_outBarycentric	If non-null, the out barycentric.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToTriangleD(const MATH::GRAYD _ray, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to a plane.
			/*!
			* Test a ray to a plane by computing if the ray's path pierces the plane.
			* If the ray contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the ray is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the ray lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the ray lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_plane	  	The plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToPlaneD(const MATH::GRAYD _ray, const MATH::GPLANED _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to a sphere.
			/*!
			* Tests if a collision exists between ray to sphere by using the quadratic formula to determine if there are
			* any points of intersection (roots).
			* If the ray's path contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToSphereD(const MATH::GRAYD _ray, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to a capsule.
			/*!
			* Test a ray to a capsule by computing the closest points on the ray's path to the capsule's center line and
			* reduces the query to a point to sphere test.
			* If the ray's path contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToCapsuleD(const MATH::GRAYD _ray, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to an AABB.
			/*!
			* Test a ray to an AABB by checking if any section of the ray's path lies within the AABB's bounds.
			* If the ray's path contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToAABBD(const MATH::GRAYD _ray, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a ray to an OBB.
			/*!
			* Test a ray to an OBB by checking if any section of the ray's path lies within the AABB's bounds.
			* If the ray's path contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray	  	The ray.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRayToOBBD(const MATH::GRAYD _ray, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to a triangle.
			/*!
			* Test a triangle to a triangle by first checking if all either triangle's point lie on one side of the
			* other triangle's face and then brings the triangles into canonical form to check if any of the edges of one
			* triangle intersect the other triangle.
			* If the triangle's contact, the out result is GCollisionCheck::COLLISION.
			* If the triangle's don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle1	The first triangle.
			* \param [in]	_triangle2	The second triangle.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToTriangleD(const MATH::GTRIANGLED _triangle1, const MATH::GTRIANGLED _triangle2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to a plane.
			/*!
			* Test a triangle to a plane to compute if all of the triangle's vertices lie completely on one side of the
			* plane.
			* If the triangle contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the triangle is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the triangle lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the triangle lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_plane	  	The plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToPlaneD(const MATH::GTRIANGLED _triangle, const MATH::GPLANED _plane, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to a sphere.
			/*!
			* Test a triangle to a sphere by computing the closest point on the triangle to the sphere's center which
			* reduces the query to testing a point to sphere.
			* If the triangle contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the triangle is coplanar to the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToSphereD(const MATH::GTRIANGLED _triangle, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to a capsule.
			/*!
			* Test a triangle to capsule by computing a line on the triangle closest to the capsule's center line which
			* reduces the query to test a line to capsule.
			* If the triangle contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the triangle does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToCapsuleD(const MATH::GTRIANGLED _triangle, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to an AABB.
			/*!
			* Test a triangle to AABB by using the separating axis theorem which involves 13 axis: 3 face normals of the
			* AABB, 1 face normal of the triangle, 9 cross products between the their edges.
			* If the triangle contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the triangle does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToAABBD(const MATH::GTRIANGLED _triangle, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a triangle to an OBB.
			/*!
			* Test a triangle to an OBB by bringing the triangle into the OBB's space and performing a triangle to AABB
			* test.
			* If the triangle contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the triangle does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_triangle 	The triangle.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestTriangleToOBBD(const MATH::GTRIANGLED _triangle, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to a plane.
			/*!
			* Test a plane to a plane by computing the whether the two planes are not parallel to each other.
			* If the planes contact, the out result is GCollisionCheck::COLLISION.
			* If the planes don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_plane1   	The first plane.
			* \param [in]	_plane2   	The second plane.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToPlaneD(const MATH::GPLANED _plane1, const MATH::GPLANED _plane2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to a sphere.
			/*!
			* Test a plane to a sphere by computing the closest point on the plane to the sphere and perform a point
			* to sphere test.
			* If the plane contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the sphere lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the sphere lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_plane	  	The plane.
			* \param [in]	_sphere   	The sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToSphereD(const MATH::GPLANED _plane, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to a capsule.
			/*!
			* Test a plane to a capsule by computing the closest points on the plane to the capsule's start and end points
			* and then perform a line to capsule test.
			* If the plane contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the capsule lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the capsule lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_plane	  	The plane.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToCapsuleD(const MATH::GPLANED _plane, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to an AABB.
			/*!
			* Test a plane to an AABB by using the separating axis theorem.
			* If the plane contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the AABB lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the AABB lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_plane	  	The plane.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToAABBD(const MATH::GPLANED _plane, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a plane to an OBB.
			/*!
			* Test a plane to an OBB by using the separating axis theorem.
			* If the plane contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the OBB lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the OBB lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_plane	  	The plane.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPlaneToOBBD(const MATH::GPLANED _plane, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a sphere to sphere
			/*!
			* Test a sphere to sphere by checking the distance between their centers and the
			* sum of their radii.
			* If the spheres contact, the out result is GCollisionCheck::COLLISION.
			* If the spheres do not contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere1  	The first sphere.
			* \param [in]	_sphere2  	The second sphere.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestSphereToSphereD(const MATH::GSPHERED _sphere1, const MATH::GSPHERED _sphere2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a sphere to capsule
			/*!
			* Test a sphere to capsule by checking the distance between each's center structures
			* and the sum of their radii.
			* If the sphere contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere   	The sphere.
			* \param [in]	_capsule  	The capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestSphereToCapsuleD(const MATH::GSPHERED _sphere, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a sphere to an AABB.
			/*!
			* Test a sphere to an AABB by computing the distance from the AABB's boundary to the sphere's center and
			* computing if that distance is within the sphere's radius.
			* If the sphere contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere   	The sphere.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestSphereToAABBD(const MATH::GSPHERED _sphere, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a sphere to an OBB.
			/*!
			* Test a sphere to an OBB by computing the distance from the OBB's boundary to the sphere's center and
			* computing if that distance is within the sphere's radius.
			* If the sphere contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere   	The sphere.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestSphereToOBBD(const MATH::GSPHERED _sphere, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a capsule to a capsule.
			/*!
			* Test a capsule to another capsule by computing the closest points on each capsule's center line and
			* performing a sphere to sphere test.
			* If the capsules contact, the out result is GCollisionCheck::COLLISION.
			* If the capsules don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule1 	The first capsule.
			* \param [in]	_capsule2 	The second capsule.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestCapsuleToCapsuleD(const MATH::GCAPSULED _capsule1, const MATH::GCAPSULED _capsule2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a capsule to an AABB.
			/*!
			* Test a capsule to an AABB by increasing the AABB's extents by the capsule's radius and then performing a
			* ray to AABB test.
			* If the capsule contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the capsule does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule  	The capsule.
			* \param [in]	_aabb	  	The aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestCapsuleToAABBD(const MATH::GCAPSULED _capsule, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test a capsule to an OBB.
			/*!
			* Test a capsule to an OBB by bringing the capsule into the OBB's space and performing a capsule to AABB test.
			* If the capsule contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the capsule does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule  	The capsule.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestCapsuleToOBBD(const MATH::GCAPSULED _capsule, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test an AABB to an AABB
			/*!
			* Test an AABB to an AABB by checking if they overlap on all three axes.
			* If the AABBs contact, the out result is GCollisionCheck::COLLISION.
			* If the AABBs don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_aabb1	  	The first aabb.
			* \param [in]	_aabb2	  	The second aabb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestAABBToAABBD(const MATH::GAABBCED _aabb1, const MATH::GAABBCED _aabb2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Test an AABB to an OBB.
			/*!
			* Test an AABB to OBB performing an OBB to OBB test.
			* If the AABB contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the AABB does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_aabb	  	The aabb.
			* \param [in]	_obb	  	The obb.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestAABBToOBBD(const MATH::GAABBCED _aabb, const MATH::GOBBD _obb, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Tests OBB to OBB
			/*!
			* Tests if a collision exists between OBB to OBB by checking if the sum of their projected radii is less
			* than the distance between their projected centers meaning a separating axis exists. The result will be
			* stored in _outResult as either GCollisionCheck::NO_COLLISION or GCollisionCheck::COLLISION.
			* If the OBBs contact, the out result is GCollisionCheck::COLLISION.
			* If the OBBs don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_obb1	  	The first OBB.
			* \param [in]	_obb2	  	The second OBB.
			* \param [out]	_outResult	The out result.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestOBBToOBBD(const MATH::GOBBD _obb1, const MATH::GOBBD _obb2, GCollisionCheck& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to triangle.
			/*!
			* Test a line to a clockwise triangle by computing if the line pierces the triangle. A collision will only be
			* detected if the line is non-coplanar to the triangle and it pierces the from front face (the line's start
			* point lies in front of the triangle and the line's end lies in back of the triangle). Provides the point
			* of contact along with the normalized direction and interval along that direction the contact occurs.
			* If the line contacts the triangle, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			*
			* \param [in]	_line				The line.
			* \param [in]	_triangle			The triangle.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outBarycentric 	If non-null, the out barycentric.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToTriangleD(const MATH::GLINED _line, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD* _outBarycentric, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to plane.
			/*!
			* Test a line to a plane by computing the interval along the line that intersects the plane. Provides the point
			* of contact along with the normalized direction and interval along that direction the contact occurs.
			* If the line contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the line is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the line lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the line lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_line				The line.
			* \param [in]	_plane				The plane.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToPlaneD(const MATH::GLINED _line, const MATH::GPLANED _plane, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to sphere.
			/*!
			* Test a line to a sphere by computing the closest point on that line to the sphere's center and reducing the
			* query to a point to sphere check. Provides the point of contact along with the normalized direction
			* and interval along that direction the contact occurs.
			* If the line contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line				The line.
			* \param [in]	_sphere				The sphere.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToSphereD(const MATH::GLINED _line, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to capsule.
			/*!
			* Test a line to a capsule by finding the closest points of the line to the capsule's center line and reducing
			* the query to a point to sphere check. Provides the point of contact along with the normalized direction
			* and interval along that direction the contact occurs.
			* If the line contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_line				The line.
			* \param [in]	_capsule			The capsule.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToCapsuleD(const MATH::GLINED _line, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to AABB.
			/*!
			* Test a line to an AABB by checking if any section of the line lies within the AABB's bounds. Provides
			* the point of contact along with the normalized direction and interval along that direction the
			* contact occurs.
			* If the line contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur when the line degenerates to a point.
			*
			* \param [in]	_line				The line.
			* \param [in]	_aabb				The aabb.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToAABBD(const MATH::GLINED _line, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect line to OBB.
			/*!
			* Test a line to an OBB by checking if any section of the line lies within the OBB's bounds. Provides
			* the point of contact along with the normalized direction and interval along that direction the
			* contact occurs.
			* If the line contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the line does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur when the line degenerates to a point.
			*
			* \param [in]	_line				The line.
			* \param [in]	_obb				The obb.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outDirection   	The out direction.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectLineToOBBD(const MATH::GLINED _line, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to triangle.
			/*!
			* Test a ray to a clockwise triangle by computing if the ray's path intersects the triangle. Provides the
			* point of contact along with the interval the contact occurs on the ray.
			* If the ray's path contacts the triangle, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the triangle, the out result is GCollisionCheck::NO_COLLISION.
			* If the out barycentric argument is valid, the barycentric coordinates is stored in it.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_triangle			The triangle.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outBarycentric 	If non-null, the out barycentric.
			* \param [out]	_outInterval		The out interval along the direction.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToTriangleD(const MATH::GRAYD _ray, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD* _outBarycentric, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to plane.
			/*!
			* Test a ray to a plane by computing if the ray's path pierces the plane. Provides the point of contact along
			* with the interval the contact occurs on the ray.
			* If the ray contacts the plane, the out result is GCollisionCheck::COLLISION.
			* If the ray is coplanar to the plane, the out result is GCollisionCheck::NO_COLLISION.
			* If the ray lies below the plane, the out result is GCollisionCheck::BELOW.
			* If the ray lies above the plane, the out result is GCollisionCheck::ABOVE.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_plane				The plane.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToPlaneD(const MATH::GRAYD _ray, const MATH::GPLANED _plane, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to sphere.
			/*!
			* Tests if a collision exists between ray to sphere by using the quadratic formula to determine if there are
			* any points of intersection (roots). Provides the point of contact along with the interval the contact
			* occurs on the ray.
			* If the ray's path contacts the sphere, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the sphere, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_sphere				The sphere.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToSphereD(const MATH::GRAYD _ray, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to capsule.
			/*!
			* Test a ray to a capsule by computing the closest points on the ray's path to the capsule's center line and
			* reduces the query to a point to sphere test. Provides the point of contact along with the interval the
			* contact occurs on the ray.
			* If the ray's path contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_capsule			The capsule.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToCapsuleD(const MATH::GRAYD _ray, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to AABB.
			/*!
			* Test a ray to an AABB by checking if any section of the ray's path lies within the AABB's bounds. Provides
			* the point of contact along with the interval the contact occurs on the ray.
			* If the ray's path contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.

			*
			* \param [in]	_ray				The ray.
			* \param [in]	_aabb				The aabb.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToAABBD(const MATH::GRAYD _ray, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect ray to OBB.
			/*!
			* Test a ray to an OBB by checking if any section of the ray's path lies within the AABB's bounds. Provides
			* the point of contact along with the interval the contact occurs on the ray.
			* If the ray's path contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the ray's path does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_ray				The ray.
			* \param [in]	_obb				The obb.
			* \param [out]	_outResult			The out result.
			* \param [out]	_outContactPoint	The out contact point.
			* \param [out]	_outInterval		The out interval along the ray.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectRayToOBBD(const MATH::GRAYD _ray, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect sphere to sphere.
			/*!
			* Test a sphere to sphere by checking the distance between their centers and the
			* sum of their radii. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the spheres contact, the out result is GCollisionCheck::COLLISION.
			* If the spheres do not contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere1		   	The first sphere.
			* \param [in]	_sphere2		   	The second sphere.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance		The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectSphereToSphereD(const MATH::GSPHERED _sphere1, const MATH::GSPHERED _sphere2, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect sphere to capsule.
			/*!
			* Test a sphere to capsule by checking the distance between each's center structures
			* and the sum of their radii. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the sphere contacts the capsule, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the capsule, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere			   	The sphere.
			* \param [in]	_capsule		   	The capsule.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectSphereToCapsuleD(const MATH::GSPHERED _sphere, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect sphere to AABB.
			/*!
			* Test a sphere to an AABB by computing the distance from the AABB's boundary to the sphere's center and
			* computing if that distance is within the sphere's radius. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the sphere contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere			   	The sphere.
			* \param [in]	_aabb			   	The aabb.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectSphereToAABBD(const MATH::GSPHERED _sphere, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect sphere to OBB.
			/*!
			* Test a sphere to an OBB by computing the distance from the OBB's boundary to the sphere's center and
			* computing if that distance is within the sphere's radius. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the sphere contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the sphere does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_sphere			   	The sphere.
			* \param [in]	_obb			   	The obb.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectSphereToOBBD(const MATH::GSPHERED _sphere, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect capsule to capsule.
			/*!
			* Test a capsule to another capsule by computing the closest points on each capsule's center line and
			* performing a sphere to sphere test. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the capsules contact, the out result is GCollisionCheck::COLLISION.
			* If the capsules don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule1		   	The first capsule.
			* \param [in]	_capsule2		   	The second capsule.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectCapsuleToCapsuleD(const MATH::GCAPSULED _capsule1, const MATH::GCAPSULED _capsule2, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect capsule to AABB.
			/*!
			* Test a capsule to an AABB by increasing the AABB's extents by the capsule's radius and then performing a
			* ray to AABB test. Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the capsule contacts the AABB, the out result is GCollisionCheck::COLLISION.
			* If the capsule does not contact the AABB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule		   	The capsule.
			* \param [in]	_aabb			   	The aabb.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectCapsuleToAABBD(const MATH::GCAPSULED _capsule, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect capsule to OBB.
			/*!
			* Test a capsule to an OBB by bringing the capsule into the OBB's space and performing a capsule to AABB test.
			* Provides the minimal translation information: two points on each shape's surface,
			* the direction the two points form, and distance required to translate the second shape to where the surfaces
			* barely contact.
			* If the capsule contacts the OBB, the out result is GCollisionCheck::COLLISION.
			* If the capsule does not contact the OBB, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_capsule		   	The capsule.
			* \param [in]	_obb			   	The obb.
			* \param [out]	_outResult		   	The out result.
			* \param [out]	_outContactClosest1	The first out contact closest.
			* \param [out]	_outContactClosest2	The second out contact closest.
			* \param [out]	_outDirection	   	The out direction.
			* \param [out]	_outDistance	   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectCapsuleToOBBD(const MATH::GCAPSULED _capsule, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Intersect AABB to AABB.
			/*!
			* Test an AABB to an AABB by checking if they overlap on all three axes. Provides the minimal translation
			* information: two points on each shape's surface,the direction the two points form, and distance required to
			* translate the second shape to where the surfaces barely contact.
			* If the AABBs contact, the out result is GCollisionCheck::COLLISION.
			* If the AABBs don't contact, the out result is GCollisionCheck::NO_COLLISION.
			*
			* \param [in]	_aabb1		   	The first aabb.
			* \param [in]	_aabb2		   	The second aabb.
			* \param [out]	_outResult	   	The out result.
			* \param [out]	_outContactAABB	The out contact a bb.
			* \param [out]	_outDirection  	The out direction.
			* \param [out]	_outDistance   	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn IntersectAABBToAABBD(const MATH::GAABBCED _aabb1, const MATH::GAABBCED _aabb2, GCollisionCheck& _outResult, MATH::GAABBCED& _outContactAABB, MATH::GVECTORD& _outDirection, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to line.
			/*!
			* Squared separation distance between point to line.
			*
			* \param [in]	_point			The point.
			* \param [in]	_line			The line.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToLineD(const MATH::GVECTORD _point, const MATH::GLINED _line, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to ray.
			/*!
			* Squared separation distance between point to ray.
			*
			* \param [in]	_point			The point.
			* \param [in]	_ray			The ray.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToRayD(const MATH::GVECTORD _point, const MATH::GRAYD _ray, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to triangle.
			/*!
			* Squared separation distance between point to triangle.
			*
			* \param [in]	_point			The point.
			* \param [in]	_triangle   	The triangle.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToTriangleD(const MATH::GVECTORD _point, const MATH::GTRIANGLED _triangle, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to plane.
			/*!
			* Squared separation distance between point to plane.
			*
			* \param [in]	_point			The point.
			* \param [in]	_plane			The plane.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToPlaneD(const MATH::GVECTORD _point, const MATH::GPLANED _plane, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to sphere.
			/*!
			* Squared separation distance between point to sphere.
			*
			* \param [in]	_point			The point.
			* \param [in]	_sphere			The sphere.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToSphereD(const MATH::GVECTORD _point, const MATH::GSPHERED _sphere, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to capsule.
			/*!
			* Squared separation distance between point to capsule.
			*
			* \param [in]	_point			The point.
			* \param [in]	_capsule		The capsule.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToCapsuleD(const MATH::GVECTORD _point, const MATH::GCAPSULED _capsule, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to AABB.
			/*!
			* Squared separation distance between point to a AABB.
			*
			* \param [in]	_point			The point.
			* \param [in]	_aabb			The aabb.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToAABBD(const MATH::GVECTORD _point, const MATH::GAABBMMD _aabb, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Squared distance between point to OBB.
			/*!
			* Squared separation distance between point to OBB.
			*
			* \param [in]	_point			The point.
			* \param [in]	_obb			The obb.
			* \param [out]	_outDistance	The out distance.
			*
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToOBBD(const MATH::GVECTORD _point, const MATH::GOBBD _obb, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute the barycentric coordinates of a point to triangle.
			/*!
			* Compute the barycentric coordinates given three points of a triangle with a point in respect to that triangle.
			* Both triangle and point must be coplanar. The result is stored in a vector representing the barycentric coords.
			* If the function succeeds, the return value is SUCCESS.
			* If the function fails, the return value is FAILURE. This would occur when the triangle is degenerate.
			*
			* \param [in] 	_a			   	The first point of a triangle.
			* \param [in] 	_b			   	The second point of a triangle.
			* \param [in] 	_c			   	The third point of a triangle.
			* \param [in] 	_p			   	The point coplanar with the triangle points.
			* \param [out]	_outBarycentric	The out barycentric coordinates.
			*
			* \retval GReturn::FAILURE The calculation failed.
			* \retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn BarycentricD(const MATH::GVECTORD _a, const MATH::GVECTORD _b, const MATH::GVECTORD _c, const MATH::GVECTORD _p, MATH::GVECTORD& _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
#endif // DOXYGEN_ONLY
		};
	}
}

#endif // GCOLLISION_H
