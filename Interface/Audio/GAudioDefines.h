#ifndef GAUDIODEFINES_H
#define GAUDIODEFINES_H

// Count of implemented GATTENUATION
#define G_IMPLEMENTED_ATTENUATIONS 1

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all audio library interfaces must belong.
	namespace AUDIO
	{
		/*! \addtogroup AudioOptions
		*  @{
		*/

		//! Listing of types of distance model attenuation curves a GSound3D or GMusic3D can use.
		enum GATTENUATION
		{
			//! This attenuation model is a 1/1 reduction in volume over distance.
			/*! Good for general looping ambiance and low-detail background sounds
			 *  that do not need tight 3d falloff settings. Also good for cross-fading
			 *  large radius ambient sounds.
			*/
			LINEAR,

			//! This attenuation model is a logarithmic reduction in volume over distance.
			/*! Good for sounds that need more exact 3d positioning.
			 *  Also good for making sounds 'pop' at a close distance;
			 *  good for incoming missiles and projectiles as well.
			*/
			// LOGARITHMIC,

			//! This attenuation model is a reverse logarithmic reduction in volume over distance.
			/*! Useful as a layer in weapons or other sounds that need to be
			 *  loud up to their MaxRadius.
			*/
			// LOGREVERSE,

			//! This attenuation model is an extremely steep falloff curve.
			/*! Good for 3d sounds that are pinpoint loud by the MinRadius
			 *  but need to be present from a distance.
			*/
			// INVERSE,

			//! This attenuation model is a more 'realistic' falloff model that simulates sound in a real environment.
			/*! Good for fires or other point-interest or high frequency content
			 *  that the logarithmic attenuation does not feel 'right' for a sound's falloff.
			*/
			// NATURAL
		};

		/*! @} */
	}
}

#endif

/* Information from GAudioLinux.hpp
	PA_CHANNEL_POSITION_MONO

	PA_CHANNEL_POSITION_FRONT_LEFT
	Apple, Dolby call this 'Left'.

	PA_CHANNEL_POSITION_FRONT_RIGHT
	Apple, Dolby call this 'Right'.

	PA_CHANNEL_POSITION_FRONT_CENTER
	Apple, Dolby call this 'Center'.

	PA_CHANNEL_POSITION_REAR_CENTER
	Microsoft calls this 'Back Center', Apple calls this 'Center Surround', Dolby calls this 'Surround Rear Center'.

	PA_CHANNEL_POSITION_REAR_LEFT
	Microsoft calls this 'Back Left', Apple calls this 'Left Surround' (!), Dolby calls this 'Surround Rear Left'.

	PA_CHANNEL_POSITION_REAR_RIGHT
	Microsoft calls this 'Back Right', Apple calls this 'Right Surround' (!), Dolby calls this 'Surround Rear Right'.
*/