#ifndef GDIRECTX11SURFACE_H
#define GDIRECTX11SURFACE_H

/*!
	File: GDirectX11Surface.h
	Purpose: A Gateware interface that initializes a DirectX11 rendering surface and manages it's core resources.
	Author: Andre Reid
	Contributors: Kai Huang, Ryan Powser, Lari Norri, Chase Richards, Alexander Cusaac
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../System/GWindow.h"
#include "../Graphics/GGraphicsDefines.h"

namespace GW
{
	namespace I
	{
		class GDirectX11SurfaceInterface : public virtual GEventResponderInterface
		{
		public:
			virtual GReturn GetAspectRatio(float& _outRatio) const = 0;
			virtual GReturn GetDevice(void** _outDevice) const = 0;
			virtual GReturn GetImmediateContext(void** _outContext) const = 0;
			virtual GReturn GetSwapchain(void** _outSwapchain) const = 0;
			virtual GReturn GetRenderTargetView(void** _outRenderTarget) const = 0;
			virtual GReturn GetDepthStencilView(void** _outDepthStencilView) const = 0;
		};
	}
}

#include "../../Source/Graphics/GDirectX11Surface/GDirectX11Surface.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware graphics interfaces must belong.
	namespace GRAPHICS
	{
		//! A Gateware interface that initializes a DirectX11 rendering surface and manages it's core resources.
		class GDirectX11Surface final
			: public I::GProxy<I::GDirectX11SurfaceInterface, I::GDirectX11SurfaceImplementation, GW::SYSTEM::GWindow, unsigned long long>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GDirectX11Surface)
			GATEWARE_CONST_FUNCTION(GetAspectRatio)
			GATEWARE_CONST_FUNCTION(GetDevice)
			GATEWARE_CONST_FUNCTION(GetImmediateContext)
			GATEWARE_CONST_FUNCTION(GetSwapchain)
			GATEWARE_CONST_FUNCTION(GetRenderTargetView)
			GATEWARE_CONST_FUNCTION(GetDepthStencilView)

			// reimplemented functions
			// from GEventResponderInterface
			GATEWARE_FUNCTION(Assign)
			GATEWARE_CONST_FUNCTION(Invoke)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
		   //! Creates and outputs a new GDirectX11Surface object.
		   /*!
		   *	Initializes a handle to a GDirectX11Surface object with an existing GWindow.
		   *	The created GDirectX11Surface object will have its reference count initialized
		   *	to one and register as a listener to the provided GWindow object.
		   *
		   *	This function accepts a bit mask that can hold
		   *	supported 'GGraphicsInitOptions', which will
		   *	be taken into account when creating the context.
		   *	To ignore this mask, simply pass in 0 when calling
		   *	this function and the context will be created with
		   *	default settings.
		   *
		   *	\param [in] _gwindow A pointer to an existing GWindow object.
		   *	\param [in] _initMask The bit mask that can hold special initialization options.
		   *
		   *	\retval GReturn::INVALID_ARGUMENT _gwindow is invalid.
		   *	\retval GReturn::FAILURE A GDirectX11Surface object was not created. _outSurface will be null.
		   *	\retval GReturn::SUCCESS A GDirectX11Surface object was successfully created.
		   */
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask);

			//! Returns the aspect ratio for the current window.
			/*!
			*	\param [out] _outRatio Will contain the calculated aspect ratio.
			*
			*   \retval GReturn::FAILURE No active GWindow exists to calculate an aspect ratio from.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty.
			*	\retval GReturn::SUCCESS The current aspect ratio was calculated and returned.
			*/
			virtual GReturn GetAspectRatio(float& _outRatio) const = 0;

			//! Returns the address of the current ID3D11Device.
			/*!
			*	\param [out] _outDevice Will contain the address of the device.
			*
			*   \retval GReturn::FAILURE No device exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty.
			*	\retval GReturn::SUCCESS A DirectX11 device exists and was returned.
			*/
			virtual GReturn GetDevice(void** _outDevice) const = 0;

			//! Returns the address of the current ID3D11DeviceContext.
			/*!
			*
			*	\param [out] _outContext Will contain the address of the context.
			*
			*   \retval GReturn::FAILURE No context exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty.
			*	\retval GReturn::SUCCESS A DirectX11 contexts exists and was returned.
			*/
			virtual GReturn GetImmediateContext(void** _outContext) const = 0;

			//! Returns the address of the current IDXGISwapChain.
			/*!
			*
			*	\param [out] _outSwapchain Will contain the address of the swap chain.
			*
			*   \retval GReturn::FAILURE No swap chain exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty.
			*	\retval GReturn::SUCCESS A DirectX11 swap chain exists and was returned.
			*/
			virtual GReturn GetSwapchain(void** _outSwapchain) const = 0;

			//! Returns the address of the current ID3D11RenderTargetView.
			/*!
			*	\param [out] _outRenderTarget Will contain the address of the render target view.
			*
			*   \retval GReturn::FAILURE No render target view exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty.
			*	\retval GReturn::SUCCESS A DirectX11 render target view exists and was returned.
			*/
			virtual GReturn GetRenderTargetView(void** _outRenderTarget) const = 0;

			//! Returns the address of the current ID3D11DepthStencilView.
			/*!
			*	A Depth Stencil View will only be created if requested as
			*	a special option when the 'Initialize' method is called.
			*
			*	\param [out] _outDepthStencilView Will contain the address of the depth stencil view.
			*
			*	\retval GReturn::SUCCESS A DirectX11 depth stencil view exists and was returned.
			*   \retval GReturn::FAILURE No depth stencil view exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty.
			*/
			virtual GReturn GetDepthStencilView(void** _outDepthStencilView) const = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GDIRECTX11SURFACE_H