#ifndef GDIRECTX12SURFACE_H
#define GDIRECTX12SURFACE_H

/*!
	File: GDirectX12Surface.h
	Purpose: A Gateware interface that initializes a DirectX12 rendering surface and manages it's core resources.
	Author: Kai Huang
	Contributors: Ryan Powser, Lari Norri, Chase Richards
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GEventReceiver.h"
#include "../System/GWindow.h"
#include "../Graphics/GGraphicsDefines.h"

namespace GW
{
	namespace I
	{
		class GDirectX12SurfaceInterface : public virtual GEventResponderInterface
		{
		public:
			virtual GReturn GetAspectRatio(float& outRatio) const = 0;
			virtual GReturn GetSwapchain4(void** ppOutSwapchain) const = 0;
			virtual GReturn GetSwapChainBufferIndex(unsigned int& outSwapChainBufferIndex) const = 0;

			virtual GReturn GetDevice(void** ppOutDevice) const = 0;

			virtual GReturn GetCommandList(void** ppOutDirectCommandList) const = 0;
			virtual GReturn GetCommandAllocator(void** ppOutDirectCommandAllocator) const = 0;
			virtual GReturn GetCommandQueue(void** ppOutDirectCommandQueue) const = 0;
			virtual GReturn GetFence(void** ppOutDirectFence) const = 0;

			virtual GReturn GetCBSRUADescriptorSize(unsigned int& outCBSRUADescriptorSize) const = 0;
			virtual GReturn GetSamplerDescriptorSize(unsigned int& outSamplerDescriptorSize) const = 0;
			virtual GReturn GetRenderTargetDescriptorSize(unsigned int& outRenderTargetDescriptorSize) const = 0;
			virtual GReturn GetDepthStencilDescriptorSize(unsigned int& outDepthStencilDescriptorSize) const = 0;

			virtual GReturn GetCurrentRenderTarget(void** ppOutRenderTarget) const = 0;
			virtual GReturn GetDepthStencil(void** ppOutDepthStencil) const = 0;

			virtual GReturn GetCurrentRenderTargetView(void* pOutRenderTargetView) const = 0;
			virtual GReturn GetDepthStencilView(void* pOutDepthStencilView) const = 0;

			virtual GReturn StartFrame() = 0;
			virtual GReturn EndFrame(bool VSync) = 0;
		};
	}
}

#include "../../Source/Graphics/GDirectX12Surface/GDirectX12Surface.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware graphics interfaces must belong.
	namespace GRAPHICS
	{
		//! A Gateware interface that initializes a DirectX12 rendering surface and manages it's core resources.
		class GDirectX12Surface final
			: public I::GProxy<	I::GDirectX12SurfaceInterface, I::GDirectX12SurfaceImplementation, 
								GW::SYSTEM::GWindow, unsigned long long>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GDirectX12Surface)
			GATEWARE_CONST_FUNCTION(GetAspectRatio)
			GATEWARE_CONST_FUNCTION(GetSwapchain4)
			GATEWARE_CONST_FUNCTION(GetSwapChainBufferIndex)
			GATEWARE_CONST_FUNCTION(GetDevice)
			GATEWARE_CONST_FUNCTION(GetCommandList)
			GATEWARE_CONST_FUNCTION(GetCommandAllocator)
			GATEWARE_CONST_FUNCTION(GetCommandQueue)
			GATEWARE_CONST_FUNCTION(GetFence)
			GATEWARE_CONST_FUNCTION(GetCBSRUADescriptorSize)
			GATEWARE_CONST_FUNCTION(GetSamplerDescriptorSize)
			GATEWARE_CONST_FUNCTION(GetRenderTargetDescriptorSize)
			GATEWARE_CONST_FUNCTION(GetDepthStencilDescriptorSize)
			GATEWARE_CONST_FUNCTION(GetCurrentRenderTarget)
			GATEWARE_CONST_FUNCTION(GetDepthStencil)
			GATEWARE_CONST_FUNCTION(GetCurrentRenderTargetView)
			GATEWARE_CONST_FUNCTION(GetDepthStencilView)
			GATEWARE_FUNCTION(StartFrame)
			GATEWARE_FUNCTION(EndFrame)
			
			// reimplemented functions
			// from GEventResponderInterface
			GATEWARE_FUNCTION(Assign)
			GATEWARE_CONST_FUNCTION(Invoke)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates and outputs a new GDirectX12 object.
			/*!
			*	Initializes a handle to a GDirectX12 object with an existing GWindow.
			*	The created GDirectX12 object will have its reference count initialized
			*	to one and register as a listener to the provided GWindow object.
			*
			*	This function accepts a bit mask that can hold
			*	supported 'GGraphicsInitOptions', which will
			*	be taken into account when creating the context.
			*	To ignore this mask, simply pass in 0 when calling
			*	this function and the context will be created with
			*	default settings.
			*
			*	NOTE: Supported initMask's are: GW::GRAPHICS::COLOR_10_BIT | GW::GRAPHICS::DEPTH_BUFFER_SUPPORT | GW::GRAPHICS::DEPTH_STENCIL_SUPPORT
			*
			*	\param [in] _gwindow A object to an existing valid GWindow object.
			*	\param [in] _initMask The bit mask that can hold special initialization options.
			*
			*	\retval GReturn::INVALID_ARGUMENT:	_gwindow is invalid.
			*	\retval GReturn::FAILURE:			A GDirectX12 object was not created.
			*	\retval GReturn::SUCCESS:			A GDirectX12 object was successfully created.
			*/
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask);

			//! Returns the aspect ratio for the current window.
			/*!
			*
			*	\param	[out] outRatio: A reference to an float
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No active GWindow exists to query aspect ratio.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: The aspect ratio is successfully queried.
			*/
			virtual GReturn GetAspectRatio(float& outRatio) const = 0;

			//! Returns the address of the current IDXGISwapChain4.
			/*!
			*
			*	\param [out] ppOutSwapchain: Address of an IDXGISwapChain4* object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No swap chain exists to be queried.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A IDXGISwapChain4 is successfully queried.
			*/
			virtual GReturn GetSwapchain4(void** ppOutSwapchain) const = 0;

			//! Returns the index of the swap chain buffer.
			/*!
			*
			*	\param [out] outSwapChainBufferIndex: A reference to an unsigned int
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No swap chain exists to query swap chain index.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A swap chain index is successfully queried.
			*/
			virtual GReturn GetSwapChainBufferIndex(unsigned int& outSwapChainBufferIndex) const = 0;

			//! Returns the address of the current ID3D12Device.
			/*!
			*
			*	\param [out] ppOutDevice: Address of an ID3D12Device* object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No device exists to be queried.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A ID3D12Device exists and was returned.
			*/
			virtual GReturn GetDevice(void** ppOutDevice) const = 0;

			//! Returns the address of the current ID3D12GraphicsCommandList that is used for recording direct commands.
			/*!
			*
			*	\param [out] ppOutDirectCommandList: Address of an ID3D12GraphicsCommandList* object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No command list exists to be queried.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A ID3D12GraphicsCommandList exists and was returned.
			*/
			virtual GReturn GetCommandList(void** ppOutDirectCommandList) const = 0;

			//! Returns the address of the current ID3D12CommandAllocator that is used for allocating memory for direct commands.
			/*!
			*
			*	\param [out] ppOutDirectCommandAllocator: Address of an ID3D12CommandAllocator* object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No command allocator exists to be queried.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A ID3D12CommandAllocator exists and was returned.
			*/
			virtual GReturn GetCommandAllocator(void** ppOutDirectCommandAllocator) const = 0;

			//! Returns the address of the current ID3D12CommandQueue that is used for executing direct commands.
			/*!
			*
			*	\param [out] ppOutDirectCommandQueue: Address of an ID3D12CommandQueue* object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No command list exists to be queried.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A ID3D12CommandQueue exists and was returned.
			*/
			virtual GReturn GetCommandQueue(void** ppOutDirectCommandQueue) const = 0;

			//! Returns the address of the current ID3D12Fence that is used for synchronizing the direct command queue.
			/*!
			*
			*	\param [out] ppOutDirectFence: Address of an ID3D12Fence* object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No fence exists to be queried.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A ID3D12Fence exists and was returned.
			*/
			virtual GReturn GetFence(void** ppOutDirectFence) const = 0;

			//! Returns the descriptor size of constant buffer/shader resource/unordered access.
			/*!
			*
			*	\param [out] outCBSRUADescriptorSize: A reference to an unsigned int
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No device exists to query descriptor size.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A descriptor size is successfully queried.
			*/
			virtual GReturn GetCBSRUADescriptorSize(unsigned int& outCBSRUADescriptorSize) const = 0;

			//! Returns the descriptor size of sampler.
			/*!
			*
			*	\param [out] outSamplerDescriptorSize: A reference to an unsigned int
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No device exists to query descriptor size.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A descriptor size is successfully queried.
			*/
			virtual GReturn GetSamplerDescriptorSize(unsigned int& outSamplerDescriptorSize) const = 0;

			//! Returns the descriptor size of render target.
			/*!
			*
			*	\param [out] outRenderTargetDescriptorSize: A reference to an unsigned int
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No device exists to query descriptor size.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A descriptor size is successfully queried.
			*/
			virtual GReturn GetRenderTargetDescriptorSize(unsigned int& outRenderTargetDescriptorSize) const = 0;

			//! Returns the descriptor size of depth stencil.
			/*!
			*
			*	\param [out] outDepthStencilDescriptorSize: A reference to an unsigned int
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No device exists to query descriptor size.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A descriptor size is successfully queried.
			*/
			virtual GReturn GetDepthStencilDescriptorSize(unsigned int& outDepthStencilDescriptorSize) const = 0;

			//! Returns the ID3D12Resource of current render target resource.
			/*!
			*
			*	\param [out] ppOutRenderTarget: Address of a ID3D12Resource* object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No ID3D12Resource exists to query descriptor size.
			*	\retval GReturn::EMPTY_PROXY:	The proxy is empty.
			*	\retval GReturn::SUCCESS: A ID3D12Resource is successfully queried.
			*/
			virtual GReturn GetCurrentRenderTarget(void** ppOutRenderTarget) const = 0;

			//! Returns the ID3D12Resource of depth stencil resource.
			/*!
			*
			*	\param [out] ppOutDepthStencil: Address of a ID3D12Resource* object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No ID3D12Resource exists to query descriptor size.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A ID3D12Resource is successfully queried.
			*/
			virtual GReturn GetDepthStencil(void** ppOutDepthStencil) const = 0;

			//! Returns the D3D12_CPU_DESCRIPTOR_HANDLE of current render target resource.
			/*!
			*
			*	\param [out] pOutRenderTargetView: Address of a D3D12_CPU_DESCRIPTOR_HANDLE object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No ID3D12DescriptorHeap exists to query descriptor size.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A D3D12_CPU_DESCRIPTOR_HANDLE is successfully queried.
			*/
			virtual GReturn GetCurrentRenderTargetView(void* pOutRenderTargetView) const = 0;

			//! Returns the D3D12_CPU_DESCRIPTOR_HANDLE of the current depth stencil resource.
			/*!
			*
			*	\param [out] pOutDepthStencilView: Address of a D3D12_CPU_DESCRIPTOR_HANDLE object
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*   \retval GReturn::FAILURE: No ID3D12DescriptorHeap exists to query descriptor size.
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: A D3D12_CPU_DESCRIPTOR_HANDLE is successfully queried.
			*/
			virtual GReturn GetDepthStencilView(void* pOutDepthStencilView) const = 0;

			//! Preparing the surface for a starting frame operation
			/*!
			*	This method will perform synchronization between CPU and GPU,
			*	it will also transition internal swap chain resources to its
			*	proper state.
			*
			*	**NOTE: This is an optional method. The user can fully ignore this method
			*		  however, if they do, they'll have to do the CPU-GPU synchronization
			*		  to ensure there are no resource hazards/race conditions between
			*		  the two processing units and also perform transitions for resources.**
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*	\retval GReturn::FAILURE: Internal command queue, command allocator, and command list is invalid
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: StartFrame operation has succeeded
			*/
			virtual GReturn StartFrame() = 0;

			//! Preparing the surface for a end frame operation
			/*!
			*	This method will perform execution of the commands to the command queue,
			*	it will also transition internal swap chain resources to its
			*	proper state then present the swap chain's back-buffer
			*
			*	**NOTE: This is an optional method. The user can fully ignore this method
			*		  however, if they do, they'll have to do the CPU-GPU synchronization
			*		  to ensure there are no resource hazards/race conditions between
			*		  the two processing units and also perform transitions for resources.**
			*
			*	\param [out] VSync: boolean to specify whether or not to use VSync
			*
			*   \retval GReturn::PREMATURE_DEALLOCATION: The proxy is deallocated or the window object is referencing to has been deallocated
			*	\retval GReturn::FAILURE: Internal command queue, command allocator, and command list is invalid
			*	\retval GReturn::EMPTY_PROXY: The proxy is empty.
			*	\retval GReturn::SUCCESS: StartFrame operation has succeeded
			*/
			virtual GReturn EndFrame(bool VSync) = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GDIRECTX12SURFACE_H