#ifndef GGRAPHICSDEFINES_H
#define GGRAPHICSDEFINES_H

/*!
	File: GGraphicsDefines.h
	Purpose: A Gateware interface contains useful resources for GGraphics libraries.
	Author: Andre Reid
	Contributors: Artemis Kelsie Frost, Lari H. Norri
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware Graphics library interfaces must belong.
	namespace GRAPHICS
	{
		/*! \addtogroup GraphicsOptions
		*  @{
		*/
		//! GGraphicsInitOptions holds the special options that can be requested when initializing one of Gateware's graphics surfaces.
		enum GGraphicsInitOptions
		{
			COLOR_10_BIT			= 0x1,		//!< Require high dynamic range (HDR) color format 
			DEPTH_BUFFER_SUPPORT	= 0x2,		//!< Require Z-Buffer support
			DEPTH_STENCIL_SUPPORT	= 0X4,		//!< Require Z-Buffer to contain an 8-bit stencil buffer
			OPENGL_ES_SUPPORT		= 0x8,		//!< Require OpenGL to support for mobile platforms
			DIRECT2D_SUPPORT		= 0x10,		//!< Require BGRA surface support for DirectX11
			TRIPLE_BUFFER			= 0x20,		//!< Require Vulkan's Surface to allow 3 buffers
			MSAA_2X_SUPPORT			= 0x40,		//!< Require to add MSAA Support
			MSAA_4X_SUPPORT			= 0x80,		//!< Require to add MSAA Support
			MSAA_8X_SUPPORT			= 0x100,	//!< Require to add MSAA Support
			MSAA_16X_SUPPORT		= 0x200,	//!< Require to add MSAA Support
			MSAA_32X_SUPPORT		= 0x400,	//!< Require to add MSAA Support
			MSAA_64X_SUPPORT		= 0x800,	//!< Require to add MSAA Support
			BINDLESS_SUPPORT		= 0x1000,	//!< Require Vulkan/D3D12 to support bindless resources
		};

		//! List of bit-flag options for GRasterSurface smart surface updating.
		enum GRasterUpdateFlags
		{
			ALIGN_X_LEFT			= 0x1,		//!< Align raster with left edge of window
			ALIGN_X_CENTER			= 0x2,		//!< Align raster with horizontal center of window (default)
			ALIGN_X_RIGHT			= 0x4,		//!< Align raster with right edge of window
			ALIGN_Y_TOP				= 0x8,		//!< Align raster with top edge of window
			ALIGN_Y_CENTER			= 0x10,		//!< Align raster with vertical center of window (default)
			ALIGN_Y_BOTTOM			= 0x20,		//!< Align raster with bottom edge of window
			UPSCALE_2X				= 0x40,		//!< Upscale raster by factor of 2
			UPSCALE_3X				= 0x80,		//!< Upscale raster by factor of 3
			UPSCALE_4X				= 0x100,	//!< Upscale raster by factor of 4
			UPSCALE_8X				= 0x200,	//!< Upscale raster by factor of 8
			UPSCALE_16X				= 0x400,	//!< Upscale raster by factor of 16
			STRETCH_TO_FIT			= 0x800,	//!< Expand raster to fill window when updating
			INTERPOLATE_NEAREST		= 0x1000,	//!< Use color of nearest whole pixel when upscaling (default)
			INTERPOLATE_BILINEAR	= 0x2000,	//!< Blend nearest pixel colors when upscaling
		};
		/*! @} */
	}
}

#endif // GGRAPHICSDEFINES_H