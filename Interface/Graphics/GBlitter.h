#ifndef GBLITTER_H
#define GBLITTER_H

/*!
	File: GBlitter.h
	Purpose: A Gateware interface that allows defining tiles from pixel data sources and drawing them to an offscreen canvas via tiled rendering.
	Dependencies: win32[] linux[] apple[]
	Asynchronous: YES
	Author: Artemis Kelsie Frost
	Contributors: Lari Norri
	Interface Status: Alpha (GatewareX)
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GEventReceiver.h"
#include "../Graphics/GGraphicsDefines.h"

namespace GW
{
	namespace I
	{
		class GBlitterInterface : public virtual GInterfaceInterface
		{
		public:
			// 16 flags available
			enum DrawOptions
			{
				ROTATE					= 0x1,
				MIRROR_VERTICAL			= 0x2,
				MIRROR_HORIZONTAL		= 0x4,
				IGNORE_SOURCE_COLORS	= 0x8,
				USE_SOURCE_LAYERS		= 0x10,
				USE_SOURCE_STENCILS		= 0x20,
				USE_MASKING				= 0x40,
				USE_TRANSFORMATIONS		= 0x80,
				USE_TRANSPARENCY		= 0x100,
			};
			
			// 16 B
			struct TileDefinition
			{
				unsigned short			source_id;
				unsigned short			x;
				unsigned short			y;
				unsigned short			w;
				unsigned short			h;
				unsigned int			mask_color;
				unsigned char			mask_layer;
				unsigned char			mask_stencil;
			};
			const TileDefinition GBlitterDefaultTileDefinition =
			{
				0xFFFF,
				0,
				0,
				0,
				0,
				0x00000000,
				0,
				0
			};

			// 48 B (44 B + padding)
			struct DrawInstruction
			{
				unsigned int			tile_id;
				unsigned short			flags;
				unsigned char			layer;
				unsigned char			stencil;
				float					t[3];
				float					m[2][2];
				float					p[2];
				unsigned char			pad[4];
			};
			const DrawInstruction GBlitterDefaultDrawInstruction =
			{
				0xFFFFFFFF,
				0,
				0,
				0,
				{ 0, 0, 0 },
				{ { 1, 0 }, { 0, 1 } },
				{ 0, 0 }
			};

			virtual GReturn LoadSource(
				const char* _colorsFilepath, const char* _layersFilepath, const char* _stencilsFilepath,
				unsigned short &_outIndex) = 0;
			virtual GReturn ImportSource(
				const unsigned int* _colors, const unsigned char* _layers, const unsigned char* _stencils,
				unsigned short _width, unsigned short _height, unsigned short _pixelOffset, unsigned short _rowPixelStride,
				unsigned short& _outIndex) = 0;
			virtual GReturn ImportSourceComplex(
				const unsigned int* _colors, unsigned short _colorByteStride, unsigned short _colorRowByteStride,
				const unsigned char* _layers, unsigned short _layerByteStride, unsigned short _layerRowByteStride,
				const unsigned char* _stencils, unsigned short _stencilByteStride, unsigned short _stencilRowByteStride,
				unsigned short _width, unsigned short _height, unsigned short& _outIndex) = 0;
			virtual GReturn DefineTiles(const TileDefinition* _tileDefinitions, const unsigned int _numTiles, unsigned int* _outIndices) = 0;
			virtual GReturn SetTileMaskValues(
				unsigned int* _tileIndices, unsigned int _numTiles,
				unsigned int _maskColor, unsigned char _maskLayer, unsigned char _maskStencil) = 0;
			virtual GReturn DiscardSources(unsigned short* _sourceIndices, unsigned short _numSources) = 0;
			virtual GReturn DiscardTiles(unsigned int* _tileIndices, unsigned int _numTiles) = 0;
			virtual GReturn Clear(unsigned int _color, unsigned char _layer, unsigned char _stencil) = 0;
			virtual GReturn ClearColor(unsigned int _color) = 0;
			virtual GReturn ClearLayer(unsigned char _layer) = 0;
			virtual GReturn ClearStencil(unsigned char _stencil) = 0;
			virtual GReturn DrawDeferred(const DrawInstruction* _drawInstructions, const unsigned short _numDraws) = 0;
			virtual GReturn DrawImmediate(const DrawInstruction* _drawInstructions, const unsigned short _numDraws) = 0;
			virtual GReturn Flush() = 0;
			virtual GReturn ExportResult(
				bool _flush,
				unsigned short _width, unsigned short _height, unsigned short _pixelOffset, unsigned short _rowPixelStride,
				unsigned int* _outColors, unsigned char* _outLayers, unsigned char* _outStencils) = 0;
			virtual GReturn ExportResultComplex(
				bool _flush,
				unsigned short _colorByteStride, unsigned short _colorRowByteStride,
				unsigned short _layerByteStride, unsigned short _layerRowByteStride,
				unsigned short _stencilByteStride, unsigned short _stencilRowByteStride,
				unsigned short _width, unsigned short _height,
				unsigned int* _outColors, unsigned char* _outLayers, unsigned char* _outStencils) = 0;
		};
	};
};

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Graphics/GBlitter/GBlitter.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware Graphics library interfaces must belong.
	namespace GRAPHICS
	{
		//! A Gateware interface that allows defining tiles from pixel data sources and drawing them to an offscreen bitmap via tiled rendering.
		class GBlitter final : public I::GProxy<I::GBlitterInterface, I::GBlitterImplementation, unsigned short, unsigned short>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GBlitter)
			GATEWARE_TYPEDEF(DrawOptions)
			GATEWARE_TYPEDEF(TileDefinition)
			GATEWARE_TYPEDEF(DrawInstruction)
			GATEWARE_FUNCTION(LoadSource)
			GATEWARE_FUNCTION(ImportSource)
			GATEWARE_FUNCTION(ImportSourceComplex)
			GATEWARE_FUNCTION(DefineTiles)
			GATEWARE_FUNCTION(SetTileMaskValues)
			GATEWARE_FUNCTION(DiscardSources)
			GATEWARE_FUNCTION(DiscardTiles)
			GATEWARE_FUNCTION(Clear)
			GATEWARE_FUNCTION(ClearColor)
			GATEWARE_FUNCTION(ClearLayer)
			GATEWARE_FUNCTION(ClearStencil)
			GATEWARE_FUNCTION(DrawDeferred)
			GATEWARE_FUNCTION(DrawImmediate)
			GATEWARE_FUNCTION(Flush)
			GATEWARE_FUNCTION(ExportResult)
			GATEWARE_FUNCTION(ExportResultComplex)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Tile drawing options supported by GBlitter. Bit-flags indicating which transformations should be applied to a tile during drawing.
			//!	Notes:
			//!		Eight basic orientations are available using combinations of the ROTATE, MIRROR_VERTICAL, and MIRROR_HORIZONTAL flags (all rotation angles are clockwise; "inverse" indicates mirroring about the tile's local y axis):
			//!			0*				= 0
			//!			0* inverse		= MIRROR_HORIZONTAL
			//!			90*				= ROTATE
			//!			90* inverse		= ROTATE | MIRROR_VERTICAL
			//!			180*			= MIRROR_VERTICAL | MIRROR_HORIZONTAL
			//!			180* inverse	= MIRROR_VERTICAL
			//!			270*			= ROTATE | MIRROR_VERTICAL | MIRROR_HORIZONTAL
			//!			270* inverse	= ROTATE | MIRROR_HORIZONTAL
			enum DrawOptions
			{
				ROTATE					= 0x0001, //!< Indicates that a tile should be rotated 90 degrees clockwise. (Only used by non-transformed instructions.)
				MIRROR_VERTICAL			= 0x0002, //!< Indicates that a tile should be flipped in the y axis. (Only used by non-transformed instructions.)
				MIRROR_HORIZONTAL		= 0x0004, //!< Indicates that a tile should be flipped in the x axis. (Only used by non-transformed instructions.)
				IGNORE_SOURCE_COLORS	= 0x0008, //!< Indicates that per-pixel color values from source should NOT be used. (I.E., only layer and/or stencil data from tile should be drawn to the canvas.)
				USE_SOURCE_LAYERS		= 0x0010, //!< Indicates that per-pixel layer values from source should be used.
				USE_SOURCE_STENCILS		= 0x0020, //!< Indicates that per-pixel stencil values from source should be used.
				USE_MASKING				= 0x0040, //!< Indicates that key-value bitmasking should be applied to a tile. Affects color, layer, and stencil data. (Opaque/transparent. Slightly less efficient than no transparency.)
				USE_TRANSFORMATIONS		= 0x0080, //!< Indicates that the transformation matrix and pivot should be applied to a tile. Note: Transformations use screen axes (I.E., -y is up, +y is down), NOT mathematical axes (ie, -y is down, +y is up).
				USE_TRANSPARENCY		= 0x0100, //!< Indicates that alpha-channel color blending should be applied to a tile. Affects color data. (Semi-transparency. Significantly less efficient than no transparency or bitmasking. Overuse will reduce performance.)
			};

			/*! Struct defining the data for a GBlitter tile. Specifies the source, origin position, and dimensions of a tile, as well as the color/layer/stencil values to remove with bitmasking, if used. */
			struct TileDefinition
			{
				unsigned short			source_id; /*! Index of source that tile should be defined in. (0xFFFF is considered invalid.) */
				unsigned short			x; /*! x origin of tile within source, in pixels. */
				unsigned short			y; /*! y origin of tile within, in pixels. */
				unsigned short			w; /*! Width of tile, in pixels. */
				unsigned short			h; /*! Height of tile, in pixels. */
				unsigned int			mask_color; /*! Color value to remove with bitmasking. Will ONLY be applied to color data. (Default value is 0x00000000.) */
				unsigned char			mask_layer; /*! Layer value to remove with bitmasking. Will ONLY be applied to layer data. (Default value is 0.) */
				unsigned char			mask_stencil; /*! Stencil value to remove with bitmasking. Will ONLY be applied to stencil data. (Default value is 0.) */
			};

			/*! Default uninitialized TileDefinition values. Can be used to "zero out" a tile before setting the relevant values. */
			const TileDefinition GBlitterDefaultTileDefinition =
			{
				0xFFFF,
				0,
				0,
				0,
				0,
				0x00000000,
				0,
				0
			};

			/*! Struct containing the data for a GBlitter tile drawing operation. Specifies which tile to draw, the location to draw it to, and any transformations to apply during drawing. */
			struct DrawInstruction
			{
				unsigned int			tile_id; /*! Index of tile to be drawn. (0xFFFFFFFF is considered invalid.) */
				unsigned short			flags; /*! Bit-flags indicating which \ref DrawOptions to apply. */
				unsigned char			layer; /*! Layer to draw tile to. Used for sorting even if USE_SOURCE_LAYERS flag is on. */
				unsigned char			stencil; /*! Stencil value to draw tile with if USE_SOURCE_STENCILS flag is off. */
				float					t[3]; /*! x/y/z translation. z value is used to dynamically adjust the layer to draw to. */
				float					m[2][2]; /*! 2x2 row-major transformation matrix. Only used if USE_TRANSFORMATIONS flag is on. */
				float					p[2]; /*! Pivot (Fixed point to apply transformations about). Relative to top-left corner of tile. Only used if USE_TRANSFORMATIONS flag is on. */
				unsigned char			pad[4]; /*! Padding needed for 8-byte alignment. Not used and should be ignored. */
			};

			/*! Default uninitialized DrawInstruction values. Can be used to "zero out" a draw instruction before setting the relevant values. */
			const DrawInstruction GBlitterDefaultDrawInstruction =
			{
				0xFFFFFFFF,
				0,
				0,
				0,
				{ 0, 0, 0 },
				{ { 1, 0 }, { 0, 1 } },
				{ 0, 0 }
			};

			//! Allocates and initializes a GBlitter.
			/*!
			*	Creates a GBlitter proxy which enables you to draw tiles of pixel data to an offscreen canvas.
			*
			*	\param [in] _width						The width of the blitter's canvas, in pixels.
			*	\param [in] _height						The height of the blitter's canvas, in pixels.
			*
			*	\retval GReturn::INVALID_ARGUMENT		Width and/or height was zero.
			*	\retval GReturn::SUCCESS				GBlitter created successfully.
			*/
			GReturn Create(unsigned short _width, unsigned short _height);

			//! Loads and stores a read-only raster source to define tiles from.
			/*!
			*	Use this function to load source data from files directly.
			*	All types of data are optional, but at least one must be used for a source to be valid. All images used must have the same dimensions.
			*	Greyscale (especially with no alpha channel) is recommended for layer/stencil data since the file size will be significantly smaller.
			*	If a file does not contain an alpha channel, the alpha values will be set to maximum.
			*	Supported file types:
			*		TGA (Targa)
			*			Supported color formats:
			*			-  8 bits-per-pixel greyscale						(Luminance)				[TGA documentation refers to this as black-and-white]
			*			- 16 bits-per-pixel greyscale with transparency		(Alpha-Luminance)		[TGA documentation refers to this as black-and-white]
			*			- 24 bits-per-pixel RGB color						(Red-Green-Blue)		[TGA documentation refers to this as true-color]
			*			- 32 bits-per-pixel RGB color with transparency		(Alpha-Red-Green-Blue)	[TGA documentation refers to this as true-color]
			*			-  8 bits-per-pixel indexed color					(Look-Up Table)			[TGA documentation refers to this as color-mapped]
			*				-- Only 24-bit palette entries and 8-bit palette indices are supported.
			*				-- Be sure all colors in the palette are opaque, otherwise the palette will be read incorrectly.
			*	Maximum dimensions supported: 65,535 x 65,535
			*		(TGA files do not support anything larger than this, so attempting to export a TGA larger than this will corrupt the data. The library is not able to detect if this has happened except at exact multiples of 65,536 in either dimension.)
			*	Maximum sources supported: 65,535
			*
			*	\param [in] _colorsFilepath					(OPTIONAL) Path to the file to load color values from. (Pass nullptr if not needed.)
			*	\param [in] _layersFilepath					(OPTIONAL) Path to the file to load layer values from. Only the red channel will be used if file is in a color format. (Pass nullptr if not needed.)
			*	\param [in] _stencilsFilepath				(OPTIONAL) Path to the file to load stencil values from. Only the red channel will be used if file is in a color format. (Pass nullptr if not needed.)
			*	\param [out] _outIndex						Index of the stored source.
			*
			*	\retval GReturn::INVALID_ARGUMENT			All file paths were nullptr, one or more file paths do not contain a file extension, or two or more images' dimensions did not match.
			*	\retval GReturn::FAILURE					GFile proxy could not be created to read files or image data could not be read from a file.
			*	\retval	GReturn::FILE_NOT_FOUND				One or more file paths do not point to a valid file.
			*	\retval	GReturn::FORMAT_UNSUPPORTED			One or more file paths contains an extension that is not supported. (See supported file types above.)
			*	\retval GReturn::FEATURE_UNSUPPORTED		One or more files has image format features that are not supported. (Make sure file settings are correct when exporting; See supported file types above.)
			*	\retval GReturn::IGNORED					Maximum number of sources would be exceeded. (Consider combining sources together. If you need more than the maximum, you need to optimize.)
			*	\retval GReturn::SUCCESS					Source was loaded and stored.
			*/
			virtual GReturn LoadSource(const char* _colorsFilepath, const char* _layersFilepath, const char* _stencilsFilepath, unsigned short &_outIndex) = 0;

			//! Copies and stores a raster source to define tiles from.
			/*!
			*	Use this function to import source data generated at runtime or loaded manually.
			*	All types of data are optional, but at least one must be used for a source to be valid.
			*	If multiple data arrays are passed, all of them must have the same offset and stride. If you need more flexibility, use \ref ImportSourceComplex.
			*	Maximum dimensions supported: 65,535 x 65,535
			*	Maximum sources supported: 65,535
			*	Colors are in ARGB format, with 8 bits per pixel.
			*
			*	\param [in] _colors							(OPTIONAL) Array of color data to import from. (Pass nullptr if not needed.)
			*	\param [in] _layers							(OPTIONAL) Array of layer data to import from. (Pass nullptr if not needed.)
			*	\param [in] _stencil						(OPTIONAL) Array of stencil data to import from. (Pass nullptr if not needed.)
			*	\param [in] _width							Width of source, in pixels.
			*	\param [in] _height							Height of source, in pixels.
			*	\param [in] _pixelOffset					Number of pixels to ignore at the start of the array(s).
			*	\param [in] _rowPixelStride					Distance between rows, in pixels. (May be greater than width due to padding; Pass 0 to use width as stride.)
			*	\param [out] _outIndex						Index of the stored source.
			*
			*	\retval	GReturn::INVALID_ARGUMENT			All input arrays were nullptr, width and/or height was zero, or nonzero stride less than width was passed.
			*	\retval GReturn::IGNORED					Maximum number of sources would be exceeded. (Consider combining sources together. If you need more than the maximum, you need to optimize.)
			*	\retval GReturn::SUCCESS					Source data was imported and stored.
			*/
			virtual GReturn ImportSource(const unsigned int* _colors, const unsigned char* _layers, const unsigned char* _stencils, unsigned short _width, unsigned short _height, unsigned short _pixelOffset, unsigned short _rowPixelStride, unsigned short& _outIndex) = 0;

			//! Copies and stores a raster source to define tiles from.
			/*!
			*	Use this function to import data generated at runtime or loaded manually.
			*	All types of data are optional, but at least one must be used for a source to be valid.
			*	If you do not need the flexibility of this function or you do not know how to use it, use \ref ImportSource.
			*	Maximum dimensions supported: 65,535 x 65,535
			*	Maximum sources supported: 65,535
			*	Colors are in ARGB format, with 8 bits per pixel.
			*
			*	\param [in] _colors								(OPTIONAL) Array of color data to import from. (Pass nullptr if not needed.)
			*	\param [in] _colorByteStride					(OPTIONAL) Distance between color values, in bytes. (Pass 0 if colors are not used.)
			*	\param [in] _colorRowByteStride					(OPTIONAL) Distance between rows of color data, in bytes. Must be >= _width * _colorByteStride if used. (Pass 0 if colors are not used.)
			*	\param [in] _layers								(OPTIONAL) Array of layer data to import from. (Pass nullptr if not needed.)
			*	\param [in] _layerByteStride					(OPTIONAL) Distance between layer values, in bytes. (Pass 0 if layers are not used.)
			*	\param [in] _layerRowByteStride					(OPTIONAL) Distance between rows of layer data, in bytes. Must be >= _width * _layerByteStride if used. (Pass 0 if layers are not used.)
			*	\param [in] _stencils							(OPTIONAL) Array of stencil data to import from. (Pass nullptr if not needed.)
			*	\param [in] _stencilByteStride					(OPTIONAL) Distance between stencil values, in bytes. (Pass 0 if stencils are not used.)
			*	\param [in] _stencilRowByteStride				(OPTIONAL) Distance between rows of stencil data, in bytes. Must be >= _width * _stencilByteStride if used. (Pass 0 if stencils are not used.)
			*	\param [in] _width								Width of source, in pixels. Any input arrays must be at least this wide.
			*	\param [in] _height								Height of source, in pixels. Any input arrays must be at least this tall.
			*	\param [out] _outIndex							Index of the stored source.
			*
			*	\retval	GReturn::INVALID_ARGUMENT				All input arrays were nullptr, width and/or height was zero, a used stride value was zero, or a used item stride value greater than the corresponding row stride value was passed.
			*	\retval GReturn::IGNORED						Maximum number of sources would be exceeded. (Consider combining sources together. If you need more than the maximum, you need to optimize.)
			*	\retval GReturn::SUCCESS						Source data was imported and stored.
			*/
			virtual GReturn ImportSourceComplex(const unsigned int* _colors, unsigned short _colorByteStride, unsigned short _colorRowByteStride, const unsigned char* _layers, unsigned short _layerByteStride, unsigned short _layerRowByteStride, const unsigned char* _stencils, unsigned short _stencilByteStride, unsigned short _stencilRowByteStride, unsigned short _width, unsigned short _height, unsigned short& _outIndex) = 0;

			//! Stores tile definitions and returns indices for drawing.
			/*!
			*	Maximum tiles supported: 4,294,967,295
			*
			*	\param [in] _tileDefinitions				Array of tile definitions.
			*	\param [in] _numTiles						Number of definitions in the array.
			*	\param [out] _outIndices					List of tile indices to use for drawing. (Create this yourself and pass it to the function.)
			*
			*	\retval GReturn::INVALID_ARGUMENT			_tileDefinitions was nullptr, _outIndices was nullptr, _numTiles was zero, or an invalid definition was passed.
			*	\retval GReturn::IGNORED					Maximum number of tiles would be exceeded. (Check your code for runaway loops generating tiles. No project should EVER need even REMOTELY close to the maximum number.)
			*	\retval GReturn::SUCCESS					Tiles were defined successfully.
			*/
			virtual GReturn DefineTiles(const TileDefinition* _tileDefinitions, const unsigned int _numTiles, unsigned int* _outIndices) = 0;

			//! Sets the mask color, layer, and stencil values for all tiles specified.
			/*!
			*	\param [in] _tileIndices					Array of indices indicating which tiles to set values for.
			*	\param [in] _numTiles						Number of indices in the array.
			*	\param [in] _maskColor						Color value to store as all tiles' color mask value.
			*	\param [in] _maskLayer						Layer value to store as all tiles' layer mask value.
			*	\param [in] _maskStencil					Stencil value to store as all tiles' stencil mask value.
			*
			*	\retval GReturn::INVALID_ARGUMENT			_tileIndices was nullptr, _tileIndices contains invalid indices, _numTiles was zero, or _numTiles was greater than the number of tiles in memory.
			*	\retval GReturn::SUCCESS					Mask values were set for all tiles specified.
			*/
			virtual GReturn SetTileMaskValues(unsigned int* _tileIndices, unsigned int _numTiles, unsigned int _maskColor, unsigned char _maskLayer, unsigned char _maskStencil) = 0;

			//! Unloads sources from memory and removes them from the source list.
			/*!
			*	\param [in] _sourceIndices					Array of indices indicating which sources to discard.
			*	\param [in[ _numSources						Number of indices in the array.
			*
			*	\retval GReturn::INVALID_ARGUMENT			_sourceIndices was nullptr, _sourceIndices contains invalid indices, _numSources was zero, or _numSources was greater than the number of sources in memory.
			*	\retval GReturn::SUCCESS					Sources were unloaded and source indices no longer refer to valid sources. (Be sure to also discard any tiles that were defined in discarded sources.)
			*/
			virtual GReturn DiscardSources(unsigned short* _sourceIndices, unsigned short _numSources) = 0;

			//! Unloads tiles from memory and removes them from the tile list.
			/*!
			*	\param [in] _tileIndices					Array of indices indicating which tiles to discard.
			*	\param [in[ _numTiles						Number of indices in the array.
			*
			*	\retval GReturn::INVALID_ARGUMENT			_tileIndices was nullptr, _tileIndices contains invalid indices, _numTiles was zero, or _numTiles was greater than the number of tiles in memory.
			*	\retval GReturn::SUCCESS					Tiles were unloaded and tile indices no longer refer to valid tiles.
			*/
			virtual GReturn DiscardTiles(unsigned int* _tileIndices, unsigned int _numTiles) = 0;

			//! Clears the blitter's canvas to a color, layer, and stencil value.
			/*!
			*	\param [in] _color							The color value to clear to.
			*	\param [in] _layer							The layer value to clear to.
			*	\param [in] _stencil						The stencil value to clear to.
			*
			*	\retval GReturn::SUCCESS					Blitter's canvas was cleared.
			*/
			virtual GReturn Clear(unsigned int _color, unsigned char _layer, unsigned char _stencil) = 0;

			//! Clears the blitter's canvas to a color value.
			/*!
			*	\param [in] _color							The color value to clear to.
			*
			*	\retval GReturn::SUCCESS					Blitter's canvas was cleared.
			*/
			virtual GReturn ClearColor(unsigned int _color) = 0;

			//! Clears the blitter's canvas to a layer value.
			/*!
			*	\param [in] _layer							The layer value to clear to.
			*
			*	\retval GReturn::SUCCESS					Blitter's canvas was cleared.
			*/
			virtual GReturn ClearLayer(unsigned char _layer) = 0;

			//! Clears the blitter's canvas to a stencil value.
			/*!
			*	\param [in] _stencil						The stencil value to clear to.
			*
			*	\retval GReturn::SUCCESS					Blitter's canvas was cleared.
			*/
			virtual GReturn ClearStencil(unsigned char _stencil) = 0;

			//! Takes a list of draw instructions and stores them for later drawing to the canvas.
			/*!
			*	Call \ref Flush or pass true for _flush in \ref ExportResult or \ref ExportResultComplex to draw all deferred instructions to the canvas.
			*	If too many deferred instructions are in the blitter, portions of the canvas may run out of room and any additional instructions to those portions will be discarded.
			*	Under normal usage, this will not be an issue. If it does happen, flush instructions to the result more often or use \ref DrawImmediate instead.
			*
			*	\param [in] _drawInstructions				Array of draw instructions.
			*	\param [in] _numInstructions				Number of instructions in the array.
			*
			*	\retval GReturn::INVALID_ARGUMENT			Number of instructions was zero or an invalid instruction was passed.
			*	\retval GReturn::SUCCESS					Instructions were stored for later drawing.
			*/
			virtual GReturn DrawDeferred(const DrawInstruction* _drawInstructions, const unsigned short _numDraws) = 0;

			//! Takes a list of draw instructions and draws them to the canvas immediately.
			/*!
			*	Any unflushed instructions queued with DrawDeferred will be discarded.
			*	Unlike \ref DrawDeferred, if enough instructions are passed to fill portions of the canvas, they will be sorted and drawn in batches to ensure that they are all drawn and the result is correct.
			*	The number of instructions that need to be passed for this to be necessary is very high, and this function does not allow instructions to be stored for later drawing.
			*	This function is also more expensive, and will run slightly slower because of it.
			*	If you are not passing enough instructions to cause visual artifacts or you need the flexibility to queue instructions multiple times before flushing, use \ref DrawDeferred instead.
			*
			*	\param [in] _drawInstructions				Array of draw instructions.
			*	\param [in] _numInstructions				Number of instructions in the array.
			*
			*	\retval GReturn::INVALID_ARGUMENT			Number of instructions was zero or an invalid instruction was passed.
			*	\retval GReturn::SUCCESS					Instructions were drawn to the canvas.
			*/
			virtual GReturn DrawImmediate(const DrawInstruction* _drawInstructions, const unsigned short _numDraws) = 0;

			//! Flushes any outstanding draw instructions to the result immediately.
			/*!
			*	\retval GReturn::REDUNDANT					No draw instructions were outstanding.
			*	\retval GReturn::SUCCESS					Outstanding draw instructions were flushed to the result.
			*/
			virtual GReturn Flush() = 0;

			//! Copies the rendered result to output buffer(s).
			/*!
			*	All output buffers are optional, but at least one must be provided for the function to succeed.
			*	The Complex variant runs slower than this one, so if performance is more important than flexibility, use this function instead of \ref ExportResultComplex.
			*	If multiple output buffers are passed, all of them must have the same offset and stride. If you need more flexibility, use \ref ExportResultComplex.
			*	Output buffers do NOT have to be the same size as the blitter's canvas. The result will fill whatever space is available and leave any excess untouched.
			*	However, all arrays provided must have room for at least _height rows of _width items.
			*	Note that this means that even if an array has more room than this, these dimensions are the maximum that will be copied by this function.
			*	Therefore, if you need different amounts of different types of data, call this function multiple times with different output buffers and dimensions.
			*
			*	\param [in] _flush							Whether any outstanding draw instructions should be flushed to the result before it is exported.
			*	\param [in] _width							The width of the output buffer(s), in pixels.
			*	\param [in] _height							The height of the output buffer(s), in pixels.
			*	\param [in] _pixelOffset					The number of pixels to ignore at the start of the buffer(s).
			*	\param [in[ _rowPixelStride					The distance between rows, in pixels. (May be greater than width due to padding; Pass 0 to use width as stride.)
			*	\param [out] _outColors						(OPTIONAL) Array of color data to export into. (Pass nullptr if not needed.)
			*	\param [out] _outLayers						(OPTIONAL) Array of layer data to export into. (Pass nullptr if not needed.)
			*	\param [out] _outStencils					(OPTIONAL) Array of stencil data to export into. (Pass nullptr if not needed.)
			*
			*	\retval	GReturn::INVALID_ARGUMENT			All output buffers were nullptr, width and/or height was zero, or nonzero stride less than width was passed.
			*	\retval GReturn::SUCCESS					Result data was copied into the output buffer(s).
			*/
			virtual GReturn ExportResult(bool _flush, unsigned short _width, unsigned short _height, unsigned short _pixelOffset, unsigned short _rowPixelStride, unsigned int* _outColors, unsigned char* _outLayers, unsigned char* _outStencils) = 0;

			//! Copies the rendered result to output buffer(s).
			/*!
			*	All output buffers are optional, but at least one must be provided for the function to succeed.
			*	If you do not need the flexibility of this function or you do not know how to use it, use \ref ExportResult.
			*	The standard variant runs faster than this one, so if performance is more important than flexibility, use \ref ExportResult instead of this function.
			*	Output buffers do NOT have to be the same size as the blitter's canvas. The result will fill whatever space is available and leave any excess untouched.
			*	However, all arrays provided must have room for at least _height rows of _width items.
			*	Note that this means that even if an array has more room than this, these dimensions are the maximum that will be copied by this function.
			*	Therefore, if you need different amounts of different types of data, call this function multiple times with different output buffers and dimensions.
			*
			*	\param [in] _flush							Whether any outstanding draw instructions should be flushed to the result before it is exported.
			*	\param [in] _colorByteStride				Distance between color values, in bytes. (Pass 0 if color buffer is not used.)
			*	\param [in] _colorRowByteStride				Distance between rows of color data, in bytes. Must be >= _width * _colorByteStride if used. (Pass 0 if color buffer is not used.)
			*	\param [in] _layerByteStride				Distance between layer values, in bytes. (Pass 0 if layer buffer is not used.)
			*	\param [in] _layerRowByteStride				Distance between rows of layer data, in bytes. Must be >= _width * _layerByteStride if used. (Pass 0 if layer buffer is not used.)
			*	\param [in] _stencilByteStride				Distance between stencil values, in bytes. (Pass 0 if stencil buffer is not used.)
			*	\param [in] _stencilRowByteStride			Distance between rows of stencil data, in bytes. Must be >= _width * _stencilByteStride if used. (Pass 0 if stencil buffer is not used.)
			*	\param [in] _width							Width of output buffer(s), in pixels. All output arrays must be at least this wide.
			*	\param [in] _height							Height of output buffer(s), in pixels. All output arrays must be at least this tall.
			*	\param [out] _outColors						(OPTIONAL) Array of color data to export into. (Pass nullptr if not needed.)
			*	\param [out] _outLayers						(OPTIONAL) Array of layer data to export into. (Pass nullptr if not needed.)
			*	\param [out] _outStencils					(OPTIONAL) Array of stencil data to export into. (Pass nullptr if not needed.)
			*
			*	\retval	GReturn::INVALID_ARGUMENT			All output arrays were nullptr, width and/or height was zero, a used stride value was zero, or a used item stride value greater than the corresponding row stride value was passed.
			*	\retval GReturn::SUCCESS					Result data was copied into the output buffer(s).
			*/
			virtual GReturn ExportResultComplex(bool _flush, unsigned short _colorByteStride, unsigned short _colorRowByteStride, unsigned short _layerByteStride, unsigned short _layerRowByteStride, unsigned short _stencilByteStride, unsigned short _stencilRowByteStride, unsigned short _width, unsigned short _height, unsigned int* _outColors, unsigned char* _outLayers, unsigned char* _outStencils) = 0;

#endif // DOXYGEN_ONLY
		};
	};
};

#endif // GBLITTER_H
