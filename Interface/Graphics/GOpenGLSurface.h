#ifndef GOPENGLSURFACE_H
#define GOPENGLSURFACE_H

/*!
	File: GOpenGLSurface.h
	Purpose: A Gateware interface that initializes an OpenGL rendering surface and manages it's core resources.
	Author: Andre Reid
	Contributors: Kai Huang, Lari Norri, Ozzie Mercado, Ryan Powser
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GEventReceiver.h"
#include "../System/GWindow.h"
#include "../Graphics/GGraphicsDefines.h"

namespace GW
{
	namespace I
	{
		class GOpenGLSurfaceInterface : public virtual GEventResponderInterface
		{
		public:
			virtual GReturn GetAspectRatio(float& _outRatio) const = 0;
			virtual GReturn GetContext(void** _outContext) const = 0;
			virtual GReturn UniversalSwapBuffers() = 0;
			virtual GReturn QueryExtensionFunction(const char* _extension, const char* _funcName, void** _outFuncAddress) = 0;
			virtual GReturn EnableSwapControl(bool _setSwapControl) = 0;
		};
	}
}

#include "../../Source/Graphics/GOpenGLSurface/GOpenGLSurface.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware graphics interfaces must belong.
	namespace GRAPHICS
	{
		//! A Gateware interface that initializes an OpenGL rendering surface and manages it's core resources. 
		/*!
		*	Defaults to using OpenGL core. On Windows, users can #define GATEWARE_FORCE_OPENGLES to switch to using
		*	OpenGL ES instead of core.
		*/
		class GOpenGLSurface final
			: public I::GProxy<I::GOpenGLSurfaceInterface, I::GOpenGLSurfaceImplementation, GW::SYSTEM::GWindow, unsigned long long>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GOpenGLSurface)
			GATEWARE_CONST_FUNCTION(GetAspectRatio)
			GATEWARE_CONST_FUNCTION(GetContext)
			GATEWARE_FUNCTION(UniversalSwapBuffers)
			GATEWARE_FUNCTION(QueryExtensionFunction)
			GATEWARE_FUNCTION(EnableSwapControl)

			// reimplemented functions
			// from GEventResponderInterface
			GATEWARE_FUNCTION(Assign)
			GATEWARE_CONST_FUNCTION(Invoke)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
		   //! Creates and outputs a new GOpenGLSurface object.
		   /*!
		   *	Initializes a handle to a GOpenGLSurface object with an existing GWindow.
		   *	The created GDirectX11Surface object will have its reference count initialized
		   *	to one and register as a listener to the provided GWindow object.
		   *
		   *	This function accepts a bit mask that can hold
		   *	supported 'GGraphicsInitOptions', which will
		   *	be taken into account when creating the context.
		   *	To ignore this mask, simply pass in 0 when calling
		   *	this function and the context will be created with
		   *	default settings.
		   *
		   *	\param [in] _gwindow A pointer to an existing GWindow object.
		   *	\param [in] _initMask The bit mask that can hold special initialization options.
		   *
		   *	\retval GReturn::INVALID_ARGUMENT _gwindow is invalid.
		   *	\retval GReturn::FAILURE A GOpenGLSurface object was not created.
		   *	\retval GReturn::SUCCESS A GOpenGLSurface object was successfully created.
		   */
			GReturn Create(GW::SYSTEM::GWindow _gwindow, unsigned long long _initMask);

			//! Returns the aspect ratio for the current window.
			/*!
			*	\param [out] _outRatio Will contain the calculated aspect ratio.
			*
			*   \retval GReturn::FAILURE No active GWindow exists to calculate an aspect ratio from.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS The current aspect ratio was calculated and returned.
			*/
			virtual GReturn GetAspectRatio(float& _outRatio) const = 0;

			//! Returns the current OpenGL context.
			/*!
			*
			*	\param [out] _outContext Will contain the address of the OpenGL context.
			*
			*   \retval GReturn::FAILURE No OpenGL context exists to retrieve.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS The existing OpenGL context was returned.
			*/
			virtual GReturn GetContext(void** _outContext) const = 0;

			//! Calls the appropriate method (depending on platform) to swap the front and back buffers.
			/*!
			*
			*	\retval	GReturn::FAILURE No valid OpenGL context exists to perform a buffer swap on.
			*	\retval GReturn::SUCCESS The front and back buffers were successfully swapped.
			*/
			virtual GReturn UniversalSwapBuffers() = 0;

			//! Queries if a requested OpenGL extension or function is supported.
			/*!
			*	This function accepts either an OpenGL extension or
			*	OpenGL function (or both) and searches the total list
			*	of supported extensions (generated upon creation of
			*	the context) to see if that extension/function is
			*	supported. When a function name is queried, this
			*	function will return it's appropriate address
			*	if supported.
			*
			*	\param [in] _extension The exact name of an OpenGL, WGL, or GLX extension.
			*	\param [in] _funcName The exact name of an OpenGL, WGL, or GLX function.
			*	\param [out] _outFuncAddress The address of a function pointer that matches the queried function (_funcName).
			*
			*	\retval GReturn::INVALID_ARGUMENT This function was called with an incorrect set of parameters.
			*	\retval GReturn::FAILURE The requested extension/function is not supported.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS The extension/function is supported, and the address was returned successfully (if requested).
			*/
			virtual GReturn QueryExtensionFunction(const char* _extension, const char* _funcName, void** _outFuncAddress) = 0;

			//! Enables or disables v-synchronization in regards to buffer swapping.
			/*!
			*
			*	\param [in] _setSwapControl Determines whether to enable or disable v-sync.
			*
			*   \retval GReturn::FEATURE_UNSUPPORTED The extension required to use this functionality was not supported.
			*	\retval GReturn::FAILURE No valid OpenGL context exists.
			*	\retval GReturn::EMPTY_PROXY The proxy is empty
			*	\retval GReturn::SUCCESS The appropriate function was called to enable v-sync.
			*/
			virtual GReturn EnableSwapControl(bool _setSwapControl) = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GOPENGLSURFACE_H