#ifndef GLOG_H
#define GLOG_H

/*!
	File: GLog.h
	Purpose: A Gateware interface that handles logging in a thread safe manner.
	Author: Justin W. Parks
	Contributors: Anthony G. Balsamo, Ryan Powser, Chase Richards
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "GFile.h"

namespace GW
{
	namespace I
	{
		class GLogInterface : public virtual GInterfaceInterface
		{
		public:
			virtual GReturn Log(const char* const _log) = 0;
			virtual GReturn LogCategorized(const char* const _category, const char* const _log) = 0;
			virtual GReturn EnableVerboseLogging(bool _value) = 0;
			virtual GReturn EnableConsoleLogging(bool _value) = 0;
			virtual GReturn Flush() = 0;
		};
	}
}

#include "../../Source/System/GLog/GLog.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware library interfaces must belong.
	namespace SYSTEM
	{
		//! Cross platform thread-safe logger.
		/*! 
		*	GLog inherits directly from GMultiThreaded, therefore its implementation 
		*	must be thread safe.
		*/
		class GLog final 
			: public I::GProxy<I::GLogInterface, I::GLogImplementation, const char * const>
		{
			// All Gateware API interfaces contain no variables & are pure virtual.
		public:
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GLog)
			GATEWARE_FUNCTION(Log)
			GATEWARE_FUNCTION(LogCategorized)
			GATEWARE_FUNCTION(EnableVerboseLogging)
			GATEWARE_FUNCTION(EnableConsoleLogging)
			GATEWARE_FUNCTION(Flush)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a GLog object.
			/*!
			*	This function will create a GLog object with the log file being created in the directory
			*	the program was ran from. If you want to control where the log file is going to be created
			*	then use the custom function below to pass in a GFile* that is pre set the way you want it.
			*	Reference count of created object is initialized to one.
			*
			*	\param [in] _fileName The name of the log file.
			*
			*	\retval INVALID_ARGUMENT  Either one or both arguments are nullptrs.
			*	\retval FAILURE  GLog was not created. _outLog will be null.
			*	\retval SUCCESS  GLog was successfully created.
			*/
			GReturn Create(const char* const _fileName);

			//! Creates a GLog object.
			/*!
			*	This function will create a GLog object with the GFile object that was passed in.
			*	This is so you can have more control over your log by setting up a GFile in advance.
			*	The GFile object should already have a file open for text writing.
			*	Created GLog object will have its reference count initialized to one.
			*	
			*	\param [in] _file The GFile object that this log will use.
			*
			*	\retval INVALID_ARGUMENT Either one or both the arguments are nullptr.
			*	\retval FAILURE  GLog was not created. _outLog will be null.
			*	\retval SUCCESS  GLog was successfully created.
			*/
			GReturn Create(GFile _file);

			//! Logs a null terminated string.
			/*!
			*	Date, Time, and thread ID will be appended to the front of the message unless
			*	specified otherwise (See EnableVerboseLogging). A new line character will be
			*	appended to the end of the string so your log messages do not require a new line.
			*	The string is logged to the internal GFile object.
			*
			*	\param [in] _log The message to log out.
			*
			*	\retval INVALID_ARGUMENT  A nullptr was passed in.
			*	\retval FAILURE  The queue has reached maximum size (call flush).
			*	\retval SUCCESS  Successfully queued the message to the log.
			*/
			virtual GReturn Log(const char* const _log) = 0;

			//! Logs a null terminated string with a category.
			/*!
			*	Date, Time, and thread ID will be appended to the front of the message unless
			*	specified otherwise (See EnableVerboseLogging). A new line character will be
			*	appended to the end of the string so your log messages do not require a new line.
			*	The string is logged to the internal GFile object.
			*
			*	\param [in] _category	The user-defined category the log belongs to. ie: ERROR, WARNING, INFO, etc.
			*	\param [in] _log		The message to log out.
			*
			*	\retval INVALID_ARGUMENT	Either _category or _log are nullptr.
			*	\retval FAILURE				The queue has reached maximum size (call flush).
			*	\retval SUCCESS				Successfully queued the message to the log.
			*/
			virtual GReturn LogCategorized(const char* const _category, const char* const _log) = 0;

			//! Turns verbose logging on or off.
			/*!
			*	Use this function to ensure or prevent the addition of date, time, and threadID
			*	to your log messages.
			*
			*	\param [in] _value true to turn on or false to turn off.
			*/
			virtual GReturn EnableVerboseLogging(bool _value) = 0;

			//! Turns console logging on or off.
			/*!
			*	Use this function to ensure or prevent the additional console logging.
			*
			*	\param [in] _value true to turn on or false to turn off.
			*/
			virtual GReturn EnableConsoleLogging(bool _value) = 0;

			//! Forces a log dump to file.
			/*!
			*	This will force a log dump from memory to disk and clear the log queue.
			*
			*	\retval FAILURE  Most likely a file corruption or a file is not open.
			*	\retval SUCCESS  Successfully dumped the logs.
			*/
			virtual GReturn Flush() = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GLOG_H