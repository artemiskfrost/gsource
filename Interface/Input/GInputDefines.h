#ifndef GINPUTDEFINES_H
#define GINPUTDEFINES_H

/*!
	File: GInputDefines.h
	Purpose: The Gateware key representations and any other Input related defines or structs.
	Dependencies: Windows[(none)] Linux[(none)] Mac[(none)]
	Author: Peter Farber
	Contributors: Lari H. Norri
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

/*! \addtogroup GInputCodes
 *  @{
 */

// All GInput/GBufferedInput Key definitions follow
#define G_KEY_UNKNOWN			0		 //!< 
#define G_KEY_ESCAPE			1		 //!< 
#define G_KEY_MINUS				2		 //!< 
#define G_KEY_EQUALS			3		 //!< 
#define G_KEY_BACKSPACE			4		 //!< 
#define G_KEY_TAB				5		 //!< 
#define G_KEY_BRACKET_OPEN		6		 //!< 
#define G_KEY_BRACKET_CLOSE		7		 //!< 
#define G_KEY_ENTER				8		 //!< 
#define G_KEY_LEFTCONTROL		9		 //!< 
#define G_KEY_RIGHTCONTROL		10		 //!<
#define G_KEY_SEMICOLON			11		 //!< 
#define G_KEY_QUOTE				12		 //!< 
#define G_KEY_TILDE				13		 //!< 
#define G_KEY_LEFTSHIFT			14		 //!< 
#define G_KEY_BACKSLASH			15		 //!< 
#define G_KEY_COMMA				16		 //!< 
#define G_KEY_PERIOD			17		 //!< 
#define G_KEY_FORWARDSLASH		18		 //!< 
#define G_KEY_RIGHTSHIFT		19		 //!< 
#define G_KEY_PRINTSCREEN		20		 //!< 
#define G_KEY_LEFTALT			21		 //!< 
#define G_KEY_RIGHTALT			22		 //!< 
#define G_KEY_SPACE				23		 //!< 
#define G_KEY_CAPSLOCK			24		 //!< 
#define G_KEY_NUMLOCK			25		 //!< 
#define G_KEY_SCROLL_LOCK		26		 //!< 
#define G_KEY_PAUSE				27		 //!<
#define G_KEY_HOME				28		 //!< 
#define G_KEY_UP				29		 //!< 
#define G_KEY_PAGEUP			30		 //!< 
#define G_KEY_LEFT				31		 //!< 
#define G_KEY_RIGHT				32		 //!< 
#define G_KEY_END				33		 //!< 
#define G_KEY_DOWN				34		 //!< 
#define G_KEY_PAGEDOWN			35		 //!< 
#define G_KEY_INSERT			36		 //!< 
#define G_KEY_DELETE			37		 //!< 

// Characters
#define G_KEY_A					38		 //!< 
#define G_KEY_B					39		 //!< 
#define G_KEY_C					40		 //!< 
#define G_KEY_D					41		 //!< 
#define G_KEY_E					42		 //!< 
#define G_KEY_F					43		 //!< 
#define G_KEY_G					44		 //!< 
#define G_KEY_H					45		 //!< 
#define G_KEY_I					46		 //!< 
#define G_KEY_J					47		 //!< 
#define G_KEY_K					48		 //!< 
#define G_KEY_L					49		 //!< 
#define G_KEY_M					50		 //!< 
#define G_KEY_N					51		 //!< 
#define G_KEY_O					52		 //!< 
#define G_KEY_P					53		 //!< 
#define G_KEY_Q					54		 //!< 
#define G_KEY_R					55		 //!< 
#define G_KEY_S					56		 //!< 
#define G_KEY_T					57		 //!< 
#define G_KEY_U					58		 //!< 
#define G_KEY_V					59		 //!< 
#define G_KEY_W					60		 //!< 
#define G_KEY_X					61		 //!< 
#define G_KEY_Y					62		 //!< 
#define G_KEY_Z					63		 //!< 

// Numbers
#define G_KEY_0					64		 //!< 
#define G_KEY_1					65		 //!< 
#define G_KEY_2					66		 //!< 
#define G_KEY_3					67		 //!< 
#define G_KEY_4					68		 //!< 
#define G_KEY_5					69		 //!< 
#define G_KEY_6					70		 //!< 
#define G_KEY_7					71		 //!< 
#define G_KEY_8					72		 //!< 
#define G_KEY_9					73		 //!< 

// Function Keys
#define G_KEY_F1				74		 //!< 
#define G_KEY_F2				75		 //!< 
#define G_KEY_F3				76		 //!< 
#define G_KEY_F4				77		 //!< 
#define G_KEY_F5				78		 //!< 
#define G_KEY_F6				79		 //!< 
#define G_KEY_F7				80		 //!< 
#define G_KEY_F8				81		 //!< 
#define G_KEY_F9				82		 //!< 
#define G_KEY_F10				83		 //!< 
#define G_KEY_F11				84		 //!< 
#define G_KEY_F12				85		 //!< 

// Numpad
#define G_KEY_NUMPAD_ADD		86		 //!< 
#define G_KEY_NUMPAD_SUBTRACT	87		 //!< 
#define G_KEY_NUMPAD_MULTIPLY	88		 //!< 
#define G_KEY_NUMPAD_DIVIDE		89	 	 //!< 
#define G_KEY_NUMPAD_0			90 		 //!< 
#define G_KEY_NUMPAD_1		 	91	 	 //!< 
#define G_KEY_NUMPAD_2			92 		 //!< 
#define G_KEY_NUMPAD_3			93 		 //!< 
#define G_KEY_NUMPAD_4			94 		 //!< 
#define G_KEY_NUMPAD_5			95 		 //!< 
#define G_KEY_NUMPAD_6			96 		 //!< 
#define G_KEY_NUMPAD_7			97 		 //!< 
#define G_KEY_NUMPAD_8			98 		 //!< 
#define G_KEY_NUMPAD_9			99 	 	 //!< 
#define G_KEY_NUMPAD_PERIOD		100	 	 //!< 
#define G_KEY_NUMPAD_ENTER		101	 	 //!<

// Special
#define G_KEY_COMMAND           102      //!<
#define G_KEY_FUNCTION          103      //!<

// Mouse 
#define G_BUTTON_LEFT			200		 //!< 
#define G_BUTTON_RIGHT			201		 //!< 
#define G_BUTTON_MIDDLE			202		 //!< 
#define G_MOUSE_SCROLL_UP		203		 //!< 
#define G_MOUSE_SCROLL_DOWN		204		 //!< 

// Key Masks 
#define G_MASK_SHIFT			0		 //!< 
#define G_MASK_CAPS_LOCK		1		 //!< 
#define G_MASK_CONTROL			2		 //!< 
#define G_MASK_SCROLL_LOCK		3		 //!< 
#define G_MASK_NUM_LOCK			4		 //!< 
#define G_MASK_COMMAND          5		 //!< 
#define G_MASK_ALT              6		 //!< 
#define G_MASK_FUNCTION         7		 //!< 

/*! @}*/

/*! \addtogroup GControllerCodes
 *  @{
 */

// All GController Input definitions follow
#define G_SOUTH_BTN						0 //!< 
#define G_EAST_BTN						1 //!< 
#define G_NORTH_BTN						2 //!< 
#define G_WEST_BTN						3 //!< 
#define G_LEFT_SHOULDER_BTN				4 //!< 
#define G_RIGHT_SHOULDER_BTN			5 //!< 
#define G_LEFT_TRIGGER_AXIS				6 //!< 
#define G_RIGHT_TRIGGER_AXIS			7 //!< 
#define G_DPAD_LEFT_BTN					8 //!< 
#define G_DPAD_RIGHT_BTN				9 //!< 
#define G_DPAD_UP_BTN					10 //!< 
#define G_DPAD_DOWN_BTN					11 //!< 
#define G_LEFT_THUMB_BTN				12 //!< 
#define G_RIGHT_THUMB_BTN				13 //!< 
#define G_START_BTN						14 //!< 
#define G_SELECT_BTN					15 //!< 
#define G_LX_AXIS						16 //!< 
#define G_LY_AXIS						17 //!< 
#define G_RX_AXIS						18 //!< 
#define G_RY_AXIS						19 //!< 
#define G_UNKNOWN_INPUT                 255 //!<

/*! @}*/

// Bit-manipulation macros
#define G_CHECK_BIT(var,pos) ((var) & (1<<(pos)))
#define G_TURNON_BIT(var,pos) ((var) |=  (1<<(pos)))
#define G_TURNOFF_BIT(var,pos) ((var) &= ~(1<<(pos)))
#define G_TOGGLE_BIT(var, pos) (G_CHECK_BIT(var, pos) ? (G_TURNOFF_BIT(var,pos)) : (G_TURNON_BIT(var,pos)))

// The interval daemon operation will run at for GInput (Linux only)
#define G_INPUT_OPERATION_INTERVAL 4
// The interval daemon operation will run at for GBufferedInput (Linux only)
#define G_BUFFEREDINPUT_OPERATION_INTERVAL 6
// The interval daemon operation will run at for GController
#define G_CONTROLLER_DAEMON_OPERATION_INTERVAL 4
// The interval daemon operation will run at for GController notify daemon
#define G_CONTROLLER_NOTIFY_DAEMON_OPERATION_INTERVAL 2

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware library interfaces must belong.
	namespace INPUT
	{
		//! GControllerType holds the possible types of game controllers.
		enum class GControllerType
		{
			GENERAL,	//!< 
			PS3,		//!<
			PS4,		//!<
			PS5,		//!<
			STEAM,		//!<
			SWITCHPRO,	//!<
			XBOX360,	//!<
			XBOXONE,	//!<
			XBOXSERIES,	//!<
		};
	}
}
#endif // #endif GINPUTDEFINES_H
