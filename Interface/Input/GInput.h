#ifndef GINPUT_H
#define GINPUT_H

/*!
	File: GInput.h
	Purpose: A Gateware interface that handles high-speed keyboard and mouse input.
	Asynchronous: NO
	Author: Peter Farber
	Contributors: Lari Norri, Alexandr Kozyrev, Ryan Powser, Jordan Teasdale
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"
#include "GInputDefines.h"
#include "../System/GWindow.h"

namespace GW
{
	namespace I
	{
		class GInputInterface : public virtual GInterfaceInterface
		{
		public:
			virtual GReturn GetState(int _keyCode, float& _outState) = 0;
			virtual GReturn GetMouseDelta(float& _x, float& _y) = 0;
			virtual GReturn GetMousePosition(float& _x, float& _y) const = 0;
			virtual GReturn GetKeyMask(unsigned int& _outKeyMask) const = 0;
		};
	}// end I
}// end GW 

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Input/GInput/GInput.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all Gateware input interfaces must belong.
	namespace INPUT
	{
		//! A Gateware interface that handles high-speed keyboard and mouse input.
		class GInput final
			: public I::GProxy<I::GInputInterface, I::GInputImplementation, SYSTEM::UNIVERSAL_WINDOW_HANDLE>
		{
		public: // Only public functions are exposed via Proxy, never protected or private operations
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GInput)
			GATEWARE_FUNCTION(GetState)
			GATEWARE_FUNCTION(GetMouseDelta)
			GATEWARE_CONST_FUNCTION(GetMousePosition)
			GATEWARE_CONST_FUNCTION(GetKeyMask)
			//! \endcond
			
			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a GInput Object.
			/*!
			*	Initializes a handle to a window based on the passed in universal handle.
			*
			*	\param [in] _windowHandle A universal window handle (defined in GSystemDefines.h) that contains a handle to a window (any platform).
			*
			*	\retval GReturn::INVALID_ARGUMENT A window handle in UNIVERSAL_WINDOW_HANDLE was a nullptr.
			*	\retval GReturn::FAILURE Could not make an Input Object.
			*	\retval GReturn::SUCCESS No problems found.
			*/
			GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _windowHandle);

			//! Creates a GInput Object.
			/*!
			*	Initializes a handle to a window based on the reference to a GWindow.
			*
			*	\param [in] _gWindow A reference to a GWindow proxy.
			*
			*	\retval GReturn::FAILURE Failed to obtain a window handle from GWindow.
			*	\retval GReturn::SUCCESS No problems found.
			*/
			GReturn Create(const GW::SYSTEM::GWindow _gWindow);

			//! Get the current state of any key.
			/*!
			*	Use keycodes in GInputDefines as input to this function to check the state
			*	of a particular key or button.
			*
			*	\param [in] _keyCode The key code of the key to check.
			*	\param [out] _outState value > 0 if the key was pressed.
			*
			*	\retval GReturn::SUCCESS no problems found. Values stored _outState
			*/
			virtual GReturn GetState(int _keyCode, float& _outState) = 0;

			//! Get the change in mouse position.
			/*!
			*	\code
			*	GReturn result = input.GetMouseDelta(x, y);
			*	if (GPASS(result) && result != GReturn::REDUNDANT)
			*	{
			*	  // do cool stuff
			*	}
			*	\endcode
			* 
			*	\param [out] _x A reference to a float to store the mouse delta position x.
			*	\param [out] _y A reference to a float to store the mouse delta position y.
			*
			*	\retval GReturn::SUCCESS No problems found. Values stored in _x and _y.
			*	\retval GReturn::REDUNDANT There was no change in mouse position.
			*/
			virtual GReturn GetMouseDelta(float& _x, float& _y) = 0;

			//! Get the most recent mouse position.
			/*!
			*
			*	\param [out] _x a reference to a float to store the mouse position x.
			*	\param [out] _y a reference to a float to store the mouse position y.
			*
			*	\retval GReturn::SUCCESS no problems found. Values stored in _x and _y.
			*/
			virtual GReturn GetMousePosition(float& _x, float& _y) const = 0;

			//! Get the key mask.
			/*!
			*	The key mask lets the input object know which of the functions below are
			*	active by manipulating individual bits of an unsigned int.
			*	Values for G_MASK can be found in GInputDefines.
			*
			*	\param [out] _outKeyMask G_MASK (_SHIFT, _CONTROL, _CAPS_LOCK, _NUM_LOCK, _SCROLL_LOCK).
			*
			*	\retval GReturn::SUCCESS no problems found.
			*/
			virtual GReturn GetKeyMask(unsigned int& _outKeyMask) const = 0;
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GINPUT_H
