#ifndef GMATH2DDEFINES_H
#define GMATH2DDEFINES_H

/*!
	File: GMath2DDefines.h
	Purpose: A Gateware 2D math define header that handles 2D math structs, such as vector and matrix.
	Author: Ryan Powser
	Contributors: 
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#define G2D_PI					3.14159265358979323846 //G2D_PI is deprecated! Use G2D_PI_D or G2D_PI_F instead. 
#define G2D_PI_D				3.14159265358979323846
#define G2D_PI_F					3.141592741f

#define G2D_PI_R					0.318309886183790671538 //G2D_PI_R is deprecated! Use G2D_PI_R_D or G2D_PI_R_F instead. 
#define G2D_PI_R_D				0.318309886183790671538
#define G2D_PI_R_F					0.3183098733f

#define G2D_EPSILON_F				1.192092896e-07F
#define G2D_EPSILON_D				2.2204460492503131e-016

#define G2D_ABS(num)				( ( (num) > 0 ) ?  (num) : (-(num)) )	//RETURN THE ABSOLUTE VALUE OF THE INPUT NUMBER
#define G2D_MAX(A, B)			( ( (A) > (B) ) ?  (A) : (B) )			//RETURN THE LARGER INPUT NUMBER
#define G2D_MIN(A, B)			( ( (A) < (B) ) ?  (A) : (B) )			//RETURN THE SMALLER INPUT NUMBER
#define G2D_ABS_MAX(A, B)		( ( G2D_ABS(A) > G2D_ABS(B) ) ?  G2D_ABS(A) : G2D_ABS(B) )

#define G2D_DEVIATION_EXACT 0
#define G2D_DEVIATION_PRECISE_F G2D_EPSILON_F
#define G2D_DEVIATION_STANDARD_F (G2D_EPSILON_F * 10)
#define G2D_DEVIATION_LOOSE_F (G2D_EPSILON_F * 100)

#define G2D_DEVIATION_PRECISE_D G2D_EPSILON_D
#define G2D_DEVIATION_STANDARD_D (G2D_EPSILON_D * 10)
#define G2D_DEVIATION_LOOSE_D (G2D_EPSILON_D * 100)

#define G2D_FIRST_COMPARISON_F(num1 , num2)   ( G2D_ABS( (num1) - (num2) ) <= ( G2D_EPSILON_F ) )	//FIRST CHECK IF TWO INPUT FLOATS' DIFF ARE LESS THAN EPSILON
#define G2D_SECOND_COMPARISON_F(num1 , num2)   ( G2D_ABS( (num1) - (num2) ) <= ( G2D_DEVIATION_STANDARD_F * G2D_ABS_MAX(num1, num2)) ) 	//SECOND CHECK IF TWO INPUT FLOATS' DIFF ARE LESS THAN EPSILON MULTIPLY THE LARGER INPUT FLOAT
#define G2D_COMPARISON_STANDARD_F(num1 , num2)   ( G2D_FIRST_COMPARISON_F( (num1), (num2) ) ? true : G2D_SECOND_COMPARISON_F( (num1) , (num2) ) )
#define G2D_COMPARISON_F(num1, num2, deviation) ( G2D_FIRST_COMPARISON_F( (num1), (num2) )	? true :  ( G2D_ABS( (num1) - (num2) ) <=  ( deviation * G2D_ABS_MAX(num1, num2)) )  )

#define G2D_FIRST_COMPARISON_D(num1 , num2)   ( G2D_ABS( (num1) - (num2) ) <= ( G2D_EPSILON_D ) )	//FIRST CHECK IF TWO INPUT FLOATS' DIFF ARE LESS THAN EPSILON
#define G2D_SECOND_COMPARISON_D(num1 , num2)   ( G2D_ABS( (num1) - (num2) ) <= ( G2D_DEVIATION_STANDARD_D * G2D_ABS_MAX(num1, num2)) ) //SECOND CHECK IF TWO INPUT FLOATS' DIFF ARE LESS THAN EPSILON MULTIPLY THE LARGER INPUT FLOAT
#define G2D_COMPARISON_STANDARD_D(num1 , num2)   ( G2D_FIRST_COMPARISON_D( (num1), (num2) ) ? true : G2D_SECOND_COMPARISON_D( (num1) , (num2) ) )
#define G2D_COMPARISON_D(num1 , num2, deviation)   ( G2D_FIRST_COMPARISON_D( (num1), (num2) ) ? true :( G2D_ABS( (num1) - (num2) ) <= ( deviation * (G2D_ABS_MAX(num1, num2))) )  )

#define G2D_LERP(start , end, ratio)  ( start + ratio * (end - start) )		//LINEAR INTERPOLATE TWO POINT WITH THE RATIO
#define G2D_CLAMP(num, min, max)  (((num) > (max)) ? (max) : (((num) < (min)) ? (min) : (num)))	//CLAMP THE NUMBER BETWEEN THE TOP NUMBER AND THE BOTTOM NUMBER

#define G2D_DEGREE_TO_RADIAN(degree) ( (degree) * 0.01745329251994329576923690768489) //G2D_DEGREE_TO_RADIAN is deprecated! Use G2D_DEGREE_TO_RADIAN_D or G2D_DEGREE_TO_RADIAN_F instead. 
#define G2D_DEGREE_TO_RADIAN_D(degree) ( (degree) * 0.01745329251994329576923690768489)
#define G2D_DEGREE_TO_RADIAN_F(degree) ( (degree) * 0.01745329238f)

#define G2D_RADIAN_TO_DEGREE(radian) ( (radian) * 57.295779513082320876798154814105) //G2D_RADIAN_TO_DEGREE is deprecated! UseG2D_RADIAN_TO_DEGREE_D or G2D_RADIAN_TO_DEGREE_F instead. 
#define G2D_RADIAN_TO_DEGREE_D(radian) ( (radian) * 57.295779513082320876798154814105)
#define G2D_RADIAN_TO_DEGREE_F(radian) ( (radian) * 57.29578018f )




//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The namespace to which all 2D math library interface must belong.
	namespace MATH2D
	{
		/*! \addtogroup MathStructures
		*  @{
		*/

		// Ensure identical byte alignment for structures on all platforms.
#pragma pack(push, 1)

		/*! Vector with 2 float elements. */
		struct GVECTOR2F
		{
			union 
			{
				struct
				{
					float x;
					float y;
				};
				float data[2];
			};
		};

		/*! Vector with 2 double elements. */
		struct GVECTOR2D
		{
			union
			{
				struct
				{
					double x;
					double y;
				};
				double data[2];
			};
		};

		/*! Vector with 3 float elements. */
		struct GVECTOR3F
		{
			union
			{
				struct
				{
					float x;
					float y;
					float z;
				};
				float data[3];
			};

			inline const GVECTOR2F xy() const
			{
				return GVECTOR2F{{{this->x, this->y}}};
			}
		};

		/*! Vector with 3 double elements. */
		struct GVECTOR3D
		{
			union
			{
				struct
				{
					double x;
					double y;
					double z;
				};
				double data[3];
			};

			inline const GVECTOR2D xy() const
			{
				return GVECTOR2D{ {{this->x, this->y}} };
			}
		};

		/*! Matrix with 2 float vector2s each representing a row. */
		struct GMATRIX2F
		{
			union
			{
				struct
				{
					GVECTOR2F row1;
					GVECTOR2F row2;
				};
				float data[4];
			};
		};

		/*! Matrix with 2 double vector2s each representing a row. */
		struct GMATRIX2D
		{
			union
			{
				struct
				{
					GVECTOR2D row1;
					GVECTOR2D row2;
				};
				double data[4];
			};
		};

		/*! Matrix with 3 float vector3s each representing a row. */
		struct GMATRIX3F
		{
			union
			{
				struct
				{
					GVECTOR3F row1;
					GVECTOR3F row2;
					GVECTOR3F row3;
				};
				float data[9];
			};
		};

		/*! Matrix with 3 double vector3s each representing a row. */
		struct GMATRIX3D
		{
			union
			{
				struct
				{
					GVECTOR3D row1;
					GVECTOR3D row2;
					GVECTOR3D row3;
				};
				double data[9];
			};
		};

		/*! Line with 2 float vector2s representing start and end. */
		struct GLINE2F
		{
			GVECTOR2F start;
			GVECTOR2F end;
		};

		/*! Line with 2 double vector2s representing start and end. */
		struct GLINE2D
		{
			GVECTOR2D start;
			GVECTOR2D end;
		};

		/*! Ray with 2 float vector2s representing starting position and direction. */
		struct GRAY2F
		{
			GVECTOR2F pos;
			GVECTOR2F dir;
		};

		/*! Ray with 2 double vector2s representing starting position and direction. */
		struct GRAY2D
		{
			GVECTOR2D pos;
			GVECTOR2D dir;
		};

		/*! Circle with a float vector2 for position and a float for radius. */
		struct GCIRCLE2F
		{
			union 
			{
				struct 
				{
					GVECTOR2F pos;
					float radius;
				};
				GVECTOR3F data;
			};
		};

		/*! Circle with a double vector2 for position and a double for radius. */
		struct GCIRCLE2D
		{
			union
			{
				struct
				{
					GVECTOR2D pos;
					double radius;
				};
				GVECTOR3D data;
			};
		};

		/*! Capsule with 2 float vector2s representing start and end, and a float for radius. */
		struct GCAPSULE2F
		{
			GVECTOR2F start;
			GVECTOR2F end;
			float radius;
		};
		
		/*! Capsule with 2 double vector2s representing start and end, and a double for radius. */
		struct GCAPSULE2D
		{
			GVECTOR2D start;
			GVECTOR2D end;
			double radius;
		};

		/*! Rectangle with 2 float vector2s representing min xy and max xy. */
		struct GRECTANGLE2F
		{
			GVECTOR2F min;
			GVECTOR2F max;
		};

		/*! Rectangle with 2 double vector2s representing min xy and max xy. */
		struct GRECTANGLE2D
		{
			GVECTOR2D min;
			GVECTOR2D max;
		};

		/*! Container for barycentric coordinates with 3 floats representing alpha, beta and gamma. */
		struct GBARYCENTRICF
		{
			union
			{
				struct
				{
					float alpha;
					float beta;
					float gamma;
				};
				float data[3];
			};
		};

		/*! Container for barycentric coordinates with 3 doubles representing alpha, beta and gamma. */
		struct GBARYCENTRICD
		{
			union
			{
				struct
				{
					double alpha;
					double beta;
					double gamma;
				};
				double data[3];
			};
		};

#pragma pack(pop)
		static const GVECTOR2F GZeroVector2F{ {{0,0}} };
		static const GVECTOR2D GZeroVector2D{ {{0,0}} };
		static const GVECTOR3F GZeroVector3F{ {{0,0,0}} };
		static const GVECTOR3D GZeroVector3D{ {{0,0,0}} };

		static const GMATRIX2F GIdentityMatrix2F{ {{{{{1,0}}},{{{0,1}}}}} };
		static const GMATRIX2D GIdentityMatrix2D{ {{{{{1,0}}},{{{0,1}}}}} };
		static const GMATRIX3F GIdentityMatrix3F{ {{{{{1,0,0}}},{{{0,1,0}}},{{{0,0,1}}}}} };
		static const GMATRIX3D GIdentityMatrix3D{ {{{{{1,0,0}}},{{{0,1,0}}},{{{0,0,1}}}}} };

		static const GMATRIX2F GZeroMatrix2F{ {{{{{0,0}}},{{{0,0}}}}} };
		static const GMATRIX2D GZeroMatrix2D{ {{{{{0,0}}},{{{0,0}}}}} };
		static const GMATRIX3F GZeroMatrix3F{ {{{{{0,0,0}}},{{{0,0,0}}},{{{0,0,0}}}}} };
		static const GMATRIX3D GZeroMatrix3D{ {{{{{0,0,0}}},{{{0,0,0}}},{{{0,0,0}}}}} };
		/*! @} */
	}
}
#endif // GMATH2DDEFINES_H