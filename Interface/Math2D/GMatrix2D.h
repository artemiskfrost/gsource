#ifndef GMATRIX2D_H
#define GMATRIX2D_H

/*!
	File: GMatrix2D.h
	Purpose: A Gateware interface that handles 2D Matrix functions.
	Asynchronous: NO
	Author: Ryan Powser
	Contributors:
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"
#include "GMath2DDefines.h"

namespace GW
{
	namespace I
	{
		class GMatrix2DInterface : public virtual GInterfaceInterface
		{
		public:
			// Floats
			static GReturn Add2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Add3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Subtract2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Subtract3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Multiply2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Multiply3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MatrixXVector2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MatrixXVector3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyNum2F(MATH2D::GMATRIX2F _matrix, float _scalar, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyNum3F(MATH2D::GMATRIX3F _matrix, float _scalar, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Determinant2F(MATH2D::GMATRIX2F _matrix, float& _outDeterminant) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Determinant3F(MATH2D::GMATRIX3F _matrix, float& _outDeterminant) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Transpose2F(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Transpose3F(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Inverse2F(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Inverse3F(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetRotation2F(MATH2D::GMATRIX2F _matrix, float& _outRadians) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetRotation3F(MATH2D::GMATRIX3F _matrix, float& _outRadians) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetTranslation3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetScale2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outScale) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetScale3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outScale) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetSkew2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outSkew) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetSkew3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outSkew) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Rotate2F(MATH2D::GMATRIX2F _matrix, float _radians, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateGlobal3F(MATH2D::GMATRIX3F _matrix, float _radians, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateLocal3F(MATH2D::GMATRIX3F _matrix, float _radians, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TranslateGlobal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TranslateLocal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Scale2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleGlobal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleLocal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Lerp2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, float _ratio, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Lerp3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, float _ratio, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeRelative2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeRelative3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeSeparate2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeSeparate3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Upgrade2(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Upgrade3(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			// Doubles
			static GReturn Add2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Add3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Subtract2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Subtract3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Multiply2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Multiply3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MatrixXVector2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MatrixXVector3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyNum2D(MATH2D::GMATRIX2D _matrix, double _scalar, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MultiplyNum3D(MATH2D::GMATRIX3D _matrix, double _scalar, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Determinant2D(MATH2D::GMATRIX2D _matrix, double& _outDeterminant) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Determinant3D(MATH2D::GMATRIX3D _matrix, double& _outDeterminant) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Transpose2D(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Transpose3D(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Inverse2D(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Inverse3D(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetRotation2D(MATH2D::GMATRIX2D _matrix, double& _outRadians) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetRotation3D(MATH2D::GMATRIX3D _matrix, double& _outRadians) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetTranslation3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetScale2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outScale) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetScale3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outScale) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetSkew2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outSkew) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GetSkew3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outSkew) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Rotate2D(MATH2D::GMATRIX2D _matrix, double _radians, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateGlobal3D(MATH2D::GMATRIX3D _matrix, double _radians, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn RotateLocal3D(MATH2D::GMATRIX3D _matrix, double _radians, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TranslateGlobal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TranslateLocal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Scale2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleGlobal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn ScaleLocal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Lerp2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, double _ratio, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Lerp3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, double _ratio, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeRelative2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeRelative3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeSeparate2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn MakeSeparate3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Downgrade2(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Downgrade3(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
		};
	}
}

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Math2D/GMatrix2D/GMatrix2D.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace MATH2D
	{
		//! A Gateware interface that handles 2D Matrix functions.
		class GMatrix2D final : public I::GProxy<I::GMatrix2DInterface, I::GMatrix2DImplementation>
		{
		public:
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GMatrix2D)
			// floats
			GATEWARE_STATIC_FUNCTION(Add2F)
			GATEWARE_STATIC_FUNCTION(Add3F)
			GATEWARE_STATIC_FUNCTION(Subtract2F)
			GATEWARE_STATIC_FUNCTION(Subtract3F)
			GATEWARE_STATIC_FUNCTION(Multiply2F)
			GATEWARE_STATIC_FUNCTION(Multiply3F)
			GATEWARE_STATIC_FUNCTION(MatrixXVector2F)
			GATEWARE_STATIC_FUNCTION(MatrixXVector3F)
			GATEWARE_STATIC_FUNCTION(MultiplyNum2F)
			GATEWARE_STATIC_FUNCTION(MultiplyNum3F)
			GATEWARE_STATIC_FUNCTION(Determinant2F)
			GATEWARE_STATIC_FUNCTION(Determinant3F)
			GATEWARE_STATIC_FUNCTION(Transpose2F)
			GATEWARE_STATIC_FUNCTION(Transpose3F)
			GATEWARE_STATIC_FUNCTION(Inverse2F)
			GATEWARE_STATIC_FUNCTION(Inverse3F)
			GATEWARE_STATIC_FUNCTION(GetRotation2F)
			GATEWARE_STATIC_FUNCTION(GetRotation3F)
			GATEWARE_STATIC_FUNCTION(GetTranslation3F)
			GATEWARE_STATIC_FUNCTION(GetScale2F)
			GATEWARE_STATIC_FUNCTION(GetScale3F)
			GATEWARE_STATIC_FUNCTION(GetSkew2F)
			GATEWARE_STATIC_FUNCTION(GetSkew3F)
			GATEWARE_STATIC_FUNCTION(Rotate2F)
			GATEWARE_STATIC_FUNCTION(RotateGlobal3F)
			GATEWARE_STATIC_FUNCTION(RotateLocal3F)
			GATEWARE_STATIC_FUNCTION(TranslateGlobal3F)
			GATEWARE_STATIC_FUNCTION(TranslateLocal3F)
			GATEWARE_STATIC_FUNCTION(Scale2F)
			GATEWARE_STATIC_FUNCTION(ScaleGlobal3F)
			GATEWARE_STATIC_FUNCTION(ScaleLocal3F)
			GATEWARE_STATIC_FUNCTION(Lerp2F)
			GATEWARE_STATIC_FUNCTION(Lerp3F)
			GATEWARE_STATIC_FUNCTION(MakeRelative2F)
			GATEWARE_STATIC_FUNCTION(MakeRelative3F)
			GATEWARE_STATIC_FUNCTION(MakeSeparate2F)
			GATEWARE_STATIC_FUNCTION(MakeSeparate3F)
			GATEWARE_STATIC_FUNCTION(Upgrade2)
			GATEWARE_STATIC_FUNCTION(Upgrade3)


			// doubles
			GATEWARE_STATIC_FUNCTION(Add2D)
			GATEWARE_STATIC_FUNCTION(Add3D)
			GATEWARE_STATIC_FUNCTION(Subtract2D)
			GATEWARE_STATIC_FUNCTION(Subtract3D)
			GATEWARE_STATIC_FUNCTION(Multiply2D)
			GATEWARE_STATIC_FUNCTION(Multiply3D)
			GATEWARE_STATIC_FUNCTION(MatrixXVector2D)
			GATEWARE_STATIC_FUNCTION(MatrixXVector3D)
			GATEWARE_STATIC_FUNCTION(MultiplyNum2D)
			GATEWARE_STATIC_FUNCTION(MultiplyNum3D)
			GATEWARE_STATIC_FUNCTION(Determinant2D)
			GATEWARE_STATIC_FUNCTION(Determinant3D)
			GATEWARE_STATIC_FUNCTION(Transpose2D)
			GATEWARE_STATIC_FUNCTION(Transpose3D)
			GATEWARE_STATIC_FUNCTION(Inverse2D)
			GATEWARE_STATIC_FUNCTION(Inverse3D)
			GATEWARE_STATIC_FUNCTION(GetRotation2D)
			GATEWARE_STATIC_FUNCTION(GetRotation3D)
			GATEWARE_STATIC_FUNCTION(GetTranslation3D)
			GATEWARE_STATIC_FUNCTION(GetScale2D)
			GATEWARE_STATIC_FUNCTION(GetScale3D)
			GATEWARE_STATIC_FUNCTION(GetSkew2D)
			GATEWARE_STATIC_FUNCTION(GetSkew3D)
			GATEWARE_STATIC_FUNCTION(Rotate2D)
			GATEWARE_STATIC_FUNCTION(RotateGlobal3D)
			GATEWARE_STATIC_FUNCTION(RotateLocal3D)
			GATEWARE_STATIC_FUNCTION(TranslateGlobal3D)
			GATEWARE_STATIC_FUNCTION(TranslateLocal3D)
			GATEWARE_STATIC_FUNCTION(Scale2D)
			GATEWARE_STATIC_FUNCTION(ScaleGlobal3D)
			GATEWARE_STATIC_FUNCTION(ScaleLocal3D)
			GATEWARE_STATIC_FUNCTION(Lerp2D)
			GATEWARE_STATIC_FUNCTION(Lerp3D)
			GATEWARE_STATIC_FUNCTION(MakeRelative2D)
			GATEWARE_STATIC_FUNCTION(MakeRelative3D)
			GATEWARE_STATIC_FUNCTION(MakeSeparate2D)
			GATEWARE_STATIC_FUNCTION(MakeSeparate3D)
			GATEWARE_STATIC_FUNCTION(Downgrade2)
			GATEWARE_STATIC_FUNCTION(Downgrade3)
			//! \endcond


			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a valid GMatrix2D Object. 
			/*!
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory)
			*/
			GReturn Create();

			// Floats

			//! Add two float matrix2s
			/*!
			*	Adds _matrix1 to _matrix2 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Add2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Add two float matrix3s
			/*!
			*	Adds _matrix1 to _matrix2 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Add3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two float matrix2s
			/*!
			*	Subtracts _matrix2 from _matrix1 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the subtraction
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Subtract2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two float matrix3s
			/*!
			*	Subtracts _matrix2 from _matrix1 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the subtraction
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Subtract3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply two float matrix2s
			/*!
			*	Multiplies _matrix1 by _matrix2 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Multiply2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply two float matrix3s
			/*!
			*	Multiplies _matrix1 by _matrix2 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Multiply3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a float matrix2 by a float vector2
			/*!
			*	Multiplies _vector by _matrix and stores result in _outVector.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MatrixXVector2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a float matrix3 by a float vector3
			/*!
			*	Multiplies _vector by _matrix and stores result in _outVector.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MatrixXVector3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a float matrix2 by a float scalar
			/*!
			*	Multiplies _matrix by _scalar and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _scalar		The scalar
			*	\param [out] _outMatrix		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MultiplyNum2F(MATH2D::GMATRIX2F _matrix, float _scalar, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a float matrix3 by a float scalar
			/*!
			*	Multiplies _matrix by _scalar and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _scalar		The scalar
			*	\param [out] _outMatrix		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MultiplyNum3F(MATH2D::GMATRIX3F _matrix, float _scalar, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute the determinant of a float matrix2
			/*!
			*	Calculates the determinant of _matrix and stores result in _outDeterminant.
			*
			*	\param [in]  _matrix			The matrix
			*	\param [out] _outDeterminant	The result of the calculation
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn Determinant2F(MATH2D::GMATRIX2F _matrix, float& _outDeterminant) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute the determinant of a float matrix3
			/*!
			*	Calculates the determinant of _matrix and stores result in _outDeterminant.
			*
			*	\param [in]  _matrix			The matrix
			*	\param [out] _outDeterminant	The result of the calculation
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn Determinant3F(MATH2D::GMATRIX3F _matrix, float& _outDeterminant) { return GReturn::NO_IMPLEMENTATION; }

			//! Transpose a float matrix2
			/*!
			*	Transposes _matrix and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outMatrix		The result of the transposition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Transpose2F(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Transpose a float matrix3
			/*!
			*	Transposes _matrix and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outMatrix		The result of the transposition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Transpose3F(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Invert a float matrix2
			/*!
			*	Inverts _matrix and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outMatrix		The result of the inversion
			*
			*	\retval GReturn::FAILURE				The determinant of _matrix was 0, _matrix is not invertible
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Inverse2F(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Invert a float matrix3
			/*!
			*	Inverts _matrix and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outMatrix		The result of the inversion
			*
			*	\retval GReturn::FAILURE				The determinant of _matrix was 0, _matrix is not invertible
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Inverse3F(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a float rotation in radians from a matrix2
			/*!
			*	Calculates the rotation of _matrix and stores result in _outRadians.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outRadians	The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetRotation2F(MATH2D::GMATRIX2F _matrix, float& _outRadians) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a float rotation in radians from a matrix3
			/*!
			*	Calculates the rotation of _matrix and stores the result in _outRadians.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outRadians	The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetRotation3F(MATH2D::GMATRIX3F _matrix, float& _outRadians) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a translation vector2 from a matrix3
			/*!
			*	Copies the x and y components of row3 of _matrix to _outVector.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the copy
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetTranslation3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a scale vector2 from a matrix2
			/*!
			*	Calculates the scale of _matrix and stores the result in _outScale.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outScale		The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetScale2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outScale) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a scale vector2 from a matrix3
			/*!
			*	Calculates the scale of _matrix and stores the result in _outScale.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outScale		The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetScale3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outScale) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a skew vector2 from a matrix2
			/*!
			*	Calculates the skew of _matrix and stores the result in _outSkew.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outSkew		The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetSkew2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outSkew) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a scale vector2 from a matrix3
			/*!
			*	Calculates the skew of _matrix and stores the result in _outSkew.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outSkew		The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetSkew3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F& _outSkew) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate a matrix2
			/*!
			*	Rotates _matrix by _radians and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _radians		The amount of radians to rotate by
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Rotate2F(MATH2D::GMATRIX2F _matrix, float _radians, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate a matrix3, globally
			/*!
			*	Rotates _matrix by _radians and stores the result in _outMatrix. Preserves translation.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _radians		The amount of radians to rotate by
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateGlobal3F(MATH2D::GMATRIX3F _matrix, float _radians, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate a matrix3, locally
			/*!
			*	Rotates _matrix by _radians and stores the result in _outMatrix. Does not preserve translation.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _radians		The amount of radians to rotate by
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateLocal3F(MATH2D::GMATRIX3F _matrix, float _radians, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Translate a matrix3, locally
			/*!
			*	Translates _matrix by _vector and stores the result in _outMatrix. Effectively _matrix * translationMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the translation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TranslateGlobal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Translate a matrix3, globally
			/*!
			*	Translates _matrix by _vector and stores the result in _outMatrix. Effectively translationMatrix * _matrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the translation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TranslateLocal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a matrix2
			/*!
			*	Scales _matrix by _vector and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Scale2F(MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a matrix3, globally
			/*!
			*	Scales _matrix by _vector and stores the result in _outMatrix. Preserves translation.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleGlobal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a matrix3, locally
			/*!
			*	Scales _matrix by _vector and stores the result in _outMatrix. Does not preserve translation.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleLocal3F(MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Lerp between two matrix2s
			/*!
			*	Performs a linear interpolation on every element in the input matrices and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [in]  _ratio			The ratio
			*	\param [out] _outMatrix		The result of the lerp
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Lerp2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, float _ratio, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Lerp between two matrix3s
			/*!
			*	Performs a linear interpolation on every element in the input matrices and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [in]  _ratio			The ratio
			*	\param [out] _outMatrix		The result of the lerp
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Lerp3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, float _ratio, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Make a matrix2 relative to another matrix2
			/*!
			*	Performs _matrix1 * Inverse(_matrix2) and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the calculation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeRelative2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Make a matrix3 relative to another matrix3
			/*!
			*	Performs _matrix1 * Inverse(_matrix2) and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the calculation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeRelative3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Separate a matrix2 from being relative to another matrix2
			/*!
			*	Performs _matrix1 * _matrix2 and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the calculation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeSeparate2F(MATH2D::GMATRIX2F _matrix1, MATH2D::GMATRIX2F _matrix2, MATH2D::GMATRIX2F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Separate a matrix3 from being relative to another matrix3
			/*!
			*	Performs _matrix1 * _matrix2 and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the calculation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeSeparate3F(MATH2D::GMATRIX3F _matrix1, MATH2D::GMATRIX3F _matrix2, MATH2D::GMATRIX3F& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Upgrade a float matrix2 to a double matrix2
			/*!
			*	Performs a static_cast<double> on all elements in _matrix and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix		The first matrix
			*	\param [out] _outMatrix		The result of the cast
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Upgrade2(MATH2D::GMATRIX2F _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Upgrade a float matrix3 to a double matrix3
			/*!
			*	Performs a static_cast<double> on all elements in _matrix and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix		The first matrix
			*	\param [out] _outMatrix		The result of the cast
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Upgrade3(MATH2D::GMATRIX3F _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }










			// Doubles
			
			//! Add two double matrix2s
			/*!
			*	Adds _matrix1 to _matrix2 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Add2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Add two double matrix3s
			/*!
			*	Adds _matrix1 to _matrix2 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Add3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two double matrix2s
			/*!
			*	Subtracts _matrix2 from _matrix1 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the subtraction
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Subtract2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two double matrix3s
			/*!
			*	Subtracts _matrix2 from _matrix1 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the subtraction
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Subtract3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply two double matrix2s
			/*!
			*	Multiplies _matrix1 by _matrix2 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Multiply2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply two double matrix3s
			/*!
			*	Multiplies _matrix1 by _matrix2 and stores result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Multiply3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a double matrix2 by a double vector2
			/*!
			*	Multiplies _vector by _matrix and stores result in _outVector.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MatrixXVector2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a double matrix3 by a double vector3
			/*!
			*	Multiplies _vector by _matrix and stores result in _outVector.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MatrixXVector3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a double matrix2 by a double scalar
			/*!
			*	Multiplies _matrix by _scalar and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _scalar		The scalar
			*	\param [out] _outMatrix		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MultiplyNum2D(MATH2D::GMATRIX2D _matrix, double _scalar, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a double matrix3 by a double scalar
			/*!
			*	Multiplies _matrix by _scalar and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _scalar		The scalar
			*	\param [out] _outMatrix		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MultiplyNum3D(MATH2D::GMATRIX3D _matrix, double _scalar, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute the determinant of a double matrix2
			/*!
			*	Calculates the determinant of _matrix and stores result in _outDeterminant.
			*
			*	\param [in]  _matrix			The matrix
			*	\param [out] _outDeterminant	The result of the calculation
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn Determinant2D(MATH2D::GMATRIX2D _matrix, double& _outDeterminant) { return GReturn::NO_IMPLEMENTATION; }

			//! Compute the determinant of a double matrix3
			/*!
			*	Calculates the determinant of _matrix and stores result in _outDeterminant.
			*
			*	\param [in]  _matrix			The matrix
			*	\param [out] _outDeterminant	The result of the calculation
			*
			*	\retval GReturn::SUCCESS					The calculation succeeded
			*/
			static GReturn Determinant3D(MATH2D::GMATRIX3D _matrix, double& _outDeterminant) { return GReturn::NO_IMPLEMENTATION; }

			//! Transpose a double matrix2
			/*!
			*	Transposes _matrix and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outMatrix		The result of the transposition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Transpose2D(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Transpose a double matrix3
			/*!
			*	Transposes _matrix and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outMatrix		The result of the transposition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Transpose3D(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Invert a double matrix2
			/*!
			*	Inverts _matrix and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outMatrix		The result of the inversion
			*
			*	\retval GReturn::FAILURE				The determinant of _matrix was 0, _matrix is not invertible
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Inverse2D(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Invert a double matrix3
			/*!
			*	Inverts _matrix and stores result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outMatrix		The result of the inversion
			*
			*	\retval GReturn::FAILURE				The determinant of _matrix was 0, _matrix is not invertible
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Inverse3D(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a double rotation in radians from a matrix2
			/*!
			*	Calculates the rotation of _matrix and stores result in _outRadians.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outRadians	The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetRotation2D(MATH2D::GMATRIX2D _matrix, double& _outRadians) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a double rotation in radians from a matrix3
			/*!
			*	Calculates the rotation of _matrix and stores the result in _outRadians.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outRadians	The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetRotation3D(MATH2D::GMATRIX3D _matrix, double& _outRadians) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a translation vector2 from a matrix3
			/*!
			*	Copies the x and y components of row3 of _matrix to _outVector.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the copy
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetTranslation3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a scale vector2 from a matrix2
			/*!
			*	Calculates the scale of _matrix and stores the result in _outScale.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outScale		The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetScale2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outScale) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a scale vector2 from a matrix3
			/*!
			*	Calculates the scale of _matrix and stores the result in _outScale.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outScale		The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetScale3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outScale) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a skew vector2 from a matrix2
			/*!
			*	Calculates the skew of _matrix and stores the result in _outSkew.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outSkew		The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetSkew2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outSkew) { return GReturn::NO_IMPLEMENTATION; }

			//! Retrieve a scale vector2 from a matrix3
			/*!
			*	Calculates the skew of _matrix and stores the result in _outSkew.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outSkew		The result of the calculation
			*
			*	\retval GReturn::FAILURE				x1 = y1 = x2 = y2 = 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GetSkew3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D& _outSkew) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate a matrix2
			/*!
			*	Rotates _matrix by _radians and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _radians		The amount of radians to rotate by
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Rotate2D(MATH2D::GMATRIX2D _matrix, double _radians, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate a matrix3, globally
			/*!
			*	Rotates _matrix by _radians and stores the result in _outMatrix. Preserves translation.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _radians		The amount of radians to rotate by
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateGlobal3D(MATH2D::GMATRIX3D _matrix, double _radians, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Rotate a matrix3, locally
			/*!
			*	Rotates _matrix by _radians and stores the result in _outMatrix. Does not preserve translation.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _radians		The amount of radians to rotate by
			*	\param [out] _outMatrix		The result of the rotation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn RotateLocal3D(MATH2D::GMATRIX3D _matrix, double _radians, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Translate a matrix3, locally
			/*!
			*	Translates _matrix by _vector and stores the result in _outMatrix. Effectively _matrix * translationMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the translation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TranslateGlobal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Translate a matrix3, globally
			/*!
			*	Translates _matrix by _vector and stores the result in _outMatrix. Effectively translationMatrix * _matrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the translation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn TranslateLocal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a matrix2
			/*!
			*	Scales _matrix by _vector and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Scale2D(MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a matrix3, globally
			/*!
			*	Scales _matrix by _vector and stores the result in _outMatrix. Preserves translation.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleGlobal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a matrix3, locally
			/*!
			*	Scales _matrix by _vector and stores the result in _outMatrix. Does not preserve translation.
			*
			*	\param [in]  _matrix		The matrix
			*	\param [in]  _vector		The vector
			*	\param [out] _outMatrix		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn ScaleLocal3D(MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Lerp between two matrix2s
			/*!
			*	Performs a linear interpolation on every element in the input matrices and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [in]  _ratio			The ratio
			*	\param [out] _outMatrix		The result of the lerp
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Lerp2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, double _ratio, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Lerp between two matrix3s
			/*!
			*	Performs a linear interpolation on every element in the input matrices and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [in]  _ratio			The ratio
			*	\param [out] _outMatrix		The result of the lerp
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Lerp3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, double _ratio, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Make a matrix2 relative to another matrix2
			/*!
			*	Performs _matrix1 * Inverse(_matrix2) and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the calculation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeRelative2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Make a matrix3 relative to another matrix3
			/*!
			*	Performs _matrix1 * Inverse(_matrix2) and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the calculation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeRelative3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Separate a matrix2 from being relative to another matrix2
			/*!
			*	Performs _matrix1 * _matrix2 and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the calculation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeSeparate2D(MATH2D::GMATRIX2D _matrix1, MATH2D::GMATRIX2D _matrix2, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Separate a matrix3 from being relative to another matrix3
			/*!
			*	Performs _matrix1 * _matrix2 and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix1		The first matrix
			*	\param [in]  _matrix2		The second matrix
			*	\param [out] _outMatrix		The result of the calculation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn MakeSeparate3D(MATH2D::GMATRIX3D _matrix1, MATH2D::GMATRIX3D _matrix2, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Downgrade a double matrix2 to a float matrix2
			/*!
			*	Performs a static_cast<float> on all elements in _matrix and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix		The first matrix
			*	\param [out] _outMatrix		The result of the cast
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Downgrade2(MATH2D::GMATRIX2D _matrix, MATH2D::GMATRIX2D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }

			//! Downgrade a double matrix3 to a float matrix3
			/*!
			*	Performs a static_cast<float> on all elements in _matrix and stores the result in _outMatrix.
			*
			*	\param [in]  _matrix		The first matrix
			*	\param [out] _outMatrix		The result of the cast
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Downgrade3(MATH2D::GMATRIX3D _matrix, MATH2D::GMATRIX3D& _outMatrix) { return GReturn::NO_IMPLEMENTATION; }
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // #endif GMATRIX_H