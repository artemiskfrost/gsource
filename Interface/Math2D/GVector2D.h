#ifndef GVECTOR2D_H
#define GVECTOR2D_H

/*!
	File: GVector2D.h
	Purpose: A Gateware interface that handles all 2D vector functions.
	Asynchronous: NO
	Author: Ryan Powser
	Contributors:
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"
#include "GMath2DDefines.h"

namespace GW
{
	namespace I
	{
		class GVector2DInterface : public virtual GInterfaceInterface
		{
		public:
			// floats
			static GReturn Add2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Add3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Subtract2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Subtract3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Scale2F(MATH2D::GVECTOR2F _vector, float _scalar, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Scale3F(MATH2D::GVECTOR3F _vector, float _scalar, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Dot2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Dot3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Cross2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Cross3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn VectorXMatrix2F(MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn VectorXMatrix3F(MATH2D::GVECTOR3F _vector, MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Transform2F(MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Transform3F(MATH2D::GVECTOR3F _vector, MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Magnitude2F(MATH2D::GVECTOR2F _vector, float& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Magnitude3F(MATH2D::GVECTOR3F _vector, float& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Normalize2F(MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Normalize3F(MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn LerpF(float _value1, float _value2, float _ratio, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Lerp2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float _ratio, MATH2D::GVECTOR2F& _outVector){ return GReturn::NO_IMPLEMENTATION; }
			static GReturn Lerp3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, float _ratio, MATH2D::GVECTOR3F& _outVector){ return GReturn::NO_IMPLEMENTATION; }
			static GReturn BerpF(float _value1, float _value2, float _value3, MATH2D::GBARYCENTRICF _barycentric, float& _outValue){ return GReturn::NO_IMPLEMENTATION; }
			static GReturn Berp2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F _vector3, MATH2D::GBARYCENTRICF _barycentric, MATH2D::GVECTOR2F& _outVector){ return GReturn::NO_IMPLEMENTATION; }
			static GReturn Berp3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F _vector3, MATH2D::GBARYCENTRICF _barycentric, MATH2D::GVECTOR3F& _outVector){ return GReturn::NO_IMPLEMENTATION; }
			static GReturn Spline2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F _vector3, MATH2D::GVECTOR2F _vector4, float _ratio, MATH2D::GVECTOR2F& _outVector){ return GReturn::NO_IMPLEMENTATION; }
			static GReturn GradientF(MATH2D::GLINE2F _line, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn AngleBetweenVectorsF(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outRadians) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn AngleBetweenLinesF(MATH2D::GLINE2F _vector1, MATH2D::GLINE2F _vector2, float& _outRadians){ return GReturn::NO_IMPLEMENTATION; }
			static GReturn Upgrade2(MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Upgrade3(MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			// doubles
			static GReturn Add2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Add3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Subtract2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Subtract3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Scale2D(MATH2D::GVECTOR2D _vector, double _scalar, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Scale3D(MATH2D::GVECTOR3D _vector, double _scalar, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Dot2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Dot3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Cross2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Cross3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn VectorXMatrix2D(MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn VectorXMatrix3D(MATH2D::GVECTOR3D _vector, MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Transform2D(MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Transform3D(MATH2D::GVECTOR3D _vector, MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Magnitude2D(MATH2D::GVECTOR2D _vector, double& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Magnitude3D(MATH2D::GVECTOR3D _vector, double& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Normalize2D(MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Normalize3D(MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn LerpD(double _value1, double _value2, double _ratio, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Lerp2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double _ratio, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Lerp3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, double _ratio, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn BerpD(double _value1, double _value2, double _value3, MATH2D::GBARYCENTRICD _barycentric, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Berp2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D _vector3, MATH2D::GBARYCENTRICD _barycentric, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Berp3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D _vector3, MATH2D::GBARYCENTRICD _barycentric, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Spline2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D _vector3, MATH2D::GVECTOR2D _vector4, double _ratio, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn GradientD(MATH2D::GLINE2D _line, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn AngleBetweenVectorsD(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outRadians) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn AngleBetweenLinesD(MATH2D::GLINE2D _vector1, MATH2D::GLINE2D _vector2, double& _outRadians) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Downgrade2(MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn Downgrade3(MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
		};
	}
}

// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Math2D/GVector2D/GVector2D.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware 2D math interfaces must belong.
	namespace MATH2D
	{
		//! A Gateware interface that handles all 2D vector functions.
		class GVector2D final : public I::GProxy<I::GVector2DInterface, I::GVector2DImplementation>
		{
		public:
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GVector2D)
			// Floats
			GATEWARE_STATIC_FUNCTION(Add2F)
			GATEWARE_STATIC_FUNCTION(Add3F)
			GATEWARE_STATIC_FUNCTION(Subtract2F)
			GATEWARE_STATIC_FUNCTION(Subtract3F)
			GATEWARE_STATIC_FUNCTION(Scale2F)
			GATEWARE_STATIC_FUNCTION(Scale3F)
			GATEWARE_STATIC_FUNCTION(Dot2F)
			GATEWARE_STATIC_FUNCTION(Dot3F)
			GATEWARE_STATIC_FUNCTION(Cross2F)
			GATEWARE_STATIC_FUNCTION(Cross3F)
			GATEWARE_STATIC_FUNCTION(VectorXMatrix2F)
			GATEWARE_STATIC_FUNCTION(VectorXMatrix3F)
			GATEWARE_STATIC_FUNCTION(Transform2F)
			GATEWARE_STATIC_FUNCTION(Transform3F)
			GATEWARE_STATIC_FUNCTION(Magnitude2F)
			GATEWARE_STATIC_FUNCTION(Magnitude3F)
			GATEWARE_STATIC_FUNCTION(Normalize2F)
			GATEWARE_STATIC_FUNCTION(Normalize3F)
			GATEWARE_STATIC_FUNCTION(LerpF)
			GATEWARE_STATIC_FUNCTION(Lerp2F)
			GATEWARE_STATIC_FUNCTION(Lerp3F)
			GATEWARE_STATIC_FUNCTION(BerpF)
			GATEWARE_STATIC_FUNCTION(Berp2F)
			GATEWARE_STATIC_FUNCTION(Berp3F)
			GATEWARE_STATIC_FUNCTION(Spline2F)
			GATEWARE_STATIC_FUNCTION(GradientF)
			GATEWARE_STATIC_FUNCTION(AngleBetweenVectorsF)
			GATEWARE_STATIC_FUNCTION(AngleBetweenLinesF)
			GATEWARE_STATIC_FUNCTION(Upgrade2)
			GATEWARE_STATIC_FUNCTION(Upgrade3)




			// Doubles
			GATEWARE_STATIC_FUNCTION(Add2D)
			GATEWARE_STATIC_FUNCTION(Add3D)
			GATEWARE_STATIC_FUNCTION(Subtract2D)
			GATEWARE_STATIC_FUNCTION(Subtract3D)
			GATEWARE_STATIC_FUNCTION(Scale2D)
			GATEWARE_STATIC_FUNCTION(Scale3D)
			GATEWARE_STATIC_FUNCTION(Dot2D)
			GATEWARE_STATIC_FUNCTION(Dot3D)
			GATEWARE_STATIC_FUNCTION(Cross2D)
			GATEWARE_STATIC_FUNCTION(Cross3D)
			GATEWARE_STATIC_FUNCTION(VectorXMatrix2D)
			GATEWARE_STATIC_FUNCTION(VectorXMatrix3D)
			GATEWARE_STATIC_FUNCTION(Transform2D)
			GATEWARE_STATIC_FUNCTION(Transform3D)
			GATEWARE_STATIC_FUNCTION(Magnitude2D)
			GATEWARE_STATIC_FUNCTION(Magnitude3D)
			GATEWARE_STATIC_FUNCTION(Normalize2D)
			GATEWARE_STATIC_FUNCTION(Normalize3D)
			GATEWARE_STATIC_FUNCTION(LerpD)
			GATEWARE_STATIC_FUNCTION(Lerp2D)
			GATEWARE_STATIC_FUNCTION(Lerp3D)
			GATEWARE_STATIC_FUNCTION(BerpD)
			GATEWARE_STATIC_FUNCTION(Berp2D)
			GATEWARE_STATIC_FUNCTION(Berp3D)
			GATEWARE_STATIC_FUNCTION(Spline2D)
			GATEWARE_STATIC_FUNCTION(GradientD)
			GATEWARE_STATIC_FUNCTION(AngleBetweenVectorsD)
			GATEWARE_STATIC_FUNCTION(AngleBetweenLinesD)
			GATEWARE_STATIC_FUNCTION(Downgrade2)
			GATEWARE_STATIC_FUNCTION(Downgrade3)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a valid GVector2D Object.
			/*!
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory)
			*/
			GReturn Create();

			// floats

			//! Add two float vector2s
			/*!
			*	Performs _vector1 + _vector2 and stores the result in _outVector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Add2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Add two float vector3s
			/*!
			*	Performs _vector1 + _vector2 and stores the result in _outVector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Add3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two float vector2s
			/*!
			*	Performs _vector1 - _vector2 and stores the result in _outVector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Subtract2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two float vector3s
			/*!
			*	Performs _vector1 - _vector2 and stores the result in _outVector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Subtract3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a float vector2
			/*!
			*	Performs _vector * _scalar and stores the result in _outVector.
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _scalar		The scalar
			*	\param [out] _outVector		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Scale2F(MATH2D::GVECTOR2F _vector, float _scalar, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a float vector3
			/*!
			*	Performs _vector * _scalar and stores the result in _outVector.
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _scalar		The scalar
			*	\param [out] _outVector		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Scale3F(MATH2D::GVECTOR3F _vector, float _scalar, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }


			//! Calculates the dot product of two float vector2s
			/*!
			*	Performs _vector1 (dot) _vector2 and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the dot product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Dot2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the dot product of two float vector3s
			/*!
			*	Performs _vector1 (dot) _vector2 and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the dot product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Dot3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the cross product of two float vector2s
			/*!
			*	Performs _vector1 X _vector2 and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the cross product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Cross2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the cross product of two float vector3s
			/*!
			*	Performs _vector1 X _vector2 and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the cross product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Cross3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a float vector2 by a float matrix2
			/*!
			*	Performs _vector * _matrix and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn VectorXMatrix2F(MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a float vector3 by a float matrix3
			/*!
			*	Performs _vector * _matrix and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn VectorXMatrix3F(MATH2D::GVECTOR3F _vector, MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Transform a float vector2 by a float matrix2
			/*!
			*	Performs _vector * _matrix and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the transformation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Transform2F(MATH2D::GVECTOR2F _vector, MATH2D::GMATRIX2F _matrix, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Transform a float vector3 by a float matrix3
			/*!
			*	Performs _vector * _matrix and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the transformation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Transform3F(MATH2D::GVECTOR3F _vector, MATH2D::GMATRIX3F _matrix, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the magnitude of a float vector2
			/*!
			*	Performs sqrt(x*x + y*y) and stores the result in _outValue
			*
			*	\param [in]  _vector			The vector
			*	\param [out] _outMagnitude		The result of the calculation
			*
			*	\retval GReturn::FAILURE				The magnitude of the vector was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Magnitude2F(MATH2D::GVECTOR2F _vector, float& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the magnitude of a float vector3
			/*!
			*	Performs sqrt(x*x + y*y + z*z) and stores the result in _outValue
			*
			*	\param [in]  _vector			The vector
			*	\param [out] _outMagnitude		The result of the calculation
			*
			*	\retval GReturn::FAILURE				The magnitude of the vector was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Magnitude3F(MATH2D::GVECTOR3F _vector, float& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }

			//! Normalize a float vector2
			/*!
			*	Divides each component of _vector by the magnitude of _vector and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [out] _outVector		The result of the normalization
			*
			*	\retval GReturn::FAILURE				The magnitude of the vector was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Normalize2F(MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Normalize a float vector3
			/*!
			*	Divides each component of _vector by the magnitude of _vector and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [out] _outVector		The result of the normalization
			*
			*	\retval GReturn::FAILURE				The magnitude of the vector was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Normalize3F(MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Linear interpolation between two floats
			/*!
			*	Performs (_value1 + _ratio * (_value2 - _value1)) and stores the result in _outValue
			*
			*	\param [in]  _value1		The first float
			*   \param [in]  _value2		The second float
			*   \param [in]  _ratio			The ratio
			*	\param [out] _outValue		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn LerpF(float _value1, float _value2, float _ratio, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Linear interpolation between two float vector2s
			/*!
			*	Performs LerpF() on each element in the vectors and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*   \param [in]  _vector2		The second vector
			*   \param [in]  _ratio			The ratio
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Lerp2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float _ratio, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Linear interpolation between two float vector3s
			/*!
			*	Performs LerpF() on each element in the vectors and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*   \param [in]  _vector2		The second vector
			*   \param [in]  _ratio			The ratio
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Lerp3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, float _ratio, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Barycentric interpolation between three floats
			/*!
			*	Performs ((_value1 * alpha) + (_value2 * beta) + (_value3 * gamma)) and stores the result in _outValue
			*
			*	\param [in]  _value1		The first value
			*   \param [in]  _value2		The second value
			*   \param [in]  _value3		The third value
			*   \param [in]  _barycentric	The 3 barycentric ratios: alpha, beta, and gamma
			*	\param [out] _outValue		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn BerpF(float _value1, float _value2, float _value3, MATH2D::GBARYCENTRICF _barycentric, float& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Barycentric interpolation between three float vector2s
			/*!
			*	Performs BerpF() on each element in the vectors and stores the result in _outVector
			*
			*	\param [in]  _vector1		The first vector
			*   \param [in]  _vector2		The second vector
			*   \param [in]  _vector3		The third vector
			*   \param [in]  _barycentric	The 3 barycentric ratios: alpha, beta, and gamma
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Berp2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F _vector3, MATH2D::GBARYCENTRICF _barycentric, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Barycentric interpolation between three float vector3s
			/*!
			*	Performs BerpF() on each element in the vectors and stores the result in _outVector
			*
			*	\param [in]  _vector1		The first vector
			*   \param [in]  _vector2		The second vector
			*   \param [in]  _vector3		The third vector
			*   \param [in]  _barycentric	The 3 barycentric ratios: alpha, beta, and gamma
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Berp3F(MATH2D::GVECTOR3F _vector1, MATH2D::GVECTOR3F _vector2, MATH2D::GVECTOR3F _vector3, MATH2D::GBARYCENTRICF _barycentric, MATH2D::GVECTOR3F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Spline interpolation between four float vector2s
			/*!
			*	Interpolates on a curve which connects the four vectors and stores the result in _outVector.
			*   Interpolation happens between the second and third points.
			*
			*	\param [in]  _vector1		The first point
			*   \param [in]  _vector2		The second point
			*   \param [in]  _vector3		The third point
			*   \param [in]  _vector4		The fourth point
			*   \param [in]  _ratio			The interpolation coefficient
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Spline2F(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, MATH2D::GVECTOR2F _vector3, MATH2D::GVECTOR2F _vector4, float _ratio, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the gradient vector of a line
			/*!
			*	Calculates the perpendicular vector relative to the vector parallel to _line.
			*   This is effectively the "up" direction relative to the direction of _line.
			*
			*	\param [in]  _line			The line
			*	\param [out] _outVector		The resulting gradient
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GradientF(MATH2D::GLINE2F _line, MATH2D::GVECTOR2F& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the angle between two float vectors
			/*!
			*	Calculates a radian angle between the two vectors stores the result in _outRadians
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outRadians	The resulting angle
			*
			*	\retval GReturn::FAILURE				The magnitude of one of the vectors was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn AngleBetweenVectorsF(MATH2D::GVECTOR2F _vector1, MATH2D::GVECTOR2F _vector2, float& _outRadians) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the angle between two float lines
			/*!
			*	Calculates a radian angle between the two vectors parallel to the two lines and stores the result in _outRadians
			*
			*	\param [in]  _line1			The first line
			*	\param [in]  _line2			The second line
			*	\param [out] _outRadians	The resulting angle
			*
			*	\retval GReturn::FAILURE				The magnitude of one of the vectors was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn AngleBetweenLinesF(MATH2D::GLINE2F _line1, MATH2D::GLINE2F _line2, float& _outRadians) { return GReturn::NO_IMPLEMENTATION; }

			//! Upgrade a float vector2 to a double vector2
			/*!
			*	Performs static_cast<double> on every element of _vector and stores the result in _outVector
			*
			*	\param [in]  _vector		The float vector
			*	\param [out] _outVector		The resulting double vector
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Upgrade2(MATH2D::GVECTOR2F _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Upgrade a float vector3 to a double vector3
			/*!
			*	Performs static_cast<double> on every element of _vector and stores the result in _outVector
			*
			*	\param [in]  _vector		The float vector
			*	\param [out] _outVector		The resulting double vector
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Upgrade3(MATH2D::GVECTOR3F _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }





			// doubles

			//! Add two double vector2s
			/*!
			*	Performs _vector1 + _vector2 and stores the result in _outVector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Add2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Add two double vector3s
			/*!
			*	Performs _vector1 + _vector2 and stores the result in _outVector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Add3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two double vector2s
			/*!
			*	Performs _vector1 - _vector2 and stores the result in _outVector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Subtract2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Subtract two double vector3s
			/*!
			*	Performs _vector1 - _vector2 and stores the result in _outVector.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the addition
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Subtract3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a double vector2
			/*!
			*	Performs _vector * _scalar and stores the result in _outVector.
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _scalar		The scalar
			*	\param [out] _outVector		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Scale2D(MATH2D::GVECTOR2D _vector, double _scalar, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Scale a double vector3
			/*!
			*	Performs _vector * _scalar and stores the result in _outVector.
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _scalar		The scalar
			*	\param [out] _outVector		The result of the scale
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Scale3D(MATH2D::GVECTOR3D _vector, double _scalar, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the dot product of two double vector2s
			/*!
			*	Performs _vector1 (dot) _vector2 and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the dot product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Dot2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the dot product of two double vector3s
			/*!
			*	Performs _vector1 (dot) _vector2 and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the dot product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Dot3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the cross product of two double vector2s
			/*!
			*	Performs _vector1 X _vector2 and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outValue		The result of the cross product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Cross2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculates the cross product of two double vector3s
			/*!
			*	Performs _vector1 X _vector2 as if they were vector2s and stores the result in _outValue. The z elements are ignored.
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outVector		The result of the cross product
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Cross3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a double vector2 by a double matrix2
			/*!
			*	Performs _vector * _matrix and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn VectorXMatrix2D(MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Multiply a double vector3 by a double matrix3
			/*!
			*	Performs _vector * _matrix and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the multiplication
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn VectorXMatrix3D(MATH2D::GVECTOR3D _vector, MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Transform a double vector2 by a double matrix2
			/*!
			*	Performs _vector * _matrix and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the transformation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Transform2D(MATH2D::GVECTOR2D _vector, MATH2D::GMATRIX2D _matrix, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Transform a double vector3 by a double matrix3
			/*!
			*	Performs _vector * _matrix and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [in]  _matrix		The matrix
			*	\param [out] _outVector		The result of the transformation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Transform3D(MATH2D::GVECTOR3D _vector, MATH2D::GMATRIX3D _matrix, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the magnitude of a double vector2
			/*!
			*	Performs sqrt(x*x + y*y) and stores the result in _outValue
			*
			*	\param [in]  _vector			The vector
			*	\param [out] _outMagnitude		The result of the calculation
			*
			*	\retval GReturn::FAILURE				The magnitude of the vector was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Magnitude2D(MATH2D::GVECTOR2D _vector, double& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the magnitude of a double vector3
			/*!
			*	Performs sqrt(x*x + y*y + z*z) and stores the result in _outValue
			*
			*	\param [in]  _vector		The vector
			*	\param [out] _outMagnitude		The result of the calculation
			*
			*	\retval GReturn::FAILURE				The magnitude of the vector was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Magnitude3D(MATH2D::GVECTOR3D _vector, double& _outMagnitude) { return GReturn::NO_IMPLEMENTATION; }

			//! Normalize a double vector2
			/*!
			*	Divides each component of _vector by the magnitude of _vector and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [out] _outVector		The result of the normalization
			*
			*	\retval GReturn::FAILURE				The magnitude of the vector was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Normalize2D(MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Normalize a double vector3
			/*!
			*	Divides each component of _vector by the magnitude of _vector and stores the result in _outVector
			*
			*	\param [in]  _vector		The vector
			*	\param [out] _outVector		The result of the normalization
			*
			*	\retval GReturn::FAILURE				The magnitude of the vector was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Normalize3D(MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Linear interpolation between two doubles
			/*!
			*	Performs (_value1 + _ratio * (_value2 - _value1)) and stores the result in _outValue
			*
			*	\param [in]  _value1		The first double
			*   \param [in]  _value2		The second double
			*   \param [in]  _ratio			The ratio
			*	\param [out] _outValue		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn LerpD(double _value1, double _value2, double _ratio, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Linear interpolation between two double vector2s
			/*!
			*	Performs LerpF() on each element in the vectors and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*   \param [in]  _vector2		The second vector
			*   \param [in]  _ratio			The ratio
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Lerp2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double _ratio, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Linear interpolation between two double vector3s
			/*!
			*	Performs LerpF() on each element in the vectors and stores the result in _outValue
			*
			*	\param [in]  _vector1		The first vector
			*   \param [in]  _vector2		The second vector
			*   \param [in]  _ratio			The ratio
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Lerp3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, double _ratio, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Barycentric interpolation between three doubles
			/*!
			*	Performs ((_value1 * alpha) + (_value2 * beta) + (_value3 * gamma)) and stores the result in _outValue
			*
			*	\param [in]  _value1		The first value
			*   \param [in]  _value2		The second value
			*   \param [in]  _value3		The third value
			*   \param [in]  _barycentric	The 3 barycentric ratios: alpha, beta, and gamma
			*	\param [out] _outValue		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn BerpD(double _value1, double _value2, double _value3, MATH2D::GBARYCENTRICD _barycentric, double& _outValue) { return GReturn::NO_IMPLEMENTATION; }

			//! Barycentric interpolation between three double vector2s
			/*!
			*	Performs BerpF() on each element in the vectors and stores the result in _outVector
			*
			*	\param [in]  _vector1		The first vector
			*   \param [in]  _vector2		The second vector
			*   \param [in]  _vector3		The third vector
			*   \param [in]  _barycentric	The 3 barycentric ratios: alpha, beta, and gamma
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Berp2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D _vector3, MATH2D::GBARYCENTRICD _barycentric, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Barycentric interpolation between three double vector3s
			/*!
			*	Performs BerpF() on each element in the vectors and stores the result in _outVector
			*
			*	\param [in]  _vector1		The first vector
			*   \param [in]  _vector2		The second vector
			*   \param [in]  _vector3		The third vector
			*   \param [in]  _barycentric	The 3 barycentric ratios: alpha, beta, and gamma
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Berp3D(MATH2D::GVECTOR3D _vector1, MATH2D::GVECTOR3D _vector2, MATH2D::GVECTOR3D _vector3, MATH2D::GBARYCENTRICD _barycentric, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Spline interpolation between four double vector2s
			/*!
			*	Interpolates on a curve which connects the four vectors and stores the result in _outVector.
			*   Interpolation happens between the second and third points.
			*
			*	\param [in]  _vector1		The first point
			*   \param [in]  _vector2		The second point
			*   \param [in]  _vector3		The third point
			*   \param [in]  _vector4		The fourth point
			*   \param [in]  _ratio			The interpolation coefficient
			*	\param [out] _outVector		The result of the interpolation
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn Spline2D(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, MATH2D::GVECTOR2D _vector3, MATH2D::GVECTOR2D _vector4, double _ratio, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the gradient vector of a line
			/*!
			*	Calculates the perpendicular vector relative to the vector parallel to _line.
			*   This is effectively the "up" direction relative to the direction of _line.
			*
			*	\param [in]  _line			The line
			*	\param [out] _outVector		The resulting gradient
			*
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn GradientD(MATH2D::GLINE2D _line, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the angle between two double vectors
			/*!
			*	Calculates a radian angle between the two vectors stores the result in _outRadians
			*
			*	\param [in]  _vector1		The first vector
			*	\param [in]  _vector2		The second vector
			*	\param [out] _outRadians	The resulting angle
			*
			*	\retval GReturn::FAILURE				The magnitude of one of the vectors was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn AngleBetweenVectorsD(MATH2D::GVECTOR2D _vector1, MATH2D::GVECTOR2D _vector2, double& _outRadians) { return GReturn::NO_IMPLEMENTATION; }

			//! Calculate the angle between two double lines
			/*!
			*	Calculates a radian angle between the two vectors parallel to the two lines and stores the result in _outRadians
			*
			*	\param [in]  _line1			The first line
			*	\param [in]  _line2			The second line
			*	\param [out] _outRadians	The resulting angle
			*
			*	\retval GReturn::FAILURE				The magnitude of one of the vectors was 0
			*	\retval GReturn::SUCCESS				The calculation succeeded
			*/
			static GReturn AngleBetweenLinesD(MATH2D::GLINE2D _line1, MATH2D::GLINE2D _line2, double& _outRadians) { return GReturn::NO_IMPLEMENTATION; }

			//! Downgrade a double vector2 to a float vector2
			/*!
			*	Performs static_cast<float> on every element of _vector and stores the result in _outVector
			*
			*	\param [in]  _vector		The double vector
			*	\param [out] _outVector		The resulting double vector
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Downgrade2(MATH2D::GVECTOR2D _vector, MATH2D::GVECTOR2D& _outVector) { return GReturn::NO_IMPLEMENTATION; }

			//! Downgrade a double vector3 to a float vector3
			/*!
			*	Performs static_cast<float> on every element of _vector and stores the result in _outVector
			*
			*	\param [in]  _vector		The double vector
			*	\param [out] _outVector		The resulting double vector
			*
			*	\retval GReturn::SUCCESS				The cast succeeded
			*/
			static GReturn Downgrade3(MATH2D::GVECTOR3D _vector, MATH2D::GVECTOR3D& _outVector) { return GReturn::NO_IMPLEMENTATION; }
#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GVECTOR2D_H