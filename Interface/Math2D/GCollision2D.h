#ifndef GCOLLISION2D_H
#define GCOLLISION2D_H

/*!
	File: GCollision2D.h
	Purpose: A Gateware interface that handles all 2D collision functions.
	Author: Ryan Powser
	Contributors:
	Interface Status: Beta
	Copyright: 7thGate Software LLC.
	License: MIT
*/

#include "../Core/GInterface.h"
#include "GMath2DDefines.h"

namespace GW
{
	namespace I
	{
		class GCollision2DInterface : public virtual GInterfaceInterface
		{
		public:
			enum class GCollisionCheck2D
			{
				ERROR_NO_RESULT = -3,
				ABOVE = -2,
				BELOW = -1,
				NO_COLLISION = 0,
				COLLISION = 1
			};

			// Floats
			static GReturn ImplicitLineEquationF(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, float& _outEquationResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToLine2F(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToRectangle2F(MATH2D::GVECTOR2F _point, MATH2D::GRECTANGLE2F _rectangle, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToPolygon2F(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToLine2F(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToCircle2F(MATH2D::GVECTOR2F _point, MATH2D::GCIRCLE2F _circle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToRectangle2F(MATH2D::GVECTOR2F _point, MATH2D::GRECTANGLE2F _rectangle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToPolygon2F(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToLine2F(MATH2D::GLINE2F _line1, MATH2D::GLINE2F _line2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToCircle2F(MATH2D::GLINE2F _line, MATH2D::GCIRCLE2F _circle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToRectangle2F(MATH2D::GLINE2F _line, MATH2D::GRECTANGLE2F _rectangle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToPolygon2F(MATH2D::GLINE2F _line, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestCircleToCircle2F(MATH2D::GCIRCLE2F _circle1, MATH2D::GCIRCLE2F _circle2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRectangleToRectangle2F(MATH2D::GRECTANGLE2F _rectangle1, MATH2D::GRECTANGLE2F _rectangle2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRectangleToPolygon2F(MATH2D::GRECTANGLE2F _rectangle, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPolygonToPolygon2F(MATH2D::GVECTOR2F* _polygon1, unsigned int _numVerts1, MATH2D::GVECTOR2F* _polygon2, unsigned int _numVerts2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn FindBarycentricF(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F _trianglePoint1, MATH2D::GVECTOR2F _trianglePoint2, MATH2D::GVECTOR2F _trianglePoint3, MATH2D::GBARYCENTRICF& _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
			// Doubles
			static GReturn ImplicitLineEquationD(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, double& _outEquationResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToLine2D(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToRectangle2D(MATH2D::GVECTOR2D _point, MATH2D::GRECTANGLE2D _rectangle, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn SqDistancePointToPolygon2D(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToLine2D(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToCircle2D(MATH2D::GVECTOR2D _point, MATH2D::GCIRCLE2D _circle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToRectangle2D(MATH2D::GVECTOR2D _point, MATH2D::GRECTANGLE2D _rectangle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPointToPolygon2D(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToLine2D(MATH2D::GLINE2D _line1, MATH2D::GLINE2D _line2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToCircle2D(MATH2D::GLINE2D _line, MATH2D::GCIRCLE2D _circle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToRectangle2D(MATH2D::GLINE2D _line, MATH2D::GRECTANGLE2D _rectangle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestLineToPolygon2D(MATH2D::GLINE2D _line, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestCircleToCircle2D(MATH2D::GCIRCLE2D _circle1, MATH2D::GCIRCLE2D _circle2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRectangleToRectangle2D(MATH2D::GRECTANGLE2D _rectangle1, MATH2D::GRECTANGLE2D _rectangle2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestRectangleToPolygon2D(MATH2D::GRECTANGLE2D _rectangle, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn TestPolygonToPolygon2D(MATH2D::GVECTOR2D* _polygon1, unsigned int _numVerts1, MATH2D::GVECTOR2D* _polygon2, unsigned int _numVerts2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }
			static GReturn FindBarycentricD(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D _trianglePoint1, MATH2D::GVECTOR2D _trianglePoint2, MATH2D::GVECTOR2D _trianglePoint3, MATH2D::GBARYCENTRICD& _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }
		};
	}
}


// Implementation Source link, will be search-replaced in single header version.
#include "../../Source/Math2D/GCollision2D/GCollision2D.hpp"

//! The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//! The core namespace to which all Gateware fundamental interfaces must belong.
	namespace MATH2D
	{
		//! A Gateware interface that handles all 2D collision functions.
		class GCollision2D final
			: public I::GProxy<I::GCollision2DInterface, I::GCollision2DImplementation>
		{
		public:
			// proxy functions should be ignored by doxygen
			//! \cond
			GATEWARE_PROXY_CLASS(GCollision2D)
			GATEWARE_TYPEDEF(GCollisionCheck2D)
			// floats
			GATEWARE_STATIC_FUNCTION(ImplicitLineEquationF)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToLine2F)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToRectangle2F)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToPolygon2F)
			GATEWARE_STATIC_FUNCTION(TestPointToLine2F)
			GATEWARE_STATIC_FUNCTION(TestPointToCircle2F)
			GATEWARE_STATIC_FUNCTION(TestPointToRectangle2F)
			GATEWARE_STATIC_FUNCTION(TestPointToPolygon2F)
			GATEWARE_STATIC_FUNCTION(TestLineToLine2F)
			GATEWARE_STATIC_FUNCTION(TestLineToCircle2F)
			GATEWARE_STATIC_FUNCTION(TestLineToRectangle2F)
			GATEWARE_STATIC_FUNCTION(TestLineToPolygon2F)
			GATEWARE_STATIC_FUNCTION(TestCircleToCircle2F)
			GATEWARE_STATIC_FUNCTION(TestRectangleToRectangle2F)
			GATEWARE_STATIC_FUNCTION(TestRectangleToPolygon2F)
			GATEWARE_STATIC_FUNCTION(TestPolygonToPolygon2F)
			GATEWARE_STATIC_FUNCTION(FindBarycentricF)
			// doubles
			GATEWARE_STATIC_FUNCTION(ImplicitLineEquationD)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToLine2D)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToRectangle2D)
			GATEWARE_STATIC_FUNCTION(SqDistancePointToPolygon2D)
			GATEWARE_STATIC_FUNCTION(TestPointToLine2D)
			GATEWARE_STATIC_FUNCTION(TestPointToCircle2D)
			GATEWARE_STATIC_FUNCTION(TestPointToRectangle2D)
			GATEWARE_STATIC_FUNCTION(TestPointToPolygon2D)
			GATEWARE_STATIC_FUNCTION(TestLineToLine2D)
			GATEWARE_STATIC_FUNCTION(TestLineToCircle2D)
			GATEWARE_STATIC_FUNCTION(TestLineToRectangle2D)
			GATEWARE_STATIC_FUNCTION(TestLineToPolygon2D)
			GATEWARE_STATIC_FUNCTION(TestCircleToCircle2D)
			GATEWARE_STATIC_FUNCTION(TestRectangleToRectangle2D)
			GATEWARE_STATIC_FUNCTION(TestRectangleToPolygon2D)
			GATEWARE_STATIC_FUNCTION(TestPolygonToPolygon2D)
			GATEWARE_STATIC_FUNCTION(FindBarycentricD)
			//! \endcond

			// This area does not contain actual code, it is only for the benefit of documentation generation.
#ifdef DOXYGEN_ONLY
			//! Creates a valid GCollision2D Object.
			/*!
			*	\retval GReturn::SUCCESS	This always succeeds. (unless you run out of memory)
			*/
			GReturn Create();

			//! Listing of common codes returned by select GCollision2D functions.
			/*! \brief	Values that represent collision checks */
			enum class GCollisionCheck2D {
				ERROR_NO_RESULT = -3,		/*!< Used internally by Gateware for testing purposes */
				ABOVE = -2,					/*!< An object is above another object */
				BELOW = -1,					/*!< An object is below another object */
				NO_COLLISION = 0,			/*!< There is no collision between two objects */
				COLLISION = 1				/*!< There is a collision between two objects */
			};



			// Floats

			//! Floating point version of the implicit line equation
			/*!
			*	Performs the implicit line equation on _point and _line and stores the result in _outEquationResult.
			*
			*	\param [in]		_point				The point
			*	\param [in]		_line				The line
			*	\param [out]	_outEquationResult	The result of the implicit line equation
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ImplicitLineEquationF(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, float& _outEquationResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point calculation that returns squared distance from point to line
			/*!
			*	Calculates the squared distance from _point to _line and stores the result in _outDistance.
			*
			*	\param [in]		_point			The point
			*	\param [in]		_line			The line
			*	\param [out]	_outDistance	The distance from _point to _line squared
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToLine2F(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point calculation that returns squared distance from point to rectangle
			/*!
			*	Calculates the squared distance from _point to _rectangle and stores the result in _outDistance. If _point is inside _rectangle, _outDistance will be 0.
			*
			*	\param [in]		_point			The point
			*	\param [in]		_rectangle		The rectangle
			*	\param [out]	_outDistance	The distance from _point to _rectangle squared
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToRectangle2F(MATH2D::GVECTOR2F _point, MATH2D::GRECTANGLE2F _rectangle, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point calculation that returns squared distance from point to polygon
			/*!
			*	Calculates the squared distance from _point to _polygon and stores the result in _outDistance. If _point is inside _polygon, _outDistance will be 0.
			*
			*	\param [in]		_point			The point
			*	\param [in]		_polygon		The polygon
			*	\param [in]		_numVerts		The number of vertices in _polygon
			*	\param [out]	_outDistance	The distance from _point to _polygon squared
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToPolygon2F(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, float& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a point and a line segment
			/*!
			*	Performs collision detection between _point and _line and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line degenerates to a point that is not _point
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_point is on _line or is equal to the start or end of _line
			*	<tr><td>GCollisionCheck2D::ABOVE			<td>_point is "above" _line relative to the normal of _line
			*	<tr><td>GCollisionCheck2D::BELOW			<td>_point is "below" _line relative to the normal of _line
			*	</table>
			*
			*	\param [in]		_point			The point
			*	\param [in]		_line			The line
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToLine2F(MATH2D::GVECTOR2F _point, MATH2D::GLINE2F _line, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a point and a circle
			/*!
			*	Performs collision detection between _point and _circle and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_point is not within or on the edge of _circle
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_point is within or on the edge of _circle
			*	</table>
			*
			*	\param [in]		_point			The point
			*	\param [in]		_circle			The circle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToCircle2F(MATH2D::GVECTOR2F _point, MATH2D::GCIRCLE2F _circle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a point and a rectangle
			/*!
			*	Performs collision detection between _point and _rectangle and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_point is not within, on the edge, or a corner of _rectangle
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_point is within, on the edge, or a corner of _rectangle
			*	</table>
			*
			*	\param [in]		_point			The point
			*	\param [in]		_rectangle		The rectangle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToRectangle2F(MATH2D::GVECTOR2F _point, MATH2D::GRECTANGLE2F _rectangle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a point and a polygon
			/*!
			*	Performs collision detection between _point and _polygon and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_point is not within, on the edge, or a vertex of _polygon
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_point is within, on the edge, or a vertex of _polygon
			*	</table>
			*
			*	\param [in]		_point			The point
			*	\param [in]		_polygon		A list of vertices describing the polygon
			*	\param [in]		_numVerts		The number of vertices in _polygon
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToPolygon2F(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a line and another line
			/*!
			*	Performs collision detection between _line1 and _line2 and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line1 does not collide with _line2
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_line1 intersects _line2, one or both of _line1's points are the same as one or both of _line2's points, one of or both of _line1's points are on _line2, or _line1 and _line2 are the same line
			*	</table>
			*
			*	\param [in]		_line1			The first line
			*	\param [in]		_line2			The second line
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToLine2F(MATH2D::GLINE2F _line1, MATH2D::GLINE2F _line2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a line and a circle
			/*!
			*	Performs collision detection between _line and _circle and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line does not collide with _circle
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_line intersects _circle at one or two points, _line is entirely within _circle, _line is tangent to circle, or one or both of _line's points are on the edge of _circle
			*	</table>
			*
			*	\param [in]		_line			The line
			*	\param [in]		_circle			The circle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToCircle2F(MATH2D::GLINE2F _line, MATH2D::GCIRCLE2F _circle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a line and a rectangle
			/*!
			*	Performs collision detection between _line and _rectangle and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line does not collide with _rectangle
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_line intersects _rectangle at one or two points, _line is entirely within _rectangle, _line overlays one of _rectangle's sides, or one or both of _line's points are on the edge of _rectangle
			*	</table>
			*
			*	\param [in]		_line			The line
			*	\param [in]		_rectangle		The rectangle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToRectangle2F(MATH2D::GLINE2F _line, MATH2D::GRECTANGLE2F _rectangle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a line and a polygon
			/*!
			*	Performs collision detection between _line and _polygon and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line does not collide with _polygon
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_line intersects _polygon at one or two points, _line is entirely within _polygon, _line overlays one of _polygon's sides, or one or both of _line's points are on the edge of _polygon
			*	</table>
			*
			*	\param [in]		_line			The line
			*	\param [in]		_polygon		A list of vertices describing the polygon
			*	\param [in]		_numVerts		The number of vertices in _polygon
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToPolygon2F(MATH2D::GLINE2F _line, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a circle and another circle
			/*!
			*	Performs collision detection between _circle1 and _circle2 and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_circle1 does not collide with _circle2
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_circle1 intersects _circle2 at one or two points, _circle1 is entirely within _circle2, _circle2 is entirely within _circle1, or _circle1 and _circle2 are the same circle
			*	</table>
			*
			*	\param [in]		_circle1		The first circle
			*	\param [in]		_circle2		The second circle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestCircleToCircle2F(MATH2D::GCIRCLE2F _circle1, MATH2D::GCIRCLE2F _circle2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a rectangle and another rectangle
			/*!
			*	Performs collision detection between _rectangle1 and _rectangle2 and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_rectangle1 does not collide with _rectangle2
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_rectangle1 intersects _rectangle2 at one, two, or four points, _rectangle1 is entirely within _rectangle2, _rectangle2 is entirely within _rectangle1, or _rectangle1 and _rectangle2 are the same rectangle
			*	</table>
			*
			*	\param [in]		_rectangle1		The first rectangle
			*	\param [in]		_rectangle2		The second rectangle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRectangleToRectangle2F(MATH2D::GRECTANGLE2F _rectangle1, MATH2D::GRECTANGLE2F _rectangle2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a rectangle and a polygon
			/*!
			*	Performs collision detection between _rectangle and _polygon and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_rectangle does not collide with _polygon
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_rectangle intersects _polygon at one, two, or four points, _rectangle is entirely within _polygon, or _polygon is entirely within _rectangle
			*	</table>
			*
			*	\param [in]		_rectangle		The first rectangle
			*	\param [in]		_polygon		A list of vertices describing the polygon
			*	\param [in]		_numVerts		The number of vertices in _polygon
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRectangleToPolygon2F(MATH2D::GRECTANGLE2F _rectangle, MATH2D::GVECTOR2F* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point collision test between a polygon and another polygon
			/*!
			*	Performs collision detection between _polygon1 and _polygon2 and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_polygon1 does not collide with _polygon2
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_polygon1 intersects _polygon2 at one or more points, _polygon1 is entirely within _polygon2, or _polygon2 is entirely within _polygon1
			*	</table>
			*
			*	\param [in]		_polygon1		A list of vertices describing the first polygon
			*	\param [in]		_numVerts1		The number of vertices in _polygon1
			*	\param [in]		_polygon2		A list of vertices describing the second polygon
			*	\param [in]		_numVerts2		The number of vertices in _polygon2
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPolygonToPolygon2F(MATH2D::GVECTOR2F* _polygon1, unsigned int _numVerts1, MATH2D::GVECTOR2F* _polygon2, unsigned int _numVerts2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Floating point calculation to convert the cartesian coordinates of a point to barycentric coordinates relative to a triangle
			/*!
			*	Calculates the barycentric coordinates of _point relative to the triangle formed by _trianglePoint1, _trianglePoint2, and _trianglePoint3, and stores the result in _outBarycentric.
			*
			*	\param [in]		_point				The point
			*	\param [in]		_trianglePoint1		The first point of the triangle
			*	\param [in]		_trianglePoint2		The second point of the triangle
			*	\param [in]		_trianglePoint3		The third point of the triangle
			*	\param [out]	_outBarycentric		The result of the calculation
			*
			*	\retval GReturn::FAILURE The three triangle points actually form a line which would cause a divide-by-zero error.
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn FindBarycentricF(MATH2D::GVECTOR2F _point, MATH2D::GVECTOR2F _trianglePoint1, MATH2D::GVECTOR2F _trianglePoint2, MATH2D::GVECTOR2F _trianglePoint3, MATH2D::GBARYCENTRICF& _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }










			// Doubles

			//! Double precision version of the implicit line equation
			/*!
			*	Performs the implicit line equation on _point and _line and stores the result in _outEquationResult.
			*
			*	\param [in]		_point				The point
			*	\param [in]		_line				The line
			*	\param [out]	_outEquationResult	The result of the implicit line equation
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn ImplicitLineEquationD(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, double& _outEquationResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision calculation that returns squared distance from point to line
			/*!
			*	Calculates the squared distance from _point to _line and stores the result in _outDistance.
			*
			*	\param [in]		_point			The point
			*	\param [in]		_line			The line
			*	\param [out]	_outDistance	The distance from _point to _line squared
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToLine2D(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision calculation that returns squared distance from point to rectangle
			/*!
			*	Calculates the squared distance from _point to _rectangle and stores the result in _outDistance. If _point is inside _rectangle, _outDistance will be 0.
			*
			*	\param [in]		_point			The point
			*	\param [in]		_rectangle		The rectangle
			*	\param [out]	_outDistance	The distance from _point to _rectangle squared
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToRectangle2D(MATH2D::GVECTOR2D _point, MATH2D::GRECTANGLE2D _rectangle, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision calculation that returns squared distance from point to polygon
			/*!
			*	Calculates the squared distance from _point to _polygon and stores the result in _outDistance. If _point is inside _polygon, _outDistance will be 0.
			*
			*	\param [in]		_point			The point
			*	\param [in]		_polygon		The polygon
			*	\param [in]		_numVerts		The number of vertices in _polygon
			*	\param [out]	_outDistance	The distance from _point to _polygon squared
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn SqDistancePointToPolygon2D(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, double& _outDistance) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a point and a line segment
			/*!
			*	Performs collision detection between _point and _line and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line degenerates to a point that is not _point
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_point is on _line or is equal to the start or end of _line
			*	<tr><td>GCollisionCheck2D::ABOVE			<td>_point is "above" _line relative to the normal of _line
			*	<tr><td>GCollisionCheck2D::BELOW			<td>_point is "below" _line relative to the normal of _line
			*	</table>
			*
			*	\param [in]		_point			The point
			*	\param [in]		_line			The line
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToLine2D(MATH2D::GVECTOR2D _point, MATH2D::GLINE2D _line, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a point and a circle
			/*!
			*	Performs collision detection between _point and _circle and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_point is not within or on the edge of _circle
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_point is within or on the edge of _circle
			*	</table>
			*
			*	\param [in]		_point			The point
			*	\param [in]		_circle			The circle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToCircle2D(MATH2D::GVECTOR2D _point, MATH2D::GCIRCLE2D _circle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a point and a rectangle
			/*!
			*	Performs collision detection between _point and _rectangle and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_point is not within, on the edge, or a corner of _rectangle
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_point is within, on the edge, or a corner of _rectangle
			*	</table>
			*
			*	\param [in]		_point			The point
			*	\param [in]		_rectangle		The rectangle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToRectangle2D(MATH2D::GVECTOR2D _point, MATH2D::GRECTANGLE2D _rectangle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a point and a polygon
			/*!
			*	Performs collision detection between _point and _polygon and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_point is not within, on the edge, or a vertex of _polygon
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_point is within, on the edge, or a vertex of _polygon
			*	</table>
			*
			*	\param [in]		_point			The point
			*	\param [in]		_polygon		A list of vertices describing the polygon
			*	\param [in]		_numVerts		The number of vertices in _polygon
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPointToPolygon2D(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a line and another line
			/*!
			*	Performs collision detection between _line1 and _line2 and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line1 does not collide with _line2
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_line1 intersects _line2, one or both of _line1's points are the same as one or both of _line2's points, one of or both of _line1's points are on _line2, or _line1 and _line2 are the same line
			*	</table>
			*
			*	\param [in]		_line1			The first line
			*	\param [in]		_line2			The second line
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToLine2D(MATH2D::GLINE2D _line1, MATH2D::GLINE2D _line2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a line and a circle
			/*!
			*	Performs collision detection between _line and _circle and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line does not collide with _circle
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_line intersects _circle at one or two points, _line is entirely within _circle, _line is tangent to circle, or one or both of _line's points are on the edge of _circle
			*	</table>
			*
			*	\param [in]		_line			The line
			*	\param [in]		_circle			The circle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToCircle2D(MATH2D::GLINE2D _line, MATH2D::GCIRCLE2D _circle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a line and a rectangle
			/*!
			*	Performs collision detection between _line and _rectangle and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line does not collide with _rectangle
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_line intersects _rectangle at one or two points, _line is entirely within _rectangle, _line overlays one of _rectangle's sides, or one or both of _line's points are on the edge of _rectangle
			*	</table>
			*
			*	\param [in]		_line			The line
			*	\param [in]		_rectangle		The rectangle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToRectangle2D(MATH2D::GLINE2D _line, MATH2D::GRECTANGLE2D _rectangle, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a line and a polygon
			/*!
			*	Performs collision detection between _line and _polygon and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_line does not collide with _polygon
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_line intersects _polygon at one or two points, _line is entirely within _polygon, _line overlays one of _polygon's sides, or one or both of _line's points are on the edge of _polygon
			*	</table>
			*
			*	\param [in]		_line			The line
			*	\param [in]		_polygon		A list of vertices describing the polygon
			*	\param [in]		_numVerts		The number of vertices in _polygon
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestLineToPolygon2D(MATH2D::GLINE2D _line, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a circle and another circle
			/*!
			*	Performs collision detection between _circle1 and _circle2 and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_circle1 does not collide with _circle2
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_circle1 intersects _circle2 at one or two points, _circle1 is entirely within _circle2, _circle2 is entirely within _circle1, or _circle1 and _circle2 are the same circle
			*	</table>
			*
			*	\param [in]		_circle1		The first circle
			*	\param [in]		_circle2		The second circle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestCircleToCircle2D(MATH2D::GCIRCLE2D _circle1, MATH2D::GCIRCLE2D _circle2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a rectangle and another rectangle
			/*!
			*	Performs collision detection between _rectangle1 and _rectangle2 and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_rectangle1 does not collide with _rectangle2
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_rectangle1 intersects _rectangle2 at one, two, or four points, _rectangle1 is entirely within _rectangle2, _rectangle2 is entirely within _rectangle1, or _rectangle1 and _rectangle2 are the same rectangle
			*	</table>
			*
			*	\param [in]		_rectangle1		The first rectangle
			*	\param [in]		_rectangle2		The second rectangle
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRectangleToRectangle2D(MATH2D::GRECTANGLE2D _rectangle1, MATH2D::GRECTANGLE2D _rectangle2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a rectangle and a polygon
			/*!
			*	Performs collision detection between _rectangle and _polygon and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_rectangle does not collide with _polygon
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_rectangle intersects _polygon at one, two, or four points, _rectangle is entirely within _polygon, or _polygon is entirely within _rectangle
			*	</table>
			*
			*	\param [in]		_rectangle		The first rectangle
			*	\param [in]		_polygon		A list of vertices describing the polygon
			*	\param [in]		_numVerts		The number of vertices in _polygon
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestRectangleToPolygon2D(MATH2D::GRECTANGLE2D _rectangle, MATH2D::GVECTOR2D* _polygon, unsigned int _numVerts, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision collision test between a polygon and another polygon
			/*!
			*	Performs collision detection between _polygon1 and _polygon2 and stores the result in _outResult.
			*
			*	<table>
			*	<tr><th colspan="2">Possible _outResult Values
			*	<tr><td>GCollisionCheck2D::NO_COLLISION		<td>_polygon1 does not collide with _polygon2
			*	<tr><td>GCollisionCheck2D::COLLISION		<td>_polygon1 intersects _polygon2 at one or more points, _polygon1 is entirely within _polygon2, or _polygon2 is entirely within _polygon1
			*	</table>
			*
			*	\param [in]		_polygon1		A list of vertices describing the first polygon
			*	\param [in]		_numVerts1		The number of vertices in _polygon1
			*	\param [in]		_polygon2		A list of vertices describing the second polygon
			*	\param [in]		_numVerts2		The number of vertices in _polygon2
			*	\param [out]	_outResult		The result of the collision test
			*
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn TestPolygonToPolygon2D(MATH2D::GVECTOR2D* _polygon1, unsigned int _numVerts1, MATH2D::GVECTOR2D* _polygon2, unsigned int _numVerts2, GCollisionCheck2D& _outResult) { return GReturn::NO_IMPLEMENTATION; }

			//! Double precision calculation to convert the cartesian coordinates of a point to barycentric coordinates relative to a triangle
			/*!
			*	Calculates the barycentric coordinates of _point relative to the triangle formed by _trianglePoint1, _trianglePoint2, and _trianglePoint3, and stores the result in _outBarycentric.
			*
			*	\param [in]		_point				The point
			*	\param [in]		_trianglePoint1		The first point of the triangle
			*	\param [in]		_trianglePoint2		The second point of the triangle
			*	\param [in]		_trianglePoint3		The third point of the triangle
			*	\param [out]	_outBarycentric		The result of the calculation
			*
			*	\retval GReturn::FAILURE The three triangle points actually form a line which would cause a divide-by-zero error.
			*	\retval GReturn::SUCCESS The calculation succeeded.
			*/
			static GReturn FindBarycentricD(MATH2D::GVECTOR2D _point, MATH2D::GVECTOR2D _trianglePoint1, MATH2D::GVECTOR2D _trianglePoint2, MATH2D::GVECTOR2D _trianglePoint3, MATH2D::GBARYCENTRICD& _outBarycentric) { return GReturn::NO_IMPLEMENTATION; }

#endif // DOXYGEN_ONLY
		};
	}
}
#endif // GCOLLISION2D_H