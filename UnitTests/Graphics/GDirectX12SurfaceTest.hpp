#if defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GDIRECTX12SURFACE)

#if defined(_WIN32)

bool EnableD3D12Tests = false;
TEST_CASE("Check for D3D12 hardware and Window support", "[Graphics]")
{
	GW::SYSTEM::GWindow window;
	GW::GRAPHICS::GDirectX12Surface d3dSurface;
	if (+window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
	{
		if (d3dSurface.Create(window, 0) != GW::GReturn::HARDWARE_UNAVAILABLE)
		{
			EnableD3D12Tests = true;
			std::cout << "D3D12 HARDWARE DETECTED: RUNNING ALL D3D12 UNIT TESTS" << std::endl;
		}
		else
			std::cout << "WARNING: D3D11 HARDWARE NOT DETECTED: SKIPPING ALL D3D12 UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING ALL D3D12 UNIT TESTS" << std::endl;
}

TEST_CASE("GDirectX12Surface core method test battery", "[Graphics]")
{
	if (EnableD3D12Tests == false)
	{
		std::cout << "Direct3D12/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	unsigned long long initMask = GW::GRAPHICS::COLOR_10_BIT | GW::GRAPHICS::DEPTH_BUFFER_SUPPORT | GW::GRAPHICS::DEPTH_STENCIL_SUPPORT;

	GW::GRAPHICS::GDirectX12Surface dx12Surface;

	GW::SYSTEM::GWindow window;
	GW::GEvent event;
	GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
	unsigned int missedEventCount = 0;
	unsigned int waitingEventCount = 0;

	float aspectRatio = 0.0f;
	IDXGISwapChain4* swapchain;
	unsigned int swapchainbufferindex;

	ID3D12Device* device;

	ID3D12GraphicsCommandList* cmdList;
	ID3D12CommandAllocator* cmdAloc;
	ID3D12CommandQueue* cmdQueue;
	ID3D12Fence* fence;

	unsigned int descriptorsSize[4] = {};

	ID3D12Resource* rt;
	ID3D12Resource* ds;

	D3D12_CPU_DESCRIPTOR_HANDLE rtv;
	D3D12_CPU_DESCRIPTOR_HANDLE dsv;

	SECTION("Testing empty proxy method calls")
	{
		REQUIRE(dx12Surface.Assign([]() {}) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.Invoke() == GW::GReturn::EMPTY_PROXY);
		
		REQUIRE(dx12Surface.GetAspectRatio(aspectRatio) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetSwapchain4((void**)&swapchain) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetSwapChainBufferIndex(swapchainbufferindex) == GW::GReturn::EMPTY_PROXY);

		REQUIRE(dx12Surface.GetDevice((void**)&device) == GW::GReturn::EMPTY_PROXY);

		REQUIRE(dx12Surface.GetCommandList((void**)&cmdList) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetCommandAllocator((void**)&cmdAloc) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetCommandQueue((void**)&cmdQueue) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetFence((void**)&fence) == GW::GReturn::EMPTY_PROXY);

		REQUIRE(dx12Surface.GetCBSRUADescriptorSize(descriptorsSize[0]) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetSamplerDescriptorSize(descriptorsSize[1]) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetRenderTargetDescriptorSize(descriptorsSize[2]) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetDepthStencilDescriptorSize(descriptorsSize[3]) == GW::GReturn::EMPTY_PROXY);

		REQUIRE(dx12Surface.GetCurrentRenderTarget((void**)&rt) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetDepthStencil((void**)&ds) == GW::GReturn::EMPTY_PROXY);

		REQUIRE(dx12Surface.GetCurrentRenderTargetView(&rtv) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.GetDepthStencilView(&dsv) == GW::GReturn::EMPTY_PROXY);

		REQUIRE(dx12Surface.StartFrame() == GW::GReturn::EMPTY_PROXY);
		REQUIRE(dx12Surface.EndFrame(true) == GW::GReturn::EMPTY_PROXY);
	}

	SECTION("Testing creation/destruction method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.Create(window, initMask) == GW::GReturn::SUCCESS);
		dx12Surface = nullptr;
		window = nullptr;
		REQUIRE(!dx12Surface);
		REQUIRE(!window);
	}

	SECTION("Testing valid proxy method calls")
	{
		REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.Create(window, initMask) == GW::GReturn::SUCCESS);

		REQUIRE(event.Write(GW::SYSTEM::GWindow::Events::RESIZE, windowEventData) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.Assign([]() {}) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.Invoke() == GW::GReturn::SUCCESS);
		
		REQUIRE(dx12Surface.GetAspectRatio(aspectRatio) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetSwapchain4((void**)&swapchain) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetSwapChainBufferIndex(swapchainbufferindex) == GW::GReturn::SUCCESS);

		REQUIRE(dx12Surface.GetDevice((void**)&device) == GW::GReturn::SUCCESS);

		REQUIRE(dx12Surface.GetCommandList((void**)&cmdList) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetCommandAllocator((void**)&cmdAloc) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetCommandQueue((void**)&cmdQueue) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetFence((void**)&fence) == GW::GReturn::SUCCESS);

		REQUIRE(dx12Surface.GetCBSRUADescriptorSize(descriptorsSize[0]) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetSamplerDescriptorSize(descriptorsSize[1]) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetRenderTargetDescriptorSize(descriptorsSize[2]) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetDepthStencilDescriptorSize(descriptorsSize[3]) == GW::GReturn::SUCCESS);

		REQUIRE(dx12Surface.GetCurrentRenderTarget((void**)&rt) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetDepthStencil((void**)&ds) == GW::GReturn::SUCCESS);

		REQUIRE(dx12Surface.GetCurrentRenderTargetView(&rtv) == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.GetDepthStencilView(&dsv) == GW::GReturn::SUCCESS);

		REQUIRE(dx12Surface.StartFrame() == GW::GReturn::SUCCESS);
		REQUIRE(dx12Surface.EndFrame(true) == GW::GReturn::SUCCESS);

		swapchain->Release();
		device->Release();
		cmdList->Release();
		cmdAloc->Release();
		cmdQueue->Release();
		fence->Release();
		rt->Release();
		ds->Release();
	}
}

#if !defined(DISABLE_USER_INPUT_TESTS)
TEST_CASE("GDirectX12Surface DEMO Unit Test (Using StartFrame, EndFrame)", "[Graphics]")
{
	if (EnableD3D12Tests == false)
	{
		std::cout << "Direct3D12/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::GRAPHICS::GDirectX12Surface dx12Surface;
	GW::SYSTEM::GWindow window;

	IDXGISwapChain4* swapchain;
	ID3D12GraphicsCommandList* cmdList;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx12Surface.Create(window, 0) == GW::GReturn::SUCCESS);

	REQUIRE(dx12Surface.GetSwapchain4((void**)&swapchain) == GW::GReturn::SUCCESS);
	REQUIRE(dx12Surface.GetCommandList((void**)&cmdList) == GW::GReturn::SUCCESS);

	static float t = 0;
	std::cout << "CLOSE THE WINDOW TO STOP THE EPILEPSY AND CONTINUE THE TEST\n";
	while (+window.ProcessWindowEvents())
	{
		if (+dx12Surface.StartFrame())
		{
			t += 0.01f;
			FLOAT color[4] = { sinf(t), cosf(t), tanf(t), 1.0f };

			D3D12_CPU_DESCRIPTOR_HANDLE rtv;

			if (+dx12Surface.GetCurrentRenderTargetView(&rtv))
			{
				cmdList->ClearRenderTargetView(rtv, color, 0, nullptr);
				dx12Surface.EndFrame(true);
			}
		}
	}

	swapchain->Release();
	cmdList->Release();
}

TEST_CASE("GDirectX12Surface DEMO Unit Test (Without Using StartFrame, EndFrame)", "[Graphics]")
{
	if (EnableD3D12Tests == false)
	{
		std::cout << "Direct3D12/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::GRAPHICS::GDirectX12Surface dx12Surface;
	GW::SYSTEM::GWindow window;

	IDXGISwapChain4* swapchain;
	ID3D12GraphicsCommandList* cmdList;
	ID3D12CommandAllocator* cmdaloc;
	ID3D12CommandQueue* cmdq;

	UINT64 fenceValue = 0;
	ID3D12Fence* fence;

	REQUIRE(window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED) == GW::GReturn::SUCCESS);
	REQUIRE(dx12Surface.Create(window, 0) == GW::GReturn::SUCCESS);

	REQUIRE(dx12Surface.GetSwapchain4((void**)&swapchain) == GW::GReturn::SUCCESS);
	REQUIRE(dx12Surface.GetCommandList((void**)&cmdList) == GW::GReturn::SUCCESS);
	REQUIRE(dx12Surface.GetCommandAllocator((void**)&cmdaloc) == GW::GReturn::SUCCESS);
	REQUIRE(dx12Surface.GetCommandQueue((void**)&cmdq) == GW::GReturn::SUCCESS);

	REQUIRE(dx12Surface.GetFence((void**)&fence) == GW::GReturn::SUCCESS);

	static float t = 0;
	std::cout << "CLOSE THE WINDOW TO STOP THE EPILEPSY AND CONTINUE THE TEST\n";
	while (+window.ProcessWindowEvents())
	{
		t += 0.01f;
		FLOAT color[4] = { sinf(t), cosf(t), tanf(t), 1.0f };

		D3D12_CPU_DESCRIPTOR_HANDLE rtv = {};
		ID3D12Resource* rt = nullptr;
		if (+dx12Surface.GetCurrentRenderTarget((void**)&rt) && +dx12Surface.GetCurrentRenderTargetView(&rtv))
		{
			cmdaloc->Reset();
			cmdList->Reset(cmdaloc, nullptr);

			D3D12_RESOURCE_BARRIER rtTransitionBarrier = {};
			rtTransitionBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			rtTransitionBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			rtTransitionBarrier.Transition.pResource = rt;
			rtTransitionBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			rtTransitionBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
			rtTransitionBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
			cmdList->ResourceBarrier(1, &rtTransitionBarrier);

			D3D12_VIEWPORT vp;
			vp.MinDepth = 0.0f;
			vp.MaxDepth = 1.0f;
			vp.TopLeftX = vp.TopLeftY = 0;
			vp.Width = (FLOAT)rt->GetDesc().Width;
			vp.Height = (FLOAT)rt->GetDesc().Height;
			D3D12_RECT rect;
			rect.left = 0;
			rect.right = (LONG)vp.Width;
			rect.top = 0;
			rect.bottom = (LONG)vp.Height;

			cmdList->RSSetViewports(1, &vp);
			cmdList->RSSetScissorRects(1, &rect);
			cmdList->ClearRenderTargetView(rtv, color, 0, nullptr);

			rtTransitionBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			rtTransitionBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			rtTransitionBarrier.Transition.pResource = rt;
			rtTransitionBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			rtTransitionBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
			rtTransitionBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
			cmdList->ResourceBarrier(1, &rtTransitionBarrier);

			cmdList->Close();
			ID3D12CommandList* cmdlists[] = { cmdList };
			cmdq->ExecuteCommandLists(1, cmdlists);

			UINT SyncInterval = 1u;
			UINT PresentFlags = 0u;
			DXGI_PRESENT_PARAMETERS PresentParameters = { 0u, NULL, NULL, NULL };
			

			cmdq->Signal(fence, ++fenceValue);
			if (fence->GetCompletedValue() < fenceValue)
			{
				HANDLE hnd = CreateEventEx(nullptr, nullptr, false, EVENT_ALL_ACCESS);
				fence->SetEventOnCompletion(fenceValue, hnd);
				WaitForSingleObject(hnd, INFINITE);
				CloseHandle(hnd);
			}
			CHECK(swapchain->Present1(SyncInterval, PresentFlags, &PresentParameters) == S_OK);
			rt->Release();
		}
	}

	swapchain->Release();
	cmdList->Release();
	cmdaloc->Release();
	cmdq->Release();
	fence->Release();
}
#endif _DISABLE_USER_INPUT_TESTS
#endif /* _WIN32 */
#endif /* defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GDIRECTX11SURFACE) */