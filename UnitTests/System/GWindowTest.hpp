#if defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GWINDOW)
#include <iostream>
#include "../../Source/Shared/GEnvironment.hpp"
#include <thread>

#define WindowSleepTime 100
bool EnableWindowTests = false;

#pragma region SHARED_SECTION

#define GiveWindowTimeBeforeRequiringPass(gwindow, max_wait_in_milliseconds, ... )\
	if (true)\
	{\
		auto startTime = std::chrono::steady_clock::now();\
		auto currentTime = std::chrono::steady_clock::now();\
		auto lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();\
		bool testPasses = false;\
		while (lapsedTime < max_wait_in_milliseconds && gwindow != nullptr)\
		{\
			gwindow.ProcessWindowEvents();\
			testPasses = __VA_ARGS__;\
			if (!testPasses)\
				std::this_thread::sleep_for(std::chrono::milliseconds(WindowSleepTime));\
			else\
				break;\
			currentTime = std::chrono::steady_clock::now();\
			lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();\
		}\
		REQUIRE(__VA_ARGS__);\
	}

TEST_CASE("Check for OS GUI/Window support", "[System]")
{
	GW::SYSTEM::GWindow window;
	if (+window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
	{
		EnableWindowTests = true;
		std::cout << "OS GUI DETECTED: RUNNING ALL GWINDOW UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING ALL GWINDOW UNIT TESTS" << std::endl;
}

TEST_CASE("Create GWindow object.", "[CreateGWindow], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;
	// Pass cases
	REQUIRE(G_PASS(window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(window);
}

TEST_CASE("GWindow Register Event Receivers.", "[Register], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;
	GW::CORE::GEventReceiver receiver;

	window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
	receiver.Create(window, []() {return; });

	// Fail case
	CHECK(window.Register(receiver, nullptr) == GW::GReturn::INVALID_ARGUMENT);

	auto callback = [](const GW::GEvent& _event, GW::CORE::GInterface& _interface) -> void { return; };

	// Pass case
	REQUIRE(G_PASS(window.Register(receiver, callback)));
}

TEST_CASE("GWindow Register Event Responders.", "[Register], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;
	GW::CORE::GEventResponder responder;

	window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
	responder.Create([](const GW::GEvent& _event) {return; });

	// Fail case
	CHECK(window.Register(responder, nullptr) == GW::GReturn::INVALID_ARGUMENT);

	auto callback = [](const GW::GEvent& _event, GW::CORE::GInterface& _interface) -> void { return; };

	// Pass case
	REQUIRE(G_PASS(window.Register(responder, callback)));
}

/*
	Tests if the window frame size and client size are reported as the same when fullscreen borderless.
*/
TEST_CASE("GWINDOW SIZE VS CLIENT SIZE WHEN MAXIMIZED TEST CASE", "[System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}

	// CONSTANTS

	// Windows cannot be expected to instantly move, resize, etc when told to. Each operating system uses
	// different window managers that have the final say of when an action on a window is carried out. We
	// need to give the manager time to carry out our requests for the window. Also, some windows may 
	// animate (like on macOS) and therefore need time to do so.
	constexpr int MAX_WINDOW_WAIT_TIME = 5000; // The maximum time we'll give the window to pass its test before we move on.

	// The minimum time we test for.
#if defined(__APPLE__)
	constexpr int MIN_WINDOW_WAIT_TIME = 2500; // Value chosen due to animations when minimizing and maximizing.
#elif defined(__linux__)
	constexpr int MIN_WINDOW_WAIT_TIME = 250; // Value chosen due to the window manager not being ready right after a window is created.
#else
	constexpr int MIN_WINDOW_WAIT_TIME = 0; // No problems with Win32.
#endif

	// VARIABLES

	unsigned int eventDataX = 0;
	unsigned int eventDataY = 0;
	unsigned int eventDataWidth = 0;
	unsigned int eventDataHeight = 0;
	unsigned int eventDataClientWidth = 0;
	unsigned int eventDataClientHeight = 0;

	unsigned int getXFunction = 0;
	unsigned int getYFunction = 0;
	unsigned int getWidthFunction = 0;
	unsigned int getHeightFunction = 0;
	unsigned int getterFunctionClientX = 0;
	unsigned int getterFunctionClientY = 0;
	unsigned int getClientWidthFunction = 0;
	unsigned int getClientHeightFunction = 0;

	GW::SYSTEM::GWindow::Events targetEvent = GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED; // The target event we're testing.
	GW::SYSTEM::GWindow::Events receivedEvent = GW::SYSTEM::GWindow::Events::EVENTS_PROCESSED; // The event received from GWindow.

	GW::CORE::GThreadShared threadLock;
	GW::SYSTEM::GWindow window;
	GW::CORE::GEventResponder responder;

	// SETUP

	threadLock.Create();
	window.Create(300, 300, 300, 300, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERLESS);
	responder.Create([&](const GW::GEvent& _event)
		{
			GW::SYSTEM::GWindow::Events windowEvent;
			GW::SYSTEM::GWindow::EVENT_DATA windowEventData;

			if (+_event.Read(windowEvent, windowEventData))
			{
				threadLock.LockAsyncRead();

				// When targetEvent is set to MAXIMIZE, it will start being compared to receivedEvent.
				// Once a MAXIMIZE event is received, the eventData variables will be set and this
				// if-statement will never be entered again for this test.
				if (targetEvent == GW::SYSTEM::GWindow::Events::MAXIMIZE &&
					receivedEvent != targetEvent &&
					windowEvent == targetEvent)
				{
					threadLock.UnlockAsyncRead();

					threadLock.LockSyncWrite();
					receivedEvent = windowEvent;

					switch (windowEvent)
					{
					case GW::SYSTEM::GWindow::Events::MAXIMIZE:
					{
						eventDataX = windowEventData.windowX;
						eventDataY = windowEventData.windowY;
						eventDataWidth = windowEventData.width;
						eventDataHeight = windowEventData.height;
						eventDataClientWidth = windowEventData.clientWidth;
						eventDataClientHeight = windowEventData.clientHeight;

						window.GetX(getXFunction);
						window.GetY(getYFunction);
						window.GetWidth(getWidthFunction);
						window.GetHeight(getHeightFunction);
						window.GetClientHeight(getClientHeightFunction);
						window.GetClientWidth(getClientWidthFunction);
						window.GetClientTopLeft(getterFunctionClientX, getterFunctionClientY);
						break;
					}
					}
					threadLock.UnlockSyncWrite();
				}
				else
					threadLock.UnlockAsyncRead();
			}
		});

	window.Register(responder);

	// HELPER FUNCTIONS

	// Sets targetEvent in a thread safe manor.
	auto SetTargetEvent = [&](GW::SYSTEM::GWindow::Events _event)
	{
		threadLock.LockSyncWrite();
		targetEvent = _event;
		threadLock.UnlockSyncWrite();
	};

	// Waits for the window is finished being created and setup. The test is conducted for no longer
	// than MAX_WINDOW_WAIT_TIME or until they pass.
	auto GiveWindowTimeToSetup = [&]()
	{
		auto startTime = std::chrono::steady_clock::now();
		auto currentTime = std::chrono::steady_clock::now();
		auto lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		bool testPasses = false;

		while ((lapsedTime < MAX_WINDOW_WAIT_TIME && window != nullptr) || lapsedTime < MIN_WINDOW_WAIT_TIME)
		{
			testPasses = +window.ProcessWindowEvents();

			if (!testPasses)
				std::this_thread::sleep_for(std::chrono::milliseconds(WindowSleepTime));
			else if (lapsedTime >= MIN_WINDOW_WAIT_TIME)
				break;

			currentTime = std::chrono::steady_clock::now();
			lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		}
	};

	// Waits for the target event to be received. The test is conducted for no longer than MAX_WINDOW_WAIT_TIME.
	auto WaitToReceiveTargetEvent = [&](GW::SYSTEM::GWindow::Events _event)
	{
		auto startTime = std::chrono::steady_clock::now();
		auto currentTime = std::chrono::steady_clock::now();
		auto lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		bool testPasses = false;

		while ((lapsedTime < MAX_WINDOW_WAIT_TIME && window != nullptr) || lapsedTime < MIN_WINDOW_WAIT_TIME)
		{
			window.ProcessWindowEvents();

			threadLock.LockAsyncRead();
			testPasses = (receivedEvent == _event);
			threadLock.UnlockAsyncRead();

			if (!testPasses)
				std::this_thread::sleep_for(std::chrono::milliseconds(WindowSleepTime));
			else if (lapsedTime >= MIN_WINDOW_WAIT_TIME)
				break;

			currentTime = std::chrono::steady_clock::now();
			lapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count();
		}
	};

	// FULLSCREEN TEST

	GiveWindowTimeToSetup();
	REQUIRE(G_PASS(window.ProcessWindowEvents())); // Window should be setup if events are processing.

	GW::SYSTEM::GWindow::Events event = GW::SYSTEM::GWindow::Events::MAXIMIZE;
	SetTargetEvent(event);
	REQUIRE(G_PASS(window.Maximize()));
	WaitToReceiveTargetEvent(event);

	// Client postion should be zero when style is borderless.
	threadLock.LockAsyncRead();
	REQUIRE(getterFunctionClientX == 0);
	REQUIRE(getterFunctionClientY == 0);

	// Position from event data should match getter functions.
	REQUIRE(eventDataX == getXFunction);
	REQUIRE(eventDataY == getYFunction);

	// Size of window frame and client should be equal.
	REQUIRE(getWidthFunction == getClientWidthFunction);
	REQUIRE(getHeightFunction == getClientHeightFunction);

	// Window frame and client size from event data should match client.
	REQUIRE(eventDataWidth == getClientWidthFunction);
	REQUIRE(eventDataHeight == getClientHeightFunction);
	REQUIRE(eventDataClientWidth == getClientWidthFunction);
	REQUIRE(eventDataClientHeight == getClientHeightFunction);
	threadLock.UnlockAsyncRead();

	window = nullptr;
}

#endif

#pragma endregion

#pragma region APPLE_SECTION

#if !TARGET_OS_IOS
TEST_CASE("Change Window Name.", "[SetWindowName], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	//setup
	REQUIRE(G_PASS(window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));

	//Pass Cases
	REQUIRE(G_PASS(window.SetWindowName((char*)"New Window Name")));

	//Fail Case
	REQUIRE(G_FAIL(window.SetWindowName(nullptr)));
}

TEST_CASE("Reconfigure the open Window.", "[ReconfigureWindow], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	//setup
	window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);

	// Pass cases
	REQUIRE(G_PASS(window.ReconfigureWindow(250, 500, 1000, 1000, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
}

TEST_CASE("Changing Window style.", "[ChangeWindowStyle], [System]")
{
	if (EnableWindowTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;

	//setup
	window.Create(0, 0, 800, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);

	// Pass case
	REQUIRE(G_PASS(window.ChangeWindowStyle(GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
}
#endif

#pragma endregion

#if defined(GATEWARE_ENV_APP)
#include "GWindowTest_app.hpp"
#elif defined(GATEWARE_ENV_DESKTOP)
#include "GWindowTest_desktop.hpp"
#endif // defined(GATEWARE_ENV_APP)
