#if defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GDAEMON)

bool EnableDaemonTests = false;
TEST_CASE("Check for Multi-Threading support", "[System]")
{
	if (std::thread::hardware_concurrency() > 1)
	{
		EnableDaemonTests = true;
		std::cout << "MULTIPLE HARDWARE THREADS DETECTED: RUNNING ALL GDAEMON UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: SINGLE HW THREAD DETECTED; SKIPPING SOME GDAEMON UNIT TESTS" << std::endl;
}

TEST_CASE("GDaemon creation", "[Create], [System]")
{
	printf("Logical cores count: %u\n", (unsigned int)std::thread::hardware_concurrency());
	GW::SYSTEM::GDaemon testGDaemon;
	REQUIRE(+testGDaemon.Create(50, [&]() {}));
	
	testGDaemon = nullptr;
	REQUIRE(testGDaemon.Resume() == GW::GReturn::EMPTY_PROXY);
}

TEST_CASE("GDaemon running test", "[Counter], [System]")
{
	std::atomic_int counter = { 0 };
	unsigned long long opCount = 0;
	GW::SYSTEM::GDaemon testGDaemon;

	testGDaemon.Create(100, [&counter]() { ++counter; });

	UTILS::Sleep_Exact(1050);
	testGDaemon.Counter(opCount);
	testGDaemon = nullptr;
	REQUIRE(opCount == counter); // the number of GDaemon "loops" should be exactly the same as the counter
	REQUIRE(counter >= 10);
}

TEST_CASE("GDaemon delayed start", "[System]")
{
	std::atomic_int counter = { 0 };
	GW::SYSTEM::GDaemon testGDaemon;
	// Should wait for 900 milliseconds before starting the routine
	testGDaemon.Create(25, [&]() { ++counter;  }, 900);

	UTILS::Sleep_Exact(1000);
	testGDaemon = nullptr;
	REQUIRE(counter >= 3);
	REQUIRE(counter <= 10);
}

TEST_CASE("GDaemon start paused", "[Pause], [System]")
{
	unsigned char counter = 0;
	GW::SYSTEM::GDaemon testGDaemon;
	testGDaemon.Create(25, [&counter]() { counter++; }, 0); // 0 paused to create - start paused

	UTILS::Sleep_Exact(105);
	REQUIRE(counter == 0);  // GDaemon started in paused state
}

TEST_CASE("GDaemon pausing", "[Pause], [System]")
{
	unsigned char counter = 0;
	GW::SYSTEM::GDaemon testGDaemon;
	testGDaemon.Create(25, [&counter]() { counter++; });
	REQUIRE(+testGDaemon.Pause(false, 0));

	UTILS::Sleep_Exact(105);
	REQUIRE(counter <= 1); // GDaemon was instantly paused
}

TEST_CASE("GDaemon deadlock pause avoidance", "[Pause], [System]")
{
	GW::GReturn result;
	GW::SYSTEM::GDaemon testGDaemon;
	testGDaemon.Create(25, [&testGDaemon, &result]() { result = testGDaemon.Pause(true, 0); });

	UTILS::Sleep_Exact(55);
	// You can't have these macros on separate threads
	REQUIRE(result == GW::GReturn::DEADLOCK);
}

TEST_CASE("GDaemon resuming", "[Resume], [System]")
{
	std::atomic_int counter = { 0 };
	GW::SYSTEM::GDaemon testGDaemon;

	testGDaemon.Create(100, [&counter]() { ++counter; });

	REQUIRE(testGDaemon.Resume() == GW::GReturn::REDUNDANT);
	testGDaemon.Pause(false, 0);

	REQUIRE(+testGDaemon.Resume()); // Instantly resumed right after pause

	UTILS::Sleep_Exact(275);
	testGDaemon = nullptr;
	REQUIRE(counter > 1);
}

TEST_CASE("GDaemon concurrent performance", "[System]")
{
	std::atomic_int counter = { 0 };
	GW::SYSTEM::GDaemon testGDaemon1, testGDaemon2;

	testGDaemon1.Create(50, [&counter](){ ++counter; });
	testGDaemon2.Create(100, [&counter](){ counter -= 2; });

	UTILS::Sleep_Exact(1005);
	testGDaemon2 = nullptr;
	testGDaemon1 = nullptr;
	REQUIRE(counter * counter <= 1); // both GDaemons should balance out and result in counter == 0
}

TEST_CASE("GDaemon timing offset", "[System]")
{
	if (EnableDaemonTests == false)
	{
		std::cout << "Multiple HW threads not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	std::atomic_int counter = { 0 };
	GW::SYSTEM::GDaemon testGDaemon;

	testGDaemon.Create(50, [&counter]()
	{
		UTILS::Sleep_Exact(100); // Each loop takes longer than the delay of GDaemon
		++counter;
	});

	UTILS::Sleep_Exact(1000); // was 995
	testGDaemon = nullptr;
	REQUIRE(counter >= 5);
	REQUIRE(counter <= 8);
}

TEST_CASE("GDaemon delayed chain creation", "[System]")
{
	std::atomic_int counter = { 0 };
	GW::SYSTEM::GDaemon testGDaemon1, testGDaemon2;

	// Very long delay time to attempt slowing down the creation process/performance of other GDaemons
	testGDaemon1.Create(99999, [&]() { UTILS::Sleep_Exact(2500); });

	UTILS::Sleep_Exact(125);
	testGDaemon1 = nullptr; // Destruction of sleeping GDaemon should not lock out other GDaemons

	testGDaemon2.Create(50, [&counter]() { ++counter; });

	UTILS::Sleep_Exact(525);
	testGDaemon2 = nullptr;
	REQUIRE(counter >= 10);
}

TEST_CASE("GDaemon recursive creation", "[System]")
{
	std::atomic_int counter = { 0 };
	GW::SYSTEM::GDaemon testGDaemon;
	std::function<void()> go_on;

	go_on = [&]() 
	{
		++counter;
		testGDaemon.Create(100, go_on, 100);
	};
	testGDaemon.Create(100, go_on); // Recursively recreates itself within itself (5 times)

	UTILS::Sleep_Exact(525);
	testGDaemon = nullptr;
	REQUIRE(counter >= 5);
	REQUIRE(counter <= 7);
}

TEST_CASE("GDaemon Event Testing", "[System]")
{
	// generate events
	GW::SYSTEM::GDaemon testGDaemon;
	testGDaemon.Create(1, [&]() {
		// doesn't do anything, but will still generate events
	});
	// listen for events
	bool complete = false, pause = false, resume = false;
	GW::CORE::GEventResponder listen;
	listen.Create([&](const GW::GEvent& e) {
		GW::SYSTEM::GDaemon::Events type;
		GW::SYSTEM::GDaemon::EVENT_DATA data;
		if (+e.Read(type, data)) {
			switch (type) {
			case GW::SYSTEM::GDaemon::Events::OPERATION_COMPLETED:
				complete = true;
				break;
			case GW::SYSTEM::GDaemon::Events::OPERATIONS_PAUSED:
				pause = true;
				break;
			case GW::SYSTEM::GDaemon::Events::OPERATIONS_RESUMING:
				resume = true;
				break;
			}
		}
	});
	// register
	testGDaemon.Register(listen);
	// pause then resume
	testGDaemon.Pause(true, 0);
	testGDaemon.Resume();
	// wait for all events to happen
	UTILS::Sleep_Exact(5);
	testGDaemon = nullptr;
	// did they transpire?
	REQUIRE(complete);
	REQUIRE(pause);
	REQUIRE(resume);
}

/* This test has been temporarily removed until we can determine why the test on line 235 is failing on 2 cores
TEST_CASE("GDaemon chained activation")
{
	if (EnableDaemonTests == false)
	{
		std::cout << "Multiple HW threads not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	std::atomic_int counter1 = { 0 }, counter2 = { 0 }, counter3 = { 0 }, counter4 = { 0 };
	GW::SYSTEM::GDaemon testGDaemon1, testGDaemon2, testGDaemon3, testGDaemon4;

	// Creates GDaemon4 in a paused state
	testGDaemon4.Create(100, [&]
	{
		// Pauses GDaemon3 when this daemon gets resumed in GDaemon3
		testGDaemon3.Pause(true, 0);
		// Recreates GDaemon1 with a new routine
		testGDaemon1.Create(50, [&] { counter4 += 1; });
		// Destroys itself after a single run
		testGDaemon4 = nullptr;
	}, 0); // 0 = paused

	// Creates GDaemon3 in a paused state
	testGDaemon3.Create(75, [&]
	{
		if (++counter3 == 10) // Keeps incrementing own counter until paused/destroyed
		{
			// Pauses GDaemon2 when this daemon gets resumed in GDaemon2 AND condition is met
			testGDaemon2.Pause(true, 0);
			// Resumes (wakes up) GDaemon4
			testGDaemon4.Resume();
			// Recreates itself when this GDaemon reaches the condition (counter3 == 10)
			testGDaemon3.Create(25, [&] { ++counter3; }); // Keeps incrementing with no condition and lower intervals. 
		}
	}, 0); // 0 = paused

	// Creates GDaemon2 in a paused state
	testGDaemon2.Create(25, [&]
	{
		// Keeps incrementing itself with no condition
		++counter2;
		// Continuously attempts to resume GDaemon3 (would result in REDUNDANT after the first call)
		testGDaemon3.Resume();
	}, 0); // 0 = paused

	// Creates GDaemon1 in a running state
	testGDaemon1.Create(50, [&] 
	{ 
		if (++counter1 == 10) // Keeps incrementing own counter until paused/destroyed
		{
			// When condition is met:
			// Pauses itself
			testGDaemon1.Pause(false, 0);
			// Resumes GDaemon2
			testGDaemon2.Resume();
		}
	});

	std::this_thread::sleep_for(std::chrono::milliseconds(2020)); // Run-time of ~2 seconds
	testGDaemon1 = nullptr;
	REQUIRE(counter1 == 10);  REQUIRE(counter4 >= 12); REQUIRE(counter4 <= 18); // Daemon1: Expected counter1 is 10 (first state) and counter2 is 15 (daemon1 recreated)
	REQUIRE(counter2 >= 29);  REQUIRE(counter2 <= 32);  // Daemon2: Expected counter2 is 29 (29 ticks before GDaemon2 goes into pause)
	REQUIRE(counter3 >= 13);  REQUIRE(counter3 <= 15);  // Daemon3: Expected counter3 is 14 (10 ticks in first state and 4 in the last state before GDaemon3 goes into pause)
}*/

#if 0 // verbose tests
TEST_CASE("Temporary testing for GDaemon", "[System]")
{
	//std::atomic_int counter_check = 0;
	GW::SYSTEM::GDaemon background;
	// cout is very slow so we will save string and print after
	GW::CORE::GThreadShared protec;
	std::vector<double> cache;
	cache.reserve(4096);
	protec.Create();
	// store messages here
	GW::CORE::GEventQueue keepem;
	keepem.Create(4096, background, nullptr);

	auto start = std::chrono::steady_clock::now();
	// not sure why 1ms goes to 2, 2 seems to be the minimum reliable polling
	background.Create(33, [&]() { // even with 1 overhead is minimal
		protec.LockSyncWrite();
		// display exactly how much time has passed
		auto end = std::chrono::steady_clock::now();
		cache.push_back(std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000.0);
		start = end;
		protec.UnlockSyncWrite();
	});
	// create another GDaemon that just prints messages from the other daemon
	int i = 0;
	GW::SYSTEM::GDaemon printer;
	printer.Create(1000, [&i, keepem, &cache, &protec]() mutable {
		// print anything in the queue
		while (+keepem.Find(GW::SYSTEM::GDaemon::Events::OPERATION_COMPLETED, true))
		{
			protec.LockAsyncRead();
			// print what we found
			for (; i < cache.size(); ++i)
			{
				// print something I guess
				std::cout << "Sup I am a Daemon here to be spooky in your background." << std::endl;
				std::cout << "I am the #" << i + 1 << " iteration." << std::endl;
				std::cout << "Raw elapsed time: " << cache[i] << " milliseconds." << std::endl;
			}
			protec.UnlockAsyncRead();
		}
	});

	// *** Still needs to be tested with multiple GDaemons, still needs pause & resume
	printer.Pause(false, 0);
	printer.Resume();

	unsigned long long count = 0;
	while (+printer.Counter(count) && count <= 1)
	{
		std::this_thread::yield();
		std::this_thread::sleep_for(std::chrono::microseconds(G_THREAD_DEFAULT_SLEEP));
	}

	background = nullptr; // kill it
	printer = nullptr;
	// print what we found
	for (int i = 0; i < cache.size(); ++i)
	{
		// print something I guess
		std::cout << "Sup I am a Daemon here to be spooky in your background." << std::endl;
		std::cout << "I am the #" << i + 1 << " iteration." << std::endl;
		std::cout << "Raw elapsed time: " << cache[i] << " milliseconds." << std::endl;
	}
}

TEST_CASE("Battery Test for GDaemon", "[System]")
{
	GW::CORE::GEventQueue dEventWatcher;
	// Because GDaemon is multithreading we need locks.
	GW::CORE::GThreadShared daemonLock;
	CHECK(+daemonLock.Create());
	std::vector<double> lapsedTimeKeeper; lapsedTimeKeeper.reserve(4096);

	auto startTime = std::chrono::steady_clock::now();
	// GDaemon create takes targetInterval and an Operation to perform.
	unsigned int tgtInterval = 33; //33 milliseconds = 30fps
	// This operation is to be expanded.
	//daemon_main.Create(33, nullptr);
	std::vector<GW::SYSTEM::GDaemon*> my_demons;
	my_demons.reserve(4096);

	std::cout << "Test 2: Daemon_one create\n" << std::endl;
	// GDaemon is a background process that works on intervals.
	// * IMPORTANT * Note how I moved the creation of this var to be AFTER the stack vars it refrences.
	// I COULD place it back at the top, but if I do the destructor will be called AFTER the local
	// vairables it REFRENCES have already destructed due to stack unwinding.
	// This is just a lambda thing not a GDaemon thing. If you want to get around this see the bottom of this test.
	GW::SYSTEM::GDaemon daemon_main;
	daemon_main.Create(tgtInterval, [&daemonLock, &startTime, &lapsedTimeKeeper, &my_demons]() {
		daemonLock.LockSyncWrite();

		// Push back elapsed time.
		auto currentTime = std::chrono::steady_clock::now();
		lapsedTimeKeeper.push_back(std::chrono::duration_cast<std::chrono::microseconds>(currentTime - startTime).count() / 1000.0);

		std::size_t howManyDemons = my_demons.size();
		GW::SYSTEM::GDaemon* new_demon = new GW::SYSTEM::GDaemon();
		new_demon->Create(66, [howManyDemons]() {
			for (int i = 0; i < 10; i++)
			{
				std::cout << "Inner demon #" << howManyDemons << " printing stuff:" << i << "\n";
			}
		}, true);
		my_demons.push_back(new_demon);

		startTime = currentTime;

		daemonLock.UnlockSyncWrite();
	});

	std::cout << "Test 2: Daemon_two create\n" << std::endl;
	//CHECK(+daemonLock.Create());
	GW::SYSTEM::GDaemon daemon_second;
	//daemon_second.Create(33, nullptr);
	daemon_second.Create(tgtInterval, [&daemonLock, &startTime, &lapsedTimeKeeper, &my_demons]() {
		daemonLock.LockSyncWrite();

		for (int i = 0; i < my_demons.size(); i++)
		{
			std::cout << "Pausing demon #:" << i << std::endl;
			my_demons[i]->Pause(false, 2);
			std::this_thread::sleep_for(std::chrono::microseconds(100));
			std::cout << "Resuming demon #:" << i << std::endl;
			my_demons[i]->Resume();
		}

		for (int i = 0; i < lapsedTimeKeeper.size(); i++)
		{
			std::cout << "Time passed: " << lapsedTimeKeeper[i] << std::endl;
		}
		daemonLock.UnlockSyncWrite();
	});

	// wait so stuff will actually happen
	unsigned long long count = 0;
	while (+daemon_main.Counter(count) && count <= 1)
	{
		std::this_thread::yield();
		std::this_thread::sleep_for(std::chrono::microseconds(G_THREAD_DEFAULT_SLEEP));
	}

	std::cout << "Test 2: Daemon_one delete\n" << std::endl;
	// Clean Up - not needed but doing it for test purposes. update: may be needed.
	///daemon_main.Pause(true, 1000);
	//daemon_main = nullptr;
	//daemonLock = nullptr;

	std::cout << "Test 2: Daemon_two delete\n" << std::endl;
	// Clean Up - not needed but doing it for test purposes. update: may be needed.
	///daemon_second.Pause(true, 1000);
	//daemon_second = nullptr;
	//daemonLock = nullptr;

	daemonLock.LockSyncWrite();
	for (auto i = my_demons.begin(); i != my_demons.end(); ++i)
		delete* i;
	my_demons.clear();
	daemonLock.UnlockSyncWrite();

	// TEST
	// The stack unwinds (destructs) in reverse order of creation
	// This means that any locally refrenced variable in a lambda
	// is only safe if it is declared BEFORE the GDaemon itself.
	// However, the new GDaemon does now garuntee no more(of its) 
	// jobs run after it is destructed. So as long as you follow 
	// C++ stack unwinding rules (RAII) you should be safe.

	///daemon_main = nullptr;
	///daemon_second = nullptr;

	// If you break these ordering rules you should still be able to
	// make your stack refrence access safe by forcing the Daemons to
	// destruct before any local variables by releasing them ealry.
}
#endif /* VERBOSE TESTS */
#endif /* defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GDAEMON) */