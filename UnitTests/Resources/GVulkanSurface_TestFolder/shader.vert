#version 450

layout(location = 0) in vec4 local_pos;
layout(location = 1) in vec4 uv_pos;

layout(location = 0) out vec2 uvLoc;

void main()
{
    gl_Position = local_pos;
    uvLoc = uv_pos.xy;
}