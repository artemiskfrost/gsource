#if defined(GATEWARE_ENABLE_MATH2D) && !defined(GATEWARE_DISABLE_GMATRIX2D)
TEST_CASE("Create GMatrix2D.", "[CreateGMatrix2D], [Math2D]")
{
	GW::MATH2D::GMatrix2D mClass;

	//Pass Cases
	REQUIRE(+(mClass.Create()));
}

TEST_CASE("Add two matrix2s.", "[Add2F], [Add2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF1 = { 0.0f,-1.0f, 0.0f, 0.0f };
	GW::MATH2D::GMATRIX2F mF2 = { 0.2f, 1.0f, 2.5f, 0.4f };
	GW::MATH2D::GMATRIX2F resultF;


	GW::MATH2D::GMATRIX2D mD1 = { 0.0,-1.0, 0.0, 0.0 };
	GW::MATH2D::GMATRIX2D mD2 = { 0.2, 1.0, 2.5, 0.4 };
	GW::MATH2D::GMATRIX2D resultD;

	//Fail cases
	SECTION("Float matrix2 addition.", "[Add2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Add2F(mF1, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 0.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 2.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 0.4f));
	}
	SECTION("Double matrix2 addition.", "[Add2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Add2D(mD1, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 0.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 2.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 0.4));
	}
}

TEST_CASE("Add two matrix3s.", "[Add3F], [Add3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF1 = { 1.0f, 1.0f, 1.0f,
								  1.0f, 1.0f, 1.0f,
								  1.0f, 1.0f, 1.0f };

	GW::MATH2D::GMATRIX3F mF2 = { 1.0f, 1.0f, 1.0f,
								  1.0f, 1.0f, 1.0f,
								  1.0f, 1.0f, 1.0f, };
	GW::MATH2D::GMATRIX3F resultF;


	GW::MATH2D::GMATRIX3D mD1 = { 1.0, 1.0, 1.0,
								  1.0, 1.0, 1.0,
								  1.0, 1.0, 1.0 };

	GW::MATH2D::GMATRIX3D mD2 = { 1.0, 1.0, 1.0,
								  1.0, 1.0, 1.0,
								  1.0, 1.0, 1.0 };
	GW::MATH2D::GMATRIX3D resultD;

	//Fail cases
	SECTION("Float matrix3 addition.", "[Add3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Add3F(mF1, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], 2.0f));
	}
	SECTION("Double matrix3 addition.", "[Add3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Add3D(mD1, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], 2.0));
	}
}

TEST_CASE("Subtract two matrix2s.", "[Subtract2F], [Subtract2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF1 = { 0.0f, 0.0f, 0.0f, 0.0f };
	GW::MATH2D::GMATRIX2F mF2 = { 0.2f, 1.0f, 2.5f, 0.4f };
	GW::MATH2D::GMATRIX2F resultF;


	GW::MATH2D::GMATRIX2D mD1 = { 0.0, 0.0, 0.0, 0.0 };
	GW::MATH2D::GMATRIX2D mD2 = { 0.2, 1.0, 2.5, 0.4 };
	GW::MATH2D::GMATRIX2D resultD;
					   
	//Fail cases
	SECTION("Float matrix2 subtraction.", "[Subtract2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Subtract2F(mF1, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -0.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], -2.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], -0.4f));
	}
	SECTION("Double matrix2 subtraction.", "[Subtract2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Subtract2D(mD1, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -0.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], -2.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], -0.4));
	}
}

TEST_CASE("Subtract two matrix3s.", "[Subtract3F], [Subtract3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF1 = { 0.0f, 0.0f, 0.0f,
								  0.0f, 0.0f, 0.0f,
								  0.0f, 0.0f, 0.0f };

	GW::MATH2D::GMATRIX3F mF2 = { 1.0f, 2.0f, 3.0f,
								  4.0f, 5.0f, 6.0f,
								  7.0f, 8.0f, 9.0f };
	GW::MATH2D::GMATRIX3F resultF;


	GW::MATH2D::GMATRIX3D mD1 = { 0.0, 0.0, 0.0,
								  0.0, 0.0, 0.0,
								  0.0, 0.0, 0.0 };

	GW::MATH2D::GMATRIX3D mD2 = { 1.0, 2.0, 3.0,
								  4.0, 5.0, 6.0,
								  7.0, 8.0, 9.0 };
	GW::MATH2D::GMATRIX3D resultD;

	//Fail cases
	SECTION("Float matrix3 subtraction.", "[Subtract3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Subtract3F(mF1, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], -3.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], -4.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], -5.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], -6.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], -7.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], -8.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], -9.0f));;
	}
	SECTION("Double matrix3 subtraction.", "[Subtract3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Subtract3D(mD1, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], -3.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], -4.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], -5.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], -6.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], -7.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], -8.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], -9.0));
	}
}

TEST_CASE("Multiply a matrix2 by a matrix2.", "[Multiply2F], [Multiply2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF1 = { 0.0f,-1.0f, 0.0f, 0.0f };
	GW::MATH2D::GMATRIX2F mF2 = { 0.2f, 1.0f, 2.5f, 0.4f };
	GW::MATH2D::GMATRIX2F resultF;


	GW::MATH2D::GMATRIX2D mD1 = { 0.0,-1.0, 0.0, 0.0 };
	GW::MATH2D::GMATRIX2D mD2 = { 0.2, 1.0, 2.5, 0.4 };
	GW::MATH2D::GMATRIX2D resultD;

	SECTION("Float matrix2 multiplication.", "[Multiply2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Multiply2F(mF1, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -2.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -0.4f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 0.0f));
	}
	SECTION("Double matrix2 multiplication.", "[Multiply2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Multiply2D(mD1, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -2.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -0.4));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 0.0));
	}
}

TEST_CASE("Multiply a matrix3 by a matrix3.", "[Multiply3F], [Multiply3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF1 = { 1.0f, 2.0f, 3.0f,
								  4.0f, 5.0f, 6.0f,
								  7.0f, 8.0f, 9.0f };

	GW::MATH2D::GMATRIX3F mF2 = { 2.0f, 0.0f, 0.0f,
								  0.0f, 2.0f, 0.0f,
								  0.0f, 0.0f, 2.0f };
	GW::MATH2D::GMATRIX3F resultF;
					   

	GW::MATH2D::GMATRIX3D mD1 = { 1.0, 2.0, 3.0,
								  4.0, 5.0, 6.0,
								  7.0, 8.0, 9.0 };

	GW::MATH2D::GMATRIX3D mD2 = { 2.0, 0.0, 0.0,
								  0.0, 2.0, 0.0,
								  0.0, 0.0, 2.0 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3 multiplication.", "[Multiply3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Multiply3F(mF1, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 4.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 6.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 8.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 10.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], 12.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], 14.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], 16.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], 18.0f));
	}
	SECTION("Double matrix3 multiplication.", "[Multiply3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Multiply3D(mD1, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 4.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 6.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 8.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 10.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], 12.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], 14.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], 16.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], 18.0));
	}
}

TEST_CASE("Multiply a matrix2 by a vector2.", "[MatrixXVector2F], [MatrixXVector2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF = { 1.0f, -0.2f };
	GW::MATH2D::GMATRIX2F mF = { 0.2f, 1.0f, 2.5f, 0.4f };
	GW::MATH2D::GVECTOR2F resultF;


	GW::MATH2D::GVECTOR2D vD = { 1.0, -0.2 };
	GW::MATH2D::GMATRIX2D mD = { 0.2, 1.0, 2.5, 0.4 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float matrix2 multiplication.", "[MatrixXVector2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MatrixXVector2F(mF, vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -0.3f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 0.92f));
	}
	SECTION("Double matrix2 multiplication.", "[MatrixXVector2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MatrixXVector2D(mD, vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -0.3));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 0.92));
	}
}

TEST_CASE("Multiply a matrix3 by a vector3.", "[MatrixXVector3F], [MatrixXVector3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 1.0f, 2.0f, 3.0f,
								 4.0f, 5.0f, 6.0f,
								 7.0f, 8.0f, 9.0f };

	GW::MATH2D::GVECTOR3F vF = { 1.0f, 2.0f, 3.0f };
	GW::MATH2D::GVECTOR3F resultF;


	GW::MATH2D::GMATRIX3D mD = { 1.0, 2.0, 3.0,
								 4.0, 5.0, 6.0,
								 7.0, 8.0, 9.0 };

	GW::MATH2D::GVECTOR3D vD = { 1.0, 2.0, 3.0 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float matrix3 multiplication.", "[MatrixXVector3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MatrixXVector3F(mF, vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 30.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 36.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 42.0f));
	}
	SECTION("Double matrix3 multiplication.", "[MatrixXVector3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MatrixXVector3D(mD, vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 30.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 36.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 42.0));
	}
}

TEST_CASE("Multiply a matrix2 by a number.", "[MultiplyNum2F], [MultiplyNum2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF = { 0.2f, 1.0f, 2.5f, 0.4f };
	GW::MATH2D::GMATRIX2F resultF;


	GW::MATH2D::GMATRIX2D mD = { 0.2, 1.0, 2.5, 0.4 };
	GW::MATH2D::GMATRIX2D resultD;

	SECTION("Float matrix2 multiplication.", "[MultiplyNum2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MultiplyNum2F(mF, -0.3f, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -0.06f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -0.3f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], -0.75f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], -0.12f));
	}
	SECTION("Double matrix2 multiplication.", "[MultiplyNum2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MultiplyNum2D(mD, -0.3, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -0.06));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -0.3));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], -0.75));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], -0.12));
	}
}

TEST_CASE("Multiply a matrix3 by a number.", "[MultiplyNum3F], [MultiplyNum3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 0.2f, 1.0f, 2.5f,
								 0.4f, 0.7f, 5.9f,
								 2.2f, 4.1f, 3.8f };
	GW::MATH2D::GMATRIX3F resultF;


	GW::MATH2D::GMATRIX3D mD = { 0.2, 1.0, 2.5,
								 0.4, 0.7, 5.9, 
								 2.2, 4.1, 3.8 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3 multiplication.", "[MultiplyNum3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MultiplyNum3F(mF, -0.3f, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -0.06f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -0.3f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], -0.75f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], -0.12f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], -0.21f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], -1.77f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], -0.66f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], -1.23f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], -1.14f));
	}
	SECTION("Double matrix3 multiplication.", "[MultiplyNum3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MultiplyNum3D(mD, -0.3, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -0.06));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -0.3));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], -0.75));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], -0.12));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], -0.21));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], -1.77));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], -0.66));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], -1.23));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], -1.14));
	}
}

TEST_CASE("Calculate a determinant of a matrix2.", "[Determinant2F], [Determinant2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF = { 0.2f, 1.0f, 2.5f, 0.4f };
	float resultF;

	GW::MATH2D::GMATRIX2D mD = { 0.2, 1.0, 2.5, 0.4 };
	double resultD;

	SECTION("Float matrix2 determinant.", "[Determinant2F]")
	{
		GW::MATH2D::GMatrix2D::Determinant2F(mF, resultF);
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, -2.42f));
	}
	SECTION("Double matrix2 determinant.", "[Determinant2D]")
	{
		GW::MATH2D::GMatrix2D::Determinant2D(mD, resultD);
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, -2.42));
	}
}

TEST_CASE("Calculate a determinant of a matrix3.", "[Determinant3F], [Determinant3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 0.2f, 1.0f, 2.5f, 0.4f, 0.7f, 5.9f, 2.2f, 4.1f, 3.8f };
	float resultF;

	GW::MATH2D::GMATRIX3D mD = { 0.2, 1.0, 2.5, 0.4, 0.7, 5.9, 2.2, 4.1, 3.8 };
	double resultD;

	SECTION("Float matrix3 determinant.", "[Determinant3F]")
	{
		GW::MATH2D::GMatrix2D::Determinant3F(mF, resultF);
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 7.404f));
	}
	SECTION("Double matrix3 determinant.", "[Determinant3D]")
	{
		GW::MATH2D::GMatrix2D::Determinant3D(mD, resultD);
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 7.404));
	}
}

TEST_CASE("Transpose a matrix2.", "[Transpose2F], [Transpose2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF = { 0.2f, 1.0f, 2.5f, 0.4f };
	GW::MATH2D::GMATRIX2F resultF;

	GW::MATH2D::GMATRIX2D mD = { 0.2, 1.0, 2.5, 0.4 };
	GW::MATH2D::GMATRIX2D resultD;

	SECTION("Float matrix2 transpose.", "[Transpose2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Transpose2F(mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 0.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 2.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 0.4f));
	}
	SECTION("Double matrix2 transpose.", "[Transpose2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Transpose2D(mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 0.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 2.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 0.4));
	}
}

TEST_CASE("Transpose a matrix3.", "[Transpose3F], [Transpose3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 0.2f, 1.0f, 2.5f, 0.4f, 0.7f, 5.9f, 2.2f, 4.1f, 3.8f };
	GW::MATH2D::GMATRIX3F resultF;
					   
	GW::MATH2D::GMATRIX3D mD = { 0.2, 1.0, 2.5, 0.4, 0.7, 5.9, 2.2, 4.1, 3.8 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3 transpose.", "[Transpose3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Transpose3F(mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 0.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 0.4f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 2.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 0.7f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], 4.1f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], 2.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], 5.9f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], 3.8f));
	}
	SECTION("Double matrix3 transpose.", "[Transpose3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Transpose3D(mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 0.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 0.4));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 2.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 0.7));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], 4.1));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], 2.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], 5.9));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], 3.8));
	}
}

TEST_CASE("Inverse a matrix2.", "[Inverse2F], [Inverse2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF = { 5.0f, 2.0f, -7.0f, -3.0f };
	GW::MATH2D::GMATRIX2F resultF;

	GW::MATH2D::GMATRIX2D mD = { 5.0, 2.0, -7.0, -3.0 };
	GW::MATH2D::GMATRIX2D resultD;

	SECTION("Float matrix2 inverse.", "[Inverse2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Inverse2F(mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0],  3.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1],  2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], -7.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], -5.0f));
	}
	SECTION("Double matrix2 inverse.", "[Inverse2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Inverse2D(mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0],  3.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1],  2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], -7.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], -5.0));
	}
}

TEST_CASE("Inverse a matrix3.", "[Inverse3F], [Inverse3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 3.0f, 0.0f, 2.0f,
								2.0f, 0.0f, -2.0f,
								0.0f, 1.0f, 1.0f };
	GW::MATH2D::GMATRIX3F resultF;

	GW::MATH2D::GMATRIX3D mD = { 3.0, 0.0, 2.0,
								2.0, 0.0, -2.0,
								0.0, 1.0, 1.0 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3 inverse.", "[Inverse3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Inverse3F(mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0],  0.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1],  0.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2],  0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], -0.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4],  0.3f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5],  1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6],  0.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], -0.3f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8],  0.0f));
	}
	SECTION("Double matrix3 inverse.", "[Inverse3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Inverse3D(mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0],  0.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1],  0.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2],  0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], -0.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4],  0.3));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5],  1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6],  0.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], -0.3));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8],  0.0));
	}
}

TEST_CASE("Get a rotation value from a matrix2.", "[GetRotation2F], [GetRotation2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF = { 0.0f, 1.0f,
								 1.0f, 0.0f };
	float resultF;

	GW::MATH2D::GMATRIX2D mD = { 0.0, 1.0,
								 1.0, 0.0 };
	double resultD;

	SECTION("Float matrix2.", "[GetRotation2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetRotation2F(mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 1.57079632f));
	}
	SECTION("Double matrix2.", "[GetRotation2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetRotation2D(mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 1.5707963267948966));
	}
}

TEST_CASE("Get a rotation value from a matrix3.", "[GetRotation3F], [GetRotation3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 0.0f, 1.0f, 0.0f,
								 1.0f, 0.0f, 0.0f,
								 0.0f, 0.0f, 0.0f };
	float resultF;

	GW::MATH2D::GMATRIX3D mD = { 0.0, 1.0, 0.0,
								 1.0, 0.0, 0.0,
								 0.0, 0.0, 0.0 };
	double resultD;

	SECTION("Float matrix2.", "[GetRotation2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetRotation3F(mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 1.57079632f));
	}
	SECTION("Double matrix2.", "[GetRotation2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetRotation3D(mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 1.5707963267948966));
	}
}

TEST_CASE("Get a translation vector2 form a transform matrix3.", "[GetTranslation3F], [GetTranslation3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { -2.0f, -1.0f, 0.0f,
								  2.0f, -2.0f, 1.0f, 
								  0.0f, -1.0f, 0.0f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GMATRIX3D mD = { -2.0, -1.0, 0.0,
								  2.0, -2.0, 1.0,
								  0.0, -1.0, 0.0 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float matrix3.", "[GetTranslation3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetTranslation3F(mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0],  0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -1.0f));
	}
	SECTION("Double matrix3.", "[GetTranslation3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetTranslation3D(mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0],  0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -1.0));
	}
}

TEST_CASE("Get a scale vector2 from a matrix2.", "[GetScale2F], [GetScale2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF = { 2.0f, 0.0f, 0.0f, 4.0f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GMATRIX2D mD = { 2.0, 0.0, 0.0, 4.0 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float matrix2.", "[GetScale2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetScale2F(mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 4.0f));
	}
	SECTION("Double matrix2.", "[GetScale2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetScale2D(mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 4.0));
	}
}

TEST_CASE("Get a scale vector2 from a matrix3.", "[GetScale3F], [GetScale3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 2.0f, 0.0f, 0.0f,
								0.0f, 4.0f, 0.0f,
								6.0f, -5.0f, 0.0f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GMATRIX3D mD = { 2.0, 0.0, 0.0,
								0.0, 4.0, 0.0,
								6.0, -5.0, 0.0 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float matrix3.", "[GetScale3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetScale3F(mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 4.0f));
	}
	SECTION("Double matrix3.", "[GetScale3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::GetScale3D(mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 4.0));
	}
}

TEST_CASE("Rotate a matrix2 by radians.", "[Rotate2F], [Rotate2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF = { 1.0f, 0.0f, 0.0f, 1.0f };
	GW::MATH2D::GMATRIX2F resultF;

	GW::MATH2D::GMATRIX2D mD = { 1.0, 0.0, 0.0, 1.0 };
	GW::MATH2D::GMATRIX2D resultD;

	SECTION("Float matrix3.", "[Rotate2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Rotate2F(mF, G2D_DEGREE_TO_RADIAN_F(90), resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 0.0f));
	}
	SECTION("Double matrix3.", "[Rotate2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Rotate2D(mD, G2D_DEGREE_TO_RADIAN_D(90), resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 0.0));
	}
}

TEST_CASE("Rotate a matrix3 by radians, globally. A * B = C", "[RotateGlobal3F], [RotateGlobal3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 1.0f, 0.0f, 0.0f,
								0.0f, 1.0f, 0.0f,
								10.0f, 1.2f, -9.0f};
	GW::MATH2D::GMATRIX3F resultF;

	GW::MATH2D::GMATRIX3D mD = { 1.0, 0.0, 0.0,
								0.0, 1.0, 0.0,
								10.0, 1.2, -9.0 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3.", "[RotateGlobal3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::RotateGlobal3F(mF, G2D_DEGREE_TO_RADIAN_F(90), resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], 10.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], 1.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], -9.0f));
	}
	SECTION("Double matrix3.", "[RotateGlobal3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::RotateGlobal3D(mD, G2D_DEGREE_TO_RADIAN_D(90),  resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], 10.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], 1.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], -9.0));
	}
}

TEST_CASE("Rotate a matrix3 by radians, locally. B * A = C", "[RotateLocal3F], [RotateLocal3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 1.0f, 0.0f, 0.0f,
								0.0f, 1.0f, 0.0f,
								10.0f, 1.2f, -9.0f };
	GW::MATH2D::GMATRIX3F resultF;

	GW::MATH2D::GMATRIX3D mD = { 1.0, 0.0, 0.0,
								0.0, 1.0, 0.0,
								10.0, 1.2, -9.0 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3.", "[RotateLocal3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::RotateLocal3F(mF, G2D_DEGREE_TO_RADIAN_F(90), resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], 10.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], 1.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], -9.0f));
	}
	SECTION("Double matrix3.", "[RotateLocal3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::RotateLocal3D(mD, G2D_DEGREE_TO_RADIAN_D(90), resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], 10.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], 1.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], -9.0));
	}
}
TEST_CASE("Translate a matrix3 by a vector2, globally. A * B = C", "[TranslateGlobal3F], [TranslateGlobal3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 1.0f, 2.0f, 3.0f,
								4.0f, 5.0f, 6.0f,
								7.0f, 8.0f, 9.0f };
	GW::MATH2D::GVECTOR2F vF = { 5.0f, 10.0f };
	GW::MATH2D::GMATRIX3F resultF;

	GW::MATH2D::GMATRIX3D mD = { 1.0, 2.0, 3.0,
								4.0, 5.0, 6.0,
								7.0, 8.0, 9.0 };
	GW::MATH2D::GVECTOR2D vD = { 5.0, 10.0 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3 translation.", "[TranslateGlobal3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::TranslateGlobal3F(mF, vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 16.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 32.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2],  3.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 34.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 65.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5],  6.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], 52.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], 98.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8],  9.0f));
	}
	SECTION("Double matrix3 translation.", "[TranslateGlobal3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::TranslateGlobal3D(mD, vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 16.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 32.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2],  3.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 34.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 65.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5],  6.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], 52.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], 98.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8],  9.0));
	}
}

TEST_CASE("Translate a matrix3 by a vector2, locally. B * A = C", "[TranslateLocal3F], [TranslateLocal3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 1.0f, 2.0f, 3.0f,
								4.0f, 5.0f, 6.0f,
								7.0f, 8.0f, 9.0f };
	GW::MATH2D::GVECTOR2F vF = { 5.0f, 10.0f };
	GW::MATH2D::GMATRIX3F resultF;

	GW::MATH2D::GMATRIX3D mD = { 1.0, 2.0, 3.0,
								4.0, 5.0, 6.0,
								7.0, 8.0, 9.0 };
	GW::MATH2D::GVECTOR2D vD = { 5.0, 10.0 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3 translation.", "[TranslateLocal3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::TranslateLocal3F(mF, vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0],  1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1],  2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2],  3.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3],  4.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4],  5.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5],  6.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], 52.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], 68.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], 84.0f));
	}
	SECTION("Double matrix3 translation.", "[TranslateLocal3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::TranslateLocal3D(mD, vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0],  1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1],  2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2],  3.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3],  4.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4],  5.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5],  6.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], 52.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], 68.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], 84.0));
	}
}

TEST_CASE("Scale a matrix2 by vector2.", "[Scale2F], [Scale2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF = { -2.0f, -1.0f, 0.0f, 2.0f };
	GW::MATH2D::GVECTOR2F vF = { 0.3f, 2 };
	GW::MATH2D::GMATRIX2F resultF;

	GW::MATH2D::GMATRIX2D mD = { -2.0, -1.0, 0.0, 2.0 };
	GW::MATH2D::GVECTOR2D vD = { 0.3, 2 };
	GW::MATH2D::GMATRIX2D resultD;

	SECTION("Float matrix2 Scaling.", "[Scale2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Scale2F(mF, vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -0.6f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 4.0f));
	}
	SECTION("Double matrix2 Scaling.", "[Scale2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Scale2D(mD, vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -0.6));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 4.0));
	}
}

TEST_CASE("Scale a matrix3 by a vector2, globally. A * B = C", "[ScaleGlobal3F], [ScaleGlobal3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 1.0f, 2.0f, 3.0f,
								4.0f, 5.0f, 6.0f,
								7.0f, 8.0f, 9.0f };
	GW::MATH2D::GVECTOR2F vF = { 0.5f, 2.0f };
	GW::MATH2D::GMATRIX3F resultF;

	GW::MATH2D::GMATRIX3D mD = { 1.0, 2.0, 3.0,
								4.0, 5.0, 6.0,
								7.0, 8.0, 9.0 };
	GW::MATH2D::GVECTOR2D vD = { 0.5, 2.0 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3 scale.", "[ScaleGlobal3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::ScaleGlobal3F(mF, vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0],  0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1],  4.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2],  3.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3],  2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 10.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5],  6.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6],  7.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7],  8.0));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8],  9.0f));
	}
	SECTION("Double matrix3 scale.", "[ScaleGlobal3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::ScaleGlobal3D(mD, vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0],  0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1],  4.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2],  3.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3],  2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 10.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5],  6.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6],  7.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7],  8.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8],  9.0));
	}
}

TEST_CASE("Scale a matrix3 by a vector2, locally. B * A = C", "[ScaleLocal3F], [ScaleLocal3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF = { 1.0f, 2.0f, 3.0f,
								4.0f, 5.0f, 6.0f,
								7.0f, 8.0f, 9.0f };
	GW::MATH2D::GVECTOR2F vF = { 0.5f, 2.0f };
	GW::MATH2D::GMATRIX3F resultF;

	GW::MATH2D::GMATRIX3D mD = { 1.0, 2.0, 3.0,
								4.0, 5.0, 6.0,
								7.0, 8.0, 9.0 };
	GW::MATH2D::GVECTOR2D vD = { 0.5, 2.0 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3 Scale.", "[ScaleLocal3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::ScaleLocal3F(mF, vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0],  0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1],  1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2],  1.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3],  8.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 10.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], 12.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6],  7.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7],  8.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8],  9.0f));
	}
	SECTION("Double matrix3 Scale.", "[ScaleLocal3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::ScaleLocal3D(mD, vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0],  0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1],  1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2],  1.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3],  8.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 10.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], 12.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6],  7.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7],  8.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8],  9.0));
	}
}

TEST_CASE("Lerp between two matrix2s.", "[Lerp2F], [Lerp2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF1 = { -2.0f, -1.0f, 0.0f, 2.0f };
	GW::MATH2D::GMATRIX2F mF2 = { 1.0f, 1.0f, 1.0f, 1.0f };
	GW::MATH2D::GMATRIX2F resultF;

	GW::MATH2D::GMATRIX2D mD1 = { -2.0, -1.0, 0.0, 2.0 };
	GW::MATH2D::GMATRIX2D mD2 = { 1.0, 1.f, 1.0, 1.0 };
	GW::MATH2D::GMATRIX2D resultD;

	SECTION("Float matrix2.", "[LerpF]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Lerp2F(mF1, mF2, 0.5f, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 1.5f));
	}
	SECTION("Double matrix2.", "[Lerp2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Lerp2D(mD1, mD2, 0.5, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 1.5));
	}
}

TEST_CASE("Lerp between two matrix3s.", "[Lerp3F], [Lerp3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF1 = { -2.0f, -1.0f, 0.0f,
								2.0f, 0.0f, 1.0f,
								2.0f, 3.0f, 4.0f };
	GW::MATH2D::GMATRIX3F mF2 = GW::MATH2D::GZeroMatrix3F;
	GW::MATH2D::GMATRIX3F resultF;

	GW::MATH2D::GMATRIX3D mD1 = { -2.0, -1.0, 0.0,
								2.0, 0.0, 1.0,
								2.0, 3.0, 4.0 };
	GW::MATH2D::GMATRIX3D mD2 = GW::MATH2D::GZeroMatrix3D;
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3.", "[Lerp3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Lerp3F(mF1, mF2, 0.5f, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], 0.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], 1.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], 2.0f));
	}
	SECTION("Double matrix3.", "[Lerp3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Lerp3D(mD1, mD2, 0.5, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], 0.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], 1.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], 2.0));
	}
}

TEST_CASE("Make two matrix2s relative.", "[MakeRelative2F], [MakeRelative2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF1 = { 5.0f, -1.0f, 2.0f, 4.0f };
	GW::MATH2D::GMATRIX2F mF2 = { 0.2f, 1.0f, 2.5f, 0.4f };
	GW::MATH2D::GMATRIX2F resultF;


	GW::MATH2D::GMATRIX2D mD1 = { 5.0, -1.0, 2.0, 4.0 };
	GW::MATH2D::GMATRIX2D mD2 = { 0.2, 1.0, 2.5, 0.4 };
	GW::MATH2D::GMATRIX2D resultD;

	SECTION("Float matrix2 relativation.", "[MakeRelative2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MakeRelative2F(mF1, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -1.85950413f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1],  2.14876033f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2],  3.80165289f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3],  0.49586776f));
	}
	SECTION("Double matrix2 relativation.", "[MakeRelative2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MakeRelative2D(mD1, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -1.8595041322314052));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1],  2.1487603305785128));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2],  3.8016528925619841));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3],  0.49586776859504134));
	}
}

TEST_CASE("Make two matrix3s relative.", "[MakeRelative3F], [MakeRelative3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF1 = { -2.0f, -1.0f, 0.0f,
								2.0f, 0.0f, 1.0f,
								2.0f, 3.0f, 4.0f };
	GW::MATH2D::GMATRIX3F mF2 = { 5.0f, 1.5f, 3.0f,
								1.0f, 0.5f, 0.0f,
								-3.0f, 2.0f, -4.0f };
	GW::MATH2D::GMATRIX3F resultF;


	GW::MATH2D::GMATRIX3D mD1 = { -2.0, -1.0, 0.0,
								2.0, 0.0, 1.0,
								2.0, 3.0, 4.0 };
	GW::MATH2D::GMATRIX3D mD2 = { 5.0, 1.5, 3.0,
								1.0, 0.5, 0.0,
								-3.0, 2.0, -4.0 };
	GW::MATH2D::GMATRIX3D resultD;

	SECTION("Float matrix3 relativation.", "[MakeRelative3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MakeRelative3F(mF1, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0],   0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1],  -2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2],   0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3],  -0.07692307f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4],   1.46153846f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5],  -0.30769230f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6],   3.38461538f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], -10.30769230f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8],   1.53846153f));
	}
	SECTION("Double matrix3 relativation.", "[MakeRelative3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MakeRelative3D(mD1, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0],   0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1],  -2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2],   0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3],  -0.076923076923076872));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4],   1.4615384615384617));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5],  -0.30769230769230771));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6],   3.3846153846153850));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], -10.307692307692308));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8],   1.5384615384615383));
	}
}

TEST_CASE("Make two matrix2s un-relative.", "[MakeSeparate2F], [MakeSeparate2D], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF1 = { 5.0f, -1.0f, 2.0f, 4.0f };
	GW::MATH2D::GMATRIX2F mF2 = { 0.2f, 1.0f, 2.5f, 0.4f };
	GW::MATH2D::GMATRIX2F relativeResultF;
	GW::MATH2D::GMATRIX2F resultF;


	GW::MATH2D::GMATRIX2D mD1 = { 5.0, -1.0, 2.0, 4.0 };
	GW::MATH2D::GMATRIX2D mD2 = { 0.2, 1.0, 2.5, 0.4 };
	GW::MATH2D::GMATRIX2D relativeResultD;
	GW::MATH2D::GMATRIX2D resultD;

	GW::MATH2D::GMatrix2D::MakeRelative2F(mF1, mF2, relativeResultF);
	GW::MATH2D::GMatrix2D::MakeRelative2D(mD1, mD2, relativeResultD);

	SECTION("Float matrix2 un-relativation.", "[MakeSeparate2F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MakeSeparate2F(relativeResultF, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], 5.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 4.0f));
	}
	SECTION("Double matrix2 un-relativation.", "[MakeSeparate2D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MakeSeparate2D(relativeResultD, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], 5.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 4.0));
	}
}

TEST_CASE("Make two matrix3s un-relative.", "[MakeSeparate3F], [MakeSeparate3D], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF1 = { -2.0f, -1.0f, 0.0f,
								2.0f, 0.0f, 1.0f,
								2.0f, 3.0f, 4.0f };
	GW::MATH2D::GMATRIX3F mF2 = { 5.0f, 1.5f, 3.0f,
								1.0f, 0.5f, 0.0f,
								-3.0f, 2.0f, -4.0f };
	GW::MATH2D::GMATRIX3F relativeResultF;
	GW::MATH2D::GMATRIX3F resultF;


	GW::MATH2D::GMATRIX3D mD1 = { -2.0, -1.0, 0.0,
								2.0, 0.0, 1.0,
								2.0, 3.0, 4.0 };
	GW::MATH2D::GMATRIX3D mD2 = { 5.0, 1.5, 3.0,
								1.0, 0.5, 0.0,
								-3.0, 2.0, -4.0 };
	GW::MATH2D::GMATRIX3D relativeResultD;
	GW::MATH2D::GMATRIX3D resultD;

	GW::MATH2D::GMatrix2D::MakeRelative3F(mF1, mF2, relativeResultF);
	GW::MATH2D::GMatrix2D::MakeRelative3D(mD1, mD2, relativeResultD);

	SECTION("Float matrix3 un-relativation.", "[MakeSeparate3F]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MakeSeparate3F(relativeResultF, mF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[0], -2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[1], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[2], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[3], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[4], 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[5], 1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[6], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[7], 3.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.data[8], 4.0f));
	}
	SECTION("Double matrix3 un-relativation.", "[MakeSeparate3D]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::MakeSeparate3D(relativeResultD, mD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[0], -2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[1], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[2], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[3], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[4], 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[5], 1.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[6], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[7], 3.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.data[8], 4.0));
	}
}

TEST_CASE("Cast matrix2s between float and double.", "[Upgrade2], [Downgrade2], [Math2D]")
{
	GW::MATH2D::GMATRIX2F mF1 = { 5.0f, -1.0f, 2.0f, 4.0f };
	GW::MATH2D::GMATRIX2F mF2;

	GW::MATH2D::GMATRIX2D mD1 = { 5.0, -1.0, 2.0, 4.0 };
	GW::MATH2D::GMATRIX2D mD2;

	// both of these tests use G_COMPARISION_STANDARD_F for the following reason:
	// since we are converting between two types, we can only reliably test with 
	// the accuracy of the least accurate type. ex: testing float precision using 
	// double precision will cause false positive errors. 
	SECTION("Cast float matrix2 to double matrix2.", "[Upgrade2]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Upgrade2(mF1, mD2)));
		CHECK(G2D_COMPARISON_STANDARD_F(mD2.data[0], 5.0));
		CHECK(G2D_COMPARISON_STANDARD_F(mD2.data[1], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_F(mD2.data[2], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_F(mD2.data[3], 4.0));
	}
	SECTION("Cast double matrix2 to float matrix2.", "[Downgrade2]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Downgrade2(mD1, mF2)));
		CHECK(G2D_COMPARISON_STANDARD_F(mF2.data[0], 5.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(mF2.data[1], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(mF2.data[2], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(mF2.data[3], 4.0f));
	}
}

TEST_CASE("Cast matrix3s between float and double.", "[Upgrade3], [Downgrade3], [Math2D]")
{
	GW::MATH2D::GMATRIX3F mF1 = { 5.0f, -1.0f, 2.0f, 4.0f };
	GW::MATH2D::GMATRIX3F mF2;

	GW::MATH2D::GMATRIX3D mD1 = { 5.0, -1.0, 2.0, 4.0 };
	GW::MATH2D::GMATRIX3D mD2;

	// both of these tests use G_COMPARISION_STANDARD_F for the following reason:
	// since we are converting between two types, we can only reliably test with 
	// the accuracy of the least accurate type. ex: testing float precision using 
	// double precision will cause false positive errors. 
	SECTION("Cast float matrix3 to double matrix3.", "[Upgrade3]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Upgrade3(mF1, mD2)));
		CHECK(G2D_COMPARISON_STANDARD_F(mD2.data[0], 5.0));
		CHECK(G2D_COMPARISON_STANDARD_F(mD2.data[1], -1.0));
		CHECK(G2D_COMPARISON_STANDARD_F(mD2.data[2], 2.0));
		CHECK(G2D_COMPARISON_STANDARD_F(mD2.data[3], 4.0));
	}
	SECTION("Cast double matrix3 to float matrix3.", "[Downgrade3]")
	{
		CHECK(+(GW::MATH2D::GMatrix2D::Downgrade3(mD1, mF2)));
		CHECK(G2D_COMPARISON_STANDARD_F(mF2.data[0], 5.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(mF2.data[1], -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(mF2.data[2], 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(mF2.data[3], 4.0f));
	}
}
#endif /* defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GMATRIX) */