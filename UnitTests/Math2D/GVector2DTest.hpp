#if defined(GATEWARE_ENABLE_MATH2D) && !defined(GATEWARE_DISABLE_GVECTOR2D)
TEST_CASE("Create GVector2D.", "[CreateGVector2D], [Math2D]")
{
	GW::MATH2D::GVector2D vClass;

	REQUIRE(+vClass.Create());
}

TEST_CASE("Add two vector2s.", "[Add2F], [Add2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF1 = { 1, 2 };
	GW::MATH2D::GVECTOR2F vF2 = { 0.1f, 0.2f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GVECTOR2D vD1 = { 1, 2 };
	GW::MATH2D::GVECTOR2D vD2 = { 0.1, 0.2 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float vector2 addition.", "[Add2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Add2F(vF1, vF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 1.1f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 2.2f));
	}
	SECTION("Double vector2 addition.", "[Add2D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Add2D(vD1, vD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 1.1));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 2.2));
	}
}

TEST_CASE("Add two vector3s.", "[Add3F], [Add3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF1 = { 1.0f, 2.0f, 3.0f };
	GW::MATH2D::GVECTOR3F vF2 = { 0.1f, 0.2f, 0.3f };
	GW::MATH2D::GVECTOR3F resultF;

	GW::MATH2D::GVECTOR3D vD1 = { 1.0, 2.0, 3.0 };
	GW::MATH2D::GVECTOR3D vD2 = { 0.1, 0.2, 0.3 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float vector3 addition.", "[Add3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Add3F(vF1, vF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 1.1f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 2.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.z, 3.3f));
	}
	SECTION("Double vector3 addition.", "[Add3D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Add3D(vD1, vD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 1.1));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 2.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.z, 3.3));
	}
}

TEST_CASE("Subtract two vector2s.", "[Subtract2F], [Subtract2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF1 = { 1, 2 };
	GW::MATH2D::GVECTOR2F vF2 = { 0.1f, 0.2f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GVECTOR2D vD1 = { 1, 2 };
	GW::MATH2D::GVECTOR2D vD2 = { 0.1, 0.2 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float vector2 subtraction.", "[Subtract2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Subtract2F(vF1, vF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 0.9f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 1.8f));
	}
	SECTION("Double vector2 subtraction.", "[Subtract2D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Subtract2D(vD1, vD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 0.9));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 1.8));
	}
}

TEST_CASE("Subtract two vector3s.", "[Subtract3F], [Subtract3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF1 = { 1, 2, 3 };
	GW::MATH2D::GVECTOR3F vF2 = { 0.1f, 0.2f, 0.3f };
	GW::MATH2D::GVECTOR3F resultF;

	GW::MATH2D::GVECTOR3D vD1 = { 1, 2, 3 };
	GW::MATH2D::GVECTOR3D vD2 = { 0.1, 0.2, 0.3 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float vector3 subtraction.", "[Subtract3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Subtract3F(vF1, vF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 0.9f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 1.8f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.z, 2.7f));
	}
	SECTION("Double vector3 subtraction.", "[Subtract3D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Subtract3D(vD1, vD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 0.9));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 1.8));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.z, 2.7));
	}
}

TEST_CASE("Scale a vector2.", "[Scale2F], [Scale2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF = { 0.1f, 0.2f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GVECTOR2D vD = { 0.1, 0.2 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float vector2 scaling.", "[Scale2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Scale2F(vF, 0.3f, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 0.03f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 0.06f));
	}
	SECTION("Double vector2 scaling.", "[Scale2D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Scale2D(vD, 0.3, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 0.03));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 0.06));
	}
}

TEST_CASE("Scale a vector3.", "[Scale3F], [Scale3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF = { 0.1f, 0.2f, 0.3f };
	GW::MATH2D::GVECTOR3F resultF;

	GW::MATH2D::GVECTOR3D vD = { 0.1, 0.2, 0.3 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float vector3 scaling.", "[Scale3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Scale3F(vF, 0.3f, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 0.03f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 0.06f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.z, 0.09f));
	}
	SECTION("Double vector3 scaling.", "[Scale3D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Scale3D(vD, 0.3, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 0.03));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 0.06));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.z, 0.09));
	}
}

TEST_CASE("Calculate the dot product of two vector2s.", "[Dot2F], [Dot2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF1 = { 1, 2 };
	GW::MATH2D::GVECTOR2F vF2 = { 0.1f, 0.2f };
	float resultF;

	GW::MATH2D::GVECTOR2D vD1 = { 1, 2 };
	GW::MATH2D::GVECTOR2D vD2 = { 0.1, 0.2 };
	double resultD;

	SECTION("Float vector2s.", "[DotF]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Dot2F(vF1, vF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 0.5f));
	}
	SECTION("Double vector2s.", "[DotD]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Dot2D(vD1, vD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 0.5));
	}
}

TEST_CASE("Calculate the dot product of two vector3s.", "[Dot3F], [Dot3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF1 = { 1, 2, 3 };
	GW::MATH2D::GVECTOR3F vF2 = { 0.1f, 0.2f, 0.3f };
	float resultF;

	GW::MATH2D::GVECTOR3D vD1 = { 1, 2, 3 };
	GW::MATH2D::GVECTOR3D vD2 = { 0.1, 0.2, 0.3 };
	double resultD;

	SECTION("Float vector3s.", "[Dot3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Dot3F(vF1, vF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 1.4f));
	}
	SECTION("Double vector3s.", "[Dot3D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Dot3D(vD1, vD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 1.4));
	}
}

TEST_CASE("Calculate the cross product of two vector2s.", "[Cross2F], [Cross2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF1 = { 0.835115f, 0.790102f };
	GW::MATH2D::GVECTOR2F vF2 = { 0.623082f, 0.029450f };
	float resultF;

	GW::MATH2D::GVECTOR2D vD1 = { 0.835115, 0.790102 };
	GW::MATH2D::GVECTOR2D vD2 = { 0.623082, 0.029450 };
	double resultD;

	SECTION("Float vector2s.", "[Cross2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Cross2F(vF1, vF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, -0.467704f));
	}
	SECTION("Double vector2s.", "[Cross2D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Cross2D(vD1, vD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, -0.467704197614));
	}
}

TEST_CASE("Calculate the cross product of two vector3s.", "[Cross3F], [Cross3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF1 = { 5.2f, 3.4f, 2.0f };
	GW::MATH2D::GVECTOR3F vF2 = { 0.2f, 1.0f, 2.5f };
	GW::MATH2D::GVECTOR3F resultF;

	GW::MATH2D::GVECTOR3D vD1 = { 5.2, 3.4, 2.0 };
	GW::MATH2D::GVECTOR3D vD2 = { 0.2, 1.0, 2.5 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float vector.", "[CrossVector3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Cross3F(vF1, vF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 6.5f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, -12.6f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.z, 4.52f));
	}
	SECTION( "Double vector.", "[CrossVector3D]" )
	{
		CHECK(+(GW::I::GVector2DImplementation::Cross3D(vD1, vD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 6.5));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, -12.6));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.z, 4.52));
	}
}

TEST_CASE("Multiply a vector2 by a matrix2.", "[VectorXMatrix2F], [VectorXMatrix2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF = { 5.2f, 3.4f };
	GW::MATH2D::GMATRIX2F mF = { 0.2f, 1.0f,
								0.7f, 5.9f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GVECTOR2D vD = { 5.2, 3.4 };
	GW::MATH2D::GMATRIX2D mD = { 0.2, 1.0,
								0.7, 5.9 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float vector2/matrix2.", "[VectorXMatrix2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::VectorXMatrix2F(vF, mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 3.42f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 25.26f));
	}
	SECTION("Double vector2/matrix2.", "[VectorXMatrix2D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::VectorXMatrix2D(vD, mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 3.42));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 25.26));
	}
}

TEST_CASE("Multiply a vector3 by a matrix3.", "[VectorXMatrix3F], [VectorXMatrix3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF = { 5.2f, 3.4f, 2.0f };
	GW::MATH2D::GMATRIX3F mF = { 0.2f, 1.0f, 2.3f,
								0.7f, 5.9f, 1.6f,
								-5.2f, 2.1f, 1.2f };
	GW::MATH2D::GVECTOR3F resultF;

	GW::MATH2D::GVECTOR3D vD = { 5.2, 3.4, 2.0 };
	GW::MATH2D::GMATRIX3D mD = { 0.2, 1.0, 2.3,
								0.7, 5.9, 1.6,
								-5.2, 2.1, 1.2 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float vector3/matrix3.", "[VectorXMatrix3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::VectorXMatrix3F(vF, mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, -6.98f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 29.46f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.z, 19.8f));
	}
	SECTION("Double vector3/matrix3.", "[VectorXMatrix3D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::VectorXMatrix3D(vD, mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, -6.98));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 29.46));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.z, 19.8));
	}
}

TEST_CASE("Transform a vector2 by matrix2.", "[Transform2F], [Transform2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF = { 5.2f, 3.4f };
	GW::MATH2D::GMATRIX2F mF = { 1.0f, 0.0f,
								16.0f, 32.0f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GVECTOR2D vD = { 5.2, 3.4 };
	GW::MATH2D::GMATRIX2D mD = { 1.0, 0.0,
								16.0, 32.0 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float vector2/matrix2.", "[Transform2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Transform2F(vF, mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 59.6f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 108.8f));
	}
	SECTION("Double vector2/matrix2.", "[Transform2D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Transform2D(vD, mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 59.6));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 108.8));
	}
}

TEST_CASE("Transform a vector3 by matrix3.", "[Transform3F], [Transform3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF = { 5.2f, 3.4f, 2.1f };
	GW::MATH2D::GMATRIX3F mF = { 1.0f, 0.0f, 0.0f,
								0.0f, 1.0f, 0.0f,
								16.0f, 32.0f, 64.0f };
	GW::MATH2D::GVECTOR3F resultF;

	GW::MATH2D::GVECTOR3D vD = { 5.2, 3.4, 2.1 };
	GW::MATH2D::GMATRIX3D mD = { 1.0, 0.0, 0.0,
								0.0, 1.0, 0.0,
								16.0, 32.0, 64.0 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float vector3/matrix3.", "[Transform3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Transform3F(vF, mF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 38.8f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 70.6f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.z, 134.4f));
	}
	SECTION("Double vector3/matrix3.", "[Transform3D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Transform3D(vD, mD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 38.8));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 70.6));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.z, 134.4));
	}
}

TEST_CASE("Calculate the magnitude of a vector2.", "[Magnitude2F], [Magnitude2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F v0F = { 0.0f, 0.0f };
	GW::MATH2D::GVECTOR2F vF = { 3.0f, 1.0f };
	float resultF;

	GW::MATH2D::GVECTOR2D v0D = { 0.0, 0.0 };
	GW::MATH2D::GVECTOR2D vD = { 3.0, 1.0 };
	double resultD;

	SECTION("Float vector2.", "[Magnitude2F]")
	{
		//Fail cases
		CHECK(-(GW::I::GVector2DImplementation::Magnitude2F(v0F, resultF)));

		//Pass cases
		CHECK(+(GW::I::GVector2DImplementation::Magnitude2F(vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 3.1622776f));
	}
	SECTION("Double vector2.", "[Magnitude2D]")
	{
		//Fail cases
		CHECK(-(GW::I::GVector2DImplementation::Magnitude2D(v0D, resultD)));

		//Pass cases
		CHECK(+(GW::I::GVector2DImplementation::Magnitude2D(vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 3.1622776601683795));
	}
}

TEST_CASE("Calculate the magnitude of a vector3.", "[Magnitude3F], [Magnitude3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F v0F = { 0.0f, 0.0f, 0.0f };
	GW::MATH2D::GVECTOR3F vF = { 1.0f, 2.0f, 3.0f };
	float resultF;

	GW::MATH2D::GVECTOR3D v0D = { 0.0, 0.0, 0.0 };
	GW::MATH2D::GVECTOR3D vD = { 1.0, 2.0, 3.0 };
	double resultD;

	SECTION("Float vector3.", "[Magnitude3F]")
	{
		//Fail cases
		CHECK(-(GW::I::GVector2DImplementation::Magnitude3F(v0F, resultF)));

		//Pass cases
		CHECK(+(GW::I::GVector2DImplementation::Magnitude3F(vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 3.7416573f));
	}
	SECTION("Double vector3.", "[Magnitude3D]")
	{
		//Fail cases
		CHECK(-(GW::I::GVector2DImplementation::Magnitude3D(v0D, resultD)));

		//Pass cases
		CHECK(+(GW::I::GVector2DImplementation::Magnitude3D(vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 3.7416573867739413));
	}
}

TEST_CASE("Normalize a vector2.", "[Normalize2F], [Normalize2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F v0F = { 0.0f, 0.0f };
	GW::MATH2D::GVECTOR2F vF = { 5.2f, 3.4f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GVECTOR2D v0D = { 0.0, 0.0 };
	GW::MATH2D::GVECTOR2D vD = { 5.2, 3.4 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float vector2.", "[Normalize2F]")
	{
		//Fail cases
		CHECK(-(GW::I::GVector2DImplementation::Normalize2F(v0F, resultF)));

		//Pass cases
		CHECK(+(GW::I::GVector2DImplementation::Normalize2F(vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 0.8369696f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 0.5472493f));
	}
	SECTION("Double vector2.", "[Normalize2D]")
	{
		//Fail cases
		CHECK(-(GW::I::GVector2DImplementation::Normalize2D(v0D, resultD)));

		//Pass cases
		CHECK(+(GW::I::GVector2DImplementation::Normalize2D(vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 0.83696961397354563));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 0.54724936298270288));
	}
}

TEST_CASE("Normalize a vector3.", "[Normalize3F], [Normalize3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F v0F = { 0.0f, 0.0f, 0.0f };
	GW::MATH2D::GVECTOR3F vF = { 5.2f, 3.4f, 2.2f };
	GW::MATH2D::GVECTOR3F resultF;

	GW::MATH2D::GVECTOR3D v0D = { 0.0, 0.0, 0.0 };
	GW::MATH2D::GVECTOR3D vD = { 5.2, 3.4, 2.2 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float vector3.", "[Normalize3F]")
	{
		//Fail cases
		CHECK(-(GW::I::GVector2DImplementation::Normalize3F(v0F, resultF)));

		//Pass cases
		CHECK(+(GW::I::GVector2DImplementation::Normalize3F(vF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 0.7889662f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 0.5158625f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.z, 0.3337934f));
	}
	SECTION("Double vector3.", "[Normalize3D]")
	{
		//Fail cases
		CHECK(-(GW::I::GVector2DImplementation::Normalize3D(v0D, resultD)));

		//Pass cases
		CHECK(+(GW::I::GVector2DImplementation::Normalize3D(vD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 0.78896626774472578));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 0.51586255967924377));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.z, 0.33379342096892245));
	}
}

TEST_CASE("Lerp between two floats.", "[LerpF], [LerpD], [Math2D]")
{
	float f1 = 5.2f;
	float f2 = 0.2f;
	float resultF;

	double d1 = 5.2;
	double d2 = 0.2;
	double resultD;

	SECTION("Float.", "[LerpF]")
	{
		CHECK(+(GW::I::GVector2DImplementation::LerpF(f1, f2, 0.5f, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 2.7f));
	}
	SECTION("Double", "[LerpD]")
	{
		CHECK(+(GW::I::GVector2DImplementation::LerpD(d1, d2, 0.5, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 2.7));
	}
}

TEST_CASE("Lerp between two vector2s.", "[Lerp2F], [Lerp2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF1 = { 5.2f, 3.4f };
	GW::MATH2D::GVECTOR2F vF2 = { 0.2f, 1.0f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GVECTOR2D vD1 = { 5.2, 3.4 };
	GW::MATH2D::GVECTOR2D vD2 = { 0.2, 1.0 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float vector2.", "[Lerp2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Lerp2F(vF1, vF2, 0.5f, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 2.7f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 2.2f));
	}
	SECTION("Double vector2.", "[Lerp2D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Lerp2D(vD1, vD2, 0.5, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 2.7));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 2.2));
	}
}

TEST_CASE("Lerp between two vector3s.", "[Lerp3F], [Lerp3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF1 = { 5.2f, 3.4f, 2.2f };
	GW::MATH2D::GVECTOR3F vF2 = { 0.2f, 1.0f, 0.0f };
	GW::MATH2D::GVECTOR3F resultF;

	GW::MATH2D::GVECTOR3D vD1 = { 5.2, 3.4, 2.2 };
	GW::MATH2D::GVECTOR3D vD2 = { 0.2, 1.0, 0.0 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float vector3.", "[Lerp3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Lerp3F(vF1, vF2, 0.5f, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 2.7f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 2.2f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.z, 1.1f));
	}
	SECTION("Double vector3.", "[Lerp3D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Lerp3D(vD1, vD2, 0.5, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 2.7));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 2.2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.z, 1.1));
	}
}

TEST_CASE("Berp between three floats.", "[BerpF], [BerpD], [Math2D]")
{
	float f1 = 5.2f;
	float f2 = 3.4f;
	float f3 = 2.2f;
	GW::MATH2D::GBARYCENTRICF baryF = { 0.5f, 0.2f, 0.3f };
	float resultF;

	double d1 = 5.2;
	double d2 = 3.4;
	double d3 = 2.2;
	GW::MATH2D::GBARYCENTRICD baryD = { 0.5, 0.2, 0.3 };
	double resultD;

	SECTION("Float.", "[BerpF]")
	{
		CHECK(+(GW::I::GVector2DImplementation::BerpF(f1, f2, f3, baryF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 3.94f));
	}
	SECTION("Double", "[BerpD]")
	{
		CHECK(+(GW::I::GVector2DImplementation::BerpD(d1, d2, d3, baryD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 3.94));
	}
}

TEST_CASE("Berp between three vector2s.", "[Berp2F], [Berp2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF1 = { 5.2f, -1.5f };
	GW::MATH2D::GVECTOR2F vF2 = { 3.4f, 2.8f };
	GW::MATH2D::GVECTOR2F vF3 = { 2.2f, -3.7f };
	GW::MATH2D::GBARYCENTRICF baryF = { 0.5f, 0.2f, 0.3f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GVECTOR2D vD1 = { 5.2, -1.5 };
	GW::MATH2D::GVECTOR2D vD2 = { 3.4, 2.8 };
	GW::MATH2D::GVECTOR2D vD3 = { 2.2, -3.7 };
	GW::MATH2D::GBARYCENTRICD baryD = { 0.5, 0.2, 0.3 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("Float vector2.", "[Berp2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Berp2F(vF1, vF2, vF3, baryF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 3.94f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, -1.3f));
	}
	SECTION("Double vector2", "[Berp2D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Berp2D(vD1, vD2, vD3, baryD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 3.94));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, -1.3));
	}
}

TEST_CASE("Berp between three vector3s.", "[Berp3F], [Berp3D], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF1 = { 5.2f, -1.5f, 10.0f };
	GW::MATH2D::GVECTOR3F vF2 = { 3.4f, 2.8f, 22.5f };
	GW::MATH2D::GVECTOR3F vF3 = { 2.2f, -3.7f, 99.3f };
	GW::MATH2D::GBARYCENTRICF baryF = { 0.5f, 0.2f, 0.3f };
	GW::MATH2D::GVECTOR3F resultF;

	GW::MATH2D::GVECTOR3D vD1 = { 5.2, -1.5, 10.0 };
	GW::MATH2D::GVECTOR3D vD2 = { 3.4, 2.8, 22.5 };
	GW::MATH2D::GVECTOR3D vD3 = { 2.2, -3.7, 99.3 };
	GW::MATH2D::GBARYCENTRICD baryD = { 0.5, 0.2, 0.3 };
	GW::MATH2D::GVECTOR3D resultD;

	SECTION("Float vector3.", "[Berp3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Berp3F(vF1, vF2, vF3, baryF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 3.94f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, -1.3f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.z, 39.29f));
	}
	SECTION("Double vector3", "[Berp3D]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Berp3D(vD1, vD2, vD3, baryD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 3.94));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, -1.3));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.z, 39.29));
	}
}

TEST_CASE("Spline between four vector2s.", "[Spline2F], [Slerp2D], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF1 = { 0,0 };
	GW::MATH2D::GVECTOR2F vF2 = { 1,1 };
	GW::MATH2D::GVECTOR2F vF3 = { 3,1 };
	GW::MATH2D::GVECTOR2F vF4 = { 4,0 };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GVECTOR2D vD1 = { 0,0 };
	GW::MATH2D::GVECTOR2D vD2 = { 1,1 };
	GW::MATH2D::GVECTOR2D vD3 = { 3,1 };
	GW::MATH2D::GVECTOR2D vD4 = { 4,0 };
	GW::MATH2D::GVECTOR2D resultD;

	float ratioF = 0.5f;
	double ratioD = 0.5;

	SECTION("Float vector2.", "[Spline2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Spline2F(vF1, vF2, vF3, vF4, ratioF, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 2.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 1.1614983f));
	}
	SECTION( "Double vector2.", "[Spline2D]" )
	{
		CHECK(+(GW::I::GVector2DImplementation::Spline2D(vD1, vD2, vD3, vD4, ratioD, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 2));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 1.1614983745349439));
	}
}

TEST_CASE("Calculate gradient of a line.", "[GradientF], [GradientD], [Math2D]")
{
	GW::MATH2D::GLINE2F lF1 = { 0.0f, 0.0f, 10.0f, 0.0f };
	GW::MATH2D::GVECTOR2F resultF;

	GW::MATH2D::GLINE2D lD1 = { 0.0, 0.0, 10.0, 0.0 };
	GW::MATH2D::GVECTOR2D resultD;

	SECTION("float", "[GradientF]")
	{
		CHECK(+(GW::I::GVector2DImplementation::GradientF(lF1, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.x, 0.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF.y, 10.0f));
	}
	SECTION("double", "[GradientD]")
	{
		CHECK(+(GW::I::GVector2DImplementation::GradientD(lD1, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.x, 0.0));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD.y, 10.0));
	}
}

TEST_CASE("Calculate angle between two vectors.", "[AngleBetweenVectorsF], [AngleBetweenVectorsD], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF1 = { 10.0f, 0.0f };
	GW::MATH2D::GVECTOR2F vF2 = { 0.0f, 10.0f };
	float resultF;

	GW::MATH2D::GVECTOR2D vD1 = { 10.0, 0.0 };
	GW::MATH2D::GVECTOR2D vD2 = { 0.0, 10.0 };
	double resultD;

	SECTION("float", "[AngleBetweenVectorsF]")
	{
		CHECK(+(GW::I::GVector2DImplementation::AngleBetweenVectorsF(vF1, vF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 1.5707963f));
	}
	SECTION("double", "[AngleBetweenVectorsD]")
	{
		CHECK(+(GW::I::GVector2DImplementation::AngleBetweenVectorsD(vD1, vD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 1.5707963267948966));
	}
}

TEST_CASE("Calculate angle between two lines.", "[AngleBetweenLinesF], [AngleBetweenLinesD], [Math2D]")
{
	GW::MATH2D::GLINE2F lF1 = { 0.0f, 0.0f, 10.0f, 0.0f };
	GW::MATH2D::GLINE2F lF2 = { 0.0f, 0.0f, 0.0f, 10.0f };
	float resultF;

	GW::MATH2D::GLINE2D lD1 = { 0.0, 0.0, 10.0, 0.0 };
	GW::MATH2D::GLINE2D lD2 = { 0.0, 0.0, 0.0, 10.0 };
	double resultD;

	SECTION("float", "[AngleBetweenLinesF]")
	{
		CHECK(+(GW::I::GVector2DImplementation::AngleBetweenLinesF(lF1, lF2, resultF)));
		CHECK(G2D_COMPARISON_STANDARD_F(resultF, 1.5707963f));
	}
	SECTION("double", "[AngleBetweenLinesD]")
	{
		CHECK(+(GW::I::GVector2DImplementation::AngleBetweenLinesD(lD1, lD2, resultD)));
		CHECK(G2D_COMPARISON_STANDARD_D(resultD, 1.5707963267948966));
	}
}

TEST_CASE("Cast vector2s between float and double.", "[Upgrade2F], [Downgrade2F], [Math2D]")
{
	GW::MATH2D::GVECTOR2F vF1 = { 5.0f, -1.0f };
	GW::MATH2D::GVECTOR2F vF2;

	GW::MATH2D::GVECTOR2D vD1 = { 5.0, -1.0 };
	GW::MATH2D::GVECTOR2D vD2;


	// both of these tests use G_COMPARISION_STANDARD_F for the following reason:
	// since we are converting between two types, we can only reliably test with
	// the accuracy of the least accurate type. ex: testing float precision using
	// double precision will cause false positive errors.
	SECTION("float to double", "[Upgrade2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Upgrade2(vF1, vD2)));
		CHECK(G2D_COMPARISON_STANDARD_F(vD2.x, 5.0));
		CHECK(G2D_COMPARISON_STANDARD_F(vD2.y, -1.0));
	}
	SECTION("double to float", "[Downgrade2F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Downgrade2(vD1, vF2)));
		CHECK(G2D_COMPARISON_STANDARD_F(vF2.x, 5.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(vF2.y, -1.0f));
	}
}

TEST_CASE("Cast vector3s between float and double.", "[Upgrade3F], [Downgrade3F], [Math2D]")
{
	GW::MATH2D::GVECTOR3F vF1 = { 5.0f, -1.0f, 99.0f };
	GW::MATH2D::GVECTOR3F vF2;

	GW::MATH2D::GVECTOR3D vD1 = { 5.0, -1.0, 99.0 };
	GW::MATH2D::GVECTOR3D vD2;


	// both of these tests use G_COMPARISION_STANDARD_F for the following reason:
	// since we are converting between two types, we can only reliably test with
	// the accuracy of the least accurate type. ex: testing float precision using
	// double precision will cause false positive errors.
	SECTION("float to double", "[Upgrade3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Upgrade3(vF1, vD2)));
		CHECK(G2D_COMPARISON_STANDARD_F(vD2.x, 5.0));
		CHECK(G2D_COMPARISON_STANDARD_F(vD2.y, -1.0));
		CHECK(G2D_COMPARISON_STANDARD_F(vD2.z, 99.0));
	}
	SECTION("double to float", "[Downgrade3F]")
	{
		CHECK(+(GW::I::GVector2DImplementation::Downgrade3(vD1, vF2)));
		CHECK(G2D_COMPARISON_STANDARD_F(vF2.x, 5.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(vF2.y, -1.0f));
		CHECK(G2D_COMPARISON_STANDARD_F(vF2.z, 99.0f));
	}
}

#endif /* defined(GATEWARE_ENABLE_MATH) && !defined(GATEWARE_DISABLE_GVECTOR) */