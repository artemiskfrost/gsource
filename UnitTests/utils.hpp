// File contains any shared functions & macros used to facilitate unit testing
#ifndef UTILS_HPP
#define UTILS_HPP

#include <chrono>
#include <thread>

namespace UTILS {

	// Unlike a normal std::sleep_for
	// this routine attempts to hit exactly the time requested
	// it uses a spin lock when the OS thread scheduler is not precise enough
	// useful when you need to wait a more exact length of time to test something
	// We assume the OS scheduler has a resolution no worse than 20ms, after that we spin lock 
	inline bool Sleep_Exact(long long milliseconds, long long os_resolution = 20) {

		auto start = std::chrono::steady_clock::now(); // time at call

		if (milliseconds <= 0 || os_resolution < 0) return false; // sanity check

		if (milliseconds > os_resolution) // sleep until os is inaccurate
			std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds - os_resolution));

		while (std::chrono::duration_cast<std::chrono::milliseconds>( // spin until exact millisecond
			std::chrono::steady_clock::now() - start).count() <= milliseconds) {}

		return true; // should be on the millisecond requested (assuming conservative os_resolution) 
	}

}; // end namespace

#endif // end include guard