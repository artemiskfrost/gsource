#if defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTGENERATOR)

#include "GEventCommonIncludes.h"

TEST_CASE("GEventGenerator core method test battery", "[Core]")
{
	GW::CORE::GEventGenerator eventGenerator;

	GW::CORE::GEventCache emptyCache;
	GW::CORE::GEventResponder emptyResponder;
	GW::CORE::GInterface emptyInterface;
	unsigned int listenerCount = 0;

	SECTION("Testing empty proxy method calls")
	{
		REQUIRE(eventGenerator.Observers(listenerCount) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventGenerator.Push(GW::GEvent()) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventGenerator.Register(emptyInterface, nullptr) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventGenerator.Register(emptyCache) == GW::GReturn::EMPTY_PROXY);
		REQUIRE(eventGenerator.Register(emptyResponder) == GW::GReturn::EMPTY_PROXY);
	}

	SECTION("Testing creation/destruction method calls")
	{
		REQUIRE(eventGenerator.Create() == GW::GReturn::SUCCESS);
		eventGenerator = nullptr; // Destruction
		REQUIRE(!eventGenerator); // Invalid check
	}

	SECTION("Testing valid proxy method calls")
	{
		REQUIRE(eventGenerator.Create() == GW::GReturn::SUCCESS);

		REQUIRE(eventGenerator.Observers(listenerCount) == GW::GReturn::SUCCESS);
		REQUIRE(listenerCount == 0); // It should be 0
		REQUIRE(eventGenerator.Push(GW::GEvent()) == GW::GReturn::REDUNDANT); // Should return REDUNDANT because no listeners are registered
		REQUIRE(emptyInterface.Create() == GW::GReturn::SUCCESS);
		REQUIRE(emptyCache.Create(1) == GW::GReturn::SUCCESS);
		REQUIRE(emptyResponder.Create([](const GW::GEvent&) {}) == GW::GReturn::SUCCESS);
		REQUIRE(eventGenerator.Register(emptyInterface, [](const GW::GEvent& event, GW::CORE::GInterface& observer) { return; }) == GW::GReturn::SUCCESS);
		REQUIRE(eventGenerator.Register(emptyCache) == GW::GReturn::SUCCESS);
		REQUIRE(eventGenerator.Register(emptyResponder) == GW::GReturn::SUCCESS);
		REQUIRE(eventGenerator.Observers(listenerCount) == GW::GReturn::SUCCESS);
		REQUIRE(listenerCount == 3); // It should be 3 after register methods call
		REQUIRE(eventGenerator.Push(GW::GEvent()) == GW::GReturn::SUCCESS); // Should return SUCCESS because 1 listeners are registered
	}
	ResetGlobals();
} // End of test case GEventGenerator core method test battery
#endif /* defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTGENERATOR) */