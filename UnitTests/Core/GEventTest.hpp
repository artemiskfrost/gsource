#if defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTGENERATOR) && !defined(GATEWARE_DISABLE_GEVENTRECEIVER) && !defined(GATEWARE_DISABLE_GEVENTQUEUE)

#include "GEventCommonIncludes.h"

TEST_CASE("GEvent core method test battery", "[Core]")
{
	SECTION("Testing core methods")
	{
		SECTION("Testing API::DirectX and POD")
		{
			REQUIRE(g_outEvent.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outEnum) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outData) == GW::GReturn::SUCCESS);
			REQUIRE(API::DirectX == g_outEnum);
			REQUIRE((g_pods[0] == g_outData));
			// Testing overloaded function
			REQUIRE(g_outEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
			REQUIRE(API::DirectX == g_outEnum);
			REQUIRE((g_pods[0] == g_outData));
		}

		SECTION("Testing API::Vulkan and POD")
		{
			REQUIRE(g_outEvent.Write(API::Vulkan, g_pods[1]) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outEnum) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outData) == GW::GReturn::SUCCESS);
			REQUIRE((API::Vulkan == g_outEnum));
			REQUIRE((g_pods[1] == g_outData));
			// Testing overloaded function
			REQUIRE(g_outEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
			REQUIRE(API::Vulkan == g_outEnum);
			REQUIRE((g_pods[1] == g_outData));
		}

		SECTION("Testing API::OpenGL and POD")
		{
			REQUIRE(g_outEvent.Write(API::OpenGL, g_pods[2]) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outEnum) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outData) == GW::GReturn::SUCCESS);
			REQUIRE(API::OpenGL == g_outEnum);
			REQUIRE((g_pods[2] == g_outData));
			// Testing overloaded function
			REQUIRE(g_outEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
			REQUIRE(API::OpenGL == g_outEnum);
			REQUIRE((g_pods[2] == g_outData));
		}

		SECTION("Testing API::Metal and POD")
		{
			REQUIRE(g_outEvent.Write(API::Metal, g_pods[3]) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outEnum) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outData) == GW::GReturn::SUCCESS);
			REQUIRE(API::Metal == g_outEnum);
			REQUIRE((g_pods[3] == g_outData));
			// Testing overloaded function
			REQUIRE(g_outEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
			REQUIRE(API::Metal == g_outEnum);
			REQUIRE((g_pods[3] == g_outData));
		}

		SECTION("Testing API::Mantle and POD")
		{
			REQUIRE(g_outEvent.Write(API::Mantle, g_pods[4]) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outEnum) == GW::GReturn::SUCCESS);
			REQUIRE(g_outEvent.Read(g_outData) == GW::GReturn::SUCCESS);
			REQUIRE(API::Mantle == g_outEnum);
			REQUIRE((g_pods[4] == g_outData));
			// Testing overloaded function
			REQUIRE(g_outEvent.Read(g_outEnum, g_outData) == GW::GReturn::SUCCESS);
			REQUIRE(API::Mantle == g_outEnum);
			REQUIRE((g_pods[4] == g_outData));
		}
	}
	ResetGlobals();
} // End of test case GEvent core method test battery

TEST_CASE("GEvent tests using GEventGenerator and multiple GEventReceiver with custom call back functions", "[Core]")
{
	GW::GEvent apiEvent;
	GW::CORE::GEventGenerator EventGenerator;

	// Receives all api events (Callback pops events)
	GW::CORE::GEventReceiver allAPIEventReceiver;

	// Receives api specifics events (Callback only peeks events, the newest event that has been pushed will be the event that is being waited)
	GW::CORE::GEventReceiver dxEventReceiver;
	GW::CORE::GEventReceiver vkEventReceiver;
	GW::CORE::GEventReceiver glEventReceiver;
	GW::CORE::GEventReceiver meEventReceiver;
	GW::CORE::GEventReceiver mlEventReceiver;

	// Call counts for each event receiver
	unsigned int dxCalls = 0;
	unsigned int vkCalls = 0;
	unsigned int glCalls = 0;
	unsigned int meCalls = 0;
	unsigned int mlCalls = 0;

	// Create an event generator that will be used to generate events
	// and sends them out to all listeners
	REQUIRE(EventGenerator.Create() == GW::GReturn::SUCCESS);

	SECTION("Broadcasting an event to no registered listeners should result in GW::GReturn::REDUNDANT")
	{
		// GW::GEvent::Write rewrites all internal data
		REQUIRE(+apiEvent.Write(API::DirectX, g_pods[0]));
		// No listeners has been registered or created, it is redundant call
		REQUIRE(EventGenerator.Push(apiEvent) == GW::GReturn::REDUNDANT);
	}

	REQUIRE(allAPIEventReceiver.Create(EventGenerator, [&allAPIEventReceiver, &dxCalls, &vkCalls, &glCalls, &meCalls, &mlCalls]()
	{
		if (+allAPIEventReceiver.Peek(g_outEvent))
		{
			if (+g_outEvent.Read(g_outEnum, g_outData))
			{
				if (g_outEnum == API::DirectX && g_outData == g_pods[0])
					++dxCalls;
				else if (g_outEnum == API::Vulkan && g_outData == g_pods[1])
					++vkCalls;
				else if (g_outEnum == API::OpenGL && g_outData == g_pods[2])
					++glCalls;
				else if (g_outEnum == API::Metal && g_outData == g_pods[3])
					++meCalls;
				else if (g_outEnum == API::Mantle && g_outData == g_pods[4])
					++mlCalls;
			}
		}
	}) == GW::GReturn::SUCCESS);

	REQUIRE(dxEventReceiver.Create(EventGenerator, [&dxEventReceiver, &dxCalls]()
	{
		if (+dxEventReceiver.Pop(g_outEvent))
			if (+g_outEvent.Read(g_outEnum, g_outData))
				if (g_outEnum == API::DirectX && g_outData == g_pods[0])
					++dxCalls;
	}) == GW::GReturn::SUCCESS);

	REQUIRE(vkEventReceiver.Create(EventGenerator, [&vkEventReceiver, &vkCalls]()
	{
		if (+vkEventReceiver.Pop(g_outEvent))
			if (+g_outEvent.Read(g_outEnum, g_outData))
				if (g_outEnum == API::Vulkan && g_outData == g_pods[1])
					++vkCalls;
	}) == GW::GReturn::SUCCESS);

	REQUIRE(glEventReceiver.Create(EventGenerator, [&glEventReceiver, &glCalls]()
	{
		if (+glEventReceiver.Pop(g_outEvent))
			if (+g_outEvent.Read(g_outEnum, g_outData))
				if (g_outEnum == API::OpenGL && g_outData == g_pods[2])
					++glCalls;
	}) == GW::GReturn::SUCCESS);

	REQUIRE(meEventReceiver.Create(EventGenerator, [&meEventReceiver, &meCalls]()
	{
		if (+meEventReceiver.Pop(g_outEvent))
			if (+g_outEvent.Read(g_outEnum, g_outData))
				if (g_outEnum == API::Metal && g_outData == g_pods[3])
					++meCalls;
	}) == GW::GReturn::SUCCESS);

	REQUIRE(mlEventReceiver.Create(EventGenerator, [&mlEventReceiver, &mlCalls]()
	{
		if (+mlEventReceiver.Pop(g_outEvent))
			if (+g_outEvent.Read(g_outEnum, g_outData))
				if (g_outEnum == API::Mantle && g_outData == g_pods[4])
					++mlCalls;
	}) == GW::GReturn::SUCCESS);

	// There should be 6 listener registers
	// allAPIEventReceiver, dxEventReceiver, vkEventReceiver
	// glEventReceiver, meEventReceiver, mlEventReceiver
	unsigned int listenerCount = 0;
	REQUIRE(EventGenerator.Observers(listenerCount) == GW::GReturn::SUCCESS);
	REQUIRE(listenerCount == 6);

	SECTION("Testing enum class::API::DirectX events callback")
	{
		REQUIRE(apiEvent.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent) == GW::GReturn::SUCCESS);
		REQUIRE(dxCalls == 2);
	}

	SECTION("Testing enum class::API::Vulkan events callback")
	{
		REQUIRE(apiEvent.Write(API::Vulkan, g_pods[1]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(vkCalls == 4);
	}

	SECTION("Testing enum class::API::OpenGL events callback")
	{
		REQUIRE(apiEvent.Write(API::OpenGL, g_pods[2]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(glCalls == 6);
	}

	SECTION("Testing enum class::API::Metal events callback")
	{
		REQUIRE(apiEvent.Write(API::Metal, g_pods[3]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(meCalls == 8);
	}

	SECTION("Testing enum class::API::Mantle events callback")
	{
		REQUIRE(apiEvent.Write(API::Mantle, g_pods[4]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(mlCalls == 10);
	}

	ResetGlobals();
} // End of test case GEvent tests using GEventGenerator and multiple GEventReceiver with custom call back functions
#endif /* defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTGENERATOR) && !defined(GATEWARE_DISABLE_GEVENTRECEIVER) && !defined(GATEWARE_DISABLE_GEVENTQUEUE) */