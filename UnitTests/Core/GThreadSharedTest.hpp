#if defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GTHREADSHARED)

// for resource sharing test
#include <thread>
#include <vector>
// for testing unit testing (remove when done writing tests)
#include <iostream>

TEST_CASE("GThreadShared Creation and Destruction", "[Core]")
{
	// This section will create a GThreadShared object
	// for the purpose of testing it's valid and
	// invalid returns.
	GW::CORE::GThreadShared ThreadShared_obj;

	// Initial check that ThreadShared_obj has not been created yet.
	CHECK(ThreadShared_obj.LockAsyncRead() == GW::GReturn::EMPTY_PROXY);

	SECTION("Positive testing of ThreadShared object")
	{
		REQUIRE(ThreadShared_obj.Create() == GW::GReturn::SUCCESS);

		GW::CORE::GThreadShared SecondTS_obj;

		WHEN("Weak ownership of valid proxy is shared")
		{
			SecondTS_obj = ThreadShared_obj;
			THEN("Second ThreadShare Proxy returns success")
			{
				REQUIRE(SecondTS_obj.LockAsyncRead() == GW::GReturn::SUCCESS);
				REQUIRE(SecondTS_obj.UnlockAsyncRead() == GW::GReturn::SUCCESS);
			}

			WHEN("Second object is released")
			{
				SecondTS_obj = nullptr;
				THEN("Second ThreadShare Proxy is empty and original is still valid")
				{
					REQUIRE(SecondTS_obj.LockAsyncRead() == GW::GReturn::EMPTY_PROXY);
					REQUIRE(SecondTS_obj.UnlockAsyncRead() == GW::GReturn::EMPTY_PROXY);
					REQUIRE(ThreadShared_obj.LockAsyncRead() == GW::GReturn::SUCCESS);
					REQUIRE(ThreadShared_obj.UnlockAsyncRead() == GW::GReturn::SUCCESS);
				}
			}
		}
	}

	SECTION("Negative testing of ThreadShared object")
	{
		WHEN("ThreadShared object is set to nullptr")
		{
			ThreadShared_obj = nullptr;
			THEN("ThreadShared_obj should be an empty proxy")
			{
				REQUIRE(ThreadShared_obj.LockAsyncRead() == GW::GReturn::EMPTY_PROXY);
				REQUIRE(ThreadShared_obj.UnlockAsyncRead() == GW::GReturn::EMPTY_PROXY);
				REQUIRE(ThreadShared_obj.LockSyncWrite() == GW::GReturn::EMPTY_PROXY);
				REQUIRE(ThreadShared_obj.UnlockSyncWrite() == GW::GReturn::EMPTY_PROXY);
			}
		}
	}
}

TEST_CASE("GThreadShared Resource Sharing", "[Core]")
{
	SECTION("Writing Lock Test")
	{
		auto mainThread_call = [&]()
		{
			int threadID_Count = 0;
			GW::CORE::GThreadShared ID_Lock; ID_Lock.Create();
			auto sub_thread_call = [&](std::string s) mutable
			{
				ID_Lock.LockSyncWrite();
				threadID_Count++;
				ID_Lock.UnlockSyncWrite();
			};

			WHEN("All 3 threads have run 10 times each")
			{
				int i = 0;
				while (i++ < 10)
				{
					std::thread thread_A((sub_thread_call), "thread_A");
					std::thread thread_B((sub_thread_call), "thread_B");
					std::thread thread_C((sub_thread_call), "thread_C");

					// CLEAN UP (maybe change to CATCH syntax)
					// This could be bad depending on how the internal "wait" of Joinable works.
					// if its strong as in it stops this process until true, then yay.
					// if not, then its possible to "leak" these threads.
					if (thread_A.joinable())
						thread_A.join();
					if (thread_B.joinable())
						thread_B.join();
					if (thread_C.joinable())
						thread_C.join();
				}
				THEN("threadID_Count should be 30")
				{
					REQUIRE(threadID_Count == 30);
				}
			}
			ID_Lock = nullptr;
		};

		std::thread mainThread(mainThread_call);

		REQUIRE(mainThread.joinable());
		mainThread.join();
	}

	SECTION("Reading and Writing Lock Tests")
	{
		// Thread_1(Boss) will run like "main"
		// Thread_1a(Worker) will look for even numbers and fill that vector.
		// Thread_1b(Worker) will look for odd numbers and fill that vector.
		// threads 1a&1b will replace matched numbers with 0.
		// Thread 2(QA) will be checking the array for it to be completely zero'd
		//	// which will flip a 'done' bool telling thread_1 to tell it's threads to finish
		//	// otherwise, thread 2 will set the shared iterator to the index which is not zero'd

		// shared variables
		GW::CORE::GThreadShared base_lock; base_lock.Create();
		int randNums[100];
		std::vector<int> even_matches;
		std::vector<int> odd_matches;
		bool sorting_isdone = false;
		unsigned int iterator = 0;
		bool reset_iter = false;

		auto worker_even = [&sorting_isdone, &randNums, &even_matches, &base_lock, &iterator, &reset_iter]() mutable
		{
			//std::cout << "even_funk entrance\n";
			while (true)
			{
				base_lock.LockAsyncRead();
				if (sorting_isdone)
				{
					base_lock.UnlockAsyncRead();
					break;
				}
				base_lock.UnlockAsyncRead();

				base_lock.LockSyncWrite();
				if (iterator < 100)
				{
					if (randNums[iterator] % 2 == 0 && randNums[iterator] != 0)
					{
						//std::cout << "Thread_1a at " << iterator << ": Found even: " << randNums[iterator] << std::endl;
						even_matches.push_back(randNums[iterator]);
						randNums[iterator] = 0;
						iterator++;
					}
				}
				else
					reset_iter = true;
				base_lock.UnlockSyncWrite();
			}
			//std::cout << "even_funk exit\n";
		};

		auto worker_odd = [&sorting_isdone, &randNums, &odd_matches, &base_lock, &iterator, &reset_iter]() mutable
		{
			//std::cout << "odd_funk entrance\n";
			GW::CORE::GThreadShared child_lock = base_lock;
			while (true)
			{
				child_lock.LockAsyncRead();
				if (sorting_isdone)
				{
					child_lock.UnlockAsyncRead();
					break;
				}
				child_lock.UnlockAsyncRead();

				child_lock.LockSyncWrite();
				if (iterator < 100)
				{
					if (randNums[iterator] % 2 != 0 && randNums[iterator] != 0)
					{
						//std::cout << "Thread_1b at " << iterator << ": Found odd: " << randNums[iterator] << std::endl;
						odd_matches.push_back(randNums[iterator]);
						randNums[iterator] = 0;
						iterator++;
					}
				}
				else
					reset_iter = true;
				child_lock.UnlockSyncWrite();
			}
			child_lock = nullptr;
			//std::cout << "odd_funk exit\n";
		};

		auto Manager = [&]() mutable
		{
			//std::cout << "funk entrance\n";
			// Create array of random numbers.
			// I'm locking the whole loop because i want the whole thing to run before it begins getting modified by other threads.
			// which shouldn't happen regardless but just for sanity reasons.
			base_lock.LockSyncWrite();
			for (uint8_t i = 0; i < 100; i++)
			{
				randNums[i] = rand() / (101) * (100 - 1) + 1;
			}
			base_lock.UnlockSyncWrite();

			// Perform sorting. . . //
			// worker threads.
			//std::cout << "launching thread 1a & 1b. . .\n";
			std::thread thread_1a(worker_even);
			std::thread thread_1b(worker_odd);

			while (true)
			{
				base_lock.LockAsyncRead();
				if (sorting_isdone)
				{
					base_lock.UnlockAsyncRead();
					break;
				}
				base_lock.UnlockAsyncRead();

				base_lock.LockSyncWrite();
				if (reset_iter || iterator >= 100)
				{
					iterator = 0;
					reset_iter = false;
				}
				base_lock.UnlockSyncWrite();
			}
			REQUIRE(thread_1a.joinable());
			thread_1a.join();
			REQUIRE(thread_1b.joinable());
			thread_1b.join();
			//std::cout << "funk end, 1a&1b joined\n";
		};

		auto Check_Work = [&randNums, &sorting_isdone, &base_lock, &iterator]() mutable
		{
			//std::cout << "check_funk entrance\n";
			while (true)
			{
				bool Zeroed = true;
				base_lock.LockAsyncRead();
				if (sorting_isdone)
				{
					base_lock.UnlockAsyncRead();
					break;
				}
				base_lock.UnlockAsyncRead();

				int nonZeroSpot = 0;
				for (int i = 0; i < 100; i++)
				{
					base_lock.LockAsyncRead();
					if (randNums[i] != 0)
					{
						Zeroed = false;
						nonZeroSpot = i;
						base_lock.UnlockAsyncRead();
						break;
					}
					base_lock.UnlockAsyncRead();
				}
				if (Zeroed)
				{
					base_lock.LockSyncWrite();
					sorting_isdone = true;
					base_lock.UnlockSyncWrite();
				}
				else
				{
					base_lock.LockSyncWrite();
					iterator = nonZeroSpot;
					base_lock.UnlockSyncWrite();
				}
			}
			//std::cout << "check_funk exit\n";
		};
		std::thread thread_1(Manager);
		std::thread thread_2(Check_Work);
		REQUIRE(thread_2.joinable());
		thread_2.join();

		REQUIRE(thread_1.joinable());
		thread_1.join();
	}
}
/* DEBUG ONLY TESTS */
#ifndef NDEBUG
// Initial testing of safety behaviors, better test with actual threading are needed
TEST_CASE("GThreadShared Failure and Deadlock Detection", "[Core]")
{
	SECTION("Testing Deadlock Detection")
	{
		WHEN("A Sync Write Lock is initiated")
		{
			GW::CORE::GThreadShared lock;
			lock.Create();
			lock.LockSyncWrite();
			THEN("Another Write Lock Should fail and indicate DEADLOCK was avoided")
			{
				REQUIRE(lock.LockSyncWrite() == GW::GReturn::DEADLOCK);
			}
		}
		WHEN("A Sync Write Lock is initiated")
		{
			GW::CORE::GThreadShared lock;
			lock.Create();
			lock.LockSyncWrite();
			THEN("A Read Lock Should fail and indicate DEADLOCK was avoided")
			{
				REQUIRE(lock.LockAsyncRead() == GW::GReturn::DEADLOCK);
			}
		}
		WHEN("An Async Read Lock is initiated")
		{
			GW::CORE::GThreadShared read;
			read.Create();
			read.LockAsyncRead();
			THEN("Another Read Lock Should fail and indicate DEADLOCK was avoided")
			{
				REQUIRE(read.LockAsyncRead() == GW::GReturn::DEADLOCK);
			}
		}
		WHEN("An Async Read Lock is initiated")
		{
			GW::CORE::GThreadShared read;
			read.Create();
			read.LockAsyncRead();
			THEN("A Write Lock Should fail and indicate DEADLOCK was avoided")
			{
				REQUIRE(read.LockSyncWrite() == GW::GReturn::DEADLOCK);
			}
		}
	}
	SECTION("Testing Unlock Failures")
	{
		WHEN("A GThreadShared has not been locked")
		{
			GW::CORE::GThreadShared unlocked;
			unlocked.Create();
			THEN("Unlocking it should fail")
			{
				REQUIRE(unlocked.UnlockAsyncRead() == GW::GReturn::FAILURE);
				REQUIRE(unlocked.UnlockSyncWrite() == GW::GReturn::FAILURE);
			}
		}
		WHEN("An Async Read Lock is initiated")
		{
			GW::CORE::GThreadShared read;
			read.Create();
			read.LockAsyncRead();
			THEN("A Write Unlock should fail")
			{
				REQUIRE(read.UnlockSyncWrite() == GW::GReturn::FAILURE);
			}
		}
		WHEN("A Sync Write Lock is initiated")
		{
			GW::CORE::GThreadShared lock;
			lock.Create();
			lock.LockSyncWrite();
			THEN("An Async Read Unlock should fail")
			{
				REQUIRE(lock.UnlockAsyncRead() == GW::GReturn::FAILURE);
			}
		}
	}
}
// Used to ensure thread_local data does not prevent mixed behaviors on one thread
TEST_CASE("GThreadShared Single-Threaded Mixed Locks/Unlocks", "[Core]")
{
	SECTION("Testing safely and accurately mixing lock types on a single thread")
	{
		WHEN("Multiple Lock types are initiated on one thread")
		{
			GW::CORE::GThreadShared a, b, c, d;
			a.Create(); b.Create(); c.Create(); d.Create();
			a.LockSyncWrite();
			b.LockAsyncRead();
			c.LockSyncWrite();
			d.LockAsyncRead();
			THEN("They all must unlock succesfully when mixed")
			{
				REQUIRE(c.UnlockSyncWrite() == GW::GReturn::SUCCESS);
				REQUIRE(a.UnlockSyncWrite() == GW::GReturn::SUCCESS);
				REQUIRE(d.UnlockAsyncRead() == GW::GReturn::SUCCESS);
				REQUIRE(b.UnlockAsyncRead() == GW::GReturn::SUCCESS);
			}
		}
		WHEN("Multiple Lock types are initiated on one thread")
		{
			GW::CORE::GThreadShared a, b, c, d;
			a.Create(); b.Create(); c.Create(); d.Create();
			a.LockSyncWrite();
			b.LockAsyncRead();
			c.LockSyncWrite();
			d.LockAsyncRead();
			THEN("They all must succesfully detect DEADLOCK in any order")
			{
				REQUIRE(c.LockSyncWrite() == GW::GReturn::DEADLOCK);
				REQUIRE(a.LockSyncWrite() == GW::GReturn::DEADLOCK);
				REQUIRE(d.LockAsyncRead() == GW::GReturn::DEADLOCK);
				REQUIRE(b.LockAsyncRead() == GW::GReturn::DEADLOCK);
			}
		}
		// This test will be used to check if advanced async deadlock detection works (when ready)
		/*WHEN("One thousand random locks are initiated on a single-thread")
		{
			THEN("They all must succesfully lock and then unlock in any order")
			{
			}
		}*/
	}
}
#endif /* DEBUG ONLY TESTS */
#endif /* defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GTHREADSHARED) */