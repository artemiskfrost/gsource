#if defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTGENERATOR) && !defined(GATEWARE_DISABLE_GEVENTRESPONDER) && !defined(GATEWARE_DISABLE_GEVENTCACHE)

#include "GEventCommonIncludes.h"

TEST_CASE("GEvent tests using GEventGenerator and multiple GEventResponder with custom call back functions", "[Core]")
{
	GW::GEvent apiEvent;
	GW::CORE::GEventGenerator EventGenerator;

	// Receives all api events (Callback pops events)
	GW::CORE::GEventResponder allAPIEventReceiver;

	// Receives api specifics events (Callback only peeks events, the newest event that has been pushed will be the event that is being waited)
	GW::CORE::GEventResponder dxEventReceiver;
	GW::CORE::GEventResponder vkEventReceiver;
	GW::CORE::GEventResponder glEventReceiver;
	GW::CORE::GEventResponder meEventReceiver;
	GW::CORE::GEventResponder mlEventReceiver;

	// Call counts for each event receiver
	unsigned int dxCalls = 0;
	unsigned int vkCalls = 0;
	unsigned int glCalls = 0;
	unsigned int meCalls = 0;
	unsigned int mlCalls = 0;

	// Create an event generator that will be used to generate events
	// and sends them out to all listeners
	REQUIRE(EventGenerator.Create() == GW::GReturn::SUCCESS);

	SECTION("Broadcasting an event to no registered listeners should result in GW::GReturn::REDUNDANT")
	{
		// GW::GEvent::Write rewrites all internal data
		REQUIRE(+apiEvent.Write(API::DirectX, g_pods[0]));
		// No listeners has been registered or created, it is redundant call
		REQUIRE(EventGenerator.Push(apiEvent) == GW::GReturn::REDUNDANT);
	}

	REQUIRE(allAPIEventReceiver.Create([ &dxCalls, &vkCalls, &glCalls, &meCalls, &mlCalls](const GW::GEvent& g_outEvent)
	{
		if (+g_outEvent.Read(g_outEnum, g_outData))
		{
			if (g_outEnum == API::DirectX && g_outData == g_pods[0])
				++dxCalls;
			else if (g_outEnum == API::Vulkan && g_outData == g_pods[1])
				++vkCalls;
			else if (g_outEnum == API::OpenGL && g_outData == g_pods[2])
				++glCalls;
			else if (g_outEnum == API::Metal && g_outData == g_pods[3])
				++meCalls;
			else if (g_outEnum == API::Mantle && g_outData == g_pods[4])
				++mlCalls;
		}
	}) == GW::GReturn::SUCCESS);
	REQUIRE(+EventGenerator.Register(allAPIEventReceiver));

	REQUIRE(dxEventReceiver.Create([&dxCalls](const GW::GEvent& g_outEvent)
	{
		if (+g_outEvent.Read(g_outEnum, g_outData))
			if (g_outEnum == API::DirectX && g_outData == g_pods[0])
				++dxCalls;
	}) == GW::GReturn::SUCCESS);
	REQUIRE(+EventGenerator.Register(dxEventReceiver));

	REQUIRE(vkEventReceiver.Create([&vkCalls](const GW::GEvent& g_outEvent)
	{
		if (+g_outEvent.Read(g_outEnum, g_outData))
			if (g_outEnum == API::Vulkan && g_outData == g_pods[1])
				++vkCalls;
	}) == GW::GReturn::SUCCESS);
	REQUIRE(+EventGenerator.Register(vkEventReceiver));

	REQUIRE(glEventReceiver.Create([&glCalls](const GW::GEvent& g_outEvent)
	{
		if (+g_outEvent.Read(g_outEnum, g_outData))
			if (g_outEnum == API::OpenGL && g_outData == g_pods[2])
				++glCalls;
	}) == GW::GReturn::SUCCESS);
	REQUIRE(+EventGenerator.Register(glEventReceiver));

	REQUIRE(meEventReceiver.Create([&meCalls](const GW::GEvent& g_outEvent)
	{
		if (+g_outEvent.Read(g_outEnum, g_outData))
			if (g_outEnum == API::Metal && g_outData == g_pods[3])
				++meCalls;
	}) == GW::GReturn::SUCCESS);
	REQUIRE(+EventGenerator.Register(meEventReceiver));

	REQUIRE(mlEventReceiver.Create([&mlCalls](const GW::GEvent& g_outEvent)
	{
		if (+g_outEvent.Read(g_outEnum, g_outData))
			if (g_outEnum == API::Mantle && g_outData == g_pods[4])
				++mlCalls;
	}) == GW::GReturn::SUCCESS);
	REQUIRE(+EventGenerator.Register(mlEventReceiver));

	// There should be 6 listener registers
	// allAPIEventReceiver, dxEventReceiver, vkEventReceiver
	// glEventReceiver, meEventReceiver, mlEventReceiver
	unsigned int listenerCount = 0;
	REQUIRE(EventGenerator.Observers(listenerCount) == GW::GReturn::SUCCESS);
	REQUIRE(listenerCount == 6);

	SECTION("Testing enum class::API::DirectX events callback")
	{
		REQUIRE(apiEvent.Write(API::DirectX, g_pods[0]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent) == GW::GReturn::SUCCESS);
		REQUIRE(dxCalls == 2);
	}

	SECTION("Testing enum class::API::Vulkan events callback")
	{
		REQUIRE(apiEvent.Write(API::Vulkan, g_pods[1]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(vkCalls == 4);
	}

	SECTION("Testing enum class::API::OpenGL events callback")
	{
		REQUIRE(apiEvent.Write(API::OpenGL, g_pods[2]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(glCalls == 6);
	}

	SECTION("Testing enum class::API::Metal events callback")
	{
		REQUIRE(apiEvent.Write(API::Metal, g_pods[3]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(meCalls == 8);
	}

	SECTION("Testing enum class::API::Mantle events callback")
	{
		REQUIRE(apiEvent.Write(API::Mantle, g_pods[4]) == GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(EventGenerator.Push(apiEvent)== GW::GReturn::SUCCESS);
		REQUIRE(mlCalls == 10);
	}

	ResetGlobals();
} // End of test case GEvent tests using GEventGenerator and multiple GEventResponder with custom call back functions
#endif /* defined(GATEWARE_ENABLE_CORE) && !defined(GATEWARE_DISABLE_GEVENTGENERATOR) && !defined(GATEWARE_DISABLE_GEVENTRESPONDER) && !defined(GATEWARE_DISABLE_GEVENTCACHE) */