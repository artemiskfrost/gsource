#ifndef GAUDIO_RESOURCES_H // include guard
#define GAUDIO_RESOURCES_H


namespace AudioTest
{
#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP) // UWP   this must be before WIN32 as WIN32 is also defined in UWP
#define AUDIO_PATH u8"Resources/GAudio_TestSamples/"
    static const char* testSong = AUDIO_PATH u8"testSong.wav"; //1m 11s
    static const char* testBeep = AUDIO_PATH u8"testBeep.wav"; //0.5s
    static const char* testLaser = AUDIO_PATH u8"testLaser.wav"; //0.15s /// https://freesound.org/people/MusicLegends/sounds/344310/
    static const char* beep = AUDIO_PATH u8"beep.wav";                   //1s
    static const char* stereo5_1Test = AUDIO_PATH u8"stereo5.1Test.wav"; // 35s

    static const char* frontLeft = AUDIO_PATH u8"frontLeft.wav";     //1s
    static const char* frontCenter = AUDIO_PATH u8"frontCenter.wav"; //1s
    static const char* frontRight = AUDIO_PATH u8"frontRight.wav";   //1s
    static const char* surroundLeft = AUDIO_PATH u8"surroundLeft.wav";   //2s
    static const char* bass = AUDIO_PATH u8"bass.wav";                   //2s
    static const char* surroundRight = AUDIO_PATH u8"surroundRight.wav"; //2s

    static const char* masterVolume1 = AUDIO_PATH u8"masterVolumeAtOne.wav";        //2s
    static const char* masterVolume_5 = AUDIO_PATH u8"masterVolumeAtPointFive.wav"; //2s

    static const char* musicPause = AUDIO_PATH u8"musicPause.wav";     //1.8s -> 1.2s
    static const char* musicResume = AUDIO_PATH u8"musicResume.wav";   //1s
    static const char* musicPlay = AUDIO_PATH u8"musicPlay.wav";		//2s

    static const char* soundPause = AUDIO_PATH u8"soundPause.wav";   //1.8s -> 1.2s
    static const char* soundStop = AUDIO_PATH u8"soundStop.wav";     //1.8s -> 1.2s
    static const char* soundResume = AUDIO_PATH u8"soundResume.wav"; //1s
    static const char* soundPlay = AUDIO_PATH u8"soundPlay.wav";     //1s

    static const char* volume1 = AUDIO_PATH u8"volumeAtOne.wav";        //1s
    static const char* volume_5 = AUDIO_PATH u8"volumeAtPointFive.wav"; //2s

#undef AUDIO_PATH
#elif defined(WIN32) // windows
#define AUDIO_PATH u8"../../../UnitTests/Resources/GAudio_TestSamples/"

    static const char* testSong = AUDIO_PATH u8"testSong.wav"; //1m 11s
    static const char* testBeep = AUDIO_PATH u8"testBeep.wav"; //0.5s
    static const char* testLaser = AUDIO_PATH u8"testLaser.wav"; //0.15s /// https://freesound.org/people/MusicLegends/sounds/344310/
    static const char* beep = AUDIO_PATH u8"beep.wav";                   //1s
    static const char* stereo5_1Test = AUDIO_PATH u8"stereo5.1Test.wav"; // 35s

    static const char* frontLeft = AUDIO_PATH u8"frontLeft.wav";     //1s
    static const char* frontCenter = AUDIO_PATH u8"frontCenter.wav"; //1s
    static const char* frontRight = AUDIO_PATH u8"frontRight.wav";   //1s
    static const char* surroundLeft = AUDIO_PATH u8"surroundLeft.wav";   //2s
    static const char* bass = AUDIO_PATH u8"bass.wav";                   //2s
    static const char* surroundRight = AUDIO_PATH u8"surroundRight.wav"; //2s

    static const char* masterVolume1 = AUDIO_PATH u8"masterVolumeAtOne.wav";        //2s
    static const char* masterVolume_5 = AUDIO_PATH u8"masterVolumeAtPointFive.wav"; //2s

    static const char* musicPause = AUDIO_PATH u8"musicPause.wav";     //1.8s -> 1.2s
    static const char* musicResume = AUDIO_PATH u8"musicResume.wav";   //1s
    static const char* musicPlay = AUDIO_PATH u8"musicPlay.wav";		//2s

    static const char* soundPause = AUDIO_PATH u8"soundPause.wav";   //1.8s -> 1.2s
    static const char* soundStop = AUDIO_PATH u8"soundStop.wav";     //1.8s -> 1.2s
    static const char* soundResume = AUDIO_PATH u8"soundResume.wav"; //1s
    static const char* soundPlay = AUDIO_PATH u8"soundPlay.wav";     //1s

    static const char* volume1 = AUDIO_PATH u8"volumeAtOne.wav";        //1s
    static const char* volume_5 = AUDIO_PATH u8"volumeAtPointFive.wav"; //2s

#undef AUDIO_PATH
#else // linux and mac for now
#define AUDIO_PATH u8"../../../UnitTests/Resources/GAudio_TestSamples/"

    static const char* testSong = AUDIO_PATH u8"testSong.wav"; //1m 11s
    static const char* testBeep = AUDIO_PATH u8"testBeep.wav"; //0.5s
    static const char* testLaser = AUDIO_PATH u8"testLaser.wav"; //0.15s /// https://freesound.org/people/MusicLegends/sounds/344310/
    static const char* beep = AUDIO_PATH u8"beep.wav";                   //1s
    static const char* stereo5_1Test = AUDIO_PATH u8"stereo5.1Test.wav"; // 35s

    static const char* frontLeft = AUDIO_PATH u8"frontLeft.wav";     //1s
    static const char* frontCenter = AUDIO_PATH u8"frontCenter.wav"; //1s
    static const char* frontRight = AUDIO_PATH u8"frontRight.wav";   //1s
    static const char* surroundLeft = AUDIO_PATH u8"surroundLeft.wav";   //2s
    static const char* bass = AUDIO_PATH u8"bass.wav";                   //2s
    static const char* surroundRight = AUDIO_PATH u8"surroundRight.wav"; //2s

    static const char* masterVolume1 = AUDIO_PATH u8"masterVolumeAtOne.wav";        //2s
    static const char* masterVolume_5 = AUDIO_PATH u8"masterVolumeAtPointFive.wav"; //2s

    static const char* musicPause = AUDIO_PATH u8"musicPause.wav";     //1.8s -> 1.2s
    static const char* musicResume = AUDIO_PATH u8"musicResume.wav";   //1s
    static const char* musicPlay = AUDIO_PATH u8"musicPlay.wav";		//2s

    static const char* soundPause = AUDIO_PATH u8"soundPause.wav";   //1.8s -> 1.2s
    static const char* soundStop = AUDIO_PATH u8"soundStop.wav";     //1.8s -> 1.2s
    static const char* soundResume = AUDIO_PATH u8"soundResume.wav"; //1s
    static const char* soundPlay = AUDIO_PATH u8"soundPlay.wav";     //1s

    static const char* volume1 = AUDIO_PATH u8"volumeAtOne.wav";        //1s
    static const char* volume_5 = AUDIO_PATH u8"volumeAtPointFive.wav"; //2s

#undef AUDIO_PATH
#endif // Platform specifiers
}
#endif // include guard end
