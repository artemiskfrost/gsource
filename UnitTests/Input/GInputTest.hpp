#if defined(GATEWARE_ENABLE_INPUT) && !defined(GATEWARE_DISABLE_GINPUT)

#include "GInputKeySimulate.hpp"
#include "../../Interface/System/GWindow.h"

///=============================================================================
//==============================TEST CASES======================================
///=============================================================================
bool EnableInputTests = false;
TEST_CASE("Check for OS GUI Input support", "[Input]")
{
    GW::SYSTEM::GWindow window;
    if (+window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
    {
        EnableInputTests = true;
        std::cout << "OS GUI DETECTED: RUNNING ALL GINPUT UNIT TESTS" << std::endl;
    }
    else
        std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING ALL GINPUT UNIT TESTS" << std::endl;
}

TEST_CASE("GInput creation (universal handle)", "[Create], [Input]")
{
    if (EnableInputTests == false)
    {
        std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::SYSTEM::GWindow window;
    GW::INPUT::GInput testInput;
    GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE hndl;
    hndl.display = nullptr;
    hndl.window = nullptr;

    // Fail test
    REQUIRE(-testInput.Create(hndl));

    window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
    window.GetWindowHandle(hndl);

    // Pass test
    REQUIRE(+testInput.Create(hndl));
}

TEST_CASE("GInput creation (GWindow)", "[Create], [Input]")
{
    if (EnableInputTests == false)
    {
        std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::SYSTEM::GWindow window;
    GW::INPUT::GInput testInput;

    // Fail test
    REQUIRE(-testInput.Create(window));

    window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);

    // Pass test
    REQUIRE(+testInput.Create(window));
}

TEST_CASE("GInput Key/Button Down Tests", "[GetState], [Input]")
{
    if (EnableInputTests == false)
    {
        std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::SYSTEM::GWindow window;
    GW::INPUT::GInput testInput;
    window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
    testInput.Create(window);

    //Send the simulated input (Always returns true, unless Linux)
    if (GInputTest::SimulateInput(true)) // button down
    {
        window.ProcessWindowEvents();
        float outState = 0.5f;

        //Check the number keys 1, 2, 3, 4,
        CHECK(+testInput.GetState(G_KEY_1, outState));
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_2, outState));
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_3, outState));
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_4, outState));
        CHECK(outState > 0.9f);

        //Check the letters W, A, S, D
        CHECK(+testInput.GetState(G_KEY_W, outState));
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_A, outState));
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_S, outState));
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_D, outState));
        CHECK(outState > 0.9f);

        //F1 - F12//
        CHECK(+testInput.GetState(G_KEY_F1, outState)); 
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_F2, outState)); 
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_F3, outState)); 
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_F4, outState)); 
        CHECK(outState > 0.9f);

        CHECK(+testInput.GetState(G_KEY_F5, outState));
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_F6, outState));
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_F7, outState)); 
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_F8, outState)); 
        CHECK(outState > 0.9f);

        CHECK(+testInput.GetState(G_KEY_F9, outState)); 
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_F10, outState)); 
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_F11, outState)); 
        CHECK(outState > 0.9f);
        CHECK(+testInput.GetState(G_KEY_F12, outState)); 
        CHECK(outState > 0.9f);
		
		
    }
}

TEST_CASE("GInput Key/Button Up Tests", "[GetState], [Input]")
{
    if (EnableInputTests == false)
    {
        std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::SYSTEM::GWindow window;
    GW::INPUT::GInput testInput;
    window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
    testInput.Create(window);

    //Send the simulated input (Always returns true, unless Linux)
    if (GInputTest::SimulateInput(false)) // button up
    {
        window.ProcessWindowEvents();
        float outState = 0.5f;

        //Check the number keys 1, 2, 3, 4,
        CHECK(+testInput.GetState(G_KEY_1, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_2, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_3, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_4, outState));
        CHECK(outState < 0.1f);

        //Check the letters W, A, S, D
        CHECK(+testInput.GetState(G_KEY_W, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_A, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_S, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_D, outState));
        CHECK(outState < 0.1f);

        //Check the arrow keys Left, Right, Up, Down
        CHECK(+testInput.GetState(G_KEY_LEFT, outState)); // Left Key
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_RIGHT, outState)); // Right Key
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_UP, outState)); // Up Key
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_DOWN, outState)); // Down Key
        CHECK(outState < 0.1f);

        //F1 - F12//
        CHECK(+testInput.GetState(G_KEY_F1, outState)); 
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_F2, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_F3, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_F4, outState));
        CHECK(outState < 0.1f);

        CHECK(+testInput.GetState(G_KEY_F5, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_F6, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_F7, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_F8, outState));
        CHECK(outState < 0.1f);

        CHECK(+testInput.GetState(G_KEY_F9, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_F10, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_F11, outState));
        CHECK(outState < 0.1f);
        CHECK(+testInput.GetState(G_KEY_F12, outState));
        CHECK(outState < 0.1f);
    }
}

#if defined(_WIN32)
TEST_CASE("Handling Multiple GInputs", "[Create], [Input]")
{
    if (EnableInputTests == false)
    {
        std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::SYSTEM::GWindow window;
    GW::INPUT::GInput testInput;
    GW::INPUT::GInput testInputDuplicate;
    window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
    testInput.Create(window);
    testInputDuplicate.Create(window);

}
#endif

#ifndef DISABLE_USER_INPUT_TESTS
#include <thread>
TEST_CASE("GInput live loop input check")
{
    if (EnableInputTests == false)
    {
        std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    GW::SYSTEM::GWindow window;
    GW::INPUT::GInput testInput;
    window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
    testInput.Create(window);
    
	
	auto GetKey = [&](int keyCode) 
	{
		float outstate;
		testInput.GetState(keyCode, outstate);
		return outstate > 0.0f ? true : false;
	};
	
	std::cout << "GInput: Press keys to see their keycode..." << std::endl;
    while (+window.ProcessWindowEvents())
    {
        bool acceptInput = false;
        if (+window.IsFocus(acceptInput) && acceptInput)
        {
            float dX, dY, wX, wY;
            GW::GReturn result;
            if (G_PASS(result = testInput.GetMouseDelta(dX,dY)) && result != GW::GReturn::REDUNDANT)
            {
                testInput.GetMousePosition(wX,wY);
                std::cout << "Mouse moved: Delta[X " << dX << ", Y " << dY << "] | Window[X " << wX << ", Y " << wY << "]" << std::endl;
                //printf("Mouse moved: Delta[X %f, Y %f] | Window[X %f, Y %f]\n", dX, dY, wX, wY);
            }
			for (int i = 0; i < 256; i++) {
				if (GetKey(i)) {
                    switch (i) {
                        case G_KEY_UNKNOWN:
                            std::cout << "G_KEY_UNKNOWN" << std::endl;
                            break;
                        case G_KEY_ESCAPE:
                            std::cout << "G_KEY_ESCAPE" << std::endl;
                            break;
                        case G_KEY_MINUS:
                            std::cout << "G_KEY_MINUS" << std::endl;
                            break;
                        case G_KEY_EQUALS:
                            std::cout << "G_KEY_EQUALS" << std::endl;
                            break;
                        case G_KEY_BACKSPACE:
                            std::cout << "G_KEY_BACKSPACE" << std::endl;
                            break;
                        case G_KEY_TAB:
                            std::cout << "G_KEY_TAB" << std::endl;
                            break;
                        case G_KEY_BRACKET_OPEN:
                            std::cout << "G_KEY_BRACKET_OPEN" << std::endl;
                            break;
                        case G_KEY_BRACKET_CLOSE:
                            std::cout << "G_KEY_BRACKET_CLOSE" << std::endl;
                            break;
                        case G_KEY_ENTER:
                            std::cout << "G_KEY_ENTER" << std::endl;
                            break;
                        case G_KEY_LEFTCONTROL:
                            std::cout << "G_KEY_LEFTCONTROL" << std::endl;
                            break;
						case G_KEY_RIGHTCONTROL:
                            std::cout << "G_KEY_RIGHTCONTROL" << std::endl;
                            break;
                        case G_KEY_SEMICOLON:
                            std::cout << "G_KEY_SEMICOLON" << std::endl;
                            break;
                        case G_KEY_QUOTE:
                            std::cout << "G_KEY_QUOTE" << std::endl;
                            break;
                        case G_KEY_TILDE:
                            std::cout << "G_KEY_TILDE" << std::endl;
                            break;
                        case G_KEY_LEFTSHIFT:
                            std::cout << "G_KEY_LEFTSHIFT" << std::endl;
                            break;
                        case G_KEY_BACKSLASH:
                            std::cout << "G_KEY_BACKSLASH" << std::endl;
                            break;
                        case G_KEY_COMMA:
                            std::cout << "G_KEY_COMMA" << std::endl;
                            break;
                        case G_KEY_PERIOD:
                            std::cout << "G_KEY_PERIOD" << std::endl;
                            break;
                        case G_KEY_FORWARDSLASH:
                            std::cout << "G_KEY_FORWARDSLASH" << std::endl;
                            break;
                        case G_KEY_RIGHTSHIFT:
                            std::cout << "G_KEY_RIGHTSHIFT" << std::endl;
                            break;
                        case G_KEY_PRINTSCREEN:
                            std::cout << "G_KEY_PRINTSCREEN" << std::endl;
                            break;
                        case G_KEY_LEFTALT:
                            std::cout << "G_KEY_LEFTALT" << std::endl;
                            break;
                        case G_KEY_RIGHTALT:
                            std::cout << "G_KEY_RIGHTALT" << std::endl;
                            break;
                        case G_KEY_SPACE:
                            std::cout << "G_KEY_SPACE" << std::endl;
                            break;
                        case G_KEY_CAPSLOCK:
                            std::cout << "G_KEY_CAPSLOCK" << std::endl;
                            break;
                        case G_KEY_NUMLOCK:
                            std::cout << "G_KEY_NUMLOCK" << std::endl;
                            break;
                        case G_KEY_SCROLL_LOCK:
                            std::cout << "G_KEY_SCROLL_LOCK" << std::endl;
                            break;
						case G_KEY_PAUSE:
                            std::cout << "G_KEY_PAUSE" << std::endl;
                            break;
                        case G_KEY_HOME:
                            std::cout << "G_KEY_HOME" << std::endl;
                            break;
                        case G_KEY_UP:
                            std::cout << "G_KEY_UP" << std::endl;
                            break;
                        case G_KEY_PAGEUP:
                            std::cout << "G_KEY_PAGEUP" << std::endl;
                            break;
                        case G_KEY_LEFT:
                            std::cout << "G_KEY_LEFT" << std::endl;
                            break;
                        case G_KEY_RIGHT:
                            std::cout << "G_KEY_RIGHT" << std::endl;
                            break;
                        case G_KEY_END:
                            std::cout << "G_KEY_END" << std::endl;
                            break;
                        case G_KEY_DOWN:
                            std::cout << "G_KEY_DOWN" << std::endl;
                            break;
                        case G_KEY_PAGEDOWN:
                            std::cout << "G_KEY_PAGEDOWN" << std::endl;
                            break;
                        case G_KEY_INSERT:
                            std::cout << "G_KEY_INSERT" << std::endl;
                            break;
                        case G_KEY_DELETE:
                            std::cout << "G_KEY_DELETE" << std::endl;
                            break;
                        case G_KEY_A:
                            std::cout << "G_KEY_A" << std::endl;
                            break;
                        case G_KEY_B:
                            std::cout << "G_KEY_B" << std::endl;
                            break;
                        case G_KEY_C:
                            std::cout << "G_KEY_C" << std::endl;
                            break;
                        case G_KEY_D:
                            std::cout << "G_KEY_D" << std::endl;
                            break;
                        case G_KEY_E:
                            std::cout << "G_KEY_E" << std::endl;
                            break;
                        case G_KEY_F:
                            std::cout << "G_KEY_F" << std::endl;
                            break;
                        case G_KEY_G:
                            std::cout << "G_KEY_G" << std::endl;
                            break;
                        case G_KEY_H:
                            std::cout << "G_KEY_H" << std::endl;
                            break;
                        case G_KEY_I:
                            std::cout << "G_KEY_I" << std::endl;
                            break;
                        case G_KEY_J:
                            std::cout << "G_KEY_J" << std::endl;
                            break;
                        case G_KEY_K:
                            std::cout << "G_KEY_K" << std::endl;
                            break;
                        case G_KEY_L:
                            std::cout << "G_KEY_L" << std::endl;
                            break;
                        case G_KEY_M:
                            std::cout << "G_KEY_M" << std::endl;
                            break;
                        case G_KEY_N:
                            std::cout << "G_KEY_N" << std::endl;
                            break;
                        case G_KEY_O:
                            std::cout << "G_KEY_O" << std::endl;
                            break;
                        case G_KEY_P:
                            std::cout << "G_KEY_P" << std::endl;
                            break;
                        case G_KEY_Q:
                            std::cout << "G_KEY_Q" << std::endl;
                            break;
                        case G_KEY_R:
                            std::cout << "G_KEY_R" << std::endl;
                            break;
                        case G_KEY_S:
                            std::cout << "G_KEY_S" << std::endl;
                            break;
                        case G_KEY_T:
                            std::cout << "G_KEY_T" << std::endl;
                            break;
                        case G_KEY_U:
                            std::cout << "G_KEY_U" << std::endl;
                            break;
                        case G_KEY_V:
                            std::cout << "G_KEY_V" << std::endl;
                            break;
                        case G_KEY_W:
                            std::cout << "G_KEY_W" << std::endl;
                            break;
                        case G_KEY_X:
                            std::cout << "G_KEY_X" << std::endl;
                            break;
                        case G_KEY_Y:
                            std::cout << "G_KEY_Y" << std::endl;
                            break;
                        case G_KEY_Z:
                            std::cout << "G_KEY_Z" << std::endl;
                            break;
                        case G_KEY_0:
                            std::cout << "G_KEY_0" << std::endl;
                            break;
                        case G_KEY_1:
                            std::cout << "G_KEY_1" << std::endl;
                            break;
                        case G_KEY_2:
                            std::cout << "G_KEY_2" << std::endl;
                            break;
                        case G_KEY_3:
                            std::cout << "G_KEY_3" << std::endl;
                            break;
                        case G_KEY_4:
                            std::cout << "G_KEY_4" << std::endl;
                            break;
                        case G_KEY_5:
                            std::cout << "G_KEY_5" << std::endl;
                            break;
                        case G_KEY_6:
                            std::cout << "G_KEY_6" << std::endl;
                            break;
                        case G_KEY_7:
                            std::cout << "G_KEY_7" << std::endl;
                            break;
                        case G_KEY_8:
                            std::cout << "G_KEY_8" << std::endl;
                            break;
                        case G_KEY_9:
                            std::cout << "G_KEY_9" << std::endl;
                            break;
                        case G_KEY_F1:
                            std::cout << "G_KEY_F1" << std::endl;
                            break;
                        case G_KEY_F2:
                            std::cout << "G_KEY_F2" << std::endl;
                            break;
                        case G_KEY_F3:
                            std::cout << "G_KEY_F3" << std::endl;
                            break;
                        case G_KEY_F4:
                            std::cout << "G_KEY_F4" << std::endl;
                            break;
                        case G_KEY_F5:
                            std::cout << "G_KEY_F5" << std::endl;
                            break;
                        case G_KEY_F6:
                            std::cout << "G_KEY_F6" << std::endl;
                            break;
                        case G_KEY_F7:
                            std::cout << "G_KEY_F7" << std::endl;
                            break;
                        case G_KEY_F8:
                            std::cout << "G_KEY_F8" << std::endl;
                            break;
                        case G_KEY_F9:
                            std::cout << "G_KEY_F9" << std::endl;
                            break;
                        case G_KEY_F10:
                            std::cout << "G_KEY_F10" << std::endl;
                            break;
                        case G_KEY_F11:
                            std::cout << "G_KEY_F11" << std::endl;
                            break;
                        case G_KEY_F12:
                            std::cout << "G_KEY_F12" << std::endl;
                            break;
                        case G_KEY_NUMPAD_ADD:
                            std::cout << "G_KEY_NUMPAD_ADD" << std::endl;
                            break;
                        case G_KEY_NUMPAD_SUBTRACT:
                            std::cout << "G_KEY_NUMPAD_SUBTRACT" << std::endl;
                            break;
                        case G_KEY_NUMPAD_MULTIPLY:
                            std::cout << "G_KEY_NUMPAD_MULTIPLY" << std::endl;
                            break;
						case G_KEY_NUMPAD_DIVIDE:
                            std::cout << "G_KEY_NUMPAD_DIVIDE" << std::endl;
                            break;
						case G_KEY_NUMPAD_0:
                            std::cout << "G_KEY_NUMPAD_0" << std::endl;
                            break;
						case G_KEY_NUMPAD_1:
                            std::cout << "G_KEY_NUMPAD_1" << std::endl;
                            break;
						case G_KEY_NUMPAD_2:
                            std::cout << "G_KEY_NUMPAD_2" << std::endl;
                            break;
						case G_KEY_NUMPAD_3:
                            std::cout << "G_KEY_NUMPAD_3" << std::endl;
                            break;
						case G_KEY_NUMPAD_4:
                            std::cout << "G_KEY_NUMPAD_4" << std::endl;
                            break;
						case G_KEY_NUMPAD_5:
                            std::cout << "G_KEY_NUMPAD_5" << std::endl;
                            break;
						case G_KEY_NUMPAD_6:
                            std::cout << "G_KEY_NUMPAD_6" << std::endl;
                            break;
						case G_KEY_NUMPAD_7:
                            std::cout << "G_KEY_NUMPAD_7" << std::endl;
                            break;
						case G_KEY_NUMPAD_8:
                            std::cout << "G_KEY_NUMPAD_8" << std::endl;
                            break;
						case G_KEY_NUMPAD_9:
                            std::cout << "G_KEY_NUMPAD_9" << std::endl;
                            break;
						case G_KEY_NUMPAD_PERIOD:
                            std::cout << "G_KEY_NUMPAD_PERIOD" << std::endl;
                            break;
						case G_KEY_NUMPAD_ENTER:
                            std::cout << "G_KEY_NUMPAD_ENTER" << std::endl;
                            break;
                        case G_BUTTON_LEFT:
                            std::cout << "G_BUTTON_LEFT" << std::endl;
                            break;
                        case G_BUTTON_RIGHT:
                            std::cout << "G_BUTTON_RIGHT" << std::endl;
                            break;
                        case G_BUTTON_MIDDLE:
                            std::cout << "G_BUTTON_MIDDLE" << std::endl;
                            break;
                        case G_MOUSE_SCROLL_UP:
                            std::cout << "G_MOUSE_SCROLL_UP" << std::endl;
                            break;
                        case G_MOUSE_SCROLL_DOWN:
                            std::cout << "G_MOUSE_SCROLL_DOWN" << std::endl;
                            break;
                        case G_KEY_COMMAND:
                            std::cout << "G_KEY_COMMAND" << std::endl;
                            break;
                        case G_KEY_FUNCTION:
                            std::cout << "G_KEY_FUNCTION" << std::endl;
                            break;
                        default:
                            //printf("unknown at index in GetKey(%d)", i << std::endl;
                            break;
                    }
				}
			}
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}
#endif
#endif /* defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GINPUT) */
