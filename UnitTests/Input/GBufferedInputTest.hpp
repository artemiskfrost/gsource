#if defined(GATEWARE_ENABLE_INPUT) && !defined(GATEWARE_DISABLE_GBUFFEREDINPUT)
#include "GInputKeySimulate.hpp"
#include "../../Interface/System/GWindow.h"
#include "../../Interface/Core/GEventResponder.h"

bool EnableBufferedInputTests = false;
TEST_CASE("Check for OS GUI BufferedInput support", "[Input]")
{
	GW::SYSTEM::GWindow window;
	if (+window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED))
	{
		EnableBufferedInputTests = true;
		std::cout << "OS GUI DETECTED: RUNNING ALL GBUFFEREDINPUT UNIT TESTS" << std::endl;
	}
	else
		std::cout << "WARNING: OS WINDOWING SUPPORT NOT DETECTED; SKIPPING ALL GBUFFEREDINPUT UNIT TESTS" << std::endl;
}
TEST_CASE("CreateGBufferedInput Tests", "[CreateGBufferedInput], [Input]")
{
	if (EnableBufferedInputTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;
	GW::INPUT::GBufferedInput bufferedInput;

	//Check that these cases fail appropriately
	CHECK(bufferedInput.Create(window) == GW::GReturn::INVALID_ARGUMENT);
	REQUIRE(!bufferedInput);

	CHECK(G_PASS(window.Create(200, 200, 200, 200, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	CHECK(G_PASS(bufferedInput.Create(window)));
	REQUIRE(bufferedInput);
}

//Input cases are commented out becasue of known linux bug
#if defined(_WIN32) || defined(__APPLE__)
TEST_CASE("GBufferedInput Testing Key/Button Down Events", "[Input]")
{
	if (EnableBufferedInputTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;
	GW::INPUT::GBufferedInput bufferedInput;
	CHECK(G_PASS(window.Create(200, 200, 200, 200, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(window);
	CHECK(G_PASS(bufferedInput.Create(window)));
	REQUIRE(bufferedInput);

	//Will be used to keep track of rather keys were pressed or released
	bool keys[256] = { 0 };
	int mouseX, mouseY;

	GW::CORE::GEventResponder responder;
	responder.Create([&](const GW::GEvent& eventParser)
	{
        GW::INPUT::GBufferedInput::Events gbiEvent;
		GW::INPUT::GBufferedInput::EVENT_DATA gbiEventData;
		if (+eventParser.Read(gbiEvent, gbiEventData))
        {
            switch (gbiEvent)
            {
            case GW::INPUT::GBufferedInput::Events::KEYPRESSED:
                switch (gbiEventData.data)
                {
                case G_KEY_A:
                    keys[(int)'A'] = true;
                    break;
                case G_KEY_S:
                    keys[(int)'S'] = true;
                    break;
                case G_KEY_D:
                    keys[(int)'D'] = true;
                    break;
                case G_KEY_W:
                    keys[(int)'W'] = true;
                    break;
                case G_KEY_1:
                    keys[(int)'1'] = true;
                    break;
                case G_KEY_2:
                    keys[(int)'2'] = true;
                    break;
                case G_KEY_3:
                    keys[(int)'3'] = true;
                    break;
                case G_KEY_4:
                    keys[(int)'4'] = true;
                    break;
                case G_KEY_LEFT:
                    keys[0] = true;
                    break;
                case G_KEY_RIGHT:
                    keys[1] = true;
                    break;
                case G_KEY_UP:
                    keys[2] = true;
                    break;
                case G_KEY_DOWN:
                    keys[3] = true;
                    break;
                }
                break;
            case GW::INPUT::GBufferedInput::Events::KEYRELEASED:
                switch (gbiEventData.data)
                {
                case G_KEY_A:
                    keys[(int)'A'] = false;
                    break;
                case G_KEY_S:
                    keys[(int)'S'] = false;
                    break;
                case G_KEY_D:
                    keys[(int)'D'] = false;
                    break;
                case G_KEY_W:
                    keys[(int)'W'] = false;
                    break;
                case G_KEY_1:
                    keys[(int)'1'] = false;
                    break;
                case G_KEY_2:
                    keys[(int)'2'] = false;
                    break;
                case G_KEY_3:
                    keys[(int)'3'] = false;
                    break;
                case G_KEY_4:
                    keys[(int)'4'] = false;
                    break;
                case G_KEY_LEFT:
                    keys[0] = false;
                    break;
                case G_KEY_RIGHT:
                    keys[1] = false;
                    break;
                case G_KEY_UP:
                    keys[2] = false;
                    break;
                case G_KEY_DOWN:
                    keys[3] = false;
                    break;
                }
                break;

            case GW::INPUT::GBufferedInput::Events::BUTTONPRESSED:
            {
                switch (gbiEventData.data)
                {
                case G_BUTTON_LEFT:
                    keys[4] = true;
                    break;
                case G_BUTTON_MIDDLE:
                    keys[5] = true;
                    break;
                case G_BUTTON_RIGHT:
                    keys[6] = true;
                    break;
                }
                break;
            }
            case GW::INPUT::GBufferedInput::Events::BUTTONRELEASED:
                switch (gbiEventData.data)
                {
                case G_BUTTON_LEFT:
                    keys[4] = false;
                    break;
                case G_BUTTON_MIDDLE:
                    keys[5] = false;
                    break;
                case G_BUTTON_RIGHT:
                    keys[6] = false;
                    break;
                }
                break;
            }

            mouseX = gbiEventData.screenX;
            mouseY = gbiEventData.screenY;
        }
    });
    bufferedInput.Register(responder);

	//Send the simulated input
    if (GInputTest::SimulateInput(true)) {
        window.ProcessWindowEvents();

        //Check the value of our stored keys
        //Check the number keys 1, 2, 3, 4
        CHECK(keys[(int)'1'] == true);
        CHECK(keys[(int)'2'] == true);
        CHECK(keys[(int)'3'] == true);
        CHECK(keys[(int)'4'] == true);

        //Check the letters W, A, S, D
        CHECK(keys[(int)'W'] == true);
        CHECK(keys[(int)'A'] == true);
        CHECK(keys[(int)'S'] == true);
        CHECK(keys[(int)'D'] == true);
    }
}

TEST_CASE("GBufferedInput Testing Key/Button Up Events", "[Input]")
{
	if (EnableBufferedInputTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;
	GW::INPUT::GBufferedInput bufferedInput;
	CHECK(G_PASS(window.Create(200, 200, 200, 200, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(window);
	CHECK(G_PASS(bufferedInput.Create(window)));
	REQUIRE(bufferedInput);

	//Will be used to keep track of rather keys were pressed or released
	bool keys[256] = { 0 };
	int mouseX, mouseY;

	GW::CORE::GEventResponder responder;
    responder.Create([&](const GW::GEvent& eventParser)
    {
        GW::INPUT::GBufferedInput::Events gbiEvent;
        GW::INPUT::GBufferedInput::EVENT_DATA gbiEventData;
        if (+eventParser.Read(gbiEvent, gbiEventData))
        {
            switch (gbiEvent)
            {
            case GW::INPUT::GBufferedInput::Events::KEYPRESSED:
                switch (gbiEventData.data)
                {
                case G_KEY_A:
                    keys[(int)'A'] = true;
                    break;
                case G_KEY_S:
                    keys[(int)'S'] = true;
                    break;
                case G_KEY_D:
                    keys[(int)'D'] = true;
                    break;
                case G_KEY_W:
                    keys[(int)'W'] = true;
                    break;
                case G_KEY_1:
                    keys[(int)'1'] = true;
                    break;
                case G_KEY_2:
                    keys[(int)'2'] = true;
                    break;
                case G_KEY_3:
                    keys[(int)'3'] = true;
                    break;
                case G_KEY_4:
                    keys[(int)'4'] = true;
                    break;
                case G_KEY_LEFT:
                    keys[0] = true;
                    break;
                case G_KEY_RIGHT:
                    keys[1] = true;
                    break;
                case G_KEY_UP:
                    keys[2] = true;
                    break;
                case G_KEY_DOWN:
                    keys[3] = true;
                    break;
                }
                break;
            case GW::INPUT::GBufferedInput::Events::KEYRELEASED:
                switch (gbiEventData.data)
                {
                case G_KEY_A:
                    keys[(int)'A'] = false;
                    break;
                case G_KEY_S:
                    keys[(int)'S'] = false;
                    break;
                case G_KEY_D:
                    keys[(int)'D'] = false;
                    break;
                case G_KEY_W:
                    keys[(int)'W'] = false;
                    break;
                case G_KEY_1:
                    keys[(int)'1'] = false;
                    break;
                case G_KEY_2:
                    keys[(int)'2'] = false;
                    break;
                case G_KEY_3:
                    keys[(int)'3'] = false;
                    break;
                case G_KEY_4:
                    keys[(int)'4'] = false;
                    break;
                case G_KEY_LEFT:
                    keys[0] = false;
                    break;
                case G_KEY_RIGHT:
                    keys[1] = false;
                    break;
                case G_KEY_UP:
                    keys[2] = false;
                    break;
                case G_KEY_DOWN:
                    keys[3] = false;
                    break;
                }
                break;

            case GW::INPUT::GBufferedInput::Events::BUTTONPRESSED:
            {
                switch (gbiEventData.data)
                {
                case G_BUTTON_LEFT:
                    keys[4] = true;
                    break;
                case G_BUTTON_MIDDLE:
                    keys[5] = true;
                    break;
                case G_BUTTON_RIGHT:
                    keys[6] = true;
                    break;
                }
                break;
            }
            case GW::INPUT::GBufferedInput::Events::BUTTONRELEASED:
                switch (gbiEventData.data)
                {
                case G_BUTTON_LEFT:
                    keys[4] = false;
                    break;
                case G_BUTTON_MIDDLE:
                    keys[5] = false;
                    break;
                case G_BUTTON_RIGHT:
                    keys[6] = false;
                    break;
                }
                break;
            }

            mouseX = gbiEventData.screenX;
            mouseY = gbiEventData.screenY;
        }
    });
    bufferedInput.Register(responder);
    if (GInputTest::SimulateInput(false)) {
        //Send the simulated input
#ifdef _WIN32
        GInputTest::SimulateInput(false);
        window.ProcessWindowEvents();
#else
        GInputTest::SimulateInput(false);
        window.ProcessWindowEvents();
#endif

        //Check the number keys 1, 2, 3, 4
        CHECK(keys[(int)'1'] == false);
        CHECK(keys[(int)'2'] == false);
        CHECK(keys[(int)'3'] == false);
        CHECK(keys[(int)'4'] == false);

        //Check the letters A, S, D, W
        CHECK(keys[(int)'A'] == false);
        CHECK(keys[(int)'S'] == false);
        CHECK(keys[(int)'D'] == false);
        CHECK(keys[(int)'W'] == false);
    }
}
#endif //_WIN32 || __APPLE__

#if defined(_WIN32)
TEST_CASE("Handling Multiple GBufferdInputs", "[Create], [Input]")
{
    if (EnableInputTests == false)
    {
        std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    GW::SYSTEM::GWindow window;
    GW::INPUT::GBufferedInput testInput;
    GW::INPUT::GBufferedInput testInputDuplicate;
    window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
    testInput.Create(window);
    testInputDuplicate.Create(window);

}
#endif

#if !defined(DISABLE_USER_INPUT_TESTS)
/*
TEST_CASE("User input for buffered input", "[Input]")
{
	if (EnableBufferedInputTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;
	GW::INPUT::GBufferedInput bufferedInput;
	CHECK(G_PASS(window.Create(200, 200, 200, 200, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(window);
	CHECK(G_PASS(bufferedInput.Create(window)));
	REQUIRE(bufferedInput);

	GW::CORE::GEventResponder responder;
    GW::GReturn code = responder.Create([&](const GW::GEvent& eventParser)
    {
        GW::INPUT::GBufferedInput::Events gbiEvent;
        GW::INPUT::GBufferedInput::EVENT_DATA gbiEventData;
        if (+eventParser.Read(gbiEvent, gbiEventData))
        {
            switch (gbiEvent)
            {
            case GW::INPUT::GBufferedInput::Events::KEYPRESSED:
                switch (gbiEventData.data)
                {
                case G_KEY_A:
                    std::cout << "A PRESSED" << std::endl;
                    break;
                case G_KEY_S:
                    std::cout << "S PRESSED" << std::endl;
                    break;
                case G_KEY_D:
                    std::cout << "D PRESSED" << std::endl;
                    break;
                case G_KEY_W:
                    std::cout << "W PRESSED" << std::endl;
                    break;
                }
                break;
            case GW::INPUT::GBufferedInput::Events::KEYRELEASED:
                switch (gbiEventData.data)
                {
                case G_KEY_A:
                    std::cout << "A RELEASED" << std::endl;
                    break;
                case G_KEY_S:
                    std::cout << "S RELEASED" << std::endl;
                    break;
                case G_KEY_D:
                    std::cout << "D RELEASED" << std::endl;
                    break;
                case G_KEY_W:
                    std::cout << "W RELEASED" << std::endl;
                    break;
                }
                break;
            }
        }
	});
    bufferedInput.Register(responder);

	while (+window.ProcessWindowEvents())
	{

	}
}

TEST_CASE("User mouse input for buffered input", "[Input]")
{
	if (EnableBufferedInputTests == false)
	{
		std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
		return; // ignore this test
	}
	GW::SYSTEM::GWindow window;
	GW::INPUT::GBufferedInput bufferedInput;
	CHECK(G_PASS(window.Create(200, 200, 300, 300, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED)));
	REQUIRE(window);
	CHECK(G_PASS(bufferedInput.Create(window)));
	REQUIRE(bufferedInput);

	enum MouseInputTest
	{
		NONE,
		LEFT_BUTTON_DOWN,
		LEFT_BUTTON_UP,
		RIGHT_BUTTON_DOWN,
		RIGHT_BUTTON_UP,
		MIDDLE_BUTTON_DOWN,
		MIDDLE_BUTTON_UP,
		SCROLL_WHEEL_DOWN,
		SCROLL_WHEEL_UP,
		MOUSE_MOVEMENT
	};

	MouseInputTest test = MouseInputTest::NONE;
	bool testPassed = false;

	int mouseX = 0;
	int mouseY = 0;
	int mousePrevX = 0;
	int mousePrevY = 0;

	GW::CORE::GEventResponder responder;
    GW::GReturn code = responder.Create([&](const GW::GEvent& eventParser)
    {
        GW::INPUT::GBufferedInput::Events gbiEvent;
        GW::INPUT::GBufferedInput::EVENT_DATA gbiEventData;
        if (+eventParser.Read(gbiEvent, gbiEventData))
        {
            switch (gbiEvent)
            {
            case GW::INPUT::GBufferedInput::Events::BUTTONPRESSED:
                switch (gbiEventData.data)
                {
                case G_BUTTON_LEFT:
                    testPassed = (test == MouseInputTest::LEFT_BUTTON_DOWN);
                    break;
                case G_BUTTON_RIGHT:
                    testPassed = (test == MouseInputTest::RIGHT_BUTTON_DOWN);
                    break;
                case G_BUTTON_MIDDLE:
                    testPassed = (test == MouseInputTest::MIDDLE_BUTTON_DOWN);
                    break;
                }
                break;
            case GW::INPUT::GBufferedInput::Events::BUTTONRELEASED:
                switch (gbiEventData.data)
                {
                case G_BUTTON_LEFT:
                    testPassed = (test == MouseInputTest::LEFT_BUTTON_UP);
                    break;
                case G_BUTTON_RIGHT:
                    testPassed = (test == MouseInputTest::RIGHT_BUTTON_UP);
                    break;
                case G_BUTTON_MIDDLE:
                    testPassed = (test == MouseInputTest::MIDDLE_BUTTON_UP);
                    break;
                }
                break;
            case GW::INPUT::GBufferedInput::Events::MOUSESCROLL:
                switch (gbiEventData.data)
                {
                case G_MOUSE_SCROLL_UP:
                    testPassed = (test == MouseInputTest::SCROLL_WHEEL_UP);
                    break;
                case G_MOUSE_SCROLL_DOWN:
                    testPassed = (test == MouseInputTest::SCROLL_WHEEL_DOWN);
                    break;
                }
                break;
            case GW::INPUT::GBufferedInput::Events::MOUSEMOVE:
                mouseX = gbiEventData.x;
                mouseY = gbiEventData.y;
                break;
            }
        }
	});
    bufferedInput.Register(responder);
    
	constexpr float MOUSE_DISTANCE_AMOUNT = 250.0f;
	float distance = 0;
	int step = 0;

	auto CheckTest = [&](MouseInputTest testType, const char* msg) {
		if (test == MouseInputTest::NONE)
		{
			std::cout << msg << std::endl;
			test = testType;
		}
		else if (testPassed)
		{
			std::cout << "TEST PASSED" << std::endl;
			test = MouseInputTest::NONE;
			testPassed = false;
			++step;
		}
	};

	std::cout << "Testing user mouse input" << std::endl;

	while (+window.ProcessWindowEvents())
	{
		switch (step)
		{
		case 0:	CheckTest(MouseInputTest::LEFT_BUTTON_DOWN, "Press left mouse button..."); break;
		case 1:	CheckTest(MouseInputTest::LEFT_BUTTON_UP, "Release left mouse button..."); break;
		case 2:	CheckTest(MouseInputTest::RIGHT_BUTTON_DOWN, "Press right mouse button..."); break;
		case 3:	CheckTest(MouseInputTest::RIGHT_BUTTON_UP, "Release right mouse button..."); break;
		case 4:	CheckTest(MouseInputTest::MIDDLE_BUTTON_DOWN, "Press middle mouse button..."); break;
		case 5: CheckTest(MouseInputTest::MIDDLE_BUTTON_UP, "Release middle mouse button...");	break;
		// Skip scroll wheel tests because they can't be passed without a mouse on Windows. Might want to add touchpad support.
		case 6: ++step; break; //case 6: CheckTest(MouseInputTest::SCROLL_WHEEL_UP, "Scroll up with mouse wheel..."); break;
		case 7: ++step; break; //case 7: CheckTest(MouseInputTest::SCROLL_WHEEL_DOWN, "Scroll down with mouse wheel..."); break;
		case 8:
			printf("Move mouse a distance of %d pixels...\n", (int)MOUSE_DISTANCE_AMOUNT);
			test = MouseInputTest::MOUSE_MOVEMENT;
			testPassed = false;
			++step;
			break;
		case 9:
			if (distance < MOUSE_DISTANCE_AMOUNT)
			{
				int dx = mouseX - mousePrevX;
				int dy = mouseY - mousePrevY;

				int prevDistance = (int)distance;
				distance += sqrtf((float)(dx * dx + dy * dy));
				int iDis = (int)distance;

				if (prevDistance != iDis)
					printf("Distance: %d\n", iDis);
			}
			else
			{
				std::cout << "TEST PASSED" << std::endl;
				++step;
			}
			break;
		default:
			std::cout << "Testing user mouse input is complete" << std::endl;
			window = nullptr;
			break;
		}

		mousePrevX = mouseX;
		mousePrevY = mouseY;
	}
}
*/
#include <thread>
TEST_CASE("GBufferedInput live loop input check", "[Input]")
{
    if (EnableBufferedInputTests == false)
    {
        std::cout << "GUI/Window support not detected, skipping unit test." << std::endl;
        return; // ignore this test
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    GW::SYSTEM::GWindow window;
    GW::INPUT::GBufferedInput testInput;
    window.Create(0, 0, 500, 500, GW::SYSTEM::GWindowStyle::WINDOWEDBORDERED);
    testInput.Create(window);
    
    GW::CORE::GEventResponder responder;
    responder.Create([](const GW::GEvent& event)
    {
        GW::INPUT::GBufferedInput::Events gbiEvent;
        GW::INPUT::GBufferedInput::EVENT_DATA gbiEventData;
        if (+event.Read(gbiEvent, gbiEventData))
        {
            switch (gbiEvent)
            {
                case GW::INPUT::GBufferedInput::Events::KEYPRESSED:
                    std::cout << "Key was pressed: ";
                    break;
                case GW::INPUT::GBufferedInput::Events::KEYRELEASED:
                    std::cout << "Key was released: ";
                    break;
                case GW::INPUT::GBufferedInput::Events::BUTTONPRESSED:
                    std::cout << "Mouse was pressed: ";
                    break;
                case GW::INPUT::GBufferedInput::Events::BUTTONRELEASED:
                    std::cout << "Mouse was released: ";
                    break;
                case GW::INPUT::GBufferedInput::Events::MOUSEMOVE:
                    std::cout << "Mouse moved: relative[X " << gbiEventData.screenY << ", Y "
                        << gbiEventData.screenY << "] | screen[X " << gbiEventData.screenY << 
                        ", Y " << gbiEventData.screenY << "]" << std::endl;
                    return; // don't print anymore
                case GW::INPUT::GBufferedInput::Events::MOUSESCROLL:
                    std::cout << "Mouse was scrolled: " << gbiEventData.data << std::endl;
                    break;
                default:
                    std::cout << "Invalid/Unknown event captured, investigate." << std::endl;
                    break;
            }
            switch (gbiEventData.data)
            {
                case G_KEY_UNKNOWN:
                    std::cout << "G_KEY_UNKNOWN" << std::endl;
                    break;
                case G_KEY_ESCAPE:
                    std::cout << "G_KEY_ESCAPE" << std::endl;
                    break;
                case G_KEY_MINUS:
                    std::cout << "G_KEY_MINUS" << std::endl;
                    break;
                case G_KEY_EQUALS:
                    std::cout << "G_KEY_EQUALS" << std::endl;
                    break;
                case G_KEY_BACKSPACE:
                    std::cout << "G_KEY_BACKSPACE" << std::endl;
                    break;
                case G_KEY_TAB:
                    std::cout << "G_KEY_TAB" << std::endl;
                    break;
                case G_KEY_BRACKET_OPEN:
                    std::cout << "G_KEY_BRACKET_OPEN" << std::endl;
                    break;
                case G_KEY_BRACKET_CLOSE:
                    std::cout << "G_KEY_BRACKET_CLOSE" << std::endl;
                    break;
                case G_KEY_ENTER:
                    std::cout << "G_KEY_ENTER" << std::endl;
                    break;
                case G_KEY_LEFTCONTROL:
                    std::cout << "G_KEY_LEFTCONTROL" << std::endl;
                    break;
                case G_KEY_RIGHTCONTROL:
                    std::cout << "G_KEY_RIGHTCONTROL" << std::endl;
                    break;
                case G_KEY_SEMICOLON:
                    std::cout << "G_KEY_SEMICOLON" << std::endl;
                    break;
                case G_KEY_QUOTE:
                    std::cout << "G_KEY_QUOTE" << std::endl;
                    break;
                case G_KEY_TILDE:
                    std::cout << "G_KEY_TILDE" << std::endl;
                    break;
                case G_KEY_LEFTSHIFT:
                    std::cout << "G_KEY_LEFTSHIFT" << std::endl;
                    break;
                case G_KEY_BACKSLASH:
                    std::cout << "G_KEY_BACKSLASH" << std::endl;
                    break;
                case G_KEY_COMMA:
                    std::cout << "G_KEY_COMMA" << std::endl;
                    break;
                case G_KEY_PERIOD:
                    std::cout << "G_KEY_PERIOD" << std::endl;
                    break;
                case G_KEY_FORWARDSLASH:
                    std::cout << "G_KEY_FORWARDSLASH" << std::endl;
                    break;
                case G_KEY_RIGHTSHIFT:
                    std::cout << "G_KEY_RIGHTSHIFT" << std::endl;
                    break;
                case G_KEY_PRINTSCREEN:
                    std::cout << "G_KEY_PRINTSCREEN" << std::endl;
                    break;
                case G_KEY_LEFTALT:
                    std::cout << "G_KEY_LEFTALT" << std::endl;
                    break;
                case G_KEY_RIGHTALT:
                    std::cout << "G_KEY_RIGHTALT" << std::endl;
                    break;
                case G_KEY_SPACE:
                    std::cout << "G_KEY_SPACE" << std::endl;
                    break;
                case G_KEY_CAPSLOCK:
                    std::cout << "G_KEY_CAPSLOCK" << std::endl;
                    break;
                case G_KEY_NUMLOCK:
                    std::cout << "G_KEY_NUMLOCK" << std::endl;
                    break;
                case G_KEY_SCROLL_LOCK:
                    std::cout << "G_KEY_SCROLL_LOCK" << std::endl;
                    break;
                case G_KEY_PAUSE:
                    std::cout << "G_KEY_PAUSE" << std::endl;
                    break;
                case G_KEY_HOME:
                    std::cout << "G_KEY_HOME" << std::endl;
                    break;
                case G_KEY_UP:
                    std::cout << "G_KEY_UP" << std::endl;
                    break;
                case G_KEY_PAGEUP:
                    std::cout << "G_KEY_PAGEUP" << std::endl;
                    break;
                case G_KEY_LEFT:
                    std::cout << "G_KEY_LEFT" << std::endl;
                    break;
                case G_KEY_RIGHT:
                    std::cout << "G_KEY_RIGHT" << std::endl;
                    break;
                case G_KEY_END:
                    std::cout << "G_KEY_END" << std::endl;
                    break;
                case G_KEY_DOWN:
                    std::cout << "G_KEY_DOWN" << std::endl;
                    break;
                case G_KEY_PAGEDOWN:
                    std::cout << "G_KEY_PAGEDOWN" << std::endl;
                    break;
                case G_KEY_INSERT:
                    std::cout << "G_KEY_INSERT" << std::endl;
                    break;
                case G_KEY_DELETE:
                    std::cout << "G_KEY_DELETE" << std::endl;
                    break;
                case G_KEY_A:
                    std::cout << "G_KEY_A" << std::endl;
                    break;
                case G_KEY_B:
                    std::cout << "G_KEY_B" << std::endl;
                    break;
                case G_KEY_C:
                    std::cout << "G_KEY_C" << std::endl;
                    break;
                case G_KEY_D:
                    std::cout << "G_KEY_D" << std::endl;
                    break;
                case G_KEY_E:
                    std::cout << "G_KEY_E" << std::endl;
                    break;
                case G_KEY_F:
                    std::cout << "G_KEY_F" << std::endl;
                    break;
                case G_KEY_G:
                    std::cout << "G_KEY_G" << std::endl;
                    break;
                case G_KEY_H:
                    std::cout << "G_KEY_H" << std::endl;
                    break;
                case G_KEY_I:
                    std::cout << "G_KEY_I" << std::endl;
                    break;
                case G_KEY_J:
                    std::cout << "G_KEY_J" << std::endl;
                    break;
                case G_KEY_K:
                    std::cout << "G_KEY_K" << std::endl;
                    break;
                case G_KEY_L:
                    std::cout << "G_KEY_L" << std::endl;
                    break;
                case G_KEY_M:
                    std::cout << "G_KEY_M" << std::endl;
                    break;
                case G_KEY_N:
                    std::cout << "G_KEY_N" << std::endl;
                    break;
                case G_KEY_O:
                    std::cout << "G_KEY_O" << std::endl;
                    break;
                case G_KEY_P:
                    std::cout << "G_KEY_P" << std::endl;
                    break;
                case G_KEY_Q:
                    std::cout << "G_KEY_Q" << std::endl;
                    break;
                case G_KEY_R:
                    std::cout << "G_KEY_R" << std::endl;
                    break;
                case G_KEY_S:
                    std::cout << "G_KEY_S" << std::endl;
                    break;
                case G_KEY_T:
                    std::cout << "G_KEY_T" << std::endl;;
                    break;
                case G_KEY_U:
                    std::cout << "G_KEY_U" << std::endl;
                    break;
                case G_KEY_V:
                    std::cout << "G_KEY_V" << std::endl;
                    break;
                case G_KEY_W:
                    std::cout << "G_KEY_W" << std::endl;
                    break;
                case G_KEY_X:
                    std::cout << "G_KEY_X" << std::endl;
                    break;
                case G_KEY_Y:
                    std::cout << "G_KEY_Y" << std::endl;
                    break;
                case G_KEY_Z:
                    std::cout << "G_KEY_Z" << std::endl;
                    break;
                case G_KEY_0:
                    std::cout << "G_KEY_0" << std::endl;
                    break;
                case G_KEY_1:
                    std::cout << "G_KEY_1" << std::endl;
                    break;
                case G_KEY_2:
                    std::cout << "G_KEY_2" << std::endl;
                    break;
                case G_KEY_3:
                    std::cout << "G_KEY_3" << std::endl;
                    break;
                case G_KEY_4:
                    std::cout << "G_KEY_4" << std::endl;
                    break;
                case G_KEY_5:
                    std::cout << "G_KEY_5" << std::endl;
                    break;
                case G_KEY_6:
                    std::cout << "G_KEY_6" << std::endl;
                    break;
                case G_KEY_7:
                    std::cout << "G_KEY_7" << std::endl;
                    break;
                case G_KEY_8:
                    std::cout << "G_KEY_8" << std::endl;
                    break;
                case G_KEY_9:
                    std::cout << "G_KEY_9" << std::endl;
                    break;
                case G_KEY_F1:
                    std::cout << "G_KEY_F1" << std::endl;
                    break;
                case G_KEY_F2:
                    std::cout << "G_KEY_F2" << std::endl;
                    break;
                case G_KEY_F3:
                    std::cout << "G_KEY_F3" << std::endl;
                    break;
                case G_KEY_F4:
                    std::cout << "G_KEY_F4" << std::endl;
                    break;
                case G_KEY_F5:
                    std::cout << "G_KEY_F5" << std::endl;
                    break;
                case G_KEY_F6:
                    std::cout << "G_KEY_F6" << std::endl;
                    break;
                case G_KEY_F7:
                    std::cout << "G_KEY_F7" << std::endl;
                    break;
                case G_KEY_F8:
                    std::cout << "G_KEY_F8" << std::endl;
                    break;
                case G_KEY_F9:
                    std::cout << "G_KEY_F9" << std::endl;
                    break;
                case G_KEY_F10:
                    std::cout << "G_KEY_F10" << std::endl;
                    break;
                case G_KEY_F11:
                    std::cout << "G_KEY_F11" << std::endl;
                    break;
                case G_KEY_F12:
                    std::cout << "G_KEY_F12" << std::endl;
                    break;
                case G_KEY_NUMPAD_ADD:
                    std::cout << "G_KEY_NUMPAD_ADD" << std::endl;
                    break;
                case G_KEY_NUMPAD_SUBTRACT:
                    std::cout << "G_KEY_NUMPAD_SUBTRACT" << std::endl;
                    break;
                case G_KEY_NUMPAD_MULTIPLY:
                    std::cout << "G_KEY_NUMPAD_MULTIPLY" << std::endl;
                    break;
                case G_KEY_NUMPAD_DIVIDE:
                    std::cout << "G_KEY_NUMPAD_DIVIDE" << std::endl;
                    break;
                case G_KEY_NUMPAD_0:
                    std::cout << "G_KEY_NUMPAD_0" << std::endl;
                    break;
                case G_KEY_NUMPAD_1:
                    std::cout << "G_KEY_NUMPAD_1" << std::endl;
                    break;
                case G_KEY_NUMPAD_2:
                    std::cout << "G_KEY_NUMPAD_2" << std::endl;
                    break;
                case G_KEY_NUMPAD_3:
                    std::cout << "G_KEY_NUMPAD_3" << std::endl;
                    break;
                case G_KEY_NUMPAD_4:
                    std::cout << "G_KEY_NUMPAD_4" << std::endl;
                    break;
                case G_KEY_NUMPAD_5:
                    std::cout << "G_KEY_NUMPAD_5" << std::endl;
                    break;
                case G_KEY_NUMPAD_6:
                    std::cout << "G_KEY_NUMPAD_6" << std::endl;
                    break;
                case G_KEY_NUMPAD_7:
                    std::cout << "G_KEY_NUMPAD_7" << std::endl;
                    break;
                case G_KEY_NUMPAD_8:
                    std::cout << "G_KEY_NUMPAD_8" << std::endl;
                    break;
                case G_KEY_NUMPAD_9:
                    std::cout << "G_KEY_NUMPAD_9" << std::endl;
                    break;
                case G_KEY_NUMPAD_PERIOD:
                    std::cout << "G_KEY_NUMPAD_PERIOD" << std::endl;
                    break;
                case G_KEY_NUMPAD_ENTER:
                    std::cout << "G_KEY_NUMPAD_ENTER" << std::endl;
                    break;
                case G_BUTTON_LEFT:
                    std::cout << "G_BUTTON_LEFT" << std::endl;
                    break;
                case G_BUTTON_RIGHT:
                    std::cout << "G_BUTTON_RIGHT" << std::endl;
                    break;
                case G_BUTTON_MIDDLE:
                    std::cout << "G_BUTTON_MIDDLE" << std::endl;
                    break;
                case G_MOUSE_SCROLL_UP:
                    std::cout << "G_MOUSE_SCROLL_UP" << std::endl;
                    break;
                case G_MOUSE_SCROLL_DOWN:
                    std::cout << "G_MOUSE_SCROLL_DOWN" << std::endl;
                    break;
                case G_KEY_COMMAND:
                    std::cout << "G_KEY_COMMAND" << std::endl;
                    break;
                case G_KEY_FUNCTION:
                    std::cout << "G_KEY_FUNCTION" << std::endl;
                    break;
                default:
                    std::cout << "unknown at index in GetKey" << gbiEventData.data << std::endl;
                    break;
            }
        }
    });
    // register responder to buffered input
    testInput.Register(responder);
    
    std::cout << "GBufferedInput: Press keys to get messages..." << std::endl;;
    while (+window.ProcessWindowEvents())
    {
    }
}
#endif /* DISABLE_USER_INPUT_TESTS */
#endif /* defined(GATEWARE_ENABLE_SYSTEM) && !defined(GATEWARE_DISABLE_GBUFFEREDINPUT) */
