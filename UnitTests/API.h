#ifndef G_API
#define G_API

// This file should include ALL API interfaces
// CORE
#include "../Interface/Core/GInterface.h"
#include "../Interface/Core/GThreadShared.h"
#include "../Interface/Core/GEventReceiver.h" // deprecated, will be removed in a future release
#include "../Interface/Core/GEventQueue.h" // deprecated, will be removed in a future release
#include "../Interface/Core/GEventCache.h"
#include "../Interface/Core/GLogic.h"
#include "../Interface/Core/GEventResponder.h"
#include "../Interface/Core/GEventGenerator.h"

// SYSTEM
#include "../Interface/System/GConcurrent.h"
#include "../Interface/System/GDaemon.h"
#include "../Interface/System/GFile.h"
#include "../Interface/System/GLog.h"
#include "../Interface/System/GWindow.h"
#include "../Interface/System/GApp.h"

// MATH
#include "../Interface/Math/GVector.h"
#include "../Interface/Math/GQuaternion.h"
#include "../Interface/Math/GMatrix.h"
#include "../Interface/Math/GCollision.h"

// MATH2D
#include "../Interface/Math2D/GVector2D.h"
#include "../Interface/Math2D/GMatrix2D.h"
#include "../Interface/Math2D/GCollision2D.h"

// AUDIO
#include "../Interface/Audio/GAudio.h"
#include "../Interface/Audio/GSound.h"
#include "../Interface/Audio/GMusic.h"
#include "../Interface/Audio/GAudio3D.h"
#include "../Interface/Audio/GSound3D.h"
#include "../Interface/Audio/GMusic3D.h"

// GRAPHICS
#include "../Interface/Graphics/GDirectX11Surface.h"
#include "../Interface/Graphics/GDirectX12Surface.h"
#include "../Interface/Graphics/GOpenGLSurface.h"
#include "../Interface/Graphics/GRasterSurface.h"
#include "../Interface/Graphics/GVulkanSurface.h"
#include "../Interface/Graphics/GBlitter.h"

// INPUT
#include "../Interface/Input/GInput.h"
#include "../Interface/Input/GBufferedInput.h"
#include "../Interface/Input/GController.h"
#endif