#ifndef G_WARNING_SUPPRESSIONS
#define G_WARNING_SUPPRESSIONS

#if defined(_MSC_VER) //compiler is microsoft visual C (windows, visual studio) 
#pragma warning (push)
#pragma warning (disable : 4250 4723)
//Warning C4250: [X] inherits [Y] via dominance 
//Warning C4723: potential divide by 0 (this warning is generated because the compiler doesn't understand that we're checking the absolute values of our denominators before dividing by them) 

#elif defined(__clang__) //compiler is clang (apple, xcode) 
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-braces" //suggest braces around initialization of subobject 
#pragma clang diagnostic ignored "-Wswitch" //switch statement does not have a case for all possible values of enumerator 
#pragma clang diagnostic ignored "-Wreorder" //field [X] will be initialized before field [Y] 
#pragma clang diagnostic ignored "-Wparentheses" //using the result of an assignment as a condition without extra parentheses 
//Since Wparentheses' description is very unclear, here's an example of some code that would generate the warning: 
	//if(x = y.GetSomeValue()) 
		//do stuff 
//This is essentially a null check. It sets x to the value returned from y.GetSomeValue(), then executes if (x != null). 
//equivalent code would be 
	// x = y.GetSomeValue(); 
	// if(x != null) 
		//do stuff 

#elif defined (__GNUC__) //compiler is GCC (linux, codelite) 
#pragma GCC diagnostic push
// there are currently no warnings that need to be suppressed on linux. If we ever get any, suppress them here. 

#else
#pragma message("You are compiling Gateware using a compiler we do not suppress warnings on! If your build log gets flooded with superfluous warnings, feel free to make a feature request for warning suppression in your compiler.")
#endif

#endif