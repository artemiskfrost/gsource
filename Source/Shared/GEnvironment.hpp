// This file contains defines for platform-specific code
// Authors: Jacob Morales
#ifndef GENVIRONMENT_HPP
#define GENVIRONMENT_HPP

#if defined(__APPLE__)

    #include <TargetConditionals.h>

    #if TARGET_OS_IOS

        #define GATEWARE_ENV_APP

    #elif TARGET_OS_MAC

        #define GATEWARE_ENV_DESKTOP

    #endif //TARGET_OS_MAC/IOS

#elif defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)

    #include <gamingdeviceinformation.h>

    #define GATEWARE_ENV_APP

#elif defined(__linux__) || defined(_WIN32)

    #define GATEWARE_ENV_DESKTOP

#else

    #define GATEWARE_ENV_UNKNOWN

#endif //__APPLE__



#endif //GENVIRONMENT_HPP
