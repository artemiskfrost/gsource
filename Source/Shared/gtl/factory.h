/************************************************
Factory
Authors and Contributors: Christopher Kennedy, Alexander Clemmons

Factory is a random access container for use with large arrays.
Like ld_vector, factory is a variation of a resizable container like
std::vector, though the deletion process is edited to make operations 
faster for the computer. When a user calls remove() or erase(),
factory will swap the item from the back of the array to the spot 
that was erased, and the item that got moved will always be able
to be accessed via the Routing Table's keys.
***********************************************/

#ifndef _FACTORY_H_
#define _FACTORY_H_

#include <assert.h>
#include <utility>

namespace gtl
{

	template< typename T >
	class factory
	{
		// private member variables
		// current size of the array
		unsigned int _size;

		// current capacity of the vectory
		unsigned int _capacity;

		// next key value to be used
		unsigned int next_Free;

		// highest key value given out
		unsigned int highest_Key;

		// array of templated type
		T* Data = nullptr;

		// central to the class working
		// array that stays the same size as the Data array
		// used to route the accessors to the correct data
		int* Router = nullptr;

		struct Arbitrator
		{
			T* begin;
			T* end;
		};
		Arbitrator arbiter;

		// create function called anytime a factory is initialized
		void create(void)
		{
			clear(); //clear has the exact functionality we need
		}

		// expand function dynamically reallocates and creates new array
		void expand(void)
		{
			// updating capacity
			int cap = _capacity;
			_capacity *= 2;

			// creating temp pointers so the data isn't lost
			T* old = Data;
			int* old_router = Router;

			// creating a new array set to Data
			Data = new T[_capacity];
			Router = new int[_capacity];

			// copying the old array to the new array
			for (int i = 0; i < cap; i++)
			{
				Data[i] = old[i];
				Router[i] = old_router[i];
			}

			// freeing the old data
			delete[] old;
			delete[] old_router;

			//update arbitrator
			arbiter.begin = Data;
			arbiter.end = Data + _size;
		}

		// empty function gets called when destructor is called
		void empty(void)
		{
			if (Data)
				delete[] Data;


			if (Router)
				delete[] Router;


			_size = 0;
			_capacity = 0;
			next_Free = 0;
			highest_Key = 0;
			Data = nullptr;
			Router = nullptr;
		}

	public:

		/*Follows an iterator Structure similar to the iterator found in ld_vector.h*/

		class iterator;
		friend class iterator;

		class iterator
		{
			const Arbitrator* navigator; //stores the beginning and end of the array
			T* dat;

		public:
			explicit iterator(T* d = 0u, const Arbitrator* a = 0u)
				: dat(d), navigator(a) { }

			~iterator()
			{
				dat = nullptr;
				navigator = nullptr;
			}

			//dereference operators
			T* operator->() { return dat; }
			T& operator*() const { return *dat; }

			//Object comparison functions
			//is equal
			bool operator==(const iterator& comp) const
			{
				return dat == comp.dat && navigator == comp.navigator;
			}

			//is not equal
			bool operator!=(const iterator& comp) const
			{
				return dat != comp.dat || navigator != comp.navigator;
			}

			operator int() const { return static_cast<int>(dat - navigator->begin); }

			operator bool() const {
				return (dat >= navigator->begin &&
					dat <= navigator->end);
			}

			//PreFix Iterators
			iterator& operator++()
			{
				assert(dat + 1 <= navigator->end);
				dat++;
				return (*this);
			}

			iterator& operator--()
			{
				assert(dat- 1 >= navigator->begin);
				dat--;
				return (*this);
			}

			//PostFix Iterators
			iterator operator++(int)
			{
				iterator tmp = *this;
				assert(dat + 1 <= navigator->end);
				dat++;
				return tmp;
			}

			iterator operator--(int)
			{
				iterator tmp = *this;
				assert(dat + 1 >= navigator->begin);
				dat--;
				return tmp;
			}


			//Iterator arithmetic operations
			iterator operator+(int mov) const
			{
				iterator tmp = *this;
				tmp.dat += mov;
				return tmp;
			}
			iterator operator-(int mov) const
			{
				iterator tmp = *this;
				tmp.dat -= mov;
				return tmp;
			}
		}; // END iterator class

		class const_iterator;
		friend class const_iterator;

		class const_iterator
		{
			const Arbitrator* navigator; //same as ld_vector "envoy"
			T* dat;

		public:
			explicit const_iterator(T* d = 0u, const Arbitrator* a = 0u)
				: dat(d), navigator(a) { }

			~const_iterator()
			{
				dat = nullptr;
				navigator = nullptr;
			}

			//dereference operators
			const T* operator->() { return dat; }
			const T& operator*() const { return *dat; }

			//is equal
			bool operator==(const const_iterator& comp) const
			{
				return dat == comp.dat && navigator == comp.navigator;
			}

			//is not equal
			bool operator!=(const const_iterator& comp) const
			{
				return dat != comp.dat || navigator != comp.navigator;
			}

			operator int() const { return dat - navigator->begin; }
			operator bool() const {
				return (dat >= navigator->begin &&
					dat <= navigator->end);
			}
			//PreFix Iterators
			const_iterator& operator++()
			{
				assert(dat + 1 <= navigator->end);
				dat++;
				return (*this);
			}

			const_iterator& operator--()
			{
				assert(dat - 1 >= navigator->begin);
				dat--;
				return (*this);
			}
			//PostFix Iterators
			const_iterator operator++(int)
			{
				const_iterator tmp = *this;
				assert(dat + 1 <= navigator->end);
				dat++;
				return tmp;
			}
			// decrement post
			const_iterator operator--(int)
			{
				const_iterator tmp = *this;
				assert(dat - 1 >= navigator->begin);
				dat--;
				return tmp;
			}

			// jump by element amount
			const_iterator operator+(int mov) const {
				const_iterator tmp = *this;
				tmp.dat += mov;
				return tmp;
			}
			const_iterator operator-(int mov) const
			{ 
				const_iterator tmp = *this;
				tmp.dat -= mov;
				return tmp;
			}
		}; // END const_iterator class

		// Constructor
		factory()
		{
			create();
		}

		// Deconstructor
		~factory()
		{
			empty();
		}

		// Copy constructor
		factory(const factory& _fact)
		{
			*this = _fact;
		}

		// Move constructor, this should invoke move assignment operator
		factory(factory&& _fact) noexcept
		{
			*this = std::move(_fact);
		}

		factory& operator= (const factory& _fact)
		{
			this->empty();

			this->_capacity = _fact._capacity;
			this->next_Free = _fact.next_Free;
			this->_size = _fact._size;
			this->highest_Key = _fact.highest_Key;

			this->Data = new T[_capacity];
			this->Router = new int[highest_Key];
			arbiter.begin = Data;
			arbiter.end = Data + _size;

			for (unsigned int i = 0; i < highest_Key; i++)
			{
				if (i < _capacity)
					this->Data[i] = _fact.Data[i];

				this->Router[i] = _fact.Router[i];
			}

			//keep the old factory, this is making a copy

			return *this;
		}

		// Move assignment operator
		factory& operator= (factory&& _fact) noexcept
		{
			this->empty();

			this->_capacity = _fact._capacity;
			this->next_Free = _fact.next_Free;
			this->_size = _fact._size;
			this->highest_Key = _fact.highest_Key;
			this->Data = _fact.Data;
			this->Router = _fact.Router;
			this->arbiter = _fact.arbiter;

			_fact.Data = nullptr;
			_fact.Router = nullptr;

			return *this;
		}

		T& operator[] (const unsigned int _key)
		{
			assert((_key >= 0) && (_key <= highest_Key) && (Router[_key] >= 0));

			return Data[Router[_key]];
		}

		const T& operator[] (const unsigned int _key) const
		{
			assert((_key >= 0) && (_key <= highest_Key) && (Router[_key] >= 0));

			return Data[Router[_key]];
		}

		// Pushes a specified item into the array
		// This function returns the key used to push it in
		int push_back(const T& item)
		{
			// checks if pushing the item will cause the array to go out of bounds, if it does then expands array
			if ((_size + 1) >= _capacity)
				expand();

			int ReturnThis = next_Free;
			int temp_free = 0;

			if (next_Free < _size)
				temp_free = Router[next_Free] * -1;

			// assigns the next index in the array to the _item
			Data[_size] = item;
			Router[next_Free] = _size;

			//everthing updates
			if (next_Free == _size)
			{
				next_Free++;
				highest_Key++;
			}
			else
				next_Free = temp_free;

			// updates size
			_size++;

			arbiter.end = Data + _size;

			return ReturnThis;
		}

		void pop_back()
		{
			next_Free = Router[highest_Key - 1];
			_size--;
			highest_Key--;
			arbiter.end = Data + _size;
		}

		// Checks to see if the given key is available. This is the same as the assert inside of remove()
		bool is_valid(unsigned int _keyVal) const
		{
			return (_keyVal >= 0) && (_keyVal < highest_Key)
				&& (Router[_keyVal] >= 0) && &Data[Router[_keyVal]];
		}

		// Removes a specific key value from the factory and not a specific array index
		void remove(unsigned int _keyVal)
		{
			if (_size == 0)
				return;
			//error check
			assert((_keyVal >= 0) && (_keyVal <= highest_Key)
				&& (Router[_keyVal] >= 0));

			int index = Router[_keyVal];

			// setting pointers to the specific index that will be removed and the final index of the array
			T* zero = &Data[index];
			T* end = &Data[_size - 1];

			Router[_size - 1] = index;
			Router[index] = -1;
			next_Free = index;

			// copies the memory form the final index to the first index
			*zero = *end;
			

			// decrement size
			_size--;
			if (highest_Key < _size || _size == 0) //if we remove from the end or empty array
				highest_Key = _size;

			arbiter.end = Data + _size;
		}

		// Removes a from the factory using an iterator
		iterator erase(iterator iter)
		{
				if (_size == 0)
				return iter;

			assert(iter);

			int index = iter; //iterators can convert into their data array indexes

			// setting pointers to the specific index that will be removed and the final index of the array
			T* zero = &Data[index];
			T* end = &Data[_size - 1];

			Router[_size - 1] = index;
			Router[index] = -1;
			next_Free = index;

			// copies the memory form the final index to the first index
			*zero = *end;

			// decrement size
			_size--;
			if (highest_Key < _size || _size == 0) //if we remove from the end or empty array
				highest_Key = _size;

			arbiter.end = Data + _size;

			return iter;
		}

		// Shinks the array so capacity equals size 
		void shrink()
		{
			// early out 
			if (_size == 1 && _capacity == 2 || _capacity == highest_Key)
				return;

			// updating capacity to size
			_capacity = highest_Key;

			// creating a pointer and setting it to the array
			T* old = Data;
			int* old_router = Router;

			// creating a new array set to Data
			Data = new T[_capacity];

			Router = new int[highest_Key];

			// copying the data from the old array to the new array
			//needs to retain all old keys
			for (unsigned int i = 0; i < highest_Key; i++)
			{
				if (i < _size)
					Data[i] = old[i];

				Router[i] = old_router[i];
			}

			delete[] old;
			delete[] old_router;
			next_Free = _size;
		}

		// Resizes the array a capacity of your choice
		void reserve(unsigned int size)
		{
			//early out
			if (size < _size)
				return;
			// updating capacity to size
			_capacity = size;

			// creating a pointer and setting it to the array
			T* old = Data;
			int* old_router = Router;

			// creating a new array set to Data
			Data = new T[_capacity];
			Router = new int[highest_Key]; //uses highest_Key to retain keys

			// copying the data from the old array to the new array
			for (unsigned int i = 0; i < highest_Key; i++)
			{
				if (i < _size)
					Data[i] = old[i];

				Router[i] = old_router[i];
			}

			delete[] old;
			delete[] old_router;
			next_Free = _size;
		}

		T* data() { return Data; }
		const T* data() const { return Data; }

		//void align(); //needs implementation

		//void sort(); //needs implementation

		void clear()
		{
			empty();
			_capacity = 2;

			Data = new T[_capacity];
			Router = new int[_capacity];
			arbiter.begin = Data;
			arbiter.end = Data + _size;
		}

		// Returns the current size of the array
		unsigned int size(void) const { return _size; }

		// Returns the current capacity
		unsigned int capacity(void) const { return _capacity; }

		//begin and end iterators
		iterator begin(void) { return iterator(Data, &arbiter); }
		iterator end(void) { return iterator(Data + _size, &arbiter); }

		const_iterator cbegin(void) const { return const_iterator(Data, &arbiter); }
		const_iterator cend(void) const { return const_iterator(Data + _size, &arbiter); }

		//references to the data in the data array
		T& front(void) { return *begin(); }
		const T& front(void) const { return *cbegin(); }
		T& back(void) { return *(--end()); }
		const T& back(void) const { return *(--cend()); }

	};// END Factory class
} // END namespace gtl
#endif // !_FACTORY_H_