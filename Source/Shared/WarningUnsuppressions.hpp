#ifndef G_WARNING_UNSUPPRESSIONS
#define G_WARNING_UNSUPPRESSIONS

#if defined(_MSC_VER)
#pragma warning (pop)
#elif defined(__clang__)
#pragma clang diagnostic pop
#elif defined (__GNUC__)
#pragma GCC diagnostic pop
#endif
#endif