#ifndef GATEWARE_WINUTILS_H
#define GATEWARE_WINUTILS_H

#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <string>
#include <stringapiset.h> //for UTF_8 to UTF_16 conversion
#include <locale> //old include from GUtility, required for some other utf conversions
#include <codecvt>

// Author: Alexander Clemmons
// Purpose: As of April 13, 2022, winutils.h contains these overloaded functions to convert between UTF8 and UTF16.
// Copyright: 7thGate Software LLC.
// License : MIT

namespace internal_gw {
	////UTF8 to UTF16
	inline const std::wstring UTFStringConverter(const std::string& str)
	{
		int count = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), static_cast<int>(str.length()), NULL, 0);
		std::wstring wstr(count, 0);
		MultiByteToWideChar(CP_UTF8, 0, str.c_str(), static_cast<int>(str.length()), &wstr[0], count);
		return wstr;
	}
	////UTF16 to UTF8
	inline const std::string UTFStringConverter(const std::wstring& wstr)
	{
		int count = WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), static_cast<int>(wstr.length()), NULL, 0, NULL, NULL);
		std::string str(count, 0);
		WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), static_cast<int>(wstr.length()), &str[0], count, NULL, NULL);
		return str;
	}
}
#undef WIN32_LEAN_AND_MEAN
#endif // #endif _WIN32
#endif // #endif GATEWARE_WINUTILS_H
