#ifndef GATEWARE_IOSUTILS_H
#define GATEWARE_IOSUTILS_H

#if defined(__APPLE__)
    #include <TargetConditionals.h>
#if TARGET_OS_IOS

@import Foundation;

// This include is for iOS/tvOS/watchOS only.
// This is the only change made to the macros to get them to work with other Apple OS's
#import <objc/runtime.h>

// G_OBJC MACROS
//
// Author: Ozzie Mercado
// Contributors: Jacob Morales
// Purpose: These macros are designed to simplify the process of writing Objective-C classes using the Objective-C
//          Runtime Library. All Objective-C classes must be created at runtime. The implementation of Objective-C
//          classes cannot be implicitly inlined. By using the Objective-C Runtime Library, we can get around that
//          limitation. Using these macros will make writing and using the Objective-C Runtime Library classes easier.
// Copyright: 7thGate Software LLC.
// License : MIT
//
// For more information about the Objecti-C Runtime Library:
// https://developer.apple.com/documentation/objectivec/objective-c_runtime
//
// --- DIRECTIONS ---
// Below directions for writing a G_OBJC class interface, implementation, how to use call methods, and how to
// retrieve data members. Further clarification for what each G_OBJC macro does can be found below the instructions
// at each macro definition.
//
// Writing a G_OBJC Class Interface:
//
//    // This should be somewhere above the implementation of the C++ Gateware library.
//    namespace internal_gw // Keep all G_OBJC class interfaces inside the internal_gw namespace.
//    {
//        // OPTIONAL: All data members of your class should go inside the data members struct for simplicity sake.
//        //           This makes it much easier to access them later. If you don't need to include data members in
//        //           your class, then you can skip creating the data members struct.
//        G_OBJC_DATA_MEMBERS_STRUCT(YourClassNameHere)
//        {
//            // These data members are just for example.
//            NSObject* someObject;
//            bool active;
//            int values[5];
//        };
//
//        G_OBJC_HEADER_DATA_MEMBERS_PROPERTY_METHOD(YourClassNameHere); // OPTIONAL: Only required when using the G_OBJC_DATA_MEMBERS_STRUCT().
//
//        // Forward declare all member functions of your class. You could write implementation here, but this is cleaner.
//        // These functions are just for example.
//        G_OBJC_HEADER_STATIC_METHOD_WITH_ARGUMENTS(YourClassNameHere, void, clearValues, id classInstance);
//        G_OBJC_HEADER_INSTANCE_METHOD(YourClassNameHere, void, toggleActive);
//        G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(YourClassNameHere, unsigned int, windowWillResize, NSSize* size, int index);
//
//        // Create your class.
//        // Note: If 'YourObjectiveCParentClass' is NSObject, then you may not need to write your class in Objective-C.
//        //       Regardless, you can still create a G_OBJC class that subclasses NSObject.
//        G_OBJC_CLASS_BEGIN(YourClassNameHere, YourObjectiveCParentClass)
//        {
//            // Add the data members getter function to your class definition.
//            G_OBJC_CLASS_DATA_MEMBERS_PROPERTY(YourClassNameHere); // OPTIONAL: Only required when using the G_OBJC_DATA_MEMBERS_STRUCT().
//
//            // Add member functions to your class definition. These functions are just for example.
//            // Note: The 3rd argument is the type encoding for the function. Check the macro definition for more
//            //       information. The 4th argument in G_OBJC_CLASS_METHOD_WITH_ARGUMENTS() is a ':' for each
//            //       argument in the member function.
//            G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(YourClassNameHere, clearValues, "v@:@", :);
//            G_OBJC_CLASS_METHOD(YourClassNameHere, toggleActive, "v@:");
//            G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(YourClassNameHere, windowWillResize, "I@:@i", ::);
//        }
//        G_OBJC_CLASS_END(YourClassNameHere)
//    }
//
// Writing a G_OBJC Class Implementation:
//
//    // This should be somewhere below the implementation of the C++ Gateware library.
//    namespace internal_gw // Keep all G_OBJC class implementations inside the internal_gw namespace.
//    {
//        // Implementation for properties of your class.
//        G_OBJC_IMPLEMENTATION_DATA_MEMBERS_PROPERTY_METHOD(YourClassNameHere) // OPTIONAL: Only required when using the G_OBJC_DATA_MEMBERS_STRUCT().
//
//        // Implementation for all member functions of your class. These functions are just for example.
//        G_OBJC_HEADER_STATIC_METHOD_WITH_ARGUMENTS(YourClassNameHere, void, clearValues, id classInstance)
//        {
//            // Get the data members for 'classInstance'. We assume 'classInstance' is an instance of YourClassNameHere.
//            // Note: Member functions do not have direct access to member variables. G_OBJC_GET_DATA_MEMBERS() makes it easy
//            //       to get a reference to the data members of your class, use them, and make changes to them.
//            G_OBJC_DATA_MEMBERS_TYPE(YourClassNameHere)& dataMembers = G_OBJC_GET_DATA_MEMBERS(YourClassNameHere, classInstance);
//
//            for (int i = 0; i < 5; ++i)
//                dataMembers.values[i] = 0;
//        }
//
//        G_OBJC_HEADER_INSTANCE_METHOD(YourClassNameHere, void, toggleActive)
//        {
//            // Note: 'self' is available in all non-static member functions.
//            G_OBJC_DATA_MEMBERS_TYPE(YourClassNameHere)& dataMembers = G_OBJC_GET_DATA_MEMBERS(YourClassNameHere, self);
//
//            dataMembers.active = !dataMembers.active;
//        }
//
//        G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(YourClassNameHere, unsigned int, windowWillResize, NSSize* size, int index)
//        {
//            // This is a call to the above static member function 'clearValues'.
//            G_OBJC_CALL_METHOD_WITH_ARGUMENTS(YourClassNameHere, self, clearValues, self);
//
//            G_OBJC_DATA_MEMBERS_TYPE(YourClassNameHere)& dataMembers = G_OBJC_GET_DATA_MEMBERS(YourClassNameHere, self);
//            dataMembers.someObject = nullptr;
//            dataMembers.active = true;
//
//            return (unsigned int)index;
//        }
//    }
//
// Using a G_OBJC Class in a C++ Gateware library implementation:
//
//    // Allocating memory for a member of a G_OBJC class.
//    id classInstance = [internal_gw::G_OBJC_GET_CLASS(YourClassNameHere) alloc];
//
//    // Initializing a member of a G_OBJC class. Note: This example is after allocating 'classInstance'.
//    classInstance = [classInstance init];
//
//    // Both allocating and initializing a member of a G_OBJC class.
//    id classInstance = [[internal_gw::G_OBJC_GET_CLASS(YourClassNameHere) alloc] init];
//
//    // Cleaning up a member of a G_OBJC class.
//    [classInstance autorelease];
//    or
//    [classInstance release];
//
//    // Getting the data members of a G_OBJC class.
//    G_OBJC_DATA_MEMBERS_TYPE(YourClassNameHere)& dataMembers = G_OBJC_GET_DATA_MEMBERS(YourClassNameHere, classInstance);
//
//    // Getting a specific data member of a G_OBJC class. Note: This example is after getting the data members of the G_OBJC class.
//    dataMembers.specificDataMemberName
//
//    // Setting a specific data member of a G_OBJC class. Note: This example is after getting the data members of the G_OBJC class.
//    dataMembers.specificDataMemberName = 3.14f;
//
//    // Calling a member function of a G_OBJC class.
//    internal_gw::G_OBJC_CALL_METHOD(YourClassNameHere, classInstance, YourMemberFunctionNameHere);
//
//    // Calling a member function of a G_OBJC class with arguments.
//    internal_gw::G_OBJC_CALL_METHOD_WITH_ARGUMENTS(YourClassNameHere, classInstance, YourMemberFunctionNameHere, 3.14f, true);
//
// --- DIRECTIONS END ---

//! Gets a reference to an Objective-C Runtime Library class.
/*!
*    Uses the class_name to return a reference to the Objective-C Runtime Library class. If the class hasn't been
*    created yet, this function creates it at runtime before returning a reference to it.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*
*    \retval Class& Reference to an Objective-C Runtime Library class.
*/
#define G_OBJC_GET_CLASS(class_name) Class##class_name()

// G_OBJC Class Creation:

//! Makes a static declaration of a class.
/*!
*    Uses the class_name to make a static declaration of the class.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*/
#define G_OBJC_CLASS_DECLARE(class_name) static Class class_name

//! Allocates the Objective-C Runtime Library class.
/*!
*    Uses the class_name to allocate the class. The class will be created as a subclass to class_parent. This must
*    happen after G_OBJC_CLASS_DECLARE.
*    Note: If 'class_parent' is NSObject, then you may not need to write your class in Objective-C.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] class_parent An Objective-C class that class_name will inherit from. Must at least be NSObject.
*/
#define G_OBJC_CLASS_ALLOCATE(class_name, class_parent) class_name = objc_allocateClassPair([class_parent class], #class_name, 0)

//! Registers the Objective-C Runtime Library class.
/*!
*    Uses the class_name to register the class. This must happen after G_OBJC_CLASS_ALLOCATE.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*/
#define G_OBJC_CLASS_REGISTER(class_name) objc_registerClassPair(class_name)

//! Creates the function header for the G_OBJC_GET_CLASS function.
/*!
*    Uses the class_name to create the function header for the G_OBJC_GET_CLASS function. This can be used as a
*    forward declaration before the implementation is written.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*/
#define G_OBJC_CLASS_DEFINITION_HEADER(class_name) static Class& G_OBJC_GET_CLASS(class_name)

//! Creates the first part (1 of 3) of the G_OBJC_GET_CLASS function implementation.
/*!
*    Uses the class_name and class_parent to create the implementation of the G_OBJC_GET_CLASS function. This macro
*    handles creating the function header, class declaration, and class allocation. It also ensures that classes
*    cannot be allocated more than once.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] class_parent An Objective-C class that class_name will inherit from. Must at least be NSObject.
*/
#define G_OBJC_CLASS_BEGIN(class_name, class_parent) G_OBJC_CLASS_DEFINITION_HEADER(class_name)\
{\
    G_OBJC_CLASS_DECLARE(class_name);\
    if (class_name == nil)\
    {\
        G_OBJC_CLASS_ALLOCATE(class_name, class_parent);\
        if (true) // Added to allow for a {code block}.

//! Creates the last part (3 of 3) of the G_OBJC_GET_CLASS function implementation.
/*!
*    Uses the class_name to create the implementation of the G_OBJC_GET_CLASS function. This macro handles class
*    registration and the returning of a reference to the class.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*/
#define G_OBJC_CLASS_END(class_name) G_OBJC_CLASS_REGISTER(class_name);\
    }\
    return class_name; \
}

//! Creates a method for the second part (2 of 3) of the G_OBJC_GET_CLASS function implementation.
/*!
*    Uses the class_name, method_name, and method_types_string to add a method to the Objective-C Runtime Library class.
*    The class_name and method_name are used to associate the method with its implementation name created with on of the
*    the G_OBJC_HEADER or G_OBJC_IMPLEMENTATION macros.
*    Note: method_types_string should be a string of type encodings that represent the types used in the function header.
*          The 1st character must be the encoded character for the return type. The 2nd and 3rd characters must be "@:".
*          Because G_OBJC calls functions directly, it is possible to use the wrong type encodings for the function
*          without any issues. However, this may cause problems when overriding inherited functions.
*    For more information about type encodings: https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtTypeEncodings.html#//apple_ref/doc/uid/TP40008048-CH100
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] method_name The name of the method.
*    \param [in] method_types_string A string of type encodings representing each type in the function.
*/
#define G_OBJC_CLASS_METHOD(class_name, method_name, method_types_string) class_addMethod(class_name, @selector(method_name), (IMP)class_name##Method##method_name, method_types_string)

//! Creates a method with arguments for the second part (2 of 3) of the G_OBJC_GET_CLASS function implementation.
/*!
*    Uses the class_name, method_name, method_types_string, and argument_colons to add a method to the Objective-C Runtime
*    Library class. The class_name and method_name are used to associate the method with its implementation name. The
*    implementation of the method can be defined by using one of the G_OBJC_HEADER macros ending with _WITH_ARGUMENTS and
*    then writing the implementation inside a code block underneath it.
*    Note: method_types_string should be a string of type encodings that represent the types used in the function header.
*          The 1st character must be the encoded character for the return type. The 2nd and 3rd characters must be "@:".
*          All 4th character and so on must be the encoded character for each argument type in the order they appear.
*          Because G_OBJC calls functions directly, it is possible to use the wrong type encodings for the function
*          without any issues. However, this may cause problems when overriding inherited functions.
*    For more information about type encodings: https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtTypeEncodings.html#//apple_ref/doc/uid/TP40008048-CH100
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] method_name The name of the method.
*    \param [in] method_types_string A string of type encodings representing each type in the function.
*    \param [in] argument_colons A colon(:) for each argument in the function.
*/
#define G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(class_name, method_name, method_types_string, argument_colons) class_addMethod(class_name, @selector(method_name argument_colons), (IMP)class_name##Method##method_name, method_types_string)

//! Creates a property for the second part (2 of 3) of the G_OBJC_GET_CLASS function implementation.
/*!
*    Uses the class_name, type_name, and property_name to add a property to the Objective-C Runtime Library class.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_CLASS_ADD_PROPERTY(class_name, type_name, property_name) class_addIvar(class_name, "_"#property_name, sizeof(type_name), log2(sizeof(type_name)), @encode(type_name));\
{\
    objc_property_attribute_t instance_variable_type = { "T", "@\""#type_name"\"" };\
    objc_property_attribute_t instance_property_name = { "V", "_"#property_name };\
    objc_property_attribute_t instance_variable_attributes[2] = { instance_variable_type, instance_property_name };\
    class_addProperty(class_name, #property_name, instance_variable_attributes, 2);\
}

//! Creates a property get method for the second part (2 of 3) of the G_OBJC_GET_CLASS function implementation.
/*!
*    Uses the class_name and property_name to add the get method for a property to the Objective-C Runtime Library class.
*    Note: This may cause problems if the property is inherited. In that case, G_OBJC_CLASS_METHOD should be used instead.
*          This will not cause problems for non-inherited properties because G_OBJC calls the implementation of the get-
*          method directly.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] property_name The data member type.
*/
#define G_OBJC_CLASS_GET_PROPERTY_METHOD(class_name, property_name) G_OBJC_CLASS_METHOD(class_name, property_name, "@@:")

//! Creates a property set method for the second part (2 of 3) of the G_OBJC_GET_CLASS function implementation.
/*!
*    Uses the class_name and property_name to add the set method for a property to the Objective-C Runtime Library class.
*    Note: This may cause problems if the property is inherited. In that case, G_OBJC_CLASS_METHOD_WITH_ARGUMENTS should
*          be used instead. This will not cause problems for non-inherited properties because G_OBJC calls the
*          implementation of the set method directly.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] property_name The data member type.
*/
#define G_OBJC_CLASS_SET_PROPERTY_METHOD(class_name, property_name) G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(class_name, Set##property_name, "v@:@", :)

//! Creates a property and its get/set methods for the second part (2 of 3) of the G_OBJC_GET_CLASS function implementation.
/*!
*    Uses the class_name, type_name, and property_name to add a property to the Objective-C Runtime Library class. It also
*    adds both the get method and set method for the property to the class.
*    Note: You can add each data member in your class as a property, but working with a large number of properties adds many
*          extra lines to your code. Instead, I recommend using G_OBJC DATA_MEMBERS macros to reduce function calls and
*          increase the readability of your code.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_CLASS_PROPERTY(class_name, type_name, property_name) G_OBJC_CLASS_ADD_PROPERTY(class_name, type_name, property_name);\
G_OBJC_CLASS_GET_PROPERTY_METHOD(class_name, property_name);\
G_OBJC_CLASS_SET_PROPERTY_METHOD(class_name, property_name)

// G_OBJC Class Interface:

//! Makes the header for a G_OBJC class static method.
/*!
*    Uses the class_name, method_return_type, and method_name to create the header for a static method. This can be used
*    as a forward declaration before the implementation is written. In this case, this macro should be placed above
*    G_OBJC_CLASS_BEGIN in your library file. This macro can also be placed above a code block containing implementation
*    for the method.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] method_return_type The method return type.
*    \param [in] method_name The name of the member function.
*/
#define G_OBJC_HEADER_STATIC_METHOD(class_name, method_return_type, method_name) static method_return_type class_name##Method##method_name(id self, SEL _cmd)

//! Makes the header for a G_OBJC class static method with arguments.
/*!
*    Uses the class_name, method_return_type, method_name, and __VA_ARGS__ to create the header for a static method
*    with arguments. This can be used as a forward declaration before the implementation is written. In this case,
*    this macro should be placed above G_OBJC_CLASS_BEGIN in your library file. This macro can also be placed above
*    a code block containing implementation for the method.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] method_return_type The method return type.
*    \param [in] method_name The name of the member function.
*    \param [in] __VA_ARGS__ Comma-seperated arguments with their types.
*
*    \retval method_return_type The return type given.
*/
#define G_OBJC_HEADER_STATIC_METHOD_WITH_ARGUMENTS(class_name, method_return_type, method_name, ...) static method_return_type class_name##Method##method_name(id self, SEL _cmd, __VA_ARGS__)

//! Makes the header for a G_OBJC class instance method.
/*!
*    Uses the class_name, method_return_type, and method_name to create the header for an instance method. This can
*    be used as a forward declaration before the implementation is written. In this case, this macro should be placed
*    above G_OBJC_CLASS_BEGIN in your library file. This macro can also be placed above a code block containing
*    implementation for the method.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] method_return_type The method return type.
*    \param [in] method_name The name of the member function.
*
*    \retval method_return_type The return type given.
*/
#define G_OBJC_HEADER_INSTANCE_METHOD(class_name, method_return_type, method_name) inline method_return_type class_name##Method##method_name(id self, SEL _cmd)

//! Makes the header for a G_OBJC class instance method with arguments.
/*!
*    Uses the class_name, method_return_type, method_name, and __VA_ARGS__ to create the header for an instance method
*    with arguments. This can be used as a forward declaration before the implementation is written. In this case,
*    this macro should be placed above G_OBJC_CLASS_BEGIN in your library file. This macro can also be placed above
*    a code block containing implementation for the method.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] method_return_type The method return type.
*    \param [in] method_name The name of the member function.
*    \param [in] __VA_ARGS__ Comma-seperated arguments with their types.
*
*    \retval method_return_type The return type given.
*/
#define G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(class_name, method_return_type, method_name, ...) inline method_return_type class_name##Method##method_name(id self, SEL _cmd, __VA_ARGS__)

//! Makes the header for a G_OBJC class property get method.
/*!
*    Uses the class_name, type_name, and property_name to create the header for a property get method. This can be
*    used as a forward declaration before the implementation is written. In this case, this macro should be placed
*    above G_OBJC_CLASS_BEGIN in your library file. This macro can also be placed above a code block containing
*    implementation for the method.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*
*    \retval type_name& A reference to the property.
*/
#define G_OBJC_HEADER_GET_PROPERTY_METHOD(class_name, type_name, property_name) G_OBJC_HEADER_INSTANCE_METHOD(class_name, type_name&, property_name)

//! Makes the header for a G_OBJC class property get method. Note: Use for properties that are pointers.
/*!
*    Uses the class_name, type_name, and property_name to create the header for a property get method. This can be
*    used as a forward declaration before the implementation is written. In this case, this macro should be placed
*    above G_OBJC_CLASS_BEGIN in your library file. This macro can also be placed above a code block containing
*    implementation for the method.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*
*    \retval type_name A pointer to the property.
*/
#define G_OBJC_HEADER_GET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name) G_OBJC_HEADER_INSTANCE_METHOD(class_name, type_name, property_name)

//! Makes the header for a G_OBJC class property set method.
/*!
*    Uses the class_name, type_name, and property_name to create the header for a property set method. This can be
*    used as a forward declaration before the implementation is written. In this case, this macro should be placed
*    above G_OBJC_CLASS_BEGIN in your library file. This macro can also be placed above a code block containing
*    implementation for the method.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_HEADER_SET_PROPERTY_METHOD(class_name, type_name, property_name) G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(class_name, void, Set##property_name, type_name newVal)

//! Makes the header for a G_OBJC class property set method. Note: Use for properties that are pointers.
/*!
*    Uses the class_name, type_name, and property_name to create the header for a property set method. This can be
*    used as a forward declaration before the implementation is written. In this case, this macro should be placed
*    above G_OBJC_CLASS_BEGIN in your library file. This macro can also be placed above a code block containing
*    implementation for the method.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_HEADER_SET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name) G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(class_name, void, Set##property_name, type_name newVal)

//! Defines the implementation for a G_OBJC class property get method.
/*!
*    Uses the class_name, type_name, and property_name to create the implementation for a property get method.
*    The method gets the property from the instance of the class and returns a reference to it. This macro must
*    be placed below G_OBJC_CLASS_END in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*
*    \retval type_name& A reference to the property.
*/
#define G_OBJC_IMPLEMENTATION_GET_PROPERTY_METHOD(class_name, type_name, property_name) G_OBJC_HEADER_GET_PROPERTY_METHOD(class_name, type_name, property_name)\
{\
    Ivar ivar = class_getInstanceVariable(G_OBJC_GET_CLASS(class_name), "_"#property_name);\
    CFTypeRef selfPtr = CFBridgingRetain(self);\
    type_name* varPtr = (type_name*)((uint8_t*)selfPtr + ivar_getOffset(ivar));\
    return *varPtr;\
}

//! Defines the implementation for a G_OBJC class property get method. Note: Use for properties that are pointers.
/*!
*    Uses the class_name, type_name, and property_name to create the implementation for a property get method.
*    The method gets the property from the instance of the class and returns a pointer to it. This macro must
*    be placed below G_OBJC_CLASS_END in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*
*    \retval type_name A pointer to the property.
*/
#define G_OBJC_IMPLEMENTATION_GET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name) G_OBJC_HEADER_GET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name)\
{\
    Ivar ivar = class_getInstanceVariable(G_OBJC_GET_CLASS(class_name), "_"#property_name);\
    return (type_name)object_getIvar(self, ivar);\
}

//! Defines the implementation for a G_OBJC class property set method.
/*!
*    Uses the class_name, type_name, and property_name to create the implementation for a property set method.
*    The method sets the property of the class instance. This macro must be placed below G_OBJC_CLASS_END
*    in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_IMPLEMENTATION_SET_PROPERTY_METHOD(class_name, type_name, property_name) G_OBJC_HEADER_SET_PROPERTY_METHOD(class_name, type_name, property_name)\
{\
    Ivar ivar = class_getInstanceVariable(G_OBJC_GET_CLASS(class_name), "_"#property_name);\
    CFTypeRef selfPtr = CFBridgingRetain(self);\
    type_name* varPtr = (type_name*)((uint8_t*)selfPtr + ivar_getOffset(ivar));\
    *varPtr = newVal;\
}

//! Defines the implementation for a G_OBJC class property set method. Note: Use for properties that are pointers.
/*!
*    Uses the class_name, type_name, and property_name to create the implementation for a property set method.
*    The method sets the pointer property of the class instance. This macro must be placed below G_OBJC_CLASS_END
*    in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_IMPLEMENTATION_SET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name) G_OBJC_HEADER_SET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name)\
{\
    Ivar ivar = class_getInstanceVariable(G_OBJC_GET_CLASS(class_name), "_"#property_name);\
    type_name oldVal = (type_name)object_getIvar(self, ivar);\
    if (oldVal != newVal)\
        object_setIvar(self, ivar, (id)newVal);\
}

//! Convenience macro that makes the get method and set method headers for a G_OBJC class property.
/*!
*    Uses the class_name, type_name, and property_name to create the headers for a property get method and set method.
*    This macro must be placed above G_OBJC_CLASS_BEGIN in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_HEADER_PROPERTY_METHODS(class_name, type_name, property_name)\
G_OBJC_HEADER_GET_PROPERTY_METHOD(class_name, type_name, property_name);\
G_OBJC_HEADER_SET_PROPERTY_METHOD(class_name, type_name, property_name)

//! Convenience macro that makes the get method and set method headers for a G_OBJC class property. Note: Use for properties that are pointers.
/*!
*    Uses the class_name, type_name, and property_name to create the headers for a property get method and set method.
*    This macro must be placed above G_OBJC_CLASS_BEGIN in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_HEADER_PROPERTY_METHODS_FOR_POINTER(class_name, type_name, property_name)\
G_OBJC_HEADER_GET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name);\
G_OBJC_HEADER_SET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name)

//! Convenience macro that defines the get method and set method implementations for a G_OBJC class property.
/*!
*    Uses the class_name, type_name, and property_name to create the implementations for a property get method and set method.
*    This macro must be placed below G_OBJC_CLASS_END in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_IMPLEMENTATION_PROPERTY_METHODS(class_name, type_name, property_name)\
G_OBJC_IMPLEMENTATION_GET_PROPERTY_METHOD(class_name, type_name, property_name);\
G_OBJC_IMPLEMENTATION_SET_PROPERTY_METHOD(class_name, type_name, property_name)

//! Convenience macro that defines the get method and set method implementations for a G_OBJC class property. Note: Use for properties that are pointers.
/*!
*    Uses the class_name, type_name, and property_name to create the implementations for a property get method and set method.
*    This macro must be placed below G_OBJC_CLASS_END in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] type_name The data member type.
*    \param [in] property_name The data member name.
*/
#define G_OBJC_IMPLEMENTATION_PROPERTY_METHODS_FOR_POINTER(class_name, type_name, property_name)\
G_OBJC_IMPLEMENTATION_GET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name);\
G_OBJC_IMPLEMENTATION_SET_PROPERTY_METHOD_FOR_POINTER(class_name, type_name, property_name)

// G_OBJC Class Usage:

//! Calls a G_OBJC method
/*!
*    Uses the class_name, class_instance, and method_name to call a G_OBJ function.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] class_instance An instance of the class.
*    \param [in] method_name The member function name.
*
*    \retval Depends on the return type of the function called.
*/
#define G_OBJC_CALL_METHOD(class_name, class_instance, method_name) class_name##Method##method_name(class_instance, @selector(method_name))

//! Calls a G_OBJC method with arguments.
/*!
*    Uses the class_name, class_instance, method_name, and __VA_ARGS__ to call a G_OBJ function.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] class_instance An instance of the class.
*    \param [in] method_name The member function name.
*    \param [in] __VA_ARGS__ Comma-seperated parameters.
*
*    \retval Depends on the return type of the function called.
*/
#define G_OBJC_CALL_METHOD_WITH_ARGUMENTS(class_name, class_instance, method_name, ...) class_name##Method##method_name(class_instance, @selector(method_name), __VA_ARGS__)

//! Calls the G_OBJC get method of a property.
/*!
*    Uses the class_name, class_instance, and property_name to call the G_OBJ get method of a property.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] class_instance An instance of the class.
*    \param [in] property_name The data member name.
*
*    \retval Reference(&) A reference to the property. Note: If the property was created with a G_OBJC macro ending with _PROPERTY_METHOD.
*    \retval Pointer(*) A pointer to the property. Note: If the property was created with a G_OBJC macro ending with _PROPERTY_METHOD_FOR_POINTER.
*/
#define G_OBJC_GET_PROPERTY(class_name, class_instance, property_name) class_name##Method##property_name(class_instance, @selector(property_name))

//! Calls the G_OBJC set method of a property.
/*!
*    Uses the class_name, class_instance, property_name, and new_value to call the G_OBJ set method of a property.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] class_instance An instance of the class.
*    \param [in] property_name The data member name.
*    \param [in] new_value The new value for the property. Note: If the property was created with a G_OBJC macro ending with _PROPERTY_METHOD.
*    \param [in] new_value The new place in memory for the property to point to. Note: If the property was created with a G_OBJC macro ending with _PROPERTY_METHOD_FOR_POINTER.
*/
#define G_OBJC_SET_PROPERTY(class_name, class_instance, property_name, new_value) class_name##MethodSet##property_name(class_instance, @selector(property_name), new_value)

// G_OBJC Class Data Members:

//! Gets the data members type name for the G_OBJC class
/*!
*    Uses the class_name to create the type name for the data members struct.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*/
#define G_OBJC_DATA_MEMBERS_TYPE(class_name) class_name##DataMembers

//! Creates the data members struct header for the G_OBJC class.
/*!
*    Uses the class_name to create the struct header. A code block with a list of data members should immediately follow this macro in your library file.
*    This macro must be placed above G_OBJC_CLASS_BEGIN in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*/
#define G_OBJC_DATA_MEMBERS_STRUCT(class_name) struct class_name##DataMembers

//! Creates a data members property and its get method for the second part (2 of 3) of the G_OBJC_GET_CLASS function implementation.
/*!
*    Uses the class_name to add a data members property to the Objective-C Runtime Library class. It also adds both the get method for the property to
*    the class. The data members property type is equal to the expansion of G_OBJC_DATA_MEMBERS_TYPE. The data members the property contains is a result
*    of the data members added in the code block below G_OBJC_DATA_MEMBERS_STRUCT.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*/
#define G_OBJC_CLASS_DATA_MEMBERS_PROPERTY(class_name) G_OBJC_CLASS_ADD_PROPERTY(class_name, G_OBJC_DATA_MEMBERS_TYPE(class_name), dataMembersOf##class_name);\
G_OBJC_CLASS_GET_PROPERTY_METHOD(class_name, dataMembersOf##class_name)

//! Makes the header for a G_OBJC class data members property get method.
/*!
*    Uses the class_name to create the header for a data members property get method. This should be used as a forward declaration before the
*    implementation is written. This macro should be placed above G_OBJC_CLASS_BEGIN in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*
*    \retval G_OBJC_DATA_MEMBERS_TYPE& A reference to the data members property.
*/
#define G_OBJC_HEADER_DATA_MEMBERS_PROPERTY_METHOD(class_name)\
G_OBJC_HEADER_GET_PROPERTY_METHOD(class_name, G_OBJC_DATA_MEMBERS_TYPE(class_name), dataMembersOf##class_name)

//! Defines the implementation for a G_OBJC class data members property get method.
/*!
*    Uses the class_name to create the implementation for a data members property get method. The method gets the data members property from
*    the instance of the class and returns a reference to it. This macro must be placed below G_OBJC_CLASS_END in your library file.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*
*    \retval G_OBJC_DATA_MEMBERS_TYPE& A reference to the data members property.
*/
#define G_OBJC_IMPLEMENTATION_DATA_MEMBERS_PROPERTY_METHOD(class_name)\
G_OBJC_IMPLEMENTATION_GET_PROPERTY_METHOD(class_name, G_OBJC_DATA_MEMBERS_TYPE(class_name), dataMembersOf##class_name)

//! Calls the G_OBJC get method of a data members property.
/*!
*    Uses the class_name and class_instance to call the G_OBJ get method of the data members property.
*
*    \param [in] class_name The Objective-C Runtime Library class name.
*    \param [in] class_instance An instance of the class.
*
*    \retval G_OBJC_DATA_MEMBERS_TYPE& A reference to the data members property.
*/
#define G_OBJC_GET_DATA_MEMBERS(class_name, class_instance) G_OBJC_GET_PROPERTY(class_name, class_instance, dataMembersOf##class_name)

// G_OBJC MACROS END


// Thanks to Chris for this little snippet and reminding everyone the proper way to interact with the apple UI layer
// https://chritto.wordpress.com/2012/12/20/updating-the-ui-from-another-thread/
inline void RUN_ON_UI_THREAD(dispatch_block_t block)
{
    if ([NSThread isMainThread])
        block();
    else
        dispatch_sync(dispatch_get_main_queue(), block);
}

#endif // #endif TARGET_OS_IOS
#endif // #endif __APPLE__
#endif // #endif GATEWARE_IOSUTILS_H
