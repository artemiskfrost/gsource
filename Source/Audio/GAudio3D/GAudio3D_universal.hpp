namespace GW
{
	namespace I
	{
		class GAudio3DImplementation :	public virtual GAudio3DInterface,
										public GAudioImplementation
		{
		public:
			CORE::GEventGenerator gEventGen;
			GReturn Create()
			{
				GReturn result = gEventGen.Create();
				if (result != GReturn::SUCCESS)
					return result;

				return GAudioImplementation::Create();
			}

			GReturn Update3DListener(GW::MATH::GVECTORF _position, GW::MATH::GQUATERNIONF _orientation) override
			{
				MATH::GMATRIXF listener = MATH::GIdentityMatrixF;
				MATH::GMATRIXF transform = MATH::GIdentityMatrixF;
				MATH::GQUATERNIONF orientation{};
				orientation.x = _orientation.x;
				orientation.y = _orientation.y;
				orientation.z = _orientation.z;
				orientation.w = _orientation.w;

				MATH::GMatrix::ConvertQuaternionF(orientation, listener);

				listener.row4.x = _position.x;
				listener.row4.y = _position.y;
				listener.row4.z = _position.z;
				listener.row4.w = 1.0f;

				if (MATH::GMatrix::InverseF(listener, transform) != GReturn::SUCCESS)
					return GReturn::FAILURE;
				
				GEvent gEvent;
				EVENT_DATA eventData{};
				eventData.position = listener.row4;
				eventData.quaternion = _orientation;
				gEvent.Write(Events::UPDATE_LISTENER, eventData);
				gEventGen.Push(gEvent);

				eventData.position = transform.row4;
				MATH::GQuaternion::SetByMatrixF(transform, eventData.quaternion);
				gEvent.Write(Events::UPDATE_TRANSFORM_AND_SPATIALIZE, eventData);

				return gEventGen.Push(gEvent);
			}
		};
	}
}