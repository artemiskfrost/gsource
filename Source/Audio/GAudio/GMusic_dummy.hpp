namespace GW
{
    namespace I
    {
        class GMusicImplementation : public virtual GMusicInterface
        {
        public:
            // Main class
            GReturn Create(const char* _path, GW::AUDIO::GAudio _audio, float _value = 1.0f)
            {
                return GReturn::INTERFACE_UNSUPPORTED;
            }
            GReturn SetChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
                return GReturn::FAILURE;
            }
            GReturn SetVolume(float _newVolume) override
            {
                return GReturn::FAILURE;
            }
            GReturn Play(bool _loop = false) override
            {
                return GReturn::FAILURE;
            }
            GReturn Pause() override
            {
                return GReturn::FAILURE;
            }
            GReturn Resume() override
            {
                return GReturn::FAILURE;
            }
            GReturn Stop() override
            {
                return GReturn::FAILURE;
            }
            GReturn GetSourceChannels(unsigned int& returnedChannelNum) const override
            {
                return GReturn::FAILURE;
            }
            GReturn GetOutputChannels(unsigned int& returnedChannelNum) const override
            {
                return GReturn::FAILURE;
            }
            GReturn isPlaying(bool& _returnedBool) const override
            {
                return GReturn::FAILURE;
            }
        };
    }
}
