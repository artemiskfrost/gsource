namespace GW
{
    namespace I
    {
        class GAudioImplementation :    public virtual GAudioInterface,
                                        public GEventGeneratorImplementation
        {
        public:
            // Main class
            GReturn Create()
            {
                return GReturn::INTERFACE_UNSUPPORTED;
            }
            GReturn SetMasterVolume(float _value) override
            {
                return GReturn::FAILURE;
            }
            GReturn SetGlobalSoundVolume(float _value) override
            {
                return GReturn::FAILURE;
            }
            GReturn SetGlobalMusicVolume(float _value) override
            {
                return GReturn::FAILURE;
            }
            GReturn SetSoundsChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
                return GReturn::FAILURE;
            }
            GReturn SetMusicChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
                return GReturn::FAILURE;
            }
            GReturn PlaySounds() override
            {
                return GReturn::FAILURE;
            }
            GReturn PauseSounds() override
            {
                return GReturn::FAILURE;
            }
            GReturn ResumeSounds() override
            {
                return GReturn::FAILURE;
            }
            GReturn StopSounds() override
            {
                return GReturn::FAILURE;
            }
            GReturn PlayMusic() override
            {
                return GReturn::FAILURE;
            }
            GReturn PauseMusic() override
            {
                return GReturn::FAILURE;
            }
            GReturn ResumeMusic() override
            {
                return GReturn::FAILURE;
            }
            GReturn StopMusic() override
            {
                return GReturn::FAILURE;
            }
        };
    }
}
