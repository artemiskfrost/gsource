#define G_NUM_OF_OUTPUTS 6
#define G_STREAMING_BUFFER_SIZE 63553
#define G_MAX_BUFFER_COUNT 3

#include "../../Shared/macutils.h"

namespace GW
{
    namespace I
    {
        class GMusicImplementation;
    }
}

namespace internal_gw
{
    // GMacMusic Interface

    // Data members of GMacMusic
    G_OBJC_DATA_MEMBERS_STRUCT(GMacMusic)
    {
        int currentBufferIndex;
        int index;

        std::atomic<int> buffersQueued;
        std::atomic<bool> stopFlag;
        std::atomic<bool> loops;
        std::atomic<bool> isPlaying;
        std::atomic<bool> isPaused;

        AVAudioFrameCount CurrentPosition;
        AVAudioFrameCount MaxPosition;

        GW::I::GMusicImplementation* gMusic;
        GMacAudio* audio;

        AVAudioPlayerNode* player;

        AVAudioUnit* matrixMixerNode;
        AVAudioFormat* audioFormat;

        AVAudioFile* file;
        AVAudioPCMBuffer* buffers[G_MAX_BUFFER_COUNT];
        NSThread* scheduler;
        NSCondition* bufferCondition;
    };

    // Forward declarations of GMacMusic methods
    G_OBJC_HEADER_DATA_MEMBERS_PROPERTY_METHOD(GMacMusic);

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacMusic, id, initWithPath, NSString* _path);
    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, unsigned int, GetChannels);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacMusic, bool, SetChannelVolumes, float* _volumes, unsigned int _numChannels);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacMusic, bool, SetVolume, float _newVolume);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacMusic, bool, Play, bool _loops);
    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, bool, Pause);
    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, bool, Resume);
    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, bool, Stop);
    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, void, Stream);
    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, bool, Unload);

    // Creates the GMacMusic class at runtime when G_OBJC_GET_CLASS(GMacMusic) is called.
    G_OBJC_CLASS_BEGIN(GMacMusic, NSObject)
    {
        G_OBJC_CLASS_DATA_MEMBERS_PROPERTY(GMacMusic);

        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GMacMusic, initWithPath, "@@:@", :);
        G_OBJC_CLASS_METHOD(GMacMusic, GetChannels, "I@:");
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GMacMusic, SetChannelVolumes, "B@:@I", ::);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GMacMusic, SetVolume, "B@:f", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GMacMusic, Play, "B@:B", :);
        G_OBJC_CLASS_METHOD(GMacMusic, Pause, "B@:");
        G_OBJC_CLASS_METHOD(GMacMusic, Resume, "B@:");
        G_OBJC_CLASS_METHOD(GMacMusic, Stop, "B@:");
        G_OBJC_CLASS_METHOD(GMacMusic, Stream, "v@:");
        G_OBJC_CLASS_METHOD(GMacMusic, Unload, "B@:");
    }
    G_OBJC_CLASS_END(GMacMusic)

    // GMacMusic Interface End
}

namespace GW
{
    namespace I
    {
        class GMusicImplementation : public virtual GMusicInterface,
            protected GThreadSharedImplementation
        {
            float masterVolume = 1.0f; // global master volume
            float globalMusicVolume = 1.0f; // global sounds volume
            float volume = 1.0f; // volume of this sound

            float       channelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // channel volumes of this sound
            float masterChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }; // global master volumes
            id mac_msc = nullptr;
            GW::AUDIO::GAudio gAudio;
            GW::CORE::GEventReceiver gReceiver;

            void Destroy()
            {
                LockSyncWrite();
                if (mac_msc)
                {
                    internal_gw::G_OBJC_CALL_METHOD(GMacMusic, mac_msc, Unload);
                    mac_msc = nullptr;
                }
                UnlockSyncWrite();
            }
        public:
            GW::SYSTEM::GConcurrent gConcurrent;
            virtual ~GMusicImplementation()
            {
                // Objective C cleanup
                Destroy();
            }

            GReturn Create(const char* _path, GW::AUDIO::GAudio _audio, float _volume = 1.0f)
            {
                if (!_path || !_audio)
                    return GReturn::INVALID_ARGUMENT;

                if (_volume < 0.0f || _volume > 1.0f)
                    return GReturn::INVALID_ARGUMENT;

                gAudio = _audio;
                auto audioImplementation = std::dynamic_pointer_cast<GW::I::GAudioImplementation>(*_audio);

                mac_msc = [internal_gw::G_OBJC_GET_CLASS(GMacMusic) alloc];

                internal_gw::G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& mac_mscDataMembers = internal_gw::G_OBJC_GET_DATA_MEMBERS(GMacMusic, mac_msc);
                mac_mscDataMembers.gMusic = this;
                mac_mscDataMembers.audio = audioImplementation->mac_audio;

                NSString* nsPath = [[[NSString alloc]initWithUTF8String:_path] autorelease];
                internal_gw::G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GMacMusic, mac_msc, initWithPath, nsPath);

                globalMusicVolume = audioImplementation->musicVolume;
                masterVolume = audioImplementation->masterVolume;
                memcpy(masterChannelVolumes, audioImplementation->musicChannelVolumes, 6 * sizeof(float));

                GReturn result = SetVolume(_volume);
                if (result != GReturn::SUCCESS)
                    return result;

                result = SetChannelVolumes(channelVolumes, 6);
                if (result != GReturn::SUCCESS)
                    return result;

                result = GThreadSharedImplementation::Create();
                if (result != GReturn::SUCCESS)
                    return result;

                result = gConcurrent.Create(true);
                if (result != GReturn::SUCCESS) // Events are suppressed
                    return result;

                return gReceiver.Create(_audio, [&]()
                {
                    GW::GEvent gEvent;
                    GW::AUDIO::GAudio::Events audioEvent;
                    GW::AUDIO::GAudio::EVENT_DATA audioEventData;
                    // Process the event message
                    gReceiver.Pop(gEvent);
                    gEvent.Read(audioEvent);

                    switch (audioEvent)
                    {
                    case GW::AUDIO::GAudio::Events::DESTROY:
                    {
                        //printf("DESTROY RECEIVED IN MUSIC\n");
                        // If GAudio is destroyed, the sound is no longer operational
                        Destroy();
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::PLAY_MUSIC:
                    {
                        Play();
                        //bool playing;
                        //isPlaying(playing);
                        //printf("MUSIC PLAY %d\n", playing);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::PAUSE_MUSIC:
                    {
                        Pause();
                        //bool playing;
                        //isPlaying(playing);
                        //printf("MUSIC PAUSE %d\n", !playing);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::RESUME_MUSIC:
                    {
                        Resume();
                        //bool playing;
                        //isPlaying(playing);
                        //printf("MUSIC RESUME %d\n", playing);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::STOP_MUSIC:
                    {
                        Stop();
                        //bool playing;
                        //isPlaying(playing);
                        //printf("MUSIC STOP: %d\n", !playing);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::MASTER_VOLUME_CHANGED:
                    {
                        gEvent.Read(audioEventData);
                        masterVolume = audioEventData.channelVolumes[0];
                        // Update the current volume with a new master volume
                        SetVolume(volume);
                        //printf("MASTER_VOLUME: %f | VOLUME: %f\n", masterVolume, volume * globalMusicVolume * masterVolume);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::MUSIC_VOLUME_CHANGED:
                    {
                        gEvent.Read(audioEventData);
                        globalMusicVolume = audioEventData.channelVolumes[0];
                        // Update the current volume with a new master volume
                        SetVolume(volume);
                        //printf("GLOBAL MUSIC VOLUME: %f | VOLUME: %f\n", globalMusicVolume, volume * globalMusicVolume * masterVolume);
                        break;
                    }
                    case GW::AUDIO::GAudio::Events::MUSIC_CHANNEL_VOLUMES_CHANGED:
                    {
                        gEvent.Read(audioEventData);
                        memcpy(masterChannelVolumes, audioEventData.channelVolumes, audioEventData.numOfChannels * sizeof(float));
                        SetChannelVolumes(channelVolumes, audioEventData.numOfChannels);
                        //printf("MUSIC CHANNEL VOLUMES: { %f, %f, %f, %f, %f, %f }\n", channelVolumes[0] * masterChannelVolumes[0], channelVolumes[1] * masterChannelVolumes[1], channelVolumes[2] * masterChannelVolumes[2], channelVolumes[3] * masterChannelVolumes[3], channelVolumes[4] * masterChannelVolumes[4], channelVolumes[5] * masterChannelVolumes[5]);
                        break;
                    }
                    default:
                    {
                        break;
                    }
                    }
                });
            }

            GReturn SetChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_numChannels == 0 || _numChannels > 6 || _values == nullptr)
                    return GReturn::INVALID_ARGUMENT;

                float resultChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
                for (unsigned int i = 0; i < _numChannels; i++)
                {
                    if (_values[i] < 0.0f)
                        return GReturn::INVALID_ARGUMENT;

                    // apply clamping
                    channelVolumes[i] = (_values[i] > 1.0f) ? 1.0f : _values[i];
                    // apply master channel volumes
                    resultChannelVolumes[i] = channelVolumes[i] * masterChannelVolumes[i];
                }

                bool result = internal_gw::G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GMacMusic, mac_msc, SetChannelVolumes, resultChannelVolumes, _numChannels);

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn SetVolume(float _newVolume) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                if (_newVolume < 0.0f)
                    return GReturn::INVALID_ARGUMENT;

                // Clip the passed volume to max
                volume = (_newVolume > 1.0f) ? 1.0f : _newVolume;

                // Apply master volume ratios to the music volume (Doesn't need to be normalized, since result is always < 1.0f)
                bool result = internal_gw::G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GMacMusic, mac_msc, SetVolume, volume * globalMusicVolume * masterVolume);

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn Play(bool _loop = false) override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                internal_gw::G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& mac_mscDataMembers = internal_gw::G_OBJC_GET_DATA_MEMBERS(GMacMusic, mac_msc);
                if (mac_mscDataMembers.isPlaying == true)
                {
                    if (-Stop())
                        return GReturn::FAILURE;
                }

                if (internal_gw::G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GMacMusic, mac_msc, Play, _loop) == false)
                    return GReturn::FAILURE;


                gConcurrent.BranchSingular([&]()
                {
                    if (mac_msc)
                        internal_gw::G_OBJC_CALL_METHOD(GMacMusic, mac_msc, Stream);
                    else return;
                });

                return GReturn::SUCCESS;
            }

            GReturn Pause() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                bool result = internal_gw::G_OBJC_CALL_METHOD(GMacMusic, mac_msc, Pause);

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn Resume() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                bool result = internal_gw::G_OBJC_CALL_METHOD(GMacMusic, mac_msc, Resume);

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn Stop() override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                bool result = internal_gw::G_OBJC_CALL_METHOD(GMacMusic, mac_msc, Stop);

                return result == true ? GReturn::SUCCESS : GReturn::FAILURE;
            }

            GReturn GetSourceChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = internal_gw::G_OBJC_CALL_METHOD(GMacMusic, mac_msc, GetChannels);
                return GReturn::SUCCESS;
            }

            GReturn GetOutputChannels(unsigned int& returnedChannelNum) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                returnedChannelNum = G_NUM_OF_OUTPUTS;
                return GReturn::SUCCESS;
            }

            GReturn isPlaying(bool& _returnedBool) const override
            {
                if (!gAudio)
                    return GReturn::PREMATURE_DEALLOCATION;

                internal_gw::G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& mac_mscDataMembers = internal_gw::G_OBJC_GET_DATA_MEMBERS(GMacMusic, mac_msc);
                _returnedBool = mac_mscDataMembers.isPlaying;
                return GReturn::SUCCESS;
            }

            // ThreadShared
            GReturn LockAsyncRead() const override
            {
                return GThreadSharedImplementation::LockAsyncRead();
            }

            GReturn UnlockAsyncRead() const override
            {
                return GThreadSharedImplementation::UnlockAsyncRead();
            }

            GReturn LockSyncWrite() override
            {
                return GThreadSharedImplementation::LockSyncWrite();
            }

            GReturn UnlockSyncWrite() override
            {
                return GThreadSharedImplementation::UnlockSyncWrite();
            }
        };
    }// end I
}// end GW

namespace internal_gw
{
    // GMacMusic Implementation

    G_OBJC_IMPLEMENTATION_DATA_MEMBERS_PROPERTY_METHOD(GMacMusic);

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacMusic, id, initWithPath, NSString* _path)
    {
        self = [self init];

        if (self)
        {
            G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);
            selfDataMembers.player = [[AVAudioPlayerNode alloc]init];
            [selfDataMembers.audio->engine attachNode : selfDataMembers.player] ;

            NSURL* filePath = [[NSURL alloc]initFileURLWithPath:_path];
            selfDataMembers.file = [[AVAudioFile alloc]initForReading:filePath commonFormat : AVAudioPCMFormatFloat32 interleaved : false error : nil];
            [filePath release] ;
            selfDataMembers.buffers[0] = [[AVAudioPCMBuffer alloc]initWithPCMFormat:[selfDataMembers.file processingFormat] frameCapacity : G_STREAMING_BUFFER_SIZE];
            selfDataMembers.buffers[1] = [[AVAudioPCMBuffer alloc]initWithPCMFormat:[selfDataMembers.file processingFormat] frameCapacity : G_STREAMING_BUFFER_SIZE];
            selfDataMembers.buffers[2] = [[AVAudioPCMBuffer alloc]initWithPCMFormat:[selfDataMembers.file processingFormat] frameCapacity : G_STREAMING_BUFFER_SIZE];

            AudioComponentDescription mixerDesc;
            mixerDesc.componentType = kAudioUnitType_Mixer;
            mixerDesc.componentSubType = kAudioUnitSubType_MatrixMixer;
            mixerDesc.componentManufacturer = kAudioUnitManufacturer_Apple;
            mixerDesc.componentFlags = kAudioComponentFlag_SandboxSafe;
            mixerDesc.componentFlagsMask = 0;

            selfDataMembers.matrixMixerNode = nullptr;
            [AVAudioUnit instantiateWithComponentDescription : mixerDesc options : kAudioComponentInstantiation_LoadInProcess completionHandler :
            ^ (__kindof AVAudioUnit * _Nullable mixerUnit, NSError * _Nullable error)
            {
                selfDataMembers.matrixMixerNode = mixerUnit;
                [selfDataMembers.audio->engine attachNode : selfDataMembers.matrixMixerNode] ;
            }] ;

            while (!selfDataMembers.matrixMixerNode)
            {
                usleep(100); // waiting for the completionHandler to finish
            }

            // Give the mixer one input bus and one output bus
            UInt32 inBuses = 1;
            UInt32 outBuses = 1;
            AudioUnitSetProperty(selfDataMembers.matrixMixerNode.audioUnit, kAudioUnitProperty_ElementCount, kAudioUnitScope_Input, 0, &inBuses, sizeof(UInt32));
            AudioUnitSetProperty(selfDataMembers.matrixMixerNode.audioUnit, kAudioUnitProperty_ElementCount, kAudioUnitScope_Output, 0, &outBuses, sizeof(UInt32));

            // Set the mixer's input format to have the correct number of channels
            AudioStreamBasicDescription mixerFormatIn;
            UInt32 size;
            AudioUnitGetProperty(selfDataMembers.matrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &mixerFormatIn, &size);
            mixerFormatIn.mChannelsPerFrame = G_NUM_OF_OUTPUTS;
            AudioUnitSetProperty(selfDataMembers.matrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &mixerFormatIn, size);

            // Set the mixer's output format to have the correct number of channels
            AudioStreamBasicDescription mixerFormatOut;
            AudioUnitGetProperty(selfDataMembers.matrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, &mixerFormatOut, &size);
            mixerFormatOut.mChannelsPerFrame = G_NUM_OF_OUTPUTS;
            AudioUnitSetProperty(selfDataMembers.matrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, &mixerFormatOut, size);

            AVAudioFormat* audioFormat;
            if (selfDataMembers.buffers[0].format.channelCount == 1) // for mono sounds
                audioFormat = [[AVAudioFormat alloc]
                initWithCommonFormat:AVAudioPCMFormatFloat32 sampleRate : selfDataMembers.buffers[0].format.sampleRate channels : 1 interleaved : false];
            // Attempt to use more than 2 channels instead of default format
            else
                audioFormat = [[AVAudioFormat alloc]
                initWithCommonFormat:AVAudioPCMFormatFloat32 sampleRate : selfDataMembers.buffers[0].format.sampleRate channels : G_NUM_OF_OUTPUTS interleaved : false];

            [selfDataMembers.audio->engine connect : selfDataMembers.player to : selfDataMembers.audio->engine.mainMixerNode format : audioFormat] ;
            [audioFormat release] ;

            selfDataMembers.bufferCondition = [[NSCondition alloc]init];
            selfDataMembers.CurrentPosition = 0;
            selfDataMembers.MaxPosition = 0;
            selfDataMembers.currentBufferIndex = 0;
            selfDataMembers.index = 0;
            selfDataMembers.buffersQueued = 0;
            selfDataMembers.stopFlag = false;
            selfDataMembers.loops = false;
            selfDataMembers.isPlaying = false;
            selfDataMembers.isPaused = false;
            selfDataMembers.MaxPosition = selfDataMembers.file.length;
        }

        return self;
    }

    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, unsigned int, GetChannels)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);

        if (!selfDataMembers.player)
            return 0;

        return[selfDataMembers.file processingFormat].channelCount;
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacMusic, bool, SetChannelVolumes, float* _volumes, unsigned int _numChannels)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);

        if (!selfDataMembers.player)
            return false;

        if (_numChannels > 1)
        {
            float newVal = 0.0f;
            float sumChannels = 0.0f;
            for (int i = 0; i < _numChannels; i++)
            {
                // total of all channels
                sumChannels += _volumes[i];
                // channels 0 and 4 are left channels which have pan of -1
                if (i == 0 || i == 4)
                    newVal += _volumes[i] * -1.0f;
                // skip 2 and 3, since center channels have pan of 0
                else if (i == 1 || i == 5)
                    newVal += _volumes[i];
            }
            selfDataMembers.gMusic->LockSyncWrite();
            // obtain average and set the panning (sum has to be greater than 1 to apply clamping)
            selfDataMembers.player.pan = (sumChannels > 1.0f) ? (newVal / sumChannels) : newVal;
            selfDataMembers.gMusic->UnlockSyncWrite();
        }
        else
        {
            selfDataMembers.gMusic->LockSyncWrite();
            selfDataMembers.player.pan = -1 * _volumes[0];
            selfDataMembers.gMusic->UnlockSyncWrite();
        }

        return true;
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacMusic, bool, SetVolume, float _newVolume)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);

        if (!selfDataMembers.player)
            return false;

        selfDataMembers.gMusic->LockSyncWrite();
        [selfDataMembers.player setVolume : (_newVolume)] ;
        float check = [selfDataMembers.player volume];
        selfDataMembers.gMusic->UnlockSyncWrite();

        if (check == _newVolume)
            return true;

        return false;
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GMacMusic, bool, Play, bool _loops)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);

        if (!selfDataMembers.player)
            return false;

        selfDataMembers.loops = _loops;
        if (selfDataMembers.isPlaying == false)
        {
            selfDataMembers.CurrentPosition = 0;
            selfDataMembers.file.framePosition = 0;
            [selfDataMembers.player playAtTime : 0] ;
        }

        selfDataMembers.isPlaying = true;
        selfDataMembers.isPaused = false;
        selfDataMembers.stopFlag = false;

        return[selfDataMembers.player isPlaying];
    }

    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, bool, Pause)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);

        if (!selfDataMembers.player)
            return false;

        [selfDataMembers.player pause] ;
        selfDataMembers.isPlaying = false;
        selfDataMembers.isPaused = true;

        return ![selfDataMembers.player isPlaying];
    }

    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, bool, Resume)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);

        if (!selfDataMembers.player)
            return false;

        if (selfDataMembers.isPaused == true)
            [selfDataMembers.player play];

        selfDataMembers.isPlaying = true;
        selfDataMembers.isPaused = false;

        return[selfDataMembers.player isPlaying];
    }

    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, bool, Stop)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);

        if (!selfDataMembers.player)
            return false;

        selfDataMembers.stopFlag = true;
        [selfDataMembers.player stop] ;
        [selfDataMembers.player prepareWithFrameCount : G_STREAMING_BUFFER_SIZE] ;
        // Reset to the start of the sample
        selfDataMembers.CurrentPosition = 0;
        selfDataMembers.buffersQueued = 0;

        selfDataMembers.isPlaying = false;
        selfDataMembers.isPaused = false;

        selfDataMembers.gMusic->gConcurrent.Converge(0);
        return ![selfDataMembers.player isPlaying];
    }

    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, void, Stream)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);

        while (selfDataMembers.stopFlag == false)
        {
            NSError* testError = nil;
            AVAudioFrameCount valid = MIN(G_STREAMING_BUFFER_SIZE, selfDataMembers.MaxPosition - selfDataMembers.CurrentPosition > 0 ? selfDataMembers.MaxPosition - selfDataMembers.CurrentPosition : selfDataMembers.CurrentPosition);
            selfDataMembers.CurrentPosition += valid;

            AVAudioPCMBuffer* currentBuffer = selfDataMembers.buffers[selfDataMembers.currentBufferIndex];
            [selfDataMembers.file readIntoBuffer : currentBuffer frameCount : valid error : &testError] ;

            [selfDataMembers.bufferCondition lock] ;
            while (selfDataMembers.stopFlag == false && selfDataMembers.buffersQueued >= 2)
            {
                //sleep(1); // slows down the thread during waiting time
                [selfDataMembers.bufferCondition wait] ;
            }
            ++selfDataMembers.buffersQueued;
            [selfDataMembers.bufferCondition unlock] ;


            [selfDataMembers.player scheduleBuffer : currentBuffer completionHandler : ^ {
                [selfDataMembers.bufferCondition lock] ;
                --selfDataMembers.buffersQueued;
                if (selfDataMembers.stopFlag == true || selfDataMembers.buffersQueued < 2)
                    [selfDataMembers.bufferCondition signal];
                [selfDataMembers.bufferCondition unlock] ;
            }] ;

            if (++selfDataMembers.currentBufferIndex >= G_MAX_BUFFER_COUNT)
                selfDataMembers.currentBufferIndex = 0;

            //This following code is similar to needed code down below in an attempt to stop
            if (valid != G_STREAMING_BUFFER_SIZE)
            {
                if (selfDataMembers.loops)
                {
                    selfDataMembers.CurrentPosition = 0;
                    selfDataMembers.file.framePosition = 0;
                }
            }

            //checks if we are at end of the song
            if (selfDataMembers.CurrentPosition >= selfDataMembers.MaxPosition)
            {
                //if looping reset file pointer and our current position
                if (selfDataMembers.loops)
                {
                    selfDataMembers.CurrentPosition = 0;
                    selfDataMembers.file.framePosition = 0;
                }
                else if (selfDataMembers.buffersQueued == 0 && selfDataMembers.stopFlag == false)
                {
                    G_OBJC_CALL_METHOD(GMacMusic, self, Stop);
                    return;
                }
            }
        }
    }

    G_OBJC_HEADER_INSTANCE_METHOD(GMacMusic, bool, Unload)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GMacMusic)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GMacMusic, self);

        bool macresult = false;
        if (selfDataMembers.player != nil)
        {
            G_OBJC_CALL_METHOD(GMacMusic, self, Stop);
            [selfDataMembers.player release] ;
            selfDataMembers.player = nil;
            macresult = true;
        }

        // Cleans up memory in the autorelease pool
        [self autorelease] ;
        return macresult;
    }

    // GMacMusic Implementation End
}

#undef G_NUM_OF_OUTPUTS
#undef G_STREAMING_BUFFER_SIZE
#undef G_MAX_BUFFER_COUNT
