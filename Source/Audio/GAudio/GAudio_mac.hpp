#ifdef __OBJC__
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVFAudio.h>
#import <AudioToolbox/AudioToolbox.h>
#endif

#include <atomic>
#include <math.h>
#define G_NUM_OF_OUTPUTS 6

namespace internal_gw
{
    class GMacAudio
    {
    public:
        AVAudioEngine* engine;
        AVAudioPlayerNode* player;
        AVAudioUnit* mainMatrixMixerNode;


        bool Init()
        {
            bool result = false;

            engine = [[AVAudioEngine alloc]init];
            player = [[AVAudioPlayerNode alloc]init];
            [engine attachNode : player] ;
             
            
            AudioComponentDescription mixerDesc;
            mixerDesc.componentType = kAudioUnitType_Mixer;
            mixerDesc.componentSubType = kAudioUnitSubType_MatrixMixer;
            mixerDesc.componentManufacturer = kAudioUnitManufacturer_Apple;
            mixerDesc.componentFlags = kAudioComponentFlag_SandboxSafe;
            mixerDesc.componentFlagsMask = 0;

            mainMatrixMixerNode = nullptr;
            [AVAudioUnit instantiateWithComponentDescription : mixerDesc options : kAudioComponentInstantiation_LoadInProcess completionHandler :
            ^ (__kindof AVAudioUnit * _Nullable mixerUnit, NSError * _Nullable error)
            {
                mainMatrixMixerNode = mixerUnit;
                [engine attachNode : mainMatrixMixerNode] ;
            }] ;

            while (!mainMatrixMixerNode)
            {
                usleep(100); // waiting for the completionHandler to finish
            }

            /* NOTE: This code currently does not do anything. */
            /* It is supposed to adjust the output channels to 6 but fails. */
            /* The mixer and buses create ok but the channel setting code fails*/
            /*
            // Give the mixer one input bus and one output bus
            UInt32 inBuses = 1;
            UInt32 outBuses = 1;
            OSStatus os =
            AudioUnitSetProperty(mainMatrixMixerNode.audioUnit, kAudioUnitProperty_ElementCount, kAudioUnitScope_Input, 0, &inBuses, sizeof(UInt32));
            if (os != errSecSuccess)
                return result;
            os = AudioUnitSetProperty(mainMatrixMixerNode.audioUnit, kAudioUnitProperty_ElementCount, kAudioUnitScope_Output, 0, &outBuses, sizeof(UInt32));
            if (os != errSecSuccess)
                return result;
            
            // Set the mixer's input format to have the correct number of channels
            UInt32 size;
            AudioStreamBasicDescription mixerFormatIn;
            os = AudioUnitGetProperty(mainMatrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &mixerFormatIn, &size);
            if (os != errSecSuccess)
                return result;
            
            mixerFormatIn.mChannelsPerFrame = G_NUM_OF_OUTPUTS;
            os = AudioUnitSetProperty(mainMatrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &mixerFormatIn, size);
            if (os != errSecSuccess)
                return result;
            
            // Set the mixer's output format to have the correct number of channels
            AudioStreamBasicDescription mixerFormatOut;
            os = AudioUnitGetProperty(mainMatrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, &mixerFormatOut, &size);
            if (os != errSecSuccess)
                return result;
            
            mixerFormatOut.mChannelsPerFrame = G_NUM_OF_OUTPUTS;
            os = AudioUnitSetProperty(mainMatrixMixerNode.audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, &mixerFormatOut, size);
            if (os != errSecSuccess)
                return result;
            */
            [engine connect : player to : engine.mainMixerNode format : nil] ;
            [engine connect : engine.mainMixerNode to : engine.outputNode format : nil] ;
            
            [engine startAndReturnError : nil] ;

            return result = engine.isRunning;
        }

        bool Unload()
        {
            // Detaches AVplayer from the AVengine
            [engine disconnectNodeInput : player] ;
            [engine detachNode : player] ;

            [player release] ;
            player = nil;

            // Cleans up memory
            if (engine != nil)
            {
                [engine release] ;
                engine = nil;
            }

            return true;
        }
    };
}

namespace GW
{
    namespace I
    {
        class GAudioImplementation :    public virtual GAudioInterface,
                                        public GEventGeneratorImplementation
        {
        public:
            float masterVolume = 1.0f;
            float soundsVolume = 1.0f;
            float musicVolume = 1.0f;
            float soundsChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
            float musicChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
            internal_gw::GMacAudio* mac_audio = nullptr;

            virtual ~GAudioImplementation()
            {
                // Broadcast DESTROY here
                GEvent gEvent;
                // EVENT_DATA is not required for this event
                gEvent.Write(Events::DESTROY, nullptr_t());
                this->Push(gEvent);

                // Cleanup Objective C data
                mac_audio->Unload();
                delete mac_audio;
                mac_audio = nullptr;
            }

            // Main class
            GReturn Create()
            {
                mac_audio = new internal_gw::GMacAudio();
                bool success = mac_audio->Init();
                if (success == false)
                    return GReturn::HARDWARE_UNAVAILABLE;
                // Initialize GAudio's volumes
                masterVolume = 1.0f;
                soundsVolume = 1.0f;
                musicVolume = 1.0f;

                return GEventGeneratorImplementation::Create();
            }

            GReturn SetMasterVolume(float _value) override
            {
                if (_value < 0.0f)
                    return GReturn::INVALID_ARGUMENT;

                if (fabsf(_value - masterVolume) < FLT_EPSILON)
                    return GReturn::REDUNDANT;

                masterVolume = (_value > 1.0f) ? 1.0f : _value;

                GEvent gEvent;
                EVENT_DATA eventData;
                // Set the first value of a passed array to our actual master volume
                eventData.channelVolumes[0] = masterVolume;
                eventData.numOfChannels = 0; // we are not using channels in this event
                gEvent.Write(Events::MASTER_VOLUME_CHANGED, eventData);

                return this->Push(gEvent);
            }

            GReturn SetGlobalSoundVolume(float _value) override
            {
                if (_value < 0.0f)
                    return GReturn::INVALID_ARGUMENT;

                if (fabsf(_value - soundsVolume) < FLT_EPSILON)
                    return GReturn::REDUNDANT;

                soundsVolume = (_value > 1.0f) ? 1.0f : _value;

                GEvent gEvent;
                EVENT_DATA eventData;
                // Set the first value of a passed array to our actual master volume
                eventData.channelVolumes[0] = soundsVolume;
                eventData.numOfChannels = 0; // we are not using channels in this event
                gEvent.Write(Events::SOUNDS_VOLUME_CHANGED, eventData);

                return this->Push(gEvent);
            }

            GReturn SetGlobalMusicVolume(float _value) override
            {
                if (_value < 0.0f)
                    return GReturn::INVALID_ARGUMENT;

                if (fabsf(_value - musicVolume) < FLT_EPSILON)
                    return GReturn::REDUNDANT;

                musicVolume = (_value > 1.0f) ? 1.0f : _value;

                GEvent gEvent;
                EVENT_DATA eventData;
                // Set the first value of a passed array to our actual master volume
                eventData.channelVolumes[0] = musicVolume;
                eventData.numOfChannels = 0; // we are not using channels in this event
                gEvent.Write(Events::MUSIC_VOLUME_CHANGED, eventData);

                return this->Push(gEvent);
            }

            GReturn SetSoundsChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
                if (_values == nullptr)
                    return GReturn::INVALID_ARGUMENT;

                if (_numChannels == 0 || _numChannels > 6)
                    return GReturn::INVALID_ARGUMENT;

                for (unsigned int i = 0; i < 6; ++i)
                {
                    soundsChannelVolumes[i] = (i < _numChannels) ? _values[i] : 0.0f;
                }

                GEvent gEvent;
                EVENT_DATA eventData;
                memcpy(eventData.channelVolumes, soundsChannelVolumes, 6 * sizeof(float));
                eventData.numOfChannels = _numChannels;
                gEvent.Write(Events::SOUND_CHANNEL_VOLUMES_CHANGED, eventData);

                return this->Push(gEvent);
            }

            GReturn SetMusicChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
                if (_values == nullptr)
                    return GReturn::INVALID_ARGUMENT;

                if (_numChannels == 0 || _numChannels > 6)
                    return GReturn::INVALID_ARGUMENT;

                for (unsigned int i = 0; i < 6; ++i)
                {
                    musicChannelVolumes[i] = (i < _numChannels) ? _values[i] : 0.0f;
                }

                GEvent gEvent;
                EVENT_DATA eventData;
                memcpy(eventData.channelVolumes, musicChannelVolumes, 6 * sizeof(float));
                eventData.numOfChannels = _numChannels;
                gEvent.Write(Events::MUSIC_CHANNEL_VOLUMES_CHANGED, eventData);

                return this->Push(gEvent);
            }

            // Sound Events
            GReturn PlaySounds() override
            {
                GEvent gEvent;
                // EVENT_DATA is not required for this event
                gEvent.Write(Events::PLAY_SOUNDS, nullptr_t());

                return this->Push(gEvent);
            }
            GReturn PauseSounds() override
            {
                GEvent gEvent;
                // EVENT_DATA is not required for this event
                gEvent.Write(Events::PAUSE_SOUNDS, nullptr_t());

                return this->Push(gEvent);
            }
            GReturn ResumeSounds() override
            {
                GEvent gEvent;
                // EVENT_DATA is not required for this event
                gEvent.Write(Events::RESUME_SOUNDS, nullptr_t());

                return this->Push(gEvent);
            }
            GReturn StopSounds() override
            {
                GEvent gEvent;
                // EVENT_DATA is not required for this event
                gEvent.Write(Events::STOP_SOUNDS, nullptr_t());

                return this->Push(gEvent);
            }

            // Music Events
            GReturn PlayMusic() override
            {
                GEvent gEvent;
                // EVENT_DATA is not required for this event
                gEvent.Write(Events::PLAY_MUSIC, nullptr_t());

                return this->Push(gEvent);
            }
            GReturn PauseMusic() override
            {
                GEvent gEvent;
                // EVENT_DATA is not required for this event
                gEvent.Write(Events::PAUSE_MUSIC, nullptr_t());

                return this->Push(gEvent);
            }
            GReturn ResumeMusic() override
            {
                GEvent gEvent;
                // EVENT_DATA is not required for this event
                gEvent.Write(Events::RESUME_MUSIC, nullptr_t());

                return this->Push(gEvent);
            }
            GReturn StopMusic() override
            {
                GEvent gEvent;
                // EVENT_DATA is not required for this event
                gEvent.Write(Events::STOP_MUSIC, nullptr_t());

                return this->Push(gEvent);
            }
        };
    }// end I
}// end GW

#undef G_NUM_OF_OUTPUTS
