#if _MSC_VER >= 1700
	#pragma comment(lib, "xaudio2.lib")
#endif

#include <xaudio2.h>
#include <xaudio2fx.h>
#include <atomic>
#include <math.h>

namespace GW
{
    namespace I
    {
        class GAudioImplementation :	public virtual GAudioInterface,
										public GEventGeneratorImplementation
        {
			struct PlatformAudio
			{
				IXAudio2* myAudio = nullptr;
				IXAudio2MasteringVoice* theMasterVoice = nullptr;
			};
        public:
			float masterVolume = 1.0f;
			float soundsVolume = 1.0f;
			float musicVolume = 1.0f;
			float soundsChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
			float musicChannelVolumes[6] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
			PlatformAudio XAudioData;

			virtual ~GAudioImplementation()
			{
				// Broadcast DESTROY here
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::DESTROY, nullptr_t());
				this->Push(gEvent);

				if (XAudioData.theMasterVoice)
					XAudioData.theMasterVoice->DestroyVoice();

				if (XAudioData.myAudio)
				{
					XAudioData.myAudio->StopEngine();
					XAudioData.myAudio->Release();
				}
			}

            // Main class
            GReturn Create()
            {
				if (XAudioData.myAudio != nullptr)
					return GReturn::FAILURE;

				HRESULT theResult = CoInitialize(NULL);
				if (FAILED(theResult = XAudio2Create(&XAudioData.myAudio)))
					return GReturn::HARDWARE_UNAVAILABLE;

				if (FAILED(theResult = XAudioData.myAudio->CreateMasteringVoice(&XAudioData.theMasterVoice)))
					return GReturn::HARDWARE_UNAVAILABLE;

				// Initalize GAudio's volumes
				masterVolume = 1.0f;
				soundsVolume = 1.0f;
				musicVolume = 1.0f;

				#ifndef NDEBUG
				XAUDIO2_DEBUG_CONFIGURATION debug = {};
				debug.TraceMask = XAUDIO2_LOG_ERRORS | XAUDIO2_LOG_WARNINGS;
				debug.BreakMask = XAUDIO2_LOG_ERRORS;
				XAudioData.myAudio->SetDebugConfiguration(&debug, 0);
				#endif

				return GEventGeneratorImplementation::Create();
            }

            GReturn SetMasterVolume(float _value) override
            {
				if (_value < 0.0f)
					return GReturn::INVALID_ARGUMENT;

				if (fabsf(_value - masterVolume) < FLT_EPSILON)
					return GReturn::REDUNDANT;

				masterVolume = (_value > 1.0f) ? 1.0f : _value;

				GEvent gEvent;
				EVENT_DATA eventData;
				// Set the first value of a passed array to our actual master volume
				eventData.channelVolumes[0] = masterVolume;
				eventData.numOfChannels = 0; // we are not using channels in this event
				gEvent.Write(Events::MASTER_VOLUME_CHANGED, eventData);

				return this->Push(gEvent);
            }

			GReturn SetGlobalSoundVolume(float _value) override
			{
				if (_value < 0.0f)
					return GReturn::INVALID_ARGUMENT;

				if (fabsf(_value - soundsVolume) < FLT_EPSILON)
					return GReturn::REDUNDANT;

				soundsVolume = (_value > 1.0f) ? 1.0f : _value;

				GEvent gEvent;
				EVENT_DATA eventData;
				// Set the first value of a passed array to our actual master volume
				eventData.channelVolumes[0] = soundsVolume;
				eventData.numOfChannels = 0; // we are not using channels in this event
				gEvent.Write(Events::SOUNDS_VOLUME_CHANGED, eventData);

				return this->Push(gEvent);
			}

			GReturn SetGlobalMusicVolume(float _value) override
			{
				if (_value < 0.0f)
					return GReturn::INVALID_ARGUMENT;

				if (fabsf(_value - musicVolume) < FLT_EPSILON)
					return GReturn::REDUNDANT;

				musicVolume = (_value > 1.0f) ? 1.0f : _value;

				GEvent gEvent;
				EVENT_DATA eventData;
				// Set the first value of a passed array to our actual master volume
				eventData.channelVolumes[0] = musicVolume;
				eventData.numOfChannels = 0; // we are not using channels in this event
				gEvent.Write(Events::MUSIC_VOLUME_CHANGED, eventData);

				return this->Push(gEvent);
			}

            GReturn SetSoundsChannelVolumes(const float* _values, unsigned int _numChannels) override
            {
				if (_values == nullptr)
					return GReturn::INVALID_ARGUMENT;

				if (_numChannels == 0 || _numChannels > 6)
					return GReturn::INVALID_ARGUMENT;

				for (unsigned int i = 0; i < 6; ++i)
				{
					soundsChannelVolumes[i] = (i < _numChannels) ? _values[i] : 0.0f;
				}

				GEvent gEvent;
				EVENT_DATA eventData;
				memcpy(eventData.channelVolumes, soundsChannelVolumes, 6 * sizeof(float));
				eventData.numOfChannels = _numChannels;
				gEvent.Write(Events::SOUND_CHANNEL_VOLUMES_CHANGED, eventData);

				return this->Push(gEvent);
            }

			GReturn SetMusicChannelVolumes(const float* _values, unsigned int _numChannels) override
			{
				if (_values == nullptr)
					return GReturn::INVALID_ARGUMENT;

				if (_numChannels == 0 || _numChannels > 6)
					return GReturn::INVALID_ARGUMENT;

				for (unsigned int i = 0; i < 6; ++i)
				{
					musicChannelVolumes[i] = (i < _numChannels) ? _values[i] : 0.0f;
				}

				GEvent gEvent;
				EVENT_DATA eventData;
				memcpy(eventData.channelVolumes, musicChannelVolumes, 6 * sizeof(float));
				eventData.numOfChannels = _numChannels;
				gEvent.Write(Events::MUSIC_CHANNEL_VOLUMES_CHANGED, eventData);

				return this->Push(gEvent);
			}

			// Sound Events
			GReturn PlaySounds() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::PLAY_SOUNDS, nullptr_t());

				return this->Push(gEvent);
			}
            GReturn PauseSounds() override
            {
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::PAUSE_SOUNDS, nullptr_t());
				
                return this->Push(gEvent);
            }
			GReturn ResumeSounds() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::RESUME_SOUNDS, nullptr_t());

				return this->Push(gEvent);
			}
            GReturn StopSounds() override
            {
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::STOP_SOUNDS, nullptr_t());

				return this->Push(gEvent);
            }

			// Music Events
			GReturn PlayMusic() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::PLAY_MUSIC, nullptr_t());

				return this->Push(gEvent);
			}
			GReturn PauseMusic() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::PAUSE_MUSIC, nullptr_t());

				return this->Push(gEvent);
			}
			GReturn ResumeMusic() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::RESUME_MUSIC, nullptr_t());

				return this->Push(gEvent);
			}
			GReturn StopMusic() override
			{
				GEvent gEvent;
				// EVENT_DATA is not required for this event
				gEvent.Write(Events::STOP_MUSIC, nullptr_t());

				return this->Push(gEvent);
			}
        };
    }// end I
}// end GW