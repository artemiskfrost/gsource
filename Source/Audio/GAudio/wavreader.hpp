#ifndef WAVREADER_HPP_
#define WAVREADER_HPP_

#include "../../../Interface/System/GFile.h"
#include <memory>

#define WR_INVALID_OFFSET 0xFFFFFFFF

// WAV format defines
#define WR_WAV_FORMAT_PCM 0x0001
#define WR_WAV_FORMAT_IEEE_FLOAT 0x0003


class WavReader
{
public:
    // This is a Windows specific thing, can be ignored on Linux.
    struct GUID {
        uint32_t  Data1;
        uint16_t Data2;
        uint16_t Data3;
        uint8_t  Data4[8];
    };

    struct PCM_FORMAT
    {
        uint16_t formatTag;
        uint16_t channels;
        uint32_t sampleRate;
        uint32_t byteRate;
        uint16_t blockAlign;
        uint16_t bitsPerSample;

        // For extra information, also known as cbSize
        uint16_t extraChunkSize;
    };

    // This is mostly going to stick 1:1 to Windows, but shouldn't affect Linux.
    struct PCM_HEADER
    {
        PCM_FORMAT format;

        union 
        {
            uint16_t validBitsPerSample;
            uint16_t samplesPerBlock;
            uint16_t reserved;
        } Samples;

        uint32_t channelMask;
        GUID subFormat;
    };

    struct PCM_BUFFER
    {
        size_t size;
        std::unique_ptr<uint8_t[]> data;
    };

private:
    enum class WaveTag
    {
        RIFF = 0x46464952, // = 'FFIR'
        DATA = 0x61746164, // = 'atad'
        FMT = 0x20746D66,  // = ' tmf'
        WAVE = 0x45564157, // = 'EVAW'
    };

    PCM_HEADER m_header = {};
    PCM_BUFFER m_buffer = { 0, nullptr };

    bool m_isSigned = false;

    // This will be set to the offset of the data chunk
    size_t m_dataOffset = WR_INVALID_OFFSET;

    GW::SYSTEM::GFile m_file;

    bool m_isFileOpen = false;

public:
    WavReader()
    {
        m_file.Create();
    }

    ~WavReader()
    {
        if (m_isFileOpen)
        {
            m_file.CloseFile();
            m_isFileOpen = false;
        }
    }

    // This will read the file and store all data inside the buffer as expected.
    GW::GReturn ReadWAV(const char* path)
    {
        if (m_isFileOpen)
        {
            return GW::GReturn::FAILURE;
        }

        if (-m_file.OpenBinaryRead(path))
        {
            return GW::GReturn::FILE_NOT_FOUND;
        }

        m_isFileOpen = true;

        // variables for determining data information
        unsigned long chunkId = 0;
        unsigned long chunkSize = 0;
        unsigned long wavFormat = 0;
        bool foundAudioData = false;

        GW::GReturn result = GW::GReturn::SUCCESS;
        while (result == GW::GReturn::SUCCESS && !foundAudioData)
        {
            if (-m_file.Read(reinterpret_cast<char*>(&chunkId), 4))
            {
                // could not acquire chunk type
                result = GW::GReturn::FAILURE;
                break;
            }

            if (-m_file.Read(reinterpret_cast<char*>(&chunkSize), 4))
            {
                // could not acquire chunk size
                result = GW::GReturn::FAILURE;
                break;
            }

            switch (static_cast<WaveTag>(chunkId))
            {
            case WaveTag::RIFF:
            {
                chunkSize = 4;

                if (-m_file.Read(reinterpret_cast<char*>(&wavFormat), chunkSize))
                {
                    // could not acquire the file type
                    result = GW::GReturn::FAILURE;
                    break;
                }

                // Checks if the format is a wav file, if not, it is unsupported
                if (wavFormat != static_cast<unsigned long>(WaveTag::WAVE))
                {
                    // the file is not a wav file
                    result = GW::GReturn::FORMAT_UNSUPPORTED;
                    break;
                }

                break;
            }

            case WaveTag::FMT:
            {
                if (-m_file.Read(reinterpret_cast<char*>(&m_header.format), chunkSize))
                {
                    // could not read the chunk data
                    result = GW::GReturn::FAILURE;
                    break;
                }

                if (m_header.format.formatTag != WR_WAV_FORMAT_PCM &&
                    m_header.format.formatTag != WR_WAV_FORMAT_IEEE_FLOAT)
                {
                    // the file is not a PCM file and has some form of compression
                    result = GW::GReturn::FORMAT_UNSUPPORTED;
                    break;
                }

                break;
            }

            case WaveTag::DATA:
            {
                m_buffer.data.reset(new uint8_t[chunkSize]);

                // Zero out the buffer
                memset(m_buffer.data.get(), 0, static_cast<int>(chunkSize));

                if (-m_file.Read(reinterpret_cast<char*>(m_buffer.data.get()), chunkSize))
                {
                    // could not read the audio data
                    result = GW::GReturn::FAILURE;
                    break;
                }

                m_buffer.size = chunkSize;


                // contains size of the audio buffer in bytes
                foundAudioData = true;
                break;
            }

            default:
            {
                std::unique_ptr<char> pThrowawayDataBuffer(new char[chunkSize]);
                if (-m_file.Read(pThrowawayDataBuffer.get(), chunkSize))
                {
                    // something unknown happened that caused the data to not be read
                    result = GW::GReturn::FAILURE;
                }
                break;
            }
            }
        }

        CloseFile();

        m_isSigned = m_header.format.bitsPerSample != 8;

    	return result;
    }

    // This will read the file and store only the offset to the data chunk, keeping the file seeked to the start of the chunk.
    GW::GReturn ReadWAVOffset(const char* path)
    {
#define WR_DEFAULT_CHUNK_SIZE 4

        if (m_isFileOpen)
        {
            return GW::GReturn::FAILURE;
        }

        if (-m_file.OpenBinaryRead(path))
        {
            return GW::GReturn::FILE_NOT_FOUND;
        }

        m_isFileOpen = true;

        // variables for determining data information
        unsigned long chunkId = 0;
        unsigned long chunkSize = 0;
        unsigned long wavFormat = 0;
        unsigned long dataOffset = 0;
        bool foundAudioData = false;

        GW::GReturn result = GW::GReturn::SUCCESS;
        while (result == GW::GReturn::SUCCESS && !foundAudioData)
        {
            if (-m_file.Read(reinterpret_cast<char*>(&chunkId), WR_DEFAULT_CHUNK_SIZE))
            {
                // could not acquire chunk type
                result = GW::GReturn::FAILURE;
                break;
            }

            dataOffset += WR_DEFAULT_CHUNK_SIZE;

            if (-m_file.Read(reinterpret_cast<char*>(&chunkSize), WR_DEFAULT_CHUNK_SIZE))
            {
                // could not acquire chunk size
                result = GW::GReturn::FAILURE;
                break;
            }

            dataOffset += WR_DEFAULT_CHUNK_SIZE;

            switch (static_cast<WaveTag>(chunkId))
            {
            case WaveTag::RIFF:
            {
                chunkSize = 4;
                if (-m_file.Read(reinterpret_cast<char*>(&wavFormat), chunkSize))
                {
                    // could not acquire the file type
                    result = GW::GReturn::FAILURE;
                    break;
                }

                // Checks if the format is a wav file, if not, it is unsupported
                if (wavFormat != static_cast<unsigned long>(WaveTag::WAVE))
                {
                    // the file is not a wav file
                    result = GW::GReturn::FORMAT_UNSUPPORTED;
                    break;
                }

                dataOffset += chunkSize;

                break;
            }

            case WaveTag::FMT:
            {
                if (-m_file.Read(reinterpret_cast<char*>(&m_header.format), chunkSize))
                {
                    // could not read the chunk data
                    result = GW::GReturn::FAILURE;
                    break;
                }

                // 0x0001 for PCM and 0x0003 for IEEE float
                if (m_header.format.formatTag != WR_WAV_FORMAT_PCM &&
                    m_header.format.formatTag != WR_WAV_FORMAT_IEEE_FLOAT)
                {
                    // the file is not a PCM file and has some form of compression
                    result = GW::GReturn::FORMAT_UNSUPPORTED;
                    break;
                }

                dataOffset += chunkSize;

                break;
            }

            case WaveTag::DATA:
            {
                m_buffer.size = chunkSize;
                m_dataOffset = dataOffset;
                foundAudioData = true; // audio data is the last part of the file. stop looping
                break;
            }

            default:
            {
                std::unique_ptr<char> bogusData(new char[chunkSize]);
                if (-m_file.Read(bogusData.get(), chunkSize))
                {
                    // something unknown happened that caused the data to not be read
                    result = GW::GReturn::FAILURE;
                }

                dataOffset += chunkSize;

                break;
            }
            }
        }

#undef WR_DEFAULT_CHUNK_SIZE

        return result;
    }

    GW::GReturn SeekToDataOffset()
    {
        if (!m_isFileOpen || m_dataOffset == WR_INVALID_OFFSET)
        {
            return GW::GReturn::FAILURE;
        }

        unsigned int unusedPosition;
        m_file.Seek(0, m_dataOffset, unusedPosition);

        return GW::GReturn::SUCCESS;
    }

    GW::GReturn ReadDataChunk(char* buffer, size_t size)
    {
        if (!m_isFileOpen || m_dataOffset == WR_INVALID_OFFSET || -m_file.Read(buffer, size))
        {
            return GW::GReturn::FAILURE;
        }

        return GW::GReturn::SUCCESS;
    }

    void CloseFile()
    {
        if (m_isFileOpen)
        {
            m_file.CloseFile();
            m_isFileOpen = false;
        }
    }

    [[nodiscard]] bool IsSigned() const
    {
        return m_isSigned;
    }

    [[nodiscard]] PCM_BUFFER& GetBuffer() const
    {
        return const_cast<PCM_BUFFER&>(m_buffer);
    }

    [[nodiscard]] size_t GetBufferSize() const
    {
        return m_buffer.size;
    }

    // Returns a mutable reference to the header, this is only used by Windows, but could also be implemented into linux
    // in the future.
    [[nodiscard]] PCM_HEADER& GetHeader()
    {
        return m_header;
    }

    // This will return a failure if the offset isn't set. We have to check to ensure that the offset is valid and the
    // file is reading the data in the correct manner, otherwise the offset will be invalid (0xFFFFFFFF).
    GW::GReturn GetOffset(size_t& offset) const
    {
        if (m_dataOffset == WR_INVALID_OFFSET)
        {
            return GW::GReturn::FAILURE;
        }

        offset = m_dataOffset;
        return GW::GReturn::SUCCESS;
    }

    // Resets everything back to defaults, discarding any data that was read.
    void Reset()
    {
        m_header = {};
        m_buffer = {};
        m_isSigned = false;
        m_dataOffset = WR_INVALID_OFFSET;
    }
};

#undef WR_WAV_FORMAT_PCM
#undef WR_WAV_FORMAT_IEEE_FLOAT
#undef WR_INVALID_OFFSET

#endif // WAVREADER_HPP_
