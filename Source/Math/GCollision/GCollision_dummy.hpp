namespace GW
{
	namespace I
	{
		class GCollisionImplementation : public virtual GCollisionInterface,
			private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::INTERFACE_UNSUPPORTED;
			}

			// float
			static GReturn ConvertAABBCEToAABBMMF(const MATH::GAABBCEF _aabbCE, MATH::GAABBMMF& _outAABBMM)
			{
				return GReturn::FAILURE;
			}

			static GReturn ConvertAABBMMToAABBCEF(const MATH::GAABBMMF _aabbMM, MATH::GAABBCEF& _outAABCE)
			{
				return GReturn::FAILURE;
			}

			static GReturn ComputePlaneF(const MATH::GVECTORF _planePositionA, const MATH::GVECTORF _planePositionB, const MATH::GVECTORF _planePositionC, MATH::GPLANEF& _outPlane)
			{
				return GReturn::FAILURE;
			}

			static GReturn IsTriangleF(const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToConvexPolygonF(const MATH::GVECTORF _queryPoint, const MATH::GVECTORF* _polygonPoints, const unsigned int _pointsCount, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToLineF(const MATH::GLINEF _line, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointsToLineFromLineF(const MATH::GLINEF _line1, const MATH::GLINEF _line2, MATH::GVECTORF& _outPoint1, MATH::GVECTORF& _outPoint2)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToRayF(const MATH::GRAYF _ray, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToTriangleF(const MATH::GTRIANGLEF _triangle, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToPlaneF(const MATH::GPLANEF _plane, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToSphereF(const MATH::GSPHEREF _sphere, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToCapsuleF(const MATH::GCAPSULEF _capsule, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToAABBF(const MATH::GAABBMMF _aabb, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToOBBF(const MATH::GOBBF _obb, const MATH::GVECTORF _queryPoint, MATH::GVECTORF& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ComputeSphereFromPointsF(const MATH::GVECTORF* _pointCloud, const unsigned int _pointsCount, MATH::GSPHEREF& _outSphere)
			{
				return GReturn::FAILURE;
			}

			static GReturn ComputeAABBFromPointsF(const MATH::GVECTORF* _pointCloud, const unsigned int _pointsCount, MATH::GAABBMMF& _outAABB)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToLineF(const MATH::GVECTORF _point, const MATH::GLINEF _line, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToRayF(const MATH::GVECTORF _point, const MATH::GRAYF _ray, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToTriangleF(const MATH::GVECTORF _point, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToPlaneF(const MATH::GVECTORF _point, const MATH::GPLANEF _plane, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToSphereF(const MATH::GVECTORF _point, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToCapsuleF(const MATH::GVECTORF _point, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToAABBF(const MATH::GVECTORF _point, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToOBBF(const MATH::GVECTORF _point, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToLineF(const MATH::GLINEF _line1, const MATH::GLINEF _line2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToRayF(const MATH::GLINEF _line, const MATH::GRAYF _ray, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToTriangleF(const MATH::GLINEF _line, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToPlaneF(const MATH::GLINEF _line, const MATH::GPLANEF _plane, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToSphereF(const MATH::GLINEF _line, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToCapsuleF(const MATH::GLINEF _line, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToAABBF(const MATH::GLINEF _line, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToOBBF(const MATH::GLINEF _line, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToTriangleF(const MATH::GRAYF _ray, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToPlaneF(const MATH::GRAYF _ray, const MATH::GPLANEF _plane, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToSphereF(const MATH::GRAYF _ray, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToCapsuleF(const MATH::GRAYF _ray, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToAABBF(const MATH::GRAYF _ray, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToOBBF(const MATH::GRAYF _ray, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToTriangleF(const MATH::GTRIANGLEF _triangle1, const MATH::GTRIANGLEF _triangle2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToPlaneF(const MATH::GTRIANGLEF _triangle, const MATH::GPLANEF _plane, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToSphereF(const MATH::GTRIANGLEF _triangle, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToCapsuleF(const MATH::GTRIANGLEF _triangle, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToAABBF(const MATH::GTRIANGLEF _triangle, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToOBBF(const MATH::GTRIANGLEF _triangle, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToPlaneF(const MATH::GPLANEF _plane1, const MATH::GPLANEF _plane2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToSphereF(const MATH::GPLANEF _plane, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToCapsuleF(const MATH::GPLANEF _plane, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToAABBF(const MATH::GPLANEF _plane, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToOBBF(const MATH::GPLANEF _plane, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestSphereToSphereF(const MATH::GSPHEREF _sphere1, const MATH::GSPHEREF _sphere2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestSphereToCapsuleF(const MATH::GSPHEREF _sphere, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestSphereToAABBF(const MATH::GSPHEREF _sphere, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestSphereToOBBF(const MATH::GSPHEREF _sphere, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestCapsuleToCapsuleF(const MATH::GCAPSULEF _capsule1, const MATH::GCAPSULEF _capsule2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestCapsuleToAABBF(const MATH::GCAPSULEF _capsule, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestCapsuleToOBBF(const MATH::GCAPSULEF _capsule, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestAABBToAABBF(const MATH::GAABBCEF _aabb1, const MATH::GAABBCEF _aabb2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestAABBToOBBF(const MATH::GAABBCEF _aabb, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestOBBToOBBF(const MATH::GOBBF _obb1, const MATH::GOBBF _obb2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToTriangleF(const MATH::GLINEF _line, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF* _outBarycentric, MATH::GVECTORF& _outDirection, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToPlaneF(const MATH::GLINEF _line, const MATH::GPLANEF _plane, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToSphereF(const MATH::GLINEF _line, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToCapsuleF(const MATH::GLINEF _line, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToAABBF(const MATH::GLINEF _line, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToOBBF(const MATH::GLINEF _line, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToTriangleF(const MATH::GRAYF _ray, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, MATH::GVECTORF* _outBarycentric, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToPlaneF(const MATH::GRAYF _ray, const MATH::GPLANEF _plane, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToSphereF(const MATH::GRAYF _ray, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToCapsuleF(const MATH::GRAYF _ray, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToAABBF(const MATH::GRAYF _ray, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToOBBF(const MATH::GRAYF _ray, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint, float& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectSphereToSphereF(const MATH::GSPHEREF _sphere1, const MATH::GSPHEREF _sphere2, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectSphereToCapsuleF(const MATH::GSPHEREF _sphere, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectSphereToAABBF(const MATH::GSPHEREF _sphere, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectSphereToOBBF(const MATH::GSPHEREF _sphere, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectCapsuleToCapsuleF(const MATH::GCAPSULEF _capsule1, const MATH::GCAPSULEF _capsule2, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectCapsuleToAABBF(const MATH::GCAPSULEF _capsule, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectCapsuleToOBBF(const MATH::GCAPSULEF _capsule, const MATH::GOBBF _obb, GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectAABBToAABBF(const MATH::GAABBCEF _aabb1, const MATH::GAABBCEF _aabb2, GCollisionCheck& _outResult, MATH::GAABBCEF& _outContactAABB, MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToLineF(const MATH::GVECTORF _point, const MATH::GLINEF _line, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToRayF(const MATH::GVECTORF _point, const MATH::GRAYF _ray, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToTriangleF(const MATH::GVECTORF _point, const MATH::GTRIANGLEF _triangle, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToPlaneF(const MATH::GVECTORF _point, const MATH::GPLANEF _plane, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToSphereF(const MATH::GVECTORF _point, const MATH::GSPHEREF _sphere, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToCapsuleF(const MATH::GVECTORF _point, const MATH::GCAPSULEF _capsule, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToAABBF(const MATH::GVECTORF _point, const MATH::GAABBMMF _aabb, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToOBBF(const MATH::GVECTORF _point, const MATH::GOBBF _obb, float& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn BarycentricF(const MATH::GVECTORF _a, const MATH::GVECTORF _b, const MATH::GVECTORF _c, const MATH::GVECTORF _p, MATH::GVECTORF& _outBarycentric)
			{
				return GReturn::FAILURE;
			}

			//double

			static GReturn ConvertAABBCEToAABBMMD(const MATH::GAABBCED _aabbCE, MATH::GAABBMMD& _outAABBMM)
			{
				return GReturn::FAILURE;
			}

			static GReturn ConvertAABBMMToAABBCED(const MATH::GAABBMMD _aabbMM, MATH::GAABBCED& _outAABCE)
			{
				return GReturn::FAILURE;
			}

			static GReturn ComputePlaneD(const MATH::GVECTORD _planePositionA, const MATH::GVECTORD _planePositionB, const MATH::GVECTORD _planePositionC, MATH::GPLANED& _outPlane)
			{
				return GReturn::FAILURE;
			}

			static GReturn IsTriangleD(const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToConvexPolygonD(const MATH::GVECTORD _queryPoint, const MATH::GVECTORD* _polygonPoints, const unsigned int _pointsCount, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToLineD(const MATH::GLINED _line, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointsToLineFromLineD(const MATH::GLINED _line1, const MATH::GLINED _line2, MATH::GVECTORD& _outPoint1, MATH::GVECTORD& _outPoint2)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToRayD(const MATH::GRAYD _ray, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToTriangleD(const MATH::GTRIANGLED _triangle, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToPlaneD(const MATH::GPLANED _plane, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToSphereD(const MATH::GSPHERED _sphere, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToCapsuleD(const MATH::GCAPSULED _capsule, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToAABBD(const MATH::GAABBMMD _aabb, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ClosestPointToOBBD(const MATH::GOBBD _obb, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				return GReturn::FAILURE;
			}

			static GReturn ComputeSphereFromPointsD(const MATH::GVECTORD* _pointCloud, const unsigned int _pointsCount, MATH::GSPHERED& _outSphere)
			{
				return GReturn::FAILURE;
			}

			static GReturn ComputeAABBFromPointsD(const MATH::GVECTORD* _pointCloud, const unsigned int _pointsCount, MATH::GAABBMMD& _outAABB)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToLineD(const MATH::GVECTORD _point, const MATH::GLINED _line, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToRayD(const MATH::GVECTORD _point, const MATH::GRAYD _ray, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToTriangleD(const MATH::GVECTORD _point, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToPlaneD(const MATH::GVECTORD _point, const MATH::GPLANED _plane, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToSphereD(const MATH::GVECTORD _point, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToCapsuleD(const MATH::GVECTORD _point, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToAABBD(const MATH::GVECTORD _point, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPointToOBBD(const MATH::GVECTORD _point, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToLineD(const MATH::GLINED _line1, const MATH::GLINED _line2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToRayD(const MATH::GLINED _line, const MATH::GRAYD _ray, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToTriangleD(const MATH::GLINED _line, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToPlaneD(const MATH::GLINED _line, const MATH::GPLANED _plane, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToSphereD(const MATH::GLINED _line, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToCapsuleD(const MATH::GLINED _line, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToAABBD(const MATH::GLINED _line, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestLineToOBBD(const MATH::GLINED _line, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToTriangleD(const MATH::GRAYD _ray, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToPlaneD(const MATH::GRAYD _ray, const MATH::GPLANED _plane, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToSphereD(const MATH::GRAYD _ray, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToCapsuleD(const MATH::GRAYD _ray, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToAABBD(const MATH::GRAYD _ray, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestRayToOBBD(const MATH::GRAYD _ray, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToTriangleD(const MATH::GTRIANGLED _triangle1, const MATH::GTRIANGLED _triangle2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToPlaneD(const MATH::GTRIANGLED _triangle, const MATH::GPLANED _plane, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToSphereD(const MATH::GTRIANGLED _triangle, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToCapsuleD(const MATH::GTRIANGLED _triangle, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToAABBD(const MATH::GTRIANGLED _triangle, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestTriangleToOBBD(const MATH::GTRIANGLED _triangle, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToPlaneD(const MATH::GPLANED _plane1, const MATH::GPLANED _plane2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToSphereD(const MATH::GPLANED _plane, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToCapsuleD(const MATH::GPLANED _plane, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToAABBD(const MATH::GPLANED _plane, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestPlaneToOBBD(const MATH::GPLANED _plane, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestSphereToSphereD(const MATH::GSPHERED _sphere1, const MATH::GSPHERED _sphere2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestSphereToCapsuleD(const MATH::GSPHERED _sphere, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestSphereToAABBD(const MATH::GSPHERED _sphere, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestSphereToOBBD(const MATH::GSPHERED _sphere, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestCapsuleToCapsuleD(const MATH::GCAPSULED _capsule1, const MATH::GCAPSULED _capsule2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestCapsuleToAABBD(const MATH::GCAPSULED _capsule, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestCapsuleToOBBD(const MATH::GCAPSULED _capsule, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestAABBToAABBD(const MATH::GAABBCED _aabb1, const MATH::GAABBCED _aabb2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestAABBToOBBD(const MATH::GAABBCED _aabb, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn TestOBBToOBBD(const MATH::GOBBD _obb1, const MATH::GOBBD _obb2, GCollisionCheck& _outResult)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToTriangleD(const MATH::GLINED _line, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD* _outBarycentric, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToPlaneD(const MATH::GLINED _line, const MATH::GPLANED _plane, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToSphereD(const MATH::GLINED _line, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToCapsuleD(const MATH::GLINED _line, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToAABBD(const MATH::GLINED _line, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectLineToOBBD(const MATH::GLINED _line, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToTriangleD(const MATH::GRAYD _ray, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD* _outBarycentric, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToPlaneD(const MATH::GRAYD _ray, const MATH::GPLANED _plane, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToSphereD(const MATH::GRAYD _ray, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToCapsuleD(const MATH::GRAYD _ray, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToAABBD(const MATH::GRAYD _ray, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectRayToOBBD(const MATH::GRAYD _ray, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectSphereToSphereD(const MATH::GSPHERED _sphere1, const MATH::GSPHERED _sphere2, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectSphereToCapsuleD(const MATH::GSPHERED _sphere, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectSphereToAABBD(const MATH::GSPHERED _sphere, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectSphereToOBBD(const MATH::GSPHERED _sphere, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectCapsuleToCapsuleD(const MATH::GCAPSULED _capsule1, const MATH::GCAPSULED _capsule2, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectCapsuleToAABBD(const MATH::GCAPSULED _capsule, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectCapsuleToOBBD(const MATH::GCAPSULED _capsule, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn IntersectAABBToAABBD(const MATH::GAABBCED _aabb1, const MATH::GAABBCED _aabb2, GCollisionCheck& _outResult, MATH::GAABBCED& _outContactAABB, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToLineD(const MATH::GVECTORD _point, const MATH::GLINED _line, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToRayD(const MATH::GVECTORD _point, const MATH::GRAYD _ray, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToTriangleD(const MATH::GVECTORD _point, const MATH::GTRIANGLED _triangle, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToPlaneD(const MATH::GVECTORD _point, const MATH::GPLANED _plane, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToSphereD(const MATH::GVECTORD _point, const MATH::GSPHERED _sphere, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToCapsuleD(const MATH::GVECTORD _point, const MATH::GCAPSULED _capsule, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToAABBD(const MATH::GVECTORD _point, const MATH::GAABBMMD _aabb, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn SqDistancePointToOBBD(const MATH::GVECTORD _point, const MATH::GOBBD _obb, double& _outDistance)
			{
				return GReturn::FAILURE;
			}

			static GReturn BarycentricD(const MATH::GVECTORD _a, const MATH::GVECTORD _b, const MATH::GVECTORD _c, const MATH::GVECTORD _p, MATH::GVECTORD& _outBarycentric)
			{
				return GReturn::FAILURE;
			}
		};
	}
}