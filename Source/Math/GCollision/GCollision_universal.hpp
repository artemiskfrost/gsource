#include <cfloat>

namespace GW
{
	namespace I
	{
		class GCollisionImplementation : public virtual GCollisionInterface,
			private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::SUCCESS;
			}

			static GReturn ConvertAABBCEToAABBMMF(const MATH::GAABBCEF _aabbCE, MATH::GAABBMMF& _outAABBMM)
			{
				_outAABBMM.min =
				{
					{
						{
							_aabbCE.center.x - _aabbCE.extent.x,
							_aabbCE.center.y - _aabbCE.extent.y,
							_aabbCE.center.z - _aabbCE.extent.z
						}
					}
				};

				_outAABBMM.max =
				{
					{
						{
							_aabbCE.center.x + _aabbCE.extent.x,
							_aabbCE.center.y + _aabbCE.extent.y,
							_aabbCE.center.z + _aabbCE.extent.z
						}
					}
				};

				return GReturn::SUCCESS;
			}

			static GReturn ConvertAABBMMToAABBCEF(const MATH::GAABBMMF _aabbMM, MATH::GAABBCEF& _outAABCE)
			{
				_outAABCE.center =
				{
					{
						{
							(_aabbMM.min.x + _aabbMM.max.x) * 0.5f,
							(_aabbMM.min.y + _aabbMM.max.y) * 0.5f,
							(_aabbMM.min.z + _aabbMM.max.z) * 0.5f
						}
					}
				};

				_outAABCE.extent =
				{
					{
						{
							_aabbMM.max.x - _outAABCE.center.x,
							_aabbMM.max.y - _outAABCE.center.y,
							_aabbMM.max.z - _outAABCE.center.z
						}
					}
				};

				return GReturn::SUCCESS;
			}

			static GReturn ComputePlaneF(const MATH::GVECTORF _planePositionA, const MATH::GVECTORF _planePositionB,
										 const MATH::GVECTORF _planePositionC, MATH::GPLANEF& _outPlane)
			{
				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_planePositionB,
					_planePositionA,
					difference_ba);

				GW::MATH::GVECTORF difference_ca = {};
				GW::MATH::GVector::SubtractVectorF(
					_planePositionC,
					_planePositionA,
					difference_ca);

				GW::MATH::GVector::CrossVector3F(
					difference_ca,
					difference_ba,
					_outPlane.data);

				if(GW::MATH::GVector::NormalizeF(_outPlane.data, _outPlane.data) == GReturn::FAILURE)
				{
					 // ba and ca are parallel which degenerates to a line.
					return GReturn::FAILURE;
				}

				GW::MATH::GVector::DotF(
					_outPlane.data,
					_planePositionA.xyz(),
					_outPlane.distance);

				return GReturn::SUCCESS;
			}

			static GReturn IsTriangleF(const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult)
			{
				MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle.b,
					_triangle.a,
					difference_ba);

				MATH::GVECTORF difference_ca = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle.c,
					_triangle.a,
					difference_ca);

				MATH::GVECTORF triangle_normal = {};
				GW::MATH::GVector::CrossVector3F(
					difference_ba,
					difference_ca,
					triangle_normal);

				float triangle_area = 0.0f;
				GW::MATH::GVector::DotF(
					triangle_normal,
					triangle_normal,
					triangle_area);

				_outResult = (G_ABS(triangle_area) < G_COLLISION_THRESHOLD_F) ? GCollisionCheck::NO_COLLISION : GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToConvexPolygonF(const MATH::GVECTORF _queryPoint, const MATH::GVECTORF* _polygonPoints,
													 const unsigned int _pointsCount, GCollisionCheck& _outResult)
			{
				if(!_polygonPoints || _pointsCount < 3)
				{
					return GReturn::FAILURE;
				}

				auto Collinear = [&](const GW::MATH::GVECTORF p, const GW::MATH::GLINEF l)
				{
					if(p.x <= G_LARGER(l.start.x, l.end.x) && p.x <= G_SMALLER(l.start.x, l.end.x) &&
					   p.y <= G_LARGER(l.start.y, l.end.y) && p.y <= G_SMALLER(l.start.y, l.end.y))
					{
						return true;
					}

					return false;
				};

				auto Area2D = [&](const MATH::GVECTORF a, const MATH::GVECTORF b, const MATH::GVECTORF p)
				{
					float winding = 0;

					GW::MATH::GVECTORF difference_pa = {};
					GW::MATH::GVector::SubtractVectorF(
						p,
						a,
						difference_pa);

					GW::MATH::GVECTORF difference_ba = {};
					GW::MATH::GVector::SubtractVectorF(
						b,
						a,
						difference_ba);

					GW::MATH::GVector::CrossVector2F(
						difference_pa,
						difference_ba,
						winding);

					if(G_ABS(winding) < G_COLLISION_THRESHOLD_F)
					{
						return 0;
					}
					else if(winding > 0.0f)
					{
						return 1;
					}
					else
					{
						return -1;
					}
				};

				auto IsCrossing = [&](const GW::MATH::GLINEF l1, const GW::MATH::GLINEF l2)
				{
					int winding1 = Area2D(l1.start, l1.end, l2.start);
					int winding2 = Area2D(l1.start, l1.end, l2.end);
					int winding3 = Area2D(l2.start, l2.end, l1.start);
					int winding4 = Area2D(l2.start, l2.end, l1.end);

					if(winding1 != winding2 && winding3 != winding4)
					{
						return true;
					}
					if(winding1 == 0 && Collinear(l2.start, l1))
					{
						return true;
					}
					if(winding2 == 0 && Collinear(l2.end, l1))
					{
						return true;
					}
					if(winding3 == 0 && Collinear(l1.start, l2))
					{
						return true;
					}
					if(winding4 == 0 && Collinear(l1.end, l2))
					{
						return true;
					}

					return false;
				};

				GW::MATH::GLINEF inf_line =
				{
					{
						{
							{{{_queryPoint.x , _queryPoint.y, 0.0f, 0.0f}}},
							{{{static_cast<float>(0xffffffff), _queryPoint.y}}}
						}
					}
				};

				int count = 0;
				int i = 0;

				do
				{
					GW::MATH::GLINEF side = {};
					side.start = _polygonPoints[i];
					side.end = _polygonPoints[(i + 1) % _pointsCount];

					if(IsCrossing(side, inf_line))
					{
						if(Area2D(side.start, _queryPoint, side.end) == 0)
						{
							_outResult = Collinear(_queryPoint, side) ? GCollisionCheck::COLLISION : GCollisionCheck::NO_COLLISION;
							return GReturn::SUCCESS;
						}
						count++;
					}

					i = (i + 1) % _pointsCount;
				}
				while(i != 0);

				_outResult = static_cast<GCollisionCheck>(count & 1);
				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToLineF(const MATH::GLINEF _line, const MATH::GVECTORF _queryPoint,
											   MATH::GVECTORF& _outPoint)
			{
				MATH::GVECTORF line_a = _line.start.xyz();

				MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_line.end.xyz(),
					line_a,
					difference_ba);

				MATH::GVECTORF difference_pa = {};
				GW::MATH::GVector::SubtractVectorF(
					_queryPoint.xyz(),
					line_a,
					difference_pa);

				float interval = 0.0f;
				GW::MATH::GVector::DotF(
					difference_pa,
					difference_ba,
					interval);

				float sq_length = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_ba,
					sq_length);

				// If line degenerates to point
				if(sq_length <= G_COLLISION_THRESHOLD_F)
				{
					_outPoint = line_a;
					return GReturn::SUCCESS;
				}

				interval /= sq_length;

				interval = G_CLAMP(
					interval,
					0.0f,
					1.0f);

				GW::MATH::GVector::ScaleF(
					difference_ba,
					interval,
					_outPoint);

				GW::MATH::GVector::AddVectorF(
					line_a,
					_outPoint,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointsToLineFromLineF(const MATH::GLINEF _line1, const MATH::GLINEF _line2,
														MATH::GVECTORF& _outPoint1, MATH::GVECTORF& _outPoint2)
			{
				MATH::GLINEF line1 =
				{
					{
						{
							_line1.start.xyz(),
							_line1.end.xyz()
						}
					}
				};

				MATH::GLINEF line2 =
				{
					{
						{
							_line2.start.xyz(),
							_line2.end.xyz()
						}
					}
				};

				MATH::GVECTORF direction1 = {};
				GW::MATH::GVector::SubtractVectorF(
					line1.end,
					line1.start,
					direction1);

				MATH::GVECTORF direction2 = {};
				GW::MATH::GVector::SubtractVectorF(
					line2.end,
					line2.start,
					direction2);

				MATH::GVECTORF distance = {};
				GW::MATH::GVector::SubtractVectorF(
					line1.start,
					line2.start,
					distance);

				float sq_length1 = 0.0f;
				GW::MATH::GVector::DotF(
					direction1,
					direction1,
					sq_length1);

				float sq_length2 = 0.0f;
				GW::MATH::GVector::DotF(
					direction2,
					direction2,
					sq_length2);

				float f = 0.0f;
				GW::MATH::GVector::DotF(
					direction2,
					distance,
					f);

				float interval1 = 0.0f;
				float interval2 = 0.0f;

				int use_cases = 0x00;

				// First line degenerates to a point.
				if(sq_length1 <= G_COLLISION_THRESHOLD_F)
				{
					use_cases |= 0x01;
				}

				// Second line degenerates to a point.
				if(sq_length2 <= G_COLLISION_THRESHOLD_F)
				{
					use_cases |= 0x02;
				}

				switch(use_cases)
				{
					// First line degenerates to a point.
					case 0x01:
					{
						interval2 = f / sq_length2;
						interval2 = G_CLAMP(
							interval2,
							0.0f,
							1.0f);
					}
					break;
					// Second line degenerates to a point.
					case 0x02:
					{
						float c = 0.0f;
						GW::MATH::GVector::DotF(
							direction1,
							distance,
							c);

						interval1 = G_CLAMP(
							-c / sq_length1,
							0.0f,
							1.0f);
					}
					break;
					// Both lines degenerate to a point.
					case 0x03:
					{
						_outPoint1 = line1.start.xyz();
						_outPoint2 = line2.start.xyz();
						return GReturn::SUCCESS;
					}
					break;
					default:
					{
						float c = 0.0f;
						GW::MATH::GVector::DotF(
							direction1,
							distance,
							c);

						float b = 0.0f;
						GW::MATH::GVector::DotF(
							direction1,
							direction2,
							b);

						float denom = sq_length1 * sq_length2 - b * b;

						// Get interval for the first line from the second line unless lines are parallel.
						if(denom != 0.0f)
						{
							interval1 = G_CLAMP(
								(b * f - c * sq_length2) / denom,
								0.0f,
								1.0f);
						}
						else
						{
							interval1 = 0.0f;
						}

						// Get interval for the second line.
						interval2 = (b * interval1 + f) / sq_length2;

						if(interval2 < 0.0f)
						{
							interval2 = 0.0f;

							interval1 = G_CLAMP(
								-c / sq_length1,
								0.0f,
								1.0f);
						}
						else if(interval2 > 1.0f)
						{
							interval2 = 1.0f;

							interval1 = G_CLAMP(
								(b - c) / sq_length1,
								0.0f,
								1.0f);
						}
					}
					break;
				}

				GW::MATH::GVector::ScaleF(
					direction1,
					interval1,
					direction1);

				GW::MATH::GVector::AddVectorF(
					line1.start,
					direction1,
					_outPoint1);

				GW::MATH::GVector::ScaleF(
					direction2,
					interval2,
					direction2);

				GW::MATH::GVector::AddVectorF(
					line2.start,
					direction2,
					_outPoint2);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToRayF(const MATH::GRAYF _ray, const MATH::GVECTORF _queryPoint,
											  MATH::GVECTORF& _outPoint)
			{
				MATH::GVECTORF line_a = _ray.position.xyz();
				MATH::GVECTORF line_direction = _ray.direction.xyz();

				MATH::GVECTORF difference_pa = {};
				GW::MATH::GVector::SubtractVectorF(
					_queryPoint.xyz(),
					line_a,
					difference_pa);

				float interval = 0.0f;
				GW::MATH::GVector::DotF(
					difference_pa,
					line_direction,
					interval);

				float sq_length = 0.0f;
				GW::MATH::GVector::DotF(
					line_direction,
					line_direction,
					sq_length);

				// If line degenerates to point
				if(sq_length <= G_COLLISION_THRESHOLD_F)
				{
					_outPoint = line_a;
					return GReturn::SUCCESS;
				}

				interval /= sq_length;

				interval = interval < 0.0f ? 0.0f : interval;

				GW::MATH::GVector::ScaleF(
					line_direction,
					interval,
					_outPoint);

				GW::MATH::GVector::AddVectorF(
					line_a,
					_outPoint,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToTriangleF(const MATH::GTRIANGLEF _triangle, const MATH::GVECTORF _queryPoint,
												   MATH::GVECTORF& _outPoint)
			{
				GW::MATH::GVECTORF a = _triangle.a.xyz();
				GW::MATH::GVECTORF b = _triangle.b.xyz();
				GW::MATH::GVECTORF c = _triangle.c.xyz();
				GW::MATH::GVECTORF p = _queryPoint.xyz();

				MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					b,
					a,
					difference_ba);

				MATH::GVECTORF difference_ca = {};
				GW::MATH::GVector::SubtractVectorF(
					c,
					a,
					difference_ca);

				MATH::GVECTORF difference_pa = {};
				GW::MATH::GVector::SubtractVectorF(
					p,
					a,
					difference_pa);

				float dot1 = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_pa,
					dot1);

				float dot2 = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ca,
					difference_pa,
					dot2);

				// p is closest to vertex a
				if(dot1 <= G_COLLISION_THRESHOLD_F && dot2 <= G_COLLISION_THRESHOLD_F)
				{
					_outPoint = a;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORF difference_pb = {};
				GW::MATH::GVector::SubtractVectorF(
					p,
					b,
					difference_pb);

				float dot3 = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_pb,
					dot3);

				float dot4 = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ca,
					difference_pb,
					dot4);

				// p is closest to vertex b
				if(dot3 >= G_COLLISION_THRESHOLD_F && dot4 <= dot3)
				{
					_outPoint = b;
					return GReturn::SUCCESS;
				}

                float vc = dot1 * dot4;
                vc -= dot3 * dot2; // split improves accuracy on ARM

				// p is closest to edge ab
				if(vc <= G_COLLISION_THRESHOLD_F && dot1 >= G_COLLISION_THRESHOLD_F && dot3 <= G_COLLISION_THRESHOLD_F)
				{
					float interval = dot1 / (dot1 - dot3);

					GW::MATH::GVector::ScaleF(
						difference_ba,
						interval,
						_outPoint);

					GW::MATH::GVector::AddVectorF(
						a,
						_outPoint,
						_outPoint);

					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORF difference_pc = {};
				GW::MATH::GVector::SubtractVectorF(
					p,
					c,
					difference_pc);

				float dot5 = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ca,
					difference_pc,
					dot5);

				float dot6 = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_pc,
					dot6);

				// p is closest to vertex c
				if(dot5 >= G_COLLISION_THRESHOLD_F && dot6 <= dot5)
				{
					_outPoint = c;
					return GReturn::SUCCESS;
				}

                float vb = dot6 * dot2;
                vb -= dot1 * dot5; // split improves accuracy on ARM
                
				// p is closest to edge ac
				if(vb <= G_COLLISION_THRESHOLD_F && dot2 >= G_COLLISION_THRESHOLD_F && dot5 <= G_COLLISION_THRESHOLD_F)
				{
					float interval = dot2 / (dot2 - dot5);

					GW::MATH::GVector::ScaleF(
						difference_ca,
						interval,
						_outPoint);

					GW::MATH::GVector::AddVectorF(
						a,
						_outPoint,
						_outPoint);

					return GReturn::SUCCESS;
				}

                float va = dot3 * dot5;
                va -= dot6 * dot4; // split improves accuracy on ARM
                
				// p is closest to edge bc
				if(va <= G_COLLISION_THRESHOLD_F && (dot4 - dot3) >= G_COLLISION_THRESHOLD_F && (dot6 - dot5) >= G_COLLISION_THRESHOLD_F)
				{
					float h = dot4 - dot3;
					float interval = h / (h + dot6 - dot5);

					GW::MATH::GVector::SubtractVectorF(
						c,
						b,
						_outPoint);

					GW::MATH::GVector::ScaleF(
						_outPoint,
						interval,
						_outPoint);

					GW::MATH::GVector::AddVectorF(
						b,
						_outPoint,
						_outPoint);

					return GReturn::SUCCESS;
				}

				// p is closest to some point on the face
				float denom = 1.0f / (va + vb + vc);
				float v = vb * denom;
				float w = vc * denom;

				GW::MATH::GVector::ScaleF(
					difference_ba,
					v,
					_outPoint);

				GW::MATH::GVECTORF acw = {};
				GW::MATH::GVector::ScaleF(
					difference_ca,
					w,
					acw);

				GW::MATH::GVector::AddVectorF(
					acw,
					_outPoint,
					_outPoint);

				GW::MATH::GVector::AddVectorF(
					a,
					_outPoint,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToPlaneF(const MATH::GPLANEF _plane, const MATH::GVECTORF _queryPoint,
												MATH::GVECTORF& _outPoint)
			{
				MATH::GVECTORF plane_normal = _plane.data.xyz();
				MATH::GVECTORF query_point = _queryPoint.xyz();

				float interval = 0.0f;
				GW::MATH::GVector::DotF(
					plane_normal,
					query_point,
					interval);

				interval -= _plane.distance;

				GW::MATH::GVector::ScaleF(
					plane_normal,
					interval,
					_outPoint);

				GW::MATH::GVector::SubtractVectorF(
					query_point,
					_outPoint,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToSphereF(const MATH::GSPHEREF _sphere, const MATH::GVECTORF _queryPoint,
												 MATH::GVECTORF& _outPoint)
			{
				GW::MATH::GVECTORF direction = {};
				GW::MATH::GVector::SubtractVectorF(
					_queryPoint,
					_sphere.data,
					direction);

				if(GW::MATH::GVector::NormalizeF(direction.xyz(), direction) == GReturn::FAILURE)
				{
					_outPoint = _queryPoint;
					return GReturn::FAILURE;
				}

				GW::MATH::GVector::ScaleF(
					direction,
					_sphere.radius,
					direction);

				GW::MATH::GVector::AddVectorF(
					_sphere.data.xyz(),
					direction,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToCapsuleF(const MATH::GCAPSULEF _capsule, const MATH::GVECTORF _queryPoint,
												  MATH::GVECTORF& _outPoint)
			{
				ClosestPointToLineF(
					{{{_capsule.data[0].xyz(), _capsule.data[1].xyz()}}},
					_queryPoint,
					_outPoint);

				return ClosestPointToSphereF(
					{{{_outPoint.x, _outPoint.y, _outPoint.z, _capsule.radius}}},
					_queryPoint,
					_outPoint);
			}

			static GReturn ClosestPointToAABBF(const MATH::GAABBMMF _aabb, const MATH::GVECTORF _queryPoint,
											   MATH::GVECTORF& _outPoint)
			{
				for(int i = 0; i < 3; i++)
				{
					_outPoint.data[i] = _queryPoint.data[i];

					_outPoint.data[i] = G_LARGER(
						_outPoint.data[i],
						_aabb.min.data[i]);

					_outPoint.data[i] = G_SMALLER(
						_outPoint.data[i],
						_aabb.max.data[i]);
				}

				if(G_ABS(_queryPoint.x - _outPoint.x) < G_COLLISION_THRESHOLD_F &&
				   G_ABS(_queryPoint.y - _outPoint.y) < G_COLLISION_THRESHOLD_F &&
				   G_ABS(_queryPoint.z - _outPoint.z) < G_COLLISION_THRESHOLD_F)
				{
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToOBBF(const MATH::GOBBF _obb, const MATH::GVECTORF _queryPoint,
											  MATH::GVECTORF& _outPoint)
			{
				float xx2 = 2.0f * _obb.rotation.x * _obb.rotation.x;
				float yy2 = 2.0f * _obb.rotation.y * _obb.rotation.y;
				float zz2 = 2.0f * _obb.rotation.z * _obb.rotation.z;
				float xy2 = 2.0f * _obb.rotation.x * _obb.rotation.y;
				float xz2 = 2.0f * _obb.rotation.x * _obb.rotation.z;
				float yz2 = 2.0f * _obb.rotation.y * _obb.rotation.z;
				float wx2 = 2.0f * _obb.rotation.w * _obb.rotation.x;
				float wy2 = 2.0f * _obb.rotation.w * _obb.rotation.y;
				float wz2 = 2.0f * _obb.rotation.w * _obb.rotation.z;

				MATH::GVECTORF obb_rotation[3] =
				{
					{{{1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2}}},
					{{{xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2}}},
					{{{xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2}}}
				};

				MATH::GVECTORF difference = {};
				GW::MATH::GVector::SubtractVectorF(
					_queryPoint.xyz(),
					_obb.data->xyz(),
					difference);

				_outPoint = _obb.center.xyz();

				MATH::GVECTORF world_coord = {};

				float distance = 0.0f;

				for(int i = 0; i < 3; i++)
				{
					// Project the difference to this axis
					GW::MATH::GVector::DotF(
						difference,
						obb_rotation[i],
						distance);

					// Clamp to boundary
					distance = G_SMALLER(
						distance,
						_obb.extent.data[i]);

					distance = G_LARGER(
						distance,
						-_obb.extent.data[i]);

					GW::MATH::GVector::ScaleF(
						obb_rotation[i],
						distance,
						world_coord);

					GW::MATH::GVector::AddVectorF(
						_outPoint,
						world_coord,
						_outPoint);
				}

				if(G_ABS(_queryPoint.x - _outPoint.x) < G_COLLISION_THRESHOLD_F &&
				   G_ABS(_queryPoint.y - _outPoint.y) < G_COLLISION_THRESHOLD_F &&
				   G_ABS(_queryPoint.z - _outPoint.z) < G_COLLISION_THRESHOLD_F)
				{
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn ComputeSphereFromPointsF(const MATH::GVECTORF* _pointCloud, const unsigned int _pointsCount, MATH::GSPHEREF& _outSphere)
			{
				if(!_pointCloud || _pointsCount < 2)
				{
					return GReturn::FAILURE;
				}

				// Find a good almost bounding sphere
				unsigned int min_x = 0;
				unsigned int max_x = 0;
				unsigned int min_y = 0;
				unsigned int max_y = 0;
				unsigned int min_z = 0;
				unsigned int max_z = 0;

				for(unsigned int i = 1; i < _pointsCount; i++)
				{
					if(_pointCloud[i].x < _pointCloud[min_x].x)
					{
						min_x = i;
					}
					if(_pointCloud[i].x > _pointCloud[max_x].x)
					{
						max_x = i;
					}

					if(_pointCloud[i].y < _pointCloud[min_y].y)
					{
						min_y = i;
					}
					if(_pointCloud[i].y > _pointCloud[max_y].y)
					{
						max_y = i;
					}

					if(_pointCloud[i].z < _pointCloud[min_z].z)
					{
						min_z = i;
					}
					if(_pointCloud[i].z > _pointCloud[max_z].z)
					{
						max_z = i;
					}
				}

				GW::MATH::GVECTORF difference = {};
				GW::MATH::GVector::SubtractVectorF(
					_pointCloud[max_x],
					_pointCloud[min_x],
					difference);

				float sq_distance_x = 0.0f;
				GW::MATH::GVector::DotF(
					difference,
					difference,
					sq_distance_x);

				GW::MATH::GVector::SubtractVectorF(
					_pointCloud[max_y],
					_pointCloud[min_y],
					difference);

				float sq_distance_y = 0.0f;
				GW::MATH::GVector::DotF(
					difference,
					difference,
					sq_distance_y);

				GW::MATH::GVector::SubtractVectorF(
					_pointCloud[max_z],
					_pointCloud[min_z],
					difference);

				float sq_distance_z = 0.0f;
				GW::MATH::GVector::DotF(
					difference,
					difference,
					sq_distance_z);

				int min = min_x;
				int max = max_x;

				if((sq_distance_y > sq_distance_x) && (sq_distance_y > sq_distance_z))
				{
					min = min_y;
					max = max_y;
				}

				if((sq_distance_z > sq_distance_x) && (sq_distance_z > sq_distance_y))
				{
					min = min_z;
					max = max_z;
				}

				GW::MATH::GVector::AddVectorF(
					_pointCloud[min].xyz(),
					_pointCloud[max].xyz(),
					_outSphere.data);

				GW::MATH::GVector::ScaleF(
					_outSphere.data,
					0.5f,
					_outSphere.data);

				GW::MATH::GVector::SubtractVectorF(
					_pointCloud[max].xyz(),
					_outSphere.data,
					difference);

				GW::MATH::GVector::DotF(
					difference,
					difference,
					_outSphere.radius);

				_outSphere.radius = sqrtf(_outSphere.radius);

				// Improve the bound and include all points
				auto UpdateSphereBounds = [&](const MATH::GVECTORF p, MATH::GSPHEREF& outSphere)
				{
					GW::MATH::GVECTORF difference = {};
					GW::MATH::GVector::SubtractVectorF(
						p.xyz(),
						outSphere.data.xyz(),
						difference);

					float sq_distance = 0.0f;
					GW::MATH::GVector::DotF(
						difference,
						difference,
						sq_distance);

					if(sq_distance > outSphere.radius* outSphere.radius)
					{
						float distance = sqrtf(sq_distance);
						float radius = (outSphere.radius + distance) * 0.5f;
						float increase = (radius - outSphere.radius) / distance;

						GW::MATH::GVector::ScaleF(
							difference,
							increase,
							difference);

						GW::MATH::GVector::AddVectorF(
							outSphere.data,
							difference,
							outSphere.data);

						outSphere.radius = radius;
					}
				};

				for(unsigned int i = 0; i < _pointsCount; i++)
				{
					UpdateSphereBounds(
						_pointCloud[i],
						_outSphere);
				}

				return GReturn::SUCCESS;
			}

			static GReturn ComputeAABBFromPointsF(const MATH::GVECTORF* _pointCloud, const unsigned int _pointsCount,
												  MATH::GAABBMMF& _outAABB)
			{
				if(!_pointCloud || _pointsCount == 0)
				{
					return GReturn::FAILURE;
				}

				_outAABB.min = _pointCloud[0];
				_outAABB.max = _pointCloud[0];

				for(unsigned int i = 1; i < _pointsCount; i++)
				{
					if(_outAABB.min.x > _pointCloud[i].x)
					{
						_outAABB.min.x = _pointCloud[i].x;
					}
					if(_outAABB.max.x < _pointCloud[i].x)
					{
						_outAABB.max.x = _pointCloud[i].x;
					}

					if(_outAABB.min.y > _pointCloud[i].y)
					{
						_outAABB.min.y = _pointCloud[i].y;
					}
					if(_outAABB.max.y < _pointCloud[i].y)
					{
						_outAABB.max.y = _pointCloud[i].y;
					}

					if(_outAABB.min.z > _pointCloud[i].z)
					{
						_outAABB.min.z = _pointCloud[i].z;
					}
					if(_outAABB.max.z < _pointCloud[i].z)
					{
						_outAABB.max.z = _pointCloud[i].z;
					}
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToLineF(const MATH::GVECTORF _point, const MATH::GLINEF _line, GCollisionCheck& _outResult)
			{
				MATH::GVECTORF point = _point.xyz();

				MATH::GLINEF line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					line.end,
					line.start,
					difference_ba);

				float distance_ba = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_ba,
					distance_ba);
				distance_ba = sqrtf(distance_ba);

				GW::MATH::GVECTORF difference_pa = {};
				GW::MATH::GVector::SubtractVectorF(
					point,
					line.start,
					difference_pa);

				float distance_pa = 0.0f;
				GW::MATH::GVector::DotF(
					difference_pa,
					difference_pa,
					distance_pa);
				distance_pa = sqrtf(distance_pa);

				GW::MATH::GVECTORF difference_bp = {};
				GW::MATH::GVector::SubtractVectorF(
					line.end,
					point,
					difference_bp);

				float distance_bp = 0.0f;
				GW::MATH::GVector::DotF(
					difference_bp,
					difference_bp,
					distance_bp);
				distance_bp = sqrtf(distance_bp);

				_outResult = (G_WITHIN_STANDARD_DEVIATION_F(distance_ba, distance_pa + distance_bp)) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToRayF(const MATH::GVECTORF _point, const MATH::GRAYF _ray, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF difference_pa = {};
				GW::MATH::GVector::SubtractVectorF(
					_point.xyz(),
					_ray.position.xyz(),
					difference_pa);

				// Point is ray position
				if(GW::MATH::GVector::NormalizeF(difference_pa, difference_pa) == GReturn::FAILURE)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(difference_pa.x - _ray.direction.x) < G_COLLISION_THRESHOLD_F &&
				   G_ABS(difference_pa.y - _ray.direction.y) < G_COLLISION_THRESHOLD_F &&
				   G_ABS(difference_pa.z - _ray.direction.z) < G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToTriangleF(const MATH::GVECTORF _point, const MATH::GTRIANGLEF _triangle,
												GCollisionCheck& _outResult, MATH::GVECTORF* _outBarycentric)
			{
				MATH::GPLANEF plane = {};
				if(ComputePlaneF(_triangle.a, _triangle.b, _triangle.c, plane) == GReturn::FAILURE)
				{
					// Degenerate triangle.
					return GReturn::FAILURE;
				}

				TestPointToPlaneF(
					_point,
					plane,
					_outResult);

				if(static_cast<int>(_outResult) > 0)
				{
					MATH::GVECTORF barycentric = {};
					// Compute plane already handles fail case of degenerate triangle.
					BarycentricF(_triangle.a, _triangle.b, _triangle.c, _point, barycentric);

					_outResult = (G_GREATER_OR_EQUAL_STANDARD_F(barycentric.y, 0.0f) &&
                                  G_GREATER_OR_EQUAL_STANDARD_F(barycentric.z, 0.0f) &&
                                  G_LESSER_OR_EQUAL_STANDARD_F(barycentric.y + barycentric.z, 1.0f)) ?
						GCollisionCheck::COLLISION :
						GCollisionCheck::NO_COLLISION;

					if(_outBarycentric)
					{
						*_outBarycentric = barycentric;
					}
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToPlaneF(const MATH::GVECTORF _point, const MATH::GPLANEF _plane, GCollisionCheck& _outResult)
			{
				float distance = 0.0f;
				GW::MATH::GVector::DotF(
					_point.xyz(),
					_plane.data.xyz(),
					distance);
				distance = distance - _plane.distance;

				if(distance < -G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::BELOW;
				}
				else if(G_ABS(distance) <= G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck::ABOVE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToSphereF(const MATH::GVECTORF _point, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_point,
					_sphere.data.xyz(),
					difference_ba);

				float sq_distance = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_ba,
					sq_distance);

				float sq_radius = _sphere.radius * _sphere.radius;

				_outResult = sq_distance <= sq_radius ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToCapsuleF(const MATH::GVECTORF _point, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				GW::MATH::GSPHEREF sphere = {};

				if(ClosestPointToCapsuleF(_capsule, _point, sphere.data) == GReturn::FAILURE)
				{
					// Point is center of capsule
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				sphere.radius = _capsule.radius;
				return TestPointToSphereF(
					_point,
					sphere,
					_outResult);
			}

			static GReturn TestPointToAABBF(const MATH::GVECTORF _point, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				if(G_ABS(_aabb.center.x - _point.x) - _aabb.extent.x > G_COLLISION_THRESHOLD_F ||
				   G_ABS(_aabb.center.y - _point.y) - _aabb.extent.y > G_COLLISION_THRESHOLD_F ||
				   G_ABS(_aabb.center.z - _point.z) - _aabb.extent.z > G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToOBBF(const MATH::GVECTORF _point, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF closest_point = {};

				ClosestPointToOBBF(
					_obb,
					_point,
					closest_point);

				if(G_ABS(closest_point.x - _point.x) > G_COLLISION_THRESHOLD_F ||
				   G_ABS(closest_point.y - _point.y) > G_COLLISION_THRESHOLD_F ||
				   G_ABS(closest_point.z - _point.z) > G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToLineF(const MATH::GLINEF _line1, const MATH::GLINEF _line2, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF closest_point1 = {};
				GW::MATH::GVECTORF closest_point2 = {};

				ClosestPointsToLineFromLineF(
					_line1,
					_line2,
					closest_point1,
					closest_point2);

				if(G_ABS(closest_point1.x - closest_point2.x) > G_COLLISION_THRESHOLD_F ||
				   G_ABS(closest_point1.y - closest_point2.y) > G_COLLISION_THRESHOLD_F ||
				   G_ABS(closest_point1.z - closest_point2.z) > G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToRayF(const MATH::GLINEF _line, const MATH::GRAYF _ray, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF closest_point1 = {};
				ClosestPointToRayF(
					_ray,
					_line.start,
					closest_point1);

				GW::MATH::GVECTORF closest_point2 = {};
				ClosestPointToRayF(
					_ray,
					_line.end,
					closest_point2);

				return TestLineToLineF(
					_line,
					{{{closest_point1, closest_point2}}},
					_outResult);
			}

			static GReturn TestLineToTriangleF(const MATH::GLINEF _line, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult,
											   MATH::GVECTORF* _outBarycentric)
			{
				MATH::GLINEF line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				MATH::GTRIANGLEF triangle =
				{
					{
						{
							_triangle.a.xyz(),
							_triangle.b.xyz(),
							_triangle.c.xyz()
						}
					}
				};

				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle.b,
					triangle.a,
					difference_ba);

				GW::MATH::GVECTORF difference_ca = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle.c,
					triangle.a,
					difference_ca);

				GW::MATH::GVECTORF norm = {};
				GW::MATH::GVector::CrossVector3F(
					difference_ca,
					difference_ba,
					norm);

				GW::MATH::GVECTORF difference_line = {};
				GW::MATH::GVector::SubtractVectorF(
					line.start,
					line.end,
					difference_line);

				float dot = 0.0f;
				GW::MATH::GVector::DotF(
					difference_line,
					norm,
					dot);

				// Segment is parallel to triangle
				if(dot <= 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORF difference_pa = {};
				GW::MATH::GVector::SubtractVectorF(
					line.start,
					triangle.a,
					difference_pa);

				float interval = 0.0f;
				GW::MATH::GVector::DotF(
					difference_pa,
					norm,
					interval);

				if(interval < 0.0f || interval > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORF cross = {};
				GW::MATH::GVector::CrossVector3F(
					difference_line,
					difference_pa,
					cross);

				// Test to see if within bounds of barycentric coordinates.
				GW::MATH::GVECTORF barycentric = {};
				GW::MATH::GVector::DotF(
					difference_ca,
					cross,
					barycentric.y);
				barycentric.y = -barycentric.y;

				if(barycentric.y < 0.0f || barycentric.y > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::DotF(
					difference_ba,
					cross,
					barycentric.z);

				if(barycentric.z < 0.0f || barycentric.y + barycentric.z > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(_outBarycentric)
				{
					float over_denom = 1.0f / dot;

					barycentric.y *= over_denom;
					barycentric.z *= over_denom;

					*_outBarycentric =
					{
						{
							{
								1.0f - barycentric.y - barycentric.z,
								barycentric.y,
								barycentric.z
							}
						}
					};
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestLineToPlaneF(const MATH::GLINEF _line, const MATH::GPLANEF _plane, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_line.end.xyz(),
					_line.start.xyz(),
					difference_ba);

				float denom = 0.0f;
				GW::MATH::GVector::DotF(
					_plane.data.xyz(),
					difference_ba,
					denom);

				float num = 0.0f;
				GW::MATH::GVector::DotF(
					_plane.data.xyz(),
					_line.start.xyz(),
					num);

				if(G_ABS(denom) < G_COLLISION_THRESHOLD_F)
				{
					// Coplanar
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				float interval = (_plane.distance - num) / denom;

				if(interval >= 0.0f && interval <= 1.0f)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					// Half plane test
					num -= _plane.distance;
					_outResult = (num < 0.0f) ?
						_outResult = GCollisionCheck::BELOW :
						_outResult = GCollisionCheck::ABOVE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToSphereF(const MATH::GLINEF _line, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF closest_point = {};
				ClosestPointToLineF(
					_line,
					_sphere.data,
					closest_point);

				return TestPointToSphereF(
					closest_point,
					_sphere,
					_outResult);
			}

			static GReturn TestLineToCapsuleF(const MATH::GLINEF _line, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF closest_point1 = {};
				GW::MATH::GVECTORF closest_point2 = {};
				ClosestPointsToLineFromLineF(
					_line,
					{{{_capsule.data[0],_capsule.data[1]}}},
					closest_point1,
					closest_point2);

				return TestPointToSphereF(
					closest_point1,
					{{{closest_point2.x,closest_point2.y,closest_point2.z,_capsule.radius}}},
					_outResult);
			}

			static GReturn TestLineToAABBF(const MATH::GLINEF _line, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				MATH::GLINEF line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				GW::MATH::GVECTORF extent = _aabb.extent.xyz();

				GW::MATH::GVECTORF line_midpoint = {};
				GW::MATH::GVector::AddVectorF(
					line.start,
					line.end,
					line_midpoint);

				GW::MATH::GVector::ScaleF(
					line_midpoint,
					0.5f,
					line_midpoint);

				GW::MATH::GVECTORF line_mid_length = {};
				GW::MATH::GVector::SubtractVectorF(
					line.end,
					line_midpoint,
					line_mid_length);

				// Translate box and line to origin
				GW::MATH::GVECTORF origin = {};
				GW::MATH::GVector::SubtractVectorF(
					line_midpoint,
					_aabb.center.xyz(),
					origin);

				GW::MATH::GVECTORF axis =
				{
					{
						{
							G_ABS(line_mid_length.x),
							G_ABS(line_mid_length.y),
							G_ABS(line_mid_length.z)
						}
					}
				};

				// Separating axes
				for(int i = 0; i < 3; i++)
				{
					if(G_ABS(origin.data[i]) > extent.data[i] + axis.data[i])
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				axis.x += G_COLLISION_THRESHOLD_F;
				axis.y += G_COLLISION_THRESHOLD_F;
				axis.z += G_COLLISION_THRESHOLD_F;

				if(G_ABS(origin.y * line_mid_length.z - origin.z * line_mid_length.y) >
				   extent.y* axis.z + extent.z * axis.y)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(origin.z * line_mid_length.x - origin.x * line_mid_length.z) >
				   extent.x* axis.z + extent.z * axis.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(origin.x * line_mid_length.z - origin.y * line_mid_length.x) >
				   extent.x* axis.y + extent.z * axis.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestLineToOBBF(const MATH::GLINEF _line, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				MATH::GLINEF line =
				{
					{
						{
							_line.start,
							_line.end
						}
					}
				};
				line.start.w = 1.0f;
				line.end.w = 1.0f;

				MATH::GMATRIXF obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionF(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXF obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0f}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0f}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0f}}},
							{{{0.0f, 0.0f, 0.0f, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					line.start,
					line.start);
				GW::MATH::GVector::AddVectorF(
					line.start,
					_obb.center,
					line.start);

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					line.end,
					line.end);
				GW::MATH::GVector::AddVectorF(
					line.end,
					_obb.center,
					line.end);

				return TestLineToAABBF(
					line,
					{{{_obb.center, _obb.extent}}},
					_outResult);
			}

			static GReturn TestRayToTriangleF(const MATH::GRAYF _ray, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult,
											  MATH::GVECTORF* _outBarycentric)
			{
				MATH::GRAYF ray =
				{
					{
						{
						_ray.position.xyz(),
							_ray.direction.xyz()
						}
					}
				};

				MATH::GTRIANGLEF triangle =
				{
					{
						{
							_triangle.a.xyz(),
							_triangle.b.xyz(),
							_triangle.c.xyz()
						}
					}
				};

				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle.b,
					triangle.a,
					difference_ba);

				GW::MATH::GVECTORF difference_ca = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle.c,
					triangle.a,
					difference_ca);

				GW::MATH::GVECTORF q = {};
				GW::MATH::GVector::CrossVector3F(
					ray.direction,
					difference_ca,
					q);

				float det = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					q,
					det);

				// Ray is parallel to or points away from triangle
				if(G_ABS(det) < G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				float denom = 1.0f / det;

				GW::MATH::GVECTORF s = {};
				GW::MATH::GVector::SubtractVectorF(
					ray.position,
					triangle.a,
					s);

				GW::MATH::GVECTORF barycentric = {};
				GW::MATH::GVector::DotF(
					s,
					q,
					barycentric.y);
				barycentric.y *= denom;

				if(barycentric.y < -G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORF r = {};
				GW::MATH::GVector::CrossVector3F(
					s,
					difference_ba,
					r);

				GW::MATH::GVector::DotF(
					ray.direction,
					r,
					barycentric.z);
				barycentric.z *= denom;

				if(barycentric.z < -G_COLLISION_THRESHOLD_F || barycentric.y + barycentric.z > 1.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(_outBarycentric)
				{
					*_outBarycentric = barycentric;
					_outBarycentric->x = 1.0f - barycentric.y - barycentric.z;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestRayToPlaneF(const MATH::GRAYF _ray, const MATH::GPLANEF _plane, GCollisionCheck& _outResult)
			{
				float denom = 0.0f;
				GW::MATH::GVector::DotF(
					_plane.data.xyz(),
					_ray.direction.xyz(),
					denom);

				if(G_ABS(denom) < G_COLLISION_THRESHOLD_F)
				{
					// Ray is parallel
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				float num = 0.0f;
				GW::MATH::GVector::DotF(
					_plane.data.xyz(),
					_ray.position.xyz(),
					num);

				float interval = (_plane.distance - num) / denom;
				if(interval >= 0.0f)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					// Half plane test
					num -= _plane.distance;
					_outResult = (num < 0.0f) ?
						_outResult = GCollisionCheck::BELOW :
						_outResult = GCollisionCheck::ABOVE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestRayToSphereF(const MATH::GRAYF _ray, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult)
			{
				MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_ray.position.xyz(),
					_sphere.data.xyz(),
					difference_ba);

				float c = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_ba,
					c);
				c -= _sphere.radius * _sphere.radius;

				// At least one real root.
				if(c <= 0.0f)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				float b = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					_ray.direction.xyz(),
					b);

				// Ray is outside sphere and pointing away from it.
				if(b > 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// The discriminant determines if there were solutions or not.
				float discriminant = b * b - c;
				_outResult = (discriminant < 0.0f) ?
					GCollisionCheck::NO_COLLISION :
					GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestRayToCapsuleF(const MATH::GRAYF _ray, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF closest_point1 = {};
				ClosestPointToRayF(
					_ray,
					_capsule.data[0],
					closest_point1);

				GW::MATH::GSPHEREF sphere = {};
				ClosestPointToRayF(
					_ray,
					_capsule.data[1],
					sphere.data);

				ClosestPointsToLineFromLineF(
					{{{closest_point1, sphere.data}}},
					{{{_capsule.data[0], _capsule.data[1]}}},
					closest_point1,
					sphere.data);

				sphere.radius = _capsule.radius;

				return TestPointToSphereF(
					closest_point1,
					sphere,
					_outResult);
			}

			static GReturn TestRayToAABBF(const MATH::GRAYF _ray, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult)
			{
				float interval1 = 0.0f;
				float interval2 = 0.0f;
				float interval3 = 0.0f;
				float interval4 = 0.0f;
				float interval5 = 0.0f;
				float interval6 = 0.0f;

				if(_ray.direction.x)
				{
					interval1 = (_aabb.min.x - _ray.position.x) / _ray.direction.x;
					interval2 = (_aabb.max.x - _ray.position.x) / _ray.direction.x;
				}
				else
				{
					interval1 = (_aabb.min.x - _ray.position.x) > 0.0f ?
						(FLT_MAX) : (FLT_MIN);
					interval2 = (_aabb.max.x - _ray.position.x) > 0.0f ?
						(FLT_MAX) : (FLT_MIN);
				}

				if(_ray.direction.y)
				{
					interval3 = (_aabb.min.y - _ray.position.y) / _ray.direction.y;
					interval4 = (_aabb.max.y - _ray.position.y) / _ray.direction.y;
				}
				else
				{
					interval3 = (_aabb.min.y - _ray.position.y) > 0.0f ?
						FLT_MAX : FLT_MIN;
					interval4 = (_aabb.max.y - _ray.position.y) > 0.0f ?
						FLT_MAX : FLT_MIN;
				}

				if(_ray.direction.z)
				{
					interval5 = (_aabb.min.z - _ray.position.z) / _ray.direction.z;
					interval6 = (_aabb.max.z - _ray.position.z) / _ray.direction.z;
				}
				else
				{
					interval5 = (_aabb.min.z - _ray.position.z) > 0.0f ?
						FLT_MAX : FLT_MIN;
					interval6 = (_aabb.max.z - _ray.position.z) > 0.0f ?
						FLT_MAX : FLT_MIN;
				}

				float interval_min = G_LARGER(G_LARGER(
					G_SMALLER(interval1, interval2),
					G_SMALLER(interval3, interval4)),
					G_SMALLER(interval5, interval6));

				float interval_max = G_SMALLER(G_SMALLER(
					G_LARGER(interval1, interval2),
					G_LARGER(interval3, interval4)),
					G_LARGER(interval5, interval6));

				if(interval_max < 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(interval_max < interval_min)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestRayToOBBF(const MATH::GRAYF _ray, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				MATH::GMATRIXF obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionF(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXF obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0f}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0f}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0f}}},
							{{{0.0f, 0.0f, 0.0f, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GRAYF ray =
				{
					{
						{
							_ray.position,
							_ray.direction.xyz()
						}
					}
				};
				ray.position.w = 1.0f;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					_ray.position,
					ray.position);
				GW::MATH::GVector::AddVectorF(
					ray.position,
					_obb.center,
					ray.position);

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					_ray.direction,
					ray.direction);
				GW::MATH::GVector::NormalizeF(
					ray.direction,
					ray.direction);

				MATH::GAABBMMF mm = {};
				ConvertAABBCEToAABBMMF(
					{{{_obb.center, _obb.extent}}},
					mm);

				return TestRayToAABBF(
					ray,
					mm,
					_outResult);
			}

			static GReturn TestTriangleToTriangleF(const MATH::GTRIANGLEF _triangle1, const MATH::GTRIANGLEF _triangle2,
												   GCollisionCheck& _outResult)
			{
				// Half plane test if triangle1 lies completely on one side to triangle2 plane
				GW::MATH::GVECTORF difference_b2a2 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle2.b.xyz(),
					_triangle2.a.xyz(),
					difference_b2a2);

				GW::MATH::GVECTORF difference_c2a2 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle2.c.xyz(),
					_triangle2.a.xyz(),
					difference_c2a2);

				GW::MATH::GVECTORF plane2 = {};
				GW::MATH::GVector::CrossVector3F(
					difference_c2a2,
					difference_b2a2,
					plane2);

				GW::MATH::GVECTORF difference_a1a2 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle1.a.xyz(),
					_triangle2.a.xyz(),
					difference_a1a2);

				GW::MATH::GVECTORF det1 = {};
				GW::MATH::GVector::DotF(
					difference_a1a2,
					plane2,
					det1.x);

				GW::MATH::GVECTORF difference_b1a2 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle1.b.xyz(),
					_triangle2.a.xyz(),
					difference_b1a2);

				GW::MATH::GVector::DotF(
					difference_b1a2,
					plane2,
					det1.y);

				GW::MATH::GVECTORF difference_c1a2 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle1.c.xyz(),
					_triangle2.a.xyz(),
					difference_c1a2);

				GW::MATH::GVector::DotF(
					difference_c1a2,
					plane2,
					det1.z);

				if(((det1.x * det1.y) > 0.0f) && ((det1.x * det1.z) > 0.0f))
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Half plane test if triangle2 lies completely on one side to triangle1 plane
				GW::MATH::GVECTORF difference_b1a1 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle1.b.xyz(),
					_triangle1.a.xyz(),
					difference_b1a1);

				GW::MATH::GVECTORF difference_c1a1 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle1.c.xyz(),
					_triangle1.a.xyz(),
					difference_c1a1);

				GW::MATH::GVECTORF plane1 = {};
				GW::MATH::GVector::CrossVector3F(
					difference_c1a1,
					difference_b1a1,
					plane1);

				GW::MATH::GVECTORF difference_a2a1 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle2.a.xyz(),
					_triangle1.a.xyz(),
					difference_a2a1);

				GW::MATH::GVECTORF det2 = {};
				GW::MATH::GVector::DotF(
					difference_a2a1,
					plane1,
					det2.x);

				GW::MATH::GVECTORF difference_b2a1 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle2.b.xyz(),
					_triangle1.a.xyz(),
					difference_b2a1);

				GW::MATH::GVector::DotF(
					difference_b2a1,
					plane1,
					det2.y);

				GW::MATH::GVECTORF difference_c2a1 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle2.c.xyz(),
					_triangle1.a.xyz(),
					difference_c2a1);

				GW::MATH::GVector::DotF(
					difference_c2a1,
					plane1,
					det2.z);

				if(((det2.x * det2.y) > 0.0f) && ((det2.x * det2.z) > 0.0f))
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Bring both triangles into canonical form
				MATH::GTRIANGLEF triangle1 = {};
				MATH::GTRIANGLEF triangle2 = {};

				if(det1.x > 0.0f)
				{
					if(det1.y > 0.0f)
					{
						triangle1 =
						{
							{
								{
									_triangle1.c,
									_triangle1.a,
									_triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.c,
							_triangle2.b
								}
							}
						};
					}
					else if(det1.z > 0.0f)
					{
						triangle1 =
						{
							{
								{
									_triangle1.b,
									_triangle1.c,
									_triangle1.a
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.c,
									_triangle2.b
								}
							}
						};
					}
					else
					{
						triangle1 =
						{
							{
								{
									_triangle1.a,
									_triangle1.b,
									_triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.b,
									_triangle2.c
								}
							}
						};
					}
				}
				else if(det1.x < 0.0f)
				{
					if(det1.y < 0.0f)
					{
						triangle1 =
						{
							{
								{
									_triangle1.c,
									_triangle1.a,
									_triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.b,
									_triangle2.c
								}
							}
						};
					}
					else if(det1.z < 0.0f)
					{
						triangle1 =
						{
							{
								{
									_triangle1.b,
									_triangle1.c,
									_triangle1.a
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.b,
									_triangle2.c
								}
							}
						};
					}
					else
					{
						triangle1 =
						{
							{
								{
									_triangle1.a,
									_triangle1.b,
									_triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.c,
									_triangle2.b
								}
							}
						};
					}
				}
				else
				{
					if(det1.y < 0.0f)
					{
						if(det1.z >= 0.0f)
						{
							triangle1 =
							{
								{
									{
										_triangle1.b,
										_triangle1.c,
										_triangle1.a
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.c,
										_triangle2.b
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										_triangle1.a,
										_triangle1.b,
										_triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.b,
										_triangle2.c
									}
								}
							};
						}
					}
					else if(det1.y > 0.0f)
					{
						if(det1.z > 0.0f)
						{
							triangle1 =
							{
								{
									{
										_triangle1.a,
										_triangle1.b,
										_triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.c,
										_triangle2.b
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										_triangle1.b,
										_triangle1.c,
										_triangle1.a
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.b,
										_triangle2.c
									}
								}
							};
						}
					}
					else
					{
						if(det1.z > 0.0f)
						{
							triangle1 =
							{
								{
									{
										_triangle1.c,
										_triangle1.a,
										_triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.b,
										_triangle2.c
									}
								}
							};
						}
						else if(det1.z < 0.0f)
						{
							triangle1 =
							{
								{
									{
										_triangle1.c,
										_triangle1.a,
										_triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.c,
										_triangle2.b
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										_triangle1.a,
										_triangle1.b,
										_triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.b,
										_triangle2.c
									}
								}
							};

							// return coplanar result
							auto Orient2D = [](const float* a, const float* b, const float* c)
							{
								return ((a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]));
							};

							auto TestVertex2D = [&](
								const float* a1, const float* b1, const float* c1,
								const float* a2, const float* b2, const float* c2)
							{
								if(Orient2D(c2, a2, b1) >= 0.0f)
								{
									if(Orient2D(c2, b2, b1) <= 0.0f)
									{
										if(Orient2D(a1, a2, b1) > 0.0f)
										{
											return (Orient2D(a1, b2, b1) <= 0.0f) ? 1 : 0;
										}
										else
										{
											if(Orient2D(a1, a2, c1) >= 0.0f)
											{
												return (Orient2D(b1, c1, a2) >= 0.0f) ? 1 : 0;
											}
											else return 0;
										}
									}
									else
									{
										if(Orient2D(a1, b2, b1) <= 0.0f)
										{
											if(Orient2D(c2, b2, c1) <= 0.0f)
											{
												return (Orient2D(b1, c1, b2) >= 0.0f) ? 1 : 0;
											}
											else return 0;
										}
										else return 0;
									}
								}
								else
								{
									if(Orient2D(c2, a2, c1) >= 0.0f)
									{
										if(Orient2D(b1, c1, c2) >= 0.0f)
										{
											return (Orient2D(a1, a2, c1) >= 0.0f) ? 1 : 0;
										}
										else
										{
											if(Orient2D(b1, c1, b2) >= 0.0f)
											{
												return (Orient2D(c2, c1, b2) >= 0.0f) ? 1 : 0;
											}
											else return 0;
										}
									}
									else  return 0;
								}
							};

							auto TestEdge2D = [&](
								const float* a1, const float* b1, const float* c1,
								const float* a2, const float* b2, const float* c2)
							{
								if(Orient2D(c2, a2, b1) >= 0.0f)
								{
									if(Orient2D(a1, a2, b1) >= 0.0f)
									{
										return (Orient2D(a1, b1, c2) >= 0.0f) ? 1 : 0;
									}
									else
									{
										if(Orient2D(b1, c1, a2) >= 0.0f)
										{
											return (Orient2D(c1, a1, a2) >= 0.0f) ? 1 : 0;
										}
										else return 0;
									}
								}
								else
								{
									if(Orient2D(c2, a2, c1) >= 0.0f)
									{
										if(Orient2D(a1, a2, c1) >= 0.0f)
										{
											if(Orient2D(a1, c1, c2) >= 0.0f) return 1;
											else
											{
												return (Orient2D(b1, c1, c2) >= 0.0f) ? 1 : 0;
											}
										}
										else  return 0;
									}
									else return 0;
								}
							};

							auto TestTriangles2D = [&](
								const float* a1, const float* b1, const float* c1,
								const float* a2, const float* b2, const float* c2)
							{
								if(Orient2D(a2, b2, a1) >= 0.0f)
								{
									if(Orient2D(b2, c2, a1) >= 0.0f)
									{
										if(Orient2D(c2, a2, a1) >= 0.0f)
										{
											return 1;
										}
										else
										{
											return TestEdge2D(
												a1, b1, c1,
												a2, b2, c2);
										}
									}
									else
									{
										if(Orient2D(c2, a2, a1) >= 0.0f)
										{
											return TestEdge2D(
												a1, b1, c1,
												c2, a2, b2);
										}
										else
										{
											return TestVertex2D(
												a1, b1, c1,
												a2, b2, c2);
										}
									}
								}
								else
								{
									if(Orient2D(b2, c2, a1) >= 0.0f)
									{
										if(Orient2D(c2, a2, a1) >= 0.0f)
										{
											return TestEdge2D(
												a1, b1, c1,
												b2, c2, a2);
										}
										else
										{
											return TestVertex2D(
												a1, b1, c1,
												b2, c2, a2);
										}
									}
									else
									{
										return TestVertex2D(
											a1, b1, c1,
											c2, a2, b2);
									}
								}
							};

							GW::MATH::GVECTORF normal =
							{
								{
									{
										G_ABS(plane1.x),
										G_ABS(plane1.y),
										G_ABS(plane1.z)
									}
								}
							};

							GW::MATH::GTRIANGLEF tri2_1 = {};
							GW::MATH::GTRIANGLEF tri2_2 = {};

							if((normal.x > normal.z) && (normal.x >= normal.y))
							{
								// Project onto yz
								tri2_1.a.x = triangle1.b.z;
								tri2_1.a.y = triangle1.b.y;
								tri2_1.b.x = triangle1.a.z;
								tri2_1.b.y = triangle1.a.y;
								tri2_1.c.x = triangle1.c.z;
								tri2_1.c.y = triangle1.c.y;

								tri2_2.a.x = triangle2.b.z;
								tri2_2.a.y = triangle2.b.y;
								tri2_2.b.x = triangle2.a.z;
								tri2_2.b.y = triangle2.a.y;
								tri2_2.c.x = triangle2.c.z;
								tri2_2.c.y = triangle2.c.y;
							}
							else if((normal.y > normal.z) && (normal.y >= normal.x))
							{
								// Project onto xz
								tri2_1.a.x = triangle1.b.x;
								tri2_1.a.y = triangle1.b.z;
								tri2_1.b.x = triangle1.a.x;
								tri2_1.b.y = triangle1.a.z;
								tri2_1.c.x = triangle1.c.x;
								tri2_1.c.y = triangle1.c.z;

								tri2_2.a.x = triangle2.b.x;
								tri2_2.a.y = triangle2.b.z;
								tri2_2.b.x = triangle2.a.x;
								tri2_2.b.y = triangle2.a.z;
								tri2_2.c.x = triangle2.c.x;
								tri2_2.c.y = triangle2.c.z;
							}
							else
							{
								// Project onto xy
								tri2_1.a.x = triangle1.a.x;
								tri2_1.a.y = triangle1.a.y;
								tri2_1.b.x = triangle1.b.x;
								tri2_1.b.y = triangle1.b.y;
								tri2_1.c.x = triangle1.c.x;
								tri2_1.c.y = triangle1.c.y;

								tri2_2.a.x = triangle2.a.x;
								tri2_2.a.y = triangle2.a.y;
								tri2_2.b.x = triangle2.b.x;
								tri2_2.b.y = triangle2.b.y;
								tri2_2.c.x = triangle2.c.x;
								tri2_2.c.y = triangle2.c.y;
							}

							if(Orient2D(tri2_1.a.data, tri2_1.b.data, tri2_1.c.data) < 0.0f)
							{
								if(Orient2D(tri2_1.a.data, tri2_1.b.data, tri2_1.c.data) < 0.0f)
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data,
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data));
								}
								else
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data,
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data));
								}
							}
							else
							{
								if(Orient2D(tri2_2.a.data, tri2_2.b.data, tri2_2.c.data) < 0.0f)
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data,
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data));
								}
								else
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data,
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data));
								}
							}

							return GReturn::SUCCESS;
						}
					}
				}

				if(det2.x > 0.0f)
				{
					if(det2.y > 0.0f)
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.c,
									triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.c,
									triangle2.a,
									triangle2.b
								}
							}
						};
					}
					else if(det2.z > 0.0f)
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.c,
									triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.b,
									triangle2.c,
									triangle2.a
								}
							}
						};
					}
					else
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.b,
									triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.a,
									triangle2.b,
									triangle2.c
								}
							}
						};
					}
				}
				else if(det2.x < 0.0f)
				{
					if(det2.y < 0.0f)
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.b,
									triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.c,
									triangle2.a,
									triangle2.b
								}
							}
						};
					}
					else if(det2.z < 0.0f)
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.b,
									triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.b,
									triangle2.c,
									triangle2.a
								}
							}
						};
					}
					else
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.c,
									triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.a,
									triangle2.b,
									triangle2.c
								}
							}
						};
					}
				}
				else
				{
					if(det2.y < 0.0f)
					{
						if(det2.z >= 0.0f)
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.c,
										triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.b,
										triangle2.c,
										triangle2.a
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.b,
										triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.a,
										triangle2.b,
										triangle2.c
									}
								}
							};
						}
					}
					else if(det2.y > 0.0f)
					{
						if(det2.z > 0.0f)
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.c,
										triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.a,
										triangle2.b,
										triangle2.c
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.b,
										triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.b,
										triangle2.c,
										triangle2.a
									}
								}
							};
						}
					}
					else
					{
						if(det2.z > 0.0f)
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.b,
										triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.c,
										triangle2.a,
										triangle2.b
									}
								}
							};
						}
						else if(det2.z < 0.0f)
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.c,
										triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.c,
										triangle2.a,
										triangle2.b
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.b,
										triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.a,
										triangle2.b,
										triangle2.c
									}
								}
							};

							// return coplanar result
							auto Orient2D = [](const float* a, const float* b, const float* c)
							{
								return ((a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]));
							};

							auto TestVertex2D = [&](
								const float* a1, const float* b1, const float* c1,
								const float* a2, const float* b2, const float* c2)
							{
								if(Orient2D(c2, a2, b1) >= 0.0f)
								{
									if(Orient2D(c2, b2, b1) <= 0.0f)
									{
										if(Orient2D(a1, a2, b1) > 0.0f)
										{
											return (Orient2D(a1, b2, b1) <= 0.0f) ? 1 : 0;
										}
										else
										{
											if(Orient2D(a1, a2, c1) >= 0.0f)
											{
												return (Orient2D(b1, c1, a2) >= 0.0f) ? 1 : 0;
											}
											else return 0;
										}
									}
									else
									{
										if(Orient2D(a1, b2, b1) <= 0.0f)
										{
											if(Orient2D(c2, b2, c1) <= 0.0f)
											{
												return (Orient2D(b1, c1, b2) >= 0.0f) ? 1 : 0;
											}
											else return 0;
										}
										else return 0;
									}
								}
								else
								{
									if(Orient2D(c2, a2, c1) >= 0.0f)
									{
										if(Orient2D(b1, c1, c2) >= 0.0f)
										{
											return (Orient2D(a1, a2, c1) >= 0.0f) ? 1 : 0;
										}
										else
										{
											if(Orient2D(b1, c1, b2) >= 0.0f)
											{
												return (Orient2D(c2, c1, b2) >= 0.0f) ? 1 : 0;
											}
											else return 0;
										}
									}
									else  return 0;
								}
							};

							auto TestEdge2D = [&](
								const float* a1, const float* b1, const float* c1,
								const float* a2, const float* b2, const float* c2)
							{
								if(Orient2D(c2, a2, b1) >= 0.0f)
								{
									if(Orient2D(a1, a2, b1) >= 0.0f)
									{
										return (Orient2D(a1, b1, c2) >= 0.0f) ? 1 : 0;
									}
									else
									{
										if(Orient2D(b1, c1, a2) >= 0.0f)
										{
											return (Orient2D(c1, a1, a2) >= 0.0f) ? 1 : 0;
										}
										else return 0;
									}
								}
								else
								{
									if(Orient2D(c2, a2, c1) >= 0.0f)
									{
										if(Orient2D(a1, a2, c1) >= 0.0f)
										{
											if(Orient2D(a1, c1, c2) >= 0.0f) return 1;
											else
											{
												return (Orient2D(b1, c1, c2) >= 0.0f) ? 1 : 0;
											}
										}
										else  return 0;
									}
									else return 0;
								}
							};

							auto TestTriangles2D = [&](
								const float* a1, const float* b1, const float* c1,
								const float* a2, const float* b2, const float* c2)
							{
								if(Orient2D(a2, b2, a1) >= 0.0f)
								{
									if(Orient2D(b2, c2, a1) >= 0.0f)
									{
										if(Orient2D(c2, a2, a1) >= 0.0f)
										{
											return 1;
										}
										else
										{
											return TestEdge2D(
												a1, b1, c1,
												a2, b2, c2);
										}
									}
									else
									{
										if(Orient2D(c2, a2, a1) >= 0.0f)
										{
											return TestEdge2D(
												a1, b1, c1,
												c2, a2, b2);
										}
										else
										{
											return TestVertex2D(
												a1, b1, c1,
												a2, b2, c2);
										}
									}
								}
								else
								{
									if(Orient2D(b2, c2, a1) >= 0.0f)
									{
										if(Orient2D(c2, a2, a1) >= 0.0f)
										{
											return TestEdge2D(
												a1, b1, c1,
												b2, c2, a2);
										}
										else
										{
											return TestVertex2D(
												a1, b1, c1,
												b2, c2, a2);
										}
									}
									else
									{
										return TestVertex2D(
											a1, b1, c1,
											c2, a2, b2);
									}
								}
							};

							GW::MATH::GVECTORF normal =
							{
								{
									{
										G_ABS(plane1.x),
										G_ABS(plane1.y),
										G_ABS(plane1.z)
									}
								}
							};

							GW::MATH::GTRIANGLEF tri2_1 = {};
							GW::MATH::GTRIANGLEF tri2_2 = {};

							if((normal.x > normal.z) && (normal.x >= normal.y))
							{
								// Project onto yz
								tri2_1.a.x = triangle1.b.z;
								tri2_1.a.y = triangle1.b.y;
								tri2_1.b.x = triangle1.a.z;
								tri2_1.b.y = triangle1.a.y;
								tri2_1.c.x = triangle1.c.z;
								tri2_1.c.y = triangle1.c.y;

								tri2_2.a.x = triangle2.b.z;
								tri2_2.a.y = triangle2.b.y;
								tri2_2.b.x = triangle2.a.z;
								tri2_2.b.y = triangle2.a.y;
								tri2_2.c.x = triangle2.c.z;
								tri2_2.c.y = triangle2.c.y;
							}
							else if((normal.y > normal.z) && (normal.y >= normal.x))
							{
								// Project onto xz
								tri2_1.a.x = triangle1.b.x;
								tri2_1.a.y = triangle1.b.z;
								tri2_1.b.x = triangle1.a.x;
								tri2_1.b.y = triangle1.a.z;
								tri2_1.c.x = triangle1.c.x;
								tri2_1.c.y = triangle1.c.z;

								tri2_2.a.x = triangle2.b.x;
								tri2_2.a.y = triangle2.b.z;
								tri2_2.b.x = triangle2.a.x;
								tri2_2.b.y = triangle2.a.z;
								tri2_2.c.x = triangle2.c.x;
								tri2_2.c.y = triangle2.c.z;
							}
							else
							{
								// Project onto xy
								tri2_1.a.x = triangle1.a.x;
								tri2_1.a.y = triangle1.a.y;
								tri2_1.b.x = triangle1.b.x;
								tri2_1.b.y = triangle1.b.y;
								tri2_1.c.x = triangle1.c.x;
								tri2_1.c.y = triangle1.c.y;

								tri2_2.a.x = triangle2.a.x;
								tri2_2.a.y = triangle2.a.y;
								tri2_2.b.x = triangle2.b.x;
								tri2_2.b.y = triangle2.b.y;
								tri2_2.c.x = triangle2.c.x;
								tri2_2.c.y = triangle2.c.y;
							}

							if(Orient2D(tri2_1.a.data, tri2_1.b.data, tri2_1.c.data) < 0.0f)
							{
								if(Orient2D(tri2_1.a.data, tri2_1.b.data, tri2_1.c.data) < 0.0f)
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data,
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data));
								}
								else
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data,
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data));
								}
							}
							else
							{
								if(Orient2D(tri2_2.a.data, tri2_2.b.data, tri2_2.c.data) < 0.0f)
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data,
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data));
								}
								else
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data,
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data));
								}
							}

							return GReturn::SUCCESS;
						}
					}
				}

				// Edges confirmed to form a line where the triangle planes meet. Check if triangles are within an interval.
				GW::MATH::GVECTORF difference_a2b1 = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle2.a,
					triangle1.b,
					difference_a2b1);

				GW::MATH::GVECTORF difference_a1b1 = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle1.a,
					triangle1.b,
					difference_a1b1);

				GW::MATH::GVector::CrossVector3F(
					difference_a1b1,
					difference_a2b1,
					plane1);

				GW::MATH::GVECTORF difference_b2b1 = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle2.b,
					triangle1.b,
					difference_b2b1);

				float dot1 = 0.0f;
				GW::MATH::GVector::DotF(
					difference_b2b1,
					plane1,
					dot1);

				if(dot1 > 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::SubtractVectorF(
					triangle2.a,
					triangle1.a,
					difference_a2a1);

				GW::MATH::GVector::SubtractVectorF(
					triangle1.c,
					triangle1.a,
					difference_c1a1);

				GW::MATH::GVector::CrossVector3F(
					difference_c1a1,
					difference_a2a1,
					plane1);

				GW::MATH::GVector::SubtractVectorF(
					triangle2.c,
					triangle1.a,
					difference_c2a1);

				GW::MATH::GVector::DotF(
					difference_c2a1,
					plane1,
					dot1);

				if(dot1 > 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestTriangleToPlaneF(const MATH::GTRIANGLEF _triangle, const MATH::GPLANEF _plane,
												GCollisionCheck& _outResult)
			{
				GCollisionCheck plane_test;

				TestPointToPlaneF(
					_triangle.a,
					_plane,
					plane_test);
				_outResult = plane_test;

				if (static_cast<int>(_outResult) > 0)
				{
					return GReturn::SUCCESS;
				}

				TestPointToPlaneF(
					_triangle.b,
					_plane,
					plane_test);

				if(_outResult != plane_test)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				TestPointToPlaneF(
					_triangle.c,
					_plane,
					plane_test);

				if(_outResult != plane_test)
				{
					_outResult = GCollisionCheck::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestTriangleToSphereF(const MATH::GTRIANGLEF _triangle, const MATH::GSPHEREF _sphere,
												 GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF closest_point = {};
				ClosestPointToTriangleF(
					_triangle,
					_sphere.data,
					closest_point);

				return TestPointToSphereF(
					closest_point,
					_sphere,
					_outResult);
			}

			static GReturn TestTriangleToCapsuleF(const MATH::GTRIANGLEF _triangle, const MATH::GCAPSULEF _capsule,
												  GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF closest_point1 = {};
				ClosestPointToTriangleF(
					_triangle,
					_capsule.data[0],
					closest_point1);

				GW::MATH::GVECTORF closest_point2 = {};
				ClosestPointToTriangleF(
					_triangle,
					_capsule.data[1],
					closest_point2);

				return TestLineToCapsuleF(
					{{{closest_point1, closest_point2}}},
					_capsule,
					_outResult);
			}

			static GReturn TestTriangleToAABBF(const MATH::GTRIANGLEF _triangle, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF right = {{{1,0,0,0}}};
				GW::MATH::GVECTORF up = {{{0,1,0,0}}};
				GW::MATH::GVECTORF forward = {{{0,0,1,0}}};

				GW::MATH::GVECTORF v0 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle.a.xyz(),
					_aabb.center.xyz(),
					v0);

				GW::MATH::GVECTORF v1 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle.b.xyz(),
					_aabb.center.xyz(),
					v1);

				GW::MATH::GVECTORF v2 = {};
				GW::MATH::GVector::SubtractVectorF(
					_triangle.c.xyz(),
					_aabb.center.xyz(),
					v2);

				GW::MATH::GVECTORF f0 = {};
				GW::MATH::GVector::SubtractVectorF(
					v1,
					v0,
					f0);

				GW::MATH::GVECTORF f1 = {};
				GW::MATH::GVector::SubtractVectorF(
					v2,
					v1,
					f1);

				GW::MATH::GVECTORF f2 = {};
				GW::MATH::GVector::SubtractVectorF(
					v0,
					v2,
					f2);

				GW::MATH::GVECTORF axis[9] = {};

				GW::MATH::GVector::CrossVector3F(
					right,
					f0,
					axis[0]);

				GW::MATH::GVector::CrossVector3F(
					up,
					f0,
					axis[1]);

				GW::MATH::GVector::CrossVector3F(
					forward,
					f0,
					axis[2]);

				GW::MATH::GVector::CrossVector3F(
					right,
					f1,
					axis[3]);

				GW::MATH::GVector::CrossVector3F(
					up,
					f1,
					axis[4]);

				GW::MATH::GVector::CrossVector3F(
					forward,
					f1,
					axis[5]);

				GW::MATH::GVector::CrossVector3F(
					right,
					f2,
					axis[6]);

				GW::MATH::GVector::CrossVector3F(
					up,
					f2,
					axis[7]);

				GW::MATH::GVector::CrossVector3F(
					forward,
					f2,
					axis[8]);

				float p0 = 0.0f;
				float p1 = 0.0f;
				float p2 = 0.0f;
				float r = 0.0f;
				GW::MATH::GVECTORF dots = {};

				for(int i = 0; i < 9; i++)
				{
					GW::MATH::GVector::DotF(
						v0,
						axis[i],
						p0);
					GW::MATH::GVector::DotF(
						v1,
						axis[i],
						p1);
					GW::MATH::GVector::DotF(
						v2,
						axis[i],
						p2);

					GW::MATH::GVector::DotF(
						right,
						axis[i],
						dots.x);
					GW::MATH::GVector::DotF(
						up,
						axis[i],
						dots.y);
					GW::MATH::GVector::DotF(
						forward,
						axis[i],
						dots.z);

					r =
						_aabb.extent.x * G_ABS(dots.x) +
						_aabb.extent.y * G_ABS(dots.y) +
						_aabb.extent.z * G_ABS(dots.z);

					if(G_LARGER(-G_LARGER(p0, -G_LARGER(p1, p2)),
								G_SMALLER(p0, G_SMALLER(p1, p2))) > r)
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				// Test the 3 axis corresponding to the face normals of the AABB
				if(G_LARGER(v0.x, G_LARGER(v1.x, v2.x)) < -_aabb.extent.x ||
				   G_SMALLER(v0.x, G_SMALLER(v1.x, v2.x)) > _aabb.extent.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_LARGER(v0.y, G_LARGER(v1.y, v2.y)) < -_aabb.extent.y ||
				   G_SMALLER(v0.y, G_SMALLER(v1.y, v2.y)) > _aabb.extent.y)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_LARGER(v0.z, G_LARGER(v1.z, v2.z)) < -_aabb.extent.z ||
				   G_SMALLER(v0.z, G_SMALLER(v1.z, v2.z)) > _aabb.extent.z)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test separating axis to triangle face normal
				GW::MATH::GPLANEF plane;

				ComputePlaneF(
					_triangle.a,
					_triangle.b,
					_triangle.c,
					plane);

				return TestPlaneToAABBF(
					plane,
					_aabb,
					_outResult);
			}

			static GReturn TestTriangleToOBBF(const MATH::GTRIANGLEF _triangle, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				MATH::GMATRIXF obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionF(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXF obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0f}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0f}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0f}}},
							{{{0.0f, 0.0f, 0.0f, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GTRIANGLEF triangle =
				{
					{
						{
							_triangle.a,
							_triangle.b,
							_triangle.c
						}
					}
				};
				triangle.a.w = 1.0f;
				triangle.b.w = 1.0f;
				triangle.c.w = 1.0f;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					triangle.a,
					triangle.a);
				GW::MATH::GVector::AddVectorF(
					triangle.a,
					_obb.center,
					triangle.a);

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					triangle.b,
					triangle.b);
				GW::MATH::GVector::AddVectorF(
					triangle.b,
					_obb.center,
					triangle.b);

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					triangle.c,
					triangle.c);
				GW::MATH::GVector::AddVectorF(
					triangle.c,
					_obb.center,
					triangle.c);

				return TestTriangleToAABBF(
					triangle,
					{{{_obb.center, _obb.extent}}},
					_outResult);
			}

			static GReturn TestPlaneToPlaneF(const MATH::GPLANEF _plane1, const MATH::GPLANEF _plane2, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF cross = {};
				GW::MATH::GVector::CrossVector3F(
					_plane1.data.xyz(),
					_plane2.data.xyz(),
					cross);

				float dot = 0.0f;
				GW::MATH::GVector::DotF(
					cross,
					cross,
					dot);

				_outResult = (dot < G_COLLISION_THRESHOLD_F) ?
					GCollisionCheck::NO_COLLISION :
					GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPlaneToSphereF(const MATH::GPLANEF _plane, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult)
			{
				float distance = 0.0f;
				GW::MATH::GVector::DotF(
					_sphere.data.xyz(),
					_plane.data.xyz(),
					distance);
				distance -= _plane.distance;

				if(G_ABS(distance) <= _sphere.radius)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					_outResult = (distance > 0.0f) ?
						GCollisionCheck::ABOVE :
						GCollisionCheck::BELOW;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPlaneToCapsuleF(const MATH::GPLANEF _plane, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult)
			{
				float distance_start = 0.0f;
				GW::MATH::GVector::DotF(
					_capsule.data[0].xyz(),
					_plane.data.xyz(),
					distance_start);
				distance_start -= _plane.distance;

				float distance_end = 0.0f;
				GW::MATH::GVector::DotF(
					_capsule.data[1].xyz(),
					_plane.data.xyz(),
					distance_end);
				distance_end -= _plane.distance;

				if(distance_start * distance_end < 0.0f)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(distance_start) <= _capsule.radius || G_ABS(distance_end) <= _capsule.radius)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = (distance_start > 0.0f) ?
					GCollisionCheck::ABOVE :
					GCollisionCheck::BELOW;
				return GReturn::SUCCESS;

			}

			static GReturn TestPlaneToAABBF(const MATH::GPLANEF _plane, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF plane_normal = _plane.data.xyz();

				float interval =
					_aabb.extent.x * G_ABS(_plane.x) +
					_aabb.extent.y * G_ABS(_plane.y) +
					_aabb.extent.z * G_ABS(_plane.z);

				float distance = 0.0f;
				GW::MATH::GVector::DotF(
					plane_normal,
					_aabb.center.xyz(),
					distance);
				distance -= _plane.distance;

				if(G_ABS(distance) <= interval)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					_outResult = (distance > 0.0f) ?
						GCollisionCheck::ABOVE :
						GCollisionCheck::BELOW;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPlaneToOBBF(const MATH::GPLANEF _plane, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF plane_normal = _plane.data.xyz();

				float xx2 = 2.0f * _obb.rotation.x * _obb.rotation.x;
				float yy2 = 2.0f * _obb.rotation.y * _obb.rotation.y;
				float zz2 = 2.0f * _obb.rotation.z * _obb.rotation.z;

				float xy2 = 2.0f * _obb.rotation.x * _obb.rotation.y;
				float xz2 = 2.0f * _obb.rotation.x * _obb.rotation.z;
				float yz2 = 2.0f * _obb.rotation.y * _obb.rotation.z;

				float wx2 = 2.0f * _obb.rotation.w * _obb.rotation.x;
				float wy2 = 2.0f * _obb.rotation.w * _obb.rotation.y;
				float wz2 = 2.0f * _obb.rotation.w * _obb.rotation.z;

				MATH::GVECTORF obb_rotation[3] =
				{
					{{{1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2}}},
					{{{xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2}}},
					{{{xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2}}}
				};

				float interval = 0.0f;
				float s = 0.0f;

				for(int i = 0; i < 3; i++)
				{
					GW::MATH::GVector::DotF(
						plane_normal,
						obb_rotation[i],
						s);

					interval += _obb.extent.data[i] * G_ABS(s);
				}

				GW::MATH::GVector::DotF(
					plane_normal,
					_obb.center.xyz(),
					s);

				s -= _plane.distance;

				if(G_ABS(s) <= interval)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					_outResult = (s > 0.0f) ?
						GCollisionCheck::ABOVE :
						GCollisionCheck::BELOW;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestSphereToSphereF(const MATH::GSPHEREF _sphere1, const MATH::GSPHEREF _sphere2, GCollisionCheck& _outResult)
			{
				MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_sphere1.data.xyz(),
					_sphere2.data.xyz(),
					difference_ba);

				float sq_distance = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_ba,
					sq_distance);

				float sq_radii = _sphere1.radius + _sphere2.radius;
				sq_radii *= sq_radii;

				_outResult = (sq_distance <= sq_radii) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestSphereToCapsuleF(const MATH::GSPHEREF _sphere, const MATH::GCAPSULEF _capsule,
												GCollisionCheck& _outResult)
			{
				// Compute squared distance between the sphere center and capsule's start and end.
				float sq_distance = 0.0f;
				SqDistancePointToLineF(
					_sphere.data,
					{{{_capsule.data[0], _capsule.data[1]}}},
					sq_distance);

				float sq_radii = _sphere.radius + _capsule.radius;
				sq_radii *= sq_radii;

				_outResult = (sq_distance <= sq_radii) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestSphereToAABBF(const MATH::GSPHEREF _sphere, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				MATH::GAABBMMF mm = {};
				ConvertAABBCEToAABBMMF(
					_aabb,
					mm);

				float sq_distance = 0.0f;
				SqDistancePointToAABBF(
					_sphere.data.xyz(),
					mm,
					sq_distance);

				float sq_radius = _sphere.radius * _sphere.radius;

				_outResult = (sq_distance <= sq_radius) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestSphereToOBBF(const MATH::GSPHEREF _sphere, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORF closest_point = {};
				ClosestPointToOBBF(
					_obb,
					_sphere.data.xyz(),
					closest_point);

				GW::MATH::GVECTORF difference = {};
				GW::MATH::GVector::SubtractVectorF(
					closest_point,
					_sphere.data.xyz(),
					difference);

				float sq_radius = _sphere.radius * _sphere.radius;

				float sq_distance = 0.0f;
				GW::MATH::GVector::DotF(
					difference,
					difference,
					sq_distance);

				_outResult = (sq_distance <= sq_radius) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestCapsuleToCapsuleF(const MATH::GCAPSULEF _capsule1, const MATH::GCAPSULEF _capsule2,
												 GCollisionCheck& _outResult)
			{
				MATH::GVECTORF closest1 = {};
				MATH::GVECTORF closest2 = {};

				ClosestPointsToLineFromLineF(
					{{{_capsule1.data[0], _capsule1.data[1]}}},
					{{{_capsule2.data[0], _capsule2.data[1]}}},
					closest1,
					closest2);

				MATH::GVECTORF difference_ab = {};
				GW::MATH::GVector::SubtractVectorF(
					closest1,
					closest2,
					difference_ab);

				float sq_distance = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ab,
					difference_ab,
					sq_distance);

				float sq_radii = _capsule1.radius + _capsule2.radius;
				sq_radii *= sq_radii;

				_outResult = (sq_distance <= sq_radii) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestCapsuleToAABBF(const MATH::GCAPSULEF _capsule, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult)
			{
				// Expand AABB extents by capsule's radius.
				GW::MATH::GVECTORF center = _aabb.center.xyz();
				GW::MATH::GVECTORF extent = _aabb.extent.xyz();
				extent.x += _capsule.radius;
				extent.y += _capsule.radius;
				extent.z += _capsule.radius;

				GW::MATH::GVECTORF line_midpoint = {};
				GW::MATH::GVector::AddVectorF(
					_capsule.data[0].xyz(),
					_capsule.data[1].xyz(),
					line_midpoint);

				GW::MATH::GVector::ScaleF(
					line_midpoint,
					0.5f,
					line_midpoint);

				GW::MATH::GVECTORF line_mid_length = {};
				GW::MATH::GVector::SubtractVectorF(
					_capsule.data[1].xyz(),
					line_midpoint,
					line_mid_length);

				// Translate box and line to origin
				GW::MATH::GVECTORF o = {};
				GW::MATH::GVector::SubtractVectorF(
					line_midpoint,
					center,
					o);

				GW::MATH::GVECTORF axis =
				{
					{
						{
							G_ABS(line_mid_length.x),
							G_ABS(line_mid_length.y),
							G_ABS(line_mid_length.z)
						}
					}
				};

				// Separating axes
				for(int i = 0; i < 3; i++)
				{
					if(G_ABS(o.data[i]) > extent.data[i] + axis.data[i])
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				axis.x += G_COLLISION_THRESHOLD_F;
				axis.y += G_COLLISION_THRESHOLD_F;
				axis.z += G_COLLISION_THRESHOLD_F;

				if(G_ABS(o.y * line_mid_length.z - o.z * line_mid_length.y) >
				   extent.y* axis.z + extent.z * axis.y)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(o.z * line_mid_length.x - o.x * line_mid_length.z) >
				   extent.x* axis.z + extent.z * axis.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(o.x * line_mid_length.z - o.y * line_mid_length.x) >
				   extent.x* axis.y + extent.z * axis.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestCapsuleToOBBF(const MATH::GCAPSULEF _capsule, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				MATH::GMATRIXF obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionF(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXF obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0f}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0f}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0f}}},
							{{{0.0f, 0.0f, 0.0f, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GCAPSULEF capsule = {};
				capsule.data[0] = _capsule.data[0];
				capsule.data[0].w = 1.0f;
				capsule.data[1] = _capsule.data[1];
				capsule.data[1].w = 1.0f;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					capsule.data[0],
					capsule.data[0]);
				GW::MATH::GVector::AddVectorF(
					capsule.data[0],
					_obb.center,
					capsule.data[0]);

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					capsule.data[1],
					capsule.data[1]);
				GW::MATH::GVector::AddVectorF(
					capsule.data[1],
					_obb.center,
					capsule.data[1]);

				capsule.radius = _capsule.radius;

				return TestCapsuleToAABBF(
					capsule,
					{{{_obb.center, _obb.extent}}},
					_outResult);
			}

			static GReturn TestAABBToAABBF(const MATH::GAABBCEF _aabb1, const MATH::GAABBCEF _aabb2, GCollisionCheck& _outResult)
			{
				if(G_ABS(_aabb1.center.x - _aabb2.center.x) > (_aabb1.extent.x + _aabb2.extent.x) ||
				   G_ABS(_aabb1.center.y - _aabb2.center.y) > (_aabb1.extent.y + _aabb2.extent.y) ||
				   G_ABS(_aabb1.center.z - _aabb2.center.z) > (_aabb1.extent.z + _aabb2.extent.z))
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestAABBToOBBF(const MATH::GAABBCEF _aabb, const MATH::GOBBF _obb, GCollisionCheck& _outResult)
			{
				MATH::GOBBF obb =
				{
					{
						{
							_aabb.center,
							_aabb.extent,
							{{{0.0f, 0.0f, 0.0f, 1.0f}}}
						}
					}
				};

				return TestOBBToOBBF(
					obb,
					_obb,
					_outResult);
			}

			static GReturn TestOBBToOBBF(const MATH::GOBBF _obb1, const MATH::GOBBF _obb2, GCollisionCheck& _outResult)
			{
				// Convert quaternions into 3 axes for both OBB1 and OBB2.
				float xx2 = 2.0f * _obb1.rotation.x * _obb1.rotation.x;
				float yy2 = 2.0f * _obb1.rotation.y * _obb1.rotation.y;
				float zz2 = 2.0f * _obb1.rotation.z * _obb1.rotation.z;

				float xy2 = 2.0f * _obb1.rotation.x * _obb1.rotation.y;
				float xz2 = 2.0f * _obb1.rotation.x * _obb1.rotation.z;
				float yz2 = 2.0f * _obb1.rotation.y * _obb1.rotation.z;

				float wx2 = 2.0f * _obb1.rotation.w * _obb1.rotation.x;
				float wy2 = 2.0f * _obb1.rotation.w * _obb1.rotation.y;
				float wz2 = 2.0f * _obb1.rotation.w * _obb1.rotation.z;

				MATH::GVECTORF obb1_rotation[3] =
				{
					{{{1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2}}},
					{{{xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2}}},
					{{{xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2}}}
				};

				xx2 = 2.0f * _obb2.rotation.x * _obb2.rotation.x;
				yy2 = 2.0f * _obb2.rotation.y * _obb2.rotation.y;
				zz2 = 2.0f * _obb2.rotation.z * _obb2.rotation.z;

				xy2 = 2.0f * _obb2.rotation.x * _obb2.rotation.y;
				xz2 = 2.0f * _obb2.rotation.x * _obb2.rotation.z;
				yz2 = 2.0f * _obb2.rotation.y * _obb2.rotation.z;

				wx2 = 2.0f * _obb2.rotation.w * _obb2.rotation.x;
				wy2 = 2.0f * _obb2.rotation.w * _obb2.rotation.y;
				wz2 = 2.0f * _obb2.rotation.w * _obb2.rotation.z;

				MATH::GVECTORF obb2_rotation[3] =
				{
					{{{1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2}}},
					{{{xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2}}},
					{{{xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2}}}
				};

				float projected_radii = 0.0f;
				float projected_radii_1 = 0.0f;
				float projected_radii_2 = 0.0f;
				float test_axis = 0.0f;

				float rotation[3][3] = {};
				float abs_rotation[3][3] = {};

				for(int i = 0; i < 3; i++)
				{
					for(int j = 0; j < 3; j++)
					{
						GW::MATH::GVector::DotF(
							obb1_rotation[i],
							obb2_rotation[j],
							rotation[i][j]);
					}
				}

				// Get translation and then bring it into a's coordinate frame.
				MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_obb2.center,
					_obb1.center,
					difference_ba);

				MATH::GVECTORF translation_vec = {};
				GW::MATH::GVector::DotF(
					difference_ba,
					obb1_rotation[0],
					translation_vec.x);

				GW::MATH::GVector::DotF(
					difference_ba,
					obb1_rotation[1],
					translation_vec.y);

				GW::MATH::GVector::DotF(
					difference_ba,
					obb1_rotation[2],
					translation_vec.z);

				// Get common values while maintaining numerical precision.
				for(int i = 0; i < 3; i++)
				{
					for(int j = 0; j < 3; j++)
					{
						abs_rotation[i][j] =
							G_ABS(rotation[i][j]) + G_COLLISION_THRESHOLD_F;
					}
				}

				// Test axes for OBB1 x, y, z.
				for(int i = 0; i < 3; i++)
				{
					projected_radii_1 = _obb1.extent.data[i];

					projected_radii_2 =
						_obb2.extent.data[0] * abs_rotation[i][0] +
						_obb2.extent.data[1] * abs_rotation[i][1] +
						_obb2.extent.data[2] * abs_rotation[i][2];

					projected_radii = projected_radii_1 + projected_radii_2;

					test_axis = G_ABS(translation_vec.data[i]);

					if(test_axis > projected_radii)
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				// Test axes for OBB2 x, y, z.
				for(int i = 0; i < 3; i++)
				{
					projected_radii_1 =
						_obb1.extent.data[0] * abs_rotation[0][i] +
						_obb1.extent.data[1] * abs_rotation[1][i] +
						_obb1.extent.data[2] * abs_rotation[2][i];

					projected_radii_2 = _obb2.extent.data[i];

					projected_radii = projected_radii_1 + projected_radii_2;

					test_axis = G_ABS(
						translation_vec.data[0] * rotation[0][i] +
						translation_vec.data[1] * rotation[1][i] +
						translation_vec.data[2] * rotation[2][i]);

					if(test_axis > projected_radii)
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				// Test OBB1 x & OBB2 x
				projected_radii_1 =
					_obb1.extent.data[1] * abs_rotation[2][0] +
					_obb1.extent.data[2] * abs_rotation[1][0];

				projected_radii_2 =
					_obb2.extent.data[1] * abs_rotation[0][2] +
					_obb2.extent.data[2] * abs_rotation[0][1];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[2] * rotation[1][0] -
					translation_vec.data[1] * rotation[2][0]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 x & OBB2 y
				projected_radii_1 =
					_obb1.extent.data[1] * abs_rotation[2][1] +
					_obb1.extent.data[2] * abs_rotation[1][1];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[0][2] +
					_obb2.extent.data[2] * abs_rotation[0][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[2] * rotation[1][1] -
					translation_vec.data[1] * rotation[2][1]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 x & OBB2 z
				projected_radii_1 =
					_obb1.extent.data[1] * abs_rotation[2][2] +
					_obb1.extent.data[2] * abs_rotation[1][2];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[0][1] +
					_obb2.extent.data[1] * abs_rotation[0][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[2] * rotation[1][2] -
					translation_vec.data[1] * rotation[2][2]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 y & OBB2 x
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[2][0] +
					_obb1.extent.data[2] * abs_rotation[0][0];

				projected_radii_2 =
					_obb2.extent.data[1] * abs_rotation[1][2] +
					_obb2.extent.data[2] * abs_rotation[1][1];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[0] * rotation[2][0] -
					translation_vec.data[2] * rotation[0][0]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 y & OBB2 y
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[2][1] +
					_obb1.extent.data[2] * abs_rotation[0][1];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[1][2] +
					_obb2.extent.data[2] * abs_rotation[1][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[0] * rotation[2][1] -
					translation_vec.data[2] * rotation[0][1]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 y & OBB2 z
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[2][2] +
					_obb1.extent.data[2] * abs_rotation[0][2];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[1][1] +
					_obb2.extent.data[1] * abs_rotation[1][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[0] * rotation[2][2] -
					translation_vec.data[2] * rotation[0][2]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 z & OBB2 x
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[1][0] +
					_obb1.extent.data[1] * abs_rotation[0][0];

				projected_radii_2 =
					_obb2.extent.data[1] * abs_rotation[2][2] +
					_obb2.extent.data[2] * abs_rotation[2][1];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[1] * rotation[0][0] -
					translation_vec.data[0] * rotation[1][0]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 z & OBB2 y
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[1][1] +
					_obb1.extent.data[1] * abs_rotation[0][1];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[2][2] +
					_obb2.extent.data[2] * abs_rotation[2][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[1] * rotation[0][1] -
					translation_vec.data[0] * rotation[1][1]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 z & OBB2 z
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[1][2] +
					_obb1.extent.data[1] * abs_rotation[0][2];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[2][1] +
					_obb2.extent.data[1] * abs_rotation[2][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[1] * rotation[0][2] -
					translation_vec.data[0] * rotation[1][2]);

				_outResult = test_axis > projected_radii ?
					GCollisionCheck::NO_COLLISION :
					GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToTriangleF(const MATH::GLINEF _line, const MATH::GTRIANGLEF _triangle,
													GCollisionCheck& _outResult, MATH::GVECTORF& _outContactPoint,
													MATH::GVECTORF* _outBarycentric, MATH::GVECTORF& _outDirection,
													float& _outInterval)
			{
				MATH::GLINEF line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				MATH::GTRIANGLEF triangle =
				{
					{
						{
							_triangle.a.xyz(),
							_triangle.b.xyz(),
							_triangle.c.xyz()
						}
					}
				};

				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle.b,
					triangle.a,
					difference_ba);

				GW::MATH::GVECTORF difference_ca = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle.c,
					triangle.a,
					difference_ca);

				GW::MATH::GVECTORF norm = {};
				GW::MATH::GVector::CrossVector3F(
					difference_ca,
					difference_ba,
					norm);

				GW::MATH::GVECTORF difference_line = {};
				GW::MATH::GVector::SubtractVectorF(
					line.start,
					line.end,
					difference_line);

				float dot = 0.0f;
				GW::MATH::GVector::DotF(
					difference_line,
					norm,
					dot);

				// Segment is parallel to triangle
				if(dot <= 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORF difference_pa = {};
				GW::MATH::GVector::SubtractVectorF(
					line.start,
					triangle.a,
					difference_pa);

				float interval = 0.0f;
				GW::MATH::GVector::DotF(
					difference_pa,
					norm,
					interval);

				if(interval < 0.0f || interval > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORF cross = {};
				GW::MATH::GVector::CrossVector3F(
					difference_line,
					difference_pa,
					cross);

				// Test to see if within bounds of barycentric coordinates.
				GW::MATH::GVECTORF barycentric = {};
				GW::MATH::GVector::DotF(
					difference_ca,
					cross,
					barycentric.y);
				barycentric.y = -barycentric.y;

				if(barycentric.y < 0.0f || barycentric.y > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::DotF(
					difference_ba,
					cross,
					barycentric.z);

				if(barycentric.z < 0.0f || barycentric.y + barycentric.z > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				float over_denom = 1.0f / dot;
				barycentric.y *= over_denom;
				barycentric.z *= over_denom;
				barycentric.x = 1.0f - barycentric.y - barycentric.z;
				_outInterval = interval * over_denom;

				_outContactPoint =
				{
					{
						{
							barycentric.x * _triangle.a.x + barycentric.y * _triangle.b.x + barycentric.z * _triangle.c.x,
						barycentric.x * _triangle.a.y + barycentric.y * _triangle.b.y + barycentric.z * _triangle.c.y,
						barycentric.x * _triangle.a.z + barycentric.y * _triangle.b.z + barycentric.z * _triangle.c.z
						}
					}
				};

				if(_outBarycentric)
				{
					*_outBarycentric = barycentric;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToPlaneF(const MATH::GLINEF _line, const MATH::GPLANEF _plane, GCollisionCheck& _outResult,
												 MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection,
												 float& _outInterval)
			{
				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_line.end.xyz(),
					_line.start.xyz(),
					difference_ba);

				float denom = 0.0f;
				GW::MATH::GVector::DotF(
					_plane.data.xyz(),
					difference_ba,
					denom);

				if(G_ABS(denom) < G_COLLISION_THRESHOLD_F)
				{
					// Coplanar
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				float num = 0.0f;
				GW::MATH::GVector::DotF(
					_plane.data.xyz(),
					_line.start.xyz(),
					num);

				float interval = (_plane.distance - num) / denom;

				if(interval >= 0.0f && interval <= 1.0f)
				{
					_outResult = GCollisionCheck::COLLISION;

					_outDirection = difference_ba;
					GW::MATH::GVector::NormalizeF(
						_outDirection,
						_outDirection);

					GW::MATH::GVector::ScaleF(
						difference_ba,
						interval,
						_outContactPoint);

					GW::MATH::GVector::AddVectorF(
						_line.start.xyz(),
						_outContactPoint,
						_outContactPoint);

					_outInterval = interval;
				}
				else
				{
					num -= _plane.distance;

					_outResult = (num < 0.0f) ?
						_outResult = GCollisionCheck::BELOW :
						_outResult = GCollisionCheck::ABOVE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToSphereF(const MATH::GLINEF _line, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult,
												  MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection,
												  float& _outInterval)
			{
				MATH::GLINEF line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					line.start,
					_sphere.data.xyz(),
					difference_ba);

				GW::MATH::GVECTORF dir = {};
				GW::MATH::GVector::SubtractVectorF(
					line.end,
					line.start,
					dir);

				float sq_length = 0.0f;
				GW::MATH::GVector::DotF(
					dir,
					dir,
					sq_length);

				if(GW::MATH::GVector::NormalizeF(dir, dir) == GReturn::FAILURE)
				{
					return GReturn::FAILURE;
				}

				float b = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					dir,
					b);

				float c = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_ba,
					c);
				c -= _sphere.radius * _sphere.radius;

				if(c > 0.0f && b > 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				float discriminant = b * b - c;

				if(discriminant < 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outDirection = dir;
				_outInterval = -b - sqrtf(discriminant);

				if(_outInterval <= 0.0f)
				{
					// Line is contained in sphere.
					// Clamp to line start.
					_outInterval = 0.0f;
					_outContactPoint = line.start;
				}
				else if(_outInterval * _outInterval >= sq_length)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}
				else
				{
					GW::MATH::GVector::ScaleF(
						_outDirection,
						_outInterval,
						_outContactPoint);

					GW::MATH::GVector::AddVectorF(
						_outContactPoint,
						line.start,
						_outContactPoint);
				}

				_outResult = GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToCapsuleF(const MATH::GLINEF _line, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult,
												   MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection,
												   float& _outInterval)
			{
				GW::MATH::GVECTORF closest_point1 = {};
				GW::MATH::GVECTORF closest_point2 = {};
				ClosestPointsToLineFromLineF(
					_line,
					{{{_capsule.data[0], _capsule.data[1]}}},
					closest_point1,
					closest_point2);

				MATH::GSPHEREF sphere = {};
				sphere.data = closest_point2;
				sphere.radius = _capsule.radius;

				return IntersectLineToSphereF(
					_line,
					sphere,
					_outResult,
					_outContactPoint,
					_outDirection,
					_outInterval);
			}

			static GReturn IntersectLineToAABBF(const MATH::GLINEF _line, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult,
												MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection,
												float& _outInterval)
			{
				GW::MATH::GVector::SubtractVectorF(
					_line.end.xyz(),
					_line.start.xyz(),
					_outDirection);

				if(GW::MATH::GVector::NormalizeF(_outDirection, _outDirection) == GReturn::FAILURE)
				{
					return GReturn::FAILURE;
				}

				// TODO div by 0
				float interval1 = (_aabb.min.x - _line.start.x) / _outDirection.x;
				float interval2 = (_aabb.max.x - _line.start.x) / _outDirection.x;

				float interval3 = (_aabb.min.y - _line.start.y) / _outDirection.y;
				float interval4 = (_aabb.max.y - _line.start.y) / _outDirection.y;

				float interval5 = (_aabb.min.z - _line.start.z) / _outDirection.z;
				float interval6 = (_aabb.max.z - _line.start.z) / _outDirection.z;

				float interval_min = G_LARGER(G_LARGER(
					G_SMALLER(interval1, interval2),
					G_SMALLER(interval3, interval4)),
					G_SMALLER(interval5, interval6));

				float interval_max = G_SMALLER(G_SMALLER(
					G_LARGER(interval1, interval2),
					G_LARGER(interval3, interval4)),
					G_LARGER(interval5, interval6));

				if(interval_max < 0.0f || interval_min > 1.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(interval_max < interval_min)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outInterval = (interval_min < 0.0f) ?
					interval_max : interval_min;

				GW::MATH::GVector::ScaleF(
					_outDirection,
					_outInterval,
					_outContactPoint);

				GW::MATH::GVector::AddVectorF(
					_outContactPoint,
					_line.start.xyz(),
					_outContactPoint);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToOBBF(const MATH::GLINEF _line, const MATH::GOBBF _obb, GCollisionCheck& _outResult,
											   MATH::GVECTORF& _outContactPoint, MATH::GVECTORF& _outDirection,
											   float& _outInterval)
			{
				MATH::GLINEF line = {};
				line.start = _line.start;
				line.start.w = 1.0f;
				line.end = _line.end;
				line.end.w = 1.0f;

				MATH::GMATRIXF obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionF(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXF obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0f}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0f}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0f}}},
							{{{0.0f, 0.0f, 0.0f, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					line.start,
					line.start);
				GW::MATH::GVector::AddVectorF(
					line.start,
					_obb.center,
					line.start);

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					line.end,
					line.end);
				GW::MATH::GVector::AddVectorF(
					line.end,
					_obb.center,
					line.end);

				MATH::GAABBMMF mm = {};
				ConvertAABBCEToAABBMMF(
					{{{_obb.center, _obb.extent}}},
					mm);

				if(IntersectLineToAABBF(
					line,
					mm,
					_outResult,
					_outContactPoint,
					_outDirection,
					_outInterval) == GReturn::FAILURE)
				{
					return GReturn::FAILURE;
				}

				GMatrixImplementation::InverseF(
					obb_rotation_inverse,
					obb_rotation);

				GW::MATH::GVector::SubtractVectorF(
					_outContactPoint,
					_obb.center,
					_outContactPoint);

				_outContactPoint.w = 1.0F;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation,
					_outContactPoint,
					_outContactPoint);

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation,
					_outDirection.xyz(),
					_outDirection);

				_outContactPoint.w = 0.0F;

				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToTriangleF(const MATH::GRAYF _ray, const MATH::GTRIANGLEF _triangle, GCollisionCheck& _outResult,
												   MATH::GVECTORF& _outContactPoint, MATH::GVECTORF* _outBarycentric,
												   float& _outInterval)
			{
				MATH::GRAYF ray =
				{
					{
						{
							_ray.position.xyz(),
							_ray.direction.xyz()
						}
					}
				};

				MATH::GTRIANGLEF triangle =
				{
					{
						{
							_triangle.a.xyz(),
							_triangle.b.xyz(),
							_triangle.c.xyz()
						}
					}
				};

				GW::MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle.b,
					triangle.a,
					difference_ba);

				GW::MATH::GVECTORF difference_ca = {};
				GW::MATH::GVector::SubtractVectorF(
					triangle.c,
					triangle.a,
					difference_ca);

				GW::MATH::GVECTORF q = {};
				GW::MATH::GVector::CrossVector3F(
					ray.direction,
					difference_ca,
					q);

				float det = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					q,
					det);

				// Ray is parallel to or points away from triangle
				if(G_ABS(det) < G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				float denom = 1.0f / det;

				GW::MATH::GVECTORF s = {};
				GW::MATH::GVector::SubtractVectorF(
					ray.position,
					triangle.a,
					s);

				GW::MATH::GVECTORF barycentric = {};
				GW::MATH::GVector::DotF(
					s,
					q,
					barycentric.y);
				barycentric.y *= denom;

				if(barycentric.y < -G_COLLISION_THRESHOLD_F)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORF r = {};
				GW::MATH::GVector::CrossVector3F(
					s,
					difference_ba,
					r);

				GW::MATH::GVector::DotF(
					ray.direction,
					r,
					barycentric.z);
				barycentric.z *= denom;

				if(barycentric.z < -G_COLLISION_THRESHOLD_F || barycentric.y + barycentric.z > 1.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				barycentric.x = 1.0f - barycentric.y - barycentric.z;

				if(_outBarycentric)
				{
					*_outBarycentric = barycentric;
				}

				_outContactPoint =
				{
					{
						{
							barycentric.x * _triangle.a.x + barycentric.y * _triangle.b.x + barycentric.z * _triangle.c.x,
							barycentric.x * _triangle.a.y + barycentric.y * _triangle.b.y + barycentric.z * _triangle.c.y,
							barycentric.x * _triangle.a.z + barycentric.y * _triangle.b.z + barycentric.z * _triangle.c.z
						}
					}
				};

				GW::MATH::GVECTORF v = {};
				GW::MATH::GVector::SubtractVectorF(
					_outContactPoint,
					ray.position,
					v);
				GW::MATH::GVector::DotF(
					v,
					v,
					_outInterval);

				_outInterval = sqrtf(_outInterval);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToPlaneF(const MATH::GRAYF _ray, const MATH::GPLANEF _plane, GCollisionCheck& _outResult,
												MATH::GVECTORF& _outContactPoint,
												float& _outInterval)
			{
				float denom = 0.0f;
				GW::MATH::GVector::DotF(
					_plane.data.xyz(),
					_ray.direction.xyz(),
					denom);

				if(G_ABS(denom) < G_COLLISION_THRESHOLD_F)
				{
					// Coplanar
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				float num = 0.0f;
				GW::MATH::GVector::DotF(
					_plane.data.xyz(),
					_ray.position.xyz(),
					num);

				float interval = (_plane.distance - num) / denom;

				if(interval >= 0.0f)
				{
					_outResult = GCollisionCheck::COLLISION;

					GW::MATH::GVector::ScaleF(
						_ray.direction.xyz(),
						interval,
						_outContactPoint);

					GW::MATH::GVector::AddVectorF(
						_ray.position.xyz(),
						_outContactPoint,
						_outContactPoint);

					_outInterval = interval;
				}
				else
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToSphereF(const MATH::GRAYF _ray, const MATH::GSPHEREF _sphere, GCollisionCheck& _outResult,
												 MATH::GVECTORF& _outContactPoint,
												 float& _outInterval)
			{
				MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_ray.position.xyz(),
					_sphere.data.xyz(),
					difference_ba);

				float b = 0.0f;
				MATH::GVECTORF direction_n = _ray.direction.xyz();
				GW::MATH::GVector::DotF(
					difference_ba,
					direction_n,
					b);

				float c = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_ba,
					c);
				c = c - _sphere.radius * _sphere.radius;

				if(c > 0.0f && b > 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				float discriminant = b * b - c;

				if(discriminant < 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outInterval = -b - sqrtf(discriminant);

				// If t is negative, ray started inside sphere so clamp t to zero
				if(_outInterval < 0.0f)
				{
					_outInterval = 0.0f;
				}

				GW::MATH::GVector::ScaleF(
					direction_n,
					_outInterval,
					_outContactPoint);

				GW::MATH::GVector::AddVectorF(
					_outContactPoint,
					_ray.position.xyz(),
					_outContactPoint);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToCapsuleF(const MATH::GRAYF _ray, const MATH::GCAPSULEF _capsule, GCollisionCheck& _outResult,
												  MATH::GVECTORF& _outContactPoint,
												  float& _outInterval)
			{
				GW::MATH::GVECTORF closest_point1 = {};
				ClosestPointToRayF(
					_ray,
					_capsule.data[0],
					closest_point1);

				GW::MATH::GSPHEREF sphere = {};
				ClosestPointToRayF(
					_ray,
					_capsule.data[1],
					sphere.data);

				ClosestPointsToLineFromLineF(
					{{{closest_point1, sphere.data}}},
					{{{_capsule.data[0], _capsule.data[1]}}},
					closest_point1,
					sphere.data);

				sphere.radius = _capsule.radius;

				return IntersectRayToSphereF(
					_ray,
					sphere,
					_outResult,
					_outContactPoint,
					_outInterval);
			}

			static GReturn IntersectRayToAABBF(const MATH::GRAYF _ray, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult,
											   MATH::GVECTORF& _outContactPoint,
											   float& _outInterval)
			{
				float interval1 = 0.0f;
				float interval2 = 0.0f;
				float interval3 = 0.0f;
				float interval4 = 0.0f;
				float interval5 = 0.0f;
				float interval6 = 0.0f;

				if(_ray.direction.x)
				{
					interval1 = (_aabb.min.x - _ray.position.x) / _ray.direction.x;
					interval2 = (_aabb.max.x - _ray.position.x) / _ray.direction.x;
				}
				else
				{
					interval1 = (_aabb.min.x - _ray.position.x) > 0.0f ?
						FLT_MAX : FLT_MIN;
					interval2 = (_aabb.max.x - _ray.position.x) > 0.0f ?
						FLT_MAX : FLT_MIN;
				}

				if(_ray.direction.y)
				{
					interval3 = (_aabb.min.y - _ray.position.y) / _ray.direction.y;
					interval4 = (_aabb.max.y - _ray.position.y) / _ray.direction.y;
				}
				else
				{
					interval3 = (_aabb.min.y - _ray.position.y) > 0.0f ?
						FLT_MAX : FLT_MIN;
					interval4 = (_aabb.max.y - _ray.position.y) > 0.0f ?
						FLT_MAX : FLT_MIN;
				}

				if(_ray.direction.z)
				{
					interval5 = (_aabb.min.z - _ray.position.z) / _ray.direction.z;
					interval6 = (_aabb.max.z - _ray.position.z) / _ray.direction.z;
				}
				else
				{
					interval5 = (_aabb.min.z - _ray.position.z) > 0.0f ?
						FLT_MAX : FLT_MIN;
					interval6 = (_aabb.max.z - _ray.position.z) > 0.0f ?
						FLT_MAX : FLT_MIN;
				}

				float interval_min = G_LARGER(G_LARGER(
					G_SMALLER(interval1, interval2),
					G_SMALLER(interval3, interval4)),
					G_SMALLER(interval5, interval6));

				float interval_max = G_SMALLER(G_SMALLER(
					G_LARGER(interval1, interval2),
					G_LARGER(interval3, interval4)),
					G_LARGER(interval5, interval6));

				if(interval_max < 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(interval_max < interval_min)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outInterval = (interval_min < 0.0f) ?
					interval_max : interval_min;

				GW::MATH::GVector::ScaleF(
					_ray.direction.xyz(),
					_outInterval,
					_outContactPoint);

				GW::MATH::GVector::AddVectorF(
					_outContactPoint,
					_ray.position.xyz(),
					_outContactPoint);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToOBBF(const MATH::GRAYF _ray, const MATH::GOBBF _obb, GCollisionCheck& _outResult,
											  MATH::GVECTORF& _outContactPoint, float& _outInterval)
			{
				MATH::GMATRIXF obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionF(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXF obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0f}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0f}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0f}}},
							{{{0.0f, 0.0f, 0.0f, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GRAYF ray = {};
				ray.position = _ray.position;
				ray.position.w = 1.0f;
				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					ray.position,
					ray.position);
				GW::MATH::GVector::AddVectorF(
					ray.position,
					_obb.center,
					ray.position);

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					_ray.direction,
					ray.direction);
				GW::MATH::GVector::NormalizeF(
					ray.direction,
					ray.direction);

				MATH::GAABBMMF mm = {};
				ConvertAABBCEToAABBMMF(
					{{{_obb.center, _obb.extent}}},
					mm);

				IntersectRayToAABBF(
					ray,
					mm,
					_outResult,
					_outContactPoint,
					_outInterval);

				GMatrixImplementation::InverseF(
					obb_rotation_inverse,
					obb_rotation);

				GW::MATH::GVector::SubtractVectorF(
					_outContactPoint,
					_obb.center,
					_outContactPoint);

				_outContactPoint.w = 1.0F;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation,
					_outContactPoint,
					_outContactPoint);

				_outContactPoint.w = 0.0F;

				return GReturn::SUCCESS;
			}

			static GReturn IntersectSphereToSphereF(const MATH::GSPHEREF _sphere1, const MATH::GSPHEREF _sphere2,
													GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1,
													MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection,
													float& _outDistance)
			{
				TestSphereToSphereF(
					_sphere1,
					_sphere2,
					_outResult);

				if (static_cast<int>(_outResult) < 1)
				{
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::SubtractVectorF(
					_sphere2.data.xyz(),
					_sphere1.data.xyz(),
					_outDirection);

				GW::MATH::GVector::NormalizeF(
					_outDirection,
					_outDirection);

				GW::MATH::GVector::ScaleF(
					_outDirection,
					_sphere1.radius,
					_outContactClosest1);

				GW::MATH::GVector::AddVectorF(
					_sphere1.data.xyz(),
					_outContactClosest1,
					_outContactClosest1);

				GW::MATH::GVector::ScaleF(
					_outDirection,
					-_sphere2.radius,
					_outContactClosest2);

				GW::MATH::GVector::AddVectorF(
					_sphere2.data.xyz(),
					_outContactClosest2,
					_outContactClosest2);

				GW::MATH::GVECTORF difference = {};

				GW::MATH::GVector::SubtractVectorF(
					_outContactClosest1,
					_outContactClosest2,
					difference);

				GW::MATH::GVector::DotF(
					difference,
					difference,
					_outDistance);

				_outDistance = sqrtf(_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn IntersectSphereToCapsuleF(const MATH::GSPHEREF _sphere, const MATH::GCAPSULEF _capsule,
													 GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1,
													 MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection,
													 float& _outDistance)
			{
				MATH::GSPHEREF sphere = {};

				ClosestPointToLineF(
					{{{_capsule.data[0], _capsule.data[1]}}},
					_sphere.data,
					sphere.data);

				sphere.radius = _capsule.radius;

				return IntersectSphereToSphereF(
					_sphere,
					sphere,
					_outResult,
					_outContactClosest1,
					_outContactClosest2,
					_outDirection,
					_outDistance);
			}

			static GReturn IntersectSphereToAABBF(const MATH::GSPHEREF _sphere, const MATH::GAABBCEF _aabb, GCollisionCheck& _outResult,
												  MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2,
												  MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				MATH::GAABBMMF mm = {};
				ConvertAABBCEToAABBMMF(
					_aabb,
					mm);

				float sq_distance = 0.0f;
				SqDistancePointToAABBF(
					_sphere.data.xyz(),
					mm,
					sq_distance);

				float sq_radius = _sphere.radius * _sphere.radius;

				_outResult = (sq_distance <= sq_radius) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				if(_outResult < GCollisionCheck::COLLISION)
				{
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::SubtractVectorF(
					_aabb.center.xyz(),
					_sphere.data.xyz(),
					_outDirection);

				if(GW::MATH::GVector::NormalizeF(_outDirection, _outDirection) == GReturn::FAILURE)
				{
					_outDirection = {};
				}

				GW::MATH::GVector::ScaleF(
					_outDirection,
					_sphere.radius,
					_outContactClosest1);

				GW::MATH::GVector::AddVectorF(
					_sphere.data.xyz(),
					_outContactClosest1,
					_outContactClosest1);

				for(int i = 0; i < 3; i++)
				{
					_outContactClosest2.data[i] = _sphere.data.data[i];

					_outContactClosest2.data[i] = G_LARGER(
						_outContactClosest2.data[i],
						mm.min.data[i]);

					_outContactClosest2.data[i] = G_SMALLER(
						_outContactClosest2.data[i],
						mm.max.data[i]);
				}

				// Point is inside AABB so clamp to closest face
				if(G_ABS(_sphere.x - _outContactClosest2.x) < G_COLLISION_THRESHOLD_F ||
				   G_ABS(_sphere.y - _outContactClosest2.y) < G_COLLISION_THRESHOLD_F ||
				   G_ABS(_sphere.z - _outContactClosest2.z) < G_COLLISION_THRESHOLD_F)
				{
					GW::MATH::GVECTORF translation = {};
					float low = static_cast<float>(0xffffffff);
					float val = 0.0f;

					val = mm.max.x - _sphere.x;
					if(G_ABS(val) < low)
					{
						translation = {{{val, 0.0f, 0.0f}}};
						low = G_ABS(val);
					}

					val = _sphere.x - mm.min.x;
					if(G_ABS(val) < low)
					{
						translation = {{{val, 0.0f, 0.0f}}};
						low = G_ABS(val);
					}

					val = mm.max.y - _sphere.y;
					if(G_ABS(val) < low)
					{
						translation = {{{0.0f, val, 0.0f}}};
						low = G_ABS(val);
					}

					val = _sphere.y - mm.min.y;
					if(G_ABS(val) < low)
					{
						translation = {{{0.0f, val, 0.0f}}};
						low = G_ABS(val);
					}

					val = mm.max.z - _sphere.z;
					if(G_ABS(val) < low)
					{
						translation = {{{0.0f, 0.0f, val}}};
						low = G_ABS(val);
					}

					val = _sphere.z - mm.min.z;
					if(G_ABS(val) < low)
					{
						translation = {{{0.0f, 0.0f, val}}};
						low = G_ABS(val);
					}

					GW::MATH::GVector::AddVectorF(
						_outContactClosest2,
						translation,
						_outContactClosest2);
				}

				GW::MATH::GVector::SubtractVectorF(
					_outContactClosest2,
					_outContactClosest1,
					_outDirection);

				GW::MATH::GVector::DotF(
					_outDirection,
					_outDirection,
					_outDistance);

				_outDistance = sqrtf(_outDistance);

				if(GW::MATH::GVector::NormalizeF(_outDirection, _outDirection) == GReturn::FAILURE)
				{
					_outDirection = {};
				}

				return GReturn::SUCCESS;
			}

			static GReturn IntersectSphereToOBBF(const MATH::GSPHEREF _sphere, const MATH::GOBBF _obb, GCollisionCheck& _outResult,
												 MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2,
												 MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				MATH::GMATRIXF obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionF(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXF obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0f}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0f}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0f}}},
							{{{0.0f, 0.0f, 0.0f, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotF(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GSPHEREF sphere = {};
				sphere.data = _sphere.data;
				sphere.radius = 1.0f;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					sphere.data,
					sphere.data);
				GW::MATH::GVector::AddVectorF(
					sphere.data,
					_obb.center,
					sphere.data);
				sphere.radius = _sphere.radius;

				IntersectSphereToAABBF(
					sphere,
					{{{_obb.center, _obb.extent}}},
					_outResult,
					_outContactClosest1,
					_outContactClosest2,
					_outDirection,
					_outDistance);

				GMatrixImplementation::InverseF(
					obb_rotation_inverse,
					obb_rotation);

				GW::MATH::GVector::SubtractVectorF(
					_outContactClosest1,
					_obb.center,
					_outContactClosest1);

				_outContactClosest1.w = 1.0F;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation,
					_outContactClosest1,
					_outContactClosest1);

				_outContactClosest1.w = 0.0F;

				GW::MATH::GVector::SubtractVectorF(
					_outContactClosest2,
					_obb.center,
					_outContactClosest2);

				_outContactClosest2.w = 1.0F;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation,
					_outContactClosest2,
					_outContactClosest2);

				_outContactClosest2.w = 0.0F;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation,
					_outDirection.xyz(),
					_outDirection);

				return GReturn::SUCCESS;
			}

			static GReturn IntersectCapsuleToCapsuleF(const MATH::GCAPSULEF _capsule1, const MATH::GCAPSULEF _capsule2,
													  GCollisionCheck& _outResult, MATH::GVECTORF& _outContactClosest1,
													  MATH::GVECTORF& _outContactClosest2, MATH::GVECTORF& _outDirection,
													  float& _outDistance)
			{
				MATH::GSPHEREF sphere1 = {};
				MATH::GSPHEREF sphere2 = {};

				ClosestPointsToLineFromLineF(
					{{{_capsule1.data[0], _capsule1.data[1]}}},
					{{{_capsule2.data[0], _capsule2.data[1]}}},
					sphere1.data,
					sphere2.data);

				sphere1.radius = _capsule1.radius;
				sphere2.radius = _capsule2.radius;

				return IntersectSphereToSphereF(
					sphere1,
					sphere2,
					_outResult,
					_outContactClosest1,
					_outContactClosest2,
					_outDirection,
					_outDistance);
			}

			static GReturn IntersectCapsuleToAABBF(const MATH::GCAPSULEF _capsule, const MATH::GAABBMMF _aabb, GCollisionCheck& _outResult,
												   MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2,
												   MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				// Extend AABB by capsule radius
				MATH::GAABBMMF aabbMM =
				{
					{
						{
							{{{_aabb.min.x - _capsule.radius, _aabb.min.y - _capsule.radius, _aabb.min.z - _capsule.radius , 0.0f}}},
							{{{_aabb.max.x + _capsule.radius, _aabb.max.y + _capsule.radius, _aabb.max.z + _capsule.radius}}}
						}
					}
				};

				MATH::GRAYF ray = {};
				ray.position = _capsule.data[0].xyz();

				GW::MATH::GVector::SubtractVectorF(
					ray.position,
					_capsule.data[0].xyz(),
					ray.direction);

				GW::MATH::GVector::NormalizeF(
					ray.direction,
					ray.direction);

				IntersectRayToAABBF(
					ray,
					aabbMM,
					_outResult,
					_outContactClosest1,
					_outDistance);

				if (static_cast<int>(_outResult) <= 0 || _outDistance > 1.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Flags to determine the region
				int u = 0;
				int v = 0;

				if(_outContactClosest1.x < _aabb.min.x)
				{
					u |= 1;
				}
				if(_outContactClosest1.x > _aabb.max.x)
				{
					v |= 1;
				}

				if(_outContactClosest1.y < _aabb.min.y)
				{
					u |= 2;
				}
				if(_outContactClosest1.y > _aabb.max.y)
				{
					v |= 2;
				}

				if(_outContactClosest1.z < _aabb.min.z)
				{
					u |= 4;
				}
				if(_outContactClosest1.z > _aabb.max.z)
				{
					v |= 4;
				}

				int m = u + v;

				auto Corner = [](const MATH::GAABBMMF box, const int n)->GW::MATH::GVECTORF
				{
					if(n & 1)
					{
						return GW::MATH::GVECTORF
						{
							{{box.max.x, box.max.y, box.max.z}}
						};
					}
					else
					{
						return GW::MATH::GVECTORF
						{
							{{box.min.x, box.min.y, box.min.z}}
						};
					}
				};

				GW::MATH::GCAPSULEF capsule;
				capsule.data[0] = Corner(aabbMM, v);

				// Vertex Region
				if(m == 7)
				{
					float tmin = static_cast<float>(0xffffffff);

					capsule.data[1] = Corner(aabbMM, v ^ 1);
					IntersectLineToCapsuleF(
						{{{_capsule.data[0].xyz(), _capsule.data[1].xyz()}}},
						capsule,
						_outResult,
						_outContactClosest1,
						_outDirection,
						tmin);

					if (static_cast<int>(_outResult) > 0)
					{
						_outDistance = G_SMALLER(tmin, _outDistance);
					}

					capsule.data[1] = Corner(aabbMM, v ^ 2);
					IntersectLineToCapsuleF(
						{{{_capsule.data[0].xyz(), _capsule.data[1].xyz()}}},
						capsule,
						_outResult,
						_outContactClosest1,
						_outDirection,
						tmin);

					if (static_cast<int>(_outResult) > 0)
					{
						_outDistance = G_SMALLER(tmin, _outDistance);
					}

					capsule.data[1] = Corner(aabbMM, v ^ 4);
					IntersectLineToCapsuleF(
						{{{_capsule.data[0].xyz(), _capsule.data[1].xyz()}}},
						capsule,
						_outResult,
						_outContactClosest1,
						_outDirection,
						tmin);

					if (static_cast<int>(_outResult) > 0)
					{
						_outDistance = G_SMALLER(tmin, _outDistance);
					}

					if(tmin == static_cast<float>(0xffffffff))
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}

					_outDistance = tmin;

					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				if((m & (m - 1)) == 0)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				capsule.data[1] = capsule.data[0];
				capsule.data[0] = Corner(aabbMM, u ^ 7);

				return IntersectLineToCapsuleF(
					{{{_capsule.data[0].xyz(), _capsule.data[1].xyz()}}},
					capsule,
					_outResult,
					_outContactClosest1,
					_outDirection,
					_outDistance);
			}

			static GReturn IntersectCapsuleToOBBF(const MATH::GCAPSULEF _capsule, const MATH::GOBBF _obb, GCollisionCheck& _outResult,
												  MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2,
												  MATH::GVECTORF& _outDirection, float& _outDistance)
			{
				MATH::GMATRIXF obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionF(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXF obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0f}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0f}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0f}}},
							{{{0.0f, 0.0f, 0.0f, 1.0f}}}
						}
					}
				};

				MATH::GCAPSULEF capsule = {};
				capsule.data[0] = _capsule.data[0];
				capsule.data[0].w = 1.0f;
				capsule.data[1] = _capsule.data[1];
				capsule.data[1].w = 1.0f;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					capsule.data[0],
					capsule.data[0]);

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation_inverse,
					capsule.data[1],
					capsule.data[1]);

				capsule.radius = _capsule.radius;

				IntersectCapsuleToAABBF(
					capsule,
					{{{_obb.center, _obb.extent}}},
					_outResult,
					_outContactClosest1,
					_outContactClosest2,
					_outDirection,
					_outDistance);

				GMatrixImplementation::InverseF(
					obb_rotation_inverse,
					obb_rotation);

				GW::MATH::GVector::SubtractVectorF(
					_outContactClosest1,
					_obb.center,
					_outContactClosest1);

				_outContactClosest1.w = 1.0F;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation,
					_outContactClosest1,
					_outContactClosest1);

				_outContactClosest1.w = 0.0F;

				GW::MATH::GVector::SubtractVectorF(
					_outContactClosest2,
					_obb.center,
					_outContactClosest2);

				_outContactClosest2.w = 1.0F;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation,
					_outContactClosest2,
					_outContactClosest2);

				_outContactClosest2.w = 0.0F;

				GMatrixImplementation::VectorXMatrixF(
					obb_rotation,
					_outDirection.xyz(),
					_outDirection);

				return GReturn::SUCCESS;
			}

			static GReturn IntersectAABBToAABBF(const MATH::GAABBCEF _aabb1, const MATH::GAABBCEF _aabb2, GCollisionCheck& _outResult,
												MATH::GAABBCEF& _outContactAABB, MATH::GVECTORF& _outDirection,
												float& _outDistance)
			{
				if(G_ABS(_aabb1.center.x - _aabb2.center.x) > (_aabb1.extent.x + _aabb2.extent.x) ||
				   G_ABS(_aabb1.center.y - _aabb2.center.y) > (_aabb1.extent.y + _aabb2.extent.y) ||
				   G_ABS(_aabb1.center.z - _aabb2.center.z) > (_aabb1.extent.z + _aabb2.extent.z))
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				MATH::GAABBMMF aabb = {};
				aabb.min.x = G_LARGER(_aabb1.center.x - _aabb1.extent.x, _aabb2.center.x - _aabb2.extent.x);
				aabb.max.x = G_SMALLER(_aabb1.center.x + _aabb1.extent.x, _aabb2.center.x + _aabb2.extent.x);
				aabb.min.y = G_LARGER(_aabb1.center.y - _aabb1.extent.y, _aabb2.center.y - _aabb2.extent.y);
				aabb.max.y = G_SMALLER(_aabb1.center.y + _aabb1.extent.y, _aabb2.center.y + _aabb2.extent.y);
				aabb.min.z = G_LARGER(_aabb1.center.z - _aabb1.extent.z, _aabb2.center.z - _aabb2.extent.z);
				aabb.max.z = G_SMALLER(_aabb1.center.z + _aabb1.extent.z, _aabb2.center.z + _aabb2.extent.z);

				GW::MATH::GVECTORF difference = {};

				GW::MATH::GVector::SubtractVectorF(
					aabb.max,
					aabb.min,
					difference);

				GW::MATH::GVector::DotF(
					difference,
					difference,
					_outDistance);

				_outDistance = sqrtf(_outDistance);

				GW::MATH::GVector::SubtractVectorF(
					_aabb2.center,
					_aabb1.center,
					_outDirection);

				GW::MATH::GVector::NormalizeF(
					_outDirection,
					_outDirection);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			//static GReturn IntersectAABBToOBBF(const MATH::GAABBCEF _aabb, const MATH::GOBBF _obb, GCollisionCheck& _outResult,
			//								   MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2,
			//								   MATH::GVECTORF& _outDirection, float& _outDistance)
			//{
			//	return IntersectOBBToOBBF(
			//		{_aabb.center, _aabb.extent},
			//		_obb,
			//		_outResult,
			//		_outContactClosest1,
			//		_outContactClosest2,
			//		_outDirection,
			//		_outDistance);
			//}

			//static GReturn IntersectOBBToOBBF(const MATH::GOBBF _obb1, const MATH::GOBBF _obb2, GCollisionCheck& _outResult,
			//								  MATH::GVECTORF& _outContactClosest1, MATH::GVECTORF& _outContactClosest2,
			//								  MATH::GVECTORF& _outDirection, float& _outDistance)
			//{
			//	// Convert quaternions into 3 axes for both OBB1 and OBB2.
			//	float xx2 = 2.0f * _obb1.rotation.x * _obb1.rotation.x;
			//	float yy2 = 2.0f * _obb1.rotation.y * _obb1.rotation.y;
			//	float zz2 = 2.0f * _obb1.rotation.z * _obb1.rotation.z;

			//	float xy2 = 2.0f * _obb1.rotation.x * _obb1.rotation.y;
			//	float xz2 = 2.0f * _obb1.rotation.x * _obb1.rotation.z;
			//	float yz2 = 2.0f * _obb1.rotation.y * _obb1.rotation.z;

			//	float wx2 = 2.0f * _obb1.rotation.w * _obb1.rotation.x;
			//	float wy2 = 2.0f * _obb1.rotation.w * _obb1.rotation.y;
			//	float wz2 = 2.0f * _obb1.rotation.w * _obb1.rotation.z;

			//	MATH::GVECTORF obb1_rotation[3] =
			//	{
			//		{ 1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2 },
			//		{ xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2 },
			//		{ xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2 }
			//	};

			//	xx2 = 2.0f * _obb2.rotation.x * _obb2.rotation.x;
			//	yy2 = 2.0f * _obb2.rotation.y * _obb2.rotation.y;
			//	zz2 = 2.0f * _obb2.rotation.z * _obb2.rotation.z;

			//	xy2 = 2.0f * _obb2.rotation.x * _obb2.rotation.y;
			//	xz2 = 2.0f * _obb2.rotation.x * _obb2.rotation.z;
			//	yz2 = 2.0f * _obb2.rotation.y * _obb2.rotation.z;

			//	wx2 = 2.0f * _obb2.rotation.w * _obb2.rotation.x;
			//	wy2 = 2.0f * _obb2.rotation.w * _obb2.rotation.y;
			//	wz2 = 2.0f * _obb2.rotation.w * _obb2.rotation.z;

			//	MATH::GVECTORF obb2_rotation[3] =
			//	{
			//		{ 1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2 },
			//		{ xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2 },
			//		{ xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2 }
			//	};

			//	float projected_radii = 0.0f;
			//	float projected_radii_1 = 0.0f;
			//	float projected_radii_2 = 0.0f;
			//	float test_axis = 0.0f;

			//	float rotation[3][3] = {};
			//	float abs_rotation[3][3] = {};

			//	for(int i = 0; i < 3; i++)
			//	{
			//		for(int j = 0; j < 3; j++)
			//		{
			//			GW::MATH::GVector::DotF(
			//				obb1_rotation[i],
			//				obb2_rotation[j],
			//				rotation[i][j]);
			//		}
			//	}

			//	// Get translation and then bring it into a's coordinate frame.
			//	MATH::GVECTORF difference_ba = {};
			//	GW::MATH::GVector::SubtractVectorF(
			//		_obb2.center,
			//		_obb1.center,
			//		difference_ba);

			//	MATH::GVECTORF translation_vec = {};
			//	GW::MATH::GVector::DotF(
			//		difference_ba,
			//		obb1_rotation[0],
			//		translation_vec.x);

			//	GW::MATH::GVector::DotF(
			//		difference_ba,
			//		obb1_rotation[1],
			//		translation_vec.y);

			//	GW::MATH::GVector::DotF(
			//		difference_ba,
			//		obb1_rotation[2],
			//		translation_vec.z);

			//	// Get common values while maintaining numerical precision.
			//	for(int i = 0; i < 3; i++)
			//	{
			//		for(int j = 0; j < 3; j++)
			//		{
			//			abs_rotation[i][j] =
			//				G_ABS(rotation[i][j]) + G_COLLISION_THRESHOLD_F;
			//		}
			//	}

			//	_outDistance = static_cast<float>(0x0);

			//	auto GetInfo = [&](
			//		float axis,
			//		float radii_1,
			//		float radii_2,
			//		GW::MATH::GVECTORF vec1,
			//		GW::MATH::GVECTORF* vec2,
			//		GW::MATH::GVECTORF& dir,
			//		float& distance)
			//	{
			//		float x = G_ABS(radii_1 - (axis - radii_2));

			//		if(x < distance)
			//		{
			//			distance = x;

			//			if(vec2)
			//			{
			//				GW::MATH::GVector::CrossVector3F(
			//					vec1,
			//					vec2[0],
			//					dir);

			//				GW::MATH::GVector::NormalizeF(
			//					dir,
			//					dir);
			//			}
			//			else
			//			{
			//				GW::MATH::GVector::NormalizeF(
			//					vec1,
			//					dir);
			//			}
			//		}
			//	};

			//	// Test axes for OBB1 x, y, z.
			//	for(int i = 0; i < 3; i++)
			//	{
			//		projected_radii_1 = _obb1.extent.data[i];

			//		projected_radii_2 =
			//			_obb2.extent.data[0] * abs_rotation[i][0] +
			//			_obb2.extent.data[1] * abs_rotation[i][1] +
			//			_obb2.extent.data[2] * abs_rotation[i][2];

			//		projected_radii = projected_radii_1 + projected_radii_2;

			//		test_axis = G_ABS(translation_vec.data[i]);

			//		if(test_axis > projected_radii)
			//		{
			//			_outResult = GCollisionCheck::NO_COLLISION;
			//			return GReturn::SUCCESS;
			//		}
			//		else
			//		{
			//			GetInfo(
			//				test_axis,
			//				projected_radii_1,
			//				projected_radii_2,
			//				obb1_rotation[i],
			//				nullptr,
			//				_outDirection,
			//				_outDistance);
			//		}
			//	}

			//	// Test axes for OBB2 x, y, z.
			//	for(int i = 0; i < 3; i++)
			//	{
			//		projected_radii_1 =
			//			_obb1.extent.data[0] * abs_rotation[0][i] +
			//			_obb1.extent.data[1] * abs_rotation[1][i] +
			//			_obb1.extent.data[2] * abs_rotation[2][i];

			//		projected_radii_2 = _obb2.extent.data[i];

			//		projected_radii = projected_radii_1 + projected_radii_2;

			//		test_axis = G_ABS(
			//			translation_vec.data[0] * rotation[0][i] +
			//			translation_vec.data[1] * rotation[1][i] +
			//			translation_vec.data[2] * rotation[2][i]);

			//		if(test_axis > projected_radii)
			//		{
			//			_outResult = GCollisionCheck::NO_COLLISION;
			//			return GReturn::SUCCESS;
			//		}
			//		else
			//		{
			//			GetInfo(
			//				test_axis,
			//				projected_radii_1,
			//				projected_radii_2,
			//				obb2_rotation[i],
			//				nullptr,
			//				_outDirection,
			//				_outDistance);
			//		}
			//	}

			//	// Test OBB1 x & OBB2 x
			//	projected_radii_1 =
			//		_obb1.extent.data[1] * abs_rotation[2][0] +
			//		_obb1.extent.data[2] * abs_rotation[1][0];

			//	projected_radii_2 =
			//		_obb2.extent.data[1] * abs_rotation[0][2] +
			//		_obb2.extent.data[2] * abs_rotation[0][1];

			//	projected_radii = projected_radii_1 + projected_radii_2;

			//	test_axis = G_ABS(
			//		translation_vec.data[2] * rotation[1][0] -
			//		translation_vec.data[1] * rotation[2][0]);

			//	if(test_axis > projected_radii)
			//	{
			//		_outResult = GCollisionCheck::NO_COLLISION;
			//		return GReturn::SUCCESS;
			//	}
			//	else
			//	{
			//		GetInfo(
			//			test_axis,
			//			projected_radii_1,
			//			projected_radii_2,
			//			obb1_rotation[0],
			//			&obb2_rotation[0],
			//			_outDirection,
			//			_outDistance);
			//	}

			//	// Test OBB1 x & OBB2 y
			//	projected_radii_1 =
			//		_obb1.extent.data[1] * abs_rotation[2][1] +
			//		_obb1.extent.data[2] * abs_rotation[1][1];

			//	projected_radii_2 =
			//		_obb2.extent.data[0] * abs_rotation[0][2] +
			//		_obb2.extent.data[2] * abs_rotation[0][0];

			//	projected_radii = projected_radii_1 + projected_radii_2;

			//	test_axis = G_ABS(
			//		translation_vec.data[2] * rotation[1][1] -
			//		translation_vec.data[1] * rotation[2][1]);

			//	if(test_axis > projected_radii)
			//	{
			//		_outResult = GCollisionCheck::NO_COLLISION;
			//		return GReturn::SUCCESS;
			//	}
			//	else
			//	{
			//		GetInfo(
			//			test_axis,
			//			projected_radii_1,
			//			projected_radii_2,
			//			obb1_rotation[0],
			//			&obb2_rotation[1],
			//			_outDirection,
			//			_outDistance);
			//	}

			//	// Test OBB1 x & OBB2 z
			//	projected_radii_1 =
			//		_obb1.extent.data[1] * abs_rotation[2][2] +
			//		_obb1.extent.data[2] * abs_rotation[1][2];

			//	projected_radii_2 =
			//		_obb2.extent.data[0] * abs_rotation[0][1] +
			//		_obb2.extent.data[1] * abs_rotation[0][0];

			//	projected_radii = projected_radii_1 + projected_radii_2;

			//	test_axis = G_ABS(
			//		translation_vec.data[2] * rotation[1][2] -
			//		translation_vec.data[1] * rotation[2][2]);

			//	if(test_axis > projected_radii)
			//	{
			//		_outResult = GCollisionCheck::NO_COLLISION;
			//		return GReturn::SUCCESS;
			//	}
			//	else
			//	{
			//		GetInfo(
			//			test_axis,
			//			projected_radii_1,
			//			projected_radii_2,
			//			obb1_rotation[0],
			//			&obb2_rotation[2],
			//			_outDirection,
			//			_outDistance);
			//	}

			//	// Test OBB1 y & OBB2 x
			//	projected_radii_1 =
			//		_obb1.extent.data[0] * abs_rotation[2][0] +
			//		_obb1.extent.data[2] * abs_rotation[0][0];

			//	projected_radii_2 =
			//		_obb2.extent.data[1] * abs_rotation[1][2] +
			//		_obb2.extent.data[2] * abs_rotation[1][1];

			//	projected_radii = projected_radii_1 + projected_radii_2;

			//	test_axis = G_ABS(
			//		translation_vec.data[0] * rotation[2][0] -
			//		translation_vec.data[2] * rotation[0][0]);

			//	if(test_axis > projected_radii)
			//	{
			//		_outResult = GCollisionCheck::NO_COLLISION;
			//		return GReturn::SUCCESS;
			//	}
			//	else
			//	{
			//		GetInfo(
			//			test_axis,
			//			projected_radii_1,
			//			projected_radii_2,
			//			obb1_rotation[1],
			//			&obb2_rotation[0],
			//			_outDirection,
			//			_outDistance);
			//	}

			//	// Test OBB1 y & OBB2 y
			//	projected_radii_1 =
			//		_obb1.extent.data[0] * abs_rotation[2][1] +
			//		_obb1.extent.data[2] * abs_rotation[0][1];

			//	projected_radii_2 =
			//		_obb2.extent.data[0] * abs_rotation[1][2] +
			//		_obb2.extent.data[2] * abs_rotation[1][0];

			//	projected_radii = projected_radii_1 + projected_radii_2;

			//	test_axis = G_ABS(
			//		translation_vec.data[0] * rotation[2][1] -
			//		translation_vec.data[2] * rotation[0][1]);

			//	if(test_axis > projected_radii)
			//	{
			//		_outResult = GCollisionCheck::NO_COLLISION;
			//		return GReturn::SUCCESS;
			//	}
			//	else
			//	{
			//		GetInfo(
			//			test_axis,
			//			projected_radii_1,
			//			projected_radii_2,
			//			obb1_rotation[1],
			//			&obb2_rotation[1],
			//			_outDirection,
			//			_outDistance);
			//	}

			//	// Test OBB1 y & OBB2 z
			//	projected_radii_1 =
			//		_obb1.extent.data[0] * abs_rotation[2][2] +
			//		_obb1.extent.data[2] * abs_rotation[0][2];

			//	projected_radii_2 =
			//		_obb2.extent.data[0] * abs_rotation[1][1] +
			//		_obb2.extent.data[1] * abs_rotation[1][0];

			//	projected_radii = projected_radii_1 + projected_radii_2;

			//	test_axis = G_ABS(
			//		translation_vec.data[0] * rotation[2][2] -
			//		translation_vec.data[2] * rotation[0][2]);

			//	if(test_axis > projected_radii)
			//	{
			//		_outResult = GCollisionCheck::NO_COLLISION;
			//		return GReturn::SUCCESS;
			//	}
			//	else
			//	{
			//		GetInfo(
			//			test_axis,
			//			projected_radii_1,
			//			projected_radii_2,
			//			obb1_rotation[1],
			//			&obb2_rotation[2],
			//			_outDirection,
			//			_outDistance);
			//	}

			//	// Test OBB1 z & OBB2 x
			//	projected_radii_1 =
			//		_obb1.extent.data[0] * abs_rotation[1][0] +
			//		_obb1.extent.data[1] * abs_rotation[0][0];

			//	projected_radii_2 =
			//		_obb2.extent.data[1] * abs_rotation[2][2] +
			//		_obb2.extent.data[2] * abs_rotation[2][1];

			//	projected_radii = projected_radii_1 + projected_radii_2;

			//	test_axis = G_ABS(
			//		translation_vec.data[1] * rotation[0][0] -
			//		translation_vec.data[0] * rotation[1][0]);

			//	if(test_axis > projected_radii)
			//	{
			//		_outResult = GCollisionCheck::NO_COLLISION;
			//		return GReturn::SUCCESS;
			//	}
			//	else
			//	{
			//		GetInfo(
			//			test_axis,
			//			projected_radii_1,
			//			projected_radii_2,
			//			obb1_rotation[2],
			//			&obb2_rotation[0],
			//			_outDirection,
			//			_outDistance);
			//	}

			//	// Test OBB1 z & OBB2 y
			//	projected_radii_1 =
			//		_obb1.extent.data[0] * abs_rotation[1][1] +
			//		_obb1.extent.data[1] * abs_rotation[0][1];

			//	projected_radii_2 =
			//		_obb2.extent.data[0] * abs_rotation[2][2] +
			//		_obb2.extent.data[2] * abs_rotation[2][0];

			//	projected_radii = projected_radii_1 + projected_radii_2;

			//	test_axis = G_ABS(
			//		translation_vec.data[1] * rotation[0][1] -
			//		translation_vec.data[0] * rotation[1][1]);

			//	if(test_axis > projected_radii)
			//	{
			//		_outResult = GCollisionCheck::NO_COLLISION;
			//		return GReturn::SUCCESS;
			//	}
			//	else
			//	{
			//		GetInfo(
			//			test_axis,
			//			projected_radii_1,
			//			projected_radii_2,
			//			obb1_rotation[2],
			//			&obb2_rotation[1],
			//			_outDirection,
			//			_outDistance);
			//	}

			//	// Test OBB1 z & OBB2 z
			//	projected_radii_1 =
			//		_obb1.extent.data[0] * abs_rotation[1][2] +
			//		_obb1.extent.data[1] * abs_rotation[0][2];

			//	projected_radii_2 =
			//		_obb2.extent.data[0] * abs_rotation[2][1] +
			//		_obb2.extent.data[1] * abs_rotation[2][0];

			//	projected_radii = projected_radii_1 + projected_radii_2;

			//	test_axis = G_ABS(
			//		translation_vec.data[1] * rotation[0][2] -
			//		translation_vec.data[0] * rotation[1][2]);

			//	if(test_axis > projected_radii)
			//	{
			//		_outResult = GCollisionCheck::NO_COLLISION;
			//		return GReturn::SUCCESS;
			//	}
			//	else
			//	{
			//		GetInfo(
			//			test_axis,
			//			projected_radii_1,
			//			projected_radii_2,
			//			obb1_rotation[2],
			//			&obb2_rotation[2],
			//			_outDirection,
			//			_outDistance);
			//	}
			//	_outDistance += G_COLLISION_THRESHOLD_F;

			//	GW::MATH::GVector::ScaleF(
			//		_outDirection,
			//		_outDistance,
			//		_outContactClosest1);

			//	GW::MATH::GVector::AddVectorF(
			//		_outContactClosest1,
			//		_obb2.center,
			//		_outContactClosest1);

			//	GW::MATH::GVector::ScaleF(
			//		_outDirection,
			//		-(_outDistance),
			//		_outContactClosest2);

			//	GW::MATH::GVector::AddVectorF(
			//		_outContactClosest2,
			//		_obb2.center,
			//		_outContactClosest2);

			//	_outResult = GCollisionCheck::COLLISION;
			//	return GReturn::SUCCESS;
			//}

			static GReturn SqDistancePointToLineF(const MATH::GVECTORF _point, const MATH::GLINEF _line, float& _outDistance)
			{
				GW::MATH::GVECTORF p = _point.xyz();

				MATH::GLINEF line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					line.end,
					line.start,
					difference_ba);

				MATH::GVECTORF difference_pa = {};
				GW::MATH::GVector::SubtractVectorF(
					p,
					line.start,
					difference_pa);

				MATH::GVECTORF difference_pb = {};
				GW::MATH::GVector::SubtractVectorF(
					p,
					line.end,
					difference_pb);

				float interval1 = 0.0f;
				GW::MATH::GVector::DotF(
					difference_pa,
					difference_ba,
					interval1);

				if(interval1 <= 0.0f)
				{
					GW::MATH::GVector::DotF(
						difference_pa,
						difference_pa,
						_outDistance);

					return GReturn::SUCCESS;
				}

				float interval2 = 0.0f;
				GW::MATH::GVector::DotF(
					difference_ba,
					difference_ba,
					interval2);

				if(interval1 >= interval2)
				{
					GW::MATH::GVector::DotF(
						difference_pb,
						difference_pb,
						_outDistance);

					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::DotF(
					difference_pa,
					difference_pa,
					_outDistance);

				if(G_ABS(interval2) >= G_COLLISION_THRESHOLD_F)
				{
					_outDistance -= interval1 * interval1 / interval2;
				}

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToRayF(const MATH::GVECTORF _point, const MATH::GRAYF _ray, float& _outDistance)
			{
				GW::MATH::GVECTORF p = {};

				ClosestPointToRayF(
					_ray,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorF(
					p,
					_point.xyz(),
					p);

				GW::MATH::GVector::DotF(
					p,
					p,
					_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToTriangleF(const MATH::GVECTORF _point, const MATH::GTRIANGLEF _triangle,
													  float& _outDistance)
			{
				GW::MATH::GVECTORF p = {};
				ClosestPointToTriangleF(
					_triangle,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorF(
					p,
					_point.xyz(),
					p);

				GW::MATH::GVector::DotF(
					p,
					p,
					_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToPlaneF(const MATH::GVECTORF _point, const MATH::GPLANEF _plane,
												   float& _outDistance)
			{
				GW::MATH::GVector::DotF(
					_plane.data.xyz(),
					_point.xyz(),
					_outDistance);
				_outDistance -= _plane.distance;
				_outDistance *= _outDistance;

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToSphereF(const MATH::GVECTORF _point, const MATH::GSPHEREF _sphere,
													float& _outDistance)
			{
				GW::MATH::GVECTORF p = {};
				ClosestPointToSphereF(
					_sphere,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorF(
					p,
					_point.xyz(),
					p);

				GW::MATH::GVector::DotF(
					p,
					p,
					_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToCapsuleF(const MATH::GVECTORF _point, const MATH::GCAPSULEF _capsule,
													 float& _outDistance)
			{
				GW::MATH::GVECTORF p = {};
				ClosestPointToCapsuleF(
					_capsule,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorF(
					p,
					_point.xyz(),
					p);

				GW::MATH::GVector::DotF(
					p,
					p,
					_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToAABBF(const MATH::GVECTORF _point, const MATH::GAABBMMF _aabb,
												  float& _outDistance)
			{

				_outDistance = 0.0f;

				for(int i = 0; i < 3; i++)
				{
					float v = _point.data[i];

					if(v < _aabb.min.data[i])
					{
						_outDistance += (_aabb.min.data[i] - v) * (_aabb.min.data[i] - v);
					}

					if(v > _aabb.max.data[i])
					{
						_outDistance += (v - _aabb.max.data[i]) * (v - _aabb.max.data[i]);
					}
				}

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToOBBF(const MATH::GVECTORF _point, const MATH::GOBBF _obb, float& _outDistance)
			{
				MATH::GVECTORF p = {};
				ClosestPointToOBBF(
					_obb,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorF(
					p,
					_point,
					p);

				GW::MATH::GVector::DotF(
					p,
					p,
					_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn BarycentricF(const MATH::GVECTORF _a, const MATH::GVECTORF _b, const MATH::GVECTORF _c,
										const MATH::GVECTORF _p, MATH::GVECTORF& _outBarycentric)
			{
				MATH::GVECTORF difference_ba = {};
				GW::MATH::GVector::SubtractVectorF(
					_b,
					_a,
					difference_ba);

				MATH::GVECTORF difference_ca = {};
				GW::MATH::GVector::SubtractVectorF(
					_c,
					_a,
					difference_ca);

				MATH::GVECTORF triangle_n = {};
				GW::MATH::GVector::CrossVector3F(
					difference_ba,
					difference_ca,
					triangle_n);

				float x = G_ABS(triangle_n.x);
				float y = G_ABS(triangle_n.y);
				float z = G_ABS(triangle_n.z);

				// Degenerate triangle
				if(x + y + z < G_COLLISION_THRESHOLD_F)
				{
					return GReturn::FAILURE;
				}

				float nominator_u = 0.0f;
				float nominator_v = 0.0f;
				float w = 0.0f;

				// Project to yz plane
				if(x >= y && x >= z)
				{
					// Area of pbc in yz plane
					GW::MATH::GVector::CrossVector2F(
						GW::MATH::GVECTORF{{{_p.y - _b.y,_b.y - _c.y}}},
						GW::MATH::GVECTORF{{{_p.z - _b.z,_b.z - _c.z}}},
						nominator_u);

					// Area of pca in yz plane
					GW::MATH::GVector::CrossVector2F(
						GW::MATH::GVECTORF{{{_p.y - _c.y,_c.y - _a.y}}},
						GW::MATH::GVECTORF{{{_p.z - _c.z,_c.z - _a.z}}},
						nominator_v);

					w = 1.0f / triangle_n.x;
				}
				// Project to the xz plane
				else if(y >= x && y >= z)
				{
					// Area of pbc in xz plane
					GW::MATH::GVector::CrossVector2F(
						GW::MATH::GVECTORF{{{_p.x - _b.x,_b.x - _c.x}}},
						GW::MATH::GVECTORF{{{_p.z - _b.z,_b.z - _c.z}}},
						nominator_u);

					// Area of pca in xz plane
					GW::MATH::GVector::CrossVector2F(
						GW::MATH::GVECTORF{{{_p.x - _c.x,_c.x - _a.x}}},
						GW::MATH::GVECTORF{{{_p.z - _c.z,_c.z - _a.z}}},
						nominator_v);

					w = 1.0f / (-triangle_n.y);
				}
				// Project to the xy plane
				else
				{
					// Area of pbc in xy plane
					GW::MATH::GVector::CrossVector2F(
						GW::MATH::GVECTORF{{{_p.x - _b.x,_b.x - _c.x}}},
						GW::MATH::GVECTORF{{{_p.y - _b.y,_b.y - _c.y}}},
						nominator_u);

					// Area of pca in xy plane
					GW::MATH::GVector::CrossVector2F(
						GW::MATH::GVECTORF{{{_p.x - _c.x,_c.x - _a.x}}},
						GW::MATH::GVECTORF{{{_p.y - _c.y,_c.y - _a.y}}},
						nominator_v);

					w = 1.0f / triangle_n.z;
				}

				float u = nominator_u * w;
				float v = nominator_v * w;

				_outBarycentric =
				{
					{
						{
							u,
							v,
							1.0f - u - v
						}
					}
				};

				return GReturn::SUCCESS;
			}

			// DOUBLE

			static GReturn ConvertAABBCEToAABBMMD(const MATH::GAABBCED _aabbCE, MATH::GAABBMMD& _outAABBMM)
			{
				_outAABBMM.min =
				{
					{
						{
							_aabbCE.center.x - _aabbCE.extent.x,
							_aabbCE.center.y - _aabbCE.extent.y,
							_aabbCE.center.z - _aabbCE.extent.z
						}
					}
				};

				_outAABBMM.max =
				{
					{
						{
							_aabbCE.center.x + _aabbCE.extent.x,
							_aabbCE.center.y + _aabbCE.extent.y,
							_aabbCE.center.z + _aabbCE.extent.z
						}
					}
				};

				return GReturn::SUCCESS;
			}

			static GReturn ConvertAABBMMToAABBCED(const MATH::GAABBMMD _aabbMM, MATH::GAABBCED& _outAABCE)
			{
				_outAABCE.center =
				{
					{
						{
							(_aabbMM.min.x + _aabbMM.max.x) * 0.5,
							(_aabbMM.min.y + _aabbMM.max.y) * 0.5,
							(_aabbMM.min.z + _aabbMM.max.z) * 0.5
						}
					}
				};

				_outAABCE.extent =
				{
					{
						{
							_aabbMM.max.x - _outAABCE.center.x,
							_aabbMM.max.y - _outAABCE.center.y,
							_aabbMM.max.z - _outAABCE.center.z
						}
					}
				};

				return GReturn::SUCCESS;
			}

			static GReturn ComputePlaneD(const MATH::GVECTORD _planePositionA, const MATH::GVECTORD _planePositionB,
										 const MATH::GVECTORD _planePositionC, MATH::GPLANED& _outPlane)
			{
				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_planePositionB,
					_planePositionA,
					difference_ba);

				GW::MATH::GVECTORD difference_ca = {};
				GW::MATH::GVector::SubtractVectorD(
					_planePositionC,
					_planePositionA,
					difference_ca);

				GW::MATH::GVector::CrossVector3D(
					difference_ca,
					difference_ba,
					_outPlane.data);

				if(GW::MATH::GVector::NormalizeD(_outPlane.data, _outPlane.data) == GReturn::FAILURE)
				{
					 // ba and ca are parallel which degenerates to a line.
					return GReturn::FAILURE;
				}

				GW::MATH::GVector::DotD(
					_outPlane.data,
					_planePositionA.xyz(),
					_outPlane.distance);

				return GReturn::SUCCESS;
			}

			static GReturn IsTriangleD(const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult)
			{
				MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle.b,
					_triangle.a,
					difference_ba);

				MATH::GVECTORD difference_ca = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle.c,
					_triangle.a,
					difference_ca);

				MATH::GVECTORD triangle_normal = {};
				GW::MATH::GVector::CrossVector3D(
					difference_ba,
					difference_ca,
					triangle_normal);

				double triangle_area = 0;
				GW::MATH::GVector::DotD(
					triangle_normal,
					triangle_normal,
					triangle_area);

				_outResult = (G_ABS(triangle_area) < G_COLLISION_THRESHOLD_D) ? GCollisionCheck::NO_COLLISION : GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToConvexPolygonD(const MATH::GVECTORD _queryPoint, const MATH::GVECTORD* _polygonPoints,
													 const unsigned int _pointsCount, GCollisionCheck& _outResult)
			{
				if(!_polygonPoints || _pointsCount < 3)
				{
					return GReturn::FAILURE;
				}

				auto Collinear = [&](const GW::MATH::GVECTORD p, const GW::MATH::GLINED l)
				{
					if(p.x <= G_LARGER(l.start.x, l.end.x) && p.x <= G_SMALLER(l.start.x, l.end.x) &&
					   p.y <= G_LARGER(l.start.y, l.end.y) && p.y <= G_SMALLER(l.start.y, l.end.y))
					{
						return true;
					}

					return false;
				};

				auto Area2D = [&](const MATH::GVECTORD a, const MATH::GVECTORD b, const MATH::GVECTORD p)
				{
					double winding = 0;

					GW::MATH::GVECTORD difference_pa = {};
					GW::MATH::GVector::SubtractVectorD(
						p,
						a,
						difference_pa);

					GW::MATH::GVECTORD difference_ba = {};
					GW::MATH::GVector::SubtractVectorD(
						b,
						a,
						difference_ba);

					GW::MATH::GVector::CrossVector2D(
						difference_pa,
						difference_ba,
						winding);

					if(G_ABS(winding) < G_COLLISION_THRESHOLD_F)
					{
						return 0;
					}
					else if(winding > 0.0f)
					{
						return 1;
					}
					else
					{
						return -1;
					}
				};

				auto IsCrossing = [&](const GW::MATH::GLINED l1, const GW::MATH::GLINED l2)
				{
					int winding1 = Area2D(l1.start, l1.end, l2.start);
					int winding2 = Area2D(l1.start, l1.end, l2.end);
					int winding3 = Area2D(l2.start, l2.end, l1.start);
					int winding4 = Area2D(l2.start, l2.end, l1.end);

					if(winding1 != winding2 && winding3 != winding4)
					{
						return true;
					}
					if(winding1 == 0 && Collinear(l2.start, l1))
					{
						return true;
					}
					if(winding2 == 0 && Collinear(l2.end, l1))
					{
						return true;
					}
					if(winding3 == 0 && Collinear(l1.start, l2))
					{
						return true;
					}
					if(winding4 == 0 && Collinear(l1.end, l2))
					{
						return true;
					}

					return false;
				};

				GW::MATH::GLINED inf_line =
				{
					{
						{
							{{{_queryPoint.x, _queryPoint.y, 0.0f, 0.0f}}},
							{{{static_cast<double>(0xffffffff), _queryPoint.y}}}
						}
					}
				};

				int count = 0;
				int i = 0;

				do
				{
					GW::MATH::GLINED side = {};
					side.start = _polygonPoints[i];
					side.end = _polygonPoints[(i + 1) % _pointsCount];

					if(IsCrossing(side, inf_line))
					{
						if(Area2D(side.start, _queryPoint, side.end) == 0)
						{
							_outResult = static_cast<GCollisionCheck>(Collinear(_queryPoint, side));
							return GReturn::SUCCESS;
						}
						count++;
					}

					i = (i + 1) % _pointsCount;
				}
				while(i != 0);

				_outResult = static_cast<GCollisionCheck>(count & 1);
				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToLineD(const MATH::GLINED _line, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				MATH::GVECTORD line_a = _line.start.xyz();

				MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_line.end.xyz(),
					line_a,
					difference_ba);

				MATH::GVECTORD difference_pa = {};
				GW::MATH::GVector::SubtractVectorD(
					_queryPoint.xyz(),
					line_a,
					difference_pa);

				double interval = 0.0;
				GW::MATH::GVector::DotD(
					difference_pa,
					difference_ba,
					interval);

				double sq_length = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_ba,
					sq_length);

				// If line degenerates to point
				if(sq_length <= G_COLLISION_THRESHOLD_D)
				{
					_outPoint = line_a;
					return GReturn::SUCCESS;
				}

				interval /= sq_length;

				interval = G_CLAMP(
					interval,
					0.0,
					1.0f);

				GW::MATH::GVector::ScaleD(
					difference_ba,
					interval,
					_outPoint);

				GW::MATH::GVector::AddVectorD(
					line_a,
					_outPoint,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointsToLineFromLineD(const MATH::GLINED _line1, const MATH::GLINED _line2, MATH::GVECTORD& _outPoint1, MATH::GVECTORD& _outPoint2)
			{
				MATH::GLINED line1 =
				{
					{
						{
							_line1.start.xyz(),
							_line1.end.xyz()
						}
					}
				};

				MATH::GLINED line2 =
				{
					{
						{
							_line2.start.xyz(),
							_line2.end.xyz()
						}
					}
				};

				MATH::GVECTORD direction1 = {};
				GW::MATH::GVector::SubtractVectorD(
					line1.end,
					line1.start,
					direction1);

				MATH::GVECTORD direction2 = {};
				GW::MATH::GVector::SubtractVectorD(
					line2.end,
					line2.start,
					direction2);

				MATH::GVECTORD distance = {};
				GW::MATH::GVector::SubtractVectorD(
					line1.start,
					line2.start,
					distance);

				double sq_length1 = 0.0;
				GW::MATH::GVector::DotD(
					direction1,
					direction1,
					sq_length1);

				double sq_length2 = 0.0;
				GW::MATH::GVector::DotD(
					direction2,
					direction2,
					sq_length2);

				double f = 0.0;
				GW::MATH::GVector::DotD(
					direction2,
					distance,
					f);

				double interval1 = 0.0;
				double interval2 = 0.0;

				int use_cases = 0x00;

				// First line degenerates to a point.
				if(sq_length1 <= G_COLLISION_THRESHOLD_D)
				{
					use_cases |= 0x01;
				}

				// Second line degenerates to a point.
				if(sq_length2 <= G_COLLISION_THRESHOLD_D)
				{
					use_cases |= 0x02;
				}

				switch(use_cases)
				{
					// First line degenerates to a point.
					case 0x01:
					{
						interval2 = f / sq_length2;
						interval2 = G_CLAMP(
							interval2,
							0.0,
							1.0f);
					}
					break;
					// Second line degenerates to a point.
					case 0x02:
					{
						double c = 0.0;
						GW::MATH::GVector::DotD(
							direction1,
							distance,
							c);

						interval1 = G_CLAMP(
							-c / sq_length1,
							0.0,
							1.0f);
					}
					break;
					// Both lines degenerate to a point.
					case 0x03:
					{
						_outPoint1 = line1.start.xyz();
						_outPoint2 = line2.start.xyz();
						return GReturn::SUCCESS;
					}
					break;
					default:
					{
						double c = 0.0;
						GW::MATH::GVector::DotD(
							direction1,
							distance,
							c);

						double b = 0.0;
						GW::MATH::GVector::DotD(
							direction1,
							direction2,
							b);

						double denom = sq_length1 * sq_length2 - b * b;

						// Get interval for the first line from the second line unless lines are parallel.
						if(denom != 0.0)
						{
							interval1 = G_CLAMP(
								(b * f - c * sq_length2) / denom,
								0.0,
								1.0f);
						}
						else
						{
							interval1 = 0.0;
						}

						// Get interval for the second line.
						interval2 = (b * interval1 + f) / sq_length2;

						if(interval2 < 0.0)
						{
							interval2 = 0.0;

							interval1 = G_CLAMP(
								-c / sq_length1,
								0.0,
								1.0f);
						}
						else if(interval2 > 1.0f)
						{
							interval2 = 1.0f;

							interval1 = G_CLAMP(
								(b - c) / sq_length1,
								0.0,
								1.0f);
						}
					}
					break;
				}

				GW::MATH::GVector::ScaleD(
					direction1,
					interval1,
					direction1);

				GW::MATH::GVector::AddVectorD(
					line1.start,
					direction1,
					_outPoint1);

				GW::MATH::GVector::ScaleD(
					direction2,
					interval2,
					direction2);

				GW::MATH::GVector::AddVectorD(
					line2.start,
					direction2,
					_outPoint2);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToRayD(const MATH::GRAYD _ray, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				MATH::GVECTORD line_a = _ray.position.xyz();
				MATH::GVECTORD line_direction = _ray.direction.xyz();

				MATH::GVECTORD difference_pa = {};
				GW::MATH::GVector::SubtractVectorD(
					_queryPoint.xyz(),
					line_a,
					difference_pa);

				double interval = 0.0;
				GW::MATH::GVector::DotD(
					difference_pa,
					line_direction,
					interval);

				double sq_length = 0.0;
				GW::MATH::GVector::DotD(
					line_direction,
					line_direction,
					sq_length);

				// If line degenerates to point
				if(sq_length <= G_COLLISION_THRESHOLD_D)
				{
					_outPoint = line_a;
					return GReturn::SUCCESS;
				}

				interval /= sq_length;

				interval = interval < 0.0 ? 0.0 : interval;

				GW::MATH::GVector::ScaleD(
					line_direction,
					interval,
					_outPoint);

				GW::MATH::GVector::AddVectorD(
					line_a,
					_outPoint,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToTriangleD(const MATH::GTRIANGLED _triangle, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				GW::MATH::GVECTORD a = _triangle.a.xyz();
				GW::MATH::GVECTORD b = _triangle.b.xyz();
				GW::MATH::GVECTORD c = _triangle.c.xyz();
				GW::MATH::GVECTORD p = _queryPoint.xyz();

				MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					b,
					a,
					difference_ba);

				MATH::GVECTORD difference_ca = {};
				GW::MATH::GVector::SubtractVectorD(
					c,
					a,
					difference_ca);

				MATH::GVECTORD difference_pa = {};
				GW::MATH::GVector::SubtractVectorD(
					p,
					a,
					difference_pa);

				double dot1 = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_pa,
					dot1);

				double dot2 = 0.0;
				GW::MATH::GVector::DotD(
					difference_ca,
					difference_pa,
					dot2);

				// p is closest to vertex a
				if(dot1 <= G_COLLISION_THRESHOLD_D && dot2 <= G_COLLISION_THRESHOLD_D)
				{
					_outPoint = a;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORD difference_pb = {};
				GW::MATH::GVector::SubtractVectorD(
					p,
					b,
					difference_pb);

				double dot3 = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_pb,
					dot3);

				double dot4 = 0.0;
				GW::MATH::GVector::DotD(
					difference_ca,
					difference_pb,
					dot4);

				// p is closest to vertex b
				if(dot3 >= G_COLLISION_THRESHOLD_D && dot4 <= dot3)
				{
					_outPoint = b;
					return GReturn::SUCCESS;
				}

				double vc = dot1 * dot4 - dot3 * dot2;

				// p is closest to edge ab
				if(vc <= G_COLLISION_THRESHOLD_D && dot1 >= G_COLLISION_THRESHOLD_D && dot3 <= G_COLLISION_THRESHOLD_D)
				{
					double interval = dot1 / (dot1 - dot3);

					GW::MATH::GVector::ScaleD(
						difference_ba,
						interval,
						_outPoint);

					GW::MATH::GVector::AddVectorD(
						a,
						_outPoint,
						_outPoint);

					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORD difference_pc = {};
				GW::MATH::GVector::SubtractVectorD(
					p,
					c,
					difference_pc);

				double dot5 = 0.0;
				GW::MATH::GVector::DotD(
					difference_ca,
					difference_pc,
					dot5);

				double dot6 = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_pc,
					dot6);

				// p is closest to vertex c
				if(dot5 >= G_COLLISION_THRESHOLD_D && dot6 <= dot5)
				{
					_outPoint = c;
					return GReturn::SUCCESS;
				}

				double vb = dot6 * dot2 - dot1 * dot5;
				// p is closest to edge ac
				if(vb <= G_COLLISION_THRESHOLD_D && dot2 >= G_COLLISION_THRESHOLD_D && dot5 <= G_COLLISION_THRESHOLD_D)
				{
					double interval = dot2 / (dot2 - dot5);

					GW::MATH::GVector::ScaleD(
						difference_ca,
						interval,
						_outPoint);

					GW::MATH::GVector::AddVectorD(
						a,
						_outPoint,
						_outPoint);

					return GReturn::SUCCESS;
				}

				double va = dot3 * dot5 - dot6 * dot4;
				// p is closest to edge bc
				if(va <= G_COLLISION_THRESHOLD_D && (dot4 - dot3) >= G_COLLISION_THRESHOLD_D && (dot6 - dot5) >= G_COLLISION_THRESHOLD_D)
				{
					double h = dot4 - dot3;
					double interval = h / (h + dot6 - dot5);

					GW::MATH::GVector::SubtractVectorD(
						c,
						b,
						_outPoint);

					GW::MATH::GVector::ScaleD(
						_outPoint,
						interval,
						_outPoint);

					GW::MATH::GVector::AddVectorD(
						b,
						_outPoint,
						_outPoint);

					return GReturn::SUCCESS;
				}

				// p is closest to some point on the face
				double denom = 1.0 / (va + vb + vc);
				double v = vb * denom;
				double w = vc * denom;

				GW::MATH::GVector::ScaleD(
					difference_ba,
					v,
					_outPoint);

				GW::MATH::GVECTORD acw = {};
				GW::MATH::GVector::ScaleD(
					difference_ca,
					w,
					acw);

				GW::MATH::GVector::AddVectorD(
					acw,
					_outPoint,
					_outPoint);

				GW::MATH::GVector::AddVectorD(
					a,
					_outPoint,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToPlaneD(const MATH::GPLANED _plane, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				MATH::GVECTORD plane_normal = _plane.data.xyz();
				MATH::GVECTORD query_point = _queryPoint.xyz();

				double interval = 0.0;
				GW::MATH::GVector::DotD(
					plane_normal,
					query_point,
					interval);

				interval -= _plane.distance;

				GW::MATH::GVector::ScaleD(
					plane_normal,
					interval,
					_outPoint);

				GW::MATH::GVector::SubtractVectorD(
					query_point,
					_outPoint,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToSphereD(const MATH::GSPHERED _sphere, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				GW::MATH::GVECTORD direction = {};
				GW::MATH::GVector::SubtractVectorD(
					_queryPoint,
					_sphere.data,
					direction);

				if(GW::MATH::GVector::NormalizeD(direction.xyz(), direction) == GReturn::FAILURE)
				{
					_outPoint = _queryPoint;
					return GReturn::FAILURE;
				}

				GW::MATH::GVector::ScaleD(
					direction,
					_sphere.radius,
					direction);

				GW::MATH::GVector::AddVectorD(
					_sphere.data.xyz(),
					direction,
					_outPoint);

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToCapsuleD(const MATH::GCAPSULED _capsule, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				ClosestPointToLineD(
					{{{_capsule.data[0].xyz(),_capsule.data[1].xyz()}}},
					_queryPoint,
					_outPoint);

				return ClosestPointToSphereD(
					{{{_outPoint.x, _outPoint.y, _outPoint.z, _capsule.radius}}},
					_queryPoint,
					_outPoint);
			}

			static GReturn ClosestPointToAABBD(const MATH::GAABBMMD _aabb, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				for(int i = 0; i < 3; i++)
				{
					_outPoint.data[i] = _queryPoint.data[i];

					_outPoint.data[i] = G_LARGER(
						_outPoint.data[i],
						_aabb.min.data[i]);

					_outPoint.data[i] = G_SMALLER(
						_outPoint.data[i],
						_aabb.max.data[i]);
				}

				if(G_ABS(_queryPoint.x - _outPoint.x) < G_COLLISION_THRESHOLD_D &&
				   G_ABS(_queryPoint.y - _outPoint.y) < G_COLLISION_THRESHOLD_D &&
				   G_ABS(_queryPoint.z - _outPoint.z) < G_COLLISION_THRESHOLD_D)
				{
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn ClosestPointToOBBD(const MATH::GOBBD _obb, const MATH::GVECTORD _queryPoint, MATH::GVECTORD& _outPoint)
			{
				double xx2 = 2.0f * _obb.rotation.x * _obb.rotation.x;
				double yy2 = 2.0f * _obb.rotation.y * _obb.rotation.y;
				double zz2 = 2.0f * _obb.rotation.z * _obb.rotation.z;
				double xy2 = 2.0f * _obb.rotation.x * _obb.rotation.y;
				double xz2 = 2.0f * _obb.rotation.x * _obb.rotation.z;
				double yz2 = 2.0f * _obb.rotation.y * _obb.rotation.z;
				double wx2 = 2.0f * _obb.rotation.w * _obb.rotation.x;
				double wy2 = 2.0f * _obb.rotation.w * _obb.rotation.y;
				double wz2 = 2.0f * _obb.rotation.w * _obb.rotation.z;

				MATH::GVECTORD obb_rotation[3] =
				{
					{{{1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2}}},
					{{{xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2}}},
					{{{xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2}}}
				};

				MATH::GVECTORD difference = {};
				GW::MATH::GVector::SubtractVectorD(
					_queryPoint.xyz(),
					_obb.data->xyz(),
					difference);

				_outPoint = _obb.center.xyz();

				MATH::GVECTORD world_coord = {};

				double distance = 0.0;

				for(int i = 0; i < 3; i++)
				{
					// Project the difference to this axis
					GW::MATH::GVector::DotD(
						difference,
						obb_rotation[i],
						distance);

					// Clamp to boundary
					distance = G_SMALLER(
						distance,
						_obb.extent.data[i]);

					distance = G_LARGER(
						distance,
						-_obb.extent.data[i]);

					GW::MATH::GVector::ScaleD(
						obb_rotation[i],
						distance,
						world_coord);

					GW::MATH::GVector::AddVectorD(
						_outPoint,
						world_coord,
						_outPoint);
				}

				if(G_ABS(_queryPoint.x - _outPoint.x) < G_COLLISION_THRESHOLD_D &&
				   G_ABS(_queryPoint.y - _outPoint.y) < G_COLLISION_THRESHOLD_D &&
				   G_ABS(_queryPoint.z - _outPoint.z) < G_COLLISION_THRESHOLD_D)
				{
					return GReturn::FAILURE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn ComputeSphereFromPointsD(const MATH::GVECTORD* _pointCloud, const unsigned int _pointsCount, MATH::GSPHERED& _outSphere)
			{
				if(!_pointCloud || _pointsCount < 2)
				{
					return GReturn::FAILURE;
				}

				// Find a good almost bounding sphere
				unsigned int min_x = 0;
				unsigned int max_x = 0;
				unsigned int min_y = 0;
				unsigned int max_y = 0;
				unsigned int min_z = 0;
				unsigned int max_z = 0;

				for(unsigned int i = 1; i < _pointsCount; i++)
				{
					if(_pointCloud[i].x < _pointCloud[min_x].x)
					{
						min_x = i;
					}
					if(_pointCloud[i].x > _pointCloud[max_x].x)
					{
						max_x = i;
					}

					if(_pointCloud[i].y < _pointCloud[min_y].y)
					{
						min_y = i;
					}
					if(_pointCloud[i].y > _pointCloud[max_y].y)
					{
						max_y = i;
					}

					if(_pointCloud[i].z < _pointCloud[min_z].z)
					{
						min_z = i;
					}
					if(_pointCloud[i].z > _pointCloud[max_z].z)
					{
						max_z = i;
					}
				}

				GW::MATH::GVECTORD difference = {};
				GW::MATH::GVector::SubtractVectorD(
					_pointCloud[max_x],
					_pointCloud[min_x],
					difference);

				double sq_distance_x = 0.0;
				GW::MATH::GVector::DotD(
					difference,
					difference,
					sq_distance_x);

				GW::MATH::GVector::SubtractVectorD(
					_pointCloud[max_y],
					_pointCloud[min_y],
					difference);

				double sq_distance_y = 0.0;
				GW::MATH::GVector::DotD(
					difference,
					difference,
					sq_distance_y);

				GW::MATH::GVector::SubtractVectorD(
					_pointCloud[max_z],
					_pointCloud[min_z],
					difference);

				double sq_distance_z = 0.0;
				GW::MATH::GVector::DotD(
					difference,
					difference,
					sq_distance_z);

				int min = min_x;
				int max = max_x;

				if((sq_distance_y > sq_distance_x) && (sq_distance_y > sq_distance_z))
				{
					min = min_y;
					max = max_y;
				}

				if((sq_distance_z > sq_distance_x) && (sq_distance_z > sq_distance_y))
				{
					min = min_z;
					max = max_z;
				}

				GW::MATH::GVector::AddVectorD(
					_pointCloud[min].xyz(),
					_pointCloud[max].xyz(),
					_outSphere.data);

				GW::MATH::GVector::ScaleD(
					_outSphere.data,
					0.5f,
					_outSphere.data);

				GW::MATH::GVector::SubtractVectorD(
					_pointCloud[max].xyz(),
					_outSphere.data,
					difference);

				GW::MATH::GVector::DotD(
					difference,
					difference,
					_outSphere.radius);

				_outSphere.radius = sqrt(_outSphere.radius);

				// Improve the bound and include all points
				auto UpdateSphereBounds = [&](const MATH::GVECTORD p, MATH::GSPHERED& outSphere)
				{
					GW::MATH::GVECTORD difference = {};
					GW::MATH::GVector::SubtractVectorD(
						p.xyz(),
						outSphere.data.xyz(),
						difference);

					double sq_distance = 0.0;
					GW::MATH::GVector::DotD(
						difference,
						difference,
						sq_distance);

					if(sq_distance > outSphere.radius* outSphere.radius)
					{
						double distance = sqrt(sq_distance);
						double radius = (outSphere.radius + distance) * 0.5f;
						double increase = (radius - outSphere.radius) / distance;

						GW::MATH::GVector::ScaleD(
							difference,
							increase,
							difference);

						GW::MATH::GVector::AddVectorD(
							outSphere.data,
							difference,
							outSphere.data);

						outSphere.radius = radius;
					}
				};

				for(unsigned int i = 0; i < _pointsCount; i++)
				{
					UpdateSphereBounds(
						_pointCloud[i],
						_outSphere);
				}

				return GReturn::SUCCESS;
			}

			static GReturn ComputeAABBFromPointsD(const MATH::GVECTORD* _pointCloud, const unsigned int _pointsCount, MATH::GAABBMMD& _outAABB)
			{
				if(!_pointCloud || _pointsCount == 0)
				{
					return GReturn::FAILURE;
				}

				_outAABB.min = _pointCloud[0];
				_outAABB.max = _pointCloud[0];

				for(unsigned int i = 1; i < _pointsCount; i++)
				{
					if(_outAABB.min.x > _pointCloud[i].x)
					{
						_outAABB.min.x = _pointCloud[i].x;
					}
					if(_outAABB.max.x < _pointCloud[i].x)
					{
						_outAABB.max.x = _pointCloud[i].x;
					}

					if(_outAABB.min.y > _pointCloud[i].y)
					{
						_outAABB.min.y = _pointCloud[i].y;
					}
					if(_outAABB.max.y < _pointCloud[i].y)
					{
						_outAABB.max.y = _pointCloud[i].y;
					}

					if(_outAABB.min.z > _pointCloud[i].z)
					{
						_outAABB.min.z = _pointCloud[i].z;
					}
					if(_outAABB.max.z < _pointCloud[i].z)
					{
						_outAABB.max.z = _pointCloud[i].z;
					}
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToLineD(const MATH::GVECTORD _point, const MATH::GLINED _line, GCollisionCheck& _outResult)
			{
				MATH::GVECTORD point = _point.xyz();

				MATH::GLINED line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					line.end,
					line.start,
					difference_ba);

				double distance_ba = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_ba,
					distance_ba);
				distance_ba = sqrt(distance_ba);

				GW::MATH::GVECTORD difference_pa = {};
				GW::MATH::GVector::SubtractVectorD(
					point,
					line.start,
					difference_pa);

				double distance_pa = 0.0;
				GW::MATH::GVector::DotD(
					difference_pa,
					difference_pa,
					distance_pa);
				distance_pa = sqrt(distance_pa);

				GW::MATH::GVECTORD difference_bp = {};
				GW::MATH::GVector::SubtractVectorD(
					line.end,
					point,
					difference_bp);

				double distance_bp = 0.0;
				GW::MATH::GVector::DotD(
					difference_bp,
					difference_bp,
					distance_bp);
				distance_bp = sqrt(distance_bp);

				_outResult = (G_WITHIN_STANDARD_DEVIATION_F(distance_ba, distance_pa + distance_bp)) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToRayD(const MATH::GVECTORD _point, const MATH::GRAYD _ray, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD difference_pa = {};
				GW::MATH::GVector::SubtractVectorD(
					_point.xyz(),
					_ray.position.xyz(),
					difference_pa);

				// Point is ray position
				if(GW::MATH::GVector::NormalizeD(difference_pa, difference_pa) == GReturn::FAILURE)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(difference_pa.x - _ray.direction.x) < G_COLLISION_THRESHOLD_D &&
				   G_ABS(difference_pa.y - _ray.direction.y) < G_COLLISION_THRESHOLD_D &&
				   G_ABS(difference_pa.z - _ray.direction.z) < G_COLLISION_THRESHOLD_D)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToTriangleD(const MATH::GVECTORD _point, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric)
			{
				MATH::GPLANED plane = {};
				if(ComputePlaneD(_triangle.a, _triangle.b, _triangle.c, plane) == GReturn::FAILURE)
				{
					// Degenerate triangle.
					return GReturn::FAILURE;
				}

				TestPointToPlaneD(
					_point,
					plane,
					_outResult);

				if(static_cast<int>(_outResult) > 0)
				{
					MATH::GVECTORD barycentric = {};
					// Compute plane already handles fail case of degenerate triangle.
					BarycentricD(_triangle.a, _triangle.b, _triangle.c, _point, barycentric);
                    
                    _outResult = (G_GREATER_OR_EQUAL_STANDARD_D(barycentric.y, 0.0) &&
                                  G_GREATER_OR_EQUAL_STANDARD_D(barycentric.z, 0.0) &&
                                  G_LESSER_OR_EQUAL_STANDARD_D(barycentric.y + barycentric.z, 1.0)) ?
                        GCollisionCheck::COLLISION :
                        GCollisionCheck::NO_COLLISION;

					if(_outBarycentric)
					{
						*_outBarycentric = barycentric;
					}
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToPlaneD(const MATH::GVECTORD _point, const MATH::GPLANED _plane, GCollisionCheck& _outResult)
			{
				double distance = 0.0;
				GW::MATH::GVector::DotD(
					_point.xyz(),
					_plane.data.xyz(),
					distance);
				distance = distance - _plane.distance;

                _outResult = (distance > 0.0) ? GCollisionCheck::ABOVE : GCollisionCheck::BELOW;
                if (G_ABS(distance) < G_COLLISION_THRESHOLD_D)
                    _outResult = GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToSphereD(const MATH::GVECTORD _point, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_point,
					_sphere.data.xyz(),
					difference_ba);

				double sq_distance = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_ba,
					sq_distance);

				double sq_radius = _sphere.radius * _sphere.radius;

				_outResult = sq_distance <= sq_radius ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToCapsuleD(const MATH::GVECTORD _point, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				GW::MATH::GSPHERED sphere = {};

				if(ClosestPointToCapsuleD(_capsule, _point, sphere.data) == GReturn::FAILURE)
				{
					// Point is center of capsule
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				sphere.radius = _capsule.radius;
				return TestPointToSphereD(
					_point,
					sphere,
					_outResult);
			}

			static GReturn TestPointToAABBD(const MATH::GVECTORD _point, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				if(G_ABS(_aabb.center.x - _point.x) - _aabb.extent.x > G_COLLISION_THRESHOLD_D ||
				   G_ABS(_aabb.center.y - _point.y) - _aabb.extent.y > G_COLLISION_THRESHOLD_D ||
				   G_ABS(_aabb.center.z - _point.z) - _aabb.extent.z > G_COLLISION_THRESHOLD_D)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPointToOBBD(const MATH::GVECTORD _point, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD closest_point = {};

				ClosestPointToOBBD(
					_obb,
					_point,
					closest_point);

				if(G_ABS(closest_point.x - _point.x) > G_COLLISION_THRESHOLD_D ||
				   G_ABS(closest_point.y - _point.y) > G_COLLISION_THRESHOLD_D ||
				   G_ABS(closest_point.z - _point.z) > G_COLLISION_THRESHOLD_D)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToLineD(const MATH::GLINED _line1, const MATH::GLINED _line2, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD closest_point1 = {};
				GW::MATH::GVECTORD closest_point2 = {};

				ClosestPointsToLineFromLineD(
					_line1,
					_line2,
					closest_point1,
					closest_point2);

				if(G_ABS(closest_point1.x - closest_point2.x) > G_COLLISION_THRESHOLD_D ||
				   G_ABS(closest_point1.y - closest_point2.y) > G_COLLISION_THRESHOLD_D ||
				   G_ABS(closest_point1.z - closest_point2.z) > G_COLLISION_THRESHOLD_D)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}
				else
				{
					_outResult = GCollisionCheck::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToRayD(const MATH::GLINED _line, const MATH::GRAYD _ray, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD closest_point1 = {};
				ClosestPointToRayD(
					_ray,
					_line.start,
					closest_point1);

				GW::MATH::GVECTORD closest_point2 = {};
				ClosestPointToRayD(
					_ray,
					_line.end,
					closest_point2);

				return TestLineToLineD(
					_line,
					{{{closest_point1, closest_point2}}},
					_outResult);
			}

			static GReturn TestLineToTriangleD(const MATH::GLINED _line, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric)
			{
				MATH::GLINED line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				MATH::GTRIANGLED triangle =
				{
					{
						{
							_triangle.a.xyz(),
							_triangle.b.xyz(),
							_triangle.c.xyz()
						}
					}
				};

				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle.b,
					triangle.a,
					difference_ba);

				GW::MATH::GVECTORD difference_ca = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle.c,
					triangle.a,
					difference_ca);

				GW::MATH::GVECTORD norm = {};
				GW::MATH::GVector::CrossVector3D(
					difference_ca,
					difference_ba,
					norm);

				GW::MATH::GVECTORD difference_line = {};
				GW::MATH::GVector::SubtractVectorD(
					line.start,
					line.end,
					difference_line);

				double dot = 0.0;
				GW::MATH::GVector::DotD(
					difference_line,
					norm,
					dot);

				// Segment is parallel to triangle
				if(dot <= 0.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORD difference_pa = {};
				GW::MATH::GVector::SubtractVectorD(
					line.start,
					triangle.a,
					difference_pa);

				double interval = 0.0;
				GW::MATH::GVector::DotD(
					difference_pa,
					norm,
					interval);

				if(interval < 0.0 || interval > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORD cross = {};
				GW::MATH::GVector::CrossVector3D(
					difference_line,
					difference_pa,
					cross);

				// Test to see if within bounds of barycentric coordinates.
				GW::MATH::GVECTORD barycentric = {};
				GW::MATH::GVector::DotD(
					difference_ca,
					cross,
					barycentric.y);
				barycentric.y = -barycentric.y;

				if(barycentric.y < 0.0 || barycentric.y > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::DotD(
					difference_ba,
					cross,
					barycentric.z);

				if(barycentric.z < 0.0 || barycentric.y + barycentric.z > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(_outBarycentric)
				{
					double over_denom = 1.0 / dot;

					barycentric.y *= over_denom;
					barycentric.z *= over_denom;

					*_outBarycentric =
					{
						{
							{
								1.0 - barycentric.y - barycentric.z,
						barycentric.y,
						barycentric.z
							}
						}
					};
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestLineToPlaneD(const MATH::GLINED _line, const MATH::GPLANED _plane, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_line.end.xyz(),
					_line.start.xyz(),
					difference_ba);

				double denom = 0.0;
				GW::MATH::GVector::DotD(
					_plane.data.xyz(),
					difference_ba,
					denom);

				double num = 0.0;
				GW::MATH::GVector::DotD(
					_plane.data.xyz(),
					_line.start.xyz(),
					num);

				if(G_ABS(denom) < G_COLLISION_THRESHOLD_D)
				{
					// Coplanar
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				double interval = (_plane.distance - num) / denom;

				if(interval >= 0.0 && interval <= 1.0f)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					// Half plane test
					num -= _plane.distance;
					_outResult = (num < 0.0) ?
						_outResult = GCollisionCheck::BELOW :
						_outResult = GCollisionCheck::ABOVE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestLineToSphereD(const MATH::GLINED _line, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD closest_point = {};
				ClosestPointToLineD(
					_line,
					_sphere.data,
					closest_point);

				return TestPointToSphereD(
					closest_point,
					_sphere,
					_outResult);
			}

			static GReturn TestLineToCapsuleD(const MATH::GLINED _line, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD closest_point1 = {};
				GW::MATH::GVECTORD closest_point2 = {};
				ClosestPointsToLineFromLineD(
					_line,
					{{{_capsule.data[0],_capsule.data[1]}}},
					closest_point1,
					closest_point2);

				return TestPointToSphereD(
					closest_point1,
					{{{closest_point2.x,closest_point2.y,closest_point2.z,_capsule.radius}}},
					_outResult);
			}

			static GReturn TestLineToAABBD(const MATH::GLINED _line, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				MATH::GLINED line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				GW::MATH::GVECTORD extent = _aabb.extent.xyz();

				GW::MATH::GVECTORD line_midpoint = {};
				GW::MATH::GVector::AddVectorD(
					line.start,
					line.end,
					line_midpoint);

				GW::MATH::GVector::ScaleD(
					line_midpoint,
					0.5f,
					line_midpoint);

				GW::MATH::GVECTORD line_mid_length = {};
				GW::MATH::GVector::SubtractVectorD(
					line.end,
					line_midpoint,
					line_mid_length);

				// Translate box and line to origin
				GW::MATH::GVECTORD origin = {};
				GW::MATH::GVector::SubtractVectorD(
					line_midpoint,
					_aabb.center.xyz(),
					origin);

				GW::MATH::GVECTORD axis =
				{
					{
						{
							G_ABS(line_mid_length.x),
							G_ABS(line_mid_length.y),
							G_ABS(line_mid_length.z)
						}
					}
				};

				// Separating axes
				for(int i = 0; i < 3; i++)
				{
					if(G_ABS(origin.data[i]) > extent.data[i] + axis.data[i])
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				axis.x += G_COLLISION_THRESHOLD_D;
				axis.y += G_COLLISION_THRESHOLD_D;
				axis.z += G_COLLISION_THRESHOLD_D;

				if(G_ABS(origin.y * line_mid_length.z - origin.z * line_mid_length.y) >
				   extent.y* axis.z + extent.z * axis.y)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(origin.z * line_mid_length.x - origin.x * line_mid_length.z) >
				   extent.x* axis.z + extent.z * axis.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(origin.x * line_mid_length.z - origin.y * line_mid_length.x) >
				   extent.x* axis.y + extent.z * axis.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestLineToOBBD(const MATH::GLINED _line, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				MATH::GLINED line =
				{
					{
						{
							_line.start,
							_line.end
						}
					}
				};
				line.start.w = 1.0f;
				line.end.w = 1.0f;

				MATH::GMATRIXD obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionD(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXD obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0}}},
							{{{0.0, 0.0, 0.0, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					line.start,
					line.start);
				GW::MATH::GVector::AddVectorD(
					line.start,
					_obb.center,
					line.start);

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					line.end,
					line.end);
				GW::MATH::GVector::AddVectorD(
					line.end,
					_obb.center,
					line.end);

				return TestLineToAABBD(
					line,
					{{{_obb.center, _obb.extent}}},
					_outResult);
			}

			static GReturn TestRayToTriangleD(const MATH::GRAYD _ray, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD* _outBarycentric)
			{
				MATH::GRAYD ray =
				{
					{
						{
							_ray.position.xyz(),
							_ray.direction.xyz()
						}
					}
				};

				MATH::GTRIANGLED triangle =
				{
					{
						{
							_triangle.a.xyz(),
							_triangle.b.xyz(),
							_triangle.c.xyz()
						}
					}
				};

				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle.b,
					triangle.a,
					difference_ba);

				GW::MATH::GVECTORD difference_ca = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle.c,
					triangle.a,
					difference_ca);

				GW::MATH::GVECTORD q = {};
				GW::MATH::GVector::CrossVector3D(
					ray.direction,
					difference_ca,
					q);

				double det = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					q,
					det);

				// Ray is parallel to or points away from triangle
				if(G_ABS(det) < G_COLLISION_THRESHOLD_D)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				double denom = 1.0f / det;

				GW::MATH::GVECTORD s = {};
				GW::MATH::GVector::SubtractVectorD(
					ray.position,
					triangle.a,
					s);

				GW::MATH::GVECTORD barycentric = {};
				GW::MATH::GVector::DotD(
					s,
					q,
					barycentric.y);
				barycentric.y *= denom;

				if(barycentric.y < -G_COLLISION_THRESHOLD_D)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORD r = {};
				GW::MATH::GVector::CrossVector3D(
					s,
					difference_ba,
					r);

				GW::MATH::GVector::DotD(
					ray.direction,
					r,
					barycentric.z);
				barycentric.z *= denom;

				if(barycentric.z < -G_COLLISION_THRESHOLD_D || barycentric.y + barycentric.z > 1.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(_outBarycentric)
				{
					*_outBarycentric = barycentric;
					_outBarycentric->x = 1.0f - barycentric.y - barycentric.z;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestRayToPlaneD(const MATH::GRAYD _ray, const MATH::GPLANED _plane, GCollisionCheck& _outResult)
			{
				double denom = 0.0;
				GW::MATH::GVector::DotD(
					_plane.data.xyz(),
					_ray.direction.xyz(),
					denom);

				if(G_ABS(denom) < G_COLLISION_THRESHOLD_D)
				{
					// Ray is parallel
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				double num = 0.0;
				GW::MATH::GVector::DotD(
					_plane.data.xyz(),
					_ray.position.xyz(),
					num);

				double interval = (_plane.distance - num) / denom;
				if(interval >= 0.0)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					// Half plane test
					num -= _plane.distance;
					_outResult = (num < 0.0) ?
						_outResult = GCollisionCheck::BELOW :
						_outResult = GCollisionCheck::ABOVE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestRayToSphereD(const MATH::GRAYD _ray, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_ray.position.xyz(),
					_sphere.data.xyz(),
					difference_ba);

				double c = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_ba,
					c);
				c -= _sphere.radius * _sphere.radius;

				// At least one real root.
				if(c <= 0.0)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				double b = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					_ray.direction.xyz(),
					b);

				// Ray is outside sphere and pointing away from it.
				if(b > 0.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// The discriminant determines if there were solutions or not.
				double discriminant = b * b - c;
				_outResult = (discriminant < 0.0) ?
					GCollisionCheck::NO_COLLISION :
					GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestRayToCapsuleD(const MATH::GRAYD _ray, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD closest_point1 = {};
				ClosestPointToRayD(
					_ray,
					_capsule.data[0],
					closest_point1);

				GW::MATH::GSPHERED sphere = {};
				ClosestPointToRayD(
					_ray,
					_capsule.data[1],
					sphere.data);

				ClosestPointsToLineFromLineD(
					{{{closest_point1, sphere.data}}},
					{{{_capsule.data[0], _capsule.data[1]}}},
					closest_point1,
					sphere.data);

				sphere.radius = _capsule.radius;

				return TestPointToSphereD(
					closest_point1,
					sphere,
					_outResult);
			}

			static GReturn TestRayToAABBD(const MATH::GRAYD _ray, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult)
			{
				double interval1 = 0.0;
				double interval2 = 0.0;
				double interval3 = 0.0;
				double interval4 = 0.0;
				double interval5 = 0.0;
				double interval6 = 0.0;

				if(_ray.direction.x)
				{
					interval1 = (_aabb.min.x - _ray.position.x) / _ray.direction.x;
					interval2 = (_aabb.max.x - _ray.position.x) / _ray.direction.x;
				}
				else
				{
					interval1 = (_aabb.min.x - _ray.position.x) > 0.0 ?
						DBL_MAX : DBL_MIN;
					interval2 = (_aabb.max.x - _ray.position.x) > 0.0 ?
						DBL_MAX : DBL_MIN;
				}

				if(_ray.direction.y)
				{
					interval3 = (_aabb.min.y - _ray.position.y) / _ray.direction.y;
					interval4 = (_aabb.max.y - _ray.position.y) / _ray.direction.y;
				}
				else
				{
					interval3 = (_aabb.min.y - _ray.position.y) > 0.0 ?
						DBL_MAX : DBL_MIN;
					interval4 = (_aabb.max.y - _ray.position.y) > 0.0 ?
						DBL_MAX : DBL_MIN;
				}

				if(_ray.direction.z)
				{
					interval5 = (_aabb.min.z - _ray.position.z) / _ray.direction.z;
					interval6 = (_aabb.max.z - _ray.position.z) / _ray.direction.z;
				}
				else
				{
					interval5 = (_aabb.min.z - _ray.position.z) > 0.0 ?
						DBL_MAX : DBL_MIN;
					interval6 = (_aabb.max.z - _ray.position.z) > 0.0 ?
						DBL_MAX : DBL_MIN;
				}

				double interval_min = G_LARGER(G_LARGER(
					G_SMALLER(interval1, interval2),
					G_SMALLER(interval3, interval4)),
					G_SMALLER(interval5, interval6));

				double interval_max = G_SMALLER(G_SMALLER(
					G_LARGER(interval1, interval2),
					G_LARGER(interval3, interval4)),
					G_LARGER(interval5, interval6));

				if(interval_max < 0.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(interval_max < interval_min)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestRayToOBBD(const MATH::GRAYD _ray, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				MATH::GMATRIXD obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionD(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXD obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0}}},
							{{{0.0, 0.0, 0.0, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GRAYD ray =
				{
					{
						{
							_ray.position,
							_ray.direction.xyz()
						}
					}
				};
				ray.position.w = 1.0f;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					_ray.position,
					ray.position);
				GW::MATH::GVector::AddVectorD(
					ray.position,
					_obb.center,
					ray.position);

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					_ray.direction,
					ray.direction);
				GW::MATH::GVector::NormalizeD(
					ray.direction,
					ray.direction);

				MATH::GAABBMMD mm = {};
				ConvertAABBCEToAABBMMD(
					{{{_obb.center, _obb.extent}}},
					mm);

				return TestRayToAABBD(
					ray,
					mm,
					_outResult);
			}

			static GReturn TestTriangleToTriangleD(const MATH::GTRIANGLED _triangle1, const MATH::GTRIANGLED _triangle2, GCollisionCheck& _outResult)
			{
				// Half plane test if triangle1 lies completely on one side to triangle2 plane
				GW::MATH::GVECTORD difference_b2a2 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle2.b.xyz(),
					_triangle2.a.xyz(),
					difference_b2a2);

				GW::MATH::GVECTORD difference_c2a2 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle2.c.xyz(),
					_triangle2.a.xyz(),
					difference_c2a2);

				GW::MATH::GVECTORD plane2 = {};
				GW::MATH::GVector::CrossVector3D(
					difference_c2a2,
					difference_b2a2,
					plane2);

				GW::MATH::GVECTORD difference_a1a2 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle1.a.xyz(),
					_triangle2.a.xyz(),
					difference_a1a2);

				GW::MATH::GVECTORD det1 = {};
				GW::MATH::GVector::DotD(
					difference_a1a2,
					plane2,
					det1.x);

				GW::MATH::GVECTORD difference_b1a2 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle1.b.xyz(),
					_triangle2.a.xyz(),
					difference_b1a2);

				GW::MATH::GVector::DotD(
					difference_b1a2,
					plane2,
					det1.y);

				GW::MATH::GVECTORD difference_c1a2 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle1.c.xyz(),
					_triangle2.a.xyz(),
					difference_c1a2);

				GW::MATH::GVector::DotD(
					difference_c1a2,
					plane2,
					det1.z);

				if(((det1.x * det1.y) > 0.0) && ((det1.x * det1.z) > 0.0))
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Half plane test if triangle2 lies completely on one side to triangle1 plane
				GW::MATH::GVECTORD difference_b1a1 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle1.b.xyz(),
					_triangle1.a.xyz(),
					difference_b1a1);

				GW::MATH::GVECTORD difference_c1a1 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle1.c.xyz(),
					_triangle1.a.xyz(),
					difference_c1a1);

				GW::MATH::GVECTORD plane1 = {};
				GW::MATH::GVector::CrossVector3D(
					difference_c1a1,
					difference_b1a1,
					plane1);

				GW::MATH::GVECTORD difference_a2a1 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle2.a.xyz(),
					_triangle1.a.xyz(),
					difference_a2a1);

				GW::MATH::GVECTORD det2 = {};
				GW::MATH::GVector::DotD(
					difference_a2a1,
					plane1,
					det2.x);

				GW::MATH::GVECTORD difference_b2a1 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle2.b.xyz(),
					_triangle1.a.xyz(),
					difference_b2a1);

				GW::MATH::GVector::DotD(
					difference_b2a1,
					plane1,
					det2.y);

				GW::MATH::GVECTORD difference_c2a1 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle2.c.xyz(),
					_triangle1.a.xyz(),
					difference_c2a1);

				GW::MATH::GVector::DotD(
					difference_c2a1,
					plane1,
					det2.z);

				if(((det2.x * det2.y) > 0.0) && ((det2.x * det2.z) > 0.0))
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Bring both triangles into canonical form
				MATH::GTRIANGLED triangle1 = {};
				MATH::GTRIANGLED triangle2 = {};

				if(det1.x > 0.0)
				{
					if(det1.y > 0.0)
					{
						triangle1 =
						{
							{
								{
									_triangle1.c,
									_triangle1.a,
									_triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.c,
									_triangle2.b
								}
							}
						};
					}
					else if(det1.z > 0.0)
					{
						triangle1 =
						{
							{
								{
									_triangle1.b,
									_triangle1.c,
									_triangle1.a
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.c,
									_triangle2.b
								}
							}
						};
					}
					else
					{
						triangle1 =
						{
							{
								{
									_triangle1.a,
									_triangle1.b,
									_triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.b,
									_triangle2.c
								}
							}
						};
					}
				}
				else if(det1.x < 0.0)
				{
					if(det1.y < 0.0)
					{
						triangle1 =
						{
							{
								{
									_triangle1.c,
									_triangle1.a,
									_triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.b,
									_triangle2.c
								}
							}
						};
					}
					else if(det1.z < 0.0)
					{
						triangle1 =
						{
							{
								{
									_triangle1.b,
									_triangle1.c,
									_triangle1.a
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.b,
									_triangle2.c
								}
							}
						};
					}
					else
					{
						triangle1 =
						{
							{
								{
									_triangle1.a,
									_triangle1.b,
									_triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									_triangle2.a,
									_triangle2.c,
									_triangle2.b
								}
							}
						};
					}
				}
				else
				{
					if(det1.y < 0.0)
					{
						if(det1.z >= 0.0)
						{
							triangle1 =
							{
								{
									{
										_triangle1.b,
										_triangle1.c,
										_triangle1.a
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.c,
										_triangle2.b
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										_triangle1.a,
										_triangle1.b,
										_triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.b,
										_triangle2.c
									}
								}
							};
						}
					}
					else if(det1.y > 0.0)
					{
						if(det1.z > 0.0)
						{
							triangle1 =
							{
								{
									{
										_triangle1.a,
										_triangle1.b,
										_triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.c,
										_triangle2.b
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										_triangle1.b,
										_triangle1.c,
										_triangle1.a
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.b,
										_triangle2.c
									}
								}
							};
						}
					}
					else
					{
						if(det1.z > 0.0)
						{
							triangle1 =
							{
								{
									{
										_triangle1.c,
										_triangle1.a,
										_triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.b,
										_triangle2.c
									}
								}
							};
						}
						else if(det1.z < 0.0)
						{
							triangle1 =
							{
								{
									{
										_triangle1.c,
										_triangle1.a,
										_triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.c,
										_triangle2.b
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										_triangle1.a,
										_triangle1.b,
										_triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										_triangle2.a,
										_triangle2.b,
										_triangle2.c
									}
								}
							};

							// return coplanar result
							auto Orient2D = [](const double* a, const double* b, const double* c)
							{
								return ((a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]));
							};

							auto TestVertex2D = [&](
								const double* a1, const double* b1, const double* c1,
								const double* a2, const double* b2, const double* c2)
							{
								if(Orient2D(c2, a2, b1) >= 0.0)
								{
									if(Orient2D(c2, b2, b1) <= 0.0)
									{
										if(Orient2D(a1, a2, b1) > 0.0)
										{
											return (Orient2D(a1, b2, b1) <= 0.0) ? 1 : 0;
										}
										else
										{
											if(Orient2D(a1, a2, c1) >= 0.0)
											{
												return (Orient2D(b1, c1, a2) >= 0.0) ? 1 : 0;
											}
											else return 0;
										}
									}
									else
									{
										if(Orient2D(a1, b2, b1) <= 0.0)
										{
											if(Orient2D(c2, b2, c1) <= 0.0)
											{
												return (Orient2D(b1, c1, b2) >= 0.0) ? 1 : 0;
											}
											else return 0;
										}
										else return 0;
									}
								}
								else
								{
									if(Orient2D(c2, a2, c1) >= 0.0)
									{
										if(Orient2D(b1, c1, c2) >= 0.0)
										{
											return (Orient2D(a1, a2, c1) >= 0.0) ? 1 : 0;
										}
										else
										{
											if(Orient2D(b1, c1, b2) >= 0.0)
											{
												return (Orient2D(c2, c1, b2) >= 0.0) ? 1 : 0;
											}
											else return 0;
										}
									}
									else  return 0;
								}
							};

							auto TestEdge2D = [&](
								const double* a1, const double* b1, const double* c1,
								const double* a2, const double* b2, const double* c2)
							{
								if(Orient2D(c2, a2, b1) >= 0.0)
								{
									if(Orient2D(a1, a2, b1) >= 0.0)
									{
										return (Orient2D(a1, b1, c2) >= 0.0) ? 1 : 0;
									}
									else
									{
										if(Orient2D(b1, c1, a2) >= 0.0)
										{
											return (Orient2D(c1, a1, a2) >= 0.0) ? 1 : 0;
										}
										else return 0;
									}
								}
								else
								{
									if(Orient2D(c2, a2, c1) >= 0.0)
									{
										if(Orient2D(a1, a2, c1) >= 0.0)
										{
											if(Orient2D(a1, c1, c2) >= 0.0) return 1;
											else
											{
												return (Orient2D(b1, c1, c2) >= 0.0) ? 1 : 0;
											}
										}
										else  return 0;
									}
									else return 0;
								}
							};

							auto TestTriangles2D = [&](
								const double* a1, const double* b1, const double* c1,
								const double* a2, const double* b2, const double* c2)
							{
								if(Orient2D(a2, b2, a1) >= 0.0)
								{
									if(Orient2D(b2, c2, a1) >= 0.0)
									{
										if(Orient2D(c2, a2, a1) >= 0.0)
										{
											return 1;
										}
										else
										{
											return TestEdge2D(
												a1, b1, c1,
												a2, b2, c2);
										}
									}
									else
									{
										if(Orient2D(c2, a2, a1) >= 0.0)
										{
											return TestEdge2D(
												a1, b1, c1,
												c2, a2, b2);
										}
										else
										{
											return TestVertex2D(
												a1, b1, c1,
												a2, b2, c2);
										}
									}
								}
								else
								{
									if(Orient2D(b2, c2, a1) >= 0.0)
									{
										if(Orient2D(c2, a2, a1) >= 0.0)
										{
											return TestEdge2D(
												a1, b1, c1,
												b2, c2, a2);
										}
										else
										{
											return TestVertex2D(
												a1, b1, c1,
												b2, c2, a2);
										}
									}
									else
									{
										return TestVertex2D(
											a1, b1, c1,
											c2, a2, b2);
									}
								}
							};

							GW::MATH::GVECTORD normal =
							{
								{
									{
										G_ABS(plane1.x),
										G_ABS(plane1.y),
										G_ABS(plane1.z)
									}
								}
							};

							GW::MATH::GTRIANGLED tri2_1 = {};
							GW::MATH::GTRIANGLED tri2_2 = {};

							if((normal.x > normal.z) && (normal.x >= normal.y))
							{
								// Project onto yz
								tri2_1.a.x = triangle1.b.z;
								tri2_1.a.y = triangle1.b.y;
								tri2_1.b.x = triangle1.a.z;
								tri2_1.b.y = triangle1.a.y;
								tri2_1.c.x = triangle1.c.z;
								tri2_1.c.y = triangle1.c.y;

								tri2_2.a.x = triangle2.b.z;
								tri2_2.a.y = triangle2.b.y;
								tri2_2.b.x = triangle2.a.z;
								tri2_2.b.y = triangle2.a.y;
								tri2_2.c.x = triangle2.c.z;
								tri2_2.c.y = triangle2.c.y;
							}
							else if((normal.y > normal.z) && (normal.y >= normal.x))
							{
								// Project onto xz
								tri2_1.a.x = triangle1.b.x;
								tri2_1.a.y = triangle1.b.z;
								tri2_1.b.x = triangle1.a.x;
								tri2_1.b.y = triangle1.a.z;
								tri2_1.c.x = triangle1.c.x;
								tri2_1.c.y = triangle1.c.z;

								tri2_2.a.x = triangle2.b.x;
								tri2_2.a.y = triangle2.b.z;
								tri2_2.b.x = triangle2.a.x;
								tri2_2.b.y = triangle2.a.z;
								tri2_2.c.x = triangle2.c.x;
								tri2_2.c.y = triangle2.c.z;
							}
							else
							{
								// Project onto xy
								tri2_1.a.x = triangle1.a.x;
								tri2_1.a.y = triangle1.a.y;
								tri2_1.b.x = triangle1.b.x;
								tri2_1.b.y = triangle1.b.y;
								tri2_1.c.x = triangle1.c.x;
								tri2_1.c.y = triangle1.c.y;

								tri2_2.a.x = triangle2.a.x;
								tri2_2.a.y = triangle2.a.y;
								tri2_2.b.x = triangle2.b.x;
								tri2_2.b.y = triangle2.b.y;
								tri2_2.c.x = triangle2.c.x;
								tri2_2.c.y = triangle2.c.y;
							}

							if(Orient2D(tri2_1.a.data, tri2_1.b.data, tri2_1.c.data) < 0.0)
							{
								if(Orient2D(tri2_1.a.data, tri2_1.b.data, tri2_1.c.data) < 0.0)
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data,
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data));
								}
								else
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data,
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data));
								}
							}
							else
							{
								if(Orient2D(tri2_2.a.data, tri2_2.b.data, tri2_2.c.data) < 0.0)
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data,
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data));
								}
								else
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data,
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data));
								}
							}

							return GReturn::SUCCESS;
						}
					}
				}

				if(det2.x > 0.0)
				{
					if(det2.y > 0.0)
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.c,
									triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.c,
									triangle2.a,
									triangle2.b
								}
							}
						};
					}
					else if(det2.z > 0.0)
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.c,
									triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.b,
									triangle2.c,
									triangle2.a
								}
							}
						};
					}
					else
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.b,
									triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.a,
									triangle2.b,
									triangle2.c
								}
							}
						};
					}
				}
				else if(det2.x < 0.0)
				{
					if(det2.y < 0.0)
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.b,
									triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.c,
									triangle2.a,
									triangle2.b
								}
							}
						};
					}
					else if(det2.z < 0.0)
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.b,
									triangle1.c
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.b,
									triangle2.c,
									triangle2.a
								}
							}
						};
					}
					else
					{
						triangle1 =
						{
							{
								{
									triangle1.a,
									triangle1.c,
									triangle1.b
								}
							}
						};

						triangle2 =
						{
							{
								{
									triangle2.a,
									triangle2.b,
									triangle2.c
								}
							}
						};
					}
				}
				else
				{
					if(det2.y < 0.0)
					{
						if(det2.z >= 0.0)
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.c,
										triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.b,
										triangle2.c,
										triangle2.a
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.b,
										triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.a,
										triangle2.b,
										triangle2.c
									}
								}
							};
						}
					}
					else if(det2.y > 0.0)
					{
						if(det2.z > 0.0)
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.c,
										triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.a,
										triangle2.b,
										triangle2.c
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.b,
										triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.b,
										triangle2.c,
										triangle2.a
									}
								}
							};
						}
					}
					else
					{
						if(det2.z > 0.0)
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.b,
										triangle1.c
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.c,
										triangle2.a,
										triangle2.b
									}
								}
							};
						}
						else if(det2.z < 0.0)
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.c,
										triangle1.b
									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.c,
										triangle2.a,
										triangle2.b
									}
								}
							};
						}
						else
						{
							triangle1 =
							{
								{
									{
										triangle1.a,
										triangle1.b,
										triangle1.c

									}
								}
							};

							triangle2 =
							{
								{
									{
										triangle2.a,
										triangle2.b,
										triangle2.c
									}
								}
							};

							// return coplanar result
							auto Orient2D = [](const double* a, const double* b, const double* c)
							{
								return ((a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]));
							};

							auto TestVertex2D = [&](
								const double* a1, const double* b1, const double* c1,
								const double* a2, const double* b2, const double* c2)
							{
								if(Orient2D(c2, a2, b1) >= 0.0)
								{
									if(Orient2D(c2, b2, b1) <= 0.0)
									{
										if(Orient2D(a1, a2, b1) > 0.0)
										{
											return (Orient2D(a1, b2, b1) <= 0.0) ? 1 : 0;
										}
										else
										{
											if(Orient2D(a1, a2, c1) >= 0.0)
											{
												return (Orient2D(b1, c1, a2) >= 0.0) ? 1 : 0;
											}
											else return 0;
										}
									}
									else
									{
										if(Orient2D(a1, b2, b1) <= 0.0)
										{
											if(Orient2D(c2, b2, c1) <= 0.0)
											{
												return (Orient2D(b1, c1, b2) >= 0.0) ? 1 : 0;
											}
											else return 0;
										}
										else return 0;
									}
								}
								else
								{
									if(Orient2D(c2, a2, c1) >= 0.0)
									{
										if(Orient2D(b1, c1, c2) >= 0.0)
										{
											return (Orient2D(a1, a2, c1) >= 0.0) ? 1 : 0;
										}
										else
										{
											if(Orient2D(b1, c1, b2) >= 0.0)
											{
												return (Orient2D(c2, c1, b2) >= 0.0) ? 1 : 0;
											}
											else return 0;
										}
									}
									else  return 0;
								}
							};

							auto TestEdge2D = [&](
								const double* a1, const double* b1, const double* c1,
								const double* a2, const double* b2, const double* c2)
							{
								if(Orient2D(c2, a2, b1) >= 0.0)
								{
									if(Orient2D(a1, a2, b1) >= 0.0)
									{
										return (Orient2D(a1, b1, c2) >= 0.0) ? 1 : 0;
									}
									else
									{
										if(Orient2D(b1, c1, a2) >= 0.0)
										{
											return (Orient2D(c1, a1, a2) >= 0.0) ? 1 : 0;
										}
										else return 0;
									}
								}
								else
								{
									if(Orient2D(c2, a2, c1) >= 0.0)
									{
										if(Orient2D(a1, a2, c1) >= 0.0)
										{
											if(Orient2D(a1, c1, c2) >= 0.0) return 1;
											else
											{
												return (Orient2D(b1, c1, c2) >= 0.0) ? 1 : 0;
											}
										}
										else  return 0;
									}
									else return 0;
								}
							};

							auto TestTriangles2D = [&](
								const double* a1, const double* b1, const double* c1,
								const double* a2, const double* b2, const double* c2)
							{
								if(Orient2D(a2, b2, a1) >= 0.0)
								{
									if(Orient2D(b2, c2, a1) >= 0.0)
									{
										if(Orient2D(c2, a2, a1) >= 0.0)
										{
											return 1;
										}
										else
										{
											return TestEdge2D(
												a1, b1, c1,
												a2, b2, c2);
										}
									}
									else
									{
										if(Orient2D(c2, a2, a1) >= 0.0)
										{
											return TestEdge2D(
												a1, b1, c1,
												c2, a2, b2);
										}
										else
										{
											return TestVertex2D(
												a1, b1, c1,
												a2, b2, c2);
										}
									}
								}
								else
								{
									if(Orient2D(b2, c2, a1) >= 0.0)
									{
										if(Orient2D(c2, a2, a1) >= 0.0)
										{
											return TestEdge2D(
												a1, b1, c1,
												b2, c2, a2);
										}
										else
										{
											return TestVertex2D(
												a1, b1, c1,
												b2, c2, a2);
										}
									}
									else
									{
										return TestVertex2D(
											a1, b1, c1,
											c2, a2, b2);
									}
								}
							};

							GW::MATH::GVECTORD normal =
							{
								{
									{
										G_ABS(plane1.x),
										G_ABS(plane1.y),
										G_ABS(plane1.z)
									}
								}
							};

							GW::MATH::GTRIANGLED tri2_1 = {};
							GW::MATH::GTRIANGLED tri2_2 = {};

							if((normal.x > normal.z) && (normal.x >= normal.y))
							{
								// Project onto yz
								tri2_1.a.x = triangle1.b.z;
								tri2_1.a.y = triangle1.b.y;
								tri2_1.b.x = triangle1.a.z;
								tri2_1.b.y = triangle1.a.y;
								tri2_1.c.x = triangle1.c.z;
								tri2_1.c.y = triangle1.c.y;

								tri2_2.a.x = triangle2.b.z;
								tri2_2.a.y = triangle2.b.y;
								tri2_2.b.x = triangle2.a.z;
								tri2_2.b.y = triangle2.a.y;
								tri2_2.c.x = triangle2.c.z;
								tri2_2.c.y = triangle2.c.y;
							}
							else if((normal.y > normal.z) && (normal.y >= normal.x))
							{
								// Project onto xz
								tri2_1.a.x = triangle1.b.x;
								tri2_1.a.y = triangle1.b.z;
								tri2_1.b.x = triangle1.a.x;
								tri2_1.b.y = triangle1.a.z;
								tri2_1.c.x = triangle1.c.x;
								tri2_1.c.y = triangle1.c.z;

								tri2_2.a.x = triangle2.b.x;
								tri2_2.a.y = triangle2.b.z;
								tri2_2.b.x = triangle2.a.x;
								tri2_2.b.y = triangle2.a.z;
								tri2_2.c.x = triangle2.c.x;
								tri2_2.c.y = triangle2.c.z;
							}
							else
							{
								// Project onto xy
								tri2_1.a.x = triangle1.a.x;
								tri2_1.a.y = triangle1.a.y;
								tri2_1.b.x = triangle1.b.x;
								tri2_1.b.y = triangle1.b.y;
								tri2_1.c.x = triangle1.c.x;
								tri2_1.c.y = triangle1.c.y;

								tri2_2.a.x = triangle2.a.x;
								tri2_2.a.y = triangle2.a.y;
								tri2_2.b.x = triangle2.b.x;
								tri2_2.b.y = triangle2.b.y;
								tri2_2.c.x = triangle2.c.x;
								tri2_2.c.y = triangle2.c.y;
							}

							if(Orient2D(tri2_1.a.data, tri2_1.b.data, tri2_1.c.data) < 0.0)
							{
								if(Orient2D(tri2_1.a.data, tri2_1.b.data, tri2_1.c.data) < 0.0)
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data,
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data));
								}
								else
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data,
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data));
								}
							}
							else
							{
								if(Orient2D(tri2_2.a.data, tri2_2.b.data, tri2_2.c.data) < 0.0)
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data,
										tri2_1.a.data, tri2_1.c.data, tri2_1.b.data));
								}
								else
								{
									_outResult = static_cast<GCollisionCheck>(TestTriangles2D(
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data,
										tri2_1.a.data, tri2_1.b.data, tri2_1.c.data));
								}
							}

							return GReturn::SUCCESS;
						}
					}
				}

				// Edges confirmed to form a line where the triangle planes meet. Check if triangles are within an interval.
				GW::MATH::GVECTORD difference_a2b1 = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle2.a,
					triangle1.b,
					difference_a2b1);

				GW::MATH::GVECTORD difference_a1b1 = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle1.a,
					triangle1.b,
					difference_a1b1);

				GW::MATH::GVector::CrossVector3D(
					difference_a1b1,
					difference_a2b1,
					plane1);

				GW::MATH::GVECTORD difference_b2b1 = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle2.b,
					triangle1.b,
					difference_b2b1);

				double dot1 = 0.0;
				GW::MATH::GVector::DotD(
					difference_b2b1,
					plane1,
					dot1);

				if(dot1 > 0.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::SubtractVectorD(
					triangle2.a,
					triangle1.a,
					difference_a2a1);

				GW::MATH::GVector::SubtractVectorD(
					triangle1.c,
					triangle1.a,
					difference_c1a1);

				GW::MATH::GVector::CrossVector3D(
					difference_c1a1,
					difference_a2a1,
					plane1);

				GW::MATH::GVector::SubtractVectorD(
					triangle2.c,
					triangle1.a,
					difference_c2a1);

				GW::MATH::GVector::DotD(
					difference_c2a1,
					plane1,
					dot1);

				if(dot1 > 0.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestTriangleToPlaneD(const MATH::GTRIANGLED _triangle, const MATH::GPLANED _plane, GCollisionCheck& _outResult)
			{
				GCollisionCheck plane_test;

				TestPointToPlaneD(
					_triangle.a,
					_plane,
					plane_test);
				_outResult = plane_test;

				if (static_cast<int>(_outResult) > 0)
				{
					return GReturn::SUCCESS;
				}

				TestPointToPlaneD(
					_triangle.b,
					_plane,
					plane_test);

				if(_outResult != plane_test)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				TestPointToPlaneD(
					_triangle.c,
					_plane,
					plane_test);

				if(_outResult != plane_test)
				{
					_outResult = GCollisionCheck::COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestTriangleToSphereD(const MATH::GTRIANGLED _triangle, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD closest_point = {};
				ClosestPointToTriangleD(
					_triangle,
					_sphere.data,
					closest_point);

				return TestPointToSphereD(
					closest_point,
					_sphere,
					_outResult);
			}

			static GReturn TestTriangleToCapsuleD(const MATH::GTRIANGLED _triangle, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD closest_point1 = {};
				ClosestPointToTriangleD(
					_triangle,
					_capsule.data[0],
					closest_point1);

				GW::MATH::GVECTORD closest_point2 = {};
				ClosestPointToTriangleD(
					_triangle,
					_capsule.data[1],
					closest_point2);

				return TestLineToCapsuleD(
					{{{closest_point1, closest_point2}}},
					_capsule,
					_outResult);
			}

			static GReturn TestTriangleToAABBD(const MATH::GTRIANGLED _triangle, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD right = {{{1,0,0,0}}};
				GW::MATH::GVECTORD up = {{{0,1,0,0}}};
				GW::MATH::GVECTORD forward = {{{0,0,1,0}}};

				GW::MATH::GVECTORD v0 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle.a.xyz(),
					_aabb.center.xyz(),
					v0);

				GW::MATH::GVECTORD v1 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle.b.xyz(),
					_aabb.center.xyz(),
					v1);

				GW::MATH::GVECTORD v2 = {};
				GW::MATH::GVector::SubtractVectorD(
					_triangle.c.xyz(),
					_aabb.center.xyz(),
					v2);

				GW::MATH::GVECTORD f0 = {};
				GW::MATH::GVector::SubtractVectorD(
					v1,
					v0,
					f0);

				GW::MATH::GVECTORD f1 = {};
				GW::MATH::GVector::SubtractVectorD(
					v2,
					v1,
					f1);

				GW::MATH::GVECTORD f2 = {};
				GW::MATH::GVector::SubtractVectorD(
					v0,
					v2,
					f2);

				GW::MATH::GVECTORD axis[9] = {};

				GW::MATH::GVector::CrossVector3D(
					right,
					f0,
					axis[0]);

				GW::MATH::GVector::CrossVector3D(
					up,
					f0,
					axis[1]);

				GW::MATH::GVector::CrossVector3D(
					forward,
					f0,
					axis[2]);

				GW::MATH::GVector::CrossVector3D(
					right,
					f1,
					axis[3]);

				GW::MATH::GVector::CrossVector3D(
					up,
					f1,
					axis[4]);

				GW::MATH::GVector::CrossVector3D(
					forward,
					f1,
					axis[5]);

				GW::MATH::GVector::CrossVector3D(
					right,
					f2,
					axis[6]);

				GW::MATH::GVector::CrossVector3D(
					up,
					f2,
					axis[7]);

				GW::MATH::GVector::CrossVector3D(
					forward,
					f2,
					axis[8]);

				double p0 = 0.0;
				double p1 = 0.0;
				double p2 = 0.0;
				double r = 0.0;
				GW::MATH::GVECTORD dots = {};

				for(int i = 0; i < 9; i++)
				{
					GW::MATH::GVector::DotD(
						v0,
						axis[i],
						p0);
					GW::MATH::GVector::DotD(
						v1,
						axis[i],
						p1);
					GW::MATH::GVector::DotD(
						v2,
						axis[i],
						p2);

					GW::MATH::GVector::DotD(
						right,
						axis[i],
						dots.x);
					GW::MATH::GVector::DotD(
						up,
						axis[i],
						dots.y);
					GW::MATH::GVector::DotD(
						forward,
						axis[i],
						dots.z);

					r =
						_aabb.extent.x * G_ABS(dots.x) +
						_aabb.extent.y * G_ABS(dots.y) +
						_aabb.extent.z * G_ABS(dots.z);

					if(G_LARGER(-G_LARGER(p0, -G_LARGER(p1, p2)),
								G_SMALLER(p0, G_SMALLER(p1, p2))) > r)
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				// Test the 3 axis corresponding to the face normals of the AABB
				if(G_LARGER(v0.x, G_LARGER(v1.x, v2.x)) < -_aabb.extent.x ||
				   G_SMALLER(v0.x, G_SMALLER(v1.x, v2.x)) > _aabb.extent.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_LARGER(v0.y, G_LARGER(v1.y, v2.y)) < -_aabb.extent.y ||
				   G_SMALLER(v0.y, G_SMALLER(v1.y, v2.y)) > _aabb.extent.y)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_LARGER(v0.z, G_LARGER(v1.z, v2.z)) < -_aabb.extent.z ||
				   G_SMALLER(v0.z, G_SMALLER(v1.z, v2.z)) > _aabb.extent.z)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test separating axis to triangle face normal
				GW::MATH::GPLANED plane;

				ComputePlaneD(
					_triangle.a,
					_triangle.b,
					_triangle.c,
					plane);

				return TestPlaneToAABBD(
					plane,
					_aabb,
					_outResult);
			}

			static GReturn TestTriangleToOBBD(const MATH::GTRIANGLED _triangle, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				MATH::GMATRIXD obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionD(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXD obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0}}},
							{{{0.0, 0.0, 0.0, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GTRIANGLED triangle =
				{
					{
						{
							_triangle.a,
							_triangle.b,
							_triangle.c
						}
					}
				};
				triangle.a.w = 1.0f;
				triangle.b.w = 1.0f;
				triangle.c.w = 1.0f;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					triangle.a,
					triangle.a);
				GW::MATH::GVector::AddVectorD(
					triangle.a,
					_obb.center,
					triangle.a);

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					triangle.b,
					triangle.b);
				GW::MATH::GVector::AddVectorD(
					triangle.b,
					_obb.center,
					triangle.b);

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					triangle.c,
					triangle.c);
				GW::MATH::GVector::AddVectorD(
					triangle.c,
					_obb.center,
					triangle.c);

				return TestTriangleToAABBD(
					triangle,
					{{{_obb.center, _obb.extent}}},
					_outResult);
			}

			static GReturn TestPlaneToPlaneD(const MATH::GPLANED _plane1, const MATH::GPLANED _plane2, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD cross = {};
				GW::MATH::GVector::CrossVector3D(
					_plane1.data.xyz(),
					_plane2.data.xyz(),
					cross);

				double dot = 0.0;
				GW::MATH::GVector::DotD(
					cross,
					cross,
					dot);

				_outResult = (dot < G_COLLISION_THRESHOLD_D) ?
					GCollisionCheck::NO_COLLISION :
					GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestPlaneToSphereD(const MATH::GPLANED _plane, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult)
			{
				double distance = 0.0;
				GW::MATH::GVector::DotD(
					_sphere.data.xyz(),
					_plane.data.xyz(),
					distance);
				distance -= _plane.distance;

				if(G_ABS(distance) <= _sphere.radius)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					_outResult = (distance > 0.0) ?
						GCollisionCheck::ABOVE :
						GCollisionCheck::BELOW;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPlaneToCapsuleD(const MATH::GPLANED _plane, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				double distance_start = 0.0;
				GW::MATH::GVector::DotD(
					_capsule.data[0].xyz(),
					_plane.data.xyz(),
					distance_start);
				distance_start -= _plane.distance;

				double distance_end = 0.0;
				GW::MATH::GVector::DotD(
					_capsule.data[1].xyz(),
					_plane.data.xyz(),
					distance_end);
				distance_end -= _plane.distance;

				if(distance_start * distance_end < 0.0)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(distance_start) <= _capsule.radius || G_ABS(distance_end) <= _capsule.radius)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = (distance_start > 0.0) ?
					GCollisionCheck::ABOVE :
					GCollisionCheck::BELOW;
				return GReturn::SUCCESS;

			}

			static GReturn TestPlaneToAABBD(const MATH::GPLANED _plane, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD plane_normal = _plane.data.xyz();

				double interval =
					_aabb.extent.x * G_ABS(_plane.x) +
					_aabb.extent.y * G_ABS(_plane.y) +
					_aabb.extent.z * G_ABS(_plane.z);

				double distance = 0.0;
				GW::MATH::GVector::DotD(
					plane_normal,
					_aabb.center.xyz(),
					distance);
				distance -= _plane.distance;

				if(G_ABS(distance) <= interval)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					_outResult = (distance > 0.0) ?
						GCollisionCheck::ABOVE :
						GCollisionCheck::BELOW;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestPlaneToOBBD(const MATH::GPLANED _plane, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD plane_normal = _plane.data.xyz();

				double xx2 = 2.0f * _obb.rotation.x * _obb.rotation.x;
				double yy2 = 2.0f * _obb.rotation.y * _obb.rotation.y;
				double zz2 = 2.0f * _obb.rotation.z * _obb.rotation.z;

				double xy2 = 2.0f * _obb.rotation.x * _obb.rotation.y;
				double xz2 = 2.0f * _obb.rotation.x * _obb.rotation.z;
				double yz2 = 2.0f * _obb.rotation.y * _obb.rotation.z;

				double wx2 = 2.0f * _obb.rotation.w * _obb.rotation.x;
				double wy2 = 2.0f * _obb.rotation.w * _obb.rotation.y;
				double wz2 = 2.0f * _obb.rotation.w * _obb.rotation.z;

				MATH::GVECTORD obb_rotation[3] =
				{
					{{{1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2}}},
					{{{xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2}}},
					{{{xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2}}}
				};

				double interval = 0.0;
				double s = 0.0;

				for(int i = 0; i < 3; i++)
				{
					GW::MATH::GVector::DotD(
						plane_normal,
						obb_rotation[i],
						s);

					interval += _obb.extent.data[i] * G_ABS(s);
				}

				GW::MATH::GVector::DotD(
					plane_normal,
					_obb.center.xyz(),
					s);

				s -= _plane.distance;

				if(G_ABS(s) <= interval)
				{
					_outResult = GCollisionCheck::COLLISION;
				}
				else
				{
					_outResult = (s > 0.0) ?
						GCollisionCheck::ABOVE :
						GCollisionCheck::BELOW;
				}

				return GReturn::SUCCESS;
			}

			static GReturn TestSphereToSphereD(const MATH::GSPHERED _sphere1, const MATH::GSPHERED _sphere2, GCollisionCheck& _outResult)
			{
				MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_sphere1.data.xyz(),
					_sphere2.data.xyz(),
					difference_ba);

				double sq_distance = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_ba,
					sq_distance);

				double sq_radii = _sphere1.radius + _sphere2.radius;
				sq_radii *= sq_radii;

				_outResult = (sq_distance <= sq_radii) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestSphereToCapsuleD(const MATH::GSPHERED _sphere, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult)
			{
				// Compute squared distance between the sphere center and capsule's start and end.
				double sq_distance = 0.0;
				SqDistancePointToLineD(
					_sphere.data,
					{{{_capsule.data[0], _capsule.data[1]}}},
					sq_distance);

				double sq_radii = _sphere.radius + _capsule.radius;
				sq_radii *= sq_radii;

				_outResult = (sq_distance <= sq_radii) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestSphereToAABBD(const MATH::GSPHERED _sphere, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				MATH::GAABBMMD mm = {};
				ConvertAABBCEToAABBMMD(
					_aabb,
					mm);

				double sq_distance = 0.0;
				SqDistancePointToAABBD(
					_sphere.data.xyz(),
					mm,
					sq_distance);

				double sq_radius = _sphere.radius * _sphere.radius;

				_outResult = (sq_distance <= sq_radius) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestSphereToOBBD(const MATH::GSPHERED _sphere, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				GW::MATH::GVECTORD closest_point = {};
				ClosestPointToOBBD(
					_obb,
					_sphere.data.xyz(),
					closest_point);

				GW::MATH::GVECTORD difference = {};
				GW::MATH::GVector::SubtractVectorD(
					closest_point,
					_sphere.data.xyz(),
					difference);

				double sq_radius = _sphere.radius * _sphere.radius;

				double sq_distance = 0.0;
				GW::MATH::GVector::DotD(
					difference,
					difference,
					sq_distance);

				_outResult = (sq_distance <= sq_radius) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestCapsuleToCapsuleD(const MATH::GCAPSULED _capsule1, const MATH::GCAPSULED _capsule2, GCollisionCheck& _outResult)
			{
				MATH::GVECTORD closest1 = {};
				MATH::GVECTORD closest2 = {};

				ClosestPointsToLineFromLineD(
					{{{_capsule1.data[0], _capsule1.data[1]}}},
					{{{_capsule2.data[0], _capsule2.data[1]}}},
					closest1,
					closest2);

				MATH::GVECTORD difference_ab = {};
				GW::MATH::GVector::SubtractVectorD(
					closest1,
					closest2,
					difference_ab);

				double sq_distance = 0.0;
				GW::MATH::GVector::DotD(
					difference_ab,
					difference_ab,
					sq_distance);

				double sq_radii = _capsule1.radius + _capsule2.radius;
				sq_radii *= sq_radii;

				_outResult = (sq_distance <= sq_radii) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn TestCapsuleToAABBD(const MATH::GCAPSULED _capsule, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult)
			{
				// Expand AABB extents by capsule's radius.
				GW::MATH::GVECTORD center = _aabb.center.xyz();
				GW::MATH::GVECTORD extent = _aabb.extent.xyz();
				extent.x += _capsule.radius;
				extent.y += _capsule.radius;
				extent.z += _capsule.radius;

				GW::MATH::GVECTORD line_midpoint = {};
				GW::MATH::GVector::AddVectorD(
					_capsule.data[0].xyz(),
					_capsule.data[1].xyz(),
					line_midpoint);

				GW::MATH::GVector::ScaleD(
					line_midpoint,
					0.5f,
					line_midpoint);

				GW::MATH::GVECTORD line_mid_length = {};
				GW::MATH::GVector::SubtractVectorD(
					_capsule.data[1].xyz(),
					line_midpoint,
					line_mid_length);

				// Translate box and line to origin
				GW::MATH::GVECTORD o = {};
				GW::MATH::GVector::SubtractVectorD(
					line_midpoint,
					center,
					o);

				GW::MATH::GVECTORD axis =
				{
					{
						{
							G_ABS(line_mid_length.x),
							G_ABS(line_mid_length.y),
							G_ABS(line_mid_length.z)
						}
					}
				};

				// Separating axes
				for(int i = 0; i < 3; i++)
				{
					if(G_ABS(o.data[i]) > extent.data[i] + axis.data[i])
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				axis.x += G_COLLISION_THRESHOLD_D;
				axis.y += G_COLLISION_THRESHOLD_D;
				axis.z += G_COLLISION_THRESHOLD_D;

				if(G_ABS(o.y * line_mid_length.z - o.z * line_mid_length.y) >
				   extent.y* axis.z + extent.z * axis.y)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(o.z * line_mid_length.x - o.x * line_mid_length.z) >
				   extent.x* axis.z + extent.z * axis.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(G_ABS(o.x * line_mid_length.z - o.y * line_mid_length.x) >
				   extent.x* axis.y + extent.z * axis.x)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestCapsuleToOBBD(const MATH::GCAPSULED _capsule, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				MATH::GMATRIXD obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionD(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXD obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0}}},
							{{{0.0, 0.0, 0.0, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GCAPSULED capsule = {};
				capsule.data[0] = _capsule.data[0];
				capsule.data[0].w = 1.0f;
				capsule.data[1] = _capsule.data[1];
				capsule.data[1].w = 1.0f;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					capsule.data[0],
					capsule.data[0]);
				GW::MATH::GVector::AddVectorD(
					capsule.data[0],
					_obb.center,
					capsule.data[0]);

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					capsule.data[1],
					capsule.data[1]);
				GW::MATH::GVector::AddVectorD(
					capsule.data[1],
					_obb.center,
					capsule.data[1]);

				capsule.radius = _capsule.radius;

				return TestCapsuleToAABBD(
					capsule,
					{{{_obb.center, _obb.extent}}},
					_outResult);
			}

			static GReturn TestAABBToAABBD(const MATH::GAABBCED _aabb1, const MATH::GAABBCED _aabb2, GCollisionCheck& _outResult)
			{
				if(G_ABS(_aabb1.center.x - _aabb2.center.x) > (_aabb1.extent.x + _aabb2.extent.x) ||
				   G_ABS(_aabb1.center.y - _aabb2.center.y) > (_aabb1.extent.y + _aabb2.extent.y) ||
				   G_ABS(_aabb1.center.z - _aabb2.center.z) > (_aabb1.extent.z + _aabb2.extent.z))
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn TestAABBToOBBD(const MATH::GAABBCED _aabb, const MATH::GOBBD _obb, GCollisionCheck& _outResult)
			{
				MATH::GOBBD obb =
				{
					{
						{
							_aabb.center,
							_aabb.extent,
							{{{0.0, 0.0, 0.0, 1.0f}}}
						}
					}
				};

				return TestOBBToOBBD(
					obb,
					_obb,
					_outResult);
			}

			static GReturn TestOBBToOBBD(const MATH::GOBBD _obb1, const MATH::GOBBD _obb2, GCollisionCheck& _outResult)
			{
				// Convert quaternions into 3 axes for both OBB1 and OBB2.
				double xx2 = 2.0f * _obb1.rotation.x * _obb1.rotation.x;
				double yy2 = 2.0f * _obb1.rotation.y * _obb1.rotation.y;
				double zz2 = 2.0f * _obb1.rotation.z * _obb1.rotation.z;

				double xy2 = 2.0f * _obb1.rotation.x * _obb1.rotation.y;
				double xz2 = 2.0f * _obb1.rotation.x * _obb1.rotation.z;
				double yz2 = 2.0f * _obb1.rotation.y * _obb1.rotation.z;

				double wx2 = 2.0f * _obb1.rotation.w * _obb1.rotation.x;
				double wy2 = 2.0f * _obb1.rotation.w * _obb1.rotation.y;
				double wz2 = 2.0f * _obb1.rotation.w * _obb1.rotation.z;

				MATH::GVECTORD obb1_rotation[3] =
				{
					{{{1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2}}},
					{{{xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2}}},
					{{{xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2}}}
				};

				xx2 = 2.0f * _obb2.rotation.x * _obb2.rotation.x;
				yy2 = 2.0f * _obb2.rotation.y * _obb2.rotation.y;
				zz2 = 2.0f * _obb2.rotation.z * _obb2.rotation.z;

				xy2 = 2.0f * _obb2.rotation.x * _obb2.rotation.y;
				xz2 = 2.0f * _obb2.rotation.x * _obb2.rotation.z;
				yz2 = 2.0f * _obb2.rotation.y * _obb2.rotation.z;

				wx2 = 2.0f * _obb2.rotation.w * _obb2.rotation.x;
				wy2 = 2.0f * _obb2.rotation.w * _obb2.rotation.y;
				wz2 = 2.0f * _obb2.rotation.w * _obb2.rotation.z;

				MATH::GVECTORD obb2_rotation[3] =
				{
					{{{1.0f - yy2 - zz2, xy2 - wz2, xz2 + wy2}}},
					{{{xy2 + wz2, 1.0f - xx2 - zz2, yz2 - wx2}}},
					{{{xz2 - wy2, yz2 + wx2, 1.0f - xx2 - yy2}}}
				};

				double projected_radii = 0.0;
				double projected_radii_1 = 0.0;
				double projected_radii_2 = 0.0;
				double test_axis = 0.0;

				double rotation[3][3] = {};
				double abs_rotation[3][3] = {};

				for(int i = 0; i < 3; i++)
				{
					for(int j = 0; j < 3; j++)
					{
						GW::MATH::GVector::DotD(
							obb1_rotation[i],
							obb2_rotation[j],
							rotation[i][j]);
					}
				}

				// Get translation and then bring it into a's coordinate frame.
				MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_obb2.center,
					_obb1.center,
					difference_ba);

				MATH::GVECTORD translation_vec = {};
				GW::MATH::GVector::DotD(
					difference_ba,
					obb1_rotation[0],
					translation_vec.x);

				GW::MATH::GVector::DotD(
					difference_ba,
					obb1_rotation[1],
					translation_vec.y);

				GW::MATH::GVector::DotD(
					difference_ba,
					obb1_rotation[2],
					translation_vec.z);

				// Get common values while maintaining numerical precision.
				for(int i = 0; i < 3; i++)
				{
					for(int j = 0; j < 3; j++)
					{
						abs_rotation[i][j] =
							G_ABS(rotation[i][j]) + G_COLLISION_THRESHOLD_D;
					}
				}

				// Test axes for OBB1 x, y, z.
				for(int i = 0; i < 3; i++)
				{
					projected_radii_1 = _obb1.extent.data[i];

					projected_radii_2 =
						_obb2.extent.data[0] * abs_rotation[i][0] +
						_obb2.extent.data[1] * abs_rotation[i][1] +
						_obb2.extent.data[2] * abs_rotation[i][2];

					projected_radii = projected_radii_1 + projected_radii_2;

					test_axis = G_ABS(translation_vec.data[i]);

					if(test_axis > projected_radii)
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				// Test axes for OBB2 x, y, z.
				for(int i = 0; i < 3; i++)
				{
					projected_radii_1 =
						_obb1.extent.data[0] * abs_rotation[0][i] +
						_obb1.extent.data[1] * abs_rotation[1][i] +
						_obb1.extent.data[2] * abs_rotation[2][i];

					projected_radii_2 = _obb2.extent.data[i];

					projected_radii = projected_radii_1 + projected_radii_2;

					test_axis = G_ABS(
						translation_vec.data[0] * rotation[0][i] +
						translation_vec.data[1] * rotation[1][i] +
						translation_vec.data[2] * rotation[2][i]);

					if(test_axis > projected_radii)
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}
				}

				// Test OBB1 x & OBB2 x
				projected_radii_1 =
					_obb1.extent.data[1] * abs_rotation[2][0] +
					_obb1.extent.data[2] * abs_rotation[1][0];

				projected_radii_2 =
					_obb2.extent.data[1] * abs_rotation[0][2] +
					_obb2.extent.data[2] * abs_rotation[0][1];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[2] * rotation[1][0] -
					translation_vec.data[1] * rotation[2][0]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 x & OBB2 y
				projected_radii_1 =
					_obb1.extent.data[1] * abs_rotation[2][1] +
					_obb1.extent.data[2] * abs_rotation[1][1];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[0][2] +
					_obb2.extent.data[2] * abs_rotation[0][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[2] * rotation[1][1] -
					translation_vec.data[1] * rotation[2][1]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 x & OBB2 z
				projected_radii_1 =
					_obb1.extent.data[1] * abs_rotation[2][2] +
					_obb1.extent.data[2] * abs_rotation[1][2];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[0][1] +
					_obb2.extent.data[1] * abs_rotation[0][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[2] * rotation[1][2] -
					translation_vec.data[1] * rotation[2][2]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 y & OBB2 x
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[2][0] +
					_obb1.extent.data[2] * abs_rotation[0][0];

				projected_radii_2 =
					_obb2.extent.data[1] * abs_rotation[1][2] +
					_obb2.extent.data[2] * abs_rotation[1][1];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[0] * rotation[2][0] -
					translation_vec.data[2] * rotation[0][0]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 y & OBB2 y
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[2][1] +
					_obb1.extent.data[2] * abs_rotation[0][1];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[1][2] +
					_obb2.extent.data[2] * abs_rotation[1][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[0] * rotation[2][1] -
					translation_vec.data[2] * rotation[0][1]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 y & OBB2 z
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[2][2] +
					_obb1.extent.data[2] * abs_rotation[0][2];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[1][1] +
					_obb2.extent.data[1] * abs_rotation[1][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[0] * rotation[2][2] -
					translation_vec.data[2] * rotation[0][2]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 z & OBB2 x
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[1][0] +
					_obb1.extent.data[1] * abs_rotation[0][0];

				projected_radii_2 =
					_obb2.extent.data[1] * abs_rotation[2][2] +
					_obb2.extent.data[2] * abs_rotation[2][1];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[1] * rotation[0][0] -
					translation_vec.data[0] * rotation[1][0]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 z & OBB2 y
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[1][1] +
					_obb1.extent.data[1] * abs_rotation[0][1];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[2][2] +
					_obb2.extent.data[2] * abs_rotation[2][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[1] * rotation[0][1] -
					translation_vec.data[0] * rotation[1][1]);

				if(test_axis > projected_radii)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Test OBB1 z & OBB2 z
				projected_radii_1 =
					_obb1.extent.data[0] * abs_rotation[1][2] +
					_obb1.extent.data[1] * abs_rotation[0][2];

				projected_radii_2 =
					_obb2.extent.data[0] * abs_rotation[2][1] +
					_obb2.extent.data[1] * abs_rotation[2][0];

				projected_radii = projected_radii_1 + projected_radii_2;

				test_axis = G_ABS(
					translation_vec.data[1] * rotation[0][2] -
					translation_vec.data[0] * rotation[1][2]);

				_outResult = test_axis > projected_radii ?
					GCollisionCheck::NO_COLLISION :
					GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToTriangleD(const MATH::GLINED _line, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD* _outBarycentric, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				MATH::GLINED line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				MATH::GTRIANGLED triangle =
				{
					{
						{
							_triangle.a.xyz(),
							_triangle.b.xyz(),
							_triangle.c.xyz()
						}
					}
				};

				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle.b,
					triangle.a,
					difference_ba);

				GW::MATH::GVECTORD difference_ca = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle.c,
					triangle.a,
					difference_ca);

				GW::MATH::GVECTORD norm = {};
				GW::MATH::GVector::CrossVector3D(
					difference_ca,
					difference_ba,
					norm);

				GW::MATH::GVECTORD difference_line = {};
				GW::MATH::GVector::SubtractVectorD(
					line.start,
					line.end,
					difference_line);

				double dot = 0.0;
				GW::MATH::GVector::DotD(
					difference_line,
					norm,
					dot);

				// Segment is parallel to triangle
				if(dot <= 0.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORD difference_pa = {};
				GW::MATH::GVector::SubtractVectorD(
					line.start,
					triangle.a,
					difference_pa);

				double interval = 0.0;
				GW::MATH::GVector::DotD(
					difference_pa,
					norm,
					interval);

				if(interval < 0.0 || interval > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORD cross = {};
				GW::MATH::GVector::CrossVector3D(
					difference_line,
					difference_pa,
					cross);

				// Test to see if within bounds of barycentric coordinates.
				GW::MATH::GVECTORD barycentric = {};
				GW::MATH::GVector::DotD(
					difference_ca,
					cross,
					barycentric.y);
				barycentric.y = -barycentric.y;

				if(barycentric.y < 0.0 || barycentric.y > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::DotD(
					difference_ba,
					cross,
					barycentric.z);

				if(barycentric.z < 0.0 || barycentric.y + barycentric.z > dot)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				double over_denom = 1.0f / dot;
				barycentric.y *= over_denom;
				barycentric.z *= over_denom;
				barycentric.x = 1.0f - barycentric.y - barycentric.z;
				_outInterval = interval * over_denom;

				_outContactPoint =
				{
					{
						{
							barycentric.x * _triangle.a.x + barycentric.y * _triangle.b.x + barycentric.z * _triangle.c.x,
							barycentric.x * _triangle.a.y + barycentric.y * _triangle.b.y + barycentric.z * _triangle.c.y,
							barycentric.x * _triangle.a.z + barycentric.y * _triangle.b.z + barycentric.z * _triangle.c.z
						}
					}
				};

				if(_outBarycentric)
				{
					*_outBarycentric = barycentric;
				}

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToPlaneD(const MATH::GLINED _line, const MATH::GPLANED _plane, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_line.end.xyz(),
					_line.start.xyz(),
					difference_ba);

				double denom = 0.0;
				GW::MATH::GVector::DotD(
					_plane.data.xyz(),
					difference_ba,
					denom);

				if(G_ABS(denom) < G_COLLISION_THRESHOLD_D)
				{
					// Coplanar
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				double num = 0.0;
				GW::MATH::GVector::DotD(
					_plane.data.xyz(),
					_line.start.xyz(),
					num);

				double interval = (_plane.distance - num) / denom;

				if(interval >= 0.0 && interval <= 1.0f)
				{
					_outResult = GCollisionCheck::COLLISION;

					_outDirection = difference_ba;
					GW::MATH::GVector::NormalizeD(
						_outDirection,
						_outDirection);

					GW::MATH::GVector::ScaleD(
						difference_ba,
						interval,
						_outContactPoint);

					GW::MATH::GVector::AddVectorD(
						_line.start.xyz(),
						_outContactPoint,
						_outContactPoint);

					_outInterval = interval;
				}
				else
				{
					num -= _plane.distance;

					_outResult = (num < 0.0) ?
						_outResult = GCollisionCheck::BELOW :
						_outResult = GCollisionCheck::ABOVE;
				}

				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToSphereD(const MATH::GLINED _line, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				MATH::GLINED line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					line.start,
					_sphere.data.xyz(),
					difference_ba);

				GW::MATH::GVECTORD dir = {};
				GW::MATH::GVector::SubtractVectorD(
					line.end,
					line.start,
					dir);

				double sq_length = 0.0f;
				GW::MATH::GVector::DotD(
					dir,
					dir,
					sq_length);

				if(GW::MATH::GVector::NormalizeD(dir, dir) == GReturn::FAILURE)
				{
					return GReturn::FAILURE;
				}

				double b = 0.0f;
				GW::MATH::GVector::DotD(
					difference_ba,
					dir,
					b);

				double c = 0.0f;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_ba,
					c);
				c -= _sphere.radius * _sphere.radius;

				if(c > 0.0f && b > 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				double discriminant = b * b - c;

				if(discriminant < 0.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outDirection = dir;
				_outInterval = -b - sqrt(discriminant);

				if(_outInterval <= 0.0f)
				{
					// Line is contained in sphere.
					// Clamp to line start.
					_outInterval = 0.0f;
					_outContactPoint = line.start;
				}
				else if(_outInterval * _outInterval >= sq_length)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}
				else
				{
					GW::MATH::GVector::ScaleD(
						_outDirection,
						_outInterval,
						_outContactPoint);

					GW::MATH::GVector::AddVectorD(
						_outContactPoint,
						line.start,
						_outContactPoint);
				}

				_outResult = GCollisionCheck::COLLISION;

				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToCapsuleD(const MATH::GLINED _line, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				GW::MATH::GVECTORD closest_point1 = {};
				GW::MATH::GVECTORD closest_point2 = {};
				ClosestPointsToLineFromLineD(
					_line,
					{{{_capsule.data[0], _capsule.data[1]}}},
					closest_point1,
					closest_point2);

				MATH::GSPHERED sphere = {};
				sphere.data = closest_point2;
				sphere.radius = _capsule.radius;

				return IntersectLineToSphereD(
					_line,
					sphere,
					_outResult,
					_outContactPoint,
					_outDirection,
					_outInterval);
			}

			static GReturn IntersectLineToAABBD(const MATH::GLINED _line, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				GW::MATH::GVector::SubtractVectorD(
					_line.end.xyz(),
					_line.start.xyz(),
					_outDirection);

				if(GW::MATH::GVector::NormalizeD(_outDirection, _outDirection) == GReturn::FAILURE)
				{
					return GReturn::FAILURE;
				}

								// TODO div by 0
				double interval1 = (_aabb.min.x - _line.start.x) / _outDirection.x;
				double interval2 = (_aabb.max.x - _line.start.x) / _outDirection.x;

				double interval3 = (_aabb.min.y - _line.start.y) / _outDirection.y;
				double interval4 = (_aabb.max.y - _line.start.y) / _outDirection.y;

				double interval5 = (_aabb.min.z - _line.start.z) / _outDirection.z;
				double interval6 = (_aabb.max.z - _line.start.z) / _outDirection.z;

				double interval_min = G_LARGER(G_LARGER(
					G_SMALLER(interval1, interval2),
					G_SMALLER(interval3, interval4)),
					G_SMALLER(interval5, interval6));

				double interval_max = G_SMALLER(G_SMALLER(
					G_LARGER(interval1, interval2),
					G_LARGER(interval3, interval4)),
					G_LARGER(interval5, interval6));

				if(interval_max < 0.0 || interval_min > 1.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(interval_max < interval_min)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outInterval = (interval_min < 0.0) ?
					interval_max : interval_min;

				GW::MATH::GVector::ScaleD(
					_outDirection,
					_outInterval,
					_outContactPoint);

				GW::MATH::GVector::AddVectorD(
					_outContactPoint,
					_line.start.xyz(),
					_outContactPoint);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectLineToOBBD(const MATH::GLINED _line, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD& _outDirection, double& _outInterval)
			{
				MATH::GLINED line = {};
				line.start = _line.start;
				line.start.w = 1.0f;
				line.end = _line.end;
				line.end.w = 1.0f;

				MATH::GMATRIXD obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionD(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXD obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0}}},
							{{{0.0, 0.0, 0.0, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					line.start,
					line.start);
				GW::MATH::GVector::AddVectorD(
					line.start,
					_obb.center,
					line.start);

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					line.end,
					line.end);
				GW::MATH::GVector::AddVectorD(
					line.end,
					_obb.center,
					line.end);

				MATH::GAABBMMD mm = {};
				ConvertAABBCEToAABBMMD(
					{{{_obb.center, _obb.extent}}},
					mm);

				if(IntersectLineToAABBD(
					line,
					mm,
					_outResult,
					_outContactPoint,
					_outDirection,
					_outInterval) == GReturn::FAILURE)
				{
					return GReturn::FAILURE;
				}

				GMatrixImplementation::InverseD(
					obb_rotation_inverse,
					obb_rotation);

				GW::MATH::GVector::SubtractVectorD(
					_outContactPoint,
					_obb.center,
					_outContactPoint);

				_outContactPoint.w = 1.0F;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation,
					_outContactPoint,
					_outContactPoint);

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation,
					_outDirection.xyz(),
					_outDirection);

				_outContactPoint.w = 0.0F;

				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToTriangleD(const MATH::GRAYD _ray, const MATH::GTRIANGLED _triangle, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, MATH::GVECTORD* _outBarycentric, double& _outInterval)
			{
				MATH::GRAYD ray =
				{
					{
						{
							_ray.position.xyz(),
							_ray.direction.xyz()
						}
					}
				};

				MATH::GTRIANGLED triangle =
				{
					{
						{
							_triangle.a.xyz(),
							_triangle.b.xyz(),
							_triangle.c.xyz()
						}
					}
				};

				GW::MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle.b,
					triangle.a,
					difference_ba);

				GW::MATH::GVECTORD difference_ca = {};
				GW::MATH::GVector::SubtractVectorD(
					triangle.c,
					triangle.a,
					difference_ca);

				GW::MATH::GVECTORD q = {};
				GW::MATH::GVector::CrossVector3D(
					ray.direction,
					difference_ca,
					q);

				double det = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					q,
					det);

				// Ray is parallel to or points away from triangle
				if(G_ABS(det) < G_COLLISION_THRESHOLD_D)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				double denom = 1.0 / det;

				GW::MATH::GVECTORD s = {};
				GW::MATH::GVector::SubtractVectorD(
					ray.position,
					triangle.a,
					s);

				GW::MATH::GVECTORD barycentric = {};
				GW::MATH::GVector::DotD(
					s,
					q,
					barycentric.y);
				barycentric.y *= denom;

				if(barycentric.y < -G_COLLISION_THRESHOLD_D)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				GW::MATH::GVECTORD r = {};
				GW::MATH::GVector::CrossVector3D(
					s,
					difference_ba,
					r);

				GW::MATH::GVector::DotD(
					ray.direction,
					r,
					barycentric.z);
				barycentric.z *= denom;

				if(barycentric.z < -G_COLLISION_THRESHOLD_D || barycentric.y + barycentric.z > 1.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				barycentric.x = 1.0 - barycentric.y - barycentric.z;

				if(_outBarycentric)
				{
					*_outBarycentric = barycentric;
				}

				_outContactPoint =
				{
					{
						{
							barycentric.x * _triangle.a.x + barycentric.y * _triangle.b.x + barycentric.z * _triangle.c.x,
							barycentric.x * _triangle.a.y + barycentric.y * _triangle.b.y + barycentric.z * _triangle.c.y,
							barycentric.x * _triangle.a.z + barycentric.y * _triangle.b.z + barycentric.z * _triangle.c.z
						}
					}
				};

				GW::MATH::GVECTORD v = {};
				GW::MATH::GVector::SubtractVectorD(
					_outContactPoint,
					ray.position,
					v);
				GW::MATH::GVector::DotD(
					v,
					v,
					_outInterval);

				_outInterval = sqrt(_outInterval);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToPlaneD(const MATH::GRAYD _ray, const MATH::GPLANED _plane, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				double denom = 0.0f;
				GW::MATH::GVector::DotD(
					_plane.data.xyz(),
					_ray.direction.xyz(),
					denom);

				if(G_ABS(denom) < G_COLLISION_THRESHOLD_D)
				{
					// Coplanar
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				double num = 0.0f;
				GW::MATH::GVector::DotD(
					_plane.data.xyz(),
					_ray.position.xyz(),
					num);

				double interval = (_plane.distance - num) / denom;

				if(interval >= 0.0f)
				{
					_outResult = GCollisionCheck::COLLISION;

					GW::MATH::GVector::ScaleD(
						_ray.direction.xyz(),
						interval,
						_outContactPoint);

					GW::MATH::GVector::AddVectorD(
						_ray.position.xyz(),
						_outContactPoint,
						_outContactPoint);

					_outInterval = interval;
				}
				else
				{
					_outResult = GCollisionCheck::NO_COLLISION;
				}

				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToSphereD(const MATH::GRAYD _ray, const MATH::GSPHERED _sphere, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_ray.position.xyz(),
					_sphere.data.xyz(),
					difference_ba);

				double b = 0.0;
				MATH::GVECTORD direction_n = _ray.direction.xyz();
				GW::MATH::GVector::DotD(
					difference_ba,
					direction_n,
					b);

				double c = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_ba,
					c);
				c = c - _sphere.radius * _sphere.radius;

				if(c > 0.0 && b > 0.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				double discriminant = b * b - c;

				if(discriminant < 0.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outInterval = -b - sqrt(discriminant);

				// If t is negative, ray started inside sphere so clamp t to zero
				if(_outInterval < 0.0)
				{
					_outInterval = 0.0;
				}

				GW::MATH::GVector::ScaleD(
					direction_n,
					_outInterval,
					_outContactPoint);

				GW::MATH::GVector::AddVectorD(
					_outContactPoint,
					_ray.position.xyz(),
					_outContactPoint);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToCapsuleD(const MATH::GRAYD _ray, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				GW::MATH::GVECTORD closest_point1 = {};
				ClosestPointToRayD(
					_ray,
					_capsule.data[0],
					closest_point1);

				GW::MATH::GSPHERED sphere = {};
				ClosestPointToRayD(
					_ray,
					_capsule.data[1],
					sphere.data);

				ClosestPointsToLineFromLineD(
					{{{closest_point1, sphere.data}}},
					{{{_capsule.data[0], _capsule.data[1]}}},
					closest_point1,
					sphere.data);

				sphere.radius = _capsule.radius;

				return IntersectRayToSphereD(
					_ray,
					sphere,
					_outResult,
					_outContactPoint,
					_outInterval);
			}

			static GReturn IntersectRayToAABBD(const MATH::GRAYD _ray, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				double interval1 = 0.0;
				double interval2 = 0.0;
				double interval3 = 0.0;
				double interval4 = 0.0;
				double interval5 = 0.0;
				double interval6 = 0.0;

				if(_ray.direction.x)
				{
					interval1 = (_aabb.min.x - _ray.position.x) / _ray.direction.x;
					interval2 = (_aabb.max.x - _ray.position.x) / _ray.direction.x;
				}
				else
				{
					interval1 = (_aabb.min.x - _ray.position.x) > 0.0 ?
						DBL_MAX : DBL_MIN;
					interval2 = (_aabb.max.x - _ray.position.x) > 0.0 ?
						DBL_MAX : DBL_MIN;
				}

				if(_ray.direction.y)
				{
					interval3 = (_aabb.min.y - _ray.position.y) / _ray.direction.y;
					interval4 = (_aabb.max.y - _ray.position.y) / _ray.direction.y;
				}
				else
				{
					interval3 = (_aabb.min.y - _ray.position.y) > 0.0 ?
						DBL_MAX : DBL_MIN;
					interval4 = (_aabb.max.y - _ray.position.y) > 0.0 ?
						DBL_MAX : DBL_MIN;
				}

				if(_ray.direction.z)
				{
					interval5 = (_aabb.min.z - _ray.position.z) / _ray.direction.z;
					interval6 = (_aabb.max.z - _ray.position.z) / _ray.direction.z;
				}
				else
				{
					interval5 = (_aabb.min.z - _ray.position.z) > 0.0 ?
						DBL_MAX : DBL_MIN;
					interval6 = (_aabb.max.z - _ray.position.z) > 0.0 ?
						DBL_MAX : DBL_MIN;
				}

				double interval_min = G_LARGER(G_LARGER(
					G_SMALLER(interval1, interval2),
					G_SMALLER(interval3, interval4)),
					G_SMALLER(interval5, interval6));

				double interval_max = G_SMALLER(G_SMALLER(
					G_LARGER(interval1, interval2),
					G_LARGER(interval3, interval4)),
					G_LARGER(interval5, interval6));

				if(interval_max < 0.0)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				if(interval_max < interval_min)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				_outInterval = (interval_min < 0.0) ?
					interval_max : interval_min;

				GW::MATH::GVector::ScaleD(
					_ray.direction.xyz(),
					_outInterval,
					_outContactPoint);

				GW::MATH::GVector::AddVectorD(
					_outContactPoint,
					_ray.position.xyz(),
					_outContactPoint);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			static GReturn IntersectRayToOBBD(const MATH::GRAYD _ray, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactPoint, double& _outInterval)
			{
				MATH::GMATRIXD obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionD(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXD obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0}}},
							{{{0.0, 0.0, 0.0, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GRAYD ray = {};
				ray.position = _ray.position;
				ray.position.w = 1.0f;
				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					ray.position,
					ray.position);
				GW::MATH::GVector::AddVectorD(
					ray.position,
					_obb.center,
					ray.position);

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					_ray.direction,
					ray.direction);
				GW::MATH::GVector::NormalizeD(
					ray.direction,
					ray.direction);

				MATH::GAABBMMD mm = {};
				ConvertAABBCEToAABBMMD(
					{{{_obb.center, _obb.extent}}},
					mm);

				IntersectRayToAABBD(
					ray,
					mm,
					_outResult,
					_outContactPoint,
					_outInterval);

				GMatrixImplementation::InverseD(
					obb_rotation_inverse,
					obb_rotation);

				GW::MATH::GVector::SubtractVectorD(
					_outContactPoint,
					_obb.center,
					_outContactPoint);

				_outContactPoint.w = 1.0F;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation,
					_outContactPoint,
					_outContactPoint);

				_outContactPoint.w = 0.0F;

				return GReturn::SUCCESS;
			}

			static GReturn IntersectSphereToSphereD(const MATH::GSPHERED _sphere1, const MATH::GSPHERED _sphere2, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				TestSphereToSphereD(
					_sphere1,
					_sphere2,
					_outResult);

				if (static_cast<int>(_outResult) < 1)
				{
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::SubtractVectorD(
					_sphere2.data.xyz(),
					_sphere1.data.xyz(),
					_outDirection);

				GW::MATH::GVector::NormalizeD(
					_outDirection,
					_outDirection);

				GW::MATH::GVector::ScaleD(
					_outDirection,
					_sphere1.radius,
					_outContactClosest1);

				GW::MATH::GVector::AddVectorD(
					_sphere1.data.xyz(),
					_outContactClosest1,
					_outContactClosest1);

				GW::MATH::GVector::ScaleD(
					_outDirection,
					-_sphere2.radius,
					_outContactClosest2);

				GW::MATH::GVector::AddVectorD(
					_sphere2.data.xyz(),
					_outContactClosest2,
					_outContactClosest2);

				GW::MATH::GVECTORD difference = {};

				GW::MATH::GVector::SubtractVectorD(
					_outContactClosest1,
					_outContactClosest2,
					difference);

				GW::MATH::GVector::DotD(
					difference,
					difference,
					_outDistance);

				_outDistance = sqrt(_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn IntersectSphereToCapsuleD(const MATH::GSPHERED _sphere, const MATH::GCAPSULED _capsule, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				MATH::GSPHERED sphere = {};

				ClosestPointToLineD(
					{{{_capsule.data[0], _capsule.data[1]}}},
					_sphere.data,
					sphere.data);

				sphere.radius = _capsule.radius;

				return IntersectSphereToSphereD(
					_sphere,
					sphere,
					_outResult,
					_outContactClosest1,
					_outContactClosest2,
					_outDirection,
					_outDistance);
			}

			static GReturn IntersectSphereToAABBD(const MATH::GSPHERED _sphere, const MATH::GAABBCED _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				MATH::GAABBMMD mm = {};
				ConvertAABBCEToAABBMMD(
					_aabb,
					mm);

				double sq_distance = 0.0;
				SqDistancePointToAABBD(
					_sphere.data.xyz(),
					mm,
					sq_distance);

				double sq_radius = _sphere.radius * _sphere.radius;

				_outResult = (sq_distance <= sq_radius) ?
					GCollisionCheck::COLLISION :
					GCollisionCheck::NO_COLLISION;

				if(_outResult < GCollisionCheck::COLLISION)
				{
					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::SubtractVectorD(
					_aabb.center.xyz(),
					_sphere.data.xyz(),
					_outDirection);

				if(GW::MATH::GVector::NormalizeD(_outDirection, _outDirection) == GReturn::FAILURE)
				{
					_outDirection = {};
				}

				GW::MATH::GVector::ScaleD(
					_outDirection,
					_sphere.radius,
					_outContactClosest1);

				GW::MATH::GVector::AddVectorD(
					_sphere.data.xyz(),
					_outContactClosest1,
					_outContactClosest1);

				for(int i = 0; i < 3; i++)
				{
					_outContactClosest2.data[i] = _sphere.data.data[i];

					_outContactClosest2.data[i] = G_LARGER(
						_outContactClosest2.data[i],
						mm.min.data[i]);

					_outContactClosest2.data[i] = G_SMALLER(
						_outContactClosest2.data[i],
						mm.max.data[i]);
				}

				// Point is inside AABB so clamp to closest face
				if(G_ABS(_sphere.x - _outContactClosest2.x) < G_COLLISION_THRESHOLD_D ||
				   G_ABS(_sphere.y - _outContactClosest2.y) < G_COLLISION_THRESHOLD_D ||
				   G_ABS(_sphere.z - _outContactClosest2.z) < G_COLLISION_THRESHOLD_D)
				{
					GW::MATH::GVECTORD translation = {};
					double low = static_cast<double>(0xffffffff);
					double val = 0.0;

					val = mm.max.x - _sphere.x;
					if(G_ABS(val) < low)
					{
						translation = {{{val, 0.0, 0.0}}};
						low = G_ABS(val);
					}

					val = _sphere.x - mm.min.x;
					if(G_ABS(val) < low)
					{
						translation = {{{val, 0.0, 0.0}}};
						low = G_ABS(val);
					}

					val = mm.max.y - _sphere.y;
					if(G_ABS(val) < low)
					{
						translation = {{{0.0, val, 0.0}}};
						low = G_ABS(val);
					}

					val = _sphere.y - mm.min.y;
					if(G_ABS(val) < low)
					{
						translation = {{{0.0, val, 0.0}}};
						low = G_ABS(val);
					}

					val = mm.max.z - _sphere.z;
					if(G_ABS(val) < low)
					{
						translation = {{{0.0, 0.0, val}}};
						low = G_ABS(val);
					}

					val = _sphere.z - mm.min.z;
					if(G_ABS(val) < low)
					{
						translation = {{{0.0, 0.0, val}}};
						low = G_ABS(val);
					}

					GW::MATH::GVector::AddVectorD(
						_outContactClosest2,
						translation,
						_outContactClosest2);
				}

				GW::MATH::GVector::SubtractVectorD(
					_outContactClosest2,
					_outContactClosest1,
					_outDirection);

				GW::MATH::GVector::DotD(
					_outDirection,
					_outDirection,
					_outDistance);

				_outDistance = sqrt(_outDistance);

				if(GW::MATH::GVector::NormalizeD(_outDirection, _outDirection) == GReturn::FAILURE)
				{
					_outDirection = {};
				}

				return GReturn::SUCCESS;
			}

			static GReturn IntersectSphereToOBBD(const MATH::GSPHERED _sphere, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				MATH::GMATRIXD obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionD(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXD obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0}}},
							{{{0.0, 0.0, 0.0, 1.0f}}}
						}
					}
				};

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row1,
					obb_rotation_inverse.row4.x);
				obb_rotation_inverse.row4.x = -obb_rotation_inverse.row4.x;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row2,
					obb_rotation_inverse.row4.y);
				obb_rotation_inverse.row4.y = -obb_rotation_inverse.row4.y;

				GW::MATH::GVector::DotD(
					_obb.center,
					obb_rotation.row3,
					obb_rotation_inverse.row4.z);
				obb_rotation_inverse.row4.z = -obb_rotation_inverse.row4.z;

				MATH::GSPHERED sphere = {};
				sphere.data = _sphere.data;
				sphere.radius = 1.0f;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					sphere.data,
					sphere.data);
				GW::MATH::GVector::AddVectorD(
					sphere.data,
					_obb.center,
					sphere.data);
				sphere.radius = _sphere.radius;

				IntersectSphereToAABBD(
					sphere,
					{{{_obb.center, _obb.extent}}},
					_outResult,
					_outContactClosest1,
					_outContactClosest2,
					_outDirection,
					_outDistance);

				GMatrixImplementation::InverseD(
					obb_rotation_inverse,
					obb_rotation);

				GW::MATH::GVector::SubtractVectorD(
					_outContactClosest1,
					_obb.center,
					_outContactClosest1);

				_outContactClosest1.w = 1.0F;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation,
					_outContactClosest1,
					_outContactClosest1);

				_outContactClosest1.w = 0.0F;

				GW::MATH::GVector::SubtractVectorD(
					_outContactClosest2,
					_obb.center,
					_outContactClosest2);

				_outContactClosest2.w = 1.0F;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation,
					_outContactClosest2,
					_outContactClosest2);

				_outContactClosest2.w = 0.0F;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation,
					_outDirection.xyz(),
					_outDirection);

				return GReturn::SUCCESS;
			}

			static GReturn IntersectCapsuleToCapsuleD(const MATH::GCAPSULED _capsule1, const MATH::GCAPSULED _capsule2, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				MATH::GSPHERED sphere1 = {};
				MATH::GSPHERED sphere2 = {};

				ClosestPointsToLineFromLineD(
					{{{_capsule1.data[0], _capsule1.data[1]}}},
					{{{_capsule2.data[0], _capsule2.data[1]}}},
					sphere1.data,
					sphere2.data);

				sphere1.radius = _capsule1.radius;
				sphere2.radius = _capsule2.radius;

				return IntersectSphereToSphereD(
					sphere1,
					sphere2,
					_outResult,
					_outContactClosest1,
					_outContactClosest2,
					_outDirection,
					_outDistance);
			}

			static GReturn IntersectCapsuleToAABBD(const MATH::GCAPSULED _capsule, const MATH::GAABBMMD _aabb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				// Extend AABB by capsule radius
				MATH::GAABBMMD aabbMM =
				{
					{
						{
							{{{_aabb.min.x - _capsule.radius, _aabb.min.y - _capsule.radius, _aabb.min.z - _capsule.radius , 0.0}}},
							{{{_aabb.max.x + _capsule.radius, _aabb.max.y + _capsule.radius, _aabb.max.z + _capsule.radius}}}
						}
					}
				};

				MATH::GRAYD ray = {};
				ray.position = _capsule.data[0].xyz();

				GW::MATH::GVector::SubtractVectorD(
					ray.position,
					_capsule.data[0].xyz(),
					ray.direction);

				GW::MATH::GVector::NormalizeD(
					ray.direction,
					ray.direction);

				IntersectRayToAABBD(
					ray,
					aabbMM,
					_outResult,
					_outContactClosest1,
					_outDistance);

				if(static_cast<int>(_outResult) <= 0 || _outDistance > 1.0f)
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				// Flags to determine the region
				int u = 0;
				int v = 0;

				if(_outContactClosest1.x < _aabb.min.x)
				{
					u |= 1;
				}
				if(_outContactClosest1.x > _aabb.max.x)
				{
					v |= 1;
				}

				if(_outContactClosest1.y < _aabb.min.y)
				{
					u |= 2;
				}
				if(_outContactClosest1.y > _aabb.max.y)
				{
					v |= 2;
				}

				if(_outContactClosest1.z < _aabb.min.z)
				{
					u |= 4;
				}
				if(_outContactClosest1.z > _aabb.max.z)
				{
					v |= 4;
				}

				int m = u + v;

				auto Corner = [](const MATH::GAABBMMD box, const int n)->GW::MATH::GVECTORD
				{
					if(n & 1)
					{
						return GW::MATH::GVECTORD{{{box.max.x, box.max.y, box.max.z}}};
					}
					else
					{
						return GW::MATH::GVECTORD{{{box.min.x, box.min.y, box.min.z}}};
					}
				};

				GW::MATH::GCAPSULED capsule;
				capsule.data[0] = Corner(aabbMM, v);

				// Vertex Region
				if(m == 7)
				{
					double tmin = static_cast<double>(0xffffffff);

					capsule.data[1] = Corner(aabbMM, v ^ 1);
					IntersectLineToCapsuleD(
						{{{_capsule.data[0].xyz(), _capsule.data[1].xyz()}}},
						capsule,
						_outResult,
						_outContactClosest1,
						_outDirection,
						tmin);

					if (static_cast<int>(_outResult) > 0)
					{
						_outDistance = G_SMALLER(tmin, _outDistance);
					}

					capsule.data[1] = Corner(aabbMM, v ^ 2);
					IntersectLineToCapsuleD(
						{{{_capsule.data[0].xyz(), _capsule.data[1].xyz()}}},
						capsule,
						_outResult,
						_outContactClosest1,
						_outDirection,
						tmin);

					if(static_cast<int>(_outResult) > 0)
					{
						_outDistance = G_SMALLER(tmin, _outDistance);
					}

					capsule.data[1] = Corner(aabbMM, v ^ 4);
					IntersectLineToCapsuleD(
						{{{_capsule.data[0].xyz(), _capsule.data[1].xyz()}}},
						capsule,
						_outResult,
						_outContactClosest1,
						_outDirection,
						tmin);

					if (static_cast<int>(_outResult) > 0)
					{
						_outDistance = G_SMALLER(tmin, _outDistance);
					}

					if(tmin == static_cast<double>(0xffffffff))
					{
						_outResult = GCollisionCheck::NO_COLLISION;
						return GReturn::SUCCESS;
					}

					_outDistance = tmin;

					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				if((m & (m - 1)) == 0)
				{
					_outResult = GCollisionCheck::COLLISION;
					return GReturn::SUCCESS;
				}

				capsule.data[1] = capsule.data[0];
				capsule.data[0] = Corner(aabbMM, u ^ 7);

				return IntersectLineToCapsuleD(
					{{{_capsule.data[0].xyz(), _capsule.data[1].xyz()}}},
					capsule,
					_outResult,
					_outContactClosest1,
					_outDirection,
					_outDistance);
			}

			static GReturn IntersectCapsuleToOBBD(const MATH::GCAPSULED _capsule, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				MATH::GMATRIXD obb_rotation = {};
				GMatrixImplementation::ConvertQuaternionD(
					{{{_obb.rotation.x,_obb.rotation.y,_obb.rotation.z,_obb.rotation.w}}},
					obb_rotation);

				MATH::GMATRIXD obb_rotation_inverse =
				{
					{
						{
							{{{obb_rotation.data[0], obb_rotation.data[4], obb_rotation.data[8], 0.0}}},
							{{{obb_rotation.data[1], obb_rotation.data[5], obb_rotation.data[9], 0.0}}},
							{{{obb_rotation.data[2], obb_rotation.data[6], obb_rotation.data[10], 0.0}}},
							{{{0.0, 0.0, 0.0, 1.0f}}}
						}
					}
				};

				MATH::GCAPSULED capsule = {};
				capsule.data[0] = _capsule.data[0];
				capsule.data[0].w = 1.0f;
				capsule.data[1] = _capsule.data[1];
				capsule.data[1].w = 1.0f;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					capsule.data[0],
					capsule.data[0]);

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation_inverse,
					capsule.data[1],
					capsule.data[1]);

				capsule.radius = _capsule.radius;

				IntersectCapsuleToAABBD(
					capsule,
					{{{_obb.center, _obb.extent}}},
					_outResult,
					_outContactClosest1,
					_outContactClosest2,
					_outDirection,
					_outDistance);

				GMatrixImplementation::InverseD(
					obb_rotation_inverse,
					obb_rotation);

				GW::MATH::GVector::SubtractVectorD(
					_outContactClosest1,
					_obb.center,
					_outContactClosest1);

				_outContactClosest1.w = 1.0F;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation,
					_outContactClosest1,
					_outContactClosest1);

				_outContactClosest1.w = 0.0F;

				GW::MATH::GVector::SubtractVectorD(
					_outContactClosest2,
					_obb.center,
					_outContactClosest2);

				_outContactClosest2.w = 1.0F;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation,
					_outContactClosest2,
					_outContactClosest2);

				_outContactClosest2.w = 0.0F;

				GMatrixImplementation::VectorXMatrixD(
					obb_rotation,
					_outDirection.xyz(),
					_outDirection);

				return GReturn::SUCCESS;
			}

			static GReturn IntersectAABBToAABBD(const MATH::GAABBCED _aabb1, const MATH::GAABBCED _aabb2, GCollisionCheck& _outResult, MATH::GAABBCED& _outContactAABB, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				if(G_ABS(_aabb1.center.x - _aabb2.center.x) > (_aabb1.extent.x + _aabb2.extent.x) ||
				   G_ABS(_aabb1.center.y - _aabb2.center.y) > (_aabb1.extent.y + _aabb2.extent.y) ||
				   G_ABS(_aabb1.center.z - _aabb2.center.z) > (_aabb1.extent.z + _aabb2.extent.z))
				{
					_outResult = GCollisionCheck::NO_COLLISION;
					return GReturn::SUCCESS;
				}

				MATH::GAABBMMD aabb = {};
				aabb.min.x = G_LARGER(_aabb1.center.x - _aabb1.extent.x, _aabb2.center.x - _aabb2.extent.x);
				aabb.max.x = G_SMALLER(_aabb1.center.x + _aabb1.extent.x, _aabb2.center.x + _aabb2.extent.x);
				aabb.min.y = G_LARGER(_aabb1.center.y - _aabb1.extent.y, _aabb2.center.y - _aabb2.extent.y);
				aabb.max.y = G_SMALLER(_aabb1.center.y + _aabb1.extent.y, _aabb2.center.y + _aabb2.extent.y);
				aabb.min.z = G_LARGER(_aabb1.center.z - _aabb1.extent.z, _aabb2.center.z - _aabb2.extent.z);
				aabb.max.z = G_SMALLER(_aabb1.center.z + _aabb1.extent.z, _aabb2.center.z + _aabb2.extent.z);

				GW::MATH::GVECTORD difference = {};

				GW::MATH::GVector::SubtractVectorD(
					aabb.max,
					aabb.min,
					difference);

				GW::MATH::GVector::DotD(
					difference,
					difference,
					_outDistance);

				_outDistance = sqrt(_outDistance);

				GW::MATH::GVector::SubtractVectorD(
					_aabb2.center,
					_aabb1.center,
					_outDirection);

				GW::MATH::GVector::NormalizeD(
					_outDirection,
					_outDirection);

				_outResult = GCollisionCheck::COLLISION;
				return GReturn::SUCCESS;
			}

			/*static GReturn IntersectAABBToOBBD(const MATH::GAABBCED _aabb, const MATH::GOBBD _obb, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return IntersectOBBToOBBD(
					{_aabb.center, _aabb.extent},
					_obb,
					_outResult,
					_outContactClosest1,
					_outContactClosest2,
					_outDirection,
					_outDistance);
			}

			static GReturn IntersectOBBToOBBD(const MATH::GOBBD _obb1, const MATH::GOBBD _obb2, GCollisionCheck& _outResult, MATH::GVECTORD& _outContactClosest1, MATH::GVECTORD& _outContactClosest2, MATH::GVECTORD& _outDirection, double& _outDistance)
			{
				return GReturn::FAILURE;
			}*/

			static GReturn SqDistancePointToLineD(const MATH::GVECTORD _point, const MATH::GLINED _line, double& _outDistance)
			{
				GW::MATH::GVECTORD p = _point.xyz();

				MATH::GLINED line =
				{
					{
						{
							_line.start.xyz(),
							_line.end.xyz()
						}
					}
				};

				MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					line.end,
					line.start,
					difference_ba);

				MATH::GVECTORD difference_pa = {};
				GW::MATH::GVector::SubtractVectorD(
					p,
					line.start,
					difference_pa);

				MATH::GVECTORD difference_pb = {};
				GW::MATH::GVector::SubtractVectorD(
					p,
					line.end,
					difference_pb);

				double interval1 = 0.0;
				GW::MATH::GVector::DotD(
					difference_pa,
					difference_ba,
					interval1);

				if(interval1 <= 0.0)
				{
					GW::MATH::GVector::DotD(
						difference_pa,
						difference_pa,
						_outDistance);

					return GReturn::SUCCESS;
				}

				double interval2 = 0.0;
				GW::MATH::GVector::DotD(
					difference_ba,
					difference_ba,
					interval2);

				if(interval1 >= interval2)
				{
					GW::MATH::GVector::DotD(
						difference_pb,
						difference_pb,
						_outDistance);

					return GReturn::SUCCESS;
				}

				GW::MATH::GVector::DotD(
					difference_pa,
					difference_pa,
					_outDistance);

				if(G_ABS(interval2) >= G_COLLISION_THRESHOLD_D)
				{
					_outDistance -= interval1 * interval1 / interval2;
				}

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToRayD(const MATH::GVECTORD _point, const MATH::GRAYD _ray, double& _outDistance)
			{
				GW::MATH::GVECTORD p = {};

				ClosestPointToRayD(
					_ray,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorD(
					p,
					_point.xyz(),
					p);

				GW::MATH::GVector::DotD(
					p,
					p,
					_outDistance);

				_outDistance = sqrt(_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToTriangleD(const MATH::GVECTORD _point, const MATH::GTRIANGLED _triangle, double& _outDistance)
			{
				GW::MATH::GVECTORD p = {};
				ClosestPointToTriangleD(
					_triangle,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorD(
					p,
					_point.xyz(),
					p);

				GW::MATH::GVector::DotD(
					p,
					p,
					_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToPlaneD(const MATH::GVECTORD _point, const MATH::GPLANED _plane, double& _outDistance)
			{
				GW::MATH::GVector::DotD(
					_plane.data.xyz(),
					_point.xyz(),
					_outDistance);
				_outDistance -= _plane.distance;
				_outDistance *= _outDistance;

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToSphereD(const MATH::GVECTORD _point, const MATH::GSPHERED _sphere, double& _outDistance)
			{
				GW::MATH::GVECTORD p = {};
				ClosestPointToSphereD(
					_sphere,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorD(
					p,
					_point.xyz(),
					p);

				GW::MATH::GVector::DotD(
					p,
					p,
					_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToCapsuleD(const MATH::GVECTORD _point, const MATH::GCAPSULED _capsule, double& _outDistance)
			{
				GW::MATH::GVECTORD p = {};
				ClosestPointToCapsuleD(
					_capsule,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorD(
					p,
					_point.xyz(),
					p);

				GW::MATH::GVector::DotD(
					p,
					p,
					_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToAABBD(const MATH::GVECTORD _point, const MATH::GAABBMMD _aabb, double& _outDistance)
			{

				_outDistance = 0.0;

				for(int i = 0; i < 3; i++)
				{
					double v = _point.data[i];

					if(v < _aabb.min.data[i])
					{
						_outDistance += (_aabb.min.data[i] - v) * (_aabb.min.data[i] - v);
					}

					if(v > _aabb.max.data[i])
					{
						_outDistance += (v - _aabb.max.data[i]) * (v - _aabb.max.data[i]);
					}
				}

				return GReturn::SUCCESS;
			}

			static GReturn SqDistancePointToOBBD(const MATH::GVECTORD _point, const MATH::GOBBD _obb, double& _outDistance)
			{
				MATH::GVECTORD p = {};
				ClosestPointToOBBD(
					_obb,
					_point,
					p);

				GW::MATH::GVector::SubtractVectorD(
					p,
					_point,
					p);

				GW::MATH::GVector::DotD(
					p,
					p,
					_outDistance);

				return GReturn::SUCCESS;
			}

			static GReturn BarycentricD(const MATH::GVECTORD _a, const MATH::GVECTORD _b, const MATH::GVECTORD _c, const MATH::GVECTORD _p, MATH::GVECTORD& _outBarycentric)
			{
				MATH::GVECTORD difference_ba = {};
				GW::MATH::GVector::SubtractVectorD(
					_b,
					_a,
					difference_ba);

				MATH::GVECTORD difference_ca = {};
				GW::MATH::GVector::SubtractVectorD(
					_c,
					_a,
					difference_ca);

				MATH::GVECTORD triangle_n = {};
				GW::MATH::GVector::CrossVector3D(
					difference_ba,
					difference_ca,
					triangle_n);

				double x = G_ABS(triangle_n.x);
				double y = G_ABS(triangle_n.y);
				double z = G_ABS(triangle_n.z);

				// Degenerate triangle
				if(x + y + z < G_COLLISION_THRESHOLD_D)
				{
					return GReturn::FAILURE;
				}

				double nominator_u = 0.0;
				double nominator_v = 0.0;
				double w = 0.0;

				// Project to yz plane
				if(x >= y && x >= z)
				{
					// Area of pbc in yz plane
					GW::MATH::GVector::CrossVector2D(
						GW::MATH::GVECTORD{{{_p.y - _b.y,_b.y - _c.y}}},
						GW::MATH::GVECTORD{{{_p.z - _b.z,_b.z - _c.z}}},
						nominator_u);

					// Area of pca in yz plane
					GW::MATH::GVector::CrossVector2D(
						GW::MATH::GVECTORD{{{_p.y - _c.y,_c.y - _a.y}}},
						GW::MATH::GVECTORD{{{_p.z - _c.z,_c.z - _a.z}}},
						nominator_v);

					w = 1.0 / triangle_n.x;
				}
				// Project to the xz plane
				else if(y >= x && y >= z)
				{
					// Area of pbc in xz plane
					GW::MATH::GVector::CrossVector2D(
						GW::MATH::GVECTORD{{{_p.x - _b.x,_b.x - _c.x}}},
						GW::MATH::GVECTORD{{{_p.z - _b.z,_b.z - _c.z}}},
						nominator_u);

					// Area of pca in xz plane
					GW::MATH::GVector::CrossVector2D(
						GW::MATH::GVECTORD{{{_p.x - _c.x,_c.x - _a.x}}},
						GW::MATH::GVECTORD{{{_p.z - _c.z,_c.z - _a.z}}},
						nominator_v);

					w = 1.0 / (-triangle_n.y);
				}
				// Project to the xy plane
				else
				{
					// Area of pbc in xy plane
					GW::MATH::GVector::CrossVector2D(
						GW::MATH::GVECTORD{{{_p.x - _b.x,_b.x - _c.x}}},
						GW::MATH::GVECTORD{{{_p.y - _b.y,_b.y - _c.y}}},
						nominator_u);

					// Area of pca in xy plane
					GW::MATH::GVector::CrossVector2D(
						GW::MATH::GVECTORD{{{_p.x - _c.x,_c.x - _a.x}}},
						GW::MATH::GVECTORD{{{_p.y - _c.y,_c.y - _a.y}}},
						nominator_v);

					w = 1.0 / triangle_n.z;
				}

				double u = nominator_u * w;
				double v = nominator_v * w;

				_outBarycentric =
				{
					{
						{
							u,
							v,
							1.0 - u - v
						}
					}
				};

				return GReturn::SUCCESS;
			}
		};
	}
}
