#include <cmath>

namespace GW
{
	namespace I
	{
		class GQuaternionImplementation : public virtual GQuaternionInterface,
			private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::SUCCESS;
			}
			static GReturn AddQuaternionF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion)
			{
				_outQuaternion.x = _quaternion1.x + _quaternion2.x;
				_outQuaternion.y = _quaternion1.y + _quaternion2.y;
				_outQuaternion.z = _quaternion1.z + _quaternion2.z;
				_outQuaternion.w = _quaternion1.w + _quaternion2.w;
				return GReturn::SUCCESS;
			}
			static GReturn SubtractQuaternionF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion)
			{
				_outQuaternion.x = _quaternion1.x - _quaternion2.x;
				_outQuaternion.y = _quaternion1.y - _quaternion2.y;
				_outQuaternion.z = _quaternion1.z - _quaternion2.z;
				_outQuaternion.w = _quaternion1.w - _quaternion2.w;
				return GReturn::SUCCESS;
			}
			static GReturn MultiplyQuaternionF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GQUATERNIONF& _outQuaternion)
			{
				MATH::GQUATERNIONF _q1 = _quaternion1;
				MATH::GQUATERNIONF _q2 = _quaternion2;

				_outQuaternion.x = _q1.w * _q2.x + _q1.x * _q2.w + _q1.y * _q2.z - _q1.z * _q2.y;
				_outQuaternion.y = _q1.w * _q2.y - _q1.x * _q2.z + _q1.y * _q2.w + _q1.z * _q2.x;
				_outQuaternion.z = _q1.w * _q2.z + _q1.x * _q2.y - _q1.y * _q2.x + _q1.z * _q2.w;
				_outQuaternion.w = _q1.w * _q2.w - _q1.x * _q2.x - _q1.y * _q2.y - _q1.z * _q2.z;

				return GReturn::SUCCESS;
			}
			static GReturn ScaleF(MATH::GQUATERNIONF _quaternion, float _scalar, MATH::GQUATERNIONF& _outQuaternion)
			{
				_outQuaternion.x = _quaternion.x * _scalar;
				_outQuaternion.y = _quaternion.y * _scalar;
				_outQuaternion.z = _quaternion.z * _scalar;
				_outQuaternion.w = _quaternion.w * _scalar;
				return GReturn::SUCCESS;
			}
			static GReturn SetByVectorAngleF(MATH::GVECTORF _vector, float _radian, MATH::GQUATERNIONF& _outQuaternion)
			{
				float s = sinf(_radian / 2.0f);
				_outQuaternion.x = s * _vector.x;
				_outQuaternion.y = s * _vector.y;
				_outQuaternion.z = s * _vector.z;
				_outQuaternion.w = cosf(_radian / 2.0f);
				return GReturn::SUCCESS;
			}
			static GReturn SetByMatrixF(MATH::GMATRIXF _matrix, MATH::GQUATERNIONF& _outQuaternion)
			{
				float det;
				float sx = sqrtf(_matrix.data[0] * _matrix.data[0] + _matrix.data[4] * _matrix.data[4] + _matrix.data[8] * _matrix.data[8]);
				float sy = sqrtf(_matrix.data[1] * _matrix.data[1] + _matrix.data[5] * _matrix.data[5] + _matrix.data[9] * _matrix.data[9]);
				float sz = sqrtf(_matrix.data[2] * _matrix.data[2] + _matrix.data[6] * _matrix.data[6] + _matrix.data[10] * _matrix.data[10]);
				if (G_WITHIN_STANDARD_DEVIATION_F(sx, 0.0f) || G_WITHIN_STANDARD_DEVIATION_F(sy, 0.0f) || G_WITHIN_STANDARD_DEVIATION_F(sz, 0.0f))
				{
					//scale too close to zero, can not decompose rotation
					return GReturn::FAILURE;
				}

				float a0 = _matrix.data[0] * _matrix.data[5] - _matrix.data[1] * _matrix.data[4];
				float a1 = _matrix.data[0] * _matrix.data[6] - _matrix.data[2] * _matrix.data[4];
				float a2 = _matrix.data[0] * _matrix.data[7] - _matrix.data[3] * _matrix.data[4];
				float a3 = _matrix.data[1] * _matrix.data[6] - _matrix.data[2] * _matrix.data[5];
				float a4 = _matrix.data[1] * _matrix.data[7] - _matrix.data[3] * _matrix.data[5];
				float a5 = _matrix.data[2] * _matrix.data[7] - _matrix.data[3] * _matrix.data[6];
				float b0 = _matrix.data[8] * _matrix.data[13] - _matrix.data[9] * _matrix.data[12];
				float b1 = _matrix.data[8] * _matrix.data[14] - _matrix.data[10] * _matrix.data[12];
				float b2 = _matrix.data[8] * _matrix.data[15] - _matrix.data[11] * _matrix.data[12];
				float b3 = _matrix.data[9] * _matrix.data[14] - _matrix.data[10] * _matrix.data[13];
				float b4 = _matrix.data[9] * _matrix.data[15] - _matrix.data[11] * _matrix.data[13];
				float b5 = _matrix.data[10] * _matrix.data[15] - _matrix.data[11] * _matrix.data[14];

				det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;

				if (det < 0)
				{
					sz = -sz;
				}
				MATH::GMATRIXF Rotation = _matrix;
				Rotation.data[0] /= sx;
				Rotation.data[1] /= sy;
				Rotation.data[2] /= sz;
				Rotation.data[4] /= sx;
				Rotation.data[5] /= sy;
				Rotation.data[6] /= sz;
				Rotation.data[8] /= sx;
				Rotation.data[9] /= sy;
				Rotation.data[10] /= sz;

				float trace = _matrix.data[0] + _matrix.data[5] + _matrix.data[10] + 1;

				if (trace > G_EPSILON_F)
				{
					float s = 0.5f / sqrtf(trace);
					_outQuaternion.x = (Rotation.row3.y - Rotation.row2.z) * s;
					_outQuaternion.y = (Rotation.row1.z - Rotation.row3.x) * s;
					_outQuaternion.z = (Rotation.row2.x - Rotation.row1.y) * s;
					_outQuaternion.w = 0.25f / s;
				}
				else
				{
					if (Rotation.row1.x > Rotation.row2.y && Rotation.row1.x > Rotation.row3.z)
					{
						float s = 0.5f / sqrtf(1.0f + Rotation.row1.x - Rotation.row2.y - Rotation.row3.z);
						_outQuaternion.x = 0.25f / s;
						_outQuaternion.y = (Rotation.row1.y + Rotation.row2.x) * s;
						_outQuaternion.z = (Rotation.row1.z + Rotation.row3.x) * s;
						_outQuaternion.w = (Rotation.row3.y - Rotation.row2.z) * s;
					}
					else if (Rotation.row2.y > Rotation.row3.z)
					{
						float s = 0.5f / sqrtf(1.0f + Rotation.row2.y - Rotation.row1.x - Rotation.row3.z);
						_outQuaternion.x = (Rotation.row1.y + Rotation.row2.x) * s;
						_outQuaternion.y = 0.25f / s;
						_outQuaternion.z = (Rotation.row2.z + Rotation.row3.y) * s;
						_outQuaternion.w = (Rotation.row1.z - Rotation.row3.x) * s;
					}
					else
					{
						float s = 0.5f / sqrtf(1.0f + Rotation.row3.z - Rotation.row1.x - Rotation.row2.y);
						_outQuaternion.x = (Rotation.row1.z + Rotation.row3.x) * s;
						_outQuaternion.y = (Rotation.row2.z + Rotation.row3.y) * s;
						_outQuaternion.z = 0.25f / s;
						_outQuaternion.w = (Rotation.row2.x - Rotation.row1.y) * s;
					}
				}
				return GReturn::SUCCESS;
			}
			static GReturn DotF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float& _outValue)
			{
				_outValue = _quaternion1.w * _quaternion2.w + _quaternion1.x * _quaternion2.x + _quaternion1.y * _quaternion2.y + _quaternion1.z * _quaternion2.z;
				return GReturn::SUCCESS;
			}
			static GReturn CrossF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, MATH::GVECTORF& _outVector)
			{
				_outVector.x = (_quaternion1.y * _quaternion2.z) - (_quaternion1.z * _quaternion2.y);
				_outVector.y = (_quaternion1.z * _quaternion2.x) - (_quaternion1.x * _quaternion2.z);
				_outVector.z = (_quaternion1.x * _quaternion2.y) - (_quaternion1.y * _quaternion2.x);
				_outVector.w = 0.0f;

				return GReturn::SUCCESS;
			}
			static GReturn ConjugateF(MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion)
			{
				_outQuaternion.x = -_quaternion.x;
				_outQuaternion.y = -_quaternion.y;
				_outQuaternion.z = -_quaternion.z;
				_outQuaternion.w = _quaternion.w;
				return GReturn::SUCCESS;
			}
			static GReturn InverseF(MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion)
			{
				MATH::GQUATERNIONF temp;
				float magnitude;
				ConjugateF(_quaternion, temp);
				if (MagnitudeF(_quaternion, magnitude) != GReturn::SUCCESS)
				{
					return GReturn::FAILURE;
				}
				_outQuaternion.x = temp.x / (magnitude * magnitude);
				_outQuaternion.y = temp.y / (magnitude * magnitude);
				_outQuaternion.z = temp.z / (magnitude * magnitude);
				_outQuaternion.w = temp.w / (magnitude * magnitude);
				return GReturn::SUCCESS;
			}
			static GReturn MagnitudeF(MATH::GQUATERNIONF _quaternion, float& _outMagnitude)
			{
				_outMagnitude = sqrtf(_quaternion.x * _quaternion.x + _quaternion.y * _quaternion.y + _quaternion.z * _quaternion.z + _quaternion.w * _quaternion.w);
				if (G_WITHIN_STANDARD_DEVIATION_F(_outMagnitude, G_EPSILON_F))
					return GReturn::FAILURE;

				return GReturn::SUCCESS;
			}
			static GReturn NormalizeF(MATH::GQUATERNIONF _quaternion, MATH::GQUATERNIONF& _outQuaternion)
			{
				float magnitude;
				MagnitudeF(_quaternion, magnitude);
				if (G_WITHIN_STANDARD_DEVIATION_F(magnitude, 1.0f))
				{
					_outQuaternion = _quaternion;
					return GReturn::SUCCESS;
				}
				if (G_WITHIN_STANDARD_DEVIATION_F(magnitude, G_EPSILON_F))
				{
					return GReturn::FAILURE;
				}
				magnitude = 1 / magnitude;
				_outQuaternion.x = _quaternion.x * magnitude;
				_outQuaternion.y = _quaternion.y * magnitude;
				_outQuaternion.z = _quaternion.z * magnitude;
				_outQuaternion.w = _quaternion.w * magnitude;
				return GReturn::SUCCESS;
			}
			static GReturn IdentityF(MATH::GQUATERNIONF& _outQuaternion)
			{
				_outQuaternion = MATH::GIdentityQuaternionF;
				return GReturn::SUCCESS;
			}
			static GReturn LerpF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float _ratio, MATH::GQUATERNIONF& _outQuaternion)
			{
				_outQuaternion.x = G_LERP(_quaternion1.x, _quaternion2.x, _ratio);
				_outQuaternion.y = G_LERP(_quaternion1.y, _quaternion2.y, _ratio);
				_outQuaternion.z = G_LERP(_quaternion1.z, _quaternion2.z, _ratio);
				_outQuaternion.w = G_LERP(_quaternion1.w, _quaternion2.w, _ratio);
				return GReturn::SUCCESS;
			}
			static GReturn SlerpF(MATH::GQUATERNIONF _quaternion1, MATH::GQUATERNIONF _quaternion2, float _ratio, MATH::GQUATERNIONF& _outQuaternion)
			{
				MATH::GQUATERNIONF q1;
				MATH::GQUATERNIONF q2;
				if (NormalizeF(_quaternion1, q1) != GReturn::SUCCESS || NormalizeF(_quaternion2, q2) != GReturn::SUCCESS)
				{
					return GReturn::FAILURE;
				}
				float dot;
				DotF(q1, q2, dot);

				if (G_WITHIN_LOOSE_DEVIATION_F(dot, 1))
				{
					return LerpF(_quaternion1, _quaternion2, _ratio, _outQuaternion);
				}

				float theta = acosf(dot);

				ScaleF(q1, sinf(theta * (1 - _ratio)) / sinf(theta), q1);
				ScaleF(q2, sinf(theta * _ratio) / sinf(theta), q2);

				AddQuaternionF(q1, q2, _outQuaternion);

				return GReturn::SUCCESS;
			}
			static GReturn Upgrade(MATH::GQUATERNIONF _quaternionF, MATH::GQUATERNIOND& _outQuaternionD)
			{
				_outQuaternionD.x = static_cast<double>(_quaternionF.x);
				_outQuaternionD.y = static_cast<double>(_quaternionF.y);
				_outQuaternionD.z = static_cast<double>(_quaternionF.z);
				_outQuaternionD.w = static_cast<double>(_quaternionF.w);

				return GReturn::SUCCESS;
			}






			static GReturn AddQuaternionD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion)
			{
				_outQuaternion.x = _quaternion1.x + _quaternion2.x;
				_outQuaternion.y = _quaternion1.y + _quaternion2.y;
				_outQuaternion.z = _quaternion1.z + _quaternion2.z;
				_outQuaternion.w = _quaternion1.w + _quaternion2.w;
				return GReturn::SUCCESS;
			}
			static GReturn SubtractQuaternionD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion)
			{
				_outQuaternion.x = _quaternion1.x - _quaternion2.x;
				_outQuaternion.y = _quaternion1.y - _quaternion2.y;
				_outQuaternion.z = _quaternion1.z - _quaternion2.z;
				_outQuaternion.w = _quaternion1.w - _quaternion2.w;
				return GReturn::SUCCESS;
			}
			static GReturn MultiplyQuaternionD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GQUATERNIOND& _outQuaternion)
			{
				MATH::GQUATERNIOND _q1 = _quaternion1;
				MATH::GQUATERNIOND _q2 = _quaternion2;
				_outQuaternion.x = _q1.w * _q2.x + _q1.x * _q2.w + _q1.y * _q2.z - _q1.z * _q2.y;
				_outQuaternion.y = _q1.w * _q2.y - _q1.x * _q2.z + _q1.y * _q2.w + _q1.z * _q2.x;
				_outQuaternion.z = _q1.w * _q2.z + _q1.x * _q2.y - _q1.y * _q2.x + _q1.z * _q2.w;
				_outQuaternion.w = _q1.w * _q2.w - _q1.x * _q2.x - _q1.y * _q2.y - _q1.z * _q2.z;


				return GReturn::SUCCESS;
			}
			static GReturn ScaleD(MATH::GQUATERNIOND _quaternion, double _scalar, MATH::GQUATERNIOND& _outQuaternion)
			{
				_outQuaternion.x = _quaternion.x * _scalar;
				_outQuaternion.y = _quaternion.y * _scalar;
				_outQuaternion.z = _quaternion.z * _scalar;
				_outQuaternion.w = _quaternion.w * _scalar;
				return GReturn::SUCCESS;
			}
			static GReturn SetByVectorAngleD(MATH::GVECTORD _vector, double _radian, MATH::GQUATERNIOND& _outQuaternion)
			{
				double s = sin(_radian / 2.0);
				_outQuaternion.x = s * _vector.x;
				_outQuaternion.y = s * _vector.y;
				_outQuaternion.z = s * _vector.z;
				_outQuaternion.w = cos(_radian / 2.0);
				return GReturn::SUCCESS;
			}
			static GReturn SetByMatrixD(MATH::GMATRIXD _matrix, MATH::GQUATERNIOND& _outQuaternion)
			{
				double det;
				double sx = sqrt(_matrix.data[0] * _matrix.data[0] + _matrix.data[4] * _matrix.data[4] + _matrix.data[8] * _matrix.data[8]);
				double sy = sqrt(_matrix.data[1] * _matrix.data[1] + _matrix.data[5] * _matrix.data[5] + _matrix.data[9] * _matrix.data[9]);
				double sz = sqrt(_matrix.data[2] * _matrix.data[2] + _matrix.data[6] * _matrix.data[6] + _matrix.data[10] * _matrix.data[10]);
				if (G_WITHIN_STANDARD_DEVIATION_D(sx, 0) || G_WITHIN_STANDARD_DEVIATION_D(sy, 0) || G_WITHIN_STANDARD_DEVIATION_D(sz, 0))
				{
					//scale too close to zero, can not decompose rotation
					return GReturn::FAILURE;
				}
				double a0 = _matrix.data[0] * _matrix.data[5] - _matrix.data[1] * _matrix.data[4];
				double a1 = _matrix.data[0] * _matrix.data[6] - _matrix.data[2] * _matrix.data[4];
				double a2 = _matrix.data[0] * _matrix.data[7] - _matrix.data[3] * _matrix.data[4];
				double a3 = _matrix.data[1] * _matrix.data[6] - _matrix.data[2] * _matrix.data[5];
				double a4 = _matrix.data[1] * _matrix.data[7] - _matrix.data[3] * _matrix.data[5];
				double a5 = _matrix.data[2] * _matrix.data[7] - _matrix.data[3] * _matrix.data[6];
				double b0 = _matrix.data[8] * _matrix.data[13] - _matrix.data[9] * _matrix.data[12];
				double b1 = _matrix.data[8] * _matrix.data[14] - _matrix.data[10] * _matrix.data[12];
				double b2 = _matrix.data[8] * _matrix.data[15] - _matrix.data[11] * _matrix.data[12];
				double b3 = _matrix.data[9] * _matrix.data[14] - _matrix.data[10] * _matrix.data[13];
				double b4 = _matrix.data[9] * _matrix.data[15] - _matrix.data[11] * _matrix.data[13];
				double b5 = _matrix.data[10] * _matrix.data[15] - _matrix.data[11] * _matrix.data[14];

				det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;

				if (det < 0)
				{
					sz = -sz;
				}
				MATH::GMATRIXD Rotation = _matrix;
				Rotation.data[0] /= sx;
				Rotation.data[1] /= sy;
				Rotation.data[2] /= sz;
				Rotation.data[4] /= sx;
				Rotation.data[5] /= sy;
				Rotation.data[6] /= sz;
				Rotation.data[8] /= sx;
				Rotation.data[9] /= sy;
				Rotation.data[10] /= sz;

				double trace = _matrix.data[0] + _matrix.data[5] + _matrix.data[10] + 1;

				if (trace > G_EPSILON_D)
				{
					double s = 0.5 / sqrt(trace);
					_outQuaternion.x = (Rotation.row3.y - Rotation.row2.z) * s;
					_outQuaternion.y = (Rotation.row1.z - Rotation.row3.x) * s;
					_outQuaternion.z = (Rotation.row2.x - Rotation.row1.y) * s;
					_outQuaternion.w = 0.25 / s;
				}
				else
				{
					if (Rotation.row1.x > Rotation.row2.y && Rotation.row1.x > Rotation.row3.z)
					{
						double s = 0.5 / sqrt(1.0 + Rotation.row1.x - Rotation.row2.y - Rotation.row3.z);
						_outQuaternion.x = 0.25 / s;
						_outQuaternion.y = (Rotation.row1.y + Rotation.row2.x) * s;
						_outQuaternion.z = (Rotation.row1.z + Rotation.row3.x) * s;
						_outQuaternion.w = (Rotation.row3.y - Rotation.row2.z) * s;
					}
					else if (Rotation.row2.y > Rotation.row3.z)
					{
						double s = 0.5 / sqrt(1.0 + Rotation.row2.y - Rotation.row1.x - Rotation.row3.z);
						_outQuaternion.x = (Rotation.row1.y + Rotation.row2.x) * s;
						_outQuaternion.y = 0.25 / s;
						_outQuaternion.z = (Rotation.row2.z + Rotation.row3.y) * s;
						_outQuaternion.w = (Rotation.row1.z - Rotation.row3.x) * s;
					}
					else
					{
						double s = 0.5 / sqrt(1.0 + Rotation.row3.z - Rotation.row1.x - Rotation.row2.y);
						_outQuaternion.x = (Rotation.row1.z + Rotation.row3.x) * s;
						_outQuaternion.y = (Rotation.row2.z + Rotation.row3.y) * s;
						_outQuaternion.z = 0.25 / s;
						_outQuaternion.w = (Rotation.row2.x - Rotation.row1.y) * s;
					}
				}
				return GReturn::SUCCESS;
			}
			static GReturn DotD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double& _outValue)
			{
				_outValue = _quaternion1.w * _quaternion2.w + _quaternion1.x * _quaternion2.x + _quaternion1.y * _quaternion2.y + _quaternion1.z * _quaternion2.z;
				return GReturn::SUCCESS;
			}
			static GReturn CrossD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, MATH::GVECTORD& _outVector)
			{
				_outVector.x = (_quaternion1.y * _quaternion2.z) - (_quaternion1.z * _quaternion2.y);
				_outVector.y = (_quaternion1.z * _quaternion2.x) - (_quaternion1.x * _quaternion2.z);
				_outVector.z = (_quaternion1.x * _quaternion2.y) - (_quaternion1.y * _quaternion2.x);
				_outVector.w = 0.0;

				return GReturn::SUCCESS;
			}
			static GReturn ConjugateD(MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion)
			{
				_outQuaternion.x = -_quaternion.x;
				_outQuaternion.y = -_quaternion.y;
				_outQuaternion.z = -_quaternion.z;
				_outQuaternion.w = _quaternion.w;

				return GReturn::SUCCESS;
			}
			static GReturn InverseD(MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion)
			{
				MATH::GQUATERNIOND temp;
				double magnitude;
				ConjugateD(_quaternion, temp);
				if (MagnitudeD(_quaternion, magnitude) != GReturn::SUCCESS)
				{
					return GReturn::FAILURE;
				};

				_outQuaternion.x = temp.x / (magnitude * magnitude);
				_outQuaternion.y = temp.y / (magnitude * magnitude);
				_outQuaternion.z = temp.z / (magnitude * magnitude);
				_outQuaternion.w = temp.w / (magnitude * magnitude);
				return GReturn::SUCCESS;
			}
			static GReturn MagnitudeD(MATH::GQUATERNIOND _quaternion, double& _outMagnitude)
			{
				_outMagnitude = sqrt(_quaternion.x * _quaternion.x + _quaternion.y * _quaternion.y + _quaternion.z * _quaternion.z + _quaternion.w * _quaternion.w);
				if (G_WITHIN_STANDARD_DEVIATION_D(_outMagnitude, 0))
					return GReturn::FAILURE;
				return GReturn::SUCCESS;
			}
			static GReturn NormalizeD(MATH::GQUATERNIOND _quaternion, MATH::GQUATERNIOND& _outQuaternion)
			{
				double magnitude;
				MagnitudeD(_quaternion, magnitude);
				if (G_WITHIN_STANDARD_DEVIATION_D(magnitude, 1))
				{
					_outQuaternion = _quaternion;
					return GReturn::SUCCESS;
				}
				if (G_WITHIN_STANDARD_DEVIATION_D(magnitude, G_EPSILON_D))
				{
					return GReturn::FAILURE;
				}
				magnitude = 1 / magnitude;
				_outQuaternion.x = _quaternion.x * magnitude;
				_outQuaternion.y = _quaternion.y * magnitude;
				_outQuaternion.z = _quaternion.z * magnitude;
				_outQuaternion.w = _quaternion.w * magnitude;
				return GReturn::SUCCESS;
			}
			static GReturn IdentityD(MATH::GQUATERNIOND& _outQuaternion)
			{
				_outQuaternion = MATH::GIdentityQuaternionD;
				return GReturn::SUCCESS;
			}
			static GReturn LerpD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double _ratio, MATH::GQUATERNIOND& _outQuaternion)
			{
				if (_ratio > 1)
					_ratio = 1;
				if (_ratio < 0)
					_ratio = 0;

				if (G_WITHIN_STANDARD_DEVIATION_D(_ratio, 0))
				{
					_outQuaternion = _quaternion1;
					return GReturn::SUCCESS;
				}
				else if (G_WITHIN_STANDARD_DEVIATION_D(_ratio, 1))
				{
					_outQuaternion = _quaternion2;
					return GReturn::SUCCESS;
				}

				_outQuaternion.x = G_LERP(_quaternion1.x, _quaternion2.x, _ratio);
				_outQuaternion.y = G_LERP(_quaternion1.y, _quaternion2.y, _ratio);
				_outQuaternion.z = G_LERP(_quaternion1.z, _quaternion2.z, _ratio);
				_outQuaternion.w = G_LERP(_quaternion1.w, _quaternion2.w, _ratio);
				return GReturn::SUCCESS;
			}
			static GReturn SlerpD(MATH::GQUATERNIOND _quaternion1, MATH::GQUATERNIOND _quaternion2, double _ratio, MATH::GQUATERNIOND& _outQuaternion)
			{
				MATH::GQUATERNIOND q1;
				MATH::GQUATERNIOND q2;
				if (NormalizeD(_quaternion1, q1) != GReturn::SUCCESS || NormalizeD(_quaternion2, q2) != GReturn::SUCCESS)
				{
					return GReturn::FAILURE;
				}

				double dot;
				DotD(q1, q2, dot);

				if (G_WITHIN_LOOSE_DEVIATION_D(dot, 1))
				{
					return LerpD(_quaternion1, _quaternion2, _ratio, _outQuaternion);
				}

				double theta = acos(dot);

				ScaleD(q1, sin(theta * (1 - _ratio)) / sin(theta), q1);
				ScaleD(q2, sin(theta * _ratio) / sin(theta), q2);

				AddQuaternionD(q1, q2, _outQuaternion);

				return GReturn::SUCCESS;
			}
			static GReturn Downgrade(MATH::GQUATERNIOND _quaternionD, MATH::GQUATERNIONF& _outQuaternionF) {
				_outQuaternionF.x = static_cast<float>(_quaternionD.x);
				_outQuaternionF.y = static_cast<float>(_quaternionD.y);
				_outQuaternionF.z = static_cast<float>(_quaternionD.z);
				_outQuaternionF.w = static_cast<float>(_quaternionD.w);

				return GReturn::SUCCESS;
			}
		};
	}
}