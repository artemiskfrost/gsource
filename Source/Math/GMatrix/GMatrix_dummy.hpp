namespace GW
{
	namespace I
	{

		class GMatrixImplementation : public virtual GMatrixInterface,
			private GInterfaceImplementation
		{
		public:
			GReturn Create() { return GReturn::FEATURE_UNSUPPORTED; }
			// Floats
			static GReturn AddMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn SubtractMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MultiplyMatrixF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn VectorXMatrixF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GVECTORF& _outVector) { return GReturn::FAILURE; }
			static GReturn ConvertQuaternionF(GW::MATH::GQUATERNIONF _quaternion, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MultiplyNumF(GW::MATH::GMATRIXF _matrix, float _scalar, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn DeterminantF(GW::MATH::GMATRIXF _matrix, float& _outValue) { return GReturn::FAILURE; }
			static GReturn TransposeF(GW::MATH::GMATRIXF _matrix, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn InverseF(GW::MATH::GMATRIXF _matrix, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn IdentityF(GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn GetRotationF(GW::MATH::GMATRIXF _matrix, GW::MATH::GQUATERNIONF& _outQuaternion) { return GReturn::FAILURE; }
			static GReturn GetTranslationF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF& _outVector) { return GReturn::FAILURE; }
			static GReturn GetScaleF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF& _outVector) { return GReturn::FAILURE; }

			static GReturn RotateXGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateXLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateYGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateYLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateZGlobalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateZLocalF(GW::MATH::GMATRIXF _matrix, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotationYawPitchRollF(float _yaw, float _pitch, float _roll, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotationByVectorF(GW::MATH::GVECTORF _vector, float _radian, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn TranslateGlobalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn TranslateLocalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn ScaleGlobalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ScaleLocalF(GW::MATH::GMATRIXF _matrix, GW::MATH::GVECTORF _vector, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn LerpF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, float _ratio, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn ProjectionDirectXLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionDirectXRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionOpenGLLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionOpenGLRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionVulkanLHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionVulkanRHF(float _fovY, float _aspect, float _zn, float _zf, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn LookAtLHF(GW::MATH::GVECTORF _eye, GW::MATH::GVECTORF _at, GW::MATH::GVECTORF _up, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn LookAtRHF(GW::MATH::GVECTORF _eye, GW::MATH::GVECTORF _at, GW::MATH::GVECTORF _up, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn MakeRelativeF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeSeparateF(GW::MATH::GMATRIXF _matrix1, GW::MATH::GMATRIXF _matrix2, GW::MATH::GMATRIXF& _outMatrix) { return GReturn::FAILURE; }

			static GReturn Upgrade(GW::MATH::GMATRIXF _matrixF, GW::MATH::GMATRIXD& _outMatrixD) { return GReturn::FAILURE; }

			// Doubles
			static GReturn AddMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn SubtractMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MultiplyMatrixD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn VectorXMatrixD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GVECTORD& _outVector) { return GReturn::FAILURE; }
			static GReturn ConvertQuaternionD(GW::MATH::GQUATERNIOND _quaternion, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MultiplyNumD(GW::MATH::GMATRIXD _matrix, double _scalar, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn DeterminantD(GW::MATH::GMATRIXD _matrix, double& _outValue) { return GReturn::FAILURE; }
			static GReturn TransposeD(GW::MATH::GMATRIXD _matrix, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn InverseD(GW::MATH::GMATRIXD _matrix, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn IdentityD(GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn GetRotationD(GW::MATH::GMATRIXD _matrix, GW::MATH::GQUATERNIOND& _outQuaternion) { return GReturn::FAILURE; }
			static GReturn GetTranslationD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD& _outVector) { return GReturn::FAILURE; }
			static GReturn GetScaleD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD& _outVector) { return GReturn::FAILURE; }

			static GReturn RotateXGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateXLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateYGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateYLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateZGlobalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotateZLocalD(GW::MATH::GMATRIXD _matrix, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotationYawPitchRollD(double _yaw, double _pitch, double _roll, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn RotationByVectorD(GW::MATH::GVECTORD _vector, double _radian, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn TranslateGlobalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn TranslateLocalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn ScaleGlobalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ScaleLocalD(GW::MATH::GMATRIXD _matrix, GW::MATH::GVECTORD _vector, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn LerpD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, double _ratio, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn ProjectionDirectXLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionDirectXRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionOpenGLLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionOpenGLRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionVulkanLHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn ProjectionVulkanRHD(double _fovY, double _aspect, double _zn, double _zf, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn LookAtLHD(GW::MATH::GVECTORD _eye, GW::MATH::GVECTORD _at, GW::MATH::GVECTORD _up, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn LookAtRHD(GW::MATH::GVECTORD _eye, GW::MATH::GVECTORD _at, GW::MATH::GVECTORD _up, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn MakeRelativeD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }
			static GReturn MakeSeparateD(GW::MATH::GMATRIXD _matrix1, GW::MATH::GMATRIXD _matrix2, GW::MATH::GMATRIXD& _outMatrix) { return GReturn::FAILURE; }

			static GReturn Downgrade(GW::MATH::GMATRIXD _matrixD, GW::MATH::GMATRIXF& _outMatrixF) { return GReturn::FAILURE; }
		};
	}
}