namespace GW
{
	namespace I
	{
		class GVectorImplementation : public virtual GVectorInterface,
			private GInterfaceImplementation
		{
		public:
			GReturn Create()
			{
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			//float vector
			static GReturn AddVectorF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn SubtractVectorF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn ScaleF(MATH::GVECTORF _vector, float _scalar, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn DotF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float& _outValue)
			{
				return GReturn::FAILURE;
			}
			static GReturn CrossVector2F(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float& _outValue)
			{
				return GReturn::FAILURE;
			}
			static GReturn CrossVector3F(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn VectorXMatrixF(MATH::GVECTORF _vector, MATH::GMATRIXF _matrix, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn TransformF(MATH::GVECTORF _vector, MATH::GMATRIXF _matrix, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn MagnitudeF(MATH::GVECTORF _vector, float& _outMagnitude)
			{
				return GReturn::FAILURE;
			}
			static GReturn NormalizeF(MATH::GVECTORF _vector, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn LerpF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, float _ratio, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn SplineF(MATH::GVECTORF _vector1, MATH::GVECTORF _vector2, MATH::GVECTORF _vector3, MATH::GVECTORF _vector4, float _ratio, MATH::GVECTORF& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn Upgrade(MATH::GVECTORF _vectorF, MATH::GVECTORD& _outVectorD) 
			{
				return GReturn::FAILURE; 
			}

			//double vector
			static GReturn AddVectorD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn SubtractVectorD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn ScaleD(MATH::GVECTORD _vector, double _scalar, MATH::GVECTORD& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn DotD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double& _outValue)
			{
				return GReturn::FAILURE;
			}
			static GReturn CrossVector2D(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double& _outValue)
			{
				return GReturn::FAILURE;
			}
			static GReturn CrossVector3D(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn VectorXMatrixD(MATH::GVECTORD _vector, MATH::GMATRIXD _matrix, MATH::GVECTORD& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn TransformD(MATH::GVECTORD _vector, MATH::GMATRIXD _matrix, MATH::GVECTORD& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn MagnitudeD(MATH::GVECTORD _vector, double& _outMagnitude)
			{
				return GReturn::FAILURE;
			}
			static GReturn NormalizeD(MATH::GVECTORD _vector, MATH::GVECTORD& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn LerpD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, double _ratio, MATH::GVECTORD& _outVector)
			{
				return GReturn::FAILURE;
			}
			static GReturn SplineD(MATH::GVECTORD _vector1, MATH::GVECTORD _vector2, MATH::GVECTORD _vector3, MATH::GVECTORD _vector4, double _ratio, MATH::GVECTORD& _outVector) 
			{
				return GReturn::FAILURE;
			}
			static GReturn Downgrade(MATH::GVECTORD _vectorD, MATH::GVECTORF& _outVectorF) 
			{
				return GReturn::FAILURE; 
			}
		};
	}
}