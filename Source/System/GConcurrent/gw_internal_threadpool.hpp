#ifndef GW_INTERNAL_THREADPOOL
#define GW_INTERNAL_THREADPOOL

#include "../../Shared/ThreadPool-1/ThreadPool.h" // current implementation used for thread-pools
#include "../../../Interface/System/GSystemDefines.h"

// where Gateware keeps it's "invisible" global variables, do not modify outside of Gateware implementations
namespace internal_gw // DEVS: Only allowed on approval, favor static class members if a global is required.
{
	// for variables to be truly global across translation units they must be static WITHIN a function or class.
	// This allows us to avoid using extern and requiring definition in a user translation unit.
	static nbsdx::concurrent::ThreadPool<G_MAX_THREAD_POOL_SIZE>& GatewareThreadPool() // avoids a name collision in other units
	{
		// internally this ThreadPool has been adapted to use std::thread::hardware_concurrency() threads 
		static nbsdx::concurrent::ThreadPool<G_MAX_THREAD_POOL_SIZE> gatewareThreadPool;
		return gatewareThreadPool; // the only one we have, static inside the function ensure it is the same
	}
}
#endif 
