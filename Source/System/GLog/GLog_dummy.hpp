namespace GW
{
	namespace I
	{
		class GLogImplementation : public virtual GLogInterface
		{
		public:
			GW::GReturn Init(const char* const _fileName)
			{
				return GReturn::FAILURE;
			};

			GW::GReturn Init(GW::SYSTEM::GFile _file)
			{
				return GReturn::FAILURE;
			};

			GW::GReturn Log(const char* const _log) override
			{
				return GReturn::FAILURE;
			};

			GW::GReturn LogCategorized(const char* const _category, const char* const _log) override
			{
				return GReturn::FAILURE;
			};

			GW::GReturn EnableVerboseLogging(bool _value) override
			{
				return GReturn::FAILURE;
			};

			GW::GReturn EnableConsoleLogging(bool _value) override
			{
				return GReturn::FAILURE;
			};

			GW::GReturn Flush() override
			{
				return GReturn::FAILURE;
			};

			GW::GReturn Create(const char* const _fileName)
			{
				return GReturn::FEATURE_UNSUPPORTED;
			};

			GW::GReturn Create(GW::SYSTEM::GFile _file)
			{
				return GReturn::FEATURE_UNSUPPORTED;
			};
		};
	} //end namespace I
} // end namespace GW