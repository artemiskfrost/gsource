namespace GW
{
	namespace I
	{
		// this function is unique to the GApp
		class GAppImplementation : public GAppInterface
		{
		public:
			GReturn Create(std::function<int()> _desktopMainFunction)
			{
				// Desktop environments should not support the App execution of main
				// otherwise main would run twice.
				return GReturn::INTERFACE_UNSUPPORTED;
			}

			static GReturn PreviousExecutionStateWasTerminated(bool& _outWasTerminated)
			{
				return GReturn::INTERFACE_UNSUPPORTED;
			}
		};
	}
}