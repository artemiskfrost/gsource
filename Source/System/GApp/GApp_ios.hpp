#include <iostream>
#import <UIKit/UIKit.h>

namespace internal_gw
{
    struct GAPP_GLOBAL
    {
        std::mutex sleeper;
        std::condition_variable started;
        bool lateStart;
    };
    static GAPP_GLOBAL& GAppGlobal()
    {
        static GAPP_GLOBAL appData;
        return appData;
    }
}


namespace GW
{
    namespace I
    {
        // this function is unique to the GApp
        class GAppImplementation : public GAppInterface
        {
            GW::SYSTEM::GConcurrent run; // make global perhaps
        public:
            GReturn Create(std::function<int()> _desktopMainFunction)
            {
                GReturn result = GReturn::FAILURE;
                // ensure we have a valid main function and GConcurrent library
                if (_desktopMainFunction == nullptr || -run.Create(true))
                    return result;
                // pass below function by value
                result = run.BranchSingular([_desktopMainFunction] {
                    
                    {
                        std::unique_lock<std::mutex> uni(internal_gw::GAppGlobal().sleeper);
                        internal_gw::GAppGlobal().started.wait(uni, []{ return internal_gw::GAppGlobal().lateStart; });
                    }
                    // NOTE: may need to block main until the app launches completely.
                    // This is likely the cause of false thread exceptions caused sporadically.
                    int ret = _desktopMainFunction(); // run the unified code base
                    std::cout << "GApp: desktop main() routine returned " << ret << std::endl;
                    // send a close message so our app window know to close down
                });
                return result; // did it work?
            }
            // in case we need to debug program shutdown
            ~GAppImplementation() {    run.Converge(0); }
        };
    }
}

// This main is nearly identical to the sample main given in any of the iOS XCode templates 
int main(int argc, char * argv[])
{
    NSString * appDelegateClassName;
    @autoreleasepool
    {
        //UIApplication
        //UISceneDelegate* uisd = [UISceneDelegate mainScene];
        
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([UIResponder<UIApplicationDelegate> class]);
        
        
        
        // This code taken straight from the addObserverForName topic on Apple Developer.
        NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
        id __block token = [center addObserverForName:UIApplicationDidFinishLaunchingNotification
                                               object:nil
                                                queue:[NSOperationQueue mainQueue]
                                           usingBlock:^(NSNotification *note)
                            {
                                internal_gw::GAppGlobal().lateStart = true;
                                internal_gw::GAppGlobal().started.notify_all();
                                [center removeObserver:token];
                            }];
        
        return UIApplicationMain(argc, argv, nil, appDelegateClassName);
    }
}

// This define is here because iOS apps run off of 'standard' "int main(int, char**)"
// Due to this, end users would find that their main doesn't account for running the UIApplicationMain, which actually runs the app.
// As such we have defined main ourselves and 'overridden' the function 'main' with the following define.
// GApp's main runs first then whatever main is in the GApp constructor gets called after GApp is created.
#define main Gateware_main


