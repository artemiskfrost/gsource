#include <fstream>  //file streams
#include <string>  //strings
#include <atomic>  //atomic variables
#include <mutex>  //mutex locks
#include <stdio.h>


#include <sys/stat.h>  //File stats.
#include <string.h>
#include <cstring>

#ifndef DIR_SEPARATOR
#define G_DIR_SEPARATOR '/'
#endif

#include "../../../Interface/Core/GThreadShared.h"

namespace GW
{
	namespace I
	{
		class GFileImplementation : public virtual GW::I::GFileInterface,
			protected GThreadSharedImplementation
		{
		private:
			DIR* currDirStream;  //Maintains the current directory.
			std::fstream file;  //Maintains the current file (if one is open).
			FILE* binaryFile = NULL; //for binary read and write
			std::string currDir;  //A cached directory path for faster fetching.
			char initialDir[250];
			std::atomic<unsigned int> mode; //Used to track what open mode the file is in
			std::mutex lock; //Read/Write lock.
			unsigned int fileSize = 0;
		public:
			GFileImplementation()
			{
				currDirStream = nullptr;
			}

			~GFileImplementation()
			{
				SetCurrentWorkingDirectory(initialDir);
				//Close the current directory.
				closedir(currDirStream);
				//Close the file stream.
				CloseFile();
			}

			GReturn Create()
			{
				//Set the current working directory to the directory the program was ran from.
#if TARGET_OS_IOS || TARGET_OS_SIMULATOR
				std::string tempDir = getenv("HOME");
				tempDir += "/Library";
				GReturn rv = SetCurrentWorkingDirectory(tempDir.c_str());
				if (G_FAIL(rv))
					return rv;
#else
				GReturn rv = SetCurrentWorkingDirectory("./");
				if (G_FAIL(rv))
					return rv;
#endif

				GetCurrentWorkingDirectory(initialDir, 250);
				return GReturn::SUCCESS;
			}

			GReturn OpenBinaryRead(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is not already open.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file in the currentWorkingDirectory.
				file.open(currDir + _file, std::ios::in | std::ios::binary);

				//If the file failed to open the function fails.
				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to read
				mode = std::ios::in;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn OpenBinaryWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//If the file is currently open we fail.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + _file, std::ios::out | std::ios::binary);

				//If file failed to open we fail.
				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn AppendBinaryWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + _file, std::ios::out | std::ios::binary | std::ios::app | std::ios::ate);

				//If file failed to open we fail.
				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn OpenTextRead(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + _file, std::ios::in);

				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to read
				mode = std::ios::in;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn OpenTextWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + _file, std::ios::out);

				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn AppendTextWrite(const char* const _file) override
			{
				//Check for invalid arguments.
				if (_file == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Close the current file if there is one.
				if (file.is_open())
					return GReturn::FAILURE;

				//Open the new file.
				file.open(currDir + _file, std::ios::out | std::ios::app | std::ios::ate);

				if (!file.is_open())
					return GReturn::FILE_NOT_FOUND;

				//Set mode to write
				mode = std::ios::out;

				// get the file size
				GetFileSize(_file, fileSize);

				return GReturn::SUCCESS;
			}

			GReturn Write(const char* const _inData, unsigned int _numBytes) override
			{
				//Check for invalid arguments.
				if (_inData == nullptr || _numBytes == 0)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is open.
				if (!file.is_open() && binaryFile == NULL)
					return GReturn::FAILURE;

				//Make sure the file is opened for writing
				if (mode != std::ios::out)
					return GReturn::FAILURE;

				//Lock the write operations.
				lock.lock();

				file.write(_inData, _numBytes);

				// update the file size
				fileSize += _numBytes;

				lock.unlock();
				return GReturn::SUCCESS;
			}

			GReturn Read(char* _outData, unsigned int _numBytes) override
			{
				if (_numBytes == 0)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is open.
				if (!file.is_open() && binaryFile == NULL)
				{
					_outData = nullptr;
					return GReturn::FAILURE;
				}

				//Make sure the file is opened for reading
				if (mode != std::ios::in)
					return GReturn::FAILURE;

				if (_numBytes <= fileSize)
				{
					lock.lock();
					file.read(_outData, _numBytes);
					lock.unlock();
				}
				else
				{
					//setting the buffer size(2nd parameter) be as big as reading data size(_numBytes)
					lock.lock();
					file.read(_outData, fileSize);
					file.clear();
					lock.unlock();
					return GReturn::END_OF_FILE;
				}

				if (file.eof())
					return GReturn::END_OF_FILE;

				return GReturn::SUCCESS;
			}

			GReturn WriteLine(const char* const _inData) override
			{
				//Check for invalid arguments.
				if (_inData == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Ensure a file is open.
				if (!file.is_open())
					return GReturn::FAILURE;

				//Make sure the file is opened for writing
				if (mode != std::ios::out)
					return GReturn::FAILURE;

				//Transfer the data to a string. #defines make it so the
				//string is what we need it to be on any system we support.
				std::string writeOutString = _inData;

				//Lock the write operations.
				lock.lock();

				file << writeOutString;

				// update the file size
				fileSize += static_cast<unsigned int>(writeOutString.length());

				lock.unlock();
				return GReturn::SUCCESS;
			}

			GReturn ReadLine(char* _outData, unsigned int _outDataSize, char _delimiter) override
			{
				if (_outData == nullptr || _outDataSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Ensure file is open.
				if (!file.is_open())
					return GReturn::FAILURE;

				//Make sure the file is opened for reading
				if (mode != std::ios::in)
					return GReturn::FAILURE;

				//The string to be read into.
				std::string outString;

				//Lock the read operations.
				lock.lock();

				//Just read in data normally.
				std::getline(file, outString, _delimiter);

				if (file.eof())
				{
					file.clear(); //reset the flag

					if ((_outDataSize - 1) > fileSize)
					{
						snprintf(_outData, fileSize + 1, "%s", outString.c_str());
						lock.unlock();
						return GReturn::END_OF_FILE;
					}
				}

				//Copy the data over to the out parameter.
				snprintf(_outData, _outDataSize, "%s", outString.c_str());

				lock.unlock();

				return GReturn::SUCCESS;
			}

			GReturn CloseFile() override
			{
				//If a file is not open, we can not close it.
				if (!file.is_open() && binaryFile == NULL)
					return GReturn::FAILURE;

				if (binaryFile != NULL)
				{
					fflush(binaryFile);
					fclose(binaryFile);
					binaryFile = nullptr;
				}
				else
				{
					//Flush the file.
					file.flush();
					//Close the file.
					file.close();
				}

				// update the file size
				fileSize = 0;

				return GReturn::SUCCESS;
			}

			GReturn FlushFile() override
			{
				//If a file is not open we can not flush it.
				if (!file.is_open() && binaryFile == NULL)
					return GReturn::FAILURE;

				if (binaryFile != NULL)
				{
					fflush(binaryFile);
				}
				else
				{
					//flush the file.
					file.flush();
				}
				return GReturn::SUCCESS;
			}

			GReturn SetCurrentWorkingDirectory(const char* const _dir) override
			{
				//Check for valid arguments.
				if (_dir == nullptr)
					return GReturn::INVALID_ARGUMENT;

				//Get the absolute path.
				char buffer[PATH_MAX];
				if (realpath(_dir, buffer) == nullptr)
					return GReturn::FAILURE;

				//Ensure the directory exists.
				struct stat s;
				if (stat(buffer, &s) != 0)
					return GReturn::FILE_NOT_FOUND;

				//Assign the passed in directory to our internal directory storage.
				currDir = buffer;
				currDir += G_DIR_SEPARATOR;

				//If there is an open directory, close it.
				if (currDirStream != nullptr)
					closedir(currDirStream);

				//Open new directory.
				currDirStream = opendir(currDir.c_str());

				//Check to ensure directory is open.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				return GReturn::SUCCESS;
			}

			GReturn GetCurrentWorkingDirectory(char* _dir, unsigned int _dirSize) override
			{
				//Check for valid arguments.
				if (_dir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				//Check that a directory is open.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				snprintf(_dir, _dirSize, "%s", currDir.c_str());
				return GReturn::SUCCESS;
			}

			GReturn GetDirectorySize(unsigned int& _outSize) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				struct dirent* file;
				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				//Reset the dir size.
				_outSize = 0;

				//Get the number of files in directory.
				while ((file = readdir(currDirStream)))
				{
					if (file->d_type == DT_REG)
						++_outSize;
				}

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				return GReturn::SUCCESS;
			}

			GReturn GetSubDirectorySize(unsigned int& _outSize) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				struct dirent* subDir;
				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				//Reset the sub-dir size.
				_outSize = 0;

				while ((subDir = readdir(currDirStream)))
				{
					if (strcmp(subDir->d_name, ".\0") == 0 ||
						strcmp(subDir->d_name, "..\0") == 0)
						continue;

					if (subDir->d_type == DT_DIR)
						++_outSize;
				}

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);
				return GReturn::SUCCESS;
			}

			GReturn GetFilesFromDirectory(char* _outFiles[], unsigned int _numFiles, unsigned int _fileNameSize) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GW::GReturn::FAILURE;
				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				struct dirent* file;
				unsigned int fileIndex = 0;
				while ((file = readdir(currDirStream)) && fileIndex < _numFiles)
				{
					if (file->d_type == DT_REG)
					{
						std::string fileName(file->d_name);
						snprintf(_outFiles[fileIndex], _fileNameSize, "%s", fileName.c_str());
						++fileIndex;
					}
					else
						continue;
				}

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);
				return GReturn::SUCCESS;
			}

			GReturn GetFoldersFromDirectory(unsigned int _numsubDir, unsigned int _subDirNameSize, char* _outsubDir[]) override
			{
				//Check that there is a current working directory.
				if (currDirStream == nullptr)
					return GReturn::FAILURE;

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);

				struct dirent* subDir;
				unsigned int subDirIndex = 0;

				while ((subDir = readdir(currDirStream)) && subDirIndex < _numsubDir)
				{

					if (strcmp(subDir->d_name, ".\0") == 0 ||
						strcmp(subDir->d_name, "..\0") == 0)
						continue;

					if (subDir->d_type == DT_DIR)
					{
						std::string subDirName(subDir->d_name);
						snprintf(_outsubDir[subDirIndex], _subDirNameSize, "%s", subDirName.c_str());
						++subDirIndex;
					}
					else
						continue;
				}

				//Set the directory iterator back to the beginning.
				rewinddir(currDirStream);
				return GReturn::SUCCESS;
			}

			GReturn GetFileSize(const char* const _file, unsigned int& _outSize) override
			{
				//Make a full path to the file.
				std::string filePath = currDir;
				filePath += _file;

				struct stat s;
				if (stat(filePath.c_str(), &s) != 0)
					return GReturn::FILE_NOT_FOUND;

				//Copy the file size to the out parameter.
				_outSize = (unsigned int)s.st_size;

				return GReturn::SUCCESS;
			}

			GReturn GetInstallFolder(unsigned int _dirSize, char* _outDir) override //IOS: APPNAME.APP - Location of the installed app on the current device, used for items such as app resources 
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;
				//char* retVal = "./";

				SetCurrentWorkingDirectory("./");

				/*//Copy the directory to the out parameter
				INTERNAL::strcpy_s(_dir, _dirSize, retVal);*/

				return GetCurrentWorkingDirectory(_outDir, _dirSize);
			}

			GReturn GetSaveFolder(unsigned int _dirSize, char* _outDir) override // UWP: LOCAL //IOS: DOCUMENTS - Data that exists on the current device and is backed up in the cloud 
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				const char* retVal = "./save";

				//Copy the directory to the out parameter
				snprintf(_outDir, _dirSize, "%s", retVal);

				return GReturn::SUCCESS;
			}

			GReturn GetPreferencesFolder(unsigned int _dirSize, char* _outDir) override // UWP: ROAMING //IOS: LIBRARY - Data that exists on all devices on which the user has installed the app 
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				const char* retVal = "./preferences";

				//Copy the directory to the out parameter
				snprintf(_outDir, _dirSize, "%s", retVal);

				return GReturn::SUCCESS;
			}

			GReturn GetTempFolder(unsigned int _dirSize, char* _outDir) override // UWP: TEMPORARY //IOS: TEMP - Data that could be removed by the system at any time 
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				const char* retVal = "./temp";

				//Copy the directory to the out parameter
				snprintf(_outDir, _dirSize, "%s", retVal);

				return GReturn::SUCCESS;
			}

			GReturn GetCacheFolder(unsigned int _dirSize, char* _outDir) override // UWP: LOCALCACHE //IOS: LIBRARY/CACHES - Persistant data that exists only on the current device 
			{
				//Check for valid arguments.
				if (_outDir == nullptr || _dirSize == 0)
					return GReturn::INVALID_ARGUMENT;

				const char* retVal = "./cache";

				//Copy the directory to the out parameter
				snprintf(_outDir, _dirSize, "%s", retVal);

				return GReturn::SUCCESS;
			}

			GReturn Seek(unsigned int _seekFrom, int _amount, unsigned int& _outCurrPos) override {
				// if text file
				if (file.is_open()) {
					// if not seeking from the current position
					if (_seekFrom != -1) {
						// check for invalid arguments
						// lock
						lock.lock();
						if (0 <= _seekFrom + _amount && _seekFrom + _amount < fileSize) {
							// seek to the new position relative to _seekFrom
							std::streamoff offset = static_cast<unsigned long long>(_seekFrom) + static_cast<long long>(_amount);
							file.seekg(offset, std::ios_base::beg);

							// set the output to the new current position
							_outCurrPos = static_cast<unsigned int>(file.tellg());

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						}
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}
					else { // seeking from the current position
						// check for invalid arguments
						// lock
						lock.lock();

						unsigned int pos = static_cast<unsigned int>(file.tellg());
						if (0 <= pos + _amount && pos + _amount < fileSize) {
							// seek to the new position relative to the current position
							file.seekg(_amount, std::ios_base::cur);

							// set the output to the new current position
							_outCurrPos = static_cast<unsigned int>(file.tellg());

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						}
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}

				}
				// if binary file
				else if (binaryFile != NULL) {
					// check where we seek from
					if (_seekFrom != -1) {
						// check for invalid arguments
						// lock
						lock.lock();
						if (0 <= _seekFrom + _amount && _seekFrom + _amount < fileSize) {
							// seek to the new position relative to _seekFrom
							fseek(binaryFile, static_cast<unsigned long>(_seekFrom) + static_cast<long>(_amount), SEEK_SET);

							// set the output to the new current position
							fpos_t pos;
							fgetpos(binaryFile, &pos);
							_outCurrPos = static_cast<unsigned int>(pos.__pos);

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						}
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}
					else { // seek from the current position
						// check for invalid arguments
						// lock
						lock.lock();
						fpos_t pos;
						fgetpos(binaryFile, &pos);
						if (0 <= static_cast<unsigned int>(pos.__pos) + _amount && static_cast<unsigned int>(pos.__pos) + _amount < fileSize) {
							// seek to the new position relative to the current position
							fseek(binaryFile, _amount, SEEK_CUR);

							// set the output to the new current position
							fgetpos(binaryFile, &pos);
							_outCurrPos = static_cast<unsigned int>(pos.__pos);

							// unlock
							lock.unlock();

							return GReturn::SUCCESS;
						}
						else {
							// unlock
							lock.unlock();
							return GReturn::INVALID_ARGUMENT;
						}
					}
				}
				else return GReturn::FILE_NOT_FOUND;
			}
		};
	}//end namespace I
}//end namespace GW

#undef DIR_SEPARATOR
