namespace GW
{
	namespace I
	{
		class GWindowImplementation :	public virtual GWindowInterface,
										public GEventGeneratorImplementation
		{
		public:
			GReturn Create(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
			{
				return GReturn::FAILURE;
			}

			GReturn ProcessWindowEvents() override
			{
				return GReturn::FAILURE;
			}

			GReturn ReconfigureWindow(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style) override
			{
				return GReturn::FAILURE;
			}

			GReturn SetWindowName(const char* _newName) override
			{
				return GReturn::FAILURE;
			}

			GReturn SetIcon(int _width, int _height, const unsigned int* _argbPixels) override
			{
				return GReturn::FAILURE;
			}

			GReturn MoveWindow(int _x, int _y) override
			{
				return GReturn::FAILURE;
			}

			GReturn ResizeWindow(int _width, int _height) override
			{
				return GReturn::FAILURE;
			}

			GReturn Maximize() override
			{
				return GReturn::FAILURE;
			}

			GReturn Minimize() override
			{
				return GReturn::FAILURE;
			}

			GReturn ChangeWindowStyle(SYSTEM::GWindowStyle _style) override
			{
				return GReturn::FAILURE;
			}

			GReturn GetWidth(unsigned int& _outWidth) const override
			{
				return GReturn::FAILURE;
			}

			GReturn GetHeight(unsigned int& _outHeight) const override
			{
				return GReturn::FAILURE;
			}

			GReturn GetClientWidth(unsigned int& _outClientWidth) const override
			{
				return GReturn::FAILURE;
			}

			GReturn GetClientHeight(unsigned int& _outClientHeight) const override
			{
				return GReturn::FAILURE;
			}

			GReturn GetX(unsigned int& _outX) const override
			{
				return GReturn::FAILURE;
			}

			GReturn GetY(unsigned int& _outY) const override
			{
				return GReturn::FAILURE;
			}

			GReturn GetClientTopLeft(unsigned int& _outX, unsigned int& _outY) const override
			{
				return GReturn::FAILURE;
			}

			GReturn GetWindowHandle(SYSTEM::UNIVERSAL_WINDOW_HANDLE & _outUniversalWindowHandle) const override
			{
				return GReturn::FAILURE;
			}

			GReturn IsFullscreen(bool& _outIsFullscreen) const override
			{
				return GReturn::FAILURE;
			}
			
			GReturn IsFocus(bool& _outIsFocus) const override
			{
				return GReturn::FAILURE;
			}
			
			/*GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override
			{
				return GReturn::FAILURE;
			}

			GReturn Observers(unsigned int& _outCount) const override
			{
				return GReturn::FAILURE;
			}

			GReturn Push(const GEvent & _newEvent) override
			{
				return GReturn::FAILURE;
			}*/
		};
	}
}
