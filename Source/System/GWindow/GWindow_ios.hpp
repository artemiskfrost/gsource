#ifdef __OBJC__
@import Foundation;
@import UIKit;
#endif

#include <thread>
#include <string.h>
#include "../../Shared/uwputils.h"
#include "../../Shared/iosutils.h"



namespace GW
{
    namespace I
    {
        class GWindowImplementation : public virtual GWindowInterface,
            private GEventGeneratorImplementation
        {
        private:
            
            __block GWindowImplementation* selfReference;
            __block GEvent gEvent;
            GW::I::GWindowInterface::EVENT_DATA eventData;

            __block UIViewController* currentWindowViewController;
            __block UIWindow* window;
            
            // Needed for unsubscribing from events later.
            NSMutableArray<id>* tokens;
            unsigned int tokenCount;
            
            GReturn SetupNotifications()
            {
                NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
                tokens = [NSMutableArray<id> array];
                tokenCount = 0;
                // Event Support
                // SUSPEND,             YES
                // RESUME,              YES
                // LEAVING_BACKGROUND,  YES
                // ENTERED_BACKGROUND,  YES
                // NOTIFY,              NO  (Used in the graphics surfaces.)
                // NON_EVENT,           NO  (Not an event.)
                // MINIMIZE,            NO  (Would be implemented with UIWindowDidBecomeHiddenNotification.)
                // MAXIMIZE,            NO  (Would be implemented with UIWindowDidBecomeVisibleNotification.)
                // RESIZE,              NO  (As far as I know, this is not possible with the NSNotificationCenter system.)
                // RESIZE,              NO  (As far as I know, this is not possible with the NSNotificationCenter system.)
                // DISPLAY_CLOSED,      NO  (Linux only)
                // EVENTS_PROCESSED,    YES (Just to keep parity, doesn't mean anything in iOS.)
                // DESTROY,             YES
                
                
                // SUSPEND Event
                tokenCount++;
                [tokens addObject: [notificationCenter addObserverForName:UIApplicationWillResignActiveNotification
                                                object:nil
                                                 queue:[NSOperationQueue mainQueue]
                                            usingBlock:^(NSNotification * note) {
                 
                 // This notification sends your UIApplication object with it.
                 UIApplication* application = note.object;
                 
                 // Nabs the top-most window which for most users will be the only window,
                 // and for most other users will be the only window they care about.
                 UIWindow* window = [[application windows] lastObject];
                 
                 GW::I::GWindowInterface::EVENT_DATA eventData;
                 eventData.eventFlags = GW::I::GWindowInterface::Events::SUSPEND;
                 eventData.windowX = window.bounds.origin.x;
                 eventData.windowY = window.bounds.origin.y;
                 eventData.width   = window.bounds.size.width;
                 eventData.height  = window.bounds.size.height;
                 eventData.windowHandle = (__bridge void*) window;
                 
                 gEvent.Write(eventData.eventFlags, eventData);
                 selfReference->Push(gEvent);
                 }]];
                
                // RESUME Event
                tokenCount++;
                [tokens addObject: [notificationCenter addObserverForName:UIApplicationDidBecomeActiveNotification
                                                object:nil
                                                 queue:[NSOperationQueue mainQueue]
                                            usingBlock:^(NSNotification * note) {
                 
                 // This notification sends your UIApplication object with it.
                 UIApplication* application = note.object;
                 
                 // Nabs the top-most window which for most users will be the only window,
                 // and for most other users will be the only window they care about.
                 UIWindow* window = [[application windows] lastObject];
                 
                 GW::I::GWindowInterface::EVENT_DATA eventData;
                 eventData.eventFlags = GW::I::GWindowInterface::Events::RESUME;
                 eventData.windowX = window.bounds.origin.x;
                 eventData.windowY = window.bounds.origin.y;
                 eventData.width   = window.bounds.size.width;
                 eventData.height  = window.bounds.size.height;
                 eventData.windowHandle = (__bridge void*) window;
                 
                 gEvent.Write(eventData.eventFlags, eventData);
                 selfReference->Push(gEvent);
                 }]];
                
                
                // LEAVING_BACKGROUND Event
                tokenCount++;
                [tokens addObject: [notificationCenter addObserverForName:UIApplicationWillEnterForegroundNotification
                                                object:nil
                                                 queue:[NSOperationQueue mainQueue]
                                            usingBlock:^(NSNotification * note) {
                 
                 // This notification sends your UIApplication object with it.
                 UIApplication* application = note.object;
                 
                 // Nabs the top-most window which for most users will be the only window,
                 // and for most other users will be the only window they care about.
                 UIWindow* window = [[application windows] lastObject];
                 
                 GW::I::GWindowInterface::EVENT_DATA eventData;
                 eventData.eventFlags = GW::I::GWindowInterface::Events::LEAVING_BACKGROUND;
                 eventData.windowX = window.bounds.origin.x;
                 eventData.windowY = window.bounds.origin.y;
                 eventData.width   = window.bounds.size.width;
                 eventData.height  = window.bounds.size.height;
                 eventData.windowHandle = (__bridge void*) window;
                 
                 gEvent.Write(eventData.eventFlags, eventData);
                 selfReference->Push(gEvent);
                 }]];
                
                // ENTERED_BACKGROUND Event
                tokenCount++;
                [tokens addObject: [notificationCenter addObserverForName:UIApplicationDidEnterBackgroundNotification
                                                object:nil
                                                 queue:[NSOperationQueue mainQueue]
                                            usingBlock:^(NSNotification * note) {
                 
                 // This notification sends your UIApplication object with it.
                 UIApplication* application = note.object;
                 
                 // Nabs the top-most window which for most users will be the only window,
                 // and for most other users will be the only window they care about.
                 UIWindow* window = [[application windows] lastObject];
                 
                 GW::I::GWindowInterface::EVENT_DATA eventData;
                 eventData.eventFlags = GW::I::GWindowInterface::Events::ENTERED_BACKGROUND;
                 eventData.windowX = window.bounds.origin.x;
                 eventData.windowY = window.bounds.origin.y;
                 eventData.width   = window.bounds.size.width;
                 eventData.height  = window.bounds.size.height;
                 eventData.windowHandle = (__bridge void*) window;
                 
                 gEvent.Write(eventData.eventFlags, eventData);
                 selfReference->Push(gEvent);
                 }]];
                
                
                // DESTROY Event
                // This likely won't be called. Usually SUSPEND will be called.
                tokenCount++;
                [tokens addObject: [notificationCenter addObserverForName:UIApplicationWillTerminateNotification
                                                object:nil
                                                 queue:[NSOperationQueue mainQueue]
                                            usingBlock:^(NSNotification * note) {
                 // This notification sends your UIApplication object with it.
                 UIApplication* application = note.object;
                 
                 // Nabs the top-most window which for most users will be the only window,
                 // and for most other users will be the only window they care about.
                 UIWindow* window = [[application windows] lastObject];
                                    
                 GW::I::GWindowInterface::EVENT_DATA eventData;
                 eventData.eventFlags = GW::I::GWindowInterface::Events::DESTROY;
                 eventData.windowX = window.bounds.origin.x;
                 eventData.windowY = window.bounds.origin.y;
                 eventData.width   = window.bounds.size.width;
                 eventData.height  = window.bounds.size.height;
                 eventData.windowHandle = (__bridge void*) window;
                 
                 gEvent.Write(eventData.eventFlags, eventData);
                 selfReference->Push(gEvent);
                 }]];
                
                return GReturn::SUCCESS;
            }

        public:
            ~GWindowImplementation()
            {
                // Our UIViewController pointer can just be released here.
                // No dynamic memory should be held by this class.
                if (currentWindowViewController)
                {
                    currentWindowViewController = nil;
                }
                
                // Unsubscribe from all of our notifications
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                for (unsigned int i = 0; i < tokenCount; ++i)
                {
                    [nc removeObserver: [tokens objectAtIndex: i]];
                }
            }

            GReturn Create(int _x, int _y, int _width, int _height, SYSTEM::GWindowStyle _style)
            {
                // iOS doesn't use any of the parameters, the app window is always fullscreen.
                
                // This saves the current window, which is usually the only window on iOS, unless the end-user
                // creates another, in which GWindow will get the top-most window.
                // If the user wishes to get another window with GWindow, they would have to set the window they want
                // as the top window and call Create on their GWindow instance.
                RUN_ON_UI_THREAD(^{
                    
                    currentWindowViewController = [[[[UIApplication sharedApplication] windows] lastObject] rootViewController];
                    window = [[currentWindowViewController view] window];
                    
                });
                // For use within blocks.
                selfReference = this;
                return SetupNotifications();
            }

            GReturn ProcessWindowEvents() override
            {
                // Return Failure if the app as terminated
                if (eventData.eventFlags == Events::DESTROY)
                    return GReturn::FAILURE;
                
                eventData.eventFlags = Events::EVENTS_PROCESSED;
                gEvent.Write(eventData.eventFlags, eventData);
                Push(gEvent);

                // Always return success to keep parity.
                return GReturn::SUCCESS;
            }

            GReturn ReconfigureWindow(int _x, int _y, int _width, int _height, GW::SYSTEM::GWindowStyle _style) override
            {
                // Although this could be done, we choose not to support this at the moment.
                // Most mobile apps and games are fullscreen and handling many views with different sizes and automatic layouts
                // will probably be out of the scope of many devs making games. As such we don't support reconfig-ing windows.
                
                return GReturn::FEATURE_UNSUPPORTED;
            }

            GReturn SetWindowName(const char* _newName) override
            {
                // Renaming a window is only available on Mac Catalyst, which we don't support at this time.
                return GReturn::FEATURE_UNSUPPORTED;
            }

			GReturn SetIcon(int _width, int _height, const unsigned int* _argbPixels) override
            {
                // This must be done in the app bundle at compile time.
                
                // [[UIApplication sharedApplication] setAlternateIconName: NSString* name
                //                                       completionHandler: ((^)(NSError* error)) completionHandler]
                // This icon must be in the application bundle, but if you have multiple then you can switch between them at runtime.
                // This probably won't be supported for a while (if at all...) but the information is here.
                
                return GReturn::FEATURE_UNSUPPORTED;
            }

            GReturn MoveWindow(int _x, int _y) override
            {
                // See Reconfigure Window.
                return GReturn::FEATURE_UNSUPPORTED;
            }

            GReturn ResizeWindow(int _width, int _height) override
            {
                // See Reconfigure Window.
                return GReturn::FEATURE_UNSUPPORTED;
            }

            GReturn Maximize() override
            {
                // See Reconfigure Window.
                return GReturn::FEATURE_UNSUPPORTED;
            }

            GReturn Minimize() override
            {
                // See Reconfigure Window.
                return GReturn::FEATURE_UNSUPPORTED;
            }

            GReturn ChangeWindowStyle(GW::SYSTEM::GWindowStyle _style) override
            {
                // See Reconfigure Window.
                return GReturn::FEATURE_UNSUPPORTED;
            }

            GReturn GetWidth(unsigned int& _outWidth) const override
            {
                // Get the width of the current window.
                __block CGRect screenBounds;
                
                RUN_ON_UI_THREAD(^{
                    
                    // Gets the scaling applied to the screen on retina devices.
                    CGRect scaledBounds = [[currentWindowViewController view] bounds];
                    scaledBounds.size.width  *= [[currentWindowViewController view] contentScaleFactor];
                    screenBounds = scaledBounds;
                    
                });
                
                _outWidth = screenBounds.size.width;
                
                return GReturn::SUCCESS;
            }

            GReturn GetHeight(unsigned int& _outHeight) const override
            {
                // Get the height of the current window.
                __block CGRect screenBounds;
                
                RUN_ON_UI_THREAD(^{
                    
                    // Gets the scaling applied to the screen on retina devices.
                    CGRect scaledBounds = [[currentWindowViewController view] bounds];
                    scaledBounds.size.height  *= [[currentWindowViewController view] contentScaleFactor];
                    screenBounds = scaledBounds;
                    
                });
                
                _outHeight = screenBounds.size.height;
                
                return GReturn::SUCCESS;
            }

            GReturn GetClientWidth(unsigned int& _outClientWidth) const override
            {
                // This usually returned the safe area, but for graphics surfaces this would be preferred on desktop, as such we will make
                // it have parity with desktop platforms. May need to have "safe area" or "ui area" function in the future for displays
                // that are not perfectly rectangular, like modern phone screens.
                
                // Gets the width of the current window.
                GetWidth(_outClientWidth);
                
                // This is the code that would have the safe area insets applied.
                // Get the insets on the current view.
                //__block UIEdgeInsets insets;
                //RUN_ON_UI_THREAD(^{
                    
                //    insets = [[currentWindowViewController view] safeAreaInsets];
                    
                //});
                
                // Subtracts the insets from the window width.
                //_outClientWidth -= (insets.right + insets.left);
                
                return GReturn::SUCCESS;
            }

            GReturn GetClientHeight(unsigned int& _outClientHeight) const override
            {
                // This usually returned the safe area, but for graphics surfaces this would be preferred on desktop, as such we will make
                // it have parity with desktop platforms. May need to have "safe area" or "ui area" function in the future for displays
                // that are not perfectly rectangular, like modern phone screens.
                
                // Gets the height of the current window.
                GetWidth(_outClientHeight);
                
                // This is the code that would have the safe area insets applied.
                // Get the insets on the current view.
                //__block UIEdgeInsets insets;
                //RUN_ON_UI_THREAD(^{
                    
                //    insets = [[currentWindowViewController view] safeAreaInsets];
                    
                //});
                
                // Subtracts the insets from the window height.
                //_outClientHeight -= (insets.top + insets.bottom);
                
                return GReturn::SUCCESS;
            }

            GReturn GetX(unsigned int& _outX) const override
            {
                // Gets the origin of the topmost window.
                __block CGRect screenBounds;
                
                RUN_ON_UI_THREAD(^{
                    
                    screenBounds = [window bounds];
                    
                });
                
                _outX = screenBounds.origin.x;

                return GReturn::SUCCESS;
            }

            GReturn GetY(unsigned int& _outY) const override
            {
                // Gets the origin of the topmost window.
                __block CGRect screenBounds;
                
                RUN_ON_UI_THREAD(^{
                    
                    screenBounds = [window bounds];
                    
                });
                
                _outY = screenBounds.origin.y;

                return GReturn::SUCCESS;
            }

            GReturn GetClientTopLeft(unsigned int& _outX, unsigned int& _outY) const override
            {
                // This usually returned the safe area, but for graphics surfaces this would be preferred on desktop, as such we will make
                // it have parity with desktop platforms. May need to have "safe area" or "ui area" function in the future for displays
                // that are not perfectly rectangular, like modern phone screens.
                
                // Gets the top-left of the safe area, which may not be the printable area on iOS,
                // depends on what iPhone you are running this on.
                //__block UIEdgeInsets insets;
                //RUN_ON_UI_THREAD(^{
                    
                //    insets = [[currentWindowViewController view] safeAreaInsets];
                    
                //});
                
                // Get the top left window coordinates.
                GetX(_outX);
                GetY(_outY);
                
                // Add the corresponding coordinates for the safe area.
                //_outX += insets.left;
                //_outY += insets.top;
                
                return GReturn::SUCCESS;
            }

            GReturn GetWindowHandle(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& _outUniversalWindowHandle) const override
            {
                if (!window)
                {
                    return GReturn::FAILURE;
                }
                _outUniversalWindowHandle.window = (__bridge void *) window;
                return GReturn::SUCCESS;
            }

            GReturn IsFullscreen(bool& _outIsFullscreen) const override
            {
                // //This could be written like
                // if (windowSize < physicalScreenSize)
                //  return isFullscreen = false;
                // //But this isn't really ever used in Gateware as we make every view fullscreen.
                
                // We are never not fullscreen.
                _outIsFullscreen = TRUE;
                return GReturn::SUCCESS;
            }

            GReturn IsFocus(bool& _outIsFocus) const override
            {
                // If the app is open then it is he focus.
                // Also iOS filters input anyway.
                // Needs more testing.
                _outIsFocus = true;
                return GReturn::SUCCESS;
            }

            GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override
            {
                return GEventGeneratorImplementation::Register(_observer, _callback);
            }

            GReturn Observers(unsigned int& _outCount) const override
            {
                return GEventGeneratorImplementation::Observers(_outCount);
            }

            GReturn Push(const GEvent& _newEvent) override
            {
                return GEventGeneratorImplementation::Push(_newEvent);
            }
        };
    }
}
