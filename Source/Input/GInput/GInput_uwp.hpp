#include <Windows.h>
#include <vector>

#include <winrt/Windows.UI.Core.h>
#include <winrt/Windows.UI.Input.h>
#include <winrt/Windows.Foundation.h>
#include <winrt/Windows.ApplicationModel.Core.h>
#include <winrt/Windows.UI.ViewManagement.h>
#include "../../../Interface/Core/GEventReceiver.h"
#include <iostream>

namespace internal_gw {
	struct GINPUT_GLOBAL {
		int mousePrevX;
		int mousePrevY;
		int mousePositionX;
		int mousePositionY;
		int mouseDeltaX;
		int mouseDeltaY;
		unsigned int keyMask;

		unsigned int mouseReadCount;
		unsigned int mouseWriteCount;
		unsigned int scrollUpWriteCount;
		unsigned int scrollUpReadCount;
		unsigned int scrollDownWriteCount;
		unsigned int scrollDownReadCount;
		LONG_PTR _userWinProc;

		unsigned int n_Keys[256];
	};
	inline GINPUT_GLOBAL& GInputGlobal() {
		static GINPUT_GLOBAL keyData;
		return keyData;
	}
}

namespace GW {
	namespace I {
		class GInputImplementation : public virtual GInputInterface,
			private GEventGeneratorImplementation {
		private:
			CORE::GThreadShared threadShare;
			winrt::event_token m_keyDown;
			winrt::event_token m_keyUp;
			winrt::event_token m_pointerMoved;
			winrt::event_token m_pointerPressed;
			winrt::event_token m_pointerReleased;
			winrt::event_token m_pointerWheelChanged;

			// This should only be used if there is only one view
			winrt::Windows::Foundation::IAsyncAction CallOnMainViewUiThreadAsync(winrt::Windows::UI::Core::DispatchedHandler handler) const
			{
				co_await winrt::Windows::ApplicationModel::Core::CoreApplication::MainView().CoreWindow().Dispatcher().RunAsync(winrt::Windows::UI::Core::CoreDispatcherPriority::Normal, handler);
			}

		private:
			void OnKeyDown(winrt::Windows::UI::Core::KeyEventArgs const& _args) {
				USHORT makeCode = (USHORT)_args.VirtualKey();
				unsigned int gKey = 0;

				gKey = Keycodes[makeCode];

				internal_gw::GInputGlobal().n_Keys[gKey] = 1;

				switch (gKey) {
				case G_KEY_RIGHTSHIFT:
				case G_KEY_LEFTSHIFT:
					G_TURNON_BIT(internal_gw::GInputGlobal().keyMask, G_MASK_SHIFT);
					break;
				case G_KEY_RIGHTCONTROL:
				case G_KEY_LEFTCONTROL:
					G_TURNON_BIT(internal_gw::GInputGlobal().keyMask, G_MASK_CONTROL);
					break;
				case G_KEY_CAPSLOCK:
					G_TOGGLE_BIT(internal_gw::GInputGlobal().keyMask, G_MASK_CAPS_LOCK);
					break;
				case G_KEY_NUMLOCK:
					G_TOGGLE_BIT(internal_gw::GInputGlobal().keyMask, G_MASK_NUM_LOCK);
					break;
				case G_KEY_SCROLL_LOCK:
					G_TOGGLE_BIT(internal_gw::GInputGlobal().keyMask, G_MASK_SCROLL_LOCK);
					break;
				}
			}

			void OnKeyUp(winrt::Windows::UI::Core::KeyEventArgs const& _args) {
				USHORT makeCode = (USHORT)_args.VirtualKey();
				unsigned int gKey = 0;

				gKey = Keycodes[makeCode];

				internal_gw::GInputGlobal().n_Keys[gKey] = 0;

				switch (gKey) {
				case G_KEY_RIGHTSHIFT:
				case G_KEY_LEFTSHIFT:
					G_TURNOFF_BIT(internal_gw::GInputGlobal().keyMask, G_MASK_SHIFT);
					break;
				case G_KEY_RIGHTCONTROL:
				case G_KEY_LEFTCONTROL:
					G_TURNOFF_BIT(internal_gw::GInputGlobal().keyMask, G_MASK_CONTROL);
					break;
				}
			}

			void OnMouseMoved(winrt::Windows::UI::Core::PointerEventArgs const& _args) {
				++internal_gw::GInputGlobal().mouseWriteCount;
				//update delta mouse position
				if ((internal_gw::GInputGlobal().mouseWriteCount - internal_gw::GInputGlobal().mouseReadCount) <= 1) {
					internal_gw::GInputGlobal().mousePrevX = static_cast<int>(internal_gw::GInputGlobal().mousePositionX);
					internal_gw::GInputGlobal().mousePrevY = static_cast<int>(internal_gw::GInputGlobal().mousePositionY);
				}				

				auto p = _args.CurrentPoint().RawPosition();
				internal_gw::GInputGlobal().mousePositionX = static_cast<int>(p.X);
				internal_gw::GInputGlobal().mousePositionY = static_cast<int>(p.Y);

				internal_gw::GInputGlobal().mouseDeltaX = static_cast<int>(internal_gw::GInputGlobal().mousePositionX - internal_gw::GInputGlobal().mousePrevX);
				internal_gw::GInputGlobal().mouseDeltaY = static_cast<int>(internal_gw::GInputGlobal().mousePositionY - internal_gw::GInputGlobal().mousePrevY);

				//std::cout << internal_gw::GInputGlobal().mouseDeltaX << "--" << internal_gw::GInputGlobal().mouseDeltaY << std::endl;
			}

			void OnMousePressed(winrt::Windows::UI::Core::PointerEventArgs const& _args) {
				if (_args.CurrentPoint().Properties().IsLeftButtonPressed())
					internal_gw::GInputGlobal().n_Keys[G_BUTTON_LEFT] = 1;
				if (_args.CurrentPoint().Properties().IsRightButtonPressed())
					internal_gw::GInputGlobal().n_Keys[G_BUTTON_RIGHT] = 1;
				if (_args.CurrentPoint().Properties().IsMiddleButtonPressed())
					internal_gw::GInputGlobal().n_Keys[G_BUTTON_MIDDLE] = 1;

			}

			void OnMouseReleased(winrt::Windows::UI::Core::PointerEventArgs const& _args) {
				if (!_args.CurrentPoint().Properties().IsLeftButtonPressed())
					internal_gw::GInputGlobal().n_Keys[G_BUTTON_LEFT] = 0;
				if (!_args.CurrentPoint().Properties().IsRightButtonPressed())
					internal_gw::GInputGlobal().n_Keys[G_BUTTON_RIGHT] = 0;
				if (!_args.CurrentPoint().Properties().IsMiddleButtonPressed())
					internal_gw::GInputGlobal().n_Keys[G_BUTTON_MIDDLE] = 0;
			}

			void OnMouseScroll(winrt::Windows::UI::Core::PointerEventArgs const& _args) {
				unsigned int gKey;
				int scrollDelta = _args.CurrentPoint().Properties().MouseWheelDelta();
				if (scrollDelta > 0) {
					gKey = G_MOUSE_SCROLL_UP;
					++internal_gw::GInputGlobal().scrollUpWriteCount;
				}
				else if (scrollDelta < 0) {
					gKey = G_MOUSE_SCROLL_DOWN;
					++internal_gw::GInputGlobal().scrollDownWriteCount;
				}

				internal_gw::GInputGlobal().n_Keys[gKey] = 1;

				if (gKey != G_MOUSE_SCROLL_UP) {
					internal_gw::GInputGlobal().n_Keys[G_MOUSE_SCROLL_UP] = 0;
					internal_gw::GInputGlobal().scrollUpReadCount = internal_gw::GInputGlobal().scrollUpWriteCount;
				}
				if (gKey != G_MOUSE_SCROLL_DOWN) {
					internal_gw::GInputGlobal().n_Keys[G_MOUSE_SCROLL_DOWN] = 0;
					internal_gw::GInputGlobal().scrollDownReadCount = internal_gw::GInputGlobal().scrollDownWriteCount;
				}
			}


		public:
			~GInputImplementation() {
				// revoke events
				auto process = CallOnMainViewUiThreadAsync([this]()
					{
						auto window = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();

						window.KeyDown(m_keyDown);
						window.KeyUp(m_keyUp);
						window.PointerMoved(m_pointerMoved);
						window.PointerPressed(m_pointerPressed);
						window.PointerReleased(m_pointerReleased);
						window.PointerWheelChanged(m_pointerWheelChanged);
					}); process.get();
			}

			GReturn Push(const GEvent& _newEvent) override { return GEventGeneratorImplementation::Push(_newEvent); }

			// GWindow - one version
			// UNIVERSAL_WINDOW_HANDLE by value - another version
			GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _windowHandle) {
				if (_windowHandle.window == nullptr)
					return GW::GReturn::INVALID_ARGUMENT;

				// subscribe events to delegates
				// call on main thread
				auto process = CallOnMainViewUiThreadAsync([this]() mutable
					{
						auto view = winrt::Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
						auto window = winrt::Windows::UI::Core::CoreWindow::GetForCurrentThread();

						// Event::KEYDOWN
						this->m_keyDown = window.KeyDown([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::KeyEventArgs const& args)
							{
								this->OnKeyDown(args);
							});

						// Event::KEYUP
						this->m_keyUp = window.KeyUp([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::KeyEventArgs const& args)
							{
								this->OnKeyUp(args);
							});

						// Event::MOUSEMOVE
						this->m_pointerMoved = window.PointerMoved([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::PointerEventArgs const& args)
							{
								this->OnMouseMoved(args);
							});
						// Event::MOUSEPRESSED
						this->m_pointerPressed = window.PointerPressed([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::PointerEventArgs const& args)
							{
								this->OnMousePressed(args);
							});
						// Event::MOUSERELEASED
						this->m_pointerReleased = window.PointerReleased([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::PointerEventArgs const& args)
							{
								this->OnMouseReleased(args);
							});
						// Event::MOUSESCROLLED
						this->m_pointerWheelChanged = window.PointerWheelChanged([this]
						(winrt::Windows::UI::Core::CoreWindow const&,
							winrt::Windows::UI::Core::PointerEventArgs const& args)
							{
								this->OnMouseScroll(args);
							});
					}); process.get();

					return GReturn::SUCCESS;
			}

			GReturn Create(const GW::SYSTEM::GWindow _gWindow) {
				GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE uwHndl;

				if (-_gWindow.GetWindowHandle(uwHndl))
					return GW::GReturn::FAILURE;

				return Create(uwHndl);
			}

			GReturn GetState(int _keyCode, float& _outState) override {
				if (_keyCode == G_MOUSE_SCROLL_UP && internal_gw::GInputGlobal().n_Keys[G_MOUSE_SCROLL_UP] == 1) {
					// Compare writes to the SCROLL_UP state by GWinProc to reads by the user.
					if (internal_gw::GInputGlobal().scrollUpWriteCount != internal_gw::GInputGlobal().scrollUpReadCount)
						++internal_gw::GInputGlobal().scrollUpReadCount;
					else
						internal_gw::GInputGlobal().n_Keys[G_MOUSE_SCROLL_UP] = 0; // Prevents over-reporting scroll up.
				}
				else if (_keyCode == G_MOUSE_SCROLL_DOWN && internal_gw::GInputGlobal().n_Keys[G_MOUSE_SCROLL_DOWN] == 1) {
					if (internal_gw::GInputGlobal().scrollDownWriteCount != internal_gw::GInputGlobal().scrollDownReadCount)
						++internal_gw::GInputGlobal().scrollDownReadCount;
					else
						internal_gw::GInputGlobal().n_Keys[G_MOUSE_SCROLL_DOWN] = 0;
				}

				_outState = (float)internal_gw::GInputGlobal().n_Keys[_keyCode];
				return GW::GReturn::SUCCESS;
			}

			GReturn GetMouseDelta(float& _x, float& _y) override {
				_x = static_cast<float>(internal_gw::GInputGlobal().mouseDeltaX);
				_y = static_cast<float>(internal_gw::GInputGlobal().mouseDeltaY);
				if (internal_gw::GInputGlobal().mouseReadCount != internal_gw::GInputGlobal().mouseWriteCount) {
					internal_gw::GInputGlobal().mouseReadCount = internal_gw::GInputGlobal().mouseWriteCount;
					return GW::GReturn::SUCCESS;
				}
				return GW::GReturn::REDUNDANT;
			}

			GReturn GetMousePosition(float& _x, float& _y) const override {
				_x = static_cast<float>(internal_gw::GInputGlobal().mousePositionX);
				_y = static_cast<float>(internal_gw::GInputGlobal().mousePositionY);

				return GW::GReturn::SUCCESS;
			}

			GReturn GetKeyMask(unsigned int& _outKeyMask) const override {
				_outKeyMask = internal_gw::GInputGlobal().keyMask;
				return GW::GReturn::SUCCESS;
			}
		};
	}
}