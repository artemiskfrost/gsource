#include <cmath>
#include <chrono>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <XInput.h>
#pragma comment(lib, "XInput.lib")
#include "../../../Interface/System/GDaemon.h"
#include <objbase.h>

namespace GW
{
	namespace I
	{
		class GGeneralController; // Interface which all controller will inherit from
		class GXboxController; // xbox
		// class GPS4Controller; // Not supported yet! future devs, this is for you :)

		class GControllerImplementation :	public virtual GControllerInterface,
				protected GEventGeneratorImplementation
		{
		private:
			GGeneralController* pController = nullptr; // POLYMORHISM
		public:
			~GControllerImplementation();

			GReturn Create();

			GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override;
			GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override;
			GReturn GetMaxIndex(int& _outMax) override;
			GReturn GetNumConnected(int& _outConnectedCount) override;
			GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) override;
			GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override;
			GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override;
			GReturn StopVibration(unsigned int _controllerIndex) override;
			GReturn StopAllVibrations() override;

			GReturn Register(CORE::GEventCache _observer) override;
			GReturn Register(CORE::GEventResponder _observer) override;
			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override;
			GReturn Deregister(CORE::GInterface _observer) override;
			GReturn Observers(unsigned int& _outCount) const override;
			GReturn Push(const GEvent& _newEvent) override;
		};

		class GGeneralController :	public virtual GControllerInterface,
									public GEventGeneratorImplementation
		{
		protected:
			struct CONTROLLER_STATE
			{
				int isConnected;
				int isVibrating;
				float vibrationDuration;
				std::chrono::high_resolution_clock::time_point* vibrationStartTime;
				int maxInputs; // Hold the size of controllerInputs array
				float* controllerInputs; // controllerInputs is used to hold an array for the input values of the controller
				GW::INPUT::GControllerType controllerID;
			} *pControllers = nullptr;

			GW::I::GControllerInterface::DeadZoneTypes deadZoneType = GW::I::GControllerInterface::DeadZoneTypes::DEADZONECIRCLE;
			float deadZonePercentage = 0;

			// This function does not lock before using _controllers
			unsigned int FindEmptyControllerIndex(unsigned int _maxIndex, const CONTROLLER_STATE* _controllers) const
			{
				for (unsigned int i = 0; i < _maxIndex; ++i)
				{
					if (_controllers[i].isConnected == 0)
						return i;
				}
				return -1;
			}

			// perhaps make return value GRETURN
			CONTROLLER_STATE* CopyControllerState(const CONTROLLER_STATE* _stateToCopy, CONTROLLER_STATE* _outCopy) const
			{
				if (_stateToCopy->maxInputs == _outCopy->maxInputs)
					for (int i = 0; i < _outCopy->maxInputs; ++i)
					{
						_outCopy->controllerInputs[i] = _stateToCopy->controllerInputs[i];
					}
				else
					_outCopy = nullptr;

				return _outCopy;
			}

			void DeadZoneCalculation(float _x, float _y, float _axisMax, float _axisMin, float& _outX, float& _outY, GW::I::GControllerInterface::DeadZoneTypes _deadzoneType, float _deadzonePercentage) const
			{
				float range = _axisMax - _axisMin;
				_outX = (((_x - _axisMin) * 2) / range) - 1;
				_outY = (((_y - _axisMin) * 2) / range) - 1;
				float liveRange = 1.0f - _deadzonePercentage;
				if (_deadzoneType == GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE)
				{
					if (std::abs(_outX) <= _deadzonePercentage)
						_outX = 0.0f;
					if (std::abs(_outY) <= _deadzonePercentage)
						_outY = 0.0f;

					if (_outX > 0.0f)
						_outX = (_outX - _deadzonePercentage) / liveRange;
					else if (_outX < 0.0f)
						_outX = (_outX + _deadzonePercentage) / liveRange;
					if (_outY > 0.0f)
						_outY = (_outY - _deadzonePercentage) / liveRange;
					else if (_outY < 0.0f)
						_outY = (_outY + _deadzonePercentage) / liveRange;
				}
				else
				{
					float mag = std::sqrt(_outX * _outX + _outY * _outY);
					mag = (mag - _deadzonePercentage) / liveRange;
					_outX *= mag;
					_outY *= mag;

					if (std::abs(_outX) <= _deadzonePercentage)
						_outX = 0.0f;
					if (std::abs(_outY) <= _deadzonePercentage)
						_outY = 0.0f;
				}
			}
		public:
			GGeneralController() {}

			virtual void Initialize()
			{
				pControllers = new CONTROLLER_STATE[G_MAX_CONTROLLER_COUNT];
				deadZoneType = GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE;
				deadZonePercentage = 0.2f;

				for (unsigned int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					pControllers[i].isConnected = 0;
					pControllers[i].isVibrating = 0;
					pControllers[i].vibrationDuration = 0;
					pControllers[i].vibrationStartTime = new std::chrono::high_resolution_clock::time_point();
					pControllers[i].maxInputs = G_MAX_GENERAL_INPUTS;
					pControllers[i].controllerInputs = new float[G_MAX_GENERAL_INPUTS];
					for (unsigned int j = 0; j < G_MAX_GENERAL_INPUTS; ++j)
					{
						pControllers[i].controllerInputs[j] = 0.0f;
					}
				}
			}

			virtual void Release()
			{
				for (int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					delete[] pControllers[i].controllerInputs;
					delete pControllers[i].vibrationStartTime;
				}
				delete[] pControllers;
			}

			virtual GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override
			{
				if (_controllerIndex > G_MAX_CONTROLLER_INDEX || _inputCode < 0 || _inputCode >= G_MAX_GENERAL_INPUTS)
					return GReturn::INVALID_ARGUMENT;

				LockAsyncRead();
				if (pControllers[_controllerIndex].isConnected == 0)
				{
					UnlockAsyncRead();
					return GReturn::FAILURE;
				}

				_outState = pControllers[_controllerIndex].controllerInputs[(_inputCode)];
				UnlockAsyncRead();

				return GReturn::SUCCESS;
			}

			virtual GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override
			{
				if (_controllerIndex > G_MAX_CONTROLLER_INDEX)
					return GReturn::INVALID_ARGUMENT;
				LockAsyncRead();
				_outIsConnected = pControllers[_controllerIndex].isConnected == 0 ? false : true;
				UnlockAsyncRead();

				return GReturn::SUCCESS;
			}

			virtual GReturn GetMaxIndex(int& _outMax) override
			{
				_outMax = G_MAX_CONTROLLER_INDEX;
				return GReturn::SUCCESS;
			}

			virtual GReturn GetNumConnected(int& _outConnectedCount) override
			{
				_outConnectedCount = 0;
				LockAsyncRead();
				for (unsigned int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
				{
					if (pControllers[i].isConnected)
						++_outConnectedCount;
				}
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			virtual GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) override
			{
				if (_deadzonePercentage > 1.0f || _deadzonePercentage < 0.0f)
					return GReturn::INVALID_ARGUMENT;
				LockSyncWrite();
				deadZoneType = _type;
				deadZonePercentage = _deadzonePercentage;
				UnlockSyncWrite();
				return GReturn::SUCCESS;
			}

			virtual GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			virtual GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			virtual GReturn StopVibration(unsigned int _controllerIndex) override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			virtual GReturn StopAllVibrations() override
			{
				return GReturn::FEATURE_UNSUPPORTED;
			}

			GReturn Register(CORE::GEventCache _observer) override final { return GEventGeneratorImplementation::Register(_observer); }
			GReturn Register(CORE::GEventResponder _observer) override final { return GEventGeneratorImplementation::Register(_observer); }
			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override final { return GEventGeneratorImplementation::Register(_observer, _callback); }
			GReturn Deregister(CORE::GInterface _observer) override final { return GEventGeneratorImplementation::Deregister(_observer); }
			GReturn Observers(unsigned int& _outCount) const override final { return GEventGeneratorImplementation::Observers(_outCount); }
			GReturn Push(const GEvent& _newEvent) override final { return GEventGeneratorImplementation::Push(_newEvent); }
		};

		class GXboxController : public GGeneralController
		{
		private:
			GW::SYSTEM::GDaemon xinputDaemon;
			int XControllerSlotIndices[4];
			DWORD XControllerLastPacket[4];

			struct DaemonData
			{
				DWORD result;
				XINPUT_STATE controllerState;
				EVENT_DATA eventData;
				CONTROLLER_STATE oldState;

				DaemonData()
				{
					ZeroMemory(&controllerState, sizeof(XINPUT_STATE));
					ZeroMemory(&eventData, sizeof(EVENT_DATA));
					oldState.maxInputs = G_MAX_XBOX_INPUTS;
					oldState.controllerInputs = new float[G_MAX_XBOX_INPUTS];
				}

				~DaemonData()
				{
					delete[] oldState.controllerInputs;
				}
			} daemonData;

			float XboxDeadZoneCalc(float _value, bool _isTigger)
			{
				if (_isTigger)
				{
					if (std::abs(_value) > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
						_value /= G_MAX_XBOX_TRIGGER_AXIS;
					else
						_value = 0;
				}
				else
				{
					if (std::abs(_value) > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
						_value /= G_MAX_XBOX_THUMB_AXIS;
					else
						_value = 0;
				}
				return _value;
			}
		public:
			GXboxController()
				: GGeneralController()
			{
				ZeroMemory(XControllerLastPacket, ARRAYSIZE(XControllerLastPacket));
				for (int i = 0; i < 4; ++i)
					XControllerSlotIndices[i] = -1;
			}

			virtual void Initialize()
			{
				pControllers = new CONTROLLER_STATE[G_MAX_XBOX_CONTROLLER_COUNT];
				deadZoneType = GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE;
				deadZonePercentage = 0.2f;
				for (unsigned int i = 0; i < G_MAX_XBOX_CONTROLLER_COUNT; ++i)
				{
					pControllers[i].isConnected = 0;
					pControllers[i].isVibrating = 0;
					pControllers[i].vibrationDuration = 0;
					pControllers[i].vibrationStartTime = new std::chrono::high_resolution_clock::time_point();
					pControllers[i].maxInputs = G_MAX_XBOX_INPUTS;
					pControllers[i].controllerInputs = new float[G_MAX_XBOX_INPUTS];
				}

				// Seperating the main code into this lambda
				auto updateControllerState = [&]
				{
					/* --- NOTE ---
					* pControllers:
					*      Locked when being read or written to because it can be read and written to outside
					*      of this function. All other function should lock when reading or writing to it.
					* XControllerSlotIndices:
					*      Only Locked whenever written to because it is only written to inside of this function.
					*      All other functions should lock before reading this array while xinputDaemon is running.
					* All other member variables are not used outside of this function. Therefore, they don't need locks.
					*/
					CoInitialize(nullptr); // place this thread in COM single threaded apartment

					GW::GEvent m_GEvent;
					for (int i = 0; i < G_MAX_XBOX_CONTROLLER_COUNT; ++i)
					{
						daemonData.result = XInputGetState(i, &daemonData.controllerState);
						if (daemonData.result == ERROR_SUCCESS)
						{
							if (XControllerSlotIndices[i] < 0)
							{
								LockSyncWrite();
								XControllerSlotIndices[i] = FindEmptyControllerIndex(G_MAX_XBOX_CONTROLLER_COUNT, pControllers);
								pControllers[XControllerSlotIndices[i]].isConnected = 1;
								UnlockSyncWrite();

								daemonData.eventData.controllerIndex = XControllerSlotIndices[i];
								daemonData.eventData.inputCode = 0;
								daemonData.eventData.inputValue = 0;
								daemonData.eventData.isConnected = 1;
								daemonData.eventData.controllerID = GW::INPUT::GControllerType::XBOX360;

								m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERCONNECTED, daemonData.eventData);
								Push(m_GEvent);
							}

							if (XControllerSlotIndices[i] >= 0 && daemonData.controllerState.dwPacketNumber != XControllerLastPacket[i]) // add checks for events
							{
								LockAsyncRead();
								CopyControllerState(&pControllers[XControllerSlotIndices[i]], &daemonData.oldState);
								UnlockAsyncRead();
								daemonData.eventData.isConnected = 1;
								daemonData.eventData.controllerIndex = XControllerSlotIndices[i];

								XControllerLastPacket[i] = daemonData.controllerState.dwPacketNumber;

								// Buttons
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_A) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_SOUTH_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_SOUTH_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_A) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_SOUTH_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_SOUTH_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_B) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_EAST_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_EAST_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_B) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_EAST_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_EAST_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_Y) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_NORTH_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_NORTH_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_Y) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_NORTH_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_NORTH_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_X) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_WEST_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_WEST_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_X) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_WEST_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_WEST_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_LEFT_SHOULDER_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_SHOULDER_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_LEFT_SHOULDER_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_SHOULDER_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_RIGHT_SHOULDER_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_SHOULDER_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_RIGHT_SHOULDER_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_SHOULDER_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_DPAD_LEFT_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_LEFT_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_DPAD_LEFT_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_LEFT_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_DPAD_RIGHT_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_RIGHT_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_DPAD_RIGHT_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_RIGHT_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_DPAD_UP_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_UP_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_DPAD_UP_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_UP_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_DPAD_DOWN_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_DOWN_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_DPAD_DOWN_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_DPAD_DOWN_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_LEFT_THUMB_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_THUMB_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_LEFT_THUMB_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_THUMB_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_RIGHT_THUMB_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_THUMB_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_RIGHT_THUMB_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_THUMB_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_START) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_START_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_START_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_START) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_START_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_START_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (((daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) != 0 ? 1.0f : 0.0f) != daemonData.oldState.controllerInputs[G_SELECT_BTN])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_SELECT_BTN] = (daemonData.controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) != 0 ? 1.0f : 0.0f;
									daemonData.eventData.inputCode = G_SELECT_BTN;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_SELECT_BTN];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}

								// AXES
								if (XboxDeadZoneCalc(daemonData.controllerState.Gamepad.bLeftTrigger, true) != daemonData.oldState.controllerInputs[G_LEFT_TRIGGER_AXIS])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_TRIGGER_AXIS] = XboxDeadZoneCalc(daemonData.controllerState.Gamepad.bLeftTrigger, true);
									daemonData.eventData.inputCode = G_LEFT_TRIGGER_AXIS;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LEFT_TRIGGER_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (XboxDeadZoneCalc(daemonData.controllerState.Gamepad.bRightTrigger, true) != daemonData.oldState.controllerInputs[G_RIGHT_TRIGGER_AXIS])
								{
									LockSyncWrite();
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_TRIGGER_AXIS] = XboxDeadZoneCalc(daemonData.controllerState.Gamepad.bRightTrigger, true);
									daemonData.eventData.inputCode = G_RIGHT_TRIGGER_AXIS;
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RIGHT_TRIGGER_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}

								LockSyncWrite();

								DeadZoneCalculation(daemonData.controllerState.Gamepad.sThumbLX,
									daemonData.controllerState.Gamepad.sThumbLY,
									G_MAX_XBOX_THUMB_AXIS,
									G_MIN_XBOX_THUMB_AXIS,
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LX_AXIS],
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_LY_AXIS],
									deadZoneType,
									deadZonePercentage);

								DeadZoneCalculation(daemonData.controllerState.Gamepad.sThumbRX,
									daemonData.controllerState.Gamepad.sThumbRY,
									G_MAX_XBOX_THUMB_AXIS,
									G_MIN_XBOX_THUMB_AXIS,
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RX_AXIS],
									pControllers[XControllerSlotIndices[i]].controllerInputs[G_RY_AXIS],
									deadZoneType,
									deadZonePercentage);

								float newLX = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LX_AXIS];
								float newLY = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LY_AXIS];
								float newRX = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RX_AXIS];
								float newRY = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RY_AXIS];

								UnlockSyncWrite();

								if (newLX != daemonData.oldState.controllerInputs[G_LX_AXIS])
								{
									daemonData.eventData.inputCode = G_LX_AXIS;
									LockSyncWrite();
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LX_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
								if (newLY != daemonData.oldState.controllerInputs[G_LY_AXIS])
								{
									daemonData.eventData.inputCode = G_LY_AXIS;
									LockSyncWrite();
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_LY_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}

								if (newRX != daemonData.oldState.controllerInputs[G_RX_AXIS])
								{
									daemonData.eventData.inputCode = G_RX_AXIS;
									LockSyncWrite();
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RX_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}

								if (newRY != daemonData.oldState.controllerInputs[G_RY_AXIS])
								{
									daemonData.eventData.inputCode = G_RY_AXIS;
									LockSyncWrite();
									daemonData.eventData.inputValue = pControllers[XControllerSlotIndices[i]].controllerInputs[G_RY_AXIS];
									UnlockSyncWrite();

									m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, daemonData.eventData);
									Push(m_GEvent);
								}
							}
						}
						else // no controller connected
						{
							if (XControllerSlotIndices[i] >= 0)
							{
								//call event

								LockSyncWrite();
								pControllers[XControllerSlotIndices[i]].isConnected = 0;
								pControllers[XControllerSlotIndices[i]].isVibrating = 0;
								pControllers[XControllerSlotIndices[i]].vibrationDuration = 0.0f;
								UnlockSyncWrite();

								daemonData.eventData.controllerIndex = XControllerSlotIndices[i];
								daemonData.eventData.inputCode = 0;
								daemonData.eventData.inputValue = 0;
								daemonData.eventData.isConnected = 0;
								daemonData.eventData.controllerID = GW::INPUT::GControllerType::XBOX360;

								m_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERDISCONNECTED, daemonData.eventData);
								Push(m_GEvent);

								LockSyncWrite();
								XControllerSlotIndices[i] = -1;
								UnlockSyncWrite();
							}
						}

						LockAsyncRead();
						if (XControllerSlotIndices[i] >= 0 && pControllers[XControllerSlotIndices[i]].isVibrating)
						{
							if (pControllers[XControllerSlotIndices[i]].vibrationDuration <=
								(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() -
									*pControllers[XControllerSlotIndices[i]].vibrationStartTime).count() * .001f))
							{
								UnlockAsyncRead();

								XINPUT_VIBRATION vibrationState;
								vibrationState.wLeftMotorSpeed = 0;
								vibrationState.wRightMotorSpeed = 0;

								LockSyncWrite();
								pControllers[XControllerSlotIndices[i]].isVibrating = 0;
								pControllers[XControllerSlotIndices[i]].vibrationDuration = 0.0f;
								UnlockSyncWrite();

								XInputSetState(i, &vibrationState);

								LockAsyncRead();
							}
						}
						UnlockAsyncRead();
					}
				};

				// Event callback for xinput
				xinputDaemon.Create(G_CONTROLLER_DAEMON_OPERATION_INTERVAL, updateControllerState);

				// Call updateControllerState() here to force the update to happen before the event callback is called
				updateControllerState();
			}

			virtual void Release()
			{
				xinputDaemon = nullptr; // Stop the input thread.

				for (int i = 0; i < G_MAX_XBOX_CONTROLLER_COUNT; ++i)
				{
					delete[] pControllers[i].controllerInputs;
					delete pControllers[i].vibrationStartTime;
				}
				delete[] pControllers;
			}

			GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override final
			{
				if (_controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX || _inputCode < 0 || _inputCode  > 19)
					return GReturn::INVALID_ARGUMENT;

				LockAsyncRead();
				if (pControllers[_controllerIndex].isConnected == 0)
				{
					UnlockAsyncRead();
					return GReturn::FAILURE;
				}

				_outState = pControllers[_controllerIndex].controllerInputs[_inputCode];
				UnlockAsyncRead();

				return GReturn::SUCCESS;
			}

			GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override final
			{
				if (_controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX)
					return GReturn::INVALID_ARGUMENT;
				LockAsyncRead();
				_outIsConnected = pControllers[_controllerIndex].isConnected == 0 ? false : true;
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			GReturn GetMaxIndex(int& _outMax) override final
			{
				_outMax = G_MAX_XBOX_CONTROLLER_INDEX;
				return GReturn::SUCCESS;
			}

			GReturn GetNumConnected(int& _outConnectedCount) override final
			{
				_outConnectedCount = 0;
				LockAsyncRead();
				for (unsigned int i = 0; i < G_MAX_XBOX_CONTROLLER_COUNT; ++i)
				{
					if (pControllers[i].isConnected)
						++_outConnectedCount;
				}
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override final
			{
				if ((_controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX)
					|| (_pan < -1.0f || _pan > 1.0f)
					|| _duration < 0.0f
					|| (_strength < -1.0f || _strength > 1.0f))
					return GReturn::INVALID_ARGUMENT;

				LockAsyncRead();
				if (pControllers[_controllerIndex].isVibrating)
				{
					UnlockAsyncRead();
					return GReturn::REDUNDANT;
				}
				UnlockAsyncRead();

				XINPUT_VIBRATION vibrationState;
				unsigned int vibrationStrength = static_cast<unsigned int>(G_XINPUT_MAX_VIBRATION * _strength);
				vibrationState.wLeftMotorSpeed = static_cast<WORD>(vibrationStrength * (.5f + (.5f * (-1 * _pan))));
				vibrationState.wRightMotorSpeed = static_cast<WORD>(vibrationStrength * (.5f + (.5f * _pan)));

				LockSyncWrite();
				for (int i = 0; i < 4; ++i)
				{
					if (_controllerIndex == static_cast<unsigned int>(XControllerSlotIndices[i]))
					{
						pControllers[i].isVibrating = 1;
						pControllers[i].vibrationDuration = _duration;
						*pControllers[i].vibrationStartTime = std::chrono::high_resolution_clock::now();
						XInputSetState(i, &vibrationState);
						break;
					}
				}
				UnlockSyncWrite();
				return GReturn::SUCCESS;
			}

			GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override final
			{
				if ((_controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX))
					return GReturn::INVALID_ARGUMENT;
				LockAsyncRead();
				_outIsVibrating = pControllers[_controllerIndex].isVibrating == 0 ? false : true;
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			GReturn StopVibration(unsigned int _controllerIndex) override final
			{
				if ((_controllerIndex > G_MAX_XBOX_CONTROLLER_INDEX))
					return GReturn::INVALID_ARGUMENT;

				LockAsyncRead();
				if (pControllers[_controllerIndex].isVibrating == false)
				{
					UnlockAsyncRead();
					return GReturn::REDUNDANT;
				}
				UnlockAsyncRead();

				XINPUT_VIBRATION vibrationState;
				vibrationState.wLeftMotorSpeed = 0;
				vibrationState.wRightMotorSpeed = 0;

				LockSyncWrite();
				for (int i = 0; i < 4; ++i)
				{
					if (_controllerIndex == static_cast<unsigned int>(XControllerSlotIndices[i]))
					{
						pControllers[i].isVibrating = 0;
						pControllers[i].vibrationDuration = 0.0f;
						XInputSetState(i, &vibrationState);
						break;
					}
				}
				UnlockSyncWrite();
				return GReturn::SUCCESS;
			}

			GReturn StopAllVibrations() override final
			{
				XINPUT_VIBRATION vibrationState;
				vibrationState.wLeftMotorSpeed = 0;
				vibrationState.wRightMotorSpeed = 0;

				LockSyncWrite();
				for (int i = 0; i < G_MAX_XBOX_CONTROLLER_COUNT; ++i)
				{
					if (pControllers[i].isVibrating)
					{
						pControllers[i].isVibrating = 0;
						pControllers[i].vibrationDuration = 0.0f;
						XInputSetState(i, &vibrationState);
					}
				}
				UnlockSyncWrite();
				return GReturn::SUCCESS;
			}
		};

		// Need to put definitions here instead of class body because it needs to know function definitions
		inline GControllerImplementation::~GControllerImplementation()
		{
			if (pController)
			{
				pController->Release();
				delete pController;
				pController = nullptr;
			}
		}

		inline GReturn GControllerImplementation::Create()
		{
			pController = new GXboxController();
			pController->Initialize();

			return GReturn::SUCCESS;
		}
		inline GReturn GControllerImplementation::GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) { return pController ? pController->GetState(_controllerIndex, _inputCode, _outState) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) { return pController ? pController->IsConnected(_controllerIndex, _outIsConnected) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::GetMaxIndex(int& _outMax) { return pController ? pController->GetMaxIndex(_outMax) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::GetNumConnected(int& _outConnectedCount) { return pController ? pController->GetNumConnected(_outConnectedCount) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) { return pController ? pController->SetDeadZone(_type, _deadzonePercentage) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) { return pController ? pController->StartVibration(_controllerIndex, _pan, _duration, _strength) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) { return pController ? pController->IsVibrating(_controllerIndex, _outIsVibrating) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::StopVibration(unsigned int _controllerIndex) { return pController ? pController->StopVibration(_controllerIndex) : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::StopAllVibrations() { return pController ? pController->StopAllVibrations() : GReturn::FAILURE; }
		inline GReturn GControllerImplementation::Register(CORE::GEventCache _observer) { return pController ? pController->Register(_observer) : GEventGeneratorImplementation::Register(_observer); }
		inline GReturn GControllerImplementation::Register(CORE::GEventResponder _observer) { return pController ? pController->Register(_observer) : GEventGeneratorImplementation::Register(_observer); }
		inline GReturn GControllerImplementation::Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) { return pController ? pController->Register(_observer, _callback) : GEventGeneratorImplementation::Register(_observer, _callback); }
		inline GReturn GControllerImplementation::Deregister(CORE::GInterface _observer) { return pController ? pController->Deregister(_observer) : GEventGeneratorImplementation::Deregister(_observer); }
		inline GReturn GControllerImplementation::Observers(unsigned int& _outCount) const { return pController ? pController->Observers(_outCount) : GEventGeneratorImplementation::Observers(_outCount); }
		inline GReturn GControllerImplementation::Push(const GEvent& _newEvent) { return pController ? pController->Push(_newEvent) : GEventGeneratorImplementation::Push(_newEvent); }
	} // end I namespace
} // end GW namespace