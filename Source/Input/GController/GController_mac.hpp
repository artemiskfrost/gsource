#ifdef __OBJC__
@import Foundation;
@import Cocoa;
@import IOKit.hid;
#endif

#include <atomic>
#include <chrono>
#include <cmath>
#include <pthread.h>
#include "../../Shared/macutils.h"

namespace GW
{
    namespace I
    {
        class GGeneralController;
    }
}

namespace internal_gw
{
    // HIDMANAGER Interface
    // HidManager wrapper class

    // Data members of HIDMANAGER
    G_OBJC_DATA_MEMBERS_STRUCT(HIDMANAGER)
    {
        // These are to point to the GController variables
        GW::I::GGeneralController* pController;
        CFRunLoopRef managerRunLoop;

        int LX, LY, LZ, RX, RY, RZ;

        IOHIDManagerRef hidManager;
        NSThread* runLoopThread;
    };

    // Forward declarations of HIDMANAGER methods
    G_OBJC_HEADER_DATA_MEMBERS_PROPERTY_METHOD(HIDMANAGER);

    G_OBJC_HEADER_INSTANCE_METHOD(HIDMANAGER, void, InitManagerAndRunLoop);

    // Creates the HIDMANAGER class at runtime when G_OBJC_GET_CLASS(HIDMANAGER) is called
    G_OBJC_CLASS_BEGIN(HIDMANAGER, NSObject)
    {
        G_OBJC_CLASS_DATA_MEMBERS_PROPERTY(HIDMANAGER);

        G_OBJC_CLASS_METHOD(HIDMANAGER, InitManagerAndRunLoop, "v@:");
    }
    G_OBJC_CLASS_END(HIDMANAGER)

    // HIDMANAGER Interface End
}

namespace GW
{
    namespace I
    {
        class GGeneralController; // Interface which all controller will inherit from

        class GControllerImplementation : public virtual GControllerInterface,
            protected GEventGeneratorImplementation
        {
        private:
            GGeneralController* pController; // POLYMORHISM
        public:
            ~GControllerImplementation();

            GReturn Create();

            GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override;
            GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override;
            GReturn GetMaxIndex(int& _outMax) override;
            GReturn GetNumConnected(int& _outConnectedCount) override;
            GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) override;
            GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override;
            GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override;
            GReturn StopVibration(unsigned int _controllerIndex) override;
            GReturn StopAllVibrations() override;

            GReturn Register(CORE::GEventCache _observer) override;
            GReturn Register(CORE::GEventResponder _observer) override;
            GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override;
            GReturn Deregister(CORE::GInterface _observer) override;
            GReturn Observers(unsigned int& _outCount) const override;
            GReturn Push(const GEvent& _newEvent) override;
        };

        class GGeneralController : public GControllerInterface,
            protected GEventGeneratorImplementation
        {
        public:
            struct CONTROLLER_STATE
            {
                int isConnected;
                int isVibrating;
                float vibrationDuration;
                std::chrono::high_resolution_clock::time_point* vibrationStartTime;
                int maxInputs; // Hold the size of controllerInputs array
                float* controllerInputs; // controllerInputs is used to hold an array for the input values of the controller
                GW::INPUT::GControllerType controllerID;
                IOHIDDeviceRef device;
                int codeMapping;
                int axisMapping;
            } *pControllers;

            GW::I::GControllerInterface::DeadZoneTypes deadZoneType;
            float deadZonePercentage;

            GW::CORE::GThreadShared mutex;
            std::atomic<bool> isRunning;
            NSThread* runLoopThread;
            id manager;
            pthread_attr_t  runLoopPthread_attr;
            pthread_t       runLoopPthread_ID;

            // This function does not lock before using _controllers
            int FindEmptyControllerIndex(unsigned int _maxIndex, const CONTROLLER_STATE* _controllers)
            {
                for (unsigned int i = 0; i < _maxIndex; ++i)
                {
                    if (_controllers[i].isConnected == 0)
                        return i;
                }
                return -1;
            }

            // perhaps make return value GRETURN
            CONTROLLER_STATE* CopyControllerState(const CONTROLLER_STATE* _stateToCopy, CONTROLLER_STATE* _outCopy)
            {
                if (_stateToCopy->maxInputs == _outCopy->maxInputs)
                    for (int i = 0; i < _outCopy->maxInputs; ++i)
                    {
                        _outCopy->controllerInputs[i] = _stateToCopy->controllerInputs[i];
                    }
                else
                    _outCopy = nullptr;

                return _outCopy;
            }

            void DeadZoneCalculation(float _x, float _y, float _axisMax, float _axisMin, float& _outX, float& _outY, GW::I::GControllerInterface::DeadZoneTypes _deadzoneType, float _deadzonePercentage)
            {
                float range = _axisMax - _axisMin;
                _outX = (((_x - _axisMin) * 2) / range) - 1;
                _outY = (((_y - _axisMin) * 2) / range) - 1;
                float liveRange = 1.0f - _deadzonePercentage;
                if (_deadzoneType == GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE)
                {
                    if (std::abs(_outX) <= _deadzonePercentage)
                        _outX = 0.0f;
                    if (std::abs(_outY) <= _deadzonePercentage)
                        _outY = 0.0f;

                    if (_outX > 0.0f)
                        _outX = (_outX - _deadzonePercentage) / liveRange;
                    else if (_outX < 0.0f)
                        _outX = (_outX + _deadzonePercentage) / liveRange;
                    if (_outY > 0.0f)
                        _outY = (_outY - _deadzonePercentage) / liveRange;
                    else if (_outY < 0.0f)
                        _outY = (_outY + _deadzonePercentage) / liveRange;
                }
                else
                {
                    float mag = std::sqrt(_outX * _outX + _outY * _outY);
                    mag = (mag - _deadzonePercentage) / liveRange;
                    _outX *= mag;
                    _outY *= mag;

                    if (std::abs(_outX) <= _deadzonePercentage)
                        _outX = 0.0f;
                    if (std::abs(_outY) <= _deadzonePercentage)
                        _outY = 0.0f;
                }
            }

            static void* RunLoopThreadEntry(void* manager_ptr)
            {
                id manager = (id)manager_ptr;
                internal_gw::G_OBJC_CALL_METHOD(HIDMANAGER, manager, InitManagerAndRunLoop); // warning due to forward declartion
                return NULL;
            }
        public:
            GGeneralController() {}

            virtual void Initialize()
            {
                mutex.Create();
                pControllers = new CONTROLLER_STATE[G_MAX_CONTROLLER_COUNT];
                deadZoneType = GW::I::GControllerInterface::DeadZoneTypes::DEADZONESQUARE;
                deadZonePercentage = 0.2f;

                for (unsigned int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
                {
                    pControllers[i].isConnected = 0;
                    pControllers[i].isVibrating = 0;
                    pControllers[i].vibrationDuration = 0;
                    pControllers[i].vibrationStartTime = new std::chrono::high_resolution_clock::time_point();
                    pControllers[i].maxInputs = G_MAX_GENERAL_INPUTS;
                    pControllers[i].controllerInputs = new float[G_MAX_GENERAL_INPUTS];
                    for (unsigned int j = 0; j < G_MAX_GENERAL_INPUTS; ++j)
                    {
                        pControllers[i].controllerInputs[j] = 0.0f;
                    }
                }

                isRunning = true;
                manager = [internal_gw::G_OBJC_GET_CLASS(HIDMANAGER) alloc];

                internal_gw::G_OBJC_DATA_MEMBERS_TYPE(HIDMANAGER)& managerDataMembers = internal_gw::G_OBJC_GET_DATA_MEMBERS(HIDMANAGER, manager);
                managerDataMembers.pController = this;

                pthread_attr_init(&runLoopPthread_attr);

                pthread_attr_setdetachstate(&runLoopPthread_attr, PTHREAD_CREATE_JOINABLE);

                pthread_create(&runLoopPthread_ID, &runLoopPthread_attr, &RunLoopThreadEntry, manager);
            }

            virtual void Release()
            {
                isRunning = false;
                pthread_join(runLoopPthread_ID, NULL);
                for (int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
                {
                    delete[] pControllers[i].controllerInputs;
                    delete pControllers[i].vibrationStartTime;
                }
                delete[] pControllers;
            }

            virtual GReturn GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) override
            {
                if (_controllerIndex > G_MAX_CONTROLLER_INDEX || _inputCode < 0 || _inputCode >= G_MAX_GENERAL_INPUTS)
                    return GReturn::INVALID_ARGUMENT;
                if (pControllers[_controllerIndex].isConnected == 0)
                    return GReturn::FAILURE;
                LockAsyncRead();
                _outState = pControllers[_controllerIndex].controllerInputs[(_inputCode)];
                UnlockAsyncRead();
                return GReturn::SUCCESS;
            }

            virtual GReturn IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) override
            {
                if (_controllerIndex > G_MAX_CONTROLLER_INDEX)
                    return GReturn::INVALID_ARGUMENT;
                LockAsyncRead();
                _outIsConnected = pControllers[_controllerIndex].isConnected == 0 ? false : true;
                UnlockAsyncRead();

                return GReturn::SUCCESS;
            }

            virtual GReturn GetMaxIndex(int& _outMax) override
            {
                _outMax = G_MAX_CONTROLLER_INDEX;
                return GReturn::SUCCESS;
            }

            virtual GReturn GetNumConnected(int& _outConnectedCount) override
            {
                _outConnectedCount = 0;
                LockAsyncRead();
                for (unsigned int i = 0; i < G_MAX_CONTROLLER_COUNT; ++i)
                {
                    if (pControllers[i].isConnected)
                        ++_outConnectedCount;
                }
                UnlockAsyncRead();
                return GReturn::SUCCESS;
            }

            virtual GReturn SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) override
            {
                if (_deadzonePercentage > 1.0f || _deadzonePercentage < 0.0f)
                    return GReturn::INVALID_ARGUMENT;
                LockSyncWrite();
                deadZoneType = _type;
                deadZonePercentage = _deadzonePercentage;
                UnlockSyncWrite();
                return GReturn::SUCCESS;
            }

            virtual GReturn StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) override
            {
                return GReturn::FEATURE_UNSUPPORTED;
            }

            virtual GReturn IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) override
            {
                return GReturn::FEATURE_UNSUPPORTED;
            }

            virtual GReturn StopVibration(unsigned int _controllerIndex) override
            {
                return GReturn::FEATURE_UNSUPPORTED;
            }

            virtual GReturn StopAllVibrations() override
            {
                return GReturn::FEATURE_UNSUPPORTED;
            }

            GReturn Register(CORE::GEventCache _observer) override final { return GEventGeneratorImplementation::Register(_observer); }
            GReturn Register(CORE::GEventResponder _observer) override final { return GEventGeneratorImplementation::Register(_observer); }
            GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override final { return GEventGeneratorImplementation::Register(_observer, _callback); }
            GReturn Deregister(CORE::GInterface _observer) override final { return GEventGeneratorImplementation::Deregister(_observer); }
            GReturn Observers(unsigned int& _outCount) const override final { return GEventGeneratorImplementation::Observers(_outCount); }
            GReturn Push(const GEvent& _newEvent) override final { return GEventGeneratorImplementation::Push(_newEvent); }
        };

        inline GControllerImplementation::~GControllerImplementation()
        {
            if (pController)
            {
                pController->Release();
                delete pController;
                pController = nullptr;
            }
        }

        inline GReturn GControllerImplementation::Create()
        {
            pController = new GGeneralController();
            pController->Initialize();
            
            return GReturn::SUCCESS;
        }
        inline GReturn GControllerImplementation::GetState(unsigned int _controllerIndex, int _inputCode, float& _outState) { return pController ? pController->GetState(_controllerIndex, _inputCode, _outState) : GReturn::FAILURE; }
        inline GReturn GControllerImplementation::IsConnected(unsigned int _controllerIndex, bool& _outIsConnected) { return pController ? pController->IsConnected(_controllerIndex, _outIsConnected) : GReturn::FAILURE; }
        inline GReturn GControllerImplementation::GetMaxIndex(int& _outMax) { return pController ? pController->GetMaxIndex(_outMax) : GReturn::FAILURE; }
        inline GReturn GControllerImplementation::GetNumConnected(int& _outConnectedCount) { return pController ? pController->GetNumConnected(_outConnectedCount) : GReturn::FAILURE; }
        inline GReturn GControllerImplementation::SetDeadZone(DeadZoneTypes _type, float _deadzonePercentage) { return pController ? pController->SetDeadZone(_type, _deadzonePercentage) : GReturn::FAILURE; }
        inline GReturn GControllerImplementation::StartVibration(unsigned int _controllerIndex, float _pan, float _duration, float _strength) { return pController ? pController->StartVibration(_controllerIndex, _pan, _duration, _strength) : GReturn::FAILURE; }
        inline GReturn GControllerImplementation::IsVibrating(unsigned int _controllerIndex, bool& _outIsVibrating) { return pController ? pController->IsVibrating(_controllerIndex, _outIsVibrating) : GReturn::FAILURE; }
        inline GReturn GControllerImplementation::StopVibration(unsigned int _controllerIndex) { return pController ? pController->StopVibration(_controllerIndex) : GReturn::FAILURE; }
        inline GReturn GControllerImplementation::StopAllVibrations() { return pController ? pController->StopAllVibrations() : GReturn::FAILURE; }
        inline GReturn GControllerImplementation::Register(CORE::GEventCache _observer) { return pController ? pController->Register(_observer) : GEventGeneratorImplementation::Register(_observer); }
        inline GReturn GControllerImplementation::Register(CORE::GEventResponder _observer) { return pController ? pController->Register(_observer) : GEventGeneratorImplementation::Register(_observer); }
        inline GReturn GControllerImplementation::Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) { return pController ? pController->Register(_observer, _callback) : GEventGeneratorImplementation::Register(_observer, _callback); }
        inline GReturn GControllerImplementation::Deregister(CORE::GInterface _observer) { return pController ? pController->Deregister(_observer) : GEventGeneratorImplementation::Deregister(_observer); }
        inline GReturn GControllerImplementation::Observers(unsigned int& _outCount) const { return pController ? pController->Observers(_outCount) : GEventGeneratorImplementation::Observers(_outCount); }
        inline GReturn GControllerImplementation::Push(const GEvent& _newEvent) { return pController ? pController->Push(_newEvent) : GEventGeneratorImplementation::Push(_newEvent); }
    }
}

namespace internal_gw
{
    // Arg 1: context from IOHIDDeviceRegisterInputValueCallback
    // Arg 2: completion result for the input value operation
    // Arg 3: IOHIDDeviceRef of the device this element is from
    // Arg 4: the new element value
    static void Handle_IOHIDDeviceInputValueCallback(void* inContext, IOReturn inResult, void* inSender, IOHIDValueRef inIOHIDValueRef)
    {
        GW::GEvent l_GEvent;

        double_t scaledValue = IOHIDValueGetScaledValue(inIOHIDValueRef, kIOHIDValueScaleTypePhysical);

        IOHIDElementRef tIOHIDElementRef = IOHIDValueGetElement(inIOHIDValueRef);
        IOHIDElementType type = IOHIDElementGetType(tIOHIDElementRef);
        int32_t usage = IOHIDElementGetUsage(tIOHIDElementRef);
        
        id manager = (id)inContext;
        G_OBJC_DATA_MEMBERS_TYPE(HIDMANAGER)& managerDataMembers = G_OBJC_GET_DATA_MEMBERS(HIDMANAGER, manager);
        GW::I::GGeneralController* pController = managerDataMembers.pController;
        GW::I::GControllerInterface::EVENT_DATA eventData;
        int controllerIndex = 0;
        bool controllerFound = false;
        for (; controllerIndex < G_MAX_CONTROLLER_COUNT; ++controllerIndex)
        {
            if (pController->pControllers[controllerIndex].device == (IOHIDDeviceRef)inSender)
            {
                controllerFound = true;
                break;
            }
        }

        if (controllerFound)
        {
            while (!(usage < 549 && usage >= 0))
                usage = IOHIDElementGetUsage(tIOHIDElementRef);
            
            int inputCode = GW::I::Mac_ControllerCodes[usage][(int)pController->pControllers[controllerIndex].codeMapping];
            // switch on input type
            if (inputCode == G_UNKNOWN_INPUT)
                return;
            
            if (type == kIOHIDElementTypeInput_Button)
            {
                pController->mutex.LockSyncWrite();
                // swap 2 with controller id stored in pControllers
                pController->pControllers[controllerIndex].controllerInputs[inputCode] = (float)scaledValue;
                eventData.inputCode = inputCode;
                eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[inputCode];
                eventData.isConnected = 1;
                eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                eventData.controllerIndex = controllerIndex;
                pController->mutex.UnlockSyncWrite();

                l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                pController->Push(l_GEvent);
            }
            else if (type == kIOHIDElementTypeInput_Misc)
            {
                // get input code from array then switch base on the code to determine how to process it
                switch (inputCode)
                {
                case G_LX_AXIS:
                {
                    int& LX = managerDataMembers.LX;
                    if (scaledValue != LX)
                    {
                        int& LY = managerDataMembers.LY;
                        pController->mutex.LockSyncWrite();
                        float oldY = pController->pControllers[controllerIndex].controllerInputs[G_LY_AXIS];
                        LX = scaledValue;
                        int axisOffset = GW::I::ControllerAxisOffsets[inputCode];
                        pController->DeadZoneCalculation(LX,
                            LY,
                            GW::I::Mac_ControllerAxisRangesMax[pController->pControllers[controllerIndex].axisMapping][axisOffset],
                            GW::I::Mac_ControllerAxisRangesMin[pController->pControllers[controllerIndex].axisMapping][axisOffset],
                            pController->pControllers[controllerIndex].controllerInputs[G_LX_AXIS],
                            pController->pControllers[controllerIndex].controllerInputs[G_LY_AXIS],
                            pController->deadZoneType,
                            pController->deadZonePercentage);

                        eventData.inputCode = G_LX_AXIS;
                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_LX_AXIS];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();

                        l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                        pController->Push(l_GEvent);

                        pController->mutex.LockSyncWrite();
                        pController->pControllers[controllerIndex].controllerInputs[G_LY_AXIS] *= -1.0f; // to fix flipped value
                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_LY_AXIS];
                        pController->mutex.UnlockSyncWrite();

                        if (oldY != eventData.inputValue)
                        {
                            // Send LY event
                            eventData.inputCode = G_LY_AXIS;
                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                }
                break;

                case G_LY_AXIS:
                {
                    int& LY = managerDataMembers.LY;
                    //leftY
                    if (scaledValue != LY)
                    {
                        int& LX = managerDataMembers.LX;
                        pController->mutex.LockSyncWrite();
                        LY = scaledValue;
                        float oldX = pController->pControllers[controllerIndex].controllerInputs[G_LX_AXIS];
                        int axisOffset = GW::I::ControllerAxisOffsets[inputCode];
                        pController->DeadZoneCalculation(LX,
                            LY,
                            GW::I::Mac_ControllerAxisRangesMax[pController->pControllers[controllerIndex].axisMapping][axisOffset],
                            GW::I::Mac_ControllerAxisRangesMin[pController->pControllers[controllerIndex].axisMapping][axisOffset],
                            pController->pControllers[controllerIndex].controllerInputs[G_LX_AXIS],
                            pController->pControllers[controllerIndex].controllerInputs[G_LY_AXIS],
                            pController->deadZoneType,
                            pController->deadZonePercentage);

                        // Send LY event
                        pController->pControllers[controllerIndex].controllerInputs[G_LY_AXIS] *= -1.0f; // to fix flipped value
                        eventData.inputCode = G_LY_AXIS;
                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_LY_AXIS];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();

                        l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                        pController->Push(l_GEvent);

                        pController->mutex.LockSyncWrite();
                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_LX_AXIS];
                        pController->mutex.UnlockSyncWrite();

                        if (oldX != eventData.inputValue)
                        {
                            // Send LX event
                            eventData.inputCode = G_LX_AXIS;
                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                }
                break;

                case G_RX_AXIS:
                {
                    int& RX = managerDataMembers.RX;
                    if (scaledValue != RX)
                    {
                        int& RY = managerDataMembers.RY;
                        pController->mutex.LockSyncWrite();
                        float oldY = pController->pControllers[controllerIndex].controllerInputs[G_RY_AXIS];
                        RX = scaledValue;
                        int axisOffset = GW::I::ControllerAxisOffsets[inputCode];
                        pController->DeadZoneCalculation(RX,
                            RY,
                            GW::I::Mac_ControllerAxisRangesMax[pController->pControllers[controllerIndex].axisMapping][axisOffset],
                            GW::I::Mac_ControllerAxisRangesMin[pController->pControllers[controllerIndex].axisMapping][axisOffset],
                            pController->pControllers[controllerIndex].controllerInputs[G_RX_AXIS],
                            pController->pControllers[controllerIndex].controllerInputs[G_RY_AXIS],
                            pController->deadZoneType,
                            pController->deadZonePercentage);

                        eventData.inputCode = G_RX_AXIS;
                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_RX_AXIS];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();

                        l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                        pController->Push(l_GEvent);

                        pController->mutex.LockSyncWrite();
                        pController->pControllers[controllerIndex].controllerInputs[G_RY_AXIS] *= -1.0f; // to fix flipped value
                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_RY_AXIS];
                        pController->mutex.UnlockSyncWrite();

                        if (oldY != eventData.inputValue)
                        {
                            // Send LY event
                            eventData.inputCode = G_RY_AXIS;
                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                }
                break;

                case G_RY_AXIS:
                {
                    int& RY = managerDataMembers.RY;
                    if (scaledValue != RY)
                    {
                        int RX = managerDataMembers.RX;
                        pController->mutex.LockSyncWrite();
                        RY = scaledValue;
                        float oldX = pController->pControllers[controllerIndex].controllerInputs[G_RX_AXIS];
                        int axisOffset = GW::I::ControllerAxisOffsets[inputCode];
                        pController->DeadZoneCalculation(RX,
                            RY,
                            GW::I::Mac_ControllerAxisRangesMax[pController->pControllers[controllerIndex].axisMapping][axisOffset],
                            GW::I::Mac_ControllerAxisRangesMin[pController->pControllers[controllerIndex].axisMapping][axisOffset],
                            pController->pControllers[controllerIndex].controllerInputs[G_RX_AXIS],
                            pController->pControllers[controllerIndex].controllerInputs[G_RY_AXIS],
                            pController->deadZoneType,
                            pController->deadZonePercentage);

                        // Send RY event
                        pController->pControllers[controllerIndex].controllerInputs[G_RY_AXIS] *= -1.0f; // to fix flipped value
                        eventData.inputCode = G_RY_AXIS;
                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_RY_AXIS];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();

                        l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                        pController->Push(l_GEvent);

                        pController->mutex.LockSyncWrite();
                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_RX_AXIS];
                        pController->mutex.UnlockSyncWrite();

                        if (oldX != eventData.inputValue)
                        {
                            // Send RX event
                            eventData.inputCode = G_RX_AXIS;
                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                }
                break;

                case G_LEFT_TRIGGER_AXIS:
                {
                    int& LZ = managerDataMembers.LZ;
                    if (scaledValue != LZ)
                    {
                        pController->mutex.LockSyncWrite();
                        LZ = scaledValue;
                        float oldAxis = pController->pControllers[controllerIndex].controllerInputs[G_LEFT_TRIGGER_AXIS];
                        if (scaledValue > G_GENERAL_TRIGGER_THRESHOLD)
                        {
                            int axisOffset = GW::I::ControllerAxisOffsets[inputCode];
                            pController->pControllers[controllerIndex].controllerInputs[G_LEFT_TRIGGER_AXIS] = scaledValue / GW::I::Mac_ControllerAxisRangesMax[pController->pControllers[controllerIndex].axisMapping][axisOffset];
                        }
                        else
                            pController->pControllers[controllerIndex].controllerInputs[G_LEFT_TRIGGER_AXIS] = 0;

                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_LEFT_TRIGGER_AXIS];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (oldAxis != eventData.inputValue)
                        {
                            eventData.inputCode = G_LEFT_TRIGGER_AXIS;
                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                }
                break;

                case G_RIGHT_TRIGGER_AXIS:
                {
                    int& RZ = managerDataMembers.RZ;
                    if (scaledValue != RZ)
                    {
                        pController->mutex.LockSyncWrite();
                        RZ = scaledValue;
                        float oldAxis = pController->pControllers[controllerIndex].controllerInputs[G_RIGHT_TRIGGER_AXIS];
                        if (scaledValue > G_GENERAL_TRIGGER_THRESHOLD)
                        {
                            int axisOffset = GW::I::ControllerAxisOffsets[inputCode];
                            pController->pControllers[controllerIndex].controllerInputs[G_RIGHT_TRIGGER_AXIS] = scaledValue / GW::I::Mac_ControllerAxisRangesMax[pController->pControllers[controllerIndex].axisMapping][axisOffset];
                        }
                        else
                            pController->pControllers[controllerIndex].controllerInputs[G_RIGHT_TRIGGER_AXIS] = 0;

                        eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_RIGHT_TRIGGER_AXIS];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (oldAxis != eventData.inputValue)
                        {
                            eventData.inputCode = G_RIGHT_TRIGGER_AXIS;
                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERAXISVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                }
                break;

                case G_DPAD_LEFT_BTN: // This is used when the Dpad value is reported as a value of 0-360
                {
                    switch ((int)scaledValue)
                    {
                    case 0: // UP
                    {
                        pController->mutex.LockSyncWrite();
                        int upValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                        int rightValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                        int downValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                        int leftValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (upValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 1;
                            eventData.inputCode = G_DPAD_UP_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (rightValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 0;
                            eventData.inputCode = G_DPAD_RIGHT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (downValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 0;
                            eventData.inputCode = G_DPAD_DOWN_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (leftValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 0;
                            eventData.inputCode = G_DPAD_LEFT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                    break;

                    case 1: // UP-RIGHT
                    case 45:
                    {
                        pController->mutex.LockSyncWrite();
                        int upValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                        int rightValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                        int downValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                        int leftValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (upValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 1;
                            eventData.inputCode = G_DPAD_UP_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (rightValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 1;
                            eventData.inputCode = G_DPAD_RIGHT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (downValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 0;
                            eventData.inputCode = G_DPAD_DOWN_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (leftValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 0;
                            eventData.inputCode = G_DPAD_LEFT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                    break;

                    case 2: // RIGHT
                    case 90:
                    {
                        pController->mutex.LockSyncWrite();
                        int upValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                        int rightValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                        int downValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                        int leftValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();

                        if (rightValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 1;
                            eventData.inputCode = G_DPAD_RIGHT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (upValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 0;
                            eventData.inputCode = G_DPAD_UP_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (downValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 0;
                            eventData.inputCode = G_DPAD_DOWN_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (leftValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 0;
                            eventData.inputCode = G_DPAD_LEFT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                    break;

                    case 3: // RIGHT-DOWN
                    case 135:
                    {
                        pController->mutex.LockSyncWrite();
                        int upValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                        int rightValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                        int downValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                        int leftValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (rightValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 1;
                            eventData.inputCode = G_DPAD_RIGHT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (downValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 1;
                            eventData.inputCode = G_DPAD_DOWN_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (upValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 0;
                            eventData.inputCode = G_DPAD_UP_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }


                        if (leftValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 0;
                            eventData.inputCode = G_DPAD_LEFT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                    break;

                    case 4: // DOWN
                    case 180:
                    {
                        pController->mutex.LockSyncWrite();
                        int upValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                        int rightValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                        int downValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                        int leftValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (downValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 1;
                            eventData.inputCode = G_DPAD_DOWN_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (upValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 0;
                            eventData.inputCode = G_DPAD_UP_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (rightValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 0;
                            eventData.inputCode = G_DPAD_RIGHT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (leftValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 0;
                            eventData.inputCode = G_DPAD_LEFT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                    break;

                    case 5: // DOWN-LEFT
                    case 225:
                    {
                        pController->mutex.LockSyncWrite();
                        int upValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                        int rightValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                        int downValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                        int leftValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (downValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 1;
                            eventData.inputCode = G_DPAD_DOWN_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (leftValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 1;
                            eventData.inputCode = G_DPAD_LEFT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (upValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 0;
                            eventData.inputCode = G_DPAD_UP_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (rightValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 0;
                            eventData.inputCode = G_DPAD_RIGHT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                    break;

                    case 6: // LEFT
                    case 270:
                    {
                        pController->mutex.LockSyncWrite();
                        int upValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                        int rightValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                        int downValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                        int leftValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (leftValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 1;
                            eventData.inputCode = G_DPAD_LEFT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (upValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 0;
                            eventData.inputCode = G_DPAD_UP_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (rightValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 0;
                            eventData.inputCode = G_DPAD_RIGHT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (downValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 0;
                            eventData.inputCode = G_DPAD_DOWN_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                    break;

                    case 7: // LEFT-UP
                    case 315:
                    {
                        pController->mutex.LockSyncWrite();
                        int upValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                        int rightValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                        int downValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                        int leftValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (leftValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 1;
                            eventData.inputCode = G_DPAD_LEFT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (upValue != 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 1;
                            eventData.inputCode = G_DPAD_UP_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (rightValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 0;
                            eventData.inputCode = G_DPAD_RIGHT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (downValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 0;
                            eventData.inputCode = G_DPAD_DOWN_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                    break;

                    case -45: // Released
                    case 8:
                    case 360:
                    {
                        pController->mutex.LockSyncWrite();
                        int upValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                        int rightValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                        int downValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                        int leftValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                        eventData.isConnected = 1;
                        eventData.controllerID = pController->pControllers[controllerIndex].controllerID;
                        eventData.controllerIndex = controllerIndex;
                        pController->mutex.UnlockSyncWrite();
                        if (upValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN] = 0;
                            eventData.inputCode = G_DPAD_UP_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_UP_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (rightValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN] = 0;
                            eventData.inputCode = G_DPAD_RIGHT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_RIGHT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (downValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN] = 0;
                            eventData.inputCode = G_DPAD_DOWN_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_DOWN_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }

                        if (leftValue == 1)
                        {
                            pController->mutex.LockSyncWrite();
                            pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN] = 0;
                            eventData.inputCode = G_DPAD_LEFT_BTN;
                            eventData.inputValue = pController->pControllers[controllerIndex].controllerInputs[G_DPAD_LEFT_BTN];
                            pController->mutex.UnlockSyncWrite();

                            l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERBUTTONVALUECHANGED, eventData);
                            pController->Push(l_GEvent);
                        }
                    }
                    }
                    break;
                }
                break;
                }
            }
        }
    }

    static void GamepadWasAdded(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef device)
    {
        //NSLog(@"Gamepad was plugged in");
        id manager = (id)inContext;
        G_OBJC_DATA_MEMBERS_TYPE(HIDMANAGER)& managerDataMembers = G_OBJC_GET_DATA_MEMBERS(HIDMANAGER, manager);
        GW::I::GGeneralController* pController = managerDataMembers.pController;
        if (pController->isRunning)
        {
            GW::GEvent l_GEvent;
            pController->mutex.LockSyncWrite();
            int controllerIndex = pController->FindEmptyControllerIndex(G_MAX_CONTROLLER_COUNT, pController->pControllers);
            if (controllerIndex != -1)
            {
                IOReturn res = IOHIDDeviceOpen(device, kIOHIDOptionsTypeNone);
                if (kIOReturnSuccess == res)
                {
                    CFStringRef productKey;
                    productKey = (CFStringRef)IOHIDDeviceGetProperty(device, CFSTR(kIOHIDProductKey));
                    
                    uint32_t vendorID;
                    GW::INPUT::GControllerType controllerID;
                    // IOHIDeviceGetProperty Returns a CFTyperef based on the CString passed in. CFNumberGetValue is used to retrieve the value.
                    CFNumberGetValue((CFNumberRef)IOHIDDeviceGetProperty(device, CFSTR(kIOHIDVendorIDKey)), kCFNumberSInt32Type, &vendorID);
                    
                    switch (vendorID)
                    {
                    case G_SONY_VENDOR_ID:
                    {
                        controllerID = GW::INPUT::GControllerType::PS4;

                        CFStringRef usbProperty = (CFStringRef)IOHIDDeviceGetProperty(device, CFSTR("USB Product Name"));
                        
                        if (usbProperty == nil) // usbProperty will be nil if the device is not connected via USB.
                        {
                            pController->pControllers[controllerIndex].codeMapping = G_CODE_MAPPING_PS4_WIRELESS;
                            pController->pControllers[controllerIndex].axisMapping = G_MAC_AXIS_MAPPING_PS4_WIRELESS;
                        }
                        else
                        {
                            pController->pControllers[controllerIndex].codeMapping = G_CODE_MAPPING_PS4_WIRED;
                            pController->pControllers[controllerIndex].axisMapping = G_MAC_AXIS_MAPPING_PS4_WIRED;
                        }

                        break;
                    }

                    case G_MICROSOFT_VENDOR_ID:
                    {
                        const char* productName = CFStringGetCStringPtr(productKey, kCFStringEncodingMacRoman);
                        
                        if (strstr(productName, "Wireless") != nullptr)
                        {
                            controllerID = GW::INPUT::GControllerType::XBOXONE;
                            pController->pControllers[controllerIndex].codeMapping = G_CODE_MAPPING_XBOXONE_WIRELESS;
                            pController->pControllers[controllerIndex].axisMapping = G_MAC_AXIS_MAPPING_XBOXONE_WIRELESS;
                        }
                        else if (strstr(productName, "One") != nullptr)
                        {
                            controllerID = GW::INPUT::GControllerType::XBOXONE;
                            pController->pControllers[controllerIndex].codeMapping = G_CODE_MAPPING_XBOXONE_WIRED;
                            pController->pControllers[controllerIndex].axisMapping = G_MAC_AXIS_MAPPING_XBOXONE_WIRED;
                        }
                        else
                        {
                            controllerID = GW::INPUT::GControllerType::XBOX360;
                            pController->pControllers[controllerIndex].codeMapping = G_CODE_MAPPING_XBOX360;
                            pController->pControllers[controllerIndex].axisMapping = G_MAC_AXIS_MAPPING_XBOX360;
                        }
                        
                        break;
                    }

                    default:
                    {
                        const char* productName = CFStringGetCStringPtr(productKey, kCFStringEncodingMacRoman);
                        
                        if (strstr(productName, "Xbox") != nullptr)
                        {
                            controllerID = GW::INPUT::GControllerType::XBOX360;
                            pController->pControllers[controllerIndex].codeMapping = G_CODE_MAPPING_XBOX360;
                            pController->pControllers[controllerIndex].axisMapping = G_MAC_AXIS_MAPPING_XBOX360;
                        }
                        else
                        {
                            controllerID = GW::INPUT::GControllerType::GENERAL;
                            pController->pControllers[controllerIndex].codeMapping = G_CODE_MAPPING_GENERAL;
                            pController->pControllers[controllerIndex].axisMapping = G_MAC_AXIS_MAPPING_GENERAL;
                        }
                        
                        break;
                    }
                    }
                    
                    pController->pControllers[controllerIndex].controllerID = controllerID;
                    pController->pControllers[controllerIndex].isConnected = 1;
                    pController->pControllers[controllerIndex].device = device;

                    // send controller connected event
                    GW::I::GControllerInterface::EVENT_DATA eventData;

                    eventData.controllerIndex = controllerIndex;
                    eventData.inputCode = 0;
                    eventData.inputValue = 0;
                    eventData.isConnected = 1;
                    eventData.controllerID = pController->pControllers[controllerIndex].controllerID;

                    l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERCONNECTED, eventData);
                    pController->Push(l_GEvent);

                    IOHIDDeviceRegisterInputValueCallback(device, Handle_IOHIDDeviceInputValueCallback, (__bridge void*)manager);
                }
            }
            pController->mutex.UnlockSyncWrite();
        }
    }

    static void GamepadWasRemoved(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef device)
    {
        // NSLog(@"Gamepad was unplugged");
        IOHIDDeviceClose(device, kIOHIDOptionsTypeNone);
        id manager = (id)inContext;
        G_OBJC_DATA_MEMBERS_TYPE(HIDMANAGER)& managerDataMembers = G_OBJC_GET_DATA_MEMBERS(HIDMANAGER, manager);
        GW::I::GGeneralController* pController = managerDataMembers.pController;
        if (pController->isRunning)
        {
            GW::GEvent l_GEvent;
            pController->mutex.LockSyncWrite();
            int controllerIndex = 0;
            for (; controllerIndex < G_MAX_CONTROLLER_COUNT; ++controllerIndex)
            {
                if (device == pController->pControllers[controllerIndex].device)
                {
                    pController->pControllers[controllerIndex].isConnected = 0;

                    // send controller connected event
                    GW::I::GControllerInterface::EVENT_DATA eventData;

                    eventData.controllerIndex = controllerIndex;
                    eventData.inputCode = 0;
                    eventData.inputValue = 0;
                    eventData.isConnected = 0;
                    eventData.controllerID = pController->pControllers[controllerIndex].controllerID;

                    l_GEvent.Write(GW::I::GControllerInterface::Events::CONTROLLERCONNECTED, eventData);
                    pController->Push(l_GEvent);
                }
            }
            pController->mutex.UnlockSyncWrite();
        }
    }

    // This Function creates and return an array of dictionaries used to match connected devices
    static CFMutableArrayRef CreateHIDManagerCriteria()
    {
        // create a dictionary to add usage page/usages to
        UInt32 usagePage = kHIDPage_GenericDesktop;
        UInt32 joyUsage = kHIDUsage_GD_Joystick;
        UInt32 gamepadUsage = kHIDUsage_GD_GamePad;
        UInt32 mutiaxisUsage = kHIDUsage_GD_MultiAxisController;

        CFMutableDictionaryRef joyDictionary = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

        CFMutableDictionaryRef gamepadDictionary = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

        CFMutableDictionaryRef multiaxisDictionary = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

        CFNumberRef pageCFNumberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &usagePage);

        CFDictionarySetValue(joyDictionary, CFSTR(kIOHIDDeviceUsagePageKey), pageCFNumberRef);
        CFDictionarySetValue(gamepadDictionary, CFSTR(kIOHIDDeviceUsagePageKey), pageCFNumberRef);
        CFDictionarySetValue(multiaxisDictionary, CFSTR(kIOHIDDeviceUsagePageKey), pageCFNumberRef);
        CFRelease(pageCFNumberRef);

        CFNumberRef usageCFNumberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &joyUsage);
        CFDictionarySetValue(joyDictionary, CFSTR(kIOHIDDeviceUsageKey), usageCFNumberRef);

        usageCFNumberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &gamepadUsage);
        CFDictionarySetValue(gamepadDictionary, CFSTR(kIOHIDDeviceUsageKey), usageCFNumberRef);

        usageCFNumberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &mutiaxisUsage);
        CFDictionarySetValue(multiaxisDictionary, CFSTR(kIOHIDDeviceUsageKey), usageCFNumberRef);
        CFRelease(usageCFNumberRef);

        CFMutableArrayRef dictionaryCFArr = CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks);

        CFArrayAppendValue(dictionaryCFArr, joyDictionary);
        CFArrayAppendValue(dictionaryCFArr, gamepadDictionary);
        CFArrayAppendValue(dictionaryCFArr, multiaxisDictionary);

        CFRelease(joyDictionary);
        CFRelease(gamepadDictionary);
        CFRelease(multiaxisDictionary);

        return dictionaryCFArr;
    }

    // HIDMANAGER Implementation

    G_OBJC_IMPLEMENTATION_DATA_MEMBERS_PROPERTY_METHOD(HIDMANAGER);

    G_OBJC_HEADER_INSTANCE_METHOD(HIDMANAGER, void, InitManagerAndRunLoop)
    {
        G_OBJC_DATA_MEMBERS_TYPE(HIDMANAGER)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(HIDMANAGER, self);
        IOHIDManagerRef& hidManager = selfDataMembers.hidManager;

        hidManager = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDManagerOptionNone);

        IOHIDManagerRegisterDeviceMatchingCallback(hidManager, GamepadWasAdded, (__bridge void*)self);
        IOHIDManagerRegisterDeviceRemovalCallback(hidManager, GamepadWasRemoved, (__bridge void*)self);

        IOHIDManagerScheduleWithRunLoop(hidManager, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);

        CFMutableArrayRef criteria = CreateHIDManagerCriteria();
        IOHIDManagerSetDeviceMatchingMultiple(hidManager, criteria);

        CFRelease(criteria);

        IOHIDManagerOpen(hidManager, kIOHIDOptionsTypeNone);
        CFTimeInterval timer = 1;
        Boolean runLoopReturn = true;
        // The run loop will exit once a second to check and see if GController is still running
        // The run loop is used for processing the events for the IOHIDManager it work similar to [NSAPP run]
        GW::I::GGeneralController* pController = selfDataMembers.pController;
        while (pController->isRunning)
            CFRunLoopRunInMode(kCFRunLoopDefaultMode, timer, runLoopReturn);
    }

    // HIDMANAGER Implementation End
}



