#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include "../GInput/GInputTableRouting.hpp"
#include "../../../Interface/System/GDaemon.h"
#include "../../../Interface/Core/GEventReceiver.h"

namespace GW
{
	namespace I
	{
		class GBufferedInputImplementation : public virtual GBufferedInputInterface,
			public GEventGeneratorImplementation
		{
		private:
			unsigned int keyMask;
			Display* display = nullptr;
			Window window = 0;
			GW::SYSTEM::GDaemon inputDaemon;
			GW::CORE::GEventReceiver watcher;
			unsigned int keyStates[256]; // 1 for on, 0 for off
			unsigned int buttonStates[32]; // 1 for on, 0 for off
			int lastMouseX = 0, lastMouseY = 0;
		public:
			~GBufferedInputImplementation()
			{
				inputDaemon = nullptr;
			}

			GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _uwh)
			{
				if (!_uwh.window || !_uwh.display)
					return GReturn::INVALID_ARGUMENT;

				window = *((Window*)(_uwh.window));
				display = ((Display*)(_uwh.display));
				keyMask = 0;

				XInitThreads();// daemon run on another thread
				return inputDaemon.Create(G_BUFFEREDINPUT_OPERATION_INTERVAL, [&]()
					{
						int _code = -1;
						GEvent m_GEvent;
						Events m_Event = Events::Invalid;
						EVENT_DATA m_EventData;
						Window a, b;

						LockAsyncRead();
						XQueryPointer(display, window, &a, &b, &m_EventData.screenX, &m_EventData.screenY, &m_EventData.x, &m_EventData.y, &m_EventData.keyMask);

						char keys_return[32];
						//Get the current state of all keys.
						XQueryKeymap(display, keys_return);
						UnlockAsyncRead();
						//Loop through all the keys and check to see if they match the saved state of all the keys.
						for (unsigned int i = 5; i < 126; ++i)
						{
							m_EventData.data = Keycodes[i];
							//Save the state of current key.
							unsigned int key = keys_return[(i >> 3)] & (1 << (i & 7));
							//If a key does not match send an event saying it has been updated.
							if (key != keyStates[m_EventData.data])
							{
								if (key == 0)
								{
									m_Event = Events::KEYRELEASED;
								}
								else
								{
									m_Event = Events::KEYPRESSED;
								}

								//Send the event to all registered listeners.
								m_GEvent.Write(m_Event, m_EventData);
								Push(m_GEvent);
								keyStates[m_EventData.data] = key;
							}
						}

						if (m_EventData.keyMask & Button1Mask)
						{
							if (!keyStates[G_BUTTON_LEFT])
							{
								m_EventData.data = G_BUTTON_LEFT;
								m_Event = Events::BUTTONPRESSED;
								keyStates[G_BUTTON_LEFT] = 1;
								//Send the event to all registered listeners.
								m_GEvent.Write(m_Event, m_EventData);
								Push(m_GEvent);
							}
						}
						else
						{
							if (keyStates[G_BUTTON_LEFT])
							{
								m_EventData.data = G_BUTTON_LEFT;
								m_Event = Events::BUTTONRELEASED;
								keyStates[G_BUTTON_LEFT] = 0;
								//Send the event to all registered listeners.
								m_GEvent.Write(m_Event, m_EventData);
								Push(m_GEvent);
							}
						}

						if (m_EventData.keyMask & Button2Mask)
						{
							if (!keyStates[G_BUTTON_MIDDLE])
							{
								m_EventData.data = G_BUTTON_MIDDLE;
								m_Event = Events::BUTTONPRESSED;
								keyStates[G_BUTTON_MIDDLE] = 1;
								//Send the event to all registered listeners.
								m_GEvent.Write(m_Event, m_EventData);
								Push(m_GEvent);
							}
						}
						else
						{
							if (keyStates[G_BUTTON_MIDDLE])
							{
								m_EventData.data = G_BUTTON_MIDDLE;
								m_Event = Events::BUTTONRELEASED;
								keyStates[G_BUTTON_MIDDLE] = 0;
								//Send the event to all registered listeners.
								m_GEvent.Write(m_Event, m_EventData);
								Push(m_GEvent);
							}
						}

						if (m_EventData.keyMask & Button3Mask)
						{
							if (!keyStates[G_BUTTON_RIGHT])
							{
								m_EventData.data = G_BUTTON_RIGHT;
								m_Event = Events::BUTTONPRESSED;
								keyStates[G_BUTTON_RIGHT] = 1;
								//Send the event to all registered listeners.
								m_GEvent.Write(m_Event, m_EventData);
								Push(m_GEvent);
							}
						}
						else
						{
							if (keyStates[G_BUTTON_RIGHT])
							{
								m_EventData.data = G_BUTTON_RIGHT;
								m_Event = Events::BUTTONRELEASED;
								keyStates[G_BUTTON_RIGHT] = 0;
								//Send the event to all registered listeners.
								m_GEvent.Write(m_Event, m_EventData);
								Push(m_GEvent);
							}
						}

						if ((m_EventData.x - lastMouseX) != 0 || (m_EventData.y - lastMouseY) != 0)
						{
							m_EventData.data = G_KEY_UNKNOWN;
							m_Event = Events::MOUSEMOVE;
							//Send the event to all registered listeners.
							m_GEvent.Write(m_Event, m_EventData);
							Push(m_GEvent);

							lastMouseX = m_EventData.x;
							lastMouseY = m_EventData.y;
						}
					});
			}

			GReturn Create(GW::SYSTEM::GWindow _window)
			{
				if (!_window)
					return GReturn::INVALID_ARGUMENT;

				GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE uwh;
				GReturn code = _window.GetWindowHandle(uwh);
				if (G_FAIL(code))
					return code;

				// stop if window is killed
				watcher.Create(_window, [&]() {
					if (+watcher.Find(GW::SYSTEM::GWindow::Events::DESTROY, true))
						inputDaemon = nullptr;
					});

				return Create(uwh);
			}
		};
	}
}
