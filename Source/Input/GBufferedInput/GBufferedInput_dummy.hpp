namespace GW
{
	namespace I
	{
		class GBufferedInputImplementation :	public virtual GBufferedInputInterface,
												public GEventGeneratorImplementation
		{
		public:
			GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _uwh) { 
				return GReturn::INTERFACE_UNSUPPORTED; 
			}
			GReturn Create(GW::SYSTEM::GWindow _window) { 
				return GReturn::INTERFACE_UNSUPPORTED; 
			}
		};
	}
}