#ifdef __OBJC__
@import Foundation;
@import Cocoa;
#endif
#include "../GInput/GInputTableRouting.hpp"
#include "../../Shared/macutils.h"

namespace GW
{
    namespace I
    {
        class GBufferedInputImplementation;
    }
}

namespace internal_gw
{
    // GResponder Interface

    // Data members of GResponder
    G_OBJC_DATA_MEMBERS_STRUCT(GResponder)
    {
        unsigned int keyMask;
        int lastMouseX = 0, lastMouseY = 0;
        int screenMouseX = 0, screenMouseY = 0;
        int relativeMouseX = 0, relativeMouseY = 0;
        GW::I::GBufferedInputImplementation* pBufferedInput;
        NSWindow* targetWindow; // so we can get the height of the client area
        GW::GEvent gevent;
        char pressed[256] = { 0, }; // since isARepeat doesn't seem to work...
    };

    // Forward declarations of GResponder methods
    G_OBJC_HEADER_DATA_MEMBERS_PROPERTY_METHOD(GResponder);

    G_OBJC_HEADER_INSTANCE_METHOD(GResponder, bool, acceptFirstResponder);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, bool, acceptsFirstMouse, NSEvent* event);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, flagsChanged, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, keyDown, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, keyUp, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, mouseDown, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, mouseUp, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, mouseMoved, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, mouseDragged, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, rightMouseDragged, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, otherMouseDragged, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, rightMouseDown, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, rightMouseUp, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, otherMouseDown, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, otherMouseUp, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, scrollWheel, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, GetKeyMask, NSEvent* theEvent);
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, GetMouseLocation, NSEvent* theEvent);

    // Creates the GResponder class at runtime when G_OBJC_GET_CLASS(GResponder) is called
    G_OBJC_CLASS_BEGIN(GResponder, NSResponder)
    {
        G_OBJC_CLASS_DATA_MEMBERS_PROPERTY(GResponder);

        G_OBJC_CLASS_METHOD(GResponder, acceptFirstResponder, "B@:");
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, acceptsFirstMouse, "B@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, flagsChanged, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, keyDown, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, keyUp, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, mouseDown, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, mouseUp, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, mouseMoved, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, mouseDragged, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, rightMouseDragged, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, otherMouseDragged, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, rightMouseDown, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, rightMouseUp, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, otherMouseDown, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, otherMouseUp, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, scrollWheel, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, GetKeyMask, "v@:@", :);
        G_OBJC_CLASS_METHOD_WITH_ARGUMENTS(GResponder, GetMouseLocation, "v@:@", :);
    }
    G_OBJC_CLASS_END(GResponder)

    // GResponder Interface End
}

namespace GW
{
    namespace I
    {
        class GBufferedInputImplementation :    public virtual GBufferedInputInterface,
                                                public GEventGeneratorImplementation
        {
        private:
            NSWindow* currentResponder;
            id responder = [internal_gw::G_OBJC_GET_CLASS(GResponder) alloc];
        public:
            ~GBufferedInputImplementation()
            {
                if (responder)
                {
                    [responder release] ;
                    responder = nil;
                }
            }

            GReturn Create(GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE _uwh)
            {
                if (!_uwh.window)
                    return GReturn::INVALID_ARGUMENT;
                //Need to convert _data back into an NSWindow*.
                //NSWindow* currentResponder = ((__bridge NSWindow*)_data);
                currentResponder = ((__bridge NSWindow*)_uwh.window);

                //We only want to process the message and pass it on. So if there is already
                //a responder we set our responders next responder to be the current next responder.
                [responder setNextResponder : currentResponder.nextResponder];

                //We then set out responder to the next responder of the window.
                [currentResponder setNextResponder : responder] ;

                //We also need to make our responder the first responder of the window.
                [currentResponder makeFirstResponder : responder] ;

                internal_gw::G_OBJC_DATA_MEMBERS_TYPE(GResponder)& responderDataMembers = internal_gw::G_OBJC_GET_DATA_MEMBERS(GResponder, responder);
                responderDataMembers.pBufferedInput = this;
                responderDataMembers.targetWindow = currentResponder;

                RUN_ON_UI_THREAD(^ {
                    //In order to get mouse button presses we need to set our responder to be
                    //The next responder in the contentView as well.
                    [currentResponder.contentView setNextResponder : responder] ;
                    });
                
                //Enable responder to accept mouse move events. By default it doesn't.
                [currentResponder setAcceptsMouseMovedEvents:YES];

                return GReturn::SUCCESS;
            }

            GReturn Create(GW::SYSTEM::GWindow _window)
            {
                if (!_window)
                    return GReturn::INVALID_ARGUMENT;

                GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE uwh;
                GReturn code = _window.GetWindowHandle(uwh);
                if (G_FAIL(code))
                    return code;

                return Create(uwh);
            }
        };
    }
}

namespace internal_gw
{
    // GResponder Implementation

    G_OBJC_IMPLEMENTATION_DATA_MEMBERS_PROPERTY_METHOD(GResponder);

    G_OBJC_HEADER_INSTANCE_METHOD(GResponder, bool, acceptFirstResponder) { return YES; }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, bool, acceptsFirstMouse, NSEvent* event) { return YES; }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, flagsChanged, NSEvent* theEvent)
    {
        // these keys are treated a bit differently and need a custom handler
        NSUInteger code = [theEvent keyCode];
        NSUInteger gwcode = GW::I::Keycodes[code];
        NSUInteger xflags = [theEvent modifierFlags];
        int send_event = 0; // only send if non-zero
        
        if (gwcode == G_KEY_LEFTSHIFT || gwcode == G_KEY_RIGHTSHIFT)
             send_event = (xflags & NSEventModifierFlagShift) ? 1 : -1;
        else if (gwcode == G_KEY_LEFTALT || gwcode == G_KEY_RIGHTALT)
             send_event = (xflags & NSEventModifierFlagOption) ? 1 : -1;
        else if (gwcode == G_KEY_LEFTCONTROL || gwcode == G_KEY_RIGHTCONTROL)
             send_event = (xflags & NSEventModifierFlagControl) ? 1 : -1;
        else if (gwcode == G_KEY_NUMLOCK)
             send_event = (xflags & NSEventModifierFlagNumericPad) ? 1 : -1;
        else if (gwcode == G_KEY_COMMAND)
             send_event = (xflags & NSEventModifierFlagCommand) ? 1 : -1;
        else if (gwcode == G_KEY_FUNCTION)
             send_event = (xflags & NSEventModifierFlagFunction) ? 1 : -1;
        else if (gwcode == G_KEY_CAPSLOCK) // key up not sent until pressed again
             send_event = (xflags & NSEventModifierFlagCapsLock) ? 1 : -1;
        // filter repeated key press
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        if (send_event == 1 && selfDataMembers.pressed[gwcode] == 0)
            selfDataMembers.pressed[gwcode] = 1; // this is going to be sent for the first time
        else if (send_event == 1 && selfDataMembers.pressed[gwcode] == 1)
            send_event = 0; // ignore this repeat
        else if(send_event == -1 && selfDataMembers.pressed[gwcode] == 1)
            selfDataMembers.pressed[gwcode] = 0; // allow this message to be resent
        
        if (send_event != 0)
        {
            GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
            //Get the key from the static list of keys.
            _dataStruct.data = gwcode;
            //Call the GetKeyMask Function passing the event passed to this functions.
            G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
            //Set the keymask.
            _dataStruct.keyMask = selfDataMembers.keyMask;
            //Get the mouse position relative to the window.
            G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
            _dataStruct.x = selfDataMembers.relativeMouseX;
            _dataStruct.y = selfDataMembers.relativeMouseY;
            _dataStruct.screenX = selfDataMembers.screenMouseX;
            _dataStruct.screenY = selfDataMembers.screenMouseY;
            
            //Send off the event.
            GW::GEvent& gevent = selfDataMembers.gevent;
            gevent.Write(
                (send_event > 0) ?  GW::I::GBufferedInputInterface::Events::KEYPRESSED
                                 :  GW::I::GBufferedInputInterface::Events::KEYRELEASED,
                _dataStruct);

            GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
            pBufferedInput->Push(gevent);
        }

        if ([self nextResponder] != nil)
            [[self nextResponder]keyDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, keyDown, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        //Get the key from the static list of keys.
        _dataStruct.data = GW::I::Keycodes [[theEvent keyCode]];
        // ignore repeats
        if (selfDataMembers.pressed[_dataStruct.data] == 0)
        {
            //Call the GetKeyMask Function passing the event passed to this functions.
            G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
            //Set the keymask.
            _dataStruct.keyMask = selfDataMembers.keyMask;
            //Get the mouse position relative to the window.
            G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
            _dataStruct.x = selfDataMembers.relativeMouseX;
            _dataStruct.y = selfDataMembers.relativeMouseY;
            _dataStruct.screenX = selfDataMembers.screenMouseX;
            _dataStruct.screenY = selfDataMembers.screenMouseY;
            //Send off the event.
            GW::GEvent& gevent = selfDataMembers.gevent;
            gevent.Write(GW::I::GBufferedInputInterface::Events::KEYPRESSED, _dataStruct);
            GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
            selfDataMembers.pressed[_dataStruct.data] = 1; // filter repeats
            pBufferedInput->Push(gevent);
        }
        
        if ([self nextResponder] != nil)
            [[self nextResponder]keyDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, keyUp, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        _dataStruct.data = GW::I::Keycodes [[theEvent keyCode]];
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
        _dataStruct.keyMask = selfDataMembers.keyMask;
        //Get the mouse position relative to the window.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
        _dataStruct.x = selfDataMembers.relativeMouseX;
        _dataStruct.y = selfDataMembers.relativeMouseY;
        _dataStruct.screenX = selfDataMembers.screenMouseX;
        _dataStruct.screenY = selfDataMembers.screenMouseY;

        //Send off the event.
        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(GW::I::GBufferedInputInterface::Events::KEYRELEASED, _dataStruct);
        GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
        pBufferedInput->Push(gevent);
        // allow this event to be re-triggered
        selfDataMembers.pressed[_dataStruct.data] = 0;

        if ([self nextResponder] != nil)
            [[self nextResponder]keyUp:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, mouseDown, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        _dataStruct.data = G_BUTTON_LEFT;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
        _dataStruct.keyMask = selfDataMembers.keyMask;
        //Get the mouse position relative to the window.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
        _dataStruct.x = selfDataMembers.relativeMouseX;
        _dataStruct.y = selfDataMembers.relativeMouseY;
        _dataStruct.screenX = selfDataMembers.screenMouseX;
        _dataStruct.screenY = selfDataMembers.screenMouseY;

        //Send off the event.
        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(GW::I::GBufferedInputInterface::Events::BUTTONPRESSED, _dataStruct);

        GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
        pBufferedInput->Push(gevent);

        if ([self nextResponder] != nil)
            [[self nextResponder]mouseDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, mouseUp, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        _dataStruct.data = G_BUTTON_LEFT;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
        _dataStruct.keyMask = selfDataMembers.keyMask;
        //Get the mouse position relative to the window.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
        _dataStruct.x = selfDataMembers.relativeMouseX;
        _dataStruct.y = selfDataMembers.relativeMouseY;
        _dataStruct.screenX = selfDataMembers.screenMouseX;
        _dataStruct.screenY = selfDataMembers.screenMouseY;

        //Send off the event.
        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(GW::I::GBufferedInputInterface::Events::BUTTONRELEASED, _dataStruct);

        GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
        pBufferedInput->Push(gevent);

        if ([self nextResponder] != nil)
            [[self nextResponder]mouseUp:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, mouseMoved, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        _dataStruct.data = G_KEY_UNKNOWN;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
        _dataStruct.keyMask = selfDataMembers.keyMask;
        //Get the mouse position relative to the window.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
        _dataStruct.x = selfDataMembers.relativeMouseX;
        _dataStruct.y = selfDataMembers.relativeMouseY;
        _dataStruct.screenX = selfDataMembers.screenMouseX;
        _dataStruct.screenY = selfDataMembers.screenMouseY;

        if ((_dataStruct.x - selfDataMembers.lastMouseX) != 0 ||
            (_dataStruct.y - selfDataMembers.lastMouseY) != 0)
        {
            //Send off the event.
            GW::GEvent& gevent = selfDataMembers.gevent;
            gevent.Write(GW::I::GBufferedInputInterface::Events::MOUSEMOVE, _dataStruct);
            
            GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
            pBufferedInput->Push(gevent);
            
            selfDataMembers.lastMouseX = _dataStruct.x;
            selfDataMembers.lastMouseY = _dataStruct.y;
        }
        
        if ([self nextResponder] != nil)
            [[self nextResponder]mouseMoved:theEvent];
    }
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, mouseDragged, NSEvent* theEvent)
    {
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, mouseMoved, theEvent);
    }
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, rightMouseDragged, NSEvent* theEvent)
    {
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, mouseMoved, theEvent);
    }
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, otherMouseDragged, NSEvent* theEvent)
    {
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, mouseMoved, theEvent);
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, rightMouseDown, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        _dataStruct.data = G_BUTTON_RIGHT;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
        _dataStruct.keyMask = selfDataMembers.keyMask;
        //Get the mouse position relative to the window.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
        _dataStruct.x = selfDataMembers.relativeMouseX;
        _dataStruct.y = selfDataMembers.relativeMouseY;
        _dataStruct.screenX = selfDataMembers.screenMouseX;
        _dataStruct.screenY = selfDataMembers.screenMouseY;

        //Send off the event.
        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(GW::I::GBufferedInputInterface::Events::BUTTONPRESSED, _dataStruct);

        GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
        pBufferedInput->Push(gevent);

        if ([self nextResponder] != nil)
            [[self nextResponder]rightMouseDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, rightMouseUp, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        _dataStruct.data = G_BUTTON_RIGHT;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
        _dataStruct.keyMask = selfDataMembers.keyMask;
        //Get the mouse position relative to the window.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
        _dataStruct.x = selfDataMembers.relativeMouseX;
        _dataStruct.y = selfDataMembers.relativeMouseY;
        _dataStruct.screenX = selfDataMembers.screenMouseX;
        _dataStruct.screenY = selfDataMembers.screenMouseY;

        //Send off the event.
        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(GW::I::GBufferedInputInterface::Events::BUTTONRELEASED, _dataStruct);

        GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
        pBufferedInput->Push(gevent);

        if ([self nextResponder] != nil)
            [[self nextResponder]rightMouseUp:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, otherMouseDown, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        _dataStruct.data = G_BUTTON_MIDDLE;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
        _dataStruct.keyMask = selfDataMembers.keyMask;
        //Get the mouse position relative to the window.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
        _dataStruct.x = selfDataMembers.relativeMouseX;
        _dataStruct.y = selfDataMembers.relativeMouseY;
        _dataStruct.screenX = selfDataMembers.screenMouseX;
        _dataStruct.screenY = selfDataMembers.screenMouseY;

        //Send off the event.
        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(GW::I::GBufferedInputInterface::Events::BUTTONPRESSED, _dataStruct);

        GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
        pBufferedInput->Push(gevent);

        if ([self nextResponder] != nil)
            [[self nextResponder]otherMouseDown:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, otherMouseUp, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        _dataStruct.data = G_BUTTON_MIDDLE;
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
        _dataStruct.keyMask = selfDataMembers.keyMask;
        //Get the mouse position relative to the window.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
        _dataStruct.x = selfDataMembers.relativeMouseX;
        _dataStruct.y = selfDataMembers.relativeMouseY;
        _dataStruct.screenX = selfDataMembers.screenMouseX;
        _dataStruct.screenY = selfDataMembers.screenMouseY;

        //Send off the event.
        GW::GEvent& gevent = selfDataMembers.gevent;
        gevent.Write(GW::I::GBufferedInputInterface::Events::BUTTONRELEASED, _dataStruct);

        GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
        pBufferedInput->Push(gevent);

        if ([self nextResponder] != nil)
            [[self nextResponder]otherMouseUp:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, scrollWheel, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        GW::I::GBufferedInputImplementation::EVENT_DATA _dataStruct;
        _dataStruct.data = 0;
        //Check wether the its a scroll up or down event.
        if ([theEvent scrollingDeltaY] > 0)
        {
            _dataStruct.data = G_MOUSE_SCROLL_UP;
        }
        if ([theEvent scrollingDeltaY] < 0)
        {
            _dataStruct.data = G_MOUSE_SCROLL_DOWN;
        }
        //Get the keymask using the getkeymask function/
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetKeyMask, theEvent);
        //Set the keymask.
        _dataStruct.keyMask = selfDataMembers.keyMask;
        //Get the mouse position relative to the window.
        G_OBJC_CALL_METHOD_WITH_ARGUMENTS(GResponder, self, GetMouseLocation, theEvent);
        _dataStruct.x = selfDataMembers.relativeMouseX;
        _dataStruct.y = selfDataMembers.relativeMouseY;
        _dataStruct.screenX = selfDataMembers.screenMouseX;
        _dataStruct.screenY = selfDataMembers.screenMouseY;
        //Make sure theres data to send and send the data to all registered listeners.
        if (_dataStruct.data)
        {
            //Send off the event.
            GW::GEvent& gevent = selfDataMembers.gevent;
            gevent.Write(GW::I::GBufferedInputInterface::Events::MOUSESCROLL, _dataStruct);

            GW::I::GBufferedInputImplementation* pBufferedInput = selfDataMembers.pBufferedInput;
            pBufferedInput->Push(gevent);

        }
        if ([self nextResponder] != nil)
            [[self nextResponder]scrollWheel:theEvent];
    }

    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, GetKeyMask, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        unsigned int& keyMask = selfDataMembers.keyMask;
        keyMask = 0;
        //Get the unsigned int of all the modifier flags.
        NSUInteger flags = [theEvent modifierFlags];
        //Check individual modifier flags and turn them on respectivly to our keymask(unsigned int).
        if (flags & NSEventModifierFlagShift)
        {
            G_TURNON_BIT(keyMask, G_MASK_SHIFT);
        }
        if (flags & NSEventModifierFlagOption)
        {
            G_TURNON_BIT(keyMask, G_MASK_ALT);
        }
        if (flags & NSEventModifierFlagCommand)
        {
            G_TURNON_BIT(keyMask, G_MASK_COMMAND);
        }
        if (flags & NSEventModifierFlagControl)
        {
            G_TURNON_BIT(keyMask, G_MASK_CONTROL);
        }
        if (flags & NSEventModifierFlagCapsLock)
        {
            G_TURNON_BIT(keyMask, G_MASK_CAPS_LOCK);
        }
        if (flags & NSEventModifierFlagFunction)
        {
            G_TURNON_BIT(keyMask, G_MASK_FUNCTION);
        }
        if (flags & NSEventModifierFlagNumericPad)
        {
            G_TURNON_BIT(keyMask, G_MASK_NUM_LOCK);
        }
    }
    G_OBJC_HEADER_INSTANCE_METHOD_WITH_ARGUMENTS(GResponder, void, GetMouseLocation, NSEvent* theEvent)
    {
        G_OBJC_DATA_MEMBERS_TYPE(GResponder)& selfDataMembers = G_OBJC_GET_DATA_MEMBERS(GResponder, self);
        NSRect rect = selfDataMembers.targetWindow.frame;
        NSRect contentRect = [selfDataMembers.targetWindow contentRectForFrameRect : rect];
        NSPoint mousePosition = [theEvent locationInWindow];
        selfDataMembers.relativeMouseX = mousePosition.x;
        selfDataMembers.relativeMouseY = contentRect.size.height - mousePosition.y;
        //Get the mouse position relative to the screen.
        CGSize screenSize = [[NSScreen mainScreen] frame].size;
        NSPoint screenMousePosition = [NSEvent mouseLocation];
        selfDataMembers.screenMouseX = screenMousePosition.x;
        selfDataMembers.screenMouseY = screenSize.height - screenMousePosition.y;
    }

    // GResponder Implementation End
}
