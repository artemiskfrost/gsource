//Get the Core
@import QuartzCore;
#include "../../Shared/macutils.h"
#include </usr/local/include/vulkan/vulkan.h>
#include </usr/local/include/vulkan/vulkan_macos.h>

namespace GW
{
    namespace I
    {
		class GVulkanSurfacePlatformFucts {
		public:
            //Get the Surface Extension for Macos
			const char* GetPlatformSurfaceExtension() {
				return VK_MVK_MACOS_SURFACE_EXTENSION_NAME;
			}
            //Get the Device Extension for Macos
            const char* GetPlatformDeviceExtension() {
                unsigned current = VK_HEADER_VERSION_COMPLETE;
#ifdef VK_MAKE_API_VERSION
                unsigned compatible = VK_MAKE_API_VERSION(0, 1, 2, 176);
#else
                unsigned compatible = VK_MAKE_VERSION(1, 2, 176);
#endif
                if (current >= compatible)
                    return "VK_KHR_portability_subset";
                else
                    return nullptr;
            }
            
			void PlatformOverrideInstanceCreate(VkInstanceCreateInfo& _create_info, uint32_t& _instanceExtensionCount, 
				const char*** _instanceExtensions, uint32_t& _instanceLayerCount, const char*** _instanceLayer) {

                _create_info.flags |= VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR;
                
                const char** newExtensions = new const char*[_create_info.enabledExtensionCount + 1];
                for (int i = 0; i < _create_info.enabledExtensionCount; ++i) {
                    newExtensions[i] = _create_info.ppEnabledExtensionNames[i];
                }

				delete[] * _instanceExtensions;
				*_instanceExtensions = newExtensions;

                newExtensions[_create_info.enabledExtensionCount] = VK_KHR_PORTABILITY_ENUMERATION_EXTENSION_NAME;
                _create_info.enabledExtensionCount = ++_instanceExtensionCount;
                _create_info.ppEnabledExtensionNames = newExtensions;
            }

			//Get the platform name
			char* GetPlatformWindowName(const GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& uwh) {
				__block char* instance_app_name = nullptr;
				//Make sure this is on the main thread
				RUN_ON_UI_THREAD(^ {
					//Get the proper Window Handles
					NSWindow * nsWindow = reinterpret_cast<NSWindow*>(uwh.window);

				//Get the Window Title & Length
				const char* window_title = [nsWindow.title UTF8String];
				int len = strlen(window_title);

				//Create the Window Title
				if (len) {
					instance_app_name = new char[len + 1];
					strcpy(instance_app_name, window_title);
				}

				//If above fails, make a title
				if (!instance_app_name) {
					//Default Window Title[22] (21 Characters, +1 for null terminator)
					instance_app_name = new char[22];
					strcpy(instance_app_name, "Gateware Application");
				}
					});

				//Return The Window Title
				return instance_app_name;
			}

			//Create the surface
			VkResult CreateVkSurfaceKHR(const VkInstance& vkInstance, const GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& uwh, VkSurfaceKHR& vkSurface) {
				
				//Create this as a block
				__block VkResult r = VK_ERROR_FEATURE_NOT_PRESENT;

				//Make sure this is on the main thread
				RUN_ON_UI_THREAD(^ {
					//Get NSWindow
					NSWindow * nsWindow = static_cast<NSWindow*>(uwh.window);

				//Get NSView and add a metal layer
				NSView* nsView = nsWindow.contentView;
				nsView.wantsLayer = YES;
				nsView.layer = [CAMetalLayer layer];

				//Setup Create Info
				VkMacOSSurfaceCreateInfoMVK create_info = {};
				create_info.sType = VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK;
				create_info.pView = nsView;
				create_info.flags = 0;
				create_info.pNext = VK_NULL_HANDLE;

				//Create the Surface
				r = vkCreateMacOSSurfaceMVK(vkInstance, &create_info, VK_NULL_HANDLE, &vkSurface);

				//Hi
				//FlushOnMacEvents();
					});
				return r;
			}

			VkResult PlatformDestroyGVulkanSurface(const std::function<void()>& callback) {
				//Cleanup Vulkan Surface
				callback();

				//Return Success
				return VK_SUCCESS;
			}
		};
    }
}
