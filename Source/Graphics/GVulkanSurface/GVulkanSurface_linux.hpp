//Get the Core
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_xlib.h>

//The core namespace to which all Gateware interfaces/structures/defines must belong.
namespace GW
{
	//The namespace to which all Gateware internal implementation must belong.
	namespace I
	{
		class GVulkanSurfacePlatformFucts {
		public:
			const char* GetPlatformSurfaceExtension() {
				return VK_KHR_XLIB_SURFACE_EXTENSION_NAME;
			}
			const char* GetPlatformDeviceExtension() {
				return nullptr;
			}

			void PlatformOverrideInstanceCreate(VkInstanceCreateInfo& _create_info, uint32_t& _instanceExtensionCount,
				const char*** _instanceExtensions, uint32_t& _instanceLayerCount, const char*** _instanceLayer) {
				//Hey there! Nothing to see here!...yet
			}

			char* GetPlatformWindowName(const GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& uwh) {
				//Setup Window and Display
				Display* dpy = static_cast<Display*>(uwh.display);
				Window wnd = *(static_cast<Window*>(uwh.window));

				//Get Window Title
				XTextProperty x11_window_title;
				int status = XGetWMName(dpy, wnd, &x11_window_title);

				//Create a Window Title Separate from X11 (Cause it needs to be `Free`d by X11, According to the docs)
				char* window_title = nullptr;
				uint32_t len = strlen((char*)x11_window_title.value);
				if (x11_window_title.value && len)
				{
					window_title = new char[len + 1];
					strcpy(window_title, (char*)x11_window_title.value);
				}

				//If Above Fails (status returned 0, x11 title is nullptr, x11 title is empty), Make a Default
				if (!window_title) {
					//Default Window Title[22] (21 Characters, +1 for null terminator)
					window_title = new char[22];
					strcpy(window_title, "Gateware Application");
				}

				return window_title;
			}

			VkResult CreateVkSurfaceKHR(const VkInstance& vkInstance, const GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE& uwh, VkSurfaceKHR& vkSurface) {

				//Setup Window and Display
				Window wnd = *(static_cast<Window*>(uwh.window));
				Display* dpy = static_cast<Display*>(uwh.display);

				VkXlibSurfaceCreateInfoKHR create_info = {};
				create_info.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
				create_info.window = wnd;
				create_info.dpy = dpy;
				create_info.flags = 0;
				create_info.pNext = VK_NULL_HANDLE;

				VkResult r = vkCreateXlibSurfaceKHR(vkInstance, &create_info, VK_NULL_HANDLE, &vkSurface);
				return r;
			}

			VkResult PlatformDestroyGVulkanSurface(const std::function<void()>& callback) {
				//Cleanup Vulkan Surface
				//CleanupVulkanSurface();
				callback();

				//Return Success
				return VK_SUCCESS;
			}
		};
	} // I
} //GW
