// Implementaion for GDirectX11Surface.h
// *IMPORTANT* End users of Gateware may ignore the contents of these files if they please.(Developers Only)
// File not included in Doxygen, these files will be compiled directly into associated header file when deployed
// Developers: Do NOT assume the contents of this file are within the scope of the related interface's namespace
// Developers: These are NOT cpp files do not use "using namespace" or unprotected global variables (prefer static members)
// Developers: If you MUST use a non-static global, use the "internal_gw" namespace to store them. (only on approval)

// Gateware platform specific implementations are seperated into different files to keep things clean and flexible
// As new platforms come online or are deprecated (or upgraded) we can modify their selection through this file
// When adding implementations please try to condense redundant file includes where possible.
// This will reduce size/redundancy when the library is tool compressed into single header form.
#if !defined(GATEWARE_ENABLE_GRAPHICS) || defined(GATEWARE_DISABLE_GDIRECTX11SURFACE) || defined(__APPLE__) || defined(__linux__) || \
    (defined(GATEWARE_ENABLE_GRAPHICS) && !defined(GATEWARE_DISABLE_GDIRECTX11SURFACE) && !defined(__APPLE__) && !defined(__linux__) && !defined(_WIN32))
    // Even if a platform does not support a library a dummy implementation must be present!
    #include "GDirectX11Surface_dummy.hpp"
#elif defined(_WIN32)
    #include <winapifamily.h>
    #if (WINAPI_FAMILY == WINAPI_FAMILY_DESKTOP_APP)
        #include "GDirectX11Surface_win32.hpp"
    #elif (WINAPI_FAMILY == WINAPI_FAMILY_APP)
        #include "GDirectX11Surface_uwp.hpp"
    #endif
#endif
