#include "../../../Interface/Graphics/GGraphicsDefines.h"
#include "../../../Interface/Math/GMathDefines.h"
#include "../../../Interface/Core/GEventResponder.h"
#include "../../../Interface/Core/GThreadShared.h"
#include "../../../Interface/System/GConcurrent.h"
#include "../../../Interface/System/GWindow.h"
#include "../../../Interface//Graphics//GDirectX11Surface.h"

#include <d3dcompiler.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <assert.h>

#include <Windows.h>
#include <d3d11_3.h>
#include <wrl/client.h>


//#define GRASTERSURFACE_DEBUG_WIN32_VERBOSE_NEW					// uncomment this line to override the "new" keyword with a macro that stores additional data to track down memory leaks more easily (WIN32 Debug only)
//#define GRASTERSURFACE_DEBUG_WIN32_SERIALIZE_SMARTUPDATE_COPY		// uncomment this line to disable multithreading in SmartUpdateSurface for debugging
//#define GRASTERSURFACE_DEBUG_WIN32_ASSERTS						// uncomment this line to enable debug asserts
//#define GRASTERSURFACE_DEBUG_WIN32_PRINTS							// uncomment this line to enable debug messages to be printed to the console
//#define GRASTERSURFACE_DEBUG_WIN32_PRINT_PRIORITY_LEVEL 0			// use this to set the priority level of debug messages to print (lower number = higher priority, any priority levels greater than this will be ignored)

#if defined(_WIN32) && !defined(NDEBUG) && defined(GRASTERSURFACE_DEBUG_WIN32_VERBOSE_NEW)
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif


namespace GW
{
	namespace I
	{
		class GRasterSurfaceImplementation : public virtual GRasterSurfaceInterface,
			protected GEventResponderImplementation
		{
		private:

#pragma region DEFINES

			typedef unsigned int		Color; // XRGB quadruple
			typedef unsigned char		ColorChannel; // single value from a Color (X, R, G, or B)

#pragma endregion DEFINES
#pragma region ENUMS_STRUCTS_AND_CLASSES

			// GRasterUpdateFlags bit positions for left/right shifting
			enum UpdateFlagBitPosition
			{
				BIT_ALIGN_X_LEFT = 0,
				BIT_ALIGN_X_CENTER,
				BIT_ALIGN_X_RIGHT,
				BIT_ALIGN_Y_TOP,
				BIT_ALIGN_Y_CENTER,
				BIT_ALIGN_Y_BOTTOM,
				BIT_UPSCALE_2X,
				BIT_UPSCALE_3X,
				BIT_UPSCALE_4X,
				BIT_UPSCALE_8X,
				BIT_UPSCALE_16X,
				BIT_STRETCH_TO_FIT,
				BIT_INTERPOLATE_NEAREST,
				BIT_INTERPOLATE_BILINEAR,
			};

			// flags indicating the active state of the surface
			enum class SurfaceState : int
			{
				INVALID = -1, // GWindow has been destroyed and surface cannot work properly
				INACTIVE = 0, // surface is not active and will not draw
				ACTIVE = 1, // surface is active and can draw
			};

			// struct containing variables needed by SmartUpdateSurface
			struct SmartUpdateData
			{
				const Color*			inputColors = nullptr;			// pointer to input colors to use for update
				unsigned int			bitmapWidth = 0;				// local copy of bitmap width since it could be altered during execution
				unsigned int			bitmapHeight = 0;				// local copy of bitmap height, since it could be altered during execution
				unsigned int			rawDataWidth = 0;				// width of raw data passed to function
				unsigned int			rawDataHeight = 0;				// height of raw data passed to function
				unsigned int			upscaledDataWidth = 0;			// width of data after upscaling
				unsigned int			upscaledDataHeight = 0;			// height of data after upscaling
				int						offsetX = 0;					// horizontal offset of processed data
				int						offsetY = 0;					// vertical offset of processed data
				float					coordScaleRatioX = 1.0f;		// horizontal pixel coordinate scaling ratio
				float					coordScaleRatioY = 1.0f;		// vertical pixel coordinate scaling ratio
			};

#pragma endregion ENUMS_STRUCTS_AND_CLASSES
#pragma region VARIABLES

			const char* vertexShaderSource = R"(
				struct VERTEX
				{
				    float2 pos : POSITION;
				    float2 uv : TEXCOORD;
				};

				struct VERTEX_OUT
				{
				    float4 pos : SV_POSITION;
				    float2 uv : TEXCOORD;
				};

				VERTEX_OUT main(VERTEX inputVertex)
				{
					VERTEX_OUT output;
					output.pos = float4(inputVertex.pos, 0.0f, 1.0f);
					output.uv = inputVertex.uv;
					return output;
				}
				)";
			// Simple Pixel Shader
			const char* pixelShaderSource = R"(
				Texture2D color : register(t0);
				SamplerState filter : register(s0);

				struct PS_IN
				{
					float4 pos : SV_POSITION;
					float2 uv : TEXCOORD;
				};

				float4 main(PS_IN input) : SV_TARGET 
				{	
					return color.Sample(filter, input.uv); 
				}
				)";

			// Gateware types are named as:		gVariableName
			// Win32 types are named as:		wVariableName

			GW::SYSTEM::GWindow						m_gWindow;											// parent GWindow
			GW::CORE::GEventResponder				m_gEventListener;									// listener used to subscribe to GWindow events
			GW::SYSTEM::UNIVERSAL_WINDOW_HANDLE		m_gUniversalWindowHandle = { nullptr, nullptr };	// universal window handle extracted from GWindow
			GW::SYSTEM::GConcurrent					m_gDrawThread;										// GConcurrent used for swapping buffers and drawing
			GW::SYSTEM::GConcurrent					m_gSmartUpdateRowProcessThread;						// GConcurrent used for copying data in SmartUpdateSurface
			GW::CORE::GThreadShared					m_thread;

			HDC										m_wWindowDeviceContext = nullptr;					// window device context extracted from universal window handle

			SurfaceState							m_surfaceState = SurfaceState::INACTIVE;
			unsigned long long						m_frameCount = 0;
			Color*									m_bitmapData;										// buffer color data
			unsigned short							m_bitmapWidth = 0;									// width of surface data, as well as front and back buffers
			unsigned short							m_bitmapHeight = 0;									// height of surface data, as well as front and back buffers
			std::atomic_bool						lockChecker;

			GW::GRAPHICS::GDirectX11Surface					m_d3d11Surface;
			Microsoft::WRL::ComPtr<ID3D11Buffer>			m_d3d11VertBuff;
			Microsoft::WRL::ComPtr<ID3D11VertexShader>		m_d3d11VertShad;
			Microsoft::WRL::ComPtr<ID3D11PixelShader>		m_d3d11PixelShad;
			Microsoft::WRL::ComPtr<ID3D11InputLayout>		m_d3d11VertexLayout;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>m_d3d11ShaderResourceView;
			Microsoft::WRL::ComPtr<ID3D11SamplerState>		m_d3d11SamplerState;
			D3D11_MAPPED_SUBRESOURCE						m_d3d11MapSubRes;
			Microsoft::WRL::ComPtr<ID3D11Texture2D>			m_d3d11Tex2D;

#pragma endregion VARIABLES
#pragma region PRIVATE_FUNCTIONS

			// Prints a debug message to the console if GRASTERSURFACE_DEBUG_WIN32_PRINTS is defined.
			/*
			*	Used to assist with debugging, since multi-threading makes debugging tools harder to use.
			*	If int arguments are passed, they will be inserted into the output if _msg is a format string.
			*/
			static inline void DebugPrint(int _priorityLevel, const char* _msg,
				long long _intArg0 = 0, long long _intArg1 = 0, long long _intArg2 = 0, long long _intArg3 = 0,
				long long _intArg4 = 0, long long _intArg5 = 0, long long _intArg6 = 0, long long _intArg7 = 0)
			{
#if defined(GRASTERSURFACE_DEBUG_WIN32_PRINTS) && defined(GRASTERSURFACE_DEBUG_WIN32_PRINT_PRIORITY_LEVEL)
				if (_priorityLevel <= GRASTERSURFACE_DEBUG_WIN32_PRINT_PRIORITY_LEVEL)
					std::printf(_msg, _intArg0, _intArg1, _intArg2, _intArg3, _intArg4, _intArg5, _intArg6, _intArg7);
#endif
			}

			// Calls assert on the passed-in statement if GRASTERSURFACE_DEBUG_WIN32_ASSERTS is defined.
			static inline void DebugAssert(bool _statement)
			{
#if defined(GRASTERSURFACE_DEBUG_LINUX_ASSERTS)
				assert(_statement);
#endif
			}

			// Destroys and recreates bitmap data with new dimensions.
			/*
			*	Makes the surface active.
			*
			*	retval GReturn::SUCCESS					Completed successfully.
			*	retval GReturn::PREMATURE_DEALLOCATION	The associated GWindow was deallocated and the surface is now invalid.
			*	retval GReturn::UNEXPECTED_RESULT		The function entered an area it was not supposed to be able to.
			*/
			GReturn ResizeBitmap(unsigned int _width, unsigned int _height)
			{
				DebugPrint(1, "\nResizeBitmap\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "ResizeBitmap - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				m_bitmapWidth = _width;
				m_bitmapHeight = _height;

				if (+m_thread.LockSyncWrite()) {

					if (m_bitmapData)
					{
						delete[] m_bitmapData;
						m_bitmapData = nullptr;
					}

					m_bitmapData = new Color[m_bitmapWidth * m_bitmapHeight];

					m_thread.UnlockSyncWrite();
				}

				D3D11_TEXTURE2D_DESC desc = { 0 };
				
				desc.Width = m_bitmapWidth;
				desc.Height = m_bitmapHeight;
				desc.MipLevels = desc.ArraySize = 1;
				desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				desc.SampleDesc.Count = 1;
				desc.Usage = D3D11_USAGE_DYNAMIC;
				desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
				desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
				desc.MiscFlags = 0;

				ID3D11Device* device;
				m_d3d11Surface.GetDevice((void**)&device);

				Color* tempText = new Color[m_bitmapWidth * m_bitmapHeight];
				memset(tempText, 0xffc99aff, m_bitmapWidth * m_bitmapHeight);

				D3D11_SUBRESOURCE_DATA initData = { &tempText, sizeof(uint32_t), 0 };
				HRESULT hr = device->CreateTexture2D(&desc, &initData, m_d3d11Tex2D.ReleaseAndGetAddressOf());

				D3D11_SHADER_RESOURCE_VIEW_DESC SRVdesc = {};
				SRVdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				SRVdesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				SRVdesc.Texture2D.MipLevels = 1;

				hr = device->CreateShaderResourceView(m_d3d11Tex2D.Get(), &SRVdesc, m_d3d11ShaderResourceView.ReleaseAndGetAddressOf());

				device->Release();

				return GReturn::SUCCESS;
			}

			// Returns the state of a single bit in a bitfield
			static inline bool IsolateBit(unsigned int _flags, unsigned short _bit)
			{
				return (_flags >> _bit) & 1;
			}

			// Interpolates linearly between two pixel color values.
			static inline Color LerpColor(Color _a, Color _b, float _r)
			{
				// split pixels into channels
				ColorChannel a_channels[4] =
				{
					static_cast<ColorChannel>((_a & 0x000000FF)),
					static_cast<ColorChannel>(((_a & 0x0000FF00) >> 8)),
					static_cast<ColorChannel>(((_a & 0x00FF0000) >> 16)),
					static_cast<ColorChannel>(((_a & 0xFF000000) >> 24)),
				};
				ColorChannel b_channels[4] =
				{
					static_cast<ColorChannel>((_b & 0x000000FF)),
					static_cast<ColorChannel>(((_b & 0x0000FF00) >> 8)),
					static_cast<ColorChannel>(((_b & 0x00FF0000) >> 16)),
					static_cast<ColorChannel>(((_b & 0xFF000000) >> 24)),
				};
				// interpolate results
				ColorChannel result[4] =
				{
					static_cast<ColorChannel>((G_LERP_PRECISE(a_channels[0], b_channels[0], _r))),
					static_cast<ColorChannel>((G_LERP_PRECISE(a_channels[1], b_channels[1], _r))),
					static_cast<ColorChannel>((G_LERP_PRECISE(a_channels[2], b_channels[2], _r))),
					static_cast<ColorChannel>((G_LERP_PRECISE(a_channels[3], b_channels[3], _r))),
				};
				return *(reinterpret_cast<Color*>(result));
			}

			// Returns color at truncated integer coordinate; Faster.
			/*
				_xrgbColors				Array of colors to interpolate within
				_width					Width of color array
				_height					Height of color array
				_u						Horizontal "texel" coordinate
				_v						Vertical "texel" coordinate
			*/
			static inline Color InterpolateColorNearest(const Color* _xrgbColors,
				unsigned int _width, unsigned int _height, float _u, float _v)
			{
				return _xrgbColors[static_cast<unsigned int>(_u) + static_cast<unsigned int>(_v) * _width];
			}

			// Returns color blended between nearest four pixels; Slower.
			/*
				_xrgbColors				Array of colors to interpolate within
				_width					Width of color array
				_height					Height of color array
				_u						Horizontal "texel" coordinate
				_v						Vertical "texel" coordinate
			*/
			static inline Color InterpolateColorBilinear(const Color* _xrgbColors,
				unsigned int _width, unsigned int _height, float _u, float _v)
			{
				// offset coordinates to use pixel corners instead of centers
				_u -= 0.5f;
				_u = (_u < 0.0f) ? 0.0f : _u;
				_v -= 0.5f;
				_v = (_v < 0.0f) ? 0.0f : _v;
				// get top-left coordinates
				unsigned int u0 = static_cast<unsigned int>(_u);
				unsigned int v0 = static_cast<unsigned int>(_v);
				// get bottom-right coordinates
				unsigned int u1 = (u0 < _width - 1) ? u0 + 1 : u0;
				unsigned int v1 = (v0 < _height - 1) ? v0 + 1 : v0;
				// calculate interpolation ratios
				float rx = _u - u0;
				float ry = _v - v0;
				// interpolate results
				return LerpColor(
					LerpColor(_xrgbColors[u0 + (v0 * _width)], _xrgbColors[u1 + (v0 * _width)], rx),
					LerpColor(_xrgbColors[u0 + (v1 * _width)], _xrgbColors[u1 + (v1 * _width)], rx),
					ry);
			}

			// Iterates over a row of pixels and samples colors with nearest interpolation
			static void ProcessPixelRowNearest(const void* _unused, Color* _output, unsigned int _y, const void* _data)
			{
				DebugPrint(3, "\nProcessPixelRowNearest, row %d\n", _y);
				DebugAssert(_output != nullptr);
				// convert user data to correct type
				DebugAssert(_data != nullptr);
				const SmartUpdateData data = *(reinterpret_cast<const SmartUpdateData*>(_data));
				DebugAssert(data.inputColors != nullptr);
				DebugPrint(4, "ProcessPixelRowNearest, row %d - bitmap width = %d\n", _y, data.bitmapWidth);
				DebugPrint(4, "ProcessPixelRowNearest, row %d - bitmap height = %d\n", _y, data.bitmapHeight);
				DebugAssert(data.bitmapWidth > 0);
				DebugAssert(data.bitmapHeight > 0);
				DebugAssert(_y >= 0);
				DebugAssert(_y < data.bitmapHeight);
				// get y position within rectangular area to draw (should only draw between 0 and upscaled data height)
				int relativeY = _y - data.offsetY;
				// skip this row if not in range
				if (relativeY < 0 || relativeY >= static_cast<int>(data.upscaledDataHeight))
				{
					DebugPrint(5, "ProcessPixelRowNearest, row %d - row out of range\n", _y);
					return;
				}
				// init nearest algorithm vars
				float v = relativeY * data.coordScaleRatioY; // vertical "texel" to sample from in input data
				DebugPrint(4, "ProcessPixelRowNearest, row %d - v = %d\n", _y, static_cast<unsigned int>(v));
				DebugAssert(v >= 0);
				DebugAssert(v < data.bitmapHeight);
				// get x position within rectangular area to draw (should only draw between 0 and upscaled data width)
				int relativeX = G_CLAMP_MIN(-data.offsetX, 0);
				// iterate through pixel row and process pixels
				for (int x = G_CLAMP_MIN(data.offsetX, 0); x < G_CLAMP_MAX(data.offsetX + static_cast<int>(data.upscaledDataWidth), static_cast<int>(data.bitmapWidth)); ++x, ++relativeX)
				{
					float u = relativeX * data.coordScaleRatioX; // horizontal "texel" to sample from in input data
					DebugAssert(u >= 0);
					DebugAssert(u < data.bitmapWidth);
					_output[x] = InterpolateColorNearest(data.inputColors, data.rawDataWidth, data.rawDataHeight, u, v);
				}
				DebugPrint(3, "\n");
			}

			// Iterates over a row of pixels and samples colors with bilinear interpolation
			static void ProcessPixelRowBilinear(const void* _unused, Color* _output, unsigned int _y, const void* _data)
			{
				DebugPrint(3, "\nProcessPixelRowBilinear, row %d\n", _y);
				DebugAssert(_output != nullptr);
				// convert user data to correct type
				DebugAssert(_data != nullptr);
				SmartUpdateData data = *(const_cast<SmartUpdateData*>(reinterpret_cast<const SmartUpdateData*>(_data)));
				DebugAssert(data.inputColors != nullptr);
				DebugPrint(4, "ProcessPixelRowBilinear, row %d - bitmap width = %d\n", _y, data.bitmapWidth);
				DebugPrint(4, "ProcessPixelRowBilinear, row %d - bitmap height = %d\n", _y, data.bitmapHeight);
				DebugAssert(data.bitmapWidth > 0);
				DebugAssert(data.bitmapHeight > 0);
				DebugAssert(_y >= 0);
				DebugAssert(_y < data.bitmapHeight);
				// get y position within rectangular area to draw (should only draw between 0 and upscaled data height)
				int relativeY = _y - data.offsetY;
				// skip this row if not in range
				if (relativeY < 0 || relativeY >= static_cast<int>(data.upscaledDataHeight))
				{
					DebugPrint(5, "ProcessPixelRowBilinear, row %d - row out of range\n", _y);
					return;
				}
				// get x position within rectangular area to draw (should only draw between 0 and upscaled data width)
				int relativeX = G_CLAMP_MIN(-data.offsetX, 0);
				// init bilinear algorithm vars
				float v = relativeY * data.coordScaleRatioY; // vertical "texel" to sample from in input data
				DebugPrint(4, "ProcessPixelRowNearest, row %d - v = %d\n", _y, static_cast<unsigned int>(v));
				DebugAssert(v >= 0);
				DebugAssert(v < data.bitmapHeight);
				// iterate through row and process pixels
				for (int x = G_CLAMP_MIN(data.offsetX, 0); x < G_CLAMP_MAX(data.offsetX + static_cast<int>(data.upscaledDataWidth), static_cast<int>(data.bitmapWidth)); ++x, ++relativeX)
				{
					float u = relativeX * data.coordScaleRatioX; // horizontal "texel" to sample from in input data
					DebugAssert(u >= 0);
					DebugAssert(u < data.bitmapWidth);
					_output[x] = InterpolateColorBilinear(data.inputColors, data.rawDataWidth, data.rawDataHeight, u, v);
				}
				DebugPrint(3, "\n");
			}

			void Cleanup()
			{
				DebugPrint(1, "\nCleanup\n");
				DebugPrint(2, "Cleanup - ensure all locks are released\n");	
			}

#pragma endregion PRIVATE_FUNCTIONS

		public:

#pragma region CREATE_AND_DESTROY_FUNCTIONS

			~GRasterSurfaceImplementation()
			{
				DebugPrint(0, "\nGRasterSurface destructor\n");
				Cleanup();
			}

			GReturn Create(GW::SYSTEM::GWindow _gWindow)
			{
				DebugPrint(0, "\nCreate\n");

				// validate arguments
				DebugPrint(2, "Create - validate args\n");
				// ensure gwindow exists
				if (_gWindow == nullptr)
				{
					DebugPrint(2, "Create - m_gWindow was nullptr; return\n\n");
					return GW::GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "Create - args valid\n");

				// store arguments
				m_gWindow = _gWindow;

				// create GConcurrent objects (false = no callbacks)
				DebugPrint(2, "Create - create GConcurrent objects\n");
				m_gDrawThread.Create(false);
				m_gSmartUpdateRowProcessThread.Create(false);
				// create event listener with callback function for GWindow to call
				GReturn gr = m_gEventListener.Create([&](GW::GEvent _event)
					{
						GW::SYSTEM::GWindow::Events windowEvent;
						GW::SYSTEM::GWindow::EVENT_DATA windowEventData;
						if (+_event.Read(windowEvent, windowEventData))
							switch (windowEvent)
							{
							case GW::SYSTEM::GWindow::Events::MINIMIZE:
							{
								DebugPrint(2, "Create callback lambda - MINIMIZE event received\n");
								// deactivate raster when minimized to prevent updating while window is not visible
								m_surfaceState = SurfaceState::INACTIVE;
							} break;
							case GW::SYSTEM::GWindow::Events::MAXIMIZE:
							{
								DebugPrint(2, "Create callback lambda - MAXIMIZE event received\n");
								// get client dimensions from window and resize bitmap to fit
								DebugPrint(2, "Create callback lambda - MAXIMIZE - resize bitmap\n");
								ResizeBitmap(windowEventData.clientWidth, windowEventData.clientHeight); // makes surface active
							} break;
							case GW::SYSTEM::GWindow::Events::RESIZE:
							{
								DebugPrint(2, "Create callback lambda - RESIZE event received\n");
								// get client dimensions from window and resize bitmap to fit
								DebugPrint(2, "Create callback lambda - RESIZE - resize bitmap\n");
								if (m_d3d11Surface)
									ResizeBitmap(windowEventData.width, windowEventData.height); // makes surface active
							} break;
							case GW::SYSTEM::GWindow::Events::DESTROY:
							{
								DebugPrint(2, "Create callback lambda - DESTROY event received\n");
								m_surfaceState = SurfaceState::INVALID;

								Cleanup();
							} break;
							default:
							{
								DebugPrint(2, "Create callback lambda - unknown event received\n");
							} break;
							} // end switch (windowEvent)
					});
				// register event listener with GWindow's event generator
				DebugPrint(2, "Create - register event listener\n");
				if (-m_gWindow.Register(m_gEventListener))
				{
					DebugPrint(2, "Create - registering event listener failed; return\n\n");
					Cleanup();
					return GReturn::INVALID_ARGUMENT;
				}

				ID3D11Device* device;

				if (G_FAIL(gr = m_d3d11Surface.Create(m_gWindow, 0)))
					return gr;
				m_d3d11Surface.GetDevice((void**)&device);
				//m_d3d11Surface.GetImmediateContext((void**)&m_d3d11Context);
				//m_d3d11Surface.GetSwapchain((void**)&m_d3d11Swapchain);
				//m_d3d11Surface.GetRenderTargetView((void**)&m_d3d11rtv);

				float verts[] = {
					-1,  1, 0, 0,
					 1,  1, 1, 0,
					-1, -1, 0, 1,
					 1, -1, 1, 1,
				};
				D3D11_SUBRESOURCE_DATA bData = { verts, 0, 0 };
				CD3D11_BUFFER_DESC bDesc(sizeof(verts), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE);
				//bDesc.CPUAccessFlags = UINT(D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ);
				device->CreateBuffer(&bDesc, &bData, m_d3d11VertBuff.GetAddressOf());

				// Create Vertex Shader
				UINT compilerFlags = D3DCOMPILE_ENABLE_STRICTNESS;
				Microsoft::WRL::ComPtr<ID3DBlob> vsBlob, errors;
				if (SUCCEEDED(D3DCompile(vertexShaderSource, strlen(vertexShaderSource),
					nullptr, nullptr, nullptr, "main", "vs_4_0", compilerFlags, 0,
					vsBlob.GetAddressOf(), errors.GetAddressOf())))
				{
					device->CreateVertexShader(vsBlob->GetBufferPointer(),
						vsBlob->GetBufferSize(), nullptr, m_d3d11VertShad.GetAddressOf());
				}
				else
					std::cout << (char*)errors->GetBufferPointer() << std::endl;
				// Create Pixel Shader
				Microsoft::WRL::ComPtr<ID3DBlob> psBlob;
				errors.Reset();
				if (SUCCEEDED(D3DCompile(pixelShaderSource, strlen(pixelShaderSource),
					nullptr, nullptr, nullptr, "main", "ps_4_0", compilerFlags, 0,
					psBlob.GetAddressOf(), errors.GetAddressOf())))
				{
					device->CreatePixelShader(psBlob->GetBufferPointer(),
						psBlob->GetBufferSize(), nullptr, m_d3d11PixelShad.GetAddressOf());
				}
				else
					std::cout << (char*)errors->GetBufferPointer() << std::endl;
				// Create Input Layout
				D3D11_INPUT_ELEMENT_DESC format[] = {
					{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0,	D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
					{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0,	D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
				};
				device->CreateInputLayout(format, ARRAYSIZE(format),
					vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
					m_d3d11VertexLayout.GetAddressOf());

				CD3D11_SAMPLER_DESC samp_desc = CD3D11_SAMPLER_DESC(CD3D11_DEFAULT());
				device->CreateSamplerState(&samp_desc, m_d3d11SamplerState.GetAddressOf());

				// free temporary handle
				device->Release();

				// Create the thread for locking and unlocking
				m_thread.Create();

				unsigned int newWidth, newHeight;
				if (-m_gWindow.GetClientWidth(newWidth) || -m_gWindow.GetClientHeight(newHeight))
				{
					Cleanup();
					return GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "Create - resize bitmap to client dimensions of window\n");
				ResizeBitmap(newWidth, newHeight);

				
				DebugPrint(2, "Create - successful; return\n\n");
				return gr;
			}

#pragma endregion CREATE_AND_DESTROY_FUNCTIONS
#pragma region UPDATE_FUNCTIONS

			GReturn Clear(Color _xrgbColor) override
			{
				DebugPrint(0, "\nClear\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "Clear - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				DebugPrint(2, "Clear - attempt to lock back buffer for write\n");
				if (+m_thread.LockSyncWrite())
				{
					DebugPrint(2, "Clear - locked back buffer for write\n");
					// store bitmap dimensions locally, since they could change
					unsigned int bitmapWidth = m_bitmapWidth;
					unsigned int bitmapHeight = m_bitmapHeight;
					// fill back buffer with passed color
					DebugPrint(2, "Clear - fill back buffer with color\n");

					memset(m_bitmapData, _xrgbColor, static_cast<rsize_t>((bitmapWidth * bitmapHeight) * sizeof(Color)));
					// unlock and return
					DebugPrint(2, "Clear - attempt to unlock back buffer for write\n");
					if (+m_thread.UnlockSyncWrite())
					{
						DebugPrint(2, "Clear - unlocked back buffer for write; successful; return\n\n");
						return GReturn::SUCCESS;
					}
					DebugPrint(2, "Clear - unexpected result; return\n\n");
					return GReturn::UNEXPECTED_RESULT; // should never reach here
				}
				DebugPrint(2, "Clear - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

			GReturn UpdateSurface(const Color* _xrgbPixels, unsigned int _numPixels) override
			{
				DebugPrint(0, "\nUpdateSurface\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "UpdateSurface - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// validate arguments
				DebugPrint(2, "UpdateSurface - validate args\n");
				// ensure valid pixel array was passed
				if (_xrgbPixels == nullptr)
				{
					DebugPrint(2, "UpdateSurface - _xrgbPixels was nullptr; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure pixel count is nonzero
				if (_numPixels < 1)
				{
					DebugPrint(2, "UpdateSurface - _numPixels was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure pixel count does not exceed surface pixel count
				if (_numPixels > static_cast<unsigned int>(m_bitmapWidth * m_bitmapHeight))
				{
					DebugPrint(2, "UpdateSurface - _numPixels was > surface's pixel count; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "UpdateSurface - args valid\n");

				// lock self and back buffer to prevent read/write conflicts
				DebugPrint(2, "UpdateSurface - attempt to lock self, back buffer for write\n");
				if (+m_thread.LockSyncWrite())
				{
					DebugPrint(2, "UpdateSurface - locked self, back buffer for write\n");
					// BLIT pixel data to back buffer
					DebugPrint(2, "UpdateSurface - BLIT input pixels to back buffer\n");
					memcpy_s(m_bitmapData, static_cast<rsize_t>(_numPixels * sizeof(Color)),
						_xrgbPixels, static_cast<rsize_t>(_numPixels * sizeof(Color)));
					// unlock and return
					DebugPrint(2, "UpdateSurface - unlock and return\n");
					if (+m_thread.UnlockSyncWrite())
					{
						DebugPrint(2, "UpdateSurface - unlocked self, back buffer for write; successful; return\n\n");
						return GReturn::SUCCESS;
					}
				}
				DebugPrint(2, "UpdateSurface - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

			GReturn UpdateSurfaceSubset(const Color* _xrgbPixels, unsigned short _numRows, unsigned short _rowWidth, unsigned short _rowStride, int _destX, int _destY) override
			{
				DebugPrint(0, "\nUpdateSurfaceSubset\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "UpdateSurfaceSubset - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// store dimensions locally, since they could potentially change during execution
				DebugPrint(2, "UpdateSurfaceSubset - store bitmap dimensions\n");
				unsigned short bitmapWidth = m_bitmapWidth;
				unsigned short bitmapHeight = m_bitmapHeight;
				// validate arguments
				DebugPrint(2, "UpdateSurfaceSubset - validate args\n");
				// ensure valid pixel array was passed
				if (_xrgbPixels == nullptr)
				{
					DebugPrint(2, "UpdateSurfaceSubset - _xrgbPixels was nullpt; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure row count is nonzero
				if (_numRows < 1)
				{
					DebugPrint(2, "UpdateSurfaceSubset - _numRows was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure row width is nonzero
				if (_rowWidth < 1)
				{
					DebugPrint(2, "UpdateSurfaceSubset - _numRows was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure row stride is >= row width, if provided
				if (_rowStride > 0 && _rowStride < _rowWidth)
				{
					DebugPrint(2, "UpdateSurfaceSubset - _rowStride was < _rowWidth; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "UpdateSurfaceSubset - args valid\n");

				// return immediately if subset is completely outside surface
				if (_destX >= static_cast<int>(bitmapWidth)
					|| _destX + _rowWidth < 0
					|| _destY >= static_cast<int>(bitmapHeight)
					|| _destY + _numRows < 0)
				{
					DebugPrint(2, "UpdateSurfaceSubset - subset was outside surface; return\n\n");
					return GReturn::REDUNDANT;
				}

				// get row stride
				DebugPrint(2, "UpdateSurfaceSubset - get row stride\n");
				if (_rowStride == 0) _rowStride = _rowWidth;
				// clip subset to edges of surface
				//   left side
				DebugPrint(2, "UpdateSurfaceSubset - clip left side\n");
				if (_destX < 0)
				{
					// move starting location in source data right and shorten rows
					_xrgbPixels += -_destX;
					_rowWidth += _destX; // add, because coord is negative
					// clamp coord to surface edge
					_destX = 0;
				}
				//   right side
				DebugPrint(2, "UpdateSurfaceSubset - clip right side\n");
				if (_destX + _rowWidth > bitmapWidth)
				{
					// shorten rows
					_rowWidth -= (_destX + _rowWidth - bitmapWidth);
				}
				//   top side
				DebugPrint(2, "UpdateSurfaceSubset - clip top side\n");
				if (_destY < 0)
				{
					// move starting location in source data down and reduce row count
					_xrgbPixels += -_destY * _rowStride;
					_numRows += _destY; // add, because coord is negative
					// clamp coord to surface edge
					_destY = 0;
				}
				//   bottom side
				DebugPrint(2, "UpdateSurfaceSubset - clip bottom side\n");
				if (_destY + _numRows > bitmapHeight)
				{
					// reduce row count
					_numRows -= (_destY + _numRows - bitmapHeight);
				}
				// calculate starting index from destination x/y coords
				DebugPrint(2, "UpdateSurfaceSubset - calculate starting index\n");
				unsigned int startIndex = _destX + (_destY * bitmapWidth);

				// lock self and back buffer to prevent read/write conflicts
				DebugPrint(2, "UpdateSurfaceSubset - attempt to lock self, back buffer for write\n");
				if (+m_thread.LockSyncWrite())
				{
					DebugPrint(2, "UpdateSurfaceSubset - locked self, back buffer for write\n");
					// calculate end index
					DebugPrint(2, "UpdateSurfaceSubset - calculate end index\n");
					unsigned int endIndex = startIndex + (bitmapWidth * (_numRows - 1)) + (_rowWidth - 1);
					// BLIT rows of subset block to back buffer
					DebugPrint(2, "UpdateSurfaceSubset - BLIT data to back buffer\n");
					for (; startIndex < endIndex; startIndex += bitmapWidth, _xrgbPixels += _rowStride)
						memcpy_s(&m_bitmapData[startIndex], static_cast<rsize_t>(_rowWidth * sizeof(Color)),
							_xrgbPixels, static_cast<rsize_t>(_rowWidth * sizeof(Color)));
					// unlock and return
					DebugPrint(2, "UpdateSurfaceSubset - unlock and return\n");
					if (+m_thread.UnlockSyncWrite())
					{
						DebugPrint(2, "UpdateSurfaceSubset - unlocked self, back buffer for write; successful; return\n\n");
						return GReturn::SUCCESS;
					}
				}
				DebugPrint(2, "UpdateSurfaceSubset - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

			GReturn SmartUpdateSurface(const Color* _xrgbPixels, unsigned int _numPixels, unsigned short _rowWidth, unsigned int _drawOptionFlags) override
			{
				DebugPrint(0, "\nSmartUpdateSurface\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "SmartUpdateSurface - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// validate arguments
				DebugPrint(2, "SmartUpdateSurface - validate args\n");
				// ensure valid pixel array was passed
				if (_xrgbPixels == nullptr)
				{
					DebugPrint(2, "SmartUpdateSurface - _xrgbPixels was nullptr; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure pixel count is nonzero
				if (_numPixels < 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _numPixels was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// ensure row width is nonzero
				if (_rowWidth < 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _rowWidth was < 1; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// separate flags into sections and test each section
				const unsigned int bitmaskAlignX =
					GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_LEFT
					| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_CENTER
					| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_X_RIGHT;
				const unsigned int bitmaskAlignY =
					GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_TOP
					| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_CENTER
					| GW::GRAPHICS::GRasterUpdateFlags::ALIGN_Y_BOTTOM;
				const unsigned int bitmaskUpscale =
					GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_2X
					| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_3X
					| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_4X
					| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_8X
					| GW::GRAPHICS::GRasterUpdateFlags::UPSCALE_16X
					| GW::GRAPHICS::GRasterUpdateFlags::STRETCH_TO_FIT;
				const unsigned int bitmaskInterpolate =
					GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_NEAREST
					| GW::GRAPHICS::GRasterUpdateFlags::INTERPOLATE_BILINEAR;
				unsigned int testFlags = 0;
				// validate x alignment flags
				testFlags = _drawOptionFlags & bitmaskAlignX;
				if ((IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_X_LEFT)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_X_CENTER)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_X_RIGHT)
					) > 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _drawOptionFlags contains conflicting x alignment flags; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// validate y alignment flags
				testFlags = _drawOptionFlags & bitmaskAlignY;
				if ((IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_Y_TOP)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_Y_CENTER)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_ALIGN_Y_BOTTOM)
					) > 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _drawOptionFlags contains conflicting y alignment flags; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// validate upscaling flags
				testFlags = _drawOptionFlags & bitmaskUpscale;
				if ((IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_2X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_3X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_4X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_8X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_UPSCALE_16X)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_STRETCH_TO_FIT)
					) > 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _drawOptionFlags contains conflicting upscaling flags; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// validate interpolation flags
				testFlags = _drawOptionFlags & bitmaskInterpolate;
				if ((IsolateBit(testFlags, UpdateFlagBitPosition::BIT_INTERPOLATE_NEAREST)
					+ IsolateBit(testFlags, UpdateFlagBitPosition::BIT_INTERPOLATE_BILINEAR)
					) > 1)
				{
					DebugPrint(2, "SmartUpdateSurface - _drawOptionFlags contains conflicting interpolation flags; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				DebugPrint(2, "SmartUpdateSurface - args valid\n");
				// if arguments are valid, continue

				// store surface dimensions and border color locally to avoid needing to read data more than once
				unsigned int bitmapWidth = m_bitmapWidth;
				unsigned int bitmapHeight = m_bitmapHeight;

				// create struct to store data to use during processing
				SmartUpdateData data = {};
				// store color array and metadata
				data.inputColors = _xrgbPixels;
				data.bitmapWidth = bitmapWidth;
				data.bitmapHeight = bitmapHeight;
				data.rawDataWidth = _rowWidth;
				data.rawDataHeight = _numPixels / _rowWidth;
				// determine data dimensions after processing
				DebugPrint(2, "SmartUpdateSurface - calculate upscaled dimensions\n");
				testFlags = _drawOptionFlags & bitmaskUpscale;
				switch (testFlags)
				{
				case GW::GRAPHICS::UPSCALE_2X:
					data.upscaledDataWidth = data.rawDataWidth << 1;
					data.upscaledDataHeight = data.rawDataHeight << 1;
					break;
				case GW::GRAPHICS::UPSCALE_3X:
					data.upscaledDataWidth = data.rawDataWidth * 3;
					data.upscaledDataHeight = data.rawDataHeight * 3;
					break;
				case GW::GRAPHICS::UPSCALE_4X:
					data.upscaledDataWidth = data.rawDataWidth << 2;
					data.upscaledDataHeight = data.rawDataHeight << 2;
					break;
				case GW::GRAPHICS::UPSCALE_8X:
					data.upscaledDataWidth = data.rawDataWidth << 3;
					data.upscaledDataHeight = data.rawDataHeight << 3;
					break;
				case GW::GRAPHICS::UPSCALE_16X:
					data.upscaledDataWidth = data.rawDataWidth << 4;
					data.upscaledDataHeight = data.rawDataHeight << 4;
					break;
				case GW::GRAPHICS::STRETCH_TO_FIT:
					data.upscaledDataWidth = bitmapWidth;
					data.upscaledDataHeight = bitmapHeight;
					break;
				default:
					data.upscaledDataWidth = data.rawDataWidth;
					data.upscaledDataHeight = data.rawDataHeight;
					break;
				}

				// calculate pixel coordinate scaling ratios
				DebugPrint(2, "SmartUpdateSurface - calculate coordinate scaling ratios\n");
				data.coordScaleRatioX = data.rawDataWidth / static_cast<float>(data.upscaledDataWidth);
				data.coordScaleRatioY = data.rawDataHeight / static_cast<float>(data.upscaledDataHeight);

				// determine X alignment
				DebugPrint(2, "SmartUpdateSurface - determine x alignment\n");
				testFlags = _drawOptionFlags & bitmaskAlignX;
				switch (testFlags)
				{
				case GW::GRAPHICS::ALIGN_X_LEFT:
					data.offsetX = 0;
					break;
				case GW::GRAPHICS::ALIGN_X_RIGHT:
					data.offsetX = static_cast<int>(bitmapWidth) - static_cast<int>(data.upscaledDataWidth);
					break;
				case GW::GRAPHICS::ALIGN_X_CENTER:
				default:
					data.offsetX = (static_cast<int>(bitmapWidth) - static_cast<int>(data.upscaledDataWidth)) >> 1;
					break;
				}

				// determine Y alignment
				DebugPrint(2, "SmartUpdateSurface - determine y alignment\n");
				testFlags = _drawOptionFlags & bitmaskAlignY;
				switch (testFlags)
				{
				case GW::GRAPHICS::ALIGN_Y_TOP:
					data.offsetY = 0;
					break;
				case GW::GRAPHICS::ALIGN_Y_BOTTOM:
					data.offsetY = static_cast<int>(bitmapHeight) - static_cast<int>(data.upscaledDataHeight);
					break;
				case GW::GRAPHICS::ALIGN_Y_CENTER:
				default:
					data.offsetY = (static_cast<int>(bitmapHeight) - static_cast<int>(data.upscaledDataHeight)) >> 1;
					break;
				}

				// lock self and back buffer to prevent read/write conflicts
				DebugPrint(2, "SmartUpdateSurface - attempt to lock self, back buffer for write\n");
				if (+m_thread.LockSyncWrite())
				{
					DebugPrint(2, "SmartUpdateSurface - locked self, back buffer for write\n");
#if !defined(GRASTERSURFACE_DEBUG_WIN32_SERIALIZE_SMARTUPDATE_COPY)
					// calculate how many rows of the surface fit into 256 KB
					DebugPrint(2, "SmartUpdateSurface - calculate rows per thread\n");
					unsigned int rows = 262144 / (bitmapWidth * sizeof(Color));
					if (rows == 0) rows = 1; // min of 1
#endif

					// iterate through surface and process pixels
					if ((_drawOptionFlags & bitmaskInterpolate) == GW::GRAPHICS::INTERPOLATE_BILINEAR)
					{
#if defined(GRASTERSURFACE_DEBUG_WIN32_SERIALIZE_SMARTUPDATE_COPY)
						DebugPrint(2, "SmartUpdateSurface - process rows w/ bilinear interp (SERIAL)\n");
						for (unsigned int y = 0; y < bitmapHeight; ++y)
							ProcessPixelRowBilinear(static_cast<const void*>(nullptr), &m_bitmapData[y * bitmapWidth], y, reinterpret_cast<const void*>(&data));
#else
						DebugPrint(2, "SmartUpdateSurface - process rows w/ bilinear interp (PARALLEL)\n");
						m_gSmartUpdateRowProcessThread.BranchParallel(ProcessPixelRowBilinear,
							rows, bitmapHeight, reinterpret_cast<const void*>(&data),
							0, static_cast<const void*>(nullptr),
							static_cast<int>(bitmapWidth * sizeof(Color)), m_bitmapData);
#endif
					}
					else
					{
#if defined(GRASTERSURFACE_DEBUG_WIN32_SERIALIZE_SMARTUPDATE_COPY)
						DebugPrint(2, "SmartUpdateSurface - process rows w/ nearest interp (SERIAL)\n");
						for (unsigned int y = 0; y < bitmapHeight; ++y)
							ProcessPixelRowNearest(static_cast<const void*>(nullptr), &m_bitmapData[y * bitmapWidth], y, reinterpret_cast<const void*>(&data));
#else
						DebugPrint(2, "SmartUpdateSurface - process rows w/ nearest interp (PARALLEL)\n");
						m_gSmartUpdateRowProcessThread.BranchParallel(ProcessPixelRowNearest,
							rows, bitmapHeight, reinterpret_cast<const void*>(&data),
							0, static_cast<const void*>(nullptr),
							static_cast<int>(bitmapWidth * sizeof(Color)), m_bitmapData);
#endif
					}

#if !defined(GRASTERSURFACE_DEBUG_WIN32_SERIALIZE_SMARTUPDATE_COPY)
					// wait for data to finish processing
					DebugPrint(2, "SmartUpdateSurface - converge row processing threads\n");
					GReturn convergeResult = m_gSmartUpdateRowProcessThread.Converge(0);
					if (G_FAIL(convergeResult))
					{
						DebugPrint(2, "SmartUpdateSurface - converge failed, attempt to unlock back buffer for write\n");
						if (+m_thread.UnlockSyncWrite()) // Why isn't the GRasterSurface being unlocked here too? 
						{
							DebugPrint(2, "SmartUpdateSurface - unlocked back buffer for write; return\n\n");
							return convergeResult;
						}
					}
#endif

					// unlock and return
					DebugPrint(2, "SmartUpdateSurface - unlock and return\n");



					if (+m_thread.UnlockSyncWrite())
					{
						DebugPrint(2, "SmartUpdateSurface - unlocked self, back buffer for write; successful; return\n\n");
						return GReturn::SUCCESS;
					}
				}
				DebugPrint(2, "SmartUpdateSurface - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

#pragma endregion UPDATE_FUNCTIONS
#pragma region LOCK_AND_UNLOCK_FUNCTIONS

			GReturn LockUpdateBufferWrite(Color** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) override
			{
				DebugPrint(0, "\nLockUpdateBufferWrite\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "LockUpdateBufferWrite - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// validate arguments
				if (_outMemoryBuffer == nullptr)
				{
					DebugPrint(2, "LockUpdateBufferWrite - _outMemoryBuffer was nullptr; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// if args are valid, continue

				// reject lock if already locked for writing
				DebugPrint(2, "LockUpdateBufferWrite - check number of active write locks\n");
				if (lockChecker)
				{
					DebugPrint(2, "LockUpdateBufferWrite - write lock already active; return\n\n");
					return GReturn::FAILURE;
				}
				// otherwise, try to lock for writing
				DebugPrint(2, "LockUpdateBufferWrite - attempt to lock back buffer for write\n");
				if (+m_thread.LockSyncWrite())
				{
					DebugPrint(2, "LockUpdateBufferWrite - locked back buffer for write\n");
					lockChecker = true;
					// give buffer data pointer and dimensions to caller and return
					DebugPrint(2, "LockUpdateBufferWrite - set return values\n");
					*_outMemoryBuffer = m_bitmapData;
					_outWidth = m_bitmapWidth;
					_outHeight = m_bitmapHeight;
					DebugPrint(2, "LockUpdateBufferWrite - successful; return\n\n");
					return GReturn::SUCCESS;
				}
				DebugPrint(2, "LockUpdateBufferWrite - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}
			GReturn LockUpdateBufferRead(const Color** _outMemoryBuffer, unsigned short& _outWidth, unsigned short& _outHeight) override
			{
				DebugPrint(0, "\nLockUpdateBufferRead\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "LockUpdateBufferRead - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}

				// validate arguments
				if (_outMemoryBuffer == nullptr)
				{
					DebugPrint(2, "LockUpdateBufferWrite - _outMemoryBuffer was nullptr; return\n\n");
					return GReturn::INVALID_ARGUMENT;
				}
				// if args are valid, continue

				// reject lock if already locked for reading
				DebugPrint(2, "LockUpdateBufferRead - check number of active read locks\n");
				if (lockChecker)
				{
					DebugPrint(2, "LockUpdateBufferRead - read lock already active; return\n\n");
					return GReturn::FAILURE;
				}
				// otherwise, try to lock for reading
				DebugPrint(2, "LockUpdateBufferRead - attempt to lock front buffer for read\n");
				if (+m_thread.LockAsyncRead())
				{
					DebugPrint(2, "LockUpdateBufferRead - locked front buffer for read\n");
					lockChecker = true;
					// give buffer data pointer and dimensions to caller and return
					DebugPrint(2, "LockUpdateBufferRead - set return values\n");
					*_outMemoryBuffer = const_cast<const Color*>(m_bitmapData);
					_outWidth = m_bitmapWidth;
					_outHeight = m_bitmapHeight;
					DebugPrint(2, "LockUpdateBufferRead - successful; return\n\n");
					return GReturn::SUCCESS;
				}
				DebugPrint(2, "LockUpdateBufferRead - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

			GReturn UnlockUpdateBufferWrite() override
			{
				DebugPrint(0, "\nUnlockUpdateBufferWrite\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "UnlockUpdateBufferWrite - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				// ignore unlock if not locked
				if (!lockChecker)
				{
					DebugPrint(2, "UnlockUpdateBufferWrite - redundant; return\n\n");
					return GReturn::REDUNDANT;
				}

				lockChecker = false;

				// unlock and return
				DebugPrint(2, "UnlockUpdateBufferWrite - attempt to unlock self, back buffer for write\n");
				if (+m_thread.UnlockSyncWrite())
				{
					DebugPrint(2, "UnlockUpdateBufferWrite - unlocked self, back buffer for write; successful; return\n\n");
					return GReturn::SUCCESS;
				}

				DebugPrint(2, "UnlockUpdateBufferWrite - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}
			GReturn UnlockUpdateBufferRead() override
			{
				DebugPrint(0, "\nUnlockUpdateBufferRead\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "UnlockUpdateBufferRead - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				// ignore unlock if not locked
				if (!lockChecker)
				{
					DebugPrint(2, "UnlockUpdateBufferRead - redundant; return\n\n");
					return GReturn::REDUNDANT;
				}
				// otherwise, unlock
				lockChecker = false;
				DebugPrint(2, "UnlockUpdateBufferRead - attempt to unlock front buffer for read\n");
				if (+m_thread.UnlockAsyncRead())
				{
					DebugPrint(2, "UnlockUpdateBufferRead - unlocked front buffer for read\n");
					return GReturn::SUCCESS;
				}
				DebugPrint(2, "UnlockUpdateBufferRead - unexpected result; return\n\n");
				return GReturn::UNEXPECTED_RESULT; // should never reach here
			}

#pragma endregion LOCK_AND_UNLOCK_FUNCTIONS

			GReturn Present() override
			{
				DebugPrint(0, "\nPresent\n");
				if (m_surfaceState == SurfaceState::INVALID)
				{
					DebugPrint(2, "Present - surface invalid; return\n\n");
					return GReturn::PREMATURE_DEALLOCATION;
				}
				DebugPrint(2, "Present - draw\n");

				if (+m_thread.LockAsyncRead()) {
					ID3D11DeviceContext* context;
					m_d3d11Surface.GetImmediateContext((void**)&context);

					context->Map(m_d3d11Tex2D.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &m_d3d11MapSubRes);

					memcpy_s(m_d3d11MapSubRes.pData, static_cast<rsize_t>(m_bitmapWidth * m_bitmapHeight * sizeof(Color)),
						m_bitmapData, static_cast<rsize_t>(m_bitmapWidth * m_bitmapHeight * sizeof(Color)));

					context->Unmap(m_d3d11Tex2D.Get(), 0);

					context->Release();

					m_thread.UnlockAsyncRead();
				}

				// grab the context & render target
				IDXGISwapChain* swap;
				ID3D11DeviceContext* con;
				ID3D11RenderTargetView* view;
				m_d3d11Surface.GetImmediateContext((void**)&con);
				m_d3d11Surface.GetRenderTargetView((void**)&view);
				m_d3d11Surface.GetSwapchain((void**)&swap);
				// setup the pipeline
				ID3D11RenderTargetView* const views[] = { view };
				con->OMSetRenderTargets(ARRAYSIZE(views), views, nullptr);
				const UINT strides[] = { sizeof(float) * 4 };
				const UINT offsets[] = { 0 };
				ID3D11Buffer* const buffs[] = { m_d3d11VertBuff.Get() };
				con->IASetVertexBuffers(0, ARRAYSIZE(buffs), buffs, strides, offsets);
				con->VSSetShader(m_d3d11VertShad.Get(), nullptr, 0);
				con->PSSetShader(m_d3d11PixelShad.Get(), nullptr, 0);
				con->IASetInputLayout(m_d3d11VertexLayout.Get());
				// set a texture (srv)
				con->PSSetShaderResources(0, 1, m_d3d11ShaderResourceView.GetAddressOf());
				con->PSSetSamplers(0, 1, m_d3d11SamplerState.GetAddressOf());
				// now we can draw
				con->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
				con->Draw(4, 0);
				swap->Present(1, 0);
				// release temp handles
				view->Release();
				con->Release();
				swap->Release();

				DebugPrint(2, "Present - successful; return\n");
				return GReturn::SUCCESS;
			}

		}; // end class GRasterSurfaceImplementation
	} // end namespace I
} // end namespace GW


#if defined(_WIN32) && !defined(NDEBUG) && defined(GRASTERSURFACE_DEBUG_WIN32_VERBOSE_NEW)
#undef new
#endif

#if defined(GRASTERSURFACE_DEBUG_WIN32_VERBOSE_NEW)
#undef GRASTERSURFACE_DEBUG_WIN32_VERBOSE_NEW
#endif
#if defined(GRASTERSURFACE_DEBUG_WIN32_SERIALIZE_SMARTUPDATE_COPY)
#undef GRASTERSURFACE_DEBUG_WIN32_SERIALIZE_SMARTUPDATE_COPY
#endif
#if defined(GRASTERSURFACE_DEBUG_WIN32_ASSERTS)
#undef GRASTERSURFACE_DEBUG_WIN32_ASSERTS
#endif
#if defined(GRASTERSURFACE_DEBUG_WIN32_PRINTS)
#undef GRASTERSURFACE_DEBUG_WIN32_PRINTS
#endif
#if defined(GRASTERSURFACE_DEBUG_WIN32_PRINT_PRIORITY_LEVEL)
#undef GRASTERSURFACE_DEBUG_WIN32_PRINT_PRIORITY_LEVEL
#endif
