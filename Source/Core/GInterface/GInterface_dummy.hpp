// dummy implementation of GInterface
namespace GW {
	namespace I {
	class GInterfaceImplementation : public virtual GInterfaceInterface
	{
	public:
		GReturn Create() { return GReturn::INTERFACE_UNSUPPORTED; }
	};
	} // end CORE
} // end GW
