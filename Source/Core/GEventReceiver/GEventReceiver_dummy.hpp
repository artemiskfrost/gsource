namespace GW {
	namespace I {
		// dummy implementations allow for code compilation even when an interface is unsupported by a platform
		class GEventReceiverImplementation :	public virtual GEventReceiverInterface,
												private GThreadSharedImplementation
		{
		public:
			GReturn Create(CORE::GEventGenerator _listenToMe, std::function<void()> _callback) {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn Append(const GEvent& _inEvent) override {
				return GReturn::FAILURE;
			}
			GReturn Waiting(unsigned int& _outCount) const override {
				return GReturn::FAILURE;
			}
			GReturn Pop(GEvent& _outEvent) override {
				return GReturn::FAILURE;
			}
			GReturn Peek(GEvent& _outEvent) const override {
				return GReturn::FAILURE;
			}
			GReturn Missed(unsigned int& _outCount) const override {
				return GReturn::FAILURE;
			}
			GReturn Clear() override {
				return GReturn::FAILURE;
			}
			GReturn Invoke() const override {
				return GReturn::FAILURE;
			}
			template<class eventType>
			GReturn Find(eventType _check, bool _remove) {
				return GReturn::FAILURE;
			}
			template<class eventType, typename eventData>
			GReturn Find(eventType _check, bool _remove, eventData& _outData) {
				return GReturn::FAILURE;
			}
		};
	} // end CORE
} // end GW
// Required for dummy compilation but does nothing
namespace internal_gw {
	// making this variable volatile should hopefully stop compilers from aggressively removing its assignment
	static void(*volatile event_receiver_callback)(const GW::GEvent&, GW::CORE::GInterface&) = nullptr;
	template<class Proxy>
	static void event_receiver_logic(const GW::GEvent& _e, GW::CORE::GInterface& _i) {};
}