// internal namespace used for global Gateware variables & callbacks  
namespace internal_gw 
{
	// internal call that forwards arguments to internal std::function
	// The logic for this operation is implemented later once the GEventReceiver Proxy is defined
	static void(*volatile event_receiver_callback)(const GW::GEvent&, GW::CORE::GInterface&) = nullptr;
	
	// Actual logic for above callback, will be defined after declaration of GEventReceiver
	template<class Proxy> // will be defined as GEventReceiver
	static void event_receiver_logic(const GW::GEvent& _e, GW::CORE::GInterface& _i)
	{
		Proxy r(_i.Mirror()); // mirror for speed where possible & appropriate
		r.Append(_e); // if an invalid Proxy is requested this will fail to be initialized
		r.Invoke();
	};
} // end internal_gw namespace

// Make the implentation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {

		// Simply holds a GEvent and allows it to be updated by a GEventGenerator
		// Also connects itself to said Generator during creation
		class GEventReceiverImplementation :	public virtual GEventReceiverInterface,
												protected GThreadSharedImplementation, // resource locking
												public std::enable_shared_from_this<GInterfaceInterface>
		{
			// internal data members
			unsigned int eventWaiting = 0;
			unsigned int missedEvents = 0;
			std::function<void()> callback = nullptr;
			GEvent lastEvent; // can be used to see what happened

		public:
			// Must match Proxy argument list
			GReturn Create(CORE::GEventGenerator _listenToMe, std::function<void()> _callback)
			{
				if (_listenToMe && internal_gw::event_receiver_callback)
				{
					callback = _callback; // nullptr is ok, optional.	
					// This is how to create a Proxy from "this", ideally we try and avoid doing this.
					CORE::GInterface me(shared_from_this()); // we create a temporary so only weak access is transferred
					// While weak access is not as fast, it grants lifetime control and flexibility to the user.
					// If you want the most efficiency you should consider calling "Register" directly with a fixed callback.
					return _listenToMe.Register(me, internal_gw::event_receiver_callback);
				}
				else if(internal_gw::event_receiver_callback)
					return GReturn::INVALID_ARGUMENT; // cannot be null EventGenerator
				else
					return GReturn::UNEXPECTED_RESULT; // global callback was not assigned!
			}
			GReturn Append(const GEvent& _inEvent) override
			{
				if (+LockSyncWrite()) // making changes
				{
					if (eventWaiting) ++missedEvents; // did they miss the last one?
					lastEvent = _inEvent; // copy new event
					eventWaiting = 1; // let them know (we can only hold one)
					if (+UnlockSyncWrite()) // unlock 
						return GReturn::SUCCESS;
				}
				return GReturn::UNEXPECTED_RESULT; // should never get here
			}
			GReturn Waiting(unsigned int& _outCount) const override
			{
				LockAsyncRead();
				_outCount = eventWaiting;
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}
			GReturn Pop(GEvent& _outEvent) override
			{
				GReturn result = GReturn::FAILURE;
				if (+LockSyncWrite())
				{
					if (eventWaiting)
					{
						--eventWaiting;
						_outEvent = lastEvent;						
						result = GReturn::SUCCESS;
					}
					UnlockSyncWrite();
				}
				return result;
			}
			GReturn Peek(GEvent& _outEvent) const override
			{
				GReturn result = GReturn::FAILURE;
				if (+LockAsyncRead())
				{
					if (eventWaiting)
					{
						_outEvent = lastEvent;
						result = GReturn::SUCCESS;
					}
					UnlockAsyncRead();
				}
				return result;
			}
			GReturn Missed(unsigned int& _outCount) const override
			{
				LockAsyncRead();
				_outCount = missedEvents;
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}
			GReturn Clear() override
			{
				GReturn result = GReturn::FAILURE;
				if (+LockSyncWrite())
				{
					if (eventWaiting)
					{
						eventWaiting = 0;
						result = GReturn::SUCCESS;
					}
					UnlockSyncWrite();
				}
				return result;
			}
			GReturn Invoke() const override
			{
				if (callback)
				{					
					callback(); // Invoke internal callback (can be done from multiple threads)
					return GReturn::SUCCESS;
				}
				// No callback attached, so it cannot be invoked
				return GReturn::FAILURE; 
			}
			template<class eventType>
			GReturn Find(eventType _check, bool _remove)
			{
				eventType event;
				GReturn result = GReturn::FAILURE;
				if ((_remove) ? +LockSyncWrite() : +LockAsyncRead())
				{
					if (eventWaiting)
					{
						if (G_PASS(result = lastEvent.Read(event)))
						{
							if (event == _check) // found it?
							{
								if (_remove) --eventWaiting; // clear found event
								result = GReturn::SUCCESS;
							}
							else
								result = GReturn::FAILURE;
						}
					}
					(_remove) ? UnlockSyncWrite() : UnlockAsyncRead();
				}
				return result;
			}
			template<class eventType, typename eventData>
			GReturn Find(eventType _check, bool _remove, eventData& _outData)
			{
				eventType event;
				GReturn result = GReturn::FAILURE;
				if ((_remove) ? +LockSyncWrite() : +LockAsyncRead())
				{
					if (eventWaiting)
					{
						if (G_PASS(result = lastEvent.Read(event, _outData)))
						{
							if (event == _check) // found it?
							{
								if (_remove) --eventWaiting; // clear found event
								result = GReturn::SUCCESS;
							}
							else
								result = GReturn::FAILURE;
						}
					}
					(_remove) ? UnlockSyncWrite() : UnlockAsyncRead();
				}
				return result;
			}
		};

	} // end CORE
} // end GW
