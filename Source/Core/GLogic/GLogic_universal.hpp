// Make the implementation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {

		// Simply holds a std::function<void()> and allows it to be updated/invoked safely
		class GLogicImplementation :	public virtual GLogicInterface,
										protected GThreadSharedImplementation // resource locking
		{
			// internal data members
			std::function<void()> logic = nullptr;
		public:
			// Must match Proxy argument list
			GReturn Create(std::function<void()> _logic) {
				logic = _logic; // should move && in many cases
				return GReturn::SUCCESS;
			}
			GReturn Assign(std::function<void()> _newLogic) override {
				GReturn result;
				if (G_PASS(result = LockSyncWrite()))
				{
					logic = _newLogic; // routine updated
					return UnlockSyncWrite();
				}
				return result;
			}
			GReturn Invoke() const override {
				GReturn result = GReturn::IGNORED;
				thread_local bool recursing = false;
				if (recursing || G_PASS(result = LockAsyncRead()))
				{
					recursing = true; // recursion is allowed on this thread, bypass locks
					// run logic if available, allowing recursion on this thread
					if (logic) logic(); // routine called
					// if this was a recursive call ignore the Unlock since the stack unwind will handle it
					if (result != GReturn::IGNORED)
					{
						recursing = false;
						UnlockAsyncRead();
					}
					// report the result of the function call
					result = (logic) ? GReturn::SUCCESS : GReturn::FAILURE;
				}
				return result;
			}
		};

	} // end CORE
} // end GW
