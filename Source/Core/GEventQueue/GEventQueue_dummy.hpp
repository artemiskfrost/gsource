namespace GW {
	namespace I {
		// dummy implementations allow for code compilation even when an interface is unsupported by a platform
		class GEventQueueImplementation :	public virtual GEventQueueInterface,
											private GThreadSharedImplementation
		{
		public:
			// New operations
			GReturn Create(unsigned int _maxSize, CORE::GEventGenerator _listenToMe, std::function<void()> _callback) {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn Peek(unsigned int _eventIndex, GEvent& _outEvent) const override {
				return GReturn::FAILURE;
			}
			GReturn Max(unsigned int& _outSize) const override {
				return GReturn::FAILURE;
			}
			// Override from GEventReceiver
			GReturn Append(const GEvent& _inEvent) override {
				return GReturn::FAILURE;
			}
			GReturn Waiting(unsigned int& _outCount) const override {
				return GReturn::FAILURE;
			}
			GReturn Pop(GEvent& _outEvent) override {
				return GReturn::FAILURE;
			}
			GReturn Peek(GEvent& _outEvent) const override {
				return GReturn::FAILURE;
			}
			GReturn Missed(unsigned int& _outCount) const override {
				return GReturn::FAILURE;
			}
			GReturn Clear() override {
				return GReturn::FAILURE;
			}
			GReturn Invoke() const override {
				return GReturn::FAILURE;
			}
			// Overload Find templates
			template<class eventType>
			GReturn Find(eventType _check, bool _remove) {
				return GReturn::FAILURE;
			}
			template<class eventType, typename eventData>
			GReturn Find(eventType _check, bool _remove, eventData& _outData) {
				return GReturn::FAILURE;
			}
		};
	} // end CORE
} // end GW