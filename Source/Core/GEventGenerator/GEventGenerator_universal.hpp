#include <vector> // used to track message-able objects

// define temporary internal defines (GWI = Gateware Internal)
#define GWI_MAX_TRAVERSAL_STACK 32 

// Make the implentation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {

		// Holds a vector of GEvents which it periodically cleans and sends new events to.
		class GEventGeneratorImplementation :	public virtual GEventGeneratorInterface,
												protected GThreadSharedImplementation // handles resource locking
		{
			// internal data members
			std::vector<std::pair<CORE::GInterface, void(*)(const GEvent&, CORE::GInterface&)>> listeners;

			// Ok we use a per-function cache & smart recursion to lock down the vector.
			// We populate the cache and then execute the callbacks.

			// This should be safe from multiple threads.

			void RecursivePush(const GEvent& _newEvent, unsigned int _offset)
			{
				unsigned int numToInvoke = 0;
				CORE::GEventCache isCache;
				CORE::GEventResponder isResponder;
				// Local stack of objects to visit, can grow with recursion.
				// We use a caching technique because we don't know what logic is present in the callbacks.
				// For example a callback could invoke a "Register" or "Push" from this very class leading to a deadlock.
				std::pair<CORE::GInterface, void(*)(const GEvent&, CORE::GInterface&)> toInvoke[GWI_MAX_TRAVERSAL_STACK];
				// if this is the root call, lock for editing
				if (_offset == 0) 
					LockSyncWrite();
				// traversing the container backwards should make removals a bit more efficient
				int current = static_cast<int>(listeners.size()) - 1 - _offset;
				// traverse container, append to local array, remove dead proxies
				while (current >= 0 && numToInvoke < GWI_MAX_TRAVERSAL_STACK)
				{
					if (listeners[current].first) {
						toInvoke[numToInvoke] = listeners[current];
						numToInvoke++;
						current--;
					}
					else {
						listeners.erase(listeners.begin() + current);
						current--;
					}
				}
				// if we max local array then recurse with offset
				if (current >= 0) // still have some left?
					RecursivePush(_newEvent, _offset + GWI_MAX_TRAVERSAL_STACK);
				else // if you DON'T end up calling a recursive call, then unlock
					UnlockSyncWrite();
				// Traverse local array & Invoke each function & interface combo available.
				while (numToInvoke)
				{	// traverse backwards so we get proper FIFO traversal with recursion
					numToInvoke--;
					if (*toInvoke[numToInvoke].second != nullptr) { // this is the normal behavior
						(*toInvoke[numToInvoke].second)(_newEvent, toInvoke[numToInvoke].first);
					} // if a static handler is not present we will try to use a first class type
					else if (isResponder = toInvoke[numToInvoke].first.Mirror()) { 
						isResponder.Invoke(_newEvent); // handle it now
						isResponder = nullptr; // release handle
					}
					else if (isCache = toInvoke[numToInvoke].first.Mirror()) {
						isCache.Append(_newEvent); // handle it later
						isCache = nullptr; // release handle
					}
					toInvoke[numToInvoke].first = nullptr; // no hanging counts plz...
				}
			}
			// study
			// Q R S T U V X Y Z
			// On recurse with size 3
			// Z Y X // full
				// V U T // full
					// S R Q // full

			// execute FIFO traversal!

		public:

			GReturn Create()
			{
				return GReturn::SUCCESS; // just works
			}

			GReturn Register(CORE::GEventCache _recorder) override
			{
				if (_recorder)
				{
					LockSyncWrite();
					listeners.push_back( 
						std::pair<CORE::GInterface, 
						void(*)(const GEvent&, CORE::GInterface&)>
						(_recorder.Mirror(), nullptr));
					UnlockSyncWrite();
					return GReturn::SUCCESS;
				}
				return GReturn::INVALID_ARGUMENT;
			}

			GReturn Register(CORE::GEventResponder _responder) override
			{
				if (_responder)
				{
					LockSyncWrite();
					listeners.push_back(
						std::pair<CORE::GInterface,
						void(*)(const GEvent&, CORE::GInterface&)>
						(_responder.Mirror(), nullptr));
					UnlockSyncWrite();
					return GReturn::SUCCESS;
				}
				return GReturn::INVALID_ARGUMENT;
			}

			GReturn Register(CORE::GInterface _observer, void(*_callback)(const GEvent&, CORE::GInterface&)) override
			{
				if (_observer && _callback != nullptr)
				{
					LockSyncWrite();
					listeners.push_back( /* THIS IS FINE, THE TRAVERSAL IS ALREADY BACKWARDS! TEST NEXT! */
						std::pair<CORE::GInterface, void(*)(const GEvent&, CORE::GInterface&)>(_observer.Mirror(),_callback) );
					UnlockSyncWrite();
					return GReturn::SUCCESS;
				}
				return GReturn::INVALID_ARGUMENT;
			}

			GReturn Deregister(CORE::GInterface _observer) override
			{
				GReturn result = GReturn::INVALID_ARGUMENT;
				if (_observer && +LockSyncWrite()) 
				{
					result = GReturn::FAILURE;
					auto iter = listeners.begin();
					while (	iter != listeners.end() && 
							result == GReturn::FAILURE)
					{
						if (iter->first == _observer) {
							listeners.erase(iter);
							result = GReturn::SUCCESS;
						}
						else {
							iter = std::next(iter);
						}
					}
					UnlockSyncWrite();
				}
				return result;
			}
			
			GReturn Observers(unsigned int& _outCount) const override
			{
				LockAsyncRead();
				_outCount = static_cast<unsigned int>(listeners.size());
				UnlockAsyncRead();
				return GReturn::SUCCESS;
			}

			GReturn Push(const GEvent& _newEvent) override
			{
				if (listeners.empty()) // empty() should be thread safe on vector
					return GReturn::REDUNDANT; 
				// traverse & remove dead proxies while notifying receivers
				RecursivePush(_newEvent, 0);
				return GReturn::SUCCESS;
			}
		};

	} // end CORE
} // end GW

// undefine temporary internal defines
#undef GWI_MAX_TRAVERSAL_STACK 
