namespace GW {
	namespace I {
		// dummy implementations allow for code compilation even when an interface is unsupported by a platform
		class GThreadSharedImplementation :	public virtual GThreadSharedInterface,
											protected GInterfaceImplementation
		{
		public:
			GReturn Create() {
				return GReturn::INTERFACE_UNSUPPORTED;
			}
			GReturn LockAsyncRead() const override {
				return GReturn::FAILURE;
			}
			GReturn UnlockAsyncRead() const override {
				return GReturn::FAILURE;
			}
			GReturn LockSyncWrite() override {
				return GReturn::FAILURE;
			}
			GReturn UnlockSyncWrite() override {
				return GReturn::FAILURE;
			}
		};
	} // end CORE
} // end GW