// This files implements read & write locks through a generic mutex.
// Ideally you should replace this implementation with an optimized platform specific one.
// This version currently does not support asynchronous reads, may be updated with c++14 timed mutex.
#include <atomic>
#include <mutex>
#include <thread> // used for tracking thread ids to avoid deadlock
// Make the implementation belong to the proper gateware namespace
// We cannot use "using" here as this is an HPP and supports header only deployments
namespace GW {
	namespace I {

		// Comments in these files are up to the developer but should not be part of doxygen
		// The implementation of this class should use the fastest synchronization primitive per-platform
		// The private inheritance of "GInterfaceImplementation" is not strictly necessary here but it
		// does demonstrate how to reuse Gateware implementation code in future interface implementations. 
		class GThreadSharedImplementation :	public virtual GThreadSharedInterface, // <-- Always inherit an interface this way
											protected GInterfaceImplementation // <-- Always reuse implementation code like so
		{
			// Since we fully support C++11 you may inline initialize variables as needed.
			std::mutex sync; // used to synchronize reads & writes
			std::atomic<std::thread::id> locker; // ID of the only thread that has a lock.(used to detect deadlocks)
			std::atomic_bool mode; // tracking of active mode. false = read, true = write
		public:
			// ALL Implementations MUST have a "Create" function. You may pass any arguments you need.
			// ALL Implementations support only the default constructor, Use "Create" to actually initialize your class.
			GReturn Create()
			{
				// Init anything you cannot or do not wish to init inline here (c++11)
				return GReturn::SUCCESS;
			}
			// overload utility functions, these will be utilized by derived implementations of the parent interface
			GReturn LockAsyncRead() const override
			{
				// even though the function says async this implementation is synced for now
				if (locker == std::this_thread::get_id()) 
					return GReturn::DEADLOCK;
				// enables const correctness for downstream interfaces
				// not ideal but forcing Zero const functions for later Asynchronous interfaces is a non-option
				const_cast<std::mutex*>(&sync)->lock();
				*const_cast<std::atomic<std::thread::id>*>(&locker) = std::this_thread::get_id();
				*const_cast<std::atomic_bool*>(&mode) = false;
				// we have locked down this thread
				return GReturn::SUCCESS;
			}
			GReturn UnlockAsyncRead() const override
			{
				// even though the function says async this implementation is synced for now
				if (locker != std::this_thread::get_id() || mode == true) 
					return GReturn::FAILURE;
                // enables const correctness for downstream interfaces
				// not ideal but forcing Zero const functions for later Asynchronous interfaces is a non-option
				*const_cast<std::atomic<std::thread::id>*>(&locker) = std::thread::id();
				const_cast<std::mutex*>(&sync)->unlock();
				return GReturn::SUCCESS;
			}
			GReturn LockSyncWrite() override
			{
				// even though the function says async this implementation is synced for now
				if (locker == std::this_thread::get_id())
					return GReturn::DEADLOCK;
				sync.lock();
				locker = std::this_thread::get_id();
				mode = true;
				// we have locked down this thread
				return GReturn::SUCCESS;
			}
			GReturn UnlockSyncWrite() override
			{
				// even though the function says async this implementation is synced for now
				if (locker != std::this_thread::get_id() || mode == false)
					return GReturn::FAILURE;
				locker = std::thread::id();
				sync.unlock();
				return GReturn::SUCCESS;
			}
			// release any outstanding locks on destruction
			~GThreadSharedImplementation()
			{
				UnlockAsyncRead();
				UnlockSyncWrite();
			}
		};
		// End Gateware namespaces
	} // end CORE
} // end GW
