#!/bin/bash

# This is the install script, if you need to updae Vulkan then you should run the uninstall script 
# before running this one.

# don't run on anything but macOS

if [ ! $(uname) == "Darwin" ]; then
echo "LOG - This script should be run on macOS ONLY."
echo "LOG - It is meant to install the Vulkan SDK on macOS."
exit 1
fi


# Finds the mounted vulkansdk download

foundVulkan='0'

VDIR=$((find /Volumes -name "vulkansdk-macos*" -maxdepth 1) | head -n 1)
VULKANDIR=$VDIR

if [ ! -z "$VULKANDIR" ]; then

    foundVulkan='1';
    echo -e "\nLOG - Vulkan dmg mount install found..."

else

    echo -e "\nLOG - Vulkan dmg mount install not found..."
    
fi

if [ ! -z "$1" ]; then

    VULKANDIR="$1"
    if [ foundVulkan==1 ]; then
    
        echo "LOG - User inputted path detected, using custom path instead of mount..."
        
    else
    
        echo "LOG - Using user-inputted path..."
        
    fi

fi

echo "LOG - ${VULKANDIR}"

# Checks if all required files are present.
if [ ! -d "${VULKANDIR}/MoltenVK" ] || [ ! -e "${VULKANDIR}/install_vulkan.py" ] || [ ! -e "${VULKANDIR}/uninstall.sh" ]; then

    echo "LOG - Inputted directory did not contain Vulkan SDK, checking if SDK is mounted..."
    if [ foundVulkan=='1' ]; then
    
        VULKANDIR=$VDIR
        echo "LOG - Found Vulkan through mount... Using ${VULKANDIR} directory instead..."
        
    else
    
        echo "LOG - No Vulkan install found, please download from LunarG and provide the path."
        exit
        
    fi
fi

if [ ! -z "$VULKANDIR" ]; then

    echo "LOG - Vulkan found, attempting to install..."
    cd ${VULKANDIR};

    # Runs the python uninstall script (just in case they already have an installation)
    echo "LOG - Uninstalling any previously installed Vulkan SDK"
    ./uninstall.sh

    # Runs the python script for installing.
    echo "LOG - Installing Vulkan frameworks for macOS..."
    ./install_vulkan.py

    echo "LOG - macOS framework installed..."

    echo "LOG - Removing any previous Vulkan frameworks for iOS and tvOS..."

    rm -rf /Library/Frameworks/MoltenVK.framework
    rm -rf /Library/Frameworks/MoltenVK.xcframework

    echo "LOG - Installing Vulkan frameworks for iOS and tvOS..."

    cp -a MoltenVK/MoltenVK.xcframework /Library/Frameworks/

    echo "LOG - iOS and tvOS frameworks installed..."
    echo "LOG - All Vulkan frameworks installed"

fi
