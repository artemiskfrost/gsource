########################################################################
# This Cmake script generates a version number each time you compile

message("Begin Version Number Autogeneration:")

string( TIMESTAMP VERSION_MAJOR "%y" UTC) # last two digits of current year
string( TIMESTAMP VERSION_MINOR "%j" UTC) # acsending day of said year
string( TIMESTAMP VERSION_PATCH "%H" UTC) # UTC hour of said day
# strip any leading zeros so they are not interpreted as octal by c++
math(EXPR VERSION_MAJOR ${VERSION_MAJOR})
math(EXPR VERSION_MINOR ${VERSION_MINOR})
math(EXPR VERSION_PATCH ${VERSION_PATCH})

message("Major: " ${VERSION_MAJOR})
message("Minor: " ${VERSION_MINOR})
message("Patch: " ${VERSION_PATCH})

set(GIT_ERROR false) # Used to track git errors.

# Get name of working branch from Git.
execute_process(
    COMMAND git rev-parse --abbrev-ref HEAD
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_BRANCH
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

string(LENGTH "${GIT_BRANCH}" GIT_BRANCH_LENGTH)

if(${GIT_BRANCH_LENGTH} EQUAL 0)
    set(GIT_ERROR true)
endif()

message("Branch: " ${GIT_BRANCH})

# Get the latest abbreviated commit hash of the working branch.
execute_process(
    COMMAND git log -1 --format=%h
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_COMMIT_HASH
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

string(LENGTH "${GIT_COMMIT_HASH}" GIT_COMMIT_HASH_LENGTH)

if(${GIT_COMMIT_HASH_LENGTH} EQUAL 0)
    set(GIT_ERROR true)
endif()

message("Hash: " ${GIT_COMMIT_HASH})

# Output helpful error message if needed.
if(GIT_ERROR)
    if(WIN32)
        message(
            "ERROR: Failed to retrieve Git information.\n"
            "  This can occur for the following reasons:\n"
            "  - Git is not installed.\n"
            "  - The Git PATH is not set up in System Environment.\n"
            "  Solution: Download Git at \"https://gitforwindows.org/\". During\n"
            "            Git installation, choose the option for \"Git from the\n"
            "            command-line and also from 3rd-party software\"\n"
        )
    else(WIN32) # If on Linux, Mac, or some other non-Windows platform.
        message(
            "ERROR: Git command-line operations not detected on this platform.\n"
            "       Are you sure Git is installed?\n"
        )
    endif(WIN32)
endif(GIT_ERROR)

# Set GATEWARE_VERSION_STRING_LONG & GATEWARE_VERSION_STRING_LONG
SET(VERSION "Gateware v${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
SET(VERSION_LONG "${VERSION} (${GIT_BRANCH}) [${GIT_COMMIT_HASH}]")

SET(GIT_COMMIT_HASH ${GIT_COMMIT_HASH})
SET(GIT_BRANCH ${GIT_BRANCH})

message("Begin Writing Version File...")

configure_file (
    "${CMAKE_CURRENT_LIST_DIR}/../Source/Shared/Version.format"
    "${CMAKE_CURRENT_LIST_DIR}/../Source/Shared/GVersion.hpp"
)

SET (VERSION_SHORT "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

file(
WRITE "${CMAKE_CURRENT_LIST_DIR}/../Deploy/version.txt" "${VERSION_SHORT}" 
)

message("Version Number Autogeneration Complete!")
########################################################################
