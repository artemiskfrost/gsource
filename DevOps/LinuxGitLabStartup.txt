# These instructions allow GitLab runners installed on native Linux/Debian machines to access GPU resources.
# By default GitLab runners run as a daemon service, while conveinent this disallows GPU access.
# After you register and install your GitLab runner on a Linux machine do the following steps:
#  1.) Uninstall the auto runner using the terminal: "sudo gitlab-runner uninstall" press enter.
#  2.) Navigate to the folder "/etc/gitlab-runner" right-click and select "open as administrator".
#  3.) Open the "config.toml" file using a text editor and copy the contents to the clipboard.
#  4.) Navigate to your current home folder. ex: "/root/home/(you)"
#  5.) Open the "View" drop down, and check "Show Hidden Files". 
#  6.) Right-Click on an empty spot in the navigator and create a new folder called: ".gitlab-runner".
#  7.) Open the folder and right-click and create an empty file called: "config.toml".
#  8.) Open the empty file using a text editor and copy in the contents of the original .toml file. save it.
# 8a.) (*Bug Fix* show hidden files in "/root/home/(you)". Edit the ".bash_logout" file and comment out the last 3 lines)
# 8b.) ([optional] test the runner, it should work now by typing "gitlab-runner run" into a terminal) 

#  9.) Open the search bar and type "Startup Applications" and press enter.
# 10.) Click "Add" and in the script box type: "xterm -e gitlab-runner run"
# 11.) You should now be able to reboot the machine, login and see the runner service start.
# 12.) (optional) Enable autologin and find a way to hide/minimize this launch in the system tray if desired.

# NOTE: To get it to run locally you must run the runner as the current user with all defaults in user-mode!
# notice I am not using "sudo" nor am I specifiying a working directory. It seems basic but this is what is required.
gitlab-runner run 
# NOTE2: if "xterm" is not installed already enter: "sudo apt-get install xterm" into the terminal.
  