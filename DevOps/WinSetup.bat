:: Author: Alexander Cusaac
:: Date: 12/01/2023
:: Description: This batch file will start powershell and run the WinSetup.ps1 script at the current working directory

@echo off

:: Run WinSetup.ps1
powershell -NoProfile -ExecutionPolicy Bypass -Command "& {Set-Location %cd% ; .\WinSetup.ps1}"

exit /b