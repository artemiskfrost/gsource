#import <UIKit/UIKit.h>
#include <mutex>
#include <condition_variable>
#include <atomic>

// The following is in this file to help with code simplicity.
// It is the cleanest way I have found for the iOS files and GApp to communicate with eachother.

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


